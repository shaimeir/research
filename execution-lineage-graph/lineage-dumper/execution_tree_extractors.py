#!/usr/bin/env python3

import logging
import hashlib
import io
import copy
import extraction_filters
import contextlib
import traceback
import datetime
from collections import namedtuple

NoTime = datetime.datetime(1,1,1)

FetchGranularity = 1000

CsvHeader = ('timestamp', 'hostId', 'parentInstanceId', 'instanceId', 'secdoProcessId', 'secdoChildId', 'filePath', 'childFilePath', 'is_signed', 'is_sig_valid', 'sig_company', 'sig_product', 'depth')

class Layouts:
    # old testeu layout
    LayoutV1 = {
        'table' : 'event_stream',
        'table_filter' : "eventType = 'executed_by'",

        # columns
        'host_id' : 'hostId',
        'timestamp' : 'timestamp',

        ## 'child_cid' : 'childCID',
        'child_pid' : 'secdoChildId',
        'child_image' : 'childFilePath',
        'child_instance_id' : 'secdoChildInstanceId',

        ## 'parent_cid' : 'CID',
        'parent_pid' : 'secdoProcessId',
        'parent_image' : 'filePath',
        'parent_instance_id' : 'instanceId',
    }

    # the current up-to-date layout
    LayoutV2 = {
        'table' : 'process_instance_start',

        # columns
        'host_id' : 'hostId',
        'timestamp' : 'timestamp',

        ## 'child_cid' : 'CID',
        'child_pid' : 'secdoProcessId',
        'child_image' : 'processPath',
        'child_instance_id' : 'instanceId',

        ## 'parent_cid' : 'parentCID',
        'parent_pid' : 'secdoParentId',
        'parent_image' : 'parentFullFilePath',
        'parent_instance_id' : 'parentInstanceId',

        ## signature info
        'is_signed' : 'processSigned',
        'is_sig_valid' : 'processSignedValid',
        'sig_company' : 'processSignedCompanyName',
        'sig_product' : 'processSignedProductName',
    }

class ExecutionTreeExtractorBase(object):
    # granularity of 1M records should take less than 128MB of RAM at a time
    def __init__(self, connection, write_stream, fetch_granularity = FetchGranularity):
        self.connection = connection
        self.fetchGranularity = fetch_granularity
        self.writeStream = write_stream # CSV output 'stream' (we require .writerows())

        assert fetch_granularity > 0

        self.cursor = self.connection.cursor('list') # i am guessing this will make for faster lookups
        self.currentExecutionContext = {}

    def execute(self, query, fetchall=True, context={}):
        logging.info("executing query: %s", query)
        self.cursor.execute(query)

        if fetchall:
            return self.cursor.fetchall()

    def fetchmany(self, count):
        return self.cursor.fetchmany(count)

    @contextlib.contextmanager
    def executeContext(self, context):
        new_context = self.currentExecutionContext.copy()
        new_context.update(context)
        old_context = self.currentExecutionContext

        logging.info("setting new execution context %r", new_context)
        self.currentExecutionContext = new_context

        yield

        logging.info("clearing execution context")
        self.currentExecutionContext = old_context

    def __enter__(self):
        # create the ids table and the query results table
        self.execute("""
            -- id: temp_table

            CREATE LOCAL TEMPORARY TABLE process_ids_cache (
                timestamp Timestamp,
                process_id VARCHAR(80),
                host_id VARCHAR(80),
                instance_id INT
            )
            ON COMMIT PRESERVE ROWS
        """)

        # create the results table where queries results are dumped to
        self.execute("""
            -- id: temp_table

            CREATE LOCAL TEMPORARY TABLE results_table (
                timestamp Timestamp,
                host_id VARCHAR(80),
                parent_instance_id INT,
                child_instance_id INT,
                parent_id VARCHAR(80),
                child_id VARCHAR(80),
                parent_image VARCHAR(600),
                child_image VARCHAR(600),
                is_signed BOOLEAN,
                is_sig_valid BOOLEAN,
                sig_company VARCHAR(150),
                sig_product VARCHAR(150)
            )
            ON COMMIT PRESERVE ROWS
        """)

        # the roots ids cache
        self.execute("""
            -- id: temp_table

            CREATE LOCAL TEMPORARY TABLE roots_ids_cache (
                id INT,
                timestamp Timestamp,
                process_id VARCHAR(80),
                host_id VARCHAR(80),
                instance_id INT
            )
            ON COMMIT PRESERVE ROWS
        """)

        self.parentsExecutionInfoResultLayout = ["timestamp", "host_id", "parent_instance_id", "child_instance_id", "parent_id", "child_id", "parent_image", "child_image", "is_signed", "is_sig_valid", "sig_company", "sig_product"]
        self.childrenExecutionInfoResultLayout = ["timestamp", "host_id", "parent_instance_id", "child_instance_id", "parent_id", "child_id", "parent_image", "child_image", "is_signed", "is_sig_valid", "sig_company", "sig_product"]

        return self

    def __exit__(self, exc_type, exc_value, tb):
        if exc_type is not None:
            logging.info("leaving execution tree context because of an exception\n%s", "\n".join(traceback.format_exception(exc_type, exc_value, tb)))
        self.execute("DROP TABLE process_ids_cache, results_table, roots_ids_cache")

    def getParentsQuery(self):
        raise NotImplementedError('{}.getParentsQuery() is not implemented'.format(type(self).__name__))

    def getChildrenQuery(self):
        raise NotImplementedError('{}.getChildrenQuery() is not implemented'.format(type(self).__name__))

    def getRootsExecutionQueries(self):
        raise NotImplementedError('{}.getRootsExecutionQueries() is not implemented'.format(type(self).__name__))

    def getExpandedProcessNamesQuery(self, names):
        raise NotImplementedError('{}.getExpandedProcessNamesQuery() is not implemented'.format(type(self).__name__))

    def getParentsRefinementQuery(self, data_table, output_table, level_restriction):
        raise NotImplementedError('{}.getParentsRefinementQuery() is not implemented'.format(type(self).__name__))

    def getChildrenRefinementQuery(self, data_table, output_table, level_restriction):
        raise NotImplementedError('{}.getChildrenRefinementQuery() is not implemented'.format(type(self).__name__))

    def getLowestParentsRestrictionLevel(self):
        raise NotImplementedError('{}.getLowestParentsRestrictionLevel() is not implemented'.format(type(self).__name__))

    def getHighestChildrenRestrictionLevel(self):
        raise NotImplementedError('{}.getHighestChildrenRestrictionLevel() is not implemented'.format(type(self).__name__))

    # _ids is an array of pairs of secdo process id and a causality id
    def __fillIdsTable(self, _ids):
        _ids = list(_ids)

        logging.info("uploading %d pid/host/instance tuples to process_ids_cache", len(_ids))

        self.execute("TRUNCATE TABLE process_ids_cache")

        csv_escape = lambda s: s.replace(b"\\", b"\\\\").replace(b",", b"\\,")
        causalities_buf = b"\n".join([
                              b",".join(
                                tuple((
                                    csv_escape(str(element).encode("utf-8"))
                                    for element in _id
                                ))
                              )
                              for _id in _ids
                              if None not in _id
                          ])
        causalities_stream = io.BytesIO(causalities_buf)

        self.cursor.copy("COPY process_ids_cache FROM STDIN DELIMITER ',' ESCAPE AS '\\'", causalities_stream, buffer_size=len(causalities_buf))

        logging.info("ids table filled")

    def __fetchBatch(self, query, is_children_query, results_layout = None):
        logging.info("fetching a batch")

        logging.info("dropping current results set")
        self.execute("TRUNCATE TABLE results_table")

        count_result = self.execute("SELECT COUNT(*) AS _count FROM process_ids_cache")
        if count_result[0][0] == 0:
            logging.info("nothing to fetch - empty ids set")
            return

        logging.info("running query and dumping results in a table")
        self.execute("""
            INSERT INTO results_table
            {query}
        """.format(query=query))

        logging.info("yanking next iteration's ids to ids table")
        self.execute("TRUNCATE TABLE process_ids_cache")
        self.execute("""
            -- id: update_pid_cache

            INSERT INTO
                process_ids_cache

            SELECT DISTINCT
                timestamp,
                {id_column},
                host_id,
                {instance_id_column}

            FROM
                results_table
        """.format(id_column = "child_id" if is_children_query else "parent_id",
                   instance_id_column = "child_instance_id" if is_children_query else "parent_instance_id"))

        if results_layout is None:
            logging.info("not fetching results")
            return

        logging.info("fetching results")
        self.execute("""
            -- id: fetch_results

            SELECT DISTINCT
                {results_layout}

            FROM
                results_table
        """.format(results_layout=", ".join(results_layout)),
        False)
        results = [None]
        while len(results) > 0:
            results = self.fetchmany(self.fetchGranularity)
            logging.info("fetched a batch of %d entries", len(results))
            if len(results) > 0:
                yield results

        logging.info("done fetching parents")

    # used to fetch primal parents which will be traversed downwards to discover a prcesses neighbourhood.
    # we intend to keep the result in memory anyhow so no need to batch it
    def __fetchParents(self, level):
        logging.info("fetching parents of level %d", level)
        query = self.getParentsQuery(level)

        with self.executeContext({'level' : level, 'query_type' : 'parents fetch'}):
            yield from self.__fetchBatch(query, False, self.parentsExecutionInfoResultLayout)

        logging.info("done fetching parents of level %d", level)

    # fetch a node's children - note that we don't deal with cyclic lineage in this script
    def __fetchChildren(self, level):
        logging.info("fetching children of level %d", level)
        query = self.getChildrenQuery(level)

        with self.executeContext({'level' : level, 'query_type' : 'children fetch'}):
            yield from self.__fetchBatch(query, True, self.childrenExecutionInfoResultLayout)

        logging.info("done fetching parents of level %d", level)

    # get the roots of the tree and their respective execution tuples (proc. id, host id, instance id)
    def __fetchRoots(self, process_names):
        logging.info("fetching roots execution identifiers")

        root_names = list({hashlib.md5(process_name.lower().encode("utf-8")).hexdigest() for process_name in process_names})

        self.execute("TRUNCATE TABLE process_ids_cache")

        self.__fillIdsTable([(NoTime, rname, '', 0) for rname in root_names])

        query = self.getRootsExecutionQueries()
        # note we should mark the query as a children query because we are searching for creation by process names.
        # also, take advantage of the process/host/instance id yanking into the ids table
        list(self.__fetchBatch(query, True)) # use list() because __fetchBatch is a generator

        # now cache the roots ids
        self.execute("""
            -- id: commit_roots_cache

            INSERT INTO roots_ids_cache

            SELECT DISTINCT
                ROW_NUMBER() OVER(),
                *
            FROM
                (
                    SELECT DISTINCT
                        *
                    FROM
                        process_ids_cache
                ) distinct_ids
        """)

        # we'll need the roots execution identifiers once for the children and once for the parents so we have to fetch that info

        root_ids_count = self.execute("SELECT COUNT(*) FROM roots_ids_cache")[0][0]
        logging.info("fetched %d execution identifiers for the roots", root_ids_count)

        logging.info("roots execution identifiers fetched")

    @contextlib.contextmanager
    def __createRootFilteringAuxTables(self):
        self.execute("""
            CREATE LOCAL TEMPORARY TABLE filter_table_1 (
                originator_id INT,
                process_id VARCHAR(80),
                host_id VARCHAR(80),
                instance_id INT
            )
            ON COMMIT PRESERVE ROWS
        """)

        self.execute("""
            CREATE LOCAL TEMPORARY TABLE filter_table_2 (
                originator_id INT,
                process_id VARCHAR(80),
                host_id VARCHAR(80),
                instance_id INT
            )
            ON COMMIT PRESERVE ROWS
        """)

        yield

        self.execute("DROP TABLE filter_table_1, filter_table_2")

    def __doFilterRootsByGenerationConstraints(self):
        logging.info("filtering roots by generation constraints")
        inital_roots_count = self.execute("SELECT COUNT(*) FROM roots_ids_cache")[0][0]

        logging.info("on entry there are %d entries present in roots_ids_cache", inital_roots_count)

        table_roles = {
            True:  ("filter_table_1", "filter_table_2"),
            False: ("filter_table_2", "filter_table_1"),
        }
        table_role_selector = True
        data_table, output_table = table_roles[table_role_selector]

        logging.info("initializing %s", data_table)
        self.execute("""
            INSERT INTO {data_table}

            SELECT
                id,
                process_id,
                host_id,
                instance_id

            FROM
                roots_ids_cache
        """.format(data_table=data_table))

        logging.info("backtracking parents for %d generations", -self.getLowestParentsRestrictionLevel())
        for level in range(-1, self.getLowestParentsRestrictionLevel() - 1, -1):
            logging.info("executing level %d parent filter query", -level)
            self.execute(self.getParentsRefinementQuery(data_table, output_table, level),
                         context={'refinement' : 'parents',
                                  'level' : level,
                                  'data' : data_table,
                                  'output' : output_table})

            unfiltered_count = self.execute("SELECT COUNT(*) FROM {}".format(data_table))[0][0]
            filtered_count = self.execute("SELECT COUNT(*) FROM {}".format(output_table))[0][0]
            logging.info("iteration filtered %d entries to %d entries", unfiltered_count, filtered_count)

            # switch the roles of the tables
            table_role_selector = not table_role_selector
            data_table, output_table = table_roles[table_role_selector]
            # clear old entries
            self.execute("TRUNCATE TABLE {}".format(output_table))

        # load the roots filtered so far
        logging.info("reloading filtered roots into %s", output_table)
        self.execute("""
            INSERT INTO
                {output_table}

            SELECT DISTINCT
                id,
                process_id,
                host_id,
                instance_id

            FROM
                roots_ids_cache

            WHERE
                id IN (
                    SELECT DISTINCT
                        originator_id

                    FROM
                        {data_table}
                )
        """.format(output_table=output_table, data_table=data_table))

        # switch the roles of the tables, again
        table_role_selector = not table_role_selector
        data_table, output_table = table_roles[table_role_selector]
        # clear old entries
        self.execute("TRUNCATE TABLE {}".format(output_table))

        logging.info("tracking children for %d generations", self.getHighestChildrenRestrictionLevel())
        for level in range(1, self.getHighestChildrenRestrictionLevel() + 1):
            logging.info("executing level %d children filter query", level)
            self.execute(self.getChildrenRefinementQuery(data_table, output_table, level),
                         context={'refinement' : 'children',
                                  'level' : level,
                                  'data' : data_table,
                                  'output' : output_table})

            # switch the roles of the tables
            table_role_selector = not table_role_selector
            data_table, output_table = table_roles[table_role_selector]
            # clear old entries
            self.execute("TRUNCATE TABLE {}".format(output_table))

        logging.info("loading final results into roots_ids_cache")
        self.execute("""
            DELETE FROM
                roots_ids_cache

            WHERE
                id NOT IN (
                    SELECT DISTINCT
                        originator_id

                    FROM
                        {data_table}
                )
        """.format(data_table=data_table))

        filtered_roots_count = self.execute("SELECT COUNT(*) FROM roots_ids_cache")[0][0]

        logging.info("done filtering parents - roots count reduced from %d to %d", inital_roots_count, filtered_roots_count)

    def __filterRootsByGenerationConstraints(self):
        with self.__createRootFilteringAuxTables():
            with self.executeContext({'stage' : 'root refinement'}):
                self.__doFilterRootsByGenerationConstraints()

    # get all the processes that fit a list of patterns
    def expandProcessNames(self, process_names):
        logging.info("expanding %s", "\n\t".join(process_names))

        query = self.getExpandedProcessNamesQuery(process_names)

        expanded_processes = list(set([name[0] for name in self.execute(query)]))

        for name in expanded_processes:
            logging.info("expanded process name: %s", name)

        logging.info("expanded to %d processes", len(expanded_processes))

        return expanded_processes # filter out duplicates

    def __loadRootsToIdsTable(self):
        self.execute("TRUNCATE TABLE process_ids_cache")
        self.execute("""
            INSERT INTO
                process_ids_cache

            SELECT
                timestamp,
                process_id,
                host_id,
                instance_id
            FROM
                roots_ids_cache
        """)

    def loadRoots(self, process_names, pre_filter):
        logging.info("loading roots info")

        logging.info("expanding process names")
        assert len(process_names) > 0, "specify process names to dump execution tree for"
        process_names = self.expandProcessNames(process_names)

        logging.info("fetching root causalities")
        self.__fetchRoots(process_names)

        if pre_filter:
            logging.info("performing pre-filtering")
            self.__filterRootsByGenerationConstraints()
            self.__loadRootsToIdsTable()
        else:
            logging.info("skipping pre-filtering")

        logging.info("roots information loaded")

    def dumpExecutionTree(self, parent_depth, children_depth):
        logging.info("dumping execution lineage")

        # lookup the parent processes depth levels above
        logging.info("fetching parents")
        for depth in range(1,parent_depth+1):
            self.writeStream.writerows((line + [-depth] for line_batch in self.__fetchParents(-depth) for line in line_batch))

        logging.info("reloading roots into ids table")
        self.__loadRootsToIdsTable()

        # we have the roots for our tree, we can now find all children
        logging.info("fetching children")
        for depth in range(children_depth):
            self.writeStream.writerows((line + [depth] for line_batch in self.__fetchChildren(depth) for line in line_batch))

        logging.info("execution lineage info dumped")

class LayoutShimExecutionTreeExtractor(ExecutionTreeExtractorBase):
    @staticmethod
    def FreezeLayout(layout):
        return namedtuple('layout_{}'.format(layout.get('table', 'unknown')), list(layout.keys()))(**layout)

    def __init__(self, connection, write_stream, layout, fetch_granularity = FetchGranularity):
        super().__init__(connection, write_stream, fetch_granularity)
        self.layout = copy.deepcopy(layout)

        if 'table_filter' not in self.layout or self.layout['table_filter'] is None and self.layout['table_filter'] == '':
            self.layout['table_filter'] = '1=1' # prettify syntax

        self.layout = self.FreezeLayout(self.layout)

    # formats a generic query with the specific layout attached to the extractor
    def formatQuery(self, s, *args, **kwargs):
        return s.format(s, *args, layout=self.layout, lt=self.layout, **kwargs)

class BasicExecutionTreeExtractor(LayoutShimExecutionTreeExtractor):
    def __init__(self, connection, write_stream, layout, fetch_granularity = FetchGranularity, query_limit = None, filters = None):
        super().__init__(connection, write_stream, layout, fetch_granularity)
        self.filters = extraction_filters.NullExtractionFilters() if filters is None else filters

        self.limitStatement = "limit {}".format(query_limit) if query_limit is not None else ""
        self.staticFilter = self.formatQuery(self.filters.staticFilter())

        self.childrenQuery = self.formatQuery("""
            -- id: children_query

            SELECT DISTINCT
                {layout.timestamp} AS timestamp,
                {layout.host_id} AS host_id,
                {layout.parent_instance_id} AS parent_instance_id,
                {layout.child_instance_id} AS child_instance_id,
                {layout.parent_pid} AS parent_pid,
                {layout.child_pid} AS child_pid,
                {layout.parent_image} AS parent_image,
                {layout.child_image} AS child_image,
                {layout.is_signed} AS is_signed,
                {layout.is_sig_valid} AS is_sig_valid,
                {layout.sig_company} AS sig_company,
                {layout.sig_product} AS sig_product

            FROM
                {layout.table}

            WHERE
                ({static_filter}) AND
                ({layout.table_filter}) AND
                ({{query_restrictions}}) AND

                ({layout.parent_pid},
                 {layout.host_id},
                 {layout.parent_instance_id}) IN (
                    SELECT
                        process_id,
                        host_id,
                        instance_id

                    FROM
                        process_ids_cache

                    WHERE
                        ({timing_clause})
                )

            {limit_statement}
        """, static_filter=self.staticFilter,
             limit_statement=self.limitStatement,
             timing_clause=self.formatQuery(self.filters.childrenTimingFilter()))

        self.parentsQuery = self.formatQuery("""
            -- id: parents_query

            SELECT DISTINCT
                {layout.timestamp} AS timestamp,
                {layout.host_id} AS host_id,
                {layout.parent_instance_id} AS parent_instance_id,
                {layout.child_instance_id} AS child_instance_id,
                {layout.parent_pid} AS parent_pid,
                {layout.child_pid} AS child_pid,
                {layout.parent_image} AS parent_image,
                {layout.child_image} AS child_image,
                {layout.is_signed} AS is_signed,
                {layout.is_sig_valid} AS is_sig_valid,
                {layout.sig_company} AS sig_company,
                {layout.sig_product} AS sig_product

            FROM
                {layout.table}

            WHERE
                ({static_filter}) AND
                ({layout.table_filter}) AND
                ({{query_restrictions}}) AND

                ({layout.child_pid},
                 {layout.host_id},
                 {layout.child_instance_id}) IN (
                    SELECT
                        process_id,
                        host_id,
                        instance_id

                    FROM
                        process_ids_cache
                )

            {limit_statement}
        """, static_filter=self.staticFilter,
             limit_statement=self.limitStatement)

        self.rootsExecutionQuery = self.formatQuery("""
            -- roots_execution_query

            SELECT DISTINCT
                {layout.timestamp},
                {layout.host_id},
                0,
                {layout.child_instance_id},
                '',
                {layout.child_pid},
                '',
                ''

            FROM
                {layout.table}

            WHERE
                {layout.child_pid} IN (
                    SELECT process_id
                    FROM process_ids_cache
                )

                AND ({static_filter})

                AND ({level_filter})

            {limit_statement}
        """, limit_statement=self.limitStatement,
             level_filter=self.__generationRestriction(0),
             static_filter=self.staticFilter)

        self.parentsRefinementQuery = self.formatQuery("""
            -- id parents_refinement_query

            INSERT INTO
                {{output_table}}

            SELECT DISTINCT
                data_table.originator_id,
                execution_table.parent_pid,
                execution_table.host_id,
                execution_table.parent_instance_id

            FROM
                (
                    SELECT DISTINCT
                        {layout.host_id} AS host_id,
                        {layout.parent_pid} AS parent_pid,
                        {layout.parent_instance_id} AS parent_instance_id,
                        {layout.child_pid} AS child_pid,
                        {layout.child_instance_id} AS child_instance_id

                    FROM
                        {layout.table}

                    WHERE
                        ({layout.host_id}, {layout.child_pid}, {layout.child_instance_id}) IN
                        (
                            SELECT DISTINCT
                                host_id,
                                process_id,
                                instance_id

                            FROM
                                {{data_table}}
                        )

                        AND

                        ({{level_restriction}})
                ) execution_table

                    JOIN

                {{data_table}} AS data_table

                ON
                    execution_table.host_id = data_table.host_id AND
                    execution_table.child_pid = data_table.process_id AND
                    execution_table.child_instance_id = data_table.instance_id
        """)

        self.childrenRefinementQuery = self.formatQuery("""
            -- id: children_refinement_query

            INSERT INTO
                {{output_table}}

            SELECT DISTINCT
                data_table.originator_id,
                execution_table.child_pid,
                execution_table.host_id,
                execution_table.child_instance_id

            FROM
                (
                    SELECT DISTINCT
                        {layout.host_id} AS host_id,
                        {layout.parent_pid} AS parent_pid,
                        {layout.parent_instance_id} AS parent_instance_id,
                        {layout.child_pid} AS child_pid,
                        {layout.child_instance_id} AS child_instance_id

                    FROM
                        {layout.table}

                    WHERE
                        ({layout.host_id}, {layout.parent_pid}, {layout.parent_instance_id}) IN (
                            SELECT DISTINCT
                                host_id,
                                process_id,
                                instance_id

                            FROM
                                {{data_table}}
                        )

                        AND

                        ({{level_restriction}})
                ) execution_table

                    JOIN

                {{data_table}} AS data_table

                ON
                    execution_table.host_id = data_table.host_id AND
                    execution_table.parent_pid = data_table.process_id AND
                    execution_table.parent_instance_id = data_table.instance_id
        """)

    def __generationRestriction(self, level):
        return self.formatQuery(self.filters.lineageQueryRestrictions()[level])

    def getChildrenQuery(self, level):
        return self.childrenQuery.format(query_restrictions=self.__generationRestriction(level+1))

    def getParentsQuery(self, level):
        return self.parentsQuery.format(query_restrictions=self.__generationRestriction(level))

    def getRootsExecutionQueries(self):
        return self.rootsExecutionQuery

    def getExpandedProcessNamesQuery(self, names):
        names_clause = "\nOR\n".join([
            self.formatQuery("{layout.child_image} ILIKE '{name}'", name=n) for n in names
        ])

        return self.formatQuery("""
            -- name_expansion_query

            SELECT DISTINCT
                {layout.child_image}

            FROM
                {layout.table}

            WHERE
                ({static_filter}) AND
                ({names_clause}) AND
                ({level_filter})

            {limit_statement}
        """, static_filter=self.staticFilter,
             names_clause=names_clause,
             level_filter=self.__generationRestriction(0),
             limit_statement=self.limitStatement)

    # get all the processes that fit a list of patterns
    def expandProcessNames(self, process_names):
        expanded_processes = self.filters.filterExpandedProcessNames(super().expandProcessNames(process_names))
        for name in expanded_processes:
            logging.info("post filtering process name: %s", name)

        logging.info("filtered to %d processes", len(expanded_processes))

        return expanded_processes # filter out duplicates

    # allow interception of query execution
    def execute(self, query, fetchall=True, context={}):
        _context = self.currentExecutionContext.copy()
        _context.update(context)
        self.filters.preExecuteFilter(query, _context, self.cursor)

        return super().execute(query, fetchall)

    def getLowestParentsRestrictionLevel(self):
        restrictions = self.filters.lineageQueryRestrictions()
        return min(restrictions.keys()) if len(restrictions) > 0 else 0

    def getHighestChildrenRestrictionLevel(self):
        restrictions = self.filters.lineageQueryRestrictions()
        return max(restrictions.keys()) if len(restrictions) > 0 else 0

    def getParentsRefinementQuery(self, data_table, output_table, level):
        return self.parentsRefinementQuery.format(data_table=data_table,
                                                  output_table=output_table,
                                                  level_restriction=self.__generationRestriction(level))

    def getChildrenRefinementQuery(self, data_table, output_table, level):
        return self.childrenRefinementQuery.format(data_table=data_table,
                                                   output_table=output_table,
                                                   level_restriction=self.__generationRestriction(level))

