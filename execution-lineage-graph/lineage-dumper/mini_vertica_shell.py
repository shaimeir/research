#!/usr/bin/env python3

import tabulate
import vertica_python.errors
import logging
import pprint
import termcolor
import re
import csv
import sys
import traceback
import cmd
import contextlib
import readline
from utilities import text_utils

class MiniVerticaShell(cmd.Cmd):
    def __init__(self, completekey='tab', stdin=None, stdout=None, use_rawinput=False):
        super().__init__(completekey=completekey, stdin=text_utils.MultilineStdin(stdin if stdin is not None else sys.stdin), stdout=text_utils.ColoredText(stdout))
        self.use_rawinput = use_rawinput

        self.cursor = None
        self.query = None
        self.context = None
        self.lastSet = []

    def interact(self, cursor, query, context, prompt, intro=None):
        query = query.split("\n")
        boundary = max(map(len, query))
        query = text_utils.ColoredText.green("    \\\n".join((line + " "*(boundary-len(line)) for line in query)))

        self.prompt = text_utils.ColoredText.magenta(prompt)
        self.cursor = cursor
        self.query = query
        self.context = context
        self.lastSet = []

        if intro is None:
            intro = query
        intro = text_utils.ColoredText.cyan(intro)

        self.cmdloop(intro=intro)

    def default(self, arg):
        # assume arg to be an SQL query
        try:
            self.cursor.execute(arg)
            self.lastSet = self.cursor.fetchall()

            content = tabulate.tabulate(self.lastSet) + "\n"

            with self.stdout.colorContext('yellow'):
                self.stdout.write(content)

        except Exception:
            with self.stdout.colorContext('red'):
                self.stdout.write(traceback.format_exc() + "\n")

    def do_query(self, arg):
        """
        Print the query for which this shell has opened.
        """
        with self.stdout.colorContext('green'):
            self.stdout.write(self.query + "\n")

    def do_context(self, arg):
        """
        Print the context in which this shell is called.
        """
        with self.stdout.colorContext('green'):
            self.stdout.write(pprint.pformat(self.context) + "\n")

    def do_dumpcsv(self, arg):
        """
        Dump a query result into a csv file.
        """
        if arg is None:
            arg = ''
        arg = arg.strip()

        if '' == arg:
            self.stdout.write("enter dump file name: ")
            self.stdout.flush()
            output_file = self.stdin.readline().strip()
            with self.stdout.colorContext('blue'):
                self.stdout.write("dumping last result to file '{}'\n".format(output_file))
        else:
            arg_decomposition = arg.split(" ", 1)
            output_file = arg_decomposition[0]

            if len(arg_decomposition) == 2:
                try:
                    self.cursor.execute(arg_decomposition[1])
                    self.lastSet = self.cursor.fetchall()

                except Exception:
                    with self.stdout.colorContext('red'):
                        self.stdout.write(traceback.format_exc() + "\n")

                    return

        with open(output_file, "wt") as output_csv_file:
            csv_stream = csv.writer(output_csv_file, delimiter='\t', dialect='unix')
            csv_stream.writerows(self.lastSet)

    def do_csvdump(self, arg):
        """
        Dump a query result into a csv file.
        """
        return self.do_dumpcsv(arg)

    def emptyline(self):
        pass

    def postloop(self):
        self.cursor = None
        self.query = None
        self.context = None
        self.lastSet = []

    def do_quit(self, arg):
        """
        Quit the shell.
        """
        return True

    def do_EOF(self, arg):
        """
        Quit the shell.
        """
        return True

    def do_exit(self, arg):
        """
        Quit the shell.
        """
        return True

    def do_continue(self, arg):
        """
        Quit the shell.
        """
        return True

