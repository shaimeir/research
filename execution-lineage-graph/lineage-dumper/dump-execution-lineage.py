#!/usr/bin/env python3

import vertica_python
import argparse
import logging
import hashlib
import csv
import sys
import getpass
import os
import stat
import configparser
import execution_tree_extractors
import extraction_filters
import contextlib
import threading
import dateutil

import utilities.argparse_automation_wrappaer

DefaultsFile = os.path.expanduser("~/.research-verticas")

@contextlib.contextmanager
def stdout_as_context():
    yield sys.stdout

def scheme_from_company_name(company):
    return hashlib.md5(company.encode("utf-8")).hexdigest().lower()

def load_defaults(defaults_file):
    config = configparser.SafeConfigParser()
    config.read(defaults_file)

    strip_vals = lambda d: {k: d[k].strip() for k in d}
    return {section: strip_vals(dict(config[section])) for section in config.sections()}

def main():
    defaults = load_defaults(DefaultsFile)

    layout_factory = {
        1 : {
            'help' : "testeu style layout (with the 'event_stream' table)",
            'layout' : execution_tree_extractors.Layouts.LayoutV1,
        },
        2 : {
            'help' : "the newer layout (with the 'process_instance_start' table)",
            'layout' : execution_tree_extractors.Layouts.LayoutV2,
        },
    }
    default_layout_factory = 2

    layout_versions = list(layout_factory.keys())
    layout_versions.sort()
    layout_version_help = '\n'.join(['the layout of the database (default to {}).'.format(default_layout_factory)] + ["* {} - {}".format(v, layout_factory[v]['help']) for v in layout_factory])

    parser = utilities.argparse_automation_wrappaer.ArgumentParser(description="fetch lineage information by process name (no degredation of cyclic lineage is performed).\nyou can specifiy login information in {} and they will be used instead of missing arguments.".format(DefaultsFile))
    parser.add_argument("-v", "--vertica", required=True, choices=list(defaults.keys()), help="address of the Vertica to connect to.")
    parser.add_argument("-u", "--user", help="Vertica username. Taken from defaults file if not set.")
    parser.add_argument("-d", "--database", help="the database to attach to in vertica. Taken from defaults file if not set.")
    parser.add_argument("-w", "--password", help="Vertica user password. May be taken from defaults file if not set, otherwise interactively queried.")
    parser.add_argument("-s", "--schema", help="select the schema to apply when connecting to the database.")
    parser.add_argument("-o", "--output", output=True, default="-", help="output CSV file. '-' denotes stdout.")
    parser.add_argument("-p", "--depth", type=int, default=2, help="the number of children levels to traverse from the requested processes.")
    parser.add_argument("-l", "--layout-version", default=None, type=int, choices=list(layout_factory.keys()), help=layout_version_help)
    parser.add_argument("-i", "--interactive-filtering", action="store_true", help="display dialogs when filtering is required instead of performing trivial filtering.")
    parser.add_argument("--host-id", action='append', help="limit the queries to certain host ids. (this option is an aggregating one).")
    parser.add_argument("--instance-id", action="append", type=int, help="limit the level 0 results to certain possible instance ids")
    parser.add_argument("-g", "--generation-pattern", action='append', help=''.join((
        "limit the search through various generations with an SQL 'LIKE'-like pattern.\n",
        "the patterns can take the following forms:\n",
        "   (1) %%\\cmd.exe=-2\n",
        "   (2) %%\\ipconfig.exe=1\n",
        "   (3) %%\\svchost.exe\n",
        "(1) tells the query to look for a pattern that ends with '\\cmd.exe' in the grandparents generation.\n",
        "(2) tells the query to look for a pattern that ends with '\\ipconfig.exe' in the direct children generation.\n",
        "(3) tells the query to look for a pattern that ends with '\\svchost.exe' and the generation shall be assumed to be -1 (direct parents).\n",
    )))
    parser.add_argument("-r", "--raw-query", action="append", help="add raw queries to the static filtering.")
    parser.add_argument("-q", "--query-filter", action="append", help="add a pattern to match on running queries to inspect the database interactively when a match is found.")
    parser.add_argument("-b", "--before", type=dateutil.parser.parse, help="fetch results occurring before a specefied time.")
    parser.add_argument("-a", "--after", type=dateutil.parser.parse, help="fetch results occurring after a specefied time.")
    parser.add_argument("-t", "--children-timing", type=int, default=None, help="limit children fetching to children created this many seconds after the parent's creation.")
    parser.add_argument("--resource-pool", default=None, help="set the resource pool for the connection.")
    parser.add_argument("--parents-depth", type=int, default=None, help="the number of parents levels to traverse from the requested processes.\ndefaults to the depth option's value.")
    parser.add_argument("--query-limit", type=int, default=None, help="set the limit for the number of results per-query. may be taken from defaults file if not set.")
    parser.add_argument("--no-pre-filtering", action="store_true", help="don't run lineage filters in a pre-filtering iteration to dilute roots only to those that fit certain criteria.")
    parser.add_argument("--no-interactive-shell", action="store_true", help="don't hijack SIGQUIT to open an interactive shell")
    parser.add_argument("--verbose", action='store_true', help="enable debug output.")
    parser.add_argument("processes", nargs="+", help="the processes to dump lineage ties to. use an SQL 'LIKE' syntax.")

    args = parser.parse_args()

    assert args.children_timing is None or args.children_timing >= 0, 'children timing can only be a non-negative integer'

    default_vertica = defaults[args.vertica]

    args.vertica = default_vertica["vertica"]

    args.user = default_vertica['user'] if args.user is None else args.user
    args.database = default_vertica['database'] if args.database is None else args.database
    args.schema = args.schema if args.schema is not None else default_vertica.get('schema') if default_vertica.get('schema') is not None else scheme_from_company_name(default_vertica['company'])
    args.layout_version = int(default_vertica.get('layout', None)) if args.layout_version is None else args.layout_version
    args.resource_pool = default_vertica.get('resource_pool', None) if args.resource_pool is None else args.resource_pool

    args.password = default_vertica.get('password', None) if args.password is None else args.password
    args.query_limit = default_vertica.get('query_limit', None) if args.query_limit is None else args.query_limit

    if args.query_limit is not None:
        args.query_limit = int(args.query_limit)

    logging.basicConfig(format='%(levelname)s: %(asctime)s: %(message)s', level=logging.INFO if args.verbose else logging.WARNING)

    if args.layout_version is None:
        logging.info("using default layout version")
        args.layout_version = default_layout_factory

    layout = layout_factory[args.layout_version]['layout']

    if args.parents_depth is None:
        args.parents_depth = args.depth

    if args.password is None:
        args.password = getpass.getpass("Vertica password: ")

    logging.info("connecting to %s@%s", args.user, args.vertica)
    vertica_conn = vertica_python.connect(host=args.vertica,
                                          user=args.user,
                                          password=args.password,
                                          database=args.database)

    if args.resource_pool is not None:
        logging.info("setting resource pool: %s", args.resource_pool)
        vertica_conn.cursor().execute("SET SESSION RESOURCE_POOL = {}".format(args.resource_pool))
        vertica_conn.cursor().fetchall()

    if args.schema is not None:
        logging.info("setting schema to %s", args.schema)
        vertica_conn.cursor().execute("SET SEARCH_PATH TO '{}'".format(args.schema))
        vertica_conn.cursor().fetchall()

    logging.info("dumping lineage info for %s to %s", ", ".join(args.processes), args.output)

    # constructing filters
    filters_queue = extraction_filters.CombinedExtractionFilter()
    if args.interactive_filtering:
        logging.info("adding an interactive expanded names filter")
        filters_queue.appendFilter(extraction_filters.InteractiveFilters())

    if args.generation_pattern is not None and len(args.generation_pattern) > 0:
        logging.info("adding a generations pattern filter for the patterns: %s", ", ".join(args.generation_pattern))
        filters_queue.appendFilter(extraction_filters.GenerationPatternFilter(args.generation_pattern))

    if args.host_id is not None and len(args.host_id) > 0:
        logging.info("adding a host ids filter for: %s", ", ".join(args.host_id))
        filters_queue.appendFilter(extraction_filters.HostIdsFilter(args.host_id))

    if args.raw_query is not None and len(args.raw_query) > 0:
        for query in args.raw_query:
            logging.info("adding raw query filter: %s", query)
            filters_queue.appendFilter(extraction_filters.RawQueryFilter(query))

    if args.query_filter is not None:
        logging.info("adding interactive shell filter")
        filters_queue.appendFilter(extraction_filters.LiveShellExecutionFilter(args.query_filter))

    if args.instance_id is not None:
        logging.info("adding an instance ids filter")
        filters_queue.appendFilter(extraction_filters.InstanceIdFilter(args.instance_id))

    if args.before is not None:
        logging.info("adding a 'before' filter")
        filters_queue.appendFilter(extraction_filters.DateFilter(args.before, True))

    if args.after is not None:
        logging.info("adding a 'after' filter")
        filters_queue.appendFilter(extraction_filters.DateFilter(args.after, False))

    if args.children_timing is not None:
        logging.info("adding a children timing window of %d seconds", args.children_timing)
        filters_queue.appendFilter(extraction_filters.ChildrenTimingFilter(args.children_timing))

    # as a convinience, escape all backslashes
    processes = [p.replace("\\","\\\\") for p in args.processes]
    with (stdout_as_context() if args.output == '-' else open(args.output, "wt", newline='')) as output_csv_file:
        csv_stream = csv.writer(output_csv_file, delimiter='\t', dialect='unix')
        csv_stream.writerow(execution_tree_extractors.CsvHeader)
        with execution_tree_extractors.BasicExecutionTreeExtractor(
            vertica_conn,
            csv_stream,
            layout,
            query_limit=args.query_limit,
            filters=filters_queue
        ) as execution_tree_dumper:
            execution_tree_dumper.loadRoots(processes, not args.no_pre_filtering)
            execution_tree_dumper.dumpExecutionTree(args.parents_depth, args.depth)

    if "-" != args.output:
        logging.info("setting %s to read-only", args.output)
        os.chmod(args.output, stat.S_IRUSR | stat.S_IRGRP | stat.S_IROTH)

    logging.info("done")

if __name__ == "__main__":
    main()

