#!/usr/bin/env python3

import dialog
import logging
import signal
import sys
import code
import mini_vertica_shell
from collections import defaultdict

TrueStatement = "(1=1)"

# the basic interface for filters to apply at various points in the execution tree extractors.
# currently there's only a need for one point of interference
class ExtractionFilters(object):
    def __init__(self):
        pass

    def filterExpandedProcessNames(self, processes):
        raise NotImplementedError("{}.filterExpandedProcessNames() not implemented".format(type(self).__name__))

    def lineageQueryRestrictions(self):
        raise NotImplementedError("{}.parentsQueryRestrictions() not implemented".format(type(self).__name__))

    def staticFilter(self):
        raise NotImplementedError("{}.staticFilter() not implemented".format(type(self).__name__))

    def preExecuteFilter(self, query, context, cursor):
        raise NotImplementedError()

    def childrenTimingFilter(self):
        raise NotImplementedError()

class NullExtractionFilters(ExtractionFilters):
    def __init__(self):
        super().__init__()
        self.cacheQueryRestrictions = True

    def filterExpandedProcessNames(self, processes):
        return processes

    def lineageQueryRestrictions(self):
        return defaultdict(lambda : TrueStatement)

    def staticFilter(self):
        return TrueStatement

    def preExecuteFilter(self, query, context, cursor):
        pass

    def childrenTimingFilter(self):
        return TrueStatement

class CombinedExtractionFilter(ExtractionFilters):
    def __init__(self):
        super().__init__()
        self.filters = [NullExtractionFilters()]

        self.cachedMergedRestrictions = None
        self.cacheMergeRestrictions = None

    def appendFilter(self, new_filter):
        self.filters.append(new_filter)

    def filterExpandedProcessNames(self, processes):
        for f in self.filters:
            processes = f.filterExpandedProcessNames(processes)

        return processes

    def lineageQueryRestrictions(self):
        if self.cacheMergeRestrictions is None:
            # check if any of the filters has cacheQueryRestrictions set to False
            self.cacheMergeRestrictions = all((getattr(f, 'cacheQueryRestrictions', False) for f in self.filters))

        if self.cacheMergeRestrictions and self.cachedMergedRestrictions is not None:
            return self.cachedMergedRestrictions

        restrictions = [f.lineageQueryRestrictions() for f in self.filters]
        levels = set(sum((list(r.keys()) for r in restrictions), []))

        merged_restrictions = defaultdict(lambda : TrueStatement, {
            level : ' AND '.join(('({})'.format(r[level]) for r in restrictions if level in r)) for level in levels
        })

        if self.cacheMergeRestrictions:
            self.cachedMergedRestrictions = merged_restrictions

        logging.info("merged restrictions: %r", merged_restrictions)

        return merged_restrictions

    def staticFilter(self):
        return ' AND '.join(('({})'.format(f.staticFilter()) for f in self.filters))

    def preExecuteFilter(self, query, context, cursor):
        for f in self.filters:
            f.preExecuteFilter(query, context, cursor)

    def childrenTimingFilter(self):
        return ' AND '.join(('({})'.format(f.childrenTimingFilter()) for f in self.filters))

# limit selection to selected host ids
class HostIdsFilter(NullExtractionFilters):
    def __init__(self, host_ids):
        super().__init__()

        self.hostIdsFilter = TrueStatement # to make the syntax clearer in the query
        if host_ids is not None and len(host_ids) > 0:
            self.hostIdsFilter = "lower({{layout.host_id}}) IN ({})".format(",".join(("'{}'".format(host_id.lower()) for host_id in host_ids)))

    def staticFilter(self):
        logging.info("filter for host ids: %s", self.hostIdsFilter)
        return self.hostIdsFilter

# allow the user to pick which of the processes that the patterns he asked for will have their lineage dumped
class InteractiveFilters(NullExtractionFilters):
    def __init__(self):
        super().__init__()
        self.dialog = dialog.Dialog()

    def filterExpandedProcessNames(self, processes):
        logging.info("filtering with dialog: %r", processes)
        ok, filtered = self.dialog.checklist("Filter out what binaries to exclude from the query:",
                                             choices = [(p, "", True) for p in  processes],
                                             height=50,
                                             width=120,
                                             list_height=50
                                             )

        assert 'ok' == ok
        logging.info("filtered group: %r", filtered)
        return [f.replace("\\\\", "\\") for f in filtered] # unescape...

# limit the queries to parents that fit certain patterns
class GenerationPatternFilter(NullExtractionFilters):
    def __init__(self, patterns):
        # each entry in patterns should either be a pattern (an SQL 'LIKE'-like pattern) or a pattern terminated by a '=generation_number'
        # where generation_number is an integer.
        super().__init__()

        self.patterns = defaultdict(lambda : '1=1') # dict(level : [patterns...])

        _patterns = {}
        for pattern in patterns:
            pattern = pattern.rsplit('=', 1)
            if len(pattern) == 1:
                pattern.append("-1") # assume the default generation is direct parentage

            _patterns.setdefault(int(pattern[1]), [])
            _patterns[int(pattern[1])].append(pattern[0].lower().replace("\\","\\\\")) # escape '\'

        for level in _patterns:
            assert 0 != level, "can't apply a generation pattern"

            if level < 0: # parentage
                self.patterns[level] = ' OR '.join((("({{layout.parent_image}} ILIKE '{}')".format(p) for p in _patterns[level])))

            else: # children
                self.patterns[level] = ' OR '.join((("({{layout.child_image}} ILIKE '{}')".format(p) for p in _patterns[level])))

    def lineageQueryRestrictions(self):
        return self.patterns

# a raw query filter
class RawQueryFilter(NullExtractionFilters):
    def __init__(self, query, level=None):
        super().__init__()

        self.staticQuery = "(1=1)"
        self.lineageRestriction = defaultdict(lambda : "1=1")

        if level is None:
            _query_pair = query.rsplit(":", 1)
            if len(_query_pair) == 2:
                if "*" == _query_pair[0]:
                    self.staticQuery = _query
                else:
                    try:
                        self.lineageRestriction[int(_query_pair[1])] = _query_pair[0]
                    except:
                        self.staticQuery = query
            else:
                self.staticQuery = query
        else:
            self.lineageRestriction[level] = query

    def lineageQueryRestrictions(self):
        return self.lineageRestriction

    def staticFilter(self):
        return self.staticQuery

# filters level 0 by possible instance ids
class InstanceIdFilter(RawQueryFilter):
    def __init__(self, ids):
        filter_query = "{{layout.child_instance_id}} IN ({})".format(",".join(map(str, ids)))
        logging.info("adding instance id query filter: %s", filter_query)
        super().__init__(filter_query, 0)

class LiveShellExecutionFilter(NullExtractionFilters):
    def __init__(self, queries_filters):
        self.queriesFilters = queries_filters
        self.launchOnNextIteration = False
        self.prompt = "Vertica> "
        self.context = {}
        self.query = ""

        self.miniShell = mini_vertica_shell.MiniVerticaShell(stdout=sys.stderr)

        logging.info("registered queries filters: %r", queries_filters)

    def preExecuteFilter(self, query, context, cursor):
        logging.info("launchOnNextIteration = %r", self.launchOnNextIteration)
        if self.launchOnNextIteration:
            self.miniShell.interact(cursor, self.query, self.context, self.prompt, intro=self.query)
            self.launchOnNextIteration = False
            self.prompt = "Vertica> "
            self.context = {}
            self.query = ""

        for query_filter in self.queriesFilters:
            logging.info("checking query filter: '%s'", query_filter)
            if query_filter in query:
                logging.info("'%s' matched!", query_filter)
                self.launchOnNextIteration = True
                logging.info("launchOnNextIteration set to %r", self.launchOnNextIteration)
                self.prompt = query_filter + "> "
                self.context = context.copy()
                self.query = query
                break

class DateFilter(RawQueryFilter):
    def __init__(self, date, before):
        date_query = "{{layout.timestamp}} {inequality} '{date}'".format(
            inequality = "<=" if before else ">=",
            date = str(date)
        )
        super().__init__(date_query, 0)

class ChildrenTimingFilter(NullExtractionFilters):
    def __init__(self, timing_window):
        super().__init__()
        assert timing_window > 0, '%r is an invalid timing window' % timing_window
        self.childrenTimingFilterQuery = "(process_ids_cache.timestamp <= {{layout.table}}.{{layout.timestamp}}) AND (TIMESTAMPDIFF(SECOND, process_ids_cache.timestamp, {{layout.table}}.{{layout.timestamp}}) <= {timing_window:d})".format(timing_window=timing_window)

    def childrenTimingFilter(self):
        return self.childrenTimingFilterQuery

