#!/usr/bin/env python3

from collections import UserDict
import json
import logging
from collections import namedtuple

TreePair = namedtuple('TreePair', ('parents', 'children'))

class ExecutionTree(UserDict):
    @staticmethod
    def FromDict(dict_tree):
        assert len(dict_tree) == 1, "Can only generate tree from a single origin node"

        node_name = list(dict_tree.keys())[0]
        node = dict_tree[node_name]
        root = ExecutionTree(node_name, node['weight'])

        subtree_update_queue = [(root, node)]
        while len(subtree_update_queue) > 0:
            tree, node = subtree_update_queue.pop(0)

            subtree = node['subtree']
            for sub_node_name in subtree:
                sub_node = subtree[sub_node_name]
                tree[sub_node_name] = sub_node['weight']
                subtree_update_queue.append((tree[sub_node_name], sub_node))

        return root

    @staticmethod
    def DiffTree(reference_tree, tree, weigher):
        parents_diff = None
        children_diff = None

        if reference_tree.parents is None:
            assert tree.parents is None
        else:
            parents_diff = ExecutionTree._DiffTree(reference_tree.parents, tree.parents, weigher)

        if reference_tree.children is None:
            assert tree.children is None
        else:
            children_diff = ExecutionTree._DiffTree(reference_tree.children, tree.children, weigher)

        return TreePair(parents_diff, children_diff)

    @staticmethod
    def _DiffTree(reference_tree, tree, weigher):
        assert reference_tree.getId() == tree.getId(), "can't compare trees with different roots"

        diff_tree = ExecutionTree(reference_tree.getId(), weigher.unitWeight())

        diff_queue = [(diff_tree, reference_tree, tree)]
        while len(diff_queue) > 0:
            _diff_tree, _ref_tree, _tree = diff_queue.pop(0)

            for child in _tree:
                if child in _ref_tree:
                    _diff_tree[child] = weigher.diffWeight(_ref_tree[child].getWeight(), _tree[child].getWeight())
                    diff_queue.append((_diff_tree[child], _ref_tree[child], _tree[child]))

                    logging.debug("%s (level %d) diff between %r (ref) and %r (actual) = %r",
                                  child,
                                  _diff_tree[child].getLevel(),
                                  _ref_tree[child].getWeight(),
                                  _tree[child].getWeight(),
                                  _diff_tree[child].getWeight())
                else:
                    _diff_tree[child] = weigher.getWeightForNodeWithoutReference(_tree[child])

        return diff_tree

    def __init__(self, node_id = 'root', weight = {}, level = 0):
        super().__init__()
        self.weight = weight
        self.nodeId = node_id
        self.level = level

    def newChild(self, child_id, weight):
        assert child_id not in self.data, KeyError("{}.{}/{} already exists".format(self.nodeId, child_id, self.level+1))
        self.data[child_id] = ExecutionTree(child_id, weight, self.level+1)

    def __setitem__(self, child, weight):
        self.newChild(child, weight)

    def setdefault(self, child, weight):
        super().setdefault(child, ExecutionTree(child, weight, self.level+1))

    def getWeight(self):
        return self.weight

    def getId(self):
        return self.nodeId

    def getLevel(self):
        return self.level

    def __getAsDict(self):
        return {
            'subtree' : {
                child_id : self.data[child_id].__getAsDict() for child_id in self.data
            },
            'node' : self.nodeId,
            'weight' : self.weight,
            'level' : self.level,
        }

    def __str__(self):
        return json.dumps(self.__getAsDict(), indent=4)

    def __repr__(self):
        return str(self)

