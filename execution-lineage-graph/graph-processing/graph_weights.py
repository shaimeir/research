#!/usr/bin/env python3

import numpy
import logging
import contextlib

TheWeights = {}

def register_weight(name):
    def _register_weight_decorator(_class):
        global TheWeights
        TheWeights[name] = _class
        return _class

    return _register_weight_decorator

def get_weighers_dict():
    un_none = lambda _doc: 'no doc' if _doc is None else _doc
    return {
        name : un_none(getattr(TheWeights[name], '__doc__', None)).strip()
        for name in TheWeights
    }

def get_weigher(name):
    return TheWeights[name]

class WeigherBase:
    def __init__(self, variance_epsilon, mean_diff_epsilon, uses_parents=False, uses_children=True):
        assert variance_epsilon >= 0
        assert mean_diff_epsilon >= 0

        # some floating point calculation errors can cause a significant offshoot in the penalty resul.
        # for instance, we had a ref tree with a mean of 0.9999999999999996 and variance 1.9721522630525286e-31 compared
        # against a tree with a 1.0 mean. the result should have obviously been introduced without penalty, but instead
        # came out ~0.36 (because of mathematical errors...).
        # we use these limits to bar the possible errors.
        self.varianceEpsilon = variance_epsilon
        self.meanDiffEpsilon = mean_diff_epsilon

        self.usesParentsFlag = uses_parents
        self.usesChildrenFlag = uses_children

    def usesParents(self):
        return self.usesParentsFlag

    def usesChildren(self):
        return self.usesChildrenFlag

    def unitWeight(self):
        raise NotImplementedError()

    def diffWeight(self, ref_weight, weight):
        raise NotImplementedError()

    def getWeightForNodeWithoutReference(self, weight):
        raise NotImplementedError()

    def weighDiffTree(self, tree):
        raise NotImplementedError()

@register_weight('dummy')
class DummyWeigher(WeigherBase):
    """
    Dummy weigher used for debugging. The weight is just a dict with the diff information.
    """
    def __init__(self, variance_epsilon, mean_diff_epsilon):
        super().__init__(variance_epsilon, mean_diff_epsilon)

    def unitWeight(self):
        return "Unit Weight"

    def diffWeight(self, ref_weight, weight):
        return {
            'ref' : ref_weight,
            'weight' : weight,
            'var_eps' : self.varianceEpsilon,
            'mean_diff' : self.meanDiffEpsilon,
        }

    def getWeightForNodeWithoutReference(self, weight):
        return "Weightless"

    def weighDiffTree(self, tree):
        return "The internet doesn't weigh anything"

@register_weight('exponent')
class ExponentialPenaltyWeigher(WeigherBase):
    """
    Use sum(mean*exp(-(mean-instance_mean)^2/variance)) as a scoring method. Doesn't handle unreferenced children of the instance.
    """
    def __init__(self, variance_epsilon, mean_diff_epsilon, ignore_no_ref=False):
        super().__init__(variance_epsilon, mean_diff_epsilon)
        self.ignoreNoRef = ignore_no_ref

    def unitWeight(self):
        return {'weight' : 1}

    def diffWeight(self, ref_weight, weight):
        ref_variance = ref_weight['variance'] if ref_weight['variance'] > self.varianceEpsilon else 0
        mean_diff = numpy.power(ref_weight['mean'] - weight['mean'], 2)

        if ref_variance == 0:
            diff_weight = float(ref_weight['mean'] if mean_diff < self.meanDiffEpsilon else 0)
        else:
            if mean_diff < self.meanDiffEpsilon:
                diff_weight = ref_weight['mean'] # effectively zero, so no penalty

            diff_weight = float(
                ref_weight['mean']*numpy.exp(
                        -mean_diff/ref_variance
                    )
                )

        return {
            'weight' : diff_weight
        }

    def weighDiffTree(self, tree):
        return self._weighDiffTree(tree.children)

    def _weighDiffTree(self, tree):
        if len(tree) == 0:
            logging.debug("leaf %s (level %d) = %r", tree.getId(), tree.getLevel(), tree.getWeight())
            return tree.getWeight()['weight']

        weight = tree.getWeight()['weight'] * sum((self._weighDiffTree(tree[child]) for child in tree))
        logging.debug("node %s (level %d) = %r * (%s) = %r", tree.getId(), tree.getLevel(), tree.getWeight(), ", ".join(tree.keys()), weight)
        return weight

    def getWeightForNodeWithoutReference(self, weight):
        if self.ignoreNoRef:
            return {'weight' : 0}
        else:
            return super().getWeightForNodeWithoutReference(weight)

@register_weight('exponent-no-ref')
class ExponentialPenaltyWeigherIgnoreNoRef(ExponentialPenaltyWeigher):
    """
    Use sum(mean*exp(-(mean-instance_mean)^2/variance)) as a scoring method. Ignores unreferenced children of the instance.
    """
    def __init__(self, variance_epsilon, mean_diff_epsilon):
        super().__init__(variance_epsilon, mean_diff_epsilon, True)

@register_weight('uncontained-exponent')
class ExponentPenaltyWithContainmentWeigher(ExponentialPenaltyWeigherIgnoreNoRef):
    """
    Yield 100%% match if the children in the tree are all referenced, regardless of distribution. Otherwise revert to mean*exp(...) and ignore unreferenced children.
    """
    def __init__(self, variance_epsilon, mean_diff_epsilon):
        super().__init__(variance_epsilon, mean_diff_epsilon)
        self.weighingContext = False

    @contextlib.contextmanager
    def enterWeighingContext(self):
        self.weighingContext = True

        try:
            yield

        finally:
            self.weighingContext = False

    def getWeightForNodeWithoutReference(self, weight):
        no_ref_w = super().getWeightForNodeWithoutReference(weight)
        no_ref_w['no_ref'] = True
        return no_ref_w

    def weighDiffTree(self, tree):
        tree = tree.children

        if self.weighingContext:
            return super().weighDiffTree(tree)

        is_lineage_fully_contained = True

        tree_queue = [tree]
        while len(tree_queue) > 0 and is_lineage_fully_contained:
            _tree = tree_queue.pop(0)

            if _tree.getWeight().get('no_ref', False):
                is_lineage_fully_contained = False
            else:
                tree_queue += [_tree[child] for child in _tree]

        # if all the children are fully contained in the averege tree then the tree scores 100% similarity
        if is_lineage_fully_contained:
            return 1

        # otherwise fall back to old habits...
        with self.enterWeighingContext():
            return super().weighDiffTree(tree)

@register_weight('boolean')
class BooleanContainmentWeigher(WeigherBase):
    """
    Yield 100%% match if the instance lineage is fully contained in the averege lineage and 0%% otherwise.
    """
    def __init__(self, variance_epsilon, mean_diff_epsilon):
        super().__init__(variance_epsilon, mean_diff_epsilon)

    def getWeightForNodeWithoutReference(self, weight):
        return {'contained' : False}

    def diffWeight(self, ref_weight, weight):
        return {'contained' : True}

    def weighDiffTree(self, tree):
        tree = tree.children

        tree_queue = [tree]
        while len(tree_queue) > 0:
            _tree = tree_queue.pop(0)

            if _tree.getWeight().get('no_ref', False):
                return 0 # lineage not fully contained
            else:
                tree_queue += [_tree[child] for child in _tree]

        return 1 # lineage is fully contained

    def unitWeight(self):
        return {'contained' : True}

@register_weight('parentage-likelihood')
class ParentageLikelihoodWeigher(WeigherBase):
    """
    Assigns the aggregate ration as to each parent and uses it's multiplication as a similarity score.
    """
    def __init__(self, variance_epsilon, mean_diff_epsilon): # these arguments are meaningless
        super().__init__(variance_epsilon, mean_diff_epsilon, True, False) # use parents, skip children

    def getWeightForNodeWithoutReference(self, weight):
        return {'likelihood' : 0}

    def diffWeight(self, ref_weight, weight):
        return {'likelihood' : ref_weight['aggregate_percent']}

    def unitWeight(self):
        return {'likelihood' : 1}

    def weighDiffTree(self, tree):
        tree = tree.parents
        likelihood = 1

        while tree is not None:
            assert len(tree) <= 1
            likelihood *= tree.getWeight()['likelihood']

            tree = list(tree.values())[0] if len(tree) == 1 else None

        return likelihood

