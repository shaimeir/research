#!/usr/bin/env python3

import logging
import argparse
import glob
import os
import json
from collections import namedtuple
import tabulate

NodeRecord = namedtuple('NodeRecord', ['ancestry', 'level', 'weight'])
CorrelationRecord = namedtuple('CorrelationRecord', ['parent', 'child', 'marker'])

def extract_nodes(tree):
    nodes = []

    assert len(tree['subtree']) == 1, "no support for multiple roots"

    queue = [(0, tuple(), tree)]
    while len(queue) > 0:
        level, ancestry, _tree = queue.pop(0)

        for node, node_tree in _tree['subtree'].items():
            _ancestry = ancestry + (node,)
            nodes.append(NodeRecord(_ancestry, level - 1, node_tree['weight']['weight']))
            queue.append((level - 1, _ancestry, node_tree))

    return nodes

def fetch_correlations(tree, margin, marker):
    correlations = []

    parents = extract_nodes(tree['parents']['root'])
    children = extract_nodes(tree['children']['root'])

    for child in children:
        for parent in parents:
            if abs(parent.weight/child.weight - 1) <= margin:
                correlations.append(CorrelationRecord(parent, child, marker))
                break

    return correlations

def main():
    parser = argparse.ArgumentParser(description="looks for correlation between number of children and number of parents in an averege tree.")
    parser.add_argument("-v", "--verbose", action="store_true", help="print debug traces")
    parser.add_argument("-m", "--margin", type=float, default=0.05, help="the acceptable deviation from an exact 1:1 correlation for the ")
    parser.add_argument("json", nargs="+", help="a JSON file or a directory containing JSON files to look for correlations in.")

    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.WARN)

    jsons = []
    for _json in args.json:
        logging.debug("adding %s to batch", _json)
        if os.path.isdir(_json):
            jsons += glob.iglob(f"{_json}/*.json")

        else:
            jsons.append(_json)

    correlations = []
    for _json in jsons:
        with open(_json, "rt") as _json_f:
            tree = json.load(_json_f)

        correlations += fetch_correlations(tree, args.margin, _json)

    ancestry_to_str = lambda anc: '/'.join(anc)
    print(
        tabulate.tabulate((
            (record.marker, ancestry_to_str(record.parent.ancestry), ancestry_to_str(record.child.ancestry))
            for record in correlations
        ))
    )

if __name__ == "__main__":
    main()

