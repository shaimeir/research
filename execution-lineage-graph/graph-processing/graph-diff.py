#!/usr/bin/env python3

import argparse
import graph_weights
import execution_tree
import logging
import json
import os

import utilities.argparse_automation_wrappaer

def output(*args, **kwargs):
    print(*args, **kwargs, flush=True) # so we can tee this thing

def print_json_trees(jsons):
    for json_file in jsons:
        with open(json_file) as _jf:
            dict_tree_pair = json.load(_jf)
            output("parents:\n", execution_tree.ExecutionTree.FromDict(dict_tree_pair["parents"]), "\n\nchildren:\n", execution_tree.ExecutionTree.FromDict(dict_tree_pair["children"]), "\n\n")

def diff_trees_from_dict(tree_dict, weigher):
    parents = None
    children = None

    if weigher.usesParents():
        parents = execution_tree.ExecutionTree.FromDict(tree_dict["parents"])

    if weigher.usesChildren():
        children = execution_tree.ExecutionTree.FromDict(tree_dict["children"])

    return execution_tree.TreePair(parents=parents, children=children)

def load_json(filename, identifier_function, weigher):
    with open(filename) as _file:
        _d = json.load(_file)

    assert len(_d['parents']) <= 1, "can only accept a single root for reference trees"

    # handle (host,pid,instance) collisions by picking the most prevelant lineage
    return identifier_function(_d), diff_trees_from_dict(_d, weigher)

def print_json_trees_diff(ref_tree_file, tree_files, weigher, summary):
    _, ref_tree = load_json(ref_tree_file, lambda x: None, weigher)

    for tree_file in tree_files:
        _, tree = load_json(tree_file, lambda x: None, weigher)
        diff_tree = execution_tree.ExecutionTree.DiffTree(ref_tree, tree, weigher)

        if summary:
            output("{:.7f} {} {}".format(weigher.weighDiffTree(diff_tree), ref_tree_file, tree_file))
        else:
            output("diff tree\n", diff_tree, "\nweight: ", weigher.weighDiffTree(diff_tree))

def extract_parentage(tree_dict):
    assert len(tree_dict) <= 1, "can only accept a single root for reference trees"

    if len(tree_dict) == 0:
        return tuple()

    parentage = []
    active_parent_name = list(tree_dict.keys())[0]
    active_parent = tree_dict[active_parent_name]

    while active_parent != None:
        parentage.append(active_parent_name)

        subtree = active_parent.get('subtree', {})
        active_parent_name = max(subtree, default=None, key=lambda parent: subtree[parent].get('weight',{}).get('aggregate_percent',-1))
        active_parent = subtree.get(active_parent_name, None)

    return tuple(parentage)

def batch_comparison(ref_jsons, tree_jsons, ref_id_method, tree_id_method, weigher):
    ref_trees = {}
    comparison_tuples = []

    dirify = lambda d: [os.path.sep.join((d, p)) for p in os.listdir(d) if p.lower().endswith(".json") and os.path.isfile(os.path.sep.join((d, p)))]
    ref_jsons = sum([dirify(ref_json) if os.path.isdir(ref_json) else [ref_json] for ref_json in ref_jsons], [])

    logging.info("loading reference trees")
    for ref_json in ref_jsons:
        logging.info("loading ref json %s", ref_json)
        _id, tree = load_json(ref_json, ref_id_method, weigher)

        if _id in ref_trees:
            logging.error("%s has id %r which has already been loaded from %s! - skipping", ref_json, _id, ref_trees[_id]['file'])
        else:
            ref_trees[_id] = {
                'tree' : tree,
                'file' : ref_json,
            }

    logging.info("reference jsons loaded")

    logging.info("loading test trees")
    tree_jsons = sum([dirify(tree_json) if os.path.isdir(tree_json) else [tree_json] for tree_json in tree_jsons], [])
    for tree_json in tree_jsons:
        _id, tree = load_json(tree_json, tree_id_method, weigher)

        _ref_tree = ref_trees.get(_id, None)
        if _ref_tree is not None:
            diff_tree = execution_tree.ExecutionTree.DiffTree(_ref_tree['tree'], tree, weigher)
            comparison_tuples.append((weigher.weighDiffTree(diff_tree), tree_json, _ref_tree['file']))
            logging.info("loaded test tree %s", tree_json)

        else:
            logging.error("no reference tree found for %s", tree_json)

    return comparison_tuples

def print_child_diff(ref_jsons, tree_jsons, ref_id_method, tree_id_method, weigher):
    comparison_tuples = batch_comparison(ref_jsons, tree_jsons, ref_id_method, tree_id_method, weigher)
    comparison_tuples.sort(key=lambda t: t[0])

    for similarity_score, test_json, ref_json in comparison_tuples:
        output("{:.7f} {} {}".format(similarity_score, ref_json, test_json))

def main():
    identification_functions = {
        'parentage' : lambda d: extract_parentage(d['parents']),
        'tag' : lambda d: tuple(d['tag']),
        'alternative-tag' : lambda d: tuple(d['alternative_tag']),
    }
    id_func_types = list(identification_functions.keys())

    weigher_types = graph_weights.get_weighers_dict()

    parser = utilities.argparse_automation_wrappaer.ArgumentParser()
    parser.add_argument("-v", "--verbose", action="store_true", help="show debug output")
    parser.add_argument("--variance", type=float, default=0.00001, help="an upper bound for the variance to be considered 0.")
    parser.add_argument("--mean-diff", type=float, default=0.00001, help="an upper bound for the difference in means to be considered 0.")
    subparsers = parser.add_subparsers(help="", dest="action")

    printing_subparser = subparsers.add_parser('print', help='print the execution trees described in JSON files.')
    printing_subparser.add_argument("json", file=True, nargs="+", help="JSON files dumped by execution-graph-builder.py to print.")

    diff_subparser = subparsers.add_parser('diff', help='prints the difference tree between the children of two trees described by a couple of JSONs.')
    diff_subparser.add_argument("-w", "--weight", choices=list(weigher_types.keys()), required=True, help="the weight processing function to generate the weights in the diff tree. " + "\n".join((
        "* {} - {}.".format(_name, _doc) for _name, _doc in weigher_types.items()
    )))
    diff_subparser.add_argument("-s", "--summary", action="store_true", help="print just the score and the files comapred.")
    diff_subparser.add_argument("-r", "--ref-json", file=True, required=True, help="the reference JSON.")
    diff_subparser.add_argument("tree_json", file=True, metavar="tree-json", nargs="+", help="the trees to compare to the reference JSON.")

    batch_child_diff_subparser = subparsers.add_parser('child-diff', help='prints the similarity scores for a batch of JSON trees against a batch of averege trees.')
    batch_child_diff_subparser.add_argument("-w", "--weight", choices=list(weigher_types.keys()), required=True, help="the weight processing function to generate the weights in the diff tree. " + "\n".join((
        "* {} - {}.".format(_name, _doc) for _name, _doc in weigher_types.items()
    )))
    batch_child_diff_subparser.add_argument("-r", "--ref-json", file=True, required=True, action="append", help="a reference JSON or a directory containing reference JSONs.")
    batch_child_diff_subparser.add_argument("-i", "--ref-id-method", choices=id_func_types, default='tag', help="the method by which to index a reference tree for lookup.")
    batch_child_diff_subparser.add_argument("-t", "--tree-id-method", choices=id_func_types, default='alternative-tag', help="the method by which to index a tested JSON tree for lookup.")
    batch_child_diff_subparser.add_argument("tree_json", file=True, metavar="tree-json", nargs="+", help="JSON files or directories containing JSON files of the trees to compare to the reference JSONs.")

    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO, format="%(asctime)s %(message)s")

    if args.action == "print":
        print_json_trees(args.json)
    elif args.action == "diff":
        print_json_trees_diff(args.ref_json, args.tree_json, graph_weights.get_weigher(args.weight)(args.variance, args.mean_diff), args.summary)
    elif args.action == "child-diff":
        print_child_diff(args.ref_json,
                         args.tree_json,
                         identification_functions[args.ref_id_method],
                         identification_functions[args.tree_id_method],
                         graph_weights.get_weigher(args.weight)(args.variance, args.mean_diff))

if __name__ == "__main__":
    main()
