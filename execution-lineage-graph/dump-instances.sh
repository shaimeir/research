#!/usr/bin/env bash -eu

dump_script="$(dirname $(readlink -f ${BASH_SOURCE[0]}))"/dump-execution-lineage.py

# dump-execution-lineage.py header words order
hostId=1
parentInstanceId=2
instanceId=3
secdoProcessId=4
secdoChildId=5
filePath=6
childFilePath=7
weight=8
depth=9
# dump-execution-lineage.py output seperator
seperator='\t'

gpatterns=
vertica=
output_file=
multipattern=
interactive_query_magics=
before=
after=

exec 3<&0

function help()
{
    echo "usage: ${BASH_SOURCE[0]} -v <vertica> [-h] -o <output CSV file> -g <pattern>[=<level>] [-g <pattern>[=<level>] ...] <pattern> [<pattern>...]"
    echo "  -v - specify the name of the vertica in the defaults file to use."
    echo "  -o - specify the output CSV file to write the instances identifiers to. if -m is specified, this flag denotes an output directory."
    echo "  -g - specify a generation pattern in the format of ${dump_script}."
    echo "  -q - interactive query filter as described in ${dump_script}."
    echo "  -b - list instances created before a given time."
    echo "  -a - list instances created after a given time."
    echo "  -m - accepts a multipattern dump. read parent/grandparent patterns from a file and dump the instances list for all of those patterns."
    echo "  -h - display this help."
    echo "  patterns[...] - one or more SQL 'LIKE'-like patterns of file paths to look for."
}

function perror()
{
    echo ${@} >&2
    help >&2
    exit 1
}

function dump_instances()
{
    output_dump_file=${1}
    shift 1

    ${dump_script} -p 0 --parents-depth 1 -v ${vertica} --verbose -o - ${gpatterns} ${after} ${before} ${interactive_query_magics} ${@} |
        tr ${seperator} ',' |
        cut -d',' -f${hostId},${secdoChildId},${instanceId},${childFilePath} |
        tr -d \" |
        (
            read header
            echo ${header}
            sort -u
        ) |
        tee ${output_dump_file}
}

function dump_file_name()
{
    (echo ${1} | tr -d "\\" | tr -d '%' | tr '[:upper:]' '[:lower:]') 2>/dev/null
}

while getopts "v:g:o:q:a:b:m:h" option
do
    case "${option}" in
        "v") vertica="${OPTARG}"
             ;;
        "g") gpatterns="${gpatterns} -g ${OPTARG}"
             ;;
        "o") output_file="${OPTARG}"
             ;;
        "m") multipattern="${OPTARG}"
             ;;
        "q") interactive_query_magics="${interactive_query_magics} -q ${OPTARG}"
             ;;
        "b") before="-b '${OPTARG}'"
             ;;
        "a") after="-a '${OPTARG}'"
             ;;
        "h") help
             exit 0
             ;;
    esac
done
shift $((${OPTIND}-1))

[ "${vertica}" == "" ] && perror "missing vertica"
[ "${#}" -eq "0" ] && perror "no pattern specified"

if [ "${multipattern}" == "" ]
then
    # no multipattern
    dump_instances ${output_file} ${@}
else
    output_dir="${output_file}"
    mkdir -p "${output_dir}"

    while read parent_pattern grandparent_patter
    do
        dump_instances ${output_dir}/instances-dump-$(dump_file_name "${parent_pattern}")-$(dump_file_name "${grandparent_patter}")-$(head -c 5 /dev/urandom | md5 | head -c 10).csv \
            -g "${parent_pattern}=-1" -g "${grandparent_patter}=-2"  ${@} <&3
    done < "${multipattern}"
fi


