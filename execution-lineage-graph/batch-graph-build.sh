#!/usr/bin/env bash

set -eu

_readlink=readlink
_sed=sed
if [ "$(uname -s)" == "Darwin" ]
then
    which greadlink >/dev/null 2>&1 && _readlink="greadlink"
    which gsed >/dev/null 2>&1 && _sed="gsed"
fi

script_dir=$(dirname $(${_readlink} -f ${BASH_SOURCE[0]}))

output_directory=
verbosity=
alt_tag=
tagging_method=

function help()
{
    echo "usage: ${BASH_SOURCE[0]} -o <output directory> files[...]"
    echo "  -o - the output directory to generate the edges files in."
    echo "  -v - pass --verbose to execution-graph-builder.py."
    echo "  -t - tagging method."
    echo "  -a - alternative tagging method."
    echo "  -h - display this help"
    echo "  files[...] - a list of CSV files to process."
}

function edges_file_name()
{
    (echo $(basename ${1}) | ${_sed} -r 's/\.csv$//g' | awk '{print "edges_" $1}') 2>/dev/null
}

function perror()
{
    echo ${@} >&2
    help >&2
    exit 1
}

while getopts ":o:t:a:vh" option
do
    case "${option}" in
        "o") output_directory="${OPTARG}/"
             ;;
        "v") verbosity="--verbose"
             ;;
        "h") help
             exit 0
             ;;
        "t") tagging_method="-t ${OPTARG}"
             ;;
        "a") alt_tag="-a ${OPTARG}"
             ;;
    esac
done
shift $((${OPTIND}-1))

[ "${output_directory}" == "" ] && perror "missing output directory"
[ "${#}" -eq "0" ] && perror "no pattern specified"

mkdir -p ${output_directory}

for file in ${@}
do
    output_file=${output_directory}/$(edges_file_name ${file})
    echo "processing ${output_file}"
    ${script_dir}/graph-builder/execution-graph-builder.py ${verbosity} ${tagging_method} ${alt_tag} -o ${output_file} ${file} || echo "--- failed to build graph for ${file}" >&2
done

