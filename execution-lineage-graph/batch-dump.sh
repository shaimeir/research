#!/usr/bin/env bash

set -eu

dump_script="$(dirname $(readlink -f ${BASH_SOURCE[0]}))"/dump-execution-lineage.py

vertica=
output_directory=
parents_depth=
depth=

function dump_file_name()
{
    (echo ${1} | tr -d "\\" | tr -d '%' | tr '[:upper:]' '[:lower:]' | awk '{print $0 "-dump.csv"}') 2>/dev/null
}

function help()
{
    echo "usage: ${BASH_SOURCE[0]} [-h] -v <vertica> -o <output directory> patterns[...]"
    echo "  -v - specify the name of the vertica in the defaults file to use."
    echo "  -o - specify the output directory to write the dumps to."
    echo "  -d - the depth the dumps should be in."
    echo "  -p - give a different depth for parentage."
    echo "  -h - display this help."
    echo "  patterns[...] - one or more SQL 'LIKE'-like patterns of file paths to look for."
}

function perror()
{
    echo ${@} >&2
    help >&2
    exit 1
}

while getopts ":o:v:d:p:h" option
do
    case "${option}" in
        "o") output_directory="${OPTARG}"
             ;;
        "v") vertica="${OPTARG}"
             ;;
        "h") help
             exit 0
             ;;
        "p") parents_depth="--parents-depth ${OPTARG}"
             ;;
        "d") depth="--depth ${OPTARG}"
    esac
done
shift $((${OPTIND}-1))

[ "${output_directory}" == "" ] && perror "missing output directory"
[ "${vertica}" == "" ] && perror "missing vertica"
[ "${#}" -eq "0" ] && perror "no pattern specified"

mkdir -p "${output_directory}"

for pattern in ${@}
do
    echo "dumping information for pattern '${pattern}' in ${vertica}"
    ${dump_script} -v ${vertica} -o ${output_directory}/$(dump_file_name "${pattern}") --verbose ${parents_depth} ${depth} "${pattern}"
done

