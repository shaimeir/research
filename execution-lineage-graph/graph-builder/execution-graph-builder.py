#!/usr/bin/env python3

import argparse
import logging
import pathlib
import contextlib
import sys
import os
import tempfile
import six
import json
import glob
import pprint
import operator
import shutil
import uuid
import copy
import dateutil.parser
from dateutil.parser import parse as dateutil_parse
import datetime
import numpy
from numpy import float64
from collections import Hashable
import humanfriendly

import multiprocessing
from pyspark import SparkContext
from pyspark.sql.functions import col
from pyspark.sql import SQLContext
from pyspark.sql.types import StringType

import graph_formatting
import path_transformations
import node_tagging
import preliminary_row_filters
import edge_comments
import edge_id_funcs
from utilities import beep, rdd_tracing
import utilities.argparse_automation_wrappaer

WindowsSep = '\\'
NoChildMarker = "no_child"
NoParentMarker = "no_parent"

def save_rdd_to_text_file(output_file, rdd, keep_temp_files_on_error):
    @contextlib.contextmanager
    def __mkdtemp(_output_file):
        temp_output_base = None

        try:
            output_base = tempfile.mkdtemp(dir=os.path.dirname(_output_file))
            yield output_base

        except:
            if keep_temp_files_on_error:
                logging.info("an error occurred when writing RDD to file (%s) and temp files are kept", output_file)
            else:
                shutil.rmtree(output_base)
        else:
            shutil.rmtree(output_base)

    with __mkdtemp(os.path.dirname(output_file)) as tmp_output_base:
        logging.info("using temp directory: %s", tmp_output_base)
        tmp_output = os.path.sep.join((tmp_output_base, "data"))
        rdd.saveAsTextFile(tmp_output)

        actual_output = list(glob.iglob(os.path.sep.join((tmp_output, "part-*"))))[0]
        shutil.move(actual_output, output_file)

def to_unicode(s):
    return s if isinstance(s, six.text_type) else six.u(s)

def next_level(level):
    return level + (-1 if level < 0 else 1)

def to_hashable(tag):
    if isinstance(tag, Hashable):
        return tag
    elif isinstance(tag, list):
        return tuple(tag)
    else:
        raise NotImplementedError("can't really make %r hashable..." % tag)

def descriptor_id_str(d):
    return f"{d['depth']}/{d['host']}/{d['pid']}/{d['file'][-1]}/{d['instance']}"

@contextlib.contextmanager
def pyspark_context(log_level=None):
    logging.info("creating spark environment...")

    sc = SparkContext("local[{}]".format(multiprocessing.cpu_count()), "pyspark-{!s}".format(uuid.uuid4()))
    if log_level is not None:
        logging.info("setting spark log level to %s", log_level)
        sc.setLogLevel(log_level.upper())

    yield SQLContext(sc)

    sc.stop()

def preliminary_depth_filter(row, max_child_depth, max_parent_depth):
    with contextlib.suppress(ValueError):
        depth = int(row['depth'])
        return (max_child_depth is None or depth <= max_child_depth) and (max_parent_depth is None or depth >= max_parent_depth)

    return False # depth is not an integer

def to_dict_augment_row(row):
    d = row.asDict()
    d.setdefault('timestamp', None)
    d.setdefault('is_signed', None)
    d.setdefault('is_sig_valid', None)
    d.setdefault('sig_company', None)
    d.setdefault('sig_product', None)
    return d

def build_exec_descriptor(hostId,
                          parentInstanceId,
                          instanceId,
                          secdoProcessId,
                          secdoChildId,
                          filePath,
                          childFilePath,
                          depth,
                          timestamp,
                          is_signed,
                          is_sig_valid,
                          sig_company,
                          sig_product,
                          path_transformation,
                          pre_flipped,
                          depth_canonicalized):
    hostId = "<none>" if hostId is None else hostId
    parentInstanceId = int(-1 if parentInstanceId is None else parentInstanceId)
    instanceId = int(-1 if instanceId is None else instanceId)
    secdoProcessId = "<none>" if secdoProcessId is None else secdoProcessId
    secdoChildId = "<none>" if secdoChildId is None else secdoChildId
    filePath = to_unicode("<none>" if filePath is None else filePath)
    childFilePath = to_unicode("<none>" if childFilePath is None else childFilePath)
    depth = int(depth)

    filePath = path_transformation(filePath)
    childFilePath = path_transformation(childFilePath)
    if not depth_canonicalized:
        depth = depth if depth < 0 else depth + 1 # now depth +-1 describes an edge from 0 to +-1, +-2 from +-1 to +-2 and so on

    if depth < 0 and not pre_flipped:
        parentInstanceId, instanceId = instanceId, parentInstanceId
        secdoProcessId, secdoChildId = secdoChildId, secdoProcessId
        filePath, childFilePath = childFilePath, filePath

    exec_desc = {
        'desc_type' : 'record',
        'timestamp' : None if timestamp is None else dateutil_parse(timestamp),

        'is_signed' : None,
        'is_sig_valid' : None,
        'sig_company' : None,
        'sig_product' : None,

        'depth' : depth,
        'host' : hostId,

        'pid' : secdoProcessId,
        'instance' : parentInstanceId,
        'file' : (depth, filePath),

        # ancestry is the route taken from a root node to the current node (root to parent or root to child).
        # this is used to extract the parentage and perform instance-based tagging
        'ancestry' : [(depth, hostId, secdoProcessId, parentInstanceId, filePath)],
        # parentage is the complete path from the root the current node is born from to it's farthermost parent.
        # this is used to perform origination-based tagging (extract average trees by the parents).
        'parentage' : None,

        'augmented' : False,

        'child' : {
            'pid' : secdoChildId,
            'instance' : instanceId,
            'file' : childFilePath,
            'is_no_child' : False,

            'is_signed' : None,
            'is_sig_valid' : None,
            'sig_company' : None,
            'sig_product' : None,
        },
    }


    if is_signed is not None:
        siginfo = {
            'is_signed' : is_signed if isinstance(is_signed, bool) else "true" == is_signed.lower(),
            'is_sig_valid' : False if is_sig_valid is None else is_sig_valid if isinstance(is_sig_valid, bool) else "true" == is_sig_valid.lower(),
            'sig_company' : "<no_company>" if sig_company is None else sig_company,
            'sig_product' : "<no_product>" if sig_product is None else sig_product,
        }

        if depth < 0:
            exec_desc.update(siginfo)
        else:
            exec_desc['child'].update(siginfo)

    return exec_desc

def instance_identification_key(desc, child):
    if child:
        return (next_level(desc['depth']), desc['host'], desc['child']['pid'], desc['child']['instance'])

    else:
        return (desc['depth'], desc['host'], desc['pid'], desc['instance'])

def build_root_sig_augmentation_entry(desc):
    if desc['is_signed'] is None:
        return []

    return [
        (
            (1,) + instance_identification_key(desc, False)[1:],
            (desc['is_signed'], desc['is_sig_valid'], desc['sig_company'], desc['sig_product'])
        )
    ]

def augment_root_signatures(key_desc_sig):
    desc, sig_info = key_desc_sig[1][0], key_desc_sig[1][1]

    if sig_info is not None:
        desc['is_signed'] = sig_info[0]
        desc['is_sig_valid'] = sig_info[1]
        desc['sig_company'] = sig_info[2]
        desc['sig_product'] = sig_info[3]

    return desc

def build_no_children_resolution_descriptors(desc, no_parent_marker, no_child_marker, min_depth, max_depth):
    build_key_pair = lambda d: (instance_identification_key(d, False), [d])

    if desc['depth'] >= max_depth or desc['depth'] <= min_depth:
        return [build_key_pair(desc)]

    marker = no_parent_marker if desc['depth'] < 0 else no_child_marker

    child_depth = next_level(desc['depth'])
    child_no_children_desc = build_exec_descriptor(
        desc['host'],
        desc['child']['instance'],
        -desc['child']['instance'], # stub instance id
        desc['child']['pid'],
        desc['child']['pid'] + "." + marker, # stub pid
        desc['child']['file'],
        desc['child']['file'] + "." + marker, # stub file path
        child_depth,
        None,
        desc['child']['is_signed'],
        desc['child']['is_sig_valid'],
        desc['child']['sig_company'],
        desc['child']['sig_product'],
        lambda p: p,
        pre_flipped=True,
        depth_canonicalized=True
    )
    child_no_children_desc['child']['is_no_child'] = True

    if abs(desc['depth']) != 1 or (desc['depth'] < 0 and max_depth < 1) or (desc['depth'] > 0 and min_depth > -1):
        return [build_key_pair(desc), build_key_pair(child_no_children_desc)]

    # create a no parent entry for root elements
    marker = no_parent_marker if marker == no_child_marker else no_child_marker
    parent_depth = -desc['depth']
    parent_no_parents_desc = build_exec_descriptor(
        desc['host'],
        desc['instance'],
        -desc['instance'], # stub instance id
        desc['pid'],
        desc['pid'] + "." + marker, # stub pid
        desc['file'][-1],
        desc['file'][-1] + "." + marker, # stub file path
        parent_depth,
        None,
        desc['is_signed'],
        desc['is_sig_valid'],
        desc['sig_company'],
        desc['sig_product'],
        lambda p: p,
        pre_flipped=True,
        depth_canonicalized=True
    )
    parent_no_parents_desc['child']['is_no_child'] = True

    return [build_key_pair(desc), build_key_pair(child_no_children_desc), build_key_pair(parent_no_parents_desc)]

# this drops duplicities in (host, pid, instance id) as well
def drop_unnecessary_no_children_descriptors(desc1, desc2):
    # only instances with at least two entries (i.e. at least one non 'no-parent'/'no-child' child) get here
    # so we can basically remove all the 'is_no_child' entries and concatenate the lists
    if len(desc1) == len(desc2) == 1 and desc1[0] == desc2[0] and desc1[0]['child']['is_no_child']:
        return desc1 # keep the no-parent/no-child entry

    return list(filter(lambda e: not e['child']['is_no_child'], desc1 + desc2))

def publish_generation_name(desc, level):
    k, _desc = desc
    if abs(_desc['depth']) != level:
        return []

    child_augmentation_descriptor = {
        'file' : _desc['file'],
        'ancestry' : _desc['ancestry'],
        'timestamp' : _desc['timestamp'],

        'sig' : {
            'is_signed' : _desc['child']['is_signed'],
            'is_sig_valid' : _desc['child']['is_sig_valid'],
            'sig_company' : _desc['child']['sig_company'],
            'sig_product' : _desc['child']['sig_product'],
        },

        'is_augmentation_entry' : True,
    }

    # this causes desc to appear as many times as desc created children, so we'll need an extra step to rid ourselves of the extras
    return [(instance_identification_key(_desc, True), child_augmentation_descriptor)]

def update_augmentation_entry(k_desc_aug):
    k, _desc_aug = k_desc_aug
    exec_desc, aug_desc = _desc_aug

    # sometimes this function will be entered with identical instances because

    if exec_desc['augmented'] or aug_desc is None:
        return (k, exec_desc)

    exec_desc['file'] = aug_desc['file'] + exec_desc['file'][1:]
    exec_desc['ancestry'] = aug_desc['ancestry'] + exec_desc['ancestry']
    exec_desc['ancestor_timestamp'] = aug_desc['timestamp']
    exec_desc['augmented'] = True

    if exec_desc['is_signed'] is None:
        exec_desc.update(aug_desc['sig'])

    return (k, exec_desc)

def report_non_augmented(k_desc):
    desc = k_desc[1]
    if abs(desc['depth']) > 1 and not desc['augmented']:
        logging.error("augmentation fail: %r/%r/%r/%r, %r", desc['depth'], desc['host'], desc['pid'], desc['instance'], desc['file'])

    return k_desc

def filter_by_timing(desc, timing_limit):
    # filter children with timestamps close to their parents creation
    if desc['depth'] < 0 or desc.get('timestamp') is None or desc.get('ancestor_timestamp') is None:
        return True

    # filter weird timestamps as well
    if desc['timestamp'] >= desc['ancestor_timestamp'] and (desc['timestamp'] - desc['ancestor_timestamp']).total_seconds() <= timing_limit:
        return True

    logging.error("timing filter [%d seconds]: %s", timing_limit, descriptor_id_str(desc))
    return False

def build_child_ancestry_entry(desc):
    depth = next_level(desc['depth'])
    return (depth, desc['host'], desc['child']['pid'], desc['child']['instance'], desc['child']['file'])

def set_parentage(k_desc):
    desc, aug_entry = k_desc[1]

    if aug_entry is None:
        logging.error("no parentage found for %s/%s/%d/%s", desc['host'], desc['pid'], desc['instance'], desc['file'])

    elif not aug_entry.get('augmentation', False):
        logging.error("got a non-augmentation pair in set_parentage: %r, %r, %r", k_desc[0], desc, aug_entry)

    else:
        desc['parentage'] = aug_entry['ancestry']

    return desc

def validate_parentage(desc):
    if desc['depth'] < 0 and desc['parentage'] is None or desc['ancestry'] is None:
        logging.warning("bad augmentation (null parentage) %d/%s/%s/%d %r", desc['depth'], desc['host'], desc['pid'], desc['instance'], desc)
        return False

    # drop anything that has an ambiguity (supposed to mean identifiers collision)
    is_valid = desc['depth'] > 0 or desc['parentage'][:abs(desc['depth'])+1] == desc['ancestry'] + [build_child_ancestry_entry(desc)]

    if not is_valid:
        logging.warning("dropping a descriptor %d/%s/%s/%d %r", desc['depth'], desc['host'], desc['pid'], desc['instance'], desc)

    return is_valid

def build_exec_aggregate_descriptor(exec_desc, tagger, alt_tagger, correlate):
    tag = tagger(exec_desc)

    process_descriptor = {
        # aggregate children twice to avoid possible long caculations as memory is cheap

        # aggregate children for mean calculation
        'children' : {
            exec_desc['child']['file'] : 1
        },

        # calculate children for variance calculation
        'children_distribution' : {
            exec_desc['host'] : {
                exec_desc['instance'] : {
                    'total' : 1,
                    'weights' : {
                        exec_desc['child']['file'] : 1,
                    },
                },
            },
        },

        'host' : exec_desc['host'],
        'pid' : exec_desc['pid'],

        # these are used for debugging and lineage count generation
        'instance' : exec_desc['instance'],
        'parentage' : exec_desc['parentage'],
        'ancestry' : exec_desc['ancestry'],
        'child_instance' : exec_desc['child']['instance'],
        'child_file' : exec_desc['child']['file'],
        'child_pid' : exec_desc['child']['pid'],

        'signature' : set(),
        'children_signature' : {
            exec_desc['child']['file'] : set(),
        },

        'parentage_distribution' : {
        },

        'full_path' : exec_desc['file'],
        'path' : exec_desc['file'][-1],
        'depth' : exec_desc['depth'],

        'tag' : tag,
        'alternative_tag' : '' if alt_tagger is None else alt_tagger(exec_desc),
    }

    if exec_desc['is_signed'] is not None:
        process_descriptor['signature'].add(
            (
                exec_desc['is_signed'],
                exec_desc['is_sig_valid'],
                exec_desc['sig_company'],
                exec_desc['sig_product'],
            )
        )

    if exec_desc['child']['is_signed'] is not None:
        process_descriptor['children_signature'][exec_desc['child']['file']].add(
            (
                exec_desc['child']['is_signed'],
                exec_desc['child']['is_sig_valid'],
                exec_desc['child']['sig_company'],
                exec_desc['child']['sig_product'],
            )
        )

    if correlate and exec_desc['depth'] > 0 and exec_desc['parentage'] is not None:
        process_descriptor['parentage_distribution'] = {
            tuple((entry[-1] for entry in exec_desc['parentage'])) : {
                exec_desc['child']['file'] : {
                    tuple(exec_desc['parentage']) : 1,
                },
            },
        }

    # use "full transformed path" as a way to identify the descriptors
    return ((tag, exec_desc['file']), process_descriptor)

def build_lineage_count_entry(k_desc):
    tag_file, desc = k_desc
    if desc['depth'] < 0:
        return []

    id_key = (desc['tag'], instance_identification_key(desc, False))
    lineage = (desc['full_path'][1:], tuple((parent[-1] for parent in desc['parentage']))) # drop the heading +1/-1 in full path and the instance info in the parentage

    return [(id_key, lineage)]

def expand_lineage_count_entry(tagged_lineage_count):
    tagged_lineage, count = tagged_lineage_count
    tag, _file, parentage = tagged_lineage

    return [((tag, _file, parentage[:i]), count) for i in range(1, len(parentage)+1)]

def aggregate_children(descriptor1, descriptor2):
    children1 = descriptor1['children']
    children2 = descriptor2['children']

    for child in children2:
        children1[child] = children1.get(child, 0) + children2[child]

    child_dist1 = descriptor1['children_distribution']
    child_dist2 = descriptor2['children_distribution']

    for host in child_dist2:
        child_dist1.setdefault(host, {})

        host_dist1 = child_dist1[host]
        host_dist2 = child_dist2[host]

        for instance_id in host_dist2:
            host_dist1.setdefault(instance_id, {'total' : 0, 'weights' : {}})

            instance_dist1 = host_dist1[instance_id]
            instance_dist2 = host_dist2[instance_id]

            instance_dist1['total'] += instance_dist2['total']

            weights1 = instance_dist1['weights']
            weights2 = instance_dist2['weights']
            for child_path in weights2:
                weights1[child_path] = weights1.get(child_path, 0) + weights2[child_path]

    parentage_dist1 = descriptor1.get('parentage_distribution')
    parentage_dist2 = descriptor2.get('parentage_distribution')

    for parentage in parentage_dist2:
        parentage_children2 = parentage_dist2[parentage]

        if parentage in parentage_dist1:
            parentage_children1 = parentage_dist1[parentage]
            for child in parentage_children2:
                if child in parentage_children1:
                    for specific_child_parentage, specific_instance_count in parentage_children2[child].items():
                        parentage_children1[child].setdefault(specific_child_parentage, 0)
                        parentage_children1[child][specific_child_parentage] += specific_instance_count
                else:
                    parentage_children1[child] = parentage_children2[child]
        else:
            parentage_dist1[parentage] = parentage_children2

    descriptor1['signature'].update(descriptor2['signature'])

    children_sig2 = descriptor2['children_signature']
    children_sig1 = descriptor1['children_signature']
    for child in children_sig2:
        children_sig1.setdefault(child, set())
        children_sig1[child].update(children_sig2[child])

    return descriptor1

def build_parentage_distributions_rdd_entries(k_desc):
    _key, desc = k_desc

    parentage_distribution = desc['parentage_distribution']
    if len(parentage_distribution) == 0:
        return []

    tag = desc['tag']
    full_path = desc['full_path'][1:] # drop the preceeding 1/-1

    distribution_elements = []
    for parentage in parentage_distribution:
        tagged_full_parentage = (tag, full_path, parentage) # full_path[-1] and parentage[0] overlap

        parentage_desc = parentage_distribution[parentage]
        parentage_metrics_record = {}
        for child in parentage_desc:
            child_desc = parentage_desc[child]

            aggregate_sum = 0
            aggregate_square_sum = 0
            for _, instance_count in child_desc.items():
                aggregate_sum += instance_count
                aggregate_square_sum += instance_count ** 2

            parentage_metrics_record[child] = {
                'aggregate_sum' : aggregate_sum, # to calculate the mean
                'aggregate_square_sum' : aggregate_square_sum,
            }

        distribution_elements.append((tagged_full_parentage, parentage_metrics_record))

    return distribution_elements

def expand_parentage_distribution_entries(k_parentage_dist_entry):
    tagged_full_parentage, parentage_dist_entry = k_parentage_dist_entry
    tag = tagged_full_parentage[0]
    _file = tagged_full_parentage[1]
    full_parentage = tagged_full_parentage[2]
    return [((tag, _file, full_parentage[:i]), copy.deepcopy(parentage_dist_entry)) for i in range(1, len(full_parentage)+1)]

def aggregate_parentage_dist_metrics(parentage_dist1, parentage_dist2):
    for child in parentage_dist2:
        child2 = parentage_dist2[child]

        if child in parentage_dist1:
            child1 = parentage_dist1[child]

            child1['aggregate_sum'] += child2['aggregate_sum']
            child1['aggregate_square_sum'] += child2['aggregate_square_sum']
        else:
            parentage_dist1[child] = child2

    return parentage_dist1

def apply_lineage_count_to_parentage_dist_metrics(k_desc_count):
    key, metrics_desc, lineage_instance_count = k_desc_count[0], k_desc_count[1][0], k_desc_count[1][1]

    new_metrics = {}
    for child in metrics_desc:
        child_metrics = metrics_desc[child]
        average = child_metrics['aggregate_sum']/lineage_instance_count
        variance = child_metrics['aggregate_square_sum']/lineage_instance_count - (average**2)

        if variance < 0:
            logging.error("negative variance [=%r] for key %r and child %r", variance, key, child)

        new_metrics[child] = {
                'average' : average,
                'variance' : variance,
                'std' : numpy.sqrt(variance) if variance >= 0 else -1,
                'lineage_count' : lineage_instance_count,
        }

    return {
        'tag' : key[0],
        'file' : key[1],
        'parentage' : key[2],
        'metrics' : new_metrics,
    }

def generate_correlation(metrics_desc, correlation_average_numerical_error, correlation_std_numerical_error, correlation_min_occurences):
    _file, parentage, tag, metrics = metrics_desc['file'], metrics_desc['parentage'], metrics_desc['tag'], metrics_desc['metrics']

    correlations = []

    # check if average is at most correlation_average_numerical_error away from a round integer
    # and the std is at most correlation_std_numerical_error
    for child in metrics:
        child_desc = metrics[child]
        if child_desc['std'] < 0 or child_desc['average'] < 0:
            continue

        if ((child_desc['average'] + correlation_average_numerical_error) % 1 <= 2*correlation_average_numerical_error) and \
            child_desc['std'] <= correlation_std_numerical_error and \
            child_desc['lineage_count'] >= correlation_min_occurences:
            correlations.append(((tag, child), [
                dict(
                    parentage=parentage,
                    file=_file,
                    average=child_desc['average'],
                    std=child_desc['std'],
                    lineage_count=child_desc['lineage_count'],
                )
            ]))

    return correlations

def gather_lineages(lineages1, lineages2):
    lineages1_lineages = [(lineage['file'], lineage['parentage']) for lineage in lineages1]
    for lineage_rec in lineages2:
        _file, parentage = lineage_rec['file'], lineage_rec['parentage']
        found = False
        for i in range(len(parentage)):
            if (_file, parentage[:i+1]) in lineages1_lineages:
                found = True
                break

        if not found and (_file, parentage) not in lineages1:
            lineages1.append(lineage_rec)

    return lineages1

def prettify_correlation_record(tag_child_lineages, correlation_average_numerical_error, correlation_std_numerical_error):
    tag, child, lineages = tag_child_lineages[0][0], tag_child_lineages[0][1], tag_child_lineages[1]

    return {
        'child' : child,
        'tag' : tag,
        'average_allowed_err' : correlation_average_numerical_error,
        'std_allowed_err' : correlation_std_numerical_error,
        'lineages' : lineages,
    }

def drop_excessive_children(k_desc, max_child_prevalence):
    tag_file, desc = k_desc
    desc = copy.deepcopy(desc) # we're going to modify the descriptor so we need a copy

    high_prevalence_children = [child_name for child_name, prevelance in desc['children'].items() if prevelance > max_child_prevalence]

    all_children = desc['children']
    children_distribution = desc['children_distribution']

    for child in high_prevalence_children:
        all_children.pop(child)

    for host, host_desc in children_distribution.items():
        for instance_id, instance_desc in host_desc.items():
            children_of_interest = {k: v for k, v in instance_desc['weights'].items() if k in  high_prevalence_children}
            instance_desc['total'] -= sum(children_of_interest.values())
            for child in children_of_interest:
                instance_desc['weights'].pop(child)

    return (tag_file, desc)

def filter_low_prevelance_nodes(descriptor, prevelance_bar):
    key, descriptor = descriptor

    children = descriptor['children']
    total_children = sum(children.values())
    low_prevelance_children = [child for child in children if children[child] < total_children * prevelance_bar]
    for child in low_prevelance_children:
        children.pop(child)

    children_distribution = descriptor['children_distribution']

    logging.error("filtering low-prevelance children in %s: %s - expect hanging nodes in tree formatting", "/".join(descriptor['full_path'][1:]), ", ".join(low_prevelance_children))

    for host in children_distribution:
        host_desc = children_distribution[host]

        for instance_id in host_desc:
            instance = host_desc[instance_id]
            weights = instance['weights']

            for child in low_prevelance_children:
                instance['total'] -= weights.pop(child, 0)

    return (key, descriptor)

def calculate_variance_and_mean(descriptor):
    descriptor = descriptor[1] # drop the key...

    children = descriptor['children']
    children_distribution = descriptor['children_distribution']

    total_avg_count = float64(sum((children[child] for child in children), 0))
    statistics = {
        child : {
            'count' : children[child],
            'aggregate_percent' : children[child]/total_avg_count,
            'mean' : 0,
            'variance' : 0,
        }

        for child in children
    }

    instances_count = float64(sum((len(children_distribution[host]) for host in children_distribution)))
    for host in children_distribution:
        host_desc = children_distribution[host]
        for instance_id in host_desc:
            instance = host_desc[instance_id]
            total = float64(instance['total'])
            weights = instance['weights']

            for child in children:
                if child in weights:
                    weights[child] /= total
                    statistics[child]['mean'] += weights[child]/instances_count

    for host in children_distribution:
        host_desc = children_distribution[host]
        for instance_id in host_desc:
            weights = host_desc[instance_id]['weights']

            for child in children:
                offset = weights.get(child, 0) - statistics[child]['mean']
                statistics[child]['variance'] += offset*offset/instances_count

    defloatify = lambda d: {key : float(value) if isinstance(value, float64) else value for key, value in d.items()}
    descriptor['statistics'] = {child : defloatify(statistics[child]) for child in statistics}

    descriptor.pop('children', None)
    descriptor.pop('children_distribution', None)

    return descriptor

def descriptor_to_edges_rows(descriptor, comments, no_parent_marker, no_child_marker):
    # the descriptor is a tuple of a key (file path) and the actual descriptor
    statistics = descriptor['statistics']

    marker = no_parent_marker if descriptor['depth'] < 0 else no_child_marker
    _marker = "<{}>".format(marker)
    apply_marker = lambda name: _marker if name.endswith(marker) else name

    comments = {
        comment_name : comments[comment_name](descriptor) for comment_name in comments
    }

    return [graph_formatting.EdgeDescriptor(
                descriptor['depth'],
                descriptor['full_path'],
                descriptor['path'],
                to_unicode(apply_marker(child)),
                descriptor['tag'],
                descriptor['alternative_tag'],
                graph_formatting.WeightDescriptor(
                    stats['count'],
                    stats['aggregate_percent'],
                    stats['mean'],
                    stats['variance']
                ),
                {comment_name: comment[0] for comment_name, comment in comments.items()},
                {comment_name: comment[1][child] for comment_name, comment in comments.items() if child in comment[1]}
            )
            for child, stats in statistics.items()]

def edge_to_desc_str(edge):
    return json.dumps(graph_formatting.edge_to_dict(edge))

def build_weighted_execution_descriptor(sql_context,
                                        execution_dump_csv,
                                        max_child_depth,
                                        max_parent_depth,
                                        path_transformation,
                                        id_func,
                                        node_tagging_function,
                                        alt_tagging_func,
                                        outlier_filter_percentage,
                                        minimum_tag_occurences,
                                        max_child_prevalence,
                                        add_no_children_nodes,
                                        timing_limit,
                                        after,
                                        before,
                                        correlate,
                                        correlation_average_numerical_error,
                                        correlation_std_numerical_error,
                                        correlation_min_occurences,
                                        tracing_mode,
                                        preliminary_filters,
                                        comments,
                                        partition_size):
    logging.info("building execution lineage records from %s", execution_dump_csv)

    csv_path = pathlib.Path(os.path.abspath(execution_dump_csv)).as_uri()
    dataset = sql_context.read.csv(csv_path, header=True, inferSchema=False, sep='\t', mode='DROPMALFORMED', escape='|').rdd

    if partition_size is not None:
        num_partitions = max(int((os.stat(execution_dump_csv).st_size + partition_size - 1)/partition_size), 4*sql_context.sparkSession.sparkContext.defaultParallelism)
        logging.info("using %d partitions as rough estimate", num_partitions)
        dataset = dataset.repartition(num_partitions)

    if tracing_mode:
        logging.info("tracing dataset")
        dataset = rdd_tracing.trace_rdd(dataset)

    if dataset.isEmpty():
        logging.info("the dataset is empty - nothing to do")
        raise ValueError(f"the dataset is empty in {csv_path}")

    if max_child_depth is not None or max_parent_depth is not None:
        logging.info("filtering children below level %r and above parents level %r", max_child_depth, max_parent_depth)
        dataset = dataset.filter(lambda row: preliminary_depth_filter(row, max_child_depth, max_parent_depth))

    dataset = dataset.map(to_dict_augment_row)

    if after is not None or before is not None:
        logging.info("adding an time filter %r-%r filter", after, before)
        after = parser.parse("1-1-1") if after is None else after
        before = parser.parse("9999-1-1") if before is None else before
        dataset = dataset.setName("time_filter").filter(lambda row: row['timestamp'] is None or (after <= parser.parse(row['timestamp']) <= before))

    dataset = dataset.map(lambda row: build_exec_descriptor(**row, path_transformation=path_transformation, pre_flipped=False, depth_canonicalized=False))

    if preliminary_filters is not None:
        logging.info("adding preliminary filters: %r", tuple((getattr(_f, '__name__', repr(_f)) for _f in  preliminary_filters)))
        dataset = dataset.filter(lambda exec_desc: all((_filter(exec_desc) for _filter in preliminary_filters)))

    lowest_child = max(1, dataset.max(key=lambda desc: desc['depth'])['depth'])
    highest_parent = min(-1, dataset.min(key=lambda desc: desc['depth'])['depth'])

    max_iter = max(abs(lowest_child), abs(highest_parent))

    logging.info("max child: %d, max parent: %d", lowest_child, highest_parent)

    logging.info("augmenting signature info")
    roots_signature_rdd = dataset.\
        filter(lambda desc: desc['depth'] == -1).\
        flatMap(build_root_sig_augmentation_entry).\
        reduceByKey(lambda sig_info1, sig_info2: sig_info1) # arbitrarily pick one if there's conflicting data

    dataset = dataset.\
        map(lambda desc: (instance_identification_key(desc, False), desc)).\
        leftOuterJoin(roots_signature_rdd).\
        map(augment_root_signatures)

    if add_no_children_nodes:
        logging.info("adding missing parent/child descriptors")
        dataset = dataset.\
            flatMap(lambda desc: build_no_children_resolution_descriptors(desc,
                                                                          NoParentMarker,
                                                                          NoChildMarker,
                                                                          highest_parent,
                                                                          lowest_child)).\
            reduceByKey(drop_unnecessary_no_children_descriptors).\
            flatMap(lambda k_desc: k_desc[1])

    logging.info("lowest child = %d, highest parent = %d, max iterations = %d", lowest_child, highest_parent, max_iter)

    dataset = dataset.setName("id_exec_desc").map(lambda desc: (instance_identification_key(desc, False), desc))

    for level in range(1, max_iter):
        logging.info("name resolution iteration %r", level)
        gen_names_dataset = dataset.flatMap(lambda k_desc: publish_generation_name(k_desc, level))
        dataset = dataset.\
            leftOuterJoin(gen_names_dataset).\
            map(update_augmentation_entry).\
            filter(lambda k_desc: not k_desc[1].get('is_augmentation_entry', False))

    gen_names_dataset = None # discard gen_names_dataset

    logging.info("done updating names to proper ones")

    logging.info("reporting augmentation failures")
    dataset = dataset.map(report_non_augmented)
    logging.info("augmentation failures reported")

    if timing_limit is not None:
        logging.info("filtering by timing (removing children created within %d seconds of parent creation)", timing_limit)
        dataset = dataset.filter(lambda k_desc: filter_by_timing(k_desc[1], timing_limit))

    logging.info("building parentage chains dataset")
    dataset = dataset.map(lambda k_desc: (
            tuple(k_desc[1]['ancestry'][0][1:]),
            k_desc[1]
            )
        )

    # when creating the key, make sure to skip the level so we can have the lineage accessible for children as well
    # without too much of a hassle
    parentage_chains_dataset = dataset.\
        filter(lambda k_desc: k_desc[1]['depth'] < 0).\
        map(lambda k_desc: (k_desc[0],
                    {
                        'ancestry' : k_desc[1]['ancestry'] + [build_child_ancestry_entry(k_desc[1])],
                        'augmentation' : True,
                    })
        ).\
        reduceByKey(lambda desc1, desc2: desc1 if len(desc1['ancestry']) > len(desc2['ancestry']) else desc2)

    logging.info("built chains list, applying")
    dataset = dataset.\
        leftOuterJoin(parentage_chains_dataset).\
        map(set_parentage).\
        filter(validate_parentage)

    logging.info("chains list applied")

    dataset = dataset.\
        map(lambda desc: build_exec_aggregate_descriptor(desc, node_tagging_function, alt_tagging_func, correlate))

    if id_func is not None:
        logging.info("filtering by uniqueness criteria")
        dataset = dataset.\
            setName("uniqueness_criteria").map(lambda k_desc: ((id_func(k_desc[1]), k_desc[1]['tag']), k_desc)).\
            setName("drop_by_uniq_criteria").reduceByKey(lambda desc1, desc2: desc1).\
            map(lambda id_k_desc: id_k_desc[1])

    if minimum_tag_occurences > 0:
        logging.info("filtering tags with less than %d occurences", minimum_tag_occurences)

        logging.info("counting occurences")
        # extract the tags and count the times they appear for the roots
        tag_occurences_dataset = dataset.\
            filter(lambda k_desc: k_desc[1]['depth'] == -1).\
            map(lambda k_desc: (to_hashable(k_desc[0][0]), 1)).\
            reduceByKey(operator.add)

        logging.info("applying count filter")
        # filter out tags that don't pass the minimum
        dataset = dataset.\
            map(lambda k_desc: (to_hashable(k_desc[0][0]), k_desc)).\
            leftOuterJoin(tag_occurences_dataset).\
            filter(lambda tag_kdesc_count: tag_kdesc_count[1][1] is not None and tag_kdesc_count[1][1] >= minimum_tag_occurences).\
            map(lambda tag_kdesc_count: tag_kdesc_count[1][0])

    if correlate:
        logging.info("building lineage instance count RDD")
        lineage_instance_count_rdd = dataset.\
            flatMap(build_lineage_count_entry).\
            reduceByKey(lambda parentage1, parentage2: parentage1).\
            setName("format_lineage_counter").map(lambda id_and_lineage: ((id_and_lineage[0][0],) + id_and_lineage[1], 1)).\
            reduceByKey(operator.add).\
            flatMap(expand_lineage_count_entry).\
            reduceByKey(operator.add)

        if tracing_mode:
            lineage_instance_count_rdd = lineage_instance_count_rdd.\
                setName("lineage_count_summary").map(lambda x: x)

    dataset = dataset.reduceByKey(aggregate_children)

    correlations_rdd = None
    if correlate:
        logging.info("correlating children with parentage")
        correlations_rdd = dataset.\
            flatMap(build_parentage_distributions_rdd_entries).\
            flatMap(expand_parentage_distribution_entries).\
            reduceByKey(aggregate_parentage_dist_metrics).\
            leftOuterJoin(lineage_instance_count_rdd).\
            setName("filter_countless").filter(lambda k_joined_entry: k_joined_entry[1][1] is not None).\
            map(apply_lineage_count_to_parentage_dist_metrics).\
            flatMap(lambda metrics_desc: generate_correlation(metrics_desc, correlation_average_numerical_error, correlation_std_numerical_error, correlation_min_occurences)).\
            reduceByKey(gather_lineages).\
            map(lambda tag_child_lineages: prettify_correlation_record(tag_child_lineages, correlation_average_numerical_error, correlation_std_numerical_error))

    if max_child_prevalence is not None:
        dataset = dataset.map(lambda k_desc: drop_excessive_children(k_desc, max_child_prevalence))

    if 0 < outlier_filter_percentage:
        logging.info("filtering low prevelance nodes - %f%%", outlier_filter_percentage * 100)
        dataset = dataset.map(lambda desc: filter_low_prevelance_nodes(desc, outlier_filter_percentage))

    return (dataset.map(calculate_variance_and_mean).\
        flatMap(lambda desc: descriptor_to_edges_rows(desc, comments, NoParentMarker, NoChildMarker)).\
        sortBy(lambda edge: abs(edge.depth), ascending=True).\
        map(edge_to_desc_str).\
        coalesce(1),

        correlations_rdd.map(lambda correlation_record: json.dumps(correlation_record)).coalesce(1) if correlations_rdd is not None else None
        )

@contextlib.contextmanager
def stdout_context():
    yield sys.stdout

def main():
    path_transformation_help = 'a file path transformation.\n' + path_transformations.transformations.doc()
    node_tagging_help = "the namespace modifier to perform tree aggregation under (mean/std calculations).\n" + node_tagging.taggers.doc()
    preliminary_row_filters_help = "add preliminary elimination filters.\n" + preliminary_row_filters.filters.doc()
    edge_comments_help = "the comments to annotate the edges of the graph.\n" + edge_comments.comments.doc()
    edge_id_func_doc = "use a uniqueness criteria to drop psuedo-duplicate edges.\n" + edge_id_funcs.id_functions.doc()

    parser = utilities.argparse_automation_wrappaer.ArgumentParser(description="build a lineage graph from the data dumped by dump-execution-lineage.py")
    parser.add_argument("-o", "--output", output=True, default="-", help="the output file to write the graph edges to. '-' denotes standard output.")
    parser.add_argument("-v", "--verbose", action="store_true", help="show debug logs.")
    parser.add_argument("--spark-verbosity", default=None, help="the log level to set spark to.")
    parser.add_argument("-p", "--path-transformation", default='filename', choices=path_transformations.transformations.choicesList(), help=path_transformation_help)
    parser.add_argument("-u", "--uniqueness-criteria", default=None, choices=edge_id_funcs.id_functions.choicesList(), help=edge_id_func_doc)
    parser.add_argument("-w", "--with-outliers", action="store_true", help="don't filter very low prevelance children in the output")
    parser.add_argument("-c", "--correlate", action="store_true", help="perform children prevelance to parentage correlation filtering.")
    parser.add_argument("--correlation-average-allowed-error", type=float, default=0.001, help="the permitted numerical error when calculating the average instance count for correlation detection.")
    parser.add_argument("--correlation-std-allowed-error", type=float, default=0.001, help="the permitted numerical error when calculating the standard deviation instance count for correlation detection.")
    parser.add_argument("--correlation-min-occurences", type=int, default=3, help="the minimal number of times the lineage is required to occur for the correlation to be valid.")
    parser.add_argument("--outliers-bar", type=float, default=1, help=''.join(
        ("the outlier child prevelance bar.\n",
         "all children of a process with a weight lower than the specified percent taken from the number of parent executions are removed.",
         "if the parent has no parents itself (and therefor it's number of instances can't be calculated), the total weight",
         "of it's children is taken instead.\n",
         "e.g. if A was run 100 times (the total sum of the weights of the edges it's a child in) and it has a child B with weight 4",
         "a child C with weight 3 and a child D with a weight 16, then the edges A->B and A->C are removed if, for example, the outliers bar is set to 5%%.\n",
         "if A has no parent, than it's executions count will be replaced by 23 (=16+3+4)."
        )))
    parser.add_argument("-t", "--node-tagging", choices=node_tagging.taggers.choicesList(), default="no-tagging", help=node_tagging_help)
    parser.add_argument("-a", "--alternative-tag", choices=node_tagging.taggers.choicesList(), default=None, help=" ".join((
        "the alternative tag to attach to the instance.",
        "the alternative tag is used to compare the output tree to an average tree yielded by other tagging types.",
        "if not specified there will be no alternative tag specified.",
        "if there is an alternative tag collision then one is arbitrarily chosen.",
    )))
    parser.add_argument("-g", "--aggregation-bar", type=int, default=0, help=' '.join((
        "a minimum number of times a tag should occure in order to not be discarded.",
        "used to limit the generation of average trees with too little instances for the average tree to have significance.",
        )
    ))
    parser.add_argument("--max-child-prevalence", type=int, default=None, help="children that occure more times than this amount are discarded (they are 'not interesting').")
    parser.add_argument("-f", "--preliminary-filter", default=[], action="append", choices=preliminary_row_filters.filters.choicesList(), help=preliminary_row_filters_help)
    parser.add_argument("--comment", default=[], action="append", choices=edge_comments.comments.choicesList(), help=edge_comments_help)
    parser.add_argument("--before", type=dateutil.parser.parse, default=None, help="limit the execution to entries before a given date.")
    parser.add_argument("--after", type=dateutil.parser.parse, default=None, help="limit the execution to entries after a given date.")
    parser.add_argument("--max-child-depth", type=int, default=None, help="the deepest child level to look at (positive integer).")
    parser.add_argument("--max-parent-depth", type=int, default=None, help="the deepest parent level to look at (positive integer).")
    parser.add_argument("--ignore-no-children", action="store_true", help="don't add a virtual 'no children'/'no parent' node to accomodate childless nodes.")
    parser.add_argument("--tracing", action="store_true", help="load an external tracing module (used for debugging).")
    parser.add_argument("--keep-temp", action="store_true", help="keep temp products (output directory) if an error occurs.")
    parser.add_argument("--partition-size", default="1GB", help="the size to limit Spark partitions.")
    parser.add_argument("--no-repartitioning", action="store_true", help="use default partitioning scheme.")
    parser.add_argument("-l", "--timing-limit", type=int, default=None, help="filter only children created at most this many seconds after the parent was created.")
    parser.add_argument("lineage_dump", metavar="<lineage dump>", file=True, help="a dump file generated by dump-execution-lineage.py")
    args = parser.parse_args()

    logging.basicConfig(format="%(levelname)s: %(asctime)s %(message)s", level=logging.INFO if args.verbose else logging.WARN)
    logging.info("executed using command line: %s", " ".join(sys.argv))

    # because spark is a bitch, there's a small hack here where we added to our git project a broken link to an ignored file
    # and we'll try and import it instead of loading it from source because spark has problems with it after serialization
    # (it can't find the module when unpickling)
    if args.tracing:
        logging.info("importing debug tracing module")
        import debug_tracing

    transformation = path_transformations.transformations.option(args.path_transformation)
    demo_path = WindowsSep.join(('a','b','c','d'))
    logging.info("using path transformation %s -> %s", demo_path, transformation(demo_path))

    tagger = node_tagging.taggers.option(args.node_tagging)
    alt_tagger = None if args.alternative_tag is None else node_tagging.taggers.option(args.alternative_tag)
    logging.info("using '%s' tagging with '%s' alternative tagging", tagger.__name__, "None" if alt_tagger is None else alt_tagger.__name__)

    preliminary_filters = None
    if len(args.preliminary_filter) > 0:
        preliminary_filters = [preliminary_row_filters.filters.option(_filter) for _filter in args.preliminary_filter]

    comments = {comment_gen_name : edge_comments.comments.option(comment_gen_name) for comment_gen_name in args.comment}

    partition_size = None if args.no_repartitioning else humanfriendly.parse_size(args.partition_size)

    id_func = None if args.uniqueness_criteria is None else edge_id_funcs.id_functions.option(args.uniqueness_criteria)

    logging.info("building execution records with pyspark")
    with beep.play_random_sound_on_completion():
        with pyspark_context(args.spark_verbosity) as sql_context:
            weighted_exec_desc_rdd, correlations_rdd = \
                        build_weighted_execution_descriptor(sql_context,
                                                            args.lineage_dump,
                                                            args.max_child_depth,
                                                            args.max_parent_depth,
                                                            transformation,
                                                            id_func,
                                                            tagger,
                                                            alt_tagger,
                                                            0 if args.with_outliers else args.outliers_bar/100.0,
                                                            args.aggregation_bar,
                                                            args.max_child_prevalence,
                                                            not args.ignore_no_children,
                                                            args.timing_limit,
                                                            args.after,
                                                            args.before,
                                                            args.correlate,
                                                            args.correlation_average_allowed_error,
                                                            args.correlation_std_allowed_error,
                                                            args.correlation_min_occurences,
                                                            args.tracing,
                                                            preliminary_filters,
                                                            comments,
                                                            partition_size)

            logging.info("dumping edges to %s", args.output)
            if '-' == args.output:
                weighted_exec_desc_rdd.toDF(schema=StringType()).show()
                if correlations is not None:
                    correlations.toDF(schema=StringType()).show()
            else:
                save_rdd_to_text_file(os.path.abspath(args.output), weighted_exec_desc_rdd, args.keep_temp)
                if correlations_rdd is not None:
                    save_rdd_to_text_file(args.output + ".correlations", correlations_rdd, args.keep_temp)

    logging.info("done")

if __name__ == "__main__":
    main()

