#!/usr/bin/env python3

import logging
import utilities.option_registry_util

taggers = utilities.option_registry_util.OptionRegistry()

@taggers.register("no-tagging")
def no_tagging(exec_descriptor):
    """
    group all the nodes together (regular, non partitioned tree output)
    """
    return '1'

@taggers.register("root")
def root_node(exec_descriptor):
    """
    tag a node by it's ancestor
    """
    return exec_descriptor['ancestry'][0][1:] # skip the depth

@taggers.register("parentage")
def parentage(exec_descriptor):
    """
    tag based on the parentage execution chain (file names, not instances)
    """
    return tuple((ancestor[-1] for ancestor in exec_descriptor['parentage'])) # the list of files from the root to the parent

@taggers.register("parentage-host")
def parentage_host(exec_descriptor):
    """
    tag based on the parentage execution chain (file names, not instances) in a specific host
    """
    return (exec_descriptor['host'],) + parentage(exec_descriptor) # the list of files from the root to the parent

@taggers.register("root-group")
def root_group(exec_descriptor):
    """
    tag by the file name of the root node.
    """
    return exec_descriptor['ancestry'][0][-1]

@taggers.register("root-group-host")
def root_group(exec_descriptor):
    """
    tag by the file name of the root node.
    """
    return (exec_descriptor['host'], exec_descriptor['ancestry'][0][-1])

@taggers.register("host")
def host_group(exec_descriptor):
    """
    Tag by host alone.
    """
    return exec_descriptor['host']

