#!/usr/bin/env python3

import re
import utilities.option_registry_util

filters = utilities.option_registry_util.OptionRegistry()

GUID_RE = re.compile(r"^[{(]?[0-9A-F]{8}-([0-9A-F]{4}-){3}[0-9A-F]{12}[)}]?(\.exe)?$", re.I)

@filters.register("guid-filename")
def guid_filename_filter(exec_desc):
    """
    Filter processes with a GUID name.
    """
    return exec_desc['depth'] < 0 or len(GUID_RE.findall(exec_desc['child']['file'])) == 0

@filters.register("microsoft-signed")
def microsoft_signed(exec_desc):
    """
    Filter processes that are signed by microsoft.
    """
    if exec_desc['child']['is_signed'] is None or exec_desc['depth'] < 0:
        return True

    return not (exec_desc['child']['is_signed'] and exec_desc['child']['is_sig_valid'] and 'microsoft corporation' == exec_desc['child']['sig_company'].lower())

