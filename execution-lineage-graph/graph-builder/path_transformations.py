#!/usr/bin/env python3

import utilities.option_registry_util

transformations = utilities.option_registry_util.OptionRegistry()

WindowsSep = '\\'

@transformations.register("filename")
def filename(path):
    """
    drop full path and use only file name
    """
    return path.rsplit(WindowsSep, 1)[-1]

@transformations.register("raw")
def raw(path):
    """
    use the full file path
    """
    return path

