#!/usr/bin/env python3

import logging
import utilities.option_registry_util

comments = utilities.option_registry_util.OptionRegistry()

@comments.register("signer-info")
def signed_info_comment(exec_desc):
    """
    Add a comment containing the signer information to the output edges.
    """
    def _prettify_sig(sig):
        return (
            'valid' if sig[1] else 'invalid',
            sig[2],
            sig[3],
        ) if sig[0] else ('unsigned',)

    comment = [
        _prettify_sig(sig) for sig in exec_desc.get('signature', []) if sig[0] is not None
    ]

    comment = comment if len(comment) > 0 else None

    children_comments = {
        child : [_prettify_sig(sig) for sig in sig_list if sig[0] is not None]
        for child, sig_list in exec_desc.get('children_signature', {}).items() if len(sig_list) > 0
    }

    children_to_pop = [child for child, sigs in children_comments.items() if len(sigs) == 0]
    for child in children_to_pop:
        children_comments.pop(child)

    return comment, children_comments
