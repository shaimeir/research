#!/usr/bin/env python3

import six
import logging
import io
import termcolor
import numpy
import json
import anytree
from collections import defaultdict, namedtuple, Iterable

EdgeDescriptor = namedtuple('EdgeDescriptor', ['depth', 'full_path', 'path', 'child', 'tag', 'alternative_tag', 'weight', 'comment', 'child_comment'])
WeightDescriptor = namedtuple('WeightDescriptor', ('weight', 'aggregate_percent', 'mean', 'variance'))

def weight_to_dict(weight):
    return {
        'weight' : weight.weight,
        'aggregate_percent' : weight.aggregate_percent,
        'mean' : weight.mean,
        'variance' : weight.variance,
    }

def edge_to_dict(edge):
    return {
        'depth' : edge.depth,
        'full_path' : list(edge.full_path) if isinstance(edge.full_path, Iterable) and not isinstance(edge.full_path, str) else edge.full_path,
        'path' : edge.path,
        'child' : edge.child,
        'tag' : edge.tag,
        'alternative_tag' : edge.alternative_tag,
        'weight' : weight_to_dict(edge.weight),
        'comment' : edge.comment,
        'child_comment' : edge.child_comment,
    }

def edge_from_dict(edge_dict):
    return EdgeDescriptor(
        edge_dict['depth'],
        edge_dict['full_path'],
        edge_dict['path'],
        edge_dict['child'],
        edge_dict.get('tag','1'),
        edge_dict.get('alternative_tag',''),
        WeightDescriptor(
            edge_dict['weight']['weight'],
            edge_dict['weight']['aggregate_percent'],
            edge_dict['weight']['mean'],
            edge_dict['weight']['variance']
        ),
        edge_dict.get('comment', {}),
        edge_dict.get('child_comment', {})
    )

class GraphBuilder(object):
    def addEdge(self, depth, src, dst, weight, comment):
        raise NotImplementedError()

class GraphFormatter(object):
    def format(self, graph, stream, **options):
        raise NotImplementedError()

class DictGraphBuilder(GraphBuilder):
    def __init__(self):
        self.childrenRoot = {
            'weight' : weight_to_dict(WeightDescriptor(1,1,1,0)),
            'subtree' : {}
        }

        self.parentsRoot = {
            'weight' : weight_to_dict(WeightDescriptor(1,1,1,0)),
            'subtree' : {}
        }

    # src is a tuple consisting of the topmost depth (1 or -1) and a chain of the nodes until the leaf
    # dst is a string
    # weight is a WeightDescriptor
    def addEdge(self, src, dst, weight, comment, child_comment):
        if abs(src[0]) != 1:
            raise ValueError("hanging nodes aren't supported", src)

        level = src[0]
        root = self.childrenRoot if  level > 0 else self.parentsRoot

        path = src[1:]
        if len(path) == 1:
            root['subtree'].setdefault(path[0], {
                'weight' : weight_to_dict(WeightDescriptor(1,1,1,0)),
                'subtree' : {},
            })

        node = root
        for node_name in path:
            node = node['subtree'].get(node_name, None)
            if None == node:
                raise FileNotFoundError("couldn't find path", src)

        assert dst not in node, "adding an already existing edge"
        node['subtree'][dst] = {
            'weight' : weight_to_dict(weight),
            'comment' : child_comment,
            'subtree' : {},
        }

        node.setdefault('comment', {})
        node['comment'].update(comment)

    def getDict(self):
        return {
            'children' : {'root' : self.childrenRoot},
            'parents' : {'root' : self.parentsRoot},
        }

    def finalize(self):
        # fix the arbitrary level 1 weight
        roots = self.parentsRoot
        proper_weights = {
            root_name : sum((self.parentsRoot['subtree'][root_name]['subtree'][parent]['weight']['weight'] for parent in self.parentsRoot['subtree'][root_name]['subtree']), 0)
            for root_name in self.parentsRoot['subtree']
        }

        for root_name in proper_weights:
            self.parentsRoot['subtree'][root_name]['weight']['weight'] = proper_weights[root_name]

        for root_name in self.childrenRoot['subtree']:
            self.childrenRoot['subtree'][root_name]['weight']['weight'] = proper_weights.get(root_name, 1)

class JsonGraphFormatter(GraphFormatter):
    def format(self, graph, stream, **options):
        assert isinstance(graph, DictGraphBuilder)
        graph_dict = graph.getDict()
        graph_dict.update(options.get('extra', {}))
        return json.dump(graph_dict, stream, indent="    "*2)

class TextGraphFormatter(GraphFormatter):
    class NicelyRenderedNode(anytree.Node):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)

            self._attributeTransformations = defaultdict(lambda: lambda s: s,
                {
                    'mean' : lambda mean: "{:.5f}%".format(mean * 100),
                    'aggregate_percent' : lambda aggregate_percent: "{:.5f}%".format(aggregate_percent * 100),
                    'variance' : lambda variance: ('std', "{:.5f}%".format(numpy.sqrt(variance))),
                }
            )

        def __repr__(self):
            args = []
            for key, value in filter(lambda item: not item[0].startswith("_"),
                                     sorted(self.__dict__.items(),
                                            key=lambda item: item[0])):
                transformed_attr = self._attributeTransformations[key](value)
                if isinstance(transformed_attr, tuple):
                    args.append("%s=%s" % transformed_attr)
                else:
                    args.append("%s=%s" % (key, transformed_attr))

            _repr = "%s (%s)" % (self.name, ", ".join(args))

            color = self.__dict__.get("_color")
            if color is not None:
                return termcolor.colored(_repr, color)

            return _repr

        @staticmethod
        def SortNodes(nodes):
            return sorted(nodes, key=lambda node: node.__dict__['mean'], reverse=True)

    Colors = ['magenta', 'green', 'cyan', 'yellow', 'blue', 'red', 'white']

    def __init__(self):
        self.colorful = None

    def _formatTreeDict(self, tree_dict, extras):
        root = self.NicelyRenderedNode('root', _color = self.Colors[0] if self.colorful else None)
        root.__dict__.update(extras)

        bfs_queue = [(root, 1, child, tree_dict['subtree'][child]) for child in tree_dict['subtree']]
        while len(bfs_queue) > 0:
            parent_node, level, child_name, tree_dict = bfs_queue.pop()
            node = self.NicelyRenderedNode(child_name, parent=parent_node, _color = self.Colors[level % len(self.Colors)] if self.colorful else None, comment=tree_dict.get('comment', {}), **tree_dict.get('weight',{}))

            bfs_queue += [(node, level + 1, child, tree_dict['subtree'][child]) for child in tree_dict['subtree']]

        return root

    def format(self, graph, stream, **options):
        assert isinstance(graph, DictGraphBuilder), type(graph)

        self.colorful = options.get('colored', False)

        graph_dict = graph.getDict()

        stream.write("Parents Lineage:\n")
        parents_tree = anytree.RenderTree(self._formatTreeDict(graph_dict['parents']['root'], options.get('extra', {})), style=anytree.ContStyle(), childiter=self.NicelyRenderedNode.SortNodes)
        stream.write(str(parents_tree) + "\n")

        stream.write("\nChildren Lineage:\n")
        children_tree = anytree.RenderTree(self._formatTreeDict(graph_dict['children']['root'], options.get('extra', {})), style=anytree.ContStyle(), childiter=self.NicelyRenderedNode.SortNodes)
        stream.write(str(children_tree) + "\n")

