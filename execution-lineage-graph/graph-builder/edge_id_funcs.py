#!/usr/bin/env python3

import logging
from utilities import option_registry_util

id_functions = option_registry_util.OptionRegistry()

@id_functions.register("instance")
def instance_id_func(exec_desc):
    '''
    Identify an endge uniquely by it's src and dst host/secdo-pid/instance-id tuples
    '''
    return (
        exec_desc['depth'], exec_desc['host'],
        exec_desc['pid'], exec_desc['instance'],
        exec_desc['child_pid'], exec_desc['child_instance'],
    )

@id_functions.register("host-prevalence")
def host_id_func(exec_desc):
    '''
    Identify an edge uniquely by the host it appears on.
    '''
    return (exec_desc['depth'], exec_desc['host'], exec_desc['full_path'], exec_desc['child_file'])

