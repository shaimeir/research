#!/usr/bin/env python3

import dialog
import json
import locale
import argparse
import sys

locale.setlocale(locale.LC_ALL, '')

def get_command_line_switches(program, program_arg_desc):
    status, switches = OptionsSelectionDialog(program, program_arg_desc).getProgramArguments()
    return switches if status else None

class OptionsSelectionDialog:
    _Exit = 'Exit'
    _Option = 'Option'
    _Subparser = 'Subparser'

    _ApplyBooleanSwitch = 'apply'
    _DontApplyBooleanSwitch = "don't apply"

    _MenuDescWidth = 200
    _MenuOptionWidth = 30
    _OptionTitleWidth = 150

    def __init__(self, program, program_arg_desc):
        self.dialog = dialog.Dialog()
        self.programOptions = {}
        self.programOptionsDisplayValues = {}
        self.lastInputCache = {}
        self.chosenSubParser = None
        self.subParserArguments = []
        self.subparsers = {
            subparser : OptionsSelectionDialog(f"{program}[{subparser}]", subparser_desc) for subparser, subparser_desc in program_arg_desc.get('subparsers', {}).get('parsers', {}).items()
        }

        self.program = program
        self.programArgsDesc = program_arg_desc

    def _isSwitch(self, option_name):
        return option_name.startswith("-")

    def getOption(self, arg_name, arg_desc):
        default_value = self.lastInputCache.get(arg_name, None)
        option_name = max(arg_desc.get('args', [arg_name]), key=len)

        arg_params = arg_desc.get('kwargs', {})
        dialog_text = '{} - {}'.format(arg_name, arg_params.get('help', 'no help available').split("\n")[0])
        if len(dialog_text) > self._OptionTitleWidth:
            dialog_text = dialog_text[:self._OptionTitleWidth - 3] + '...'

        # limited input switches
        if 'choices' in arg_params:
            action = arg_params.get('action', 'store')
            width = max([max(map(len, dialog_text.split("\n")))] + list(map(len, arg_params['choices'])) + [len(dialog_text)])
            if action == 'store':
                set_choice = lambda choice: default_value is None or str(choice) == default_value
                code, selection = self.dialog.radiolist(dialog_text, choices = ((str(choice), '', set_choice(choice)) for choice in arg_params['choices']), width=width)
                return code == self.dialog.OK, selection.strip(), [option_name, selection], selection
            elif action == 'append':
                mark_choice = lambda choice: default_value is not None and str(choice) in default_value
                code, selection = self.dialog.checklist(dialog_text, choices = ((str(choice).strip(), '', mark_choice(choice)) for choice in arg_params['choices']), width=width)
                return code == self.dialog.OK, ', '.join(selection), sum([[option_name, str(choice)] for choice in selection], []), selection
            else:
                raise Exception(f"{arg_name}: combination of 'choices' and action type '{action}' is not supported", arg_desc)

        # aggregate opitons
        action = arg_params.get('action', 'store')
        nargs = arg_params.get('nargs', 1)

        if action == 'store':
            code, value = self.dialog.inputbox(dialog_text, init='' if default_value is None else default_value, width=len(dialog_text))
            input_value = value
            value = value.strip()
            parsed_option = value.split(" ") if nargs == '+' or nargs > 1 else [value]
            option_switch = [option_name] if self._isSwitch(option_name) else []
            return (
                code == self.dialog.OK,
                value.strip(),
                option_switch + parsed_option if len(value) > 0 else [],
                input_value,
            )
        elif action == 'append':
            code, values = self.dialog.editbox_str('', init='' if default_value is None else default_value)
            input_values = values
            values = [value.strip() for value in values.split("\n") if value.strip() != '']

            if nargs == 1:
                display_values = ', '.join(values)
                values = [[value] for value in values]
            else:
                display_values = ', '.join((f'({value})' for value in values))
                values = [value.split(" ") for value in values]

            return (
            code == self.dialog.OK,
            display_values,
            sum([[option_name] + value for value in values], []) if self._isSwitch(option_name) else sum(values, []),
            input_values,
            )
        elif action in ('store_true', 'store_false'):
            if default_value is None:
                default_value = self._DontApplyBooleanSwitch
            code, is_set = self.dialog.radiolist(dialog_text, choices = [(option, '', option==default_value) for option in (self._DontApplyBooleanSwitch, self._ApplyBooleanSwitch)], width=len(dialog_text))
            return (
                code == self.dialog.OK,
                'applied' if 'apply' == is_set else '',
                [option_name] if 'apply' == is_set else [],
                is_set
            )

        raise Exception(f"{arg_name}: can't handle argument descriptor", arg_desc)

    def _getOptionDescription(self, arg_name, arg_desc):
        if arg_name in self.programOptionsDisplayValues:
            description = str(self.programOptionsDisplayValues[arg_name]) + " [ "
        else:
            description = "[ " + ("(required) " if arg_desc['kwargs'].get('required', False) else "(optional) ")

        description += arg_desc.get('kwargs', {}).get('help', 'no description available')
        description = description.split("\n", 1)[0].strip()

        return (description if len(description) <= self._MenuDescWidth else description[:self._MenuDescWidth - 3] + "...") + " ]"

    def _decorateSubparserName(self, subparser):
        if subparser == self.chosenSubParser:
            return "*{}*".format(subparser)

        return subparser

    def parserOptionsMenu(self):
        if 'init' in self.programArgsDesc:
            description = self.programArgsDesc['init'].get('description', 'no description available')
        else:
            description = self.programArgsDesc.get('kwargs',{}).get('help', 'no description available')

        arguments_options = [(arg_name.replace("_", " "), self._getOptionDescription(arg_name, arg_desc)) for arg_name, arg_desc in self.programArgsDesc.get('parser', self.programArgsDesc)['arguments'].items()]

        subparsers = self.programArgsDesc.get('subparsers', {}).get('parsers', {})
        subparsers_options = [
            (
                self._decorateSubparserName(subparser.strip()).replace("_", " "),
                '(subparser) {}'.format(subparser_desc.get('kwargs',{}).get('help', 'no description available'))
            )
            for subparser, subparser_desc in subparsers.items()
        ]

        menu_choices = arguments_options + subparsers_options + [('Exit', '')]
        width = max(self._MenuOptionWidth, max(map(lambda entry: len(entry[0]), menu_choices))) + min(self._MenuDescWidth, max(map(lambda entry: len(entry[1]), menu_choices)))
        code, selection = self.dialog.menu(f'{self.program} - {description}', choices = menu_choices, width=width)

        # strip chosen sub-parser decoration
        return self.dialog.OK == code, selection.strip("*").replace(" ", "_"), self._Subparser if selection in subparsers else self._Exit if 'Exit' == selection else self._Option

    def getProgramArguments(self):
        selection = None

        while selection != self._Exit:
            is_selection, selection, _type = self.parserOptionsMenu()

            if not is_selection:
                return False, None

            if _type == self._Subparser:
                subparser = selection
                status, command_arguments = self.subparsers[subparser].getProgramArguments()
                if status:
                    self.chosenSubParser = subparser
                    self.subParserArguments = command_arguments

            elif _type == self._Option:
                option_name = selection
                status, display_value, option, input_value = self.getOption(option_name, self.programArgsDesc.get('parser', self.programArgsDesc)['arguments'][option_name])
                if status:
                    if len(option) == 0:
                        self.programOptions.pop(option_name, None)
                        self.programOptionsDisplayValues.pop(option_name, None)
                        self.lastInputCache.pop(option_name, None)
                    else:
                        self.programOptions[option_name] = option
                        self.programOptionsDisplayValues[option_name] = display_value
                        self.lastInputCache[option_name] = input_value

        args = sum(self.programOptions.values(), [])
        if self.chosenSubParser is not None:
            args += [self.chosenSubParser] + self.subParserArguments

        return True, args

##### Tests

TestGraphDiffArgsDescriptor = {
    "init": {},
    "arguments": {
        "verbose": {
            "args": [
                "-v",
                "--verbose"
            ],
            "kwargs": {
                "action": "store_true",
                "help": "show debug output"
            }
        },
        "variance": {
            "args": [
                "--variance"
            ],
            "kwargs": {
                "default": 1e-05,
                "help": "an upper bound for the variance to be considered 0."
            }
        },
        "mean_diff": {
            "args": [
                "--mean-diff"
            ],
            "kwargs": {
                "default": 1e-05,
                "help": "an upper bound for the difference in means to be considered 0."
            }
        }
    },
    "subparsers": {
        "kwargs": {
            "help": "",
            "dest": "action"
        },
        "parsers": {
            "print": {
                "kwargs": {
                    "help": "print the execution trees described in JSON files."
                },
                "parser": {
                    "init": {},
                    "arguments": {
                        "json": {
                            "args": [
                                "json"
                            ],
                            "kwargs": {
                                "nargs": "+",
                                "help": "JSON files dumped by execution-graph-builder.py to print."
                            }
                        }
                    }
                }
            },
            "diff": {
                "kwargs": {
                    "help": "prints the difference tree between the children of two trees described by a couple of JSONs."
                },
                "parser": {
                    "init": {},
                    "arguments": {
                        "weight": {
                            "args": [
                                "-w",
                                "--weight"
                            ],
                            "kwargs": {
                                "choices": [
                                    "dummy",
                                    "exponent",
                                    "exponent-no-ref",
                                    "uncontained-exponent",
                                    "boolean",
                                    "parentage-likelihood"
                                ],
                                "required": True,
                                "help": "the weight processing function to generate the weights in the diff tree. * dummy - Dummy weigher used for debugging. The weight is just a dict with the diff information..\n* exponent - Use sum(mean*exp(-(mean-instance_mean)^2/variance)) as a scoring method. Doesn't handle unreferenced children of the instance..\n* exponent-no-ref - Use sum(mean*exp(-(mean-instance_mean)^2/variance)) as a scoring method. Ignores unreferenced children of the instance..\n* uncontained-exponent - Yield 100%% match if the children in the tree are all referenced, regardless of distribution. Otherwise revert to mean*exp(...) and ignore unreferenced children..\n* boolean - Yield 100%% match if the instance lineage is fully contained in the averege lineage and 0%% otherwise..\n* parentage-likelihood - Assigns the aggregate ration as to each parent and uses it's multiplication as a similarity score.."
                            }
                        },
                        "summary": {
                            "args": [
                                "-s",
                                "--summary"
                            ],
                            "kwargs": {
                                "action": "store_true",
                                "help": "print just the score and the files comapred."
                            }
                        },
                        "ref_json": {
                            "args": [
                                "-r",
                                "--ref-json"
                            ],
                            "kwargs": {
                                "required": True,
                                "help": "the reference JSON."
                            }
                        },
                        "tree_json": {
                            "args": [
                                "tree_json"
                            ],
                            "kwargs": {
                                "metavar": "tree-json",
                                "nargs": "+",
                                "help": "the trees to compare to the reference JSON."
                            }
                        }
                    }
                }
            },
            "child-diff": {
                "kwargs": {
                    "help": "prints the similarity scores for a batch of JSON trees against a batch of averege trees."
                },
                "parser": {
                    "init": {},
                    "arguments": {
                        "weight": {
                            "args": [
                                "-w",
                                "--weight"
                            ],
                            "kwargs": {
                                "choices": [
                                    "dummy",
                                    "exponent",
                                    "exponent-no-ref",
                                    "uncontained-exponent",
                                    "boolean",
                                    "parentage-likelihood"
                                ],
                                "required": True,
                                "help": "the weight processing function to generate the weights in the diff tree. * dummy - Dummy weigher used for debugging. The weight is just a dict with the diff information..\n* exponent - Use sum(mean*exp(-(mean-instance_mean)^2/variance)) as a scoring method. Doesn't handle unreferenced children of the instance..\n* exponent-no-ref - Use sum(mean*exp(-(mean-instance_mean)^2/variance)) as a scoring method. Ignores unreferenced children of the instance..\n* uncontained-exponent - Yield 100%% match if the children in the tree are all referenced, regardless of distribution. Otherwise revert to mean*exp(...) and ignore unreferenced children..\n* boolean - Yield 100%% match if the instance lineage is fully contained in the averege lineage and 0%% otherwise..\n* parentage-likelihood - Assigns the aggregate ration as to each parent and uses it's multiplication as a similarity score.."
                            }
                        },
                        "ref_json": {
                            "args": [
                                "-r",
                                "--ref-json"
                            ],
                            "kwargs": {
                                "required": True,
                                "action": "append",
                                "help": "a reference JSON or a directory containing reference JSONs."
                            }
                        },
                        "ref_id_method": {
                            "args": [
                                "-i",
                                "--ref-id-method"
                            ],
                            "kwargs": {
                                "choices": [
                                    "parentage",
                                    "tag",
                                    "alternative-tag"
                                ],
                                "default": "tag",
                                "help": "the method by which to index a reference tree for lookup."
                            }
                        },
                        "tree_id_method": {
                            "args": [
                                "-t",
                                "--tree-id-method"
                            ],
                            "kwargs": {
                                "choices": [
                                    "parentage",
                                    "tag",
                                    "alternative-tag"
                                ],
                                "default": "alternative-tag",
                                "help": "the method by which to index a tested JSON tree for lookup."
                            }
                        },
                        "tree_json": {
                            "args": [
                                "tree_json"
                            ],
                            "kwargs": {
                                "metavar": "tree-json",
                                "nargs": "+",
                                "help": "JSON files or directories containing JSON files of the trees to compare to the reference JSONs."
                            }
                        }
                    }
                }
            }
        }
    }
}

def _option_selection_test():
    opt_select = OptionsSelectionDialog('test', {})
    vertica = opt_select.getOption(
        'vertica',
        {
            "args": [
                "-v",
                "--vertica"
            ],
            "kwargs": {
                "required": True,
                "choices": [
                    "testeu.lab",
                    "test1.il.payoneer.deprecated",
                    "cloud1.us.idt",
                    "cloud1.il.payoneer",
                    "cloud1.us.css_pan"
                ],
                "help": "address of the Vertica to connect to."
            }
        }
    )

    user = opt_select.getOption(
        'user',
        {
            "args": [
                "-u",
                "--user"
            ],
            "kwargs": {
                "help": "Vertica username. Taken from defaults file if not set."
            }
        }
    )

    host_id = opt_select.getOption(
        'host id',
        {
            "args": [
                "--host-id"
            ],
            "kwargs": {
                "action": "append",
                "help": "limit the queries to certain host ids. (this option is an aggregating one)."
            }
        }
    )

    interactive_filtering = opt_select.getOption(
        'interactive filter',
        {
            "args": [
                "-i",
                "--interactive-filtering"
            ],
            "kwargs": {
                "action": "store_true",
                "help": "display dialogs when filtering is required instead of performing trivial filtering."
            }
        }
    )

    no_pre_filtering = opt_select.getOption(
        'no pre filtering',
        {
            "args": [
                "--no-pre-filtering"
            ],
            "kwargs": {
                "action": "store_false",
                "help": "don't run lineage filters in a pre-filtering iteration to dilute roots only to those that fit certain criteria."
            }
        }
    )

    preliminary_filters = opt_select.getOption(
        'preliminary filters',
        {
            "args": [
                "-f",
                "--preliminary-filter"
            ],
            "kwargs": {
                "default": [],
                "action": "append",
                "choices": [
                    "guid-filename",
                    "microsoft-signed"
                ],
                "help": "add preliminary elimination filters.\n * guid-filename - Filter processes with a GUID name.\n * microsoft-signed - Filter processes that are signed by microsoft."
            }
        }
    )

    print("option menu:")
    print("\tvertica (choices)", vertica)
    print("\tuser (input)", user)
    print("\thost id (input + append)", host_id)
    print("\tinteractive filtering (store_true)", interactive_filtering)
    print("\tinteractive filtering (store_false)", no_pre_filtering)
    print("\tpreliminary filters (append + choices)", preliminary_filters)

def _program_menu_test():
    opt_select = OptionsSelectionDialog('graph diff', TestGraphDiffArgsDescriptor)

    opt_select.programOptions['variance'] = 0.001
    opt_select.chosenSubParser = 'child-diff'
    graph_diff_menu_result = opt_select.parserOptionsMenu()

    print("program menu:")
    print("\tgraph diff menu selection", graph_diff_menu_result)

def _full_test():
    opt_select = OptionsSelectionDialog('graph diff', TestGraphDiffArgsDescriptor)
    status, switches = opt_select.getProgramArguments()

    print("complete test")
    print("\tstatus", "OK" if status else "Cancelled")
    print("\tswitches", switches)

def _live_demo_test():
    args_desc = json.loads(sys.stdin.read().strip())
    opt_select = OptionsSelectionDialog('live demo', args_desc)

    status, switches = opt_select.getProgramArguments()

    print("complete test")
    print("\tstatus", "OK" if status else "Cancelled")
    print("\tswitches", switches)

def test():
    tests = {
        'option-test' : _option_selection_test,
        'program-menu-test' : _program_menu_test,
        'full-test' : _full_test,
        'live-demo' : _live_demo_test,
    }

    parser = argparse.ArgumentParser(description="test the program options dialog class")
    parser.add_argument(
        "test",
        choices=list(tests.keys()),
        help='what test to run'
    )

    args = parser.parse_args()

    tests[args.test]()

if __name__ == "__main__":
    test()

