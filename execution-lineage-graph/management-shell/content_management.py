#!/usr/bin/env python3

import sqlite3
import json
import logging
import contextlib
import argparse
from collections import namedtuple
import dateutil.parser
import os

class ContentManager:
    _CommandEntry = namedtuple('_CommandEntry', ('timestamp', 'app', 'program', 'arguments', 'output'))

    def __init__(self, management_db):
        self.managementDb = management_db
        self.sqlite = sqlite3.connect(management_db, isolation_level=None)
        self.cursor = self.sqlite.cursor()

        self.validateSchema()

    def _execute(self, query, *args, single_col=False):
        logging.debug("executing query: %s", query)
        result = self.cursor.execute(query, *(tuple() if len(args) == 0 else (args,))).fetchall()
        logging.debug("result is %r", result)

        if single_col:
            return [r[0] for r in result]
        return result

    def validateSchema(self):
        schema_tables = {
            'commands' : {
                "timestamp" : "DATETIME DEFAULT CURRENT_TIMESTAMP",
                "app" : "TEXT", # the applet that was invoked
                "program" : "TEXT", # the actual program to execute
                "arguments" : "TEXT", # the command line switches that were used
                "output" : "TEXT", # output files
            },
        }

        existing_tables = self._execute("SELECT name FROM sqlite_master WHERE type='table'", single_col=True)
        for table, schema in schema_tables.items():
            if  table not in existing_tables:
                schema_desc = ', '.join((f'{col} {col_type}' for col, col_type in schema.items()))
                self._execute(f'CREATE TABLE {table} ({schema_desc})')

    def saveExecutionRecord(self, app, program, arguments, output_files):
        with contextlib.suppress(sqlite3.Error):
            self._execute(
                "INSERT INTO commands(app, program, arguments, output) VALUES(?,?,?,?)",
                app,
                program,
                json.dumps({'args' : arguments, 'program' : program}),
                json.dumps({'output' : output_files}),
            )

    def _parseEntry(self, entry):
        return  self._CommandEntry(
            dateutil.parser.parse(entry[0]), # timestamp
            entry[1], # app
            entry[2], # program
            json.loads(entry[3])['args'], #arguments
            json.loads(entry[4])['output'],
        )

    def getAppHistory(self, app):
        execution_history = sorted(
            [self._parseEntry(entry) for entry in self._execute("SELECT * FROM commands WHERE app=?", app)],
            key=lambda entry: entry.timestamp
        )

        return execution_history

    @contextlib.contextmanager
    def context(self):
        try:
            yield self
        finally:
            # self.cursor.execute("commit")
            pass

def test():
    parser = argparse.ArgumentParser()
    parser.add_argument("-k", "--keep-db", action="store_true")    
    parser.add_argument("-v", "--verbose", action="store_true")    
    args = parser.parse_args()

    logging.basicConfig(format="LOG: %(message)s", level=logging.DEBUG if args.verbose else logging.WARN)

    @contextlib.contextmanager
    def _test_db(keep_db = False):
        with contextlib.suppress(FileNotFoundError):
            if not keep_db:
                os.remove("/tmp/test-db")

        try:
            yield

        finally:
            with contextlib.suppress(FileNotFoundError):
                if not keep_db:
                    os.remove("/tmp/test-db")

    test_app1 = sorted([
        ("tom-jones", "tom-jones.py", "run on for a long time".split(), "sooner or later god's gonna cut you done".split()),
        ("tom-jones", "tom-jones.py", "tell that long tongue lier".split(), "tell that midnight rider".split()),
    ])

    test_app2 = sorted([
        ("johnny-cash", "jc", "tell the rambaler, the gambler".split(), "the back biter".split()),
        ("johnny-cash", "jc", "you'll sneak up and knock on his door".split(), "look out brother you'll knock no more".split()),
    ])

    with _test_db(args.keep_db):
        with ContentManager("/tmp/test-db").context() as cm:
            for app in test_app1 + test_app2:
                cm.saveExecutionRecord(*app)

            # note that str.split() returns a list
            entry_to_tuple = lambda entry: (entry.app, entry.program, list(entry.arguments), list(entry.output))
            app1 = sorted([entry_to_tuple(e) for e in cm.getAppHistory("tom-jones")])
            app2 = sorted([entry_to_tuple(e) for e in cm.getAppHistory("johnny-cash")])

            assert app1 == test_app1
            assert app2 == test_app2

            print("tests passed successfully")

if __name__ == "__main__":
    test()

