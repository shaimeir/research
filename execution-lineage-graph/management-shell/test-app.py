#!/usr/bin/env python3

from utilities import argparse_automation_wrappaer

def main():
    parser = argparse_automation_wrappaer.ArgumentParser()
    parser.add_argument("--store-true", action='store_true', help='test switch')
    parser.add_argument("--store-value", default=None, help='test parameter')
    parser.add_argument("arguments", nargs='+', help='test arguments')

    args = parser.parse_args()
    print('test applet', args)

if __name__ == "__main__":
    main()

