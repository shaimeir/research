#!/usr/bin/env python3

import logging
import cmd
import os
import pathlib
import json
import subprocess
import contextlib
import sys

import options_dialog
from utilities import argparse_automation_wrappaer
from utilities.text_utils import ColoredText

DefaultShellConfig = os.path.expanduser("~/.management-cfg")

class ManagementShell(cmd.Cmd):
    def __init__(self, completekey='tab', stdout=ColoredText(sys.stdout)):
        super().__init__(completekey=completekey, stdout=stdout)
        self.apps = {}
        self.prompt = ColoredText.green('[') + ColoredText.yellow('research shell') + ColoredText.green(']') + ' '

    def error(self, s):
        with self.stdout.colorContext('red'):
            self.stdout.write(s.strip() + '\n')

    def addApp(self, app_name, program):
        self.apps[app_name] = program

    def _getArgumentsDump(self, program):
        p = subprocess.run([program, argparse_automation_wrappaer.ArgumentsDumpOption], stdout=subprocess.PIPE)
        with contextlib.suppress(json.decoder.JSONDecodeError):
            return json.loads(p.stdout.strip())

        return None

    def runApplet(self, program, switches):
        subprocess.run([program] + switches)

        # TODO: log switches and categorize output

    def default(self, arg):
        if arg not in self.apps:
            return self.error(f'{arg}: command not found')

        app_name = arg
        program = self.apps[arg]
        
        command_line_args_desc = self._getArgumentsDump(program)
        if command_line_args_desc is None:
            return self.error(f'{arg}: program doesn\'t support the arguments dump convention')

        switches = options_dialog.get_command_line_switches(app_name, command_line_args_desc)
        self.stdout.write("\n")
        if switches is None:
            return

        self.runApplet(program, switches)

    def do_lsapps(self, arg):
        with self.stdout.colorContext('cyan'):
            self.stdout.write('\n'.join((f'\t{app}' for app in self.apps.keys())) + '\n')

    def do_helpapp(self, arg):
        if arg not in self.apps:
            return self.error(f'{arg}: not a registered app')

        subprocess.run([self.apps[arg], '-h'])

    def emptyline(self):
        pass

    def do_quit(self, arg):
        """
        Quit the shell.
        """
        return True

    def do_EOF(self, arg):
        """
        Quit the shell.
        """
        self.stdout.write("\n")
        return True

    def do_exit(self, arg):
        """
        Quit the shell.
        """
        return True

def main():
    shell = ManagementShell()
    
    apps_dir = pathlib.Path(os.path.abspath(__file__)).parent/'apps'
    apps = [apps_dir/app for app in os.listdir(str(apps_dir)) if os.path.isfile(apps_dir/app)]

    for app in apps:
        shell.addApp(app.name, os.path.realpath(app))

    shell.cmdloop()

if __name__ == '__main__':
    main()

