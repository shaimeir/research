#!/usr/bin/env bash -eu

dump_script="$(dirname $(readlink -f ${BASH_SOURCE[0]}))"/dump-execution-lineage.py

vertica=
output_directory=
lineage_file=

function dump_file_name()
{
    (echo ${1} | tr -d "\\" | tr -d '%' | tr '[:upper:]' '[:lower:]') 2>/dev/null
}

function help()
{
    echo "usage: ${BASH_SOURCE[0]} [-h] -v <vertica> -o <output directory> [-f <lineage patterns file>] patterns[...]"
    echo "  -v - specify the name of the vertica in the defaults file to use."
    echo "  -o - specify the output directory to write the dumps to."
    echo "  -f - a file to read parent/grandparent patterns from. if not specified, uses stdin."
    echo "  -h - display this help."
    echo "  patterns[...] - one or more SQL 'LIKE'-like patterns of file paths to look for."
}

function perror()
{
    echo ${@} >&2
    help >&2
    exit 1
}

while getopts ":o:v:f:h" option
do
    case "${option}" in
        "o") output_directory="${OPTARG}"
             ;;
        "v") vertica="${OPTARG}"
             ;;
        "f") lineage_file="${OPTARG}"
             ;;
        "h") help
             exit 0
             ;;
    esac
done
shift $((${OPTIND}-1))

[ "${output_directory}" == "" ] && perror "missing output directory"
[ "${vertica}" == "" ] && perror "missing vertica"
[ "${#}" -eq "0" ] && perror "no pattern specified"

mkdir -p "${output_directory}"

if [ "" != "${lineage_file}" ]
then
    exec <${lineage_file}
fi

sed 's/\\/\\\\/g' | while read parent grandparent
do
    echo "dumping averege tree information for pattern '${@}' born of ${parent} son of ${grandparent} in ${vertica}"
    ${dump_script} --verbose -v ${vertica} -o ${output_directory}/$(dump_file_name "${parent}")-$(dump_file_name "${grandparent}")-$(head -c 32 /dev/urandom | md5 | head -c 10)-dump.csv -g "${parent}=-1" -g "${grandparent}=-2" ${@}
done

