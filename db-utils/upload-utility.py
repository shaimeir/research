#!/usr/bin/env python3

"""
a utility to upload a CSV dump to a table in a database.
this utility can accept a configuration and work with the various databases supported by the dbcursors/ subpackage.
"""

import sys
import argparse
import csv
import contextlib
import logging
import getpass

import dbcursors
import scheme

class RowParserCsv:
    '''
    a class that wraps the parsing of a row from the CSV.
    '''
    def __init__(self, reader, csv_scheme=None):
        self.reader = reader
        self.reader_iter = iter(reader)
        self.csv_scheme = csv_scheme
        self.explicit_types = {}
        self.db_columns = {}

    def add_type(self, column, col_type):
        self.explicit_types[column] = scheme.scheme_data_types.scheme_types.option(col_type)

    def add_db_col(self, col, db_column):
        if col == db_column:
            return

        elif self.csv_scheme is None or self.csv_scheme.inverse_column_translation().get(col, col) in (col, db_column):
            self.db_columns[col] = db_column

        else:
            # the scheme has a different db column name!
            raise ValueError("mismatching db-column names", col, db_column, self.csv_scheme.inverse_column_translation())

    def parse_row(self, row):
        parsed_row = row

        if self.csv_scheme is not None:
            parsed_row = self.csv_scheme.parse_row(parsed_row)

        for col_name, parser in self.explicit_types.items():
            parsed_row[col_name] = parser(row[col_name])

        if self.csv_scheme is not None:
            parsed_row = self.csv_scheme.translate_row(parsed_row)

        parsed_row = {
            self.db_columns.get(k, k) : v
            for k, v in parsed_row.items()
        }

        return parsed_row

    def __iter__(self):
        return self

    def __next__(self):
        parsed_row = self.parse_row(next(self.reader_iter))
        return parsed_row

class IterationLimiter:
    def __init__(self, iterable, limit, queue=[]):
        self.iterable = iter(iterable)
        self.limit = limit
        self.queue = queue

    def __iter__(self):
        return self

    def __next__(self):
        if self.limit <= 0:
            raise StopIteration()

        self.limit -= 1
        return next(self.iterable) if len(self.queue) == 0 else self.queue.pop(0)

class IterableChunkifier:
    def __init__(self, iterable, chunk_size):
        self.iterable = iter(iterable)
        self.chunk_size = chunk_size

    def __iter__(self):
        return self

    def __next__(self):
        sample = next(self.iterable) # make sure we have at least one more item to iterate over
        return IterationLimiter(self.iterable, self.chunk_size, [sample])

def upload_content(reader, cursor, table, chunk_size=3000):
    # to avoid reading the entire CSV all at once we'll use IterableChunkifier to read it in chunks
    for chunk in IterableChunkifier(reader, chunk_size):
        cursor.bulk_insert(table, chunk)

    cursor.commit()

@contextlib.contextmanager
def auto_yield(o):
    yield o

def main():
    db_defaults = dbcursors.default_dbs.load_dbs_from_command_line()

    parser = argparse.ArgumentParser(description='upload a CSV file to a database table.')
    dbcursors.default_dbs.add_standard_command_line_arg(parser)
    parser.add_argument("-v", "--verbose", action="store_true", help="show debug prints.")
    parser.add_argument("-d", "--db", required=True, choices=list(db_defaults.keys()), help="the database to connect to (and schema to use).")
    parser.add_argument("-t", "--table", required=True, help="the table to upload to.")
    parser.add_argument("-p", "--no-password", action='store_true', help='a password is not required.')
    parser.add_argument("-r", "--print", action='store_true', help="just print the queries required.")
    parser.add_argument("-s", "--scheme", help="use a scheme to parse CSV column types.")
    scheme_types = ", ".join(scheme.scheme_data_types.scheme_types.choicesList())
    parser.add_argument("-y", "--type", nargs=2, action='append', default=[], help=f'set explicit column types. the first argument is the column name and the second is the type (one of: {scheme_types}).')
    parser.add_argument("-l", "--db-column", nargs=2, action='append', default=[], help="set the db-column name to translate a CSV column name to. the first argument is the CSV column name and the second is the db-column name.")
    parser.add_argument("--delimiter", default="\t", help="set the CSV delimiter.")
    parser.add_argument("csv", nargs="?", default="-", help="the CSV to upload to the database ('-' for stdin).")

    args = parser.parse_args()

    logging.basicConfig(level=logging.INFO if args.verbose else logging.WARN, format="%(asctime)s [%(levelname)s] %(message)s")

    csv_scheme = None if args.scheme is None else scheme.load_scheme_file(args.scheme, scheme.get_default_schemes_directory())
    scheme_types_list = scheme.scheme_data_types.scheme_types.choicesList()
    for col, col_type in args.type:
        assert col_type in scheme_types_list, ValueError("invalid explicit column data type for column", col, col_type)

    db_args = db_defaults[args.db]
    db_args.setdefault('password', None if args.no_password or args.print else getpass.getpass('Database password: '))

    cursor = dbcursors.get_cursor_factory(db_args['dbtype'], 'print' if args.print else None, True)(**db_args)
    with cursor.context():
        with auto_yield(sys.stdin) if "-" == args.csv else open(args.csv, "rt") as csv_stream:
            reader = csv.DictReader(csv_stream, delimiter=args.delimiter)
            parsing_reader = RowParserCsv(reader, csv_scheme)

            for col, col_type in args.type:
                parsing_reader.add_type(col, col_type)

            for col, db_col in args.db_column:
                parsing_reader.add_db_col(col, db_col)

            upload_content(parsing_reader, cursor, args.table)

if __name__ == "__main__":
    sys.exit(main())

