/*
 * a PostgreSQL query that dumps alerts information (stored in the siem_events table).
 * this script assumes the correct scheme was already picked via 'SET SEARCH_PATH = db_1234...'
 */

SELECT distinct
    siem_event_id                       AS siem_event_id,
    is_archive                          AS is_archive,
    secdo_data[1]->>'secdo_alert_name'  AS alert_name,
    secdo_data[1]->>'secdo_severity'    AS severity,
    agent_id                            AS host_id,
    secdo_data[1]->>'secdoProcessId'    AS secdo_process_id,
    secdo_data[1]->>'instanceId'        AS instance_id,
    secdo_data[1]->>'CID'               AS cid

FROM
    siem_events

WHERE
    status='COMPLETED_OK'

