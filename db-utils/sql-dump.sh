#!/usr/bin/env bash

set -eu

db_utils_dir="$(dirname $(readlink -f ${BASH_SOURCE[0]}))"
get_db_prop="${db_utils_dir}/get-db-prop.py"
dumpers_dir="${db_utils_dir}/sql-dumpers"

function print_help()
{
    echo "perform a query on a company specified in a database configuration file."
    echo "the correct utility script is picked from sql-dumpers/ and the query will be fed to it's stdin."
    echo ""
    echo "${BASH_SOURCE[0]} -c COMPANY [-d DATABASE-FILE] [-s DATABASE-FILE-SUB-SECTION] [-C] [-u] [-m MARKER] [-r] [-x] [-o OUTPUT-FILE] [QUERY-FILE]"
    echo " * -c COMPANY - the company to perform the dump on. (mandatory)"
    echo " * -d DATABASE-FILE - the database file the company is defined in. (optional, default: ~/.secdo-dbs)"
    echo " * -s DATABASE-FILE-SUB-SECTION - the sub-section in the database file in which the databases are defined."
    echo " * -C - compress the traffic between the server and client. (may not always be supported)"
    echo " * -u - generate decompressed output."
    echo " * -m MARKER - set the expected marker pattern (i.e. when the actual output begins)."
    echo " * -r - don't expect an output marker and just take the query output as-is."
    echo " * -x - set the bash -x flag."
    echo " * -o OUTPUT-FILE - output file."
    echo " * QUERY-FILE - the file the query is specified in. defaults to stdin."
}

function perror()
{
    echo "${@}" >&2
    exit 1
}

function get_dump_script()
{
    dbtype="${1}"

    case "${dbtype}" in
        "mysql"|"memsql")
            echo "${dumpers_dir}/mysql-dump.sh"
            ;;

        "vertica")
            echo "${dumpers_dir}/vsql-dump.sh"
            ;;

        *)
            return 1
    esac
}

marker="SCHEME_DUMP_MARKER"
db_config_file="${HOME}/.secdo-dbs"
db_config_file_sub_sections=""
while getopts ":c:d:s:o:hs:uxm:rC" option
do
    case "${option}" in
        "c") company="${OPTARG}"
             ;;

        "d") db_config_file="${OPTARG}"
             ;;

        "s") db_config_file_sub_sections="${db_config_file_sub_sections} ${OPTARG}"
             ;;

        "o") output_switch="-o ${OPTARG}"
             ;;

        "u") u_flag="-u"
             ;;

        "h") print_help >&2
             exit 0
             ;;

        "x") set -x
             x_flag="-x"
             ;;

        "m") marker_switch="-m ${OPTARG}"
             ;;

        "r") r_flag="-r"
             ;;

        "C") server_compression_flag="-C"
             ;;
    esac
done
shift $((OPTIND-1))

if [ "${company:-}" == "" ]
then
    echo "missing company." >&2
    print_help >&2
    exit 1
fi

# set the query stream to be stdin
if [ "${#}" -eq 0 ]
then
    : # nop, do nothing - the query will be taken from stdin.
elif [ "${#}" -eq 1 ]
then
    # redirect our stdin to take the query from a file
    echo "input taken from ${1}" >&2
    exec <"${1}"
else
    echo "error: too many query files!" >&2
    print_help >&2
    exit 1
fi

# verify all required properties
${get_db_prop} --db-config ${db_config_file} ${db_config_file_sub_sections} \
                -p company \
                -p host \
                -p dbtype \
                "${company}" &>/dev/null || perror "'${company}' not fully configured in: ${db_config_file} ${db_config_file_sub_sections}"

company_name="$(${get_db_prop} --db-config ${db_config_file} ${db_config_file_sub_sections} -p company ${company})"
host="$(${get_db_prop} --db-config ${db_config_file} ${db_config_file_sub_sections} -p host ${company})"
dbtype="$(${get_db_prop} --db-config ${db_config_file} ${db_config_file_sub_sections} -p dbtype ${company})"

get_dump_script "${dbtype}" || perror "unsupported dbtype ${dbtype}"
dump_script="$(get_dump_script ${dbtype})"

# if compression was specifically specified in the command line we can carry on
if [ -z "${server_compression_flag:-}" ]
then
    :
# if there's no "compress" flag at all in the database file carry on
elif ! ${get_db_prop} --db-config ${db_config_file} ${db_config_file_sub_sections} -p compress &>/dev/null
then
    :
# if the compress flag was specified and is "1" then set the compression flag
elif [ "$(${get_db_prop} --db-config ${db_config_file} ${db_config_file_sub_sections} -p compress)" == "1" ]
then
    server_compression_flag="-C"
fi

${dump_script} -c "${company_name}" -s "${host}" ${output_switch:-} ${x_flag:-} ${u_flag:-} ${marker_switch:-} ${r_flag:-} ${x_flag:-} ${server_compression_flag:-}

