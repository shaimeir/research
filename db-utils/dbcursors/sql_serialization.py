#!/usr/bin/env python3

'''
this file will be used to unify serialization of native data types into SQL query format.
it shall be used when auto-generating queries to properly convert data given externally (command line,
programatically or from a data file).
'''

import datetime
import logging
import dateutil.parser
import inspect
from collections import Iterable, namedtuple
from recordclass import recordclass

TypeRecord = recordclass('TypeRecord', ('serializer', 'db_type'))
TypeAssociationTuple = namedtuple('TypeAssociationTuple', ('type', 'db_type'))

class QuerySerializer:
    def __init__(self, serialization_dict=None, types_dict=None, lower_func=None, default_type=str):
        self._serializers = {} if serialization_dict is None else serialization_dict
        self._types = {} if types_dict is None else types_dict
        self._lower_func = lower_func if lower_func is not None else (lambda field: f"LOWER({field})")

        self.serializers = {}
        for data_type in set(self._serializers.keys()).union(self._types.keys()):
            self.serializers[data_type] = TypeRecord(self._serializers.get(data_type, None), self._types.get(data_type, None))

        self.default_type = default_type

    def __getitem__(self, _type):
        return self.serializers[_type]

    def __setitem__(self, _type, serializer):
        if serializer is None:
            self.serializers.pop(_type, None)
            return

        if hasattr(serializer, 'serializer') and hasattr(serializer, 'db_type'):
            self.serializers[_type] = serializer
            return

        if not isinstance(serializer, (tuple, list)):
            serializer = [serializer]

        if len(serializer) == 0:
            self.serializers.pop(_type, None)
            return

        self.serializers[_type] = TypeRecord(*((tuple(serializer) + (None,))[:2]))

    def _get_serializer(self, value):
        serializer = self.serializers.get(type(value), self.serializers.get(self.default_type, None))
        assert serializer is not None, ValueError('can\'t serialize value', value, type(value))
        return serializer.serializer

    def _get_vector_serializer(self, vector):
        return tuple((self._get_serializer(value) for value in vector))

    def is_lowerable(self, col_type):
        if isinstance(col_type, TypeAssociationTuple):
            col_type = col_type.type
        assert col_type in self.serializers, ValueError('no serializer specified for queried type', col_type)
        return col_type is str or getattr(col_type, '_lowerable', False)

    def to_lower_field(self, field):
        return self._lower_func(field)

    def concat_ws(self, separator, field1, *fields):
        return f"CONCAT_WS('{separator}', " + ", ".join((field1,) + tuple(fields)) + ")"

    def serialize(self, data):
        # if we're given a list or a tuple
        if isinstance(data, (list, tuple)):
            input_type = type(data)

            # check if they are empty
            if len(data) == 0:
                return input_type()

            # check if it's a vector
            elif not isinstance(data[0], (list, tuple)):
                # return a serialized vector
                serializer = self._get_serializer(data[0])
                return input_type((serializer(value) for value in data))

            # it's a vector of vectors, so serialize all of them individually
            else:
                element_type = type(data[0])
                vector_serializer = self._get_vector_serializer(data[0])
                return input_type((element_type((serializer(value) for serializer, value in zip(vector_serializer, vector))) for vector in data))

        # if we're given some raw element then serialize it directly
        else:
            return self._get_serializer(data)(data)

    def get_columns(self, value):
        '''
        get the database columns required that are required to hold a given value.
        this function can accept a mix of values and types, so you can pass:
            1337
            int
            "hey"
            str
            (1337, str, "bla")
        and the proper type(s) will be inferred.
        '''
        default_type = self._types.get(self.default_type, None)

        if isinstance(value, (list, tuple)):
            assert len(value) > 0, ValueError('can\'t infer types for an empty list')
            # check if it's a list of vectors
            if isinstance(value[0], (list, tuple)):
                value = value[0]

            types = []
            for v in value:
                _type = self.default_type if v is None else (v if isinstance(v, type) else type(v))
                associate_type = self._types.get(_type, default_type)
                assert associate_type is not None, ValueError('unserializable type', _type)
                types.append(TypeAssociationTuple(_type, associate_type))

            return tuple(types)

        else:
            _type = self.default_type if value is None else (value if isinstance(value, type) else type(value))
            associate_type = self._types.get(_type, default_type)
            assert associate_type is not None, ValueError('unserializable type', _type)
            return TypeAssociationTuple(_type, associate_type)

    def get_db_type(self, value):
        '''
        an alias for get_columns.
        '''
        return self.get_columns(value)

# this decorator sets an extra property to the output of the decorated factory - _lowerable.
# this will enable us to "generically" know which fields can be used with a LOWER() modifier.
def lowerable(factory):
    # if it's a function it suffices to add a global variable to it
    if inspect.isclass(factory):
        factory._lowerable = True

    # if it's a functino simply modify it's result
    elif inspect.isfunction(factory):
        setattr(factory, '_lowerable', True)

        def _lowerable_factory_func(*a, **kw):
            o = factory(*a, **kw)
            try:
                setattr(o, '_lowerable', True)
            except AttributeError: # happens with record-classes
                o.__dict__['_lowerable'] = True

            return o

        factory = _lowerable_factory_func

    # otherwise assume factory to be a simple object and just modify it directly
    else:
        try:
            setattr(factory, '_lowerable', True)
        except AttributeError: # happens with record-classes
            factory.__dict__['_lowerable'] = True

    return factory

def new_string_type(type_name, size, field=None, is_lowerable=True):
    assert isinstance(size, int) and size > 0
    field = type_name if field is None else field
    new_type = recordclass(type_name, (field,))
    new_type.size = size
    new_type.field_name = field
    new_type.serializer = lambda f: "'" + getattr(f, f.field_name) + "'"
    new_type.db_type = f"VARCHAR({size})"

    if is_lowerable:
        new_type = lowerable(new_type)

    return new_type

# some special care is required for command line and process path handling as they are strings with an extra large space requirement.
# these types are only useful when creating tables and specifying a table's structure. when we are required to infer one of these
# types we may as well (and we do) infer a string as semantically the resultant queries should look the same.
CommandLine = new_string_type('CommandLine', 15000)
ProcessPath = new_string_type('ProcessPath', 600)
ProcessName = new_string_type('ProcessName', 600)
HostId = new_string_type('HostId', 80)
SecdoProcessId = new_string_type('SecdoProcessId', 80)
LargeString = new_string_type('LargeString', 1000)

DefaultSerializationDict = {
    str : lambda v: f"'{v}'",
    datetime.datetime : lambda v: f"'{v}'",
    int : str,
    float : str,
    CommandLine : CommandLine.serializer,
    ProcessPath : ProcessPath.serializer,
    ProcessName : ProcessName.serializer,
    HostId : HostId.serializer,
    SecdoProcessId : SecdoProcessId.serializer,
}

DefaultDbTypesDict = {
    str : 'VARCHAR(100)',
    int : 'INT',
    float : 'FLOAT',
    datetime.datetime : 'TIMESTAMP',
    CommandLine : CommandLine.db_type,
    ProcessPath : ProcessPath.db_type,
    ProcessName : ProcessName.db_type,
    HostId : HostId.db_type,
    SecdoProcessId : SecdoProcessId.db_type,
    LargeString : LargeString.db_type,
}

def get_default_query_serializer(serializer_class=QuerySerializer):
    return serializer_class(DefaultSerializationDict, DefaultDbTypesDict)

def test():
    test_type = new_string_type("test_type", 1337)
    print("new type summary:", test_type.serializer(test_type("test_string")), test_type.db_type)

    serializer = get_default_query_serializer()

    input_list = [
        "this is a string",
        1337,
        7331.1337,
        dateutil.parser.parse('2017-11-01 07:00:12'),
        ("another string", dateutil.parser.parse('2073-03-13')),
        [("vector #1", 7337), ("vector #2", 1331)]
    ]

    print("serialization:")
    for element in input_list:
        print("\t", element, "->", serializer.serialize(element), ";", serializer.get_columns(element))

    serializer[bytes] = lambda v: f"bytes({v})"
    serializer[type(lambda x: x)] = (lambda v: f"lambda({v})", "func_type")

    print()
    print("types association:")
    for _type, type_rec in serializer.serializers.items():
        print("\t", _type, "->", type_rec.db_type, ";", type_rec.serializer)

if __name__ == "__main__":
    test()

