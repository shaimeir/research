#!/usr/bin/env python3

'''
we will only implement a PostgreSQL cursor for now as we don't intend to execute anything on PostgreSQL
via this facility but just print a query.
'''

import psycopg2
import logging
from . import dbcursors

@dbcursors.connections.register("postgresql")
class PostgreSQLConnection(dbcursors.DbConnection):
    '''
    a PostgreSQL connection adapter using psycopg2
    '''
    def __init__(self, host, user, **attributes):
        super().__init__(host=host, user=user, **attributes)
        self.connection = psycopg2.connect(host=host, password=attributes.get('password', None), user=user, database=attributes.get('database', None))
        self.postgresql_cursor = self.connection.cursor()

    def execute(self, query):
        self.postgresql_cursor.execute(query)
        return self

    def fetchmany(self, count):
        try:
            return self.postgresql_cursor.fetchmany(size=count)
        except psycopg2.ProgrammingError as error:
            if error.args == ('no results to fetch',):
                return []

            raise

    def fetchall(self):
        try:
            return self.postgresql_cursor.fetchall()
        except psycopg2.ProgrammingError as error:
            if error.args == ('no results to fetch',):
                return []

            raise

    def close(self):
        self.connection.close()

class PostgreSQLCursor(dbcursors.DbCursor):
    '''
    PostgreSQL support.
    '''
    def setschema(self, schema):
        self.execute(f'SET SEARCH_PATH = "{schema}";')

@dbcursors.cursors.register("postgresql")
def postgresql_cursor(connection, serializer=None):
    '''
    wrap a connection with a PostgreSQL cursor (extended logic).
    '''
    return dbcursors.Md5SchemeAutoSettingCursor(PostgreSQLCursor(connection, serializer), "db_")

