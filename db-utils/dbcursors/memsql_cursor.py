#!/usr/bin/env python3

import contextlib

from . import dbcursors, mysql_cursor, sql_serialization

class MemsqlQuerySerializer(sql_serialization.QuerySerializer):
    def to_lower_field(self, field):
        # memsql comparisons are case insensitive anyway so we'll not use any modifications on the field
        return field

@dbcursors.connections.register("memsql")
class MemsqlConnection(mysql_cursor.MysqlConnection):
    '''
    a MemSQL connection support. this is fully implemented via MysqlConnection.
    '''
    pass

class RawMemsqlCursor(mysql_cursor.RawMysqlCursor):
    '''
    MemSQL cursor. mostly implemented in RawMysqlCursor.
    '''

    @staticmethod
    def new_serializer():
        return sql_serialization.get_default_query_serializer(MemsqlQuerySerializer)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.temp_tables_registry = []

    def setschema(self, schema):
        super().setschema("db_" + schema)

    def create_temp_table(self, name, columns):
        '''
        memsql doesn't support the features we need for temp tables (usage of joins and sub-queries on temp tables
        is very restricted) and so our temp tables shall be regular tables. we'll clear them on exit.
        for now there's a potential table leak as the tables need to be explicitly deleted.
        we can live with that for now.
        '''
        statement = f"CREATE TABLE {name} ("
        statement += ", ".join((
            f"{col} {col_type}"
            for col, col_type in columns.items()
        ))

        statement += ");"

        self.execute(statement)
        self.fetchall()

        self.temp_tables_registry.append(name)

    def close(self):
        for temp_table in self.temp_tables_registry:
            with contextlib.suppress(Exception):
                self.execute(f"DROP TABLE {temp_table}")
        super().close()


@dbcursors.cursors.register("memsql")
def memsql_cursor(connection, serializer=None):
    '''
    wrap a connection with a MemSQL cursor (extended logic).
    '''
    return dbcursors.Md5SchemeAutoSettingCursor(RawMemsqlCursor(connection, serializer=serializer))

