#!/usr/bin/env python3

import time
import uuid
import contextlib
import re
import datetime
import logging
import sqlparse
import logging
import termcolor
from collections import namedtuple
import hashlib
import getpass
from collections import Iterable

from utilities import option_registry_util
try:
    from . import sql_serialization
except Exception:
    import sql_serialization

# this defines the raw connection abstractions (i.e. the actual Vertica/MemSQL interface or something that just writes to the screen)
connections = option_registry_util.OptionRegistry()

# this identifies transformations and manipluations on a raw cursor (i.e. extrapulating multiple queries
# from a single query for Vertica, for instance)
cursors = option_registry_util.OptionRegistry()

def get_cursor_factory(cursor_type, connection_type=None, do_trace_connection=False):
    '''
    create a cursor and apply the proper modifiers.
    we separate the connection type from the cursor_type so we can have, for example a cursor that prints it's commands
    while it runs with another cursor's modifiers (e.g. multi-table queries like in vertica)
    '''

    connection_type = cursor_type if connection_type is None else connection_type # use the same type for cursors as for modifiers

    def _factory(*args, serializer=None, **kwargs):
        connection = connections.option(connection_type)(*args, **kwargs)
        if do_trace_connection:
            connection = trace_connection(connection)
        return cursors.option(cursor_type)(connection, serializer=serializer) if cursor_type in cursors.choices() else connection

    return _factory

class DbConnection:
    '''
    an abstraction for a database connection.
    '''
    def __init__(self, **attributes):
        self.attributes = attributes

    def execute(self, query):
        raise NotImplementedError()

    def fetchmany(self, count):
        raise NotImplementedError()

    def fetchall(self):
        raise NotImplementedError()

    def close(self):
        self.connection.close() # general default behaviour

    def cancel(self):
        pass

    @contextlib.contextmanager
    def context(self):
        try:
            yield self

        finally:
            self.close()

class DbConnectionWrapper(DbConnection):
    '''
    a wrapper that takes a connection and proxies it's calls.
    '''
    def __init__(self, connection):
        super().__init__()
        self.connection = connection
        # since connection.attributes is a dict, the following line guarantees
        # complete transperancy between self and connection
        self.attributes = connection.attributes

    def execute(self, query):
        self.connection.execute(query)
        return self

    def fetchmany(self, count):
        return self.connection.fetchmany(count)

    def fetchall(self):
        return self.connection.fetchall()

    def close(self):
        self.connection.close()

    def cancel(self):
        self.connection.cancel()

class TracedConnection(DbConnectionWrapper):
    def __init__(self, connection):
        super().__init__(connection)
        self.lastFetchCount = -1

        self.attributes["traced"] = True

    def execute(self, query):
        logging.info("executing query:\n%s", termcolor.colored(sqlparse.format(query, reindent=True, keyword_case='upper'), 'cyan'))
        self.lastFetchCount = -1
        return super().execute(query)

    def fetchmany(self, count):
        if self.lastFetchCount != count:
            logging.info("fetching results results in bulks of %d entries", count)
            self.lastFetchCount = count

        return super().fetchmany(count)

    def fetchall(self):
        logging.info("fetching all results")
        self.lastFetchCount = -1
        return super().fetchall()

    def close(self):
        logging.info("closing connection")
        self.lastFetchCount = -1
        super().close()

    def cancel(self):
        logging.info("canceling pending requests")
        super().cancel()

def trace_connection(connection):
    return TracedConnection(connection)

class DbCursor(DbConnectionWrapper):
    '''
    an abstraction for a cursor - i.e. a database connection with rich functionality.
    '''

    @staticmethod
    def new_serializer():
        return sql_serialization.get_default_query_serializer()

    def __init__(self, connection, serializer=None):
        super().__init__(connection)
        self.resource_pool_set = False
        self.set_resource_pool()

        serializer = self.new_serializer() if serializer is None else serializer
        self.serializer = serializer
        self.attributes["serializer"] = serializer

    def setschema(self, schema):
        raise NotImplementedError()

    def copycsv(self, table, csv_buffer):
        raise NotImplementedError()

    def set_resource_pool(self):
        # since this method is called from the constructor and we use multiple wrappers we
        # it will be called multiple times and therefor it's better to have a flag to limit
        # the actual assignments of the resource pool
        if not self.resource_pool_set:
            self._do_set_resource_pool()

        self.resource_pool_set = True

    def _do_set_resource_pool(self):
        pass

    def create_temp_table(self, name, columns):
        raise NotImplementedError('create_temp_table() is not implemented for this class', type(self))

    def commit(self):
        self.execute("commit")

    class _ValueSerializer:
        '''
        a utility class to be used in bulk_insert().
        it basically allows for the basic join() facility appropriately.
        '''
        def __init__(self, serializer):
            self.serializer = serializer

            self.dict_order = None
            self.serializer_method = None

        def _serialize_value(self, value):
            return self.serializer.serialize(value)

        def _serialize_iterable(self, value):
            return ", ".join((self.serializer.serialize(v) for v in value))

        def _serialize_dict(self, value):
            return self._serialize_iterable((value[k] for k in self.dict_order))

        def infer_serialization_format(self, sample_value):
            if isinstance(sample_value, dict):
                self.dict_order = tuple(sample_value.keys())
                self.serializer_method = self._serialize_dict

            elif isinstance(sample_value, Iterable) and not isinstance(sample_value, (str, bytes)):
                self.serializer_method = self._serialize_iterable

            else:
                self.serializer_method = self._serialize_value

        def serialize(self, value):
            return "(" + self.serializer_method(value) + ")"

        @property
        def columns_order_statement(self):
            return "" if self.dict_order is None else "(" + ", ".join(self.dict_order) + ")"

    def bulk_insert(self, table, values):
        assert self.serializer is not None, ValueError('can\'t serialize data, missing serializer')

        # make an iterable out of each value in case it's not a tuple or something like that (when we insert a single column)
        if not isinstance(values, (list, tuple)):
            values = list(values)

        if len(values) == 0:
            return

        bulk_size = 100

        value_serializer = self._ValueSerializer(self.serializer)
        value_serializer.infer_serialization_format(values[0])

        for bulk_index in range((len(values) + bulk_size - 1)//bulk_size):
            values_bulk = values[bulk_index * bulk_size: (bulk_index + 1) * bulk_size]
            statement = f"INSERT INTO {table} {value_serializer.columns_order_statement} VALUES " + ", ".join((value_serializer.serialize(value) for value in values_bulk)) + ";\n"
            self.execute(statement)
            self.fetchall()

class DbCursorWrapper(DbCursor):
    '''
    guess what this does...
    '''
    def __init__(self, cursor):
        self.cursor = cursor # assign bufore construction so DbCursor.__init__() can call set_resource_pool()
        super().__init__(cursor, cursor.attributes.get("serializer", None))

    def setschema(self, schema):
        self.cursor.setschema(schema)

    def copycsv(self, table, csv_buffer):
        self.cursor.copycsv(table, csv_buffer)

    def set_resource_pool(self):
        self.cursor.set_resource_pool()

    def create_temp_table(self, name, columns):
        self.cursor.create_temp_table(name, columns)

    def bulk_insert(self, table, values):
        self.cursor.bulk_insert(table, values)

class TracedCursor(DbCursorWrapper):
    def __init__(self, cursor):
        super().__init__(cursor)
        self.lastFetchCount = -1

    def execute(self, query):
        logging.info("executing query:\n%s", termcolor.colored(sqlparse.format(query, reindent=True, keyword_case='upper'), 'cyan'))
        self.lastFetchCount = -1
        return super().execute(query)

    def setschema(self, schema):
        logging.info("setting schema to '%s'", schema)
        self.lastFetchCount = -1
        return super().setschema(schema)

    def fetchmany(self, count):
        if self.lastFetchCount != count:
            logging.info("fetching results results in bulks of %d entries", count)
            self.lastFetchCount = count

        return super().fetchmany(count)

    def fetchall(self):
        logging.info("fetching all results")
        self.lastFetchCount = -1
        return super().fetchall()

    def copycsv(self, table, csv_buffer):
        logging.info("copying CSV (%d bytes) to '%s'", len(csv_buffer), table)
        self.lastFetchCount = -1
        return self.cursor.copycsv(table, csv_buffer)

    def set_resource_pool(self):
        logging.info("setting resource pool")
        super().set_resource_pool()

    def close(self):
        logging.info("closing cursor")
        self.lastFetchCount = -1
        super().close()

    def cancel(self):
        logging.info("canceling pending requests")
        super().cancel()

    def create_temp_table(self, name, columns):
        logging.info("creating local table: %s (%r)", name, columns)
        super().create_temp_table(name, columns)

    def bulk_insert(self, table, values):
        logging.info("bulk insertion of %d entries into %s", len(values), table)
        super().bulk_insert(table, values)

class MultiGroupCursor(DbCursorWrapper):
    _TableReplEntry = namedtuple('_TableReplEntry', ('re', 'table'))

    CompanySizes = {
        'default'   : 128,
        'large'     : 128,
        'medium'    : 16,
        'small'     : 1,
    }

    DefaultMultiGroupTables = [
        "code_injection_events",
        "file_system_events",
        "module_events",
        "network_events",
        "performance_monitor",
        "registry_events"
    ]

    def __init__(self, cursor):
        super().__init__(cursor)

        default_groups = list(range(self.get_company_groups_count(self.attributes.get("size", "default"))))
        self.groups = self.attributes.get("groups", default_groups)

        self.multiGroupTables = [
            self._TableReplEntry(re.compile(r"\b" + table + r"\b"), table)
            for table in self.attributes.get("multi_group_tables", MultiGroupCursor.DefaultMultiGroupTables)
        ]

        self.activeQuery = None

        self.attributes['multi_group'] = True

    def get_company_groups_count(self, size='default'):
        try:
            return MultiGroupCursor.CompanySizes[size]
        except Exception:
            return int(size)

    def _createQueryGenerator(self, query):
        def _basic_gen(basic_query):
            yield basic_query

        def _table_modifier_generator(subgen, table, groups):
            for sub_query in subgen:
                if len(table.re.findall(sub_query)) == 0:
                    yield sub_query
                else:
                    for group in self.groups:
                        yield table.re.sub(table.table + f"_group_{group}", sub_query)

        query_gen = _basic_gen(query)
        for table in self.multiGroupTables:
            query_gen = _table_modifier_generator(query_gen, table, self.groups)

        return query_gen

    def execute(self, query):
        '''
        create a query generator (multiplexing the split table) and execute it's first generated query.
        subsequent generated queries will be executed on the fetch*() methods.
        '''
        self.activeQuery = iter(self._createQueryGenerator(query))
        self._advance()
        return self

    def _advance(self):
        if self.activeQuery is None:
            return False

        try:
            super().execute(next(self.activeQuery))

        except StopIteration:
            self.activeQuery = None
            return False

        return True

    def fetchmany(self, count):
        if self.activeQuery is None:
            return []

        results = []
        while len(results) < count:
            # make sure to first fetch and only then call _advance() as the execute() method called the _advance()
            # before we got into this function.
            results += super().fetchmany(count - len(results))

            if len(results) < count and not self._advance():
                break

        return results

    def fetchall(self):
        results = super().fetchall()

        while self._advance():
            results += super().fetchall()

        return results

    def cancel(self):
        if self.activeQuery is None:
            return

        super().cancel()
        r = super().fetchall()
        self.activeQuery = None

def trace_cursor(cursor):
    return TracedCursor(cursor)

# for backwards compatibility...
tracecursor = trace_cursor

class Md5SchemeAutoSettingCursor(DbCursorWrapper):
    '''
    usually (at least in Vertica) we'd need to set the schema to the md5 of the company name.
    this can be done automatically via this class.
    '''
    def __init__(self, cursor, prefix=""):
        super().__init__(cursor)
        schema = self.attributes['schema'] if 'schema' in self.attributes else prefix + hashlib.md5(self.attributes['company'].encode()).hexdigest()
        super().setschema(schema)

class DummyDbCursor(DbCursor):
    def setschema(self, schema):
        self.execute(f"SELECT 'DUMMY SCHEMA = {schema}'")

    def copycsv(self, table, csv_buffer):
        pass

    def create_temp_table(self, name, columns):
        self.execute(f"SELECT 'CREATE TEMP TABLE {name} {columns}'")

def test():
    import printer_cursor
    dist_cursor = MultiGroupCursor(DummyDbCursor(printer_cursor.PrintingCurosr(groups=[1,2,3], multi_group_tables=("alpha", "beta", "gamma"))))
    dist_cursor.execute("""
        SELECT
            *
        FROM
            (
                SELECT * FROM alpha
                UNION
                SELECT * FROM beta
                UNION
                SELECT * FROM gamma
            )
            WHERE
                alphacol=1 AND
                betacol=2 AND
                gammacol=3 AND
    """)
    dist_cursor.fetchall()

if __name__ == "__main__":
    test()

