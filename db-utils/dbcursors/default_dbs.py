#!/usr/bin/env python3

import configobj
import argparse
import pathlib
import sys
import json
from collections import defaultdict

DefaultDatabasesFile = pathlib.Path("~/.secdo-dbs").expanduser()

def load_defaults(defaults_file=DefaultDatabasesFile, *sections):
    config = configobj.ConfigObj(str(pathlib.Path(defaults_file).expanduser()))
    for section in sections:
        if section not in config.sections:
            return {}
        config = config[section]

    strip_vals = lambda d: {k.strip(): v.strip() for k, v in d.items()}
    return {section: strip_vals(dict(config[section])) for section in config.sections}

def add_standard_command_line_arg(parser, *switches, long_switch=False):
    if len(switches) == 0:
        switches = ["--db-config"] if long_switch else ["-c", "--db-config"]

    return parser.add_argument(*switches,
                               nargs="+",
                               action="append",
                               default=[],
                               help="databases configuration file (and any specific sub-section of it).").dest

def load_dbs_from_command_line(*switches, args=None, long_switch=False):
    if args is None:
        args = sys.argv[1:]

    args = [a for a in args if a not in ('-h', '--help')]

    parser = argparse.ArgumentParser()
    field = add_standard_command_line_arg(parser, *switches, long_switch=long_switch)
    config_file_descs = getattr(parser.parse_known_args(args)[0], field)
    if len(config_file_descs) == 0:
        config_file_descs.append([DefaultDatabasesFile])

    databases = defaultdict(dict)
    for config_file_desc in config_file_descs:
        databases_batch = load_defaults(*config_file_desc)
        for db_name, db_desc in databases_batch.items():
            databases[db_name].update(db_desc)

    return dict(databases)

def main():
    parser = argparse.ArgumentParser(description="load the databases found in a configuration file")
    add_standard_command_line_arg(parser)
    parser.parse_args()
    print(json.dumps(load_dbs_from_command_line(), indent=4))

if __name__ == "__main__":
    sys.exit(main())

