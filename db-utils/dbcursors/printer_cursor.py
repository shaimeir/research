#!/usr/bin/env python3

'''
defines a database connection implementation that doesn't plug into a real database but just
writes the queries it's supposed to execute to stdout.
'''

import sqlparse

try:
    from . import dbcursors
except Exception:
    import dbcursors

@dbcursors.connections.register("print")
class PrintingCurosr(dbcursors.DbConnection):
    def execute(self, query):
        print(sqlparse.format(query + ";", reindent=True, keyword_case='upper'))

    def fetchmany(self, count):
        return []

    def fetchall(self):
        return []

    def close(self):
        pass

