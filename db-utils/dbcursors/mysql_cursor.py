#!/usr/bin/env python3

'''
the mysql cursor module to plug in with the dbcursors.py abstractions.
this is not actually in use directly, but rather used by the memsql driver as a code base.
'''

import logging
import MySQLdb # supplied by the mysqlclient package for python 3

from . import dbcursors

class MysqlConnection(dbcursors.DbConnection):
    '''
    a MySQL connection support (this is mostly here for MemSQL which conforms to MySQL's API).
    '''
    def __init__(self, host, user, password, **attributes):
        super().__init__(host=host, user=user, password=password, **attributes)
        new_attrs = {}
        if 'compress' in attributes:
            new_attrs['compress'] = int(attributes['compress'])
        self.mysql_connection = MySQLdb.connect(host=host, user=user, password=password, **new_attrs)
        self.mysql_connection.set_character_set("UTF8")
        self.mysql_cursor = self.mysql_connection.cursor(MySQLdb.cursors.DictCursor)

    def execute(self, query):
        self.mysql_cursor.execute(query)
        return self

    def fetchmany(self, count):
        return self.mysql_cursor.fetchmany(count)

    def fetchall(self):
        return self.mysql_cursor.fetchall()

    def close(self):
        self.mysql_connection.close()

class RawMysqlCursor(dbcursors.DbCursor):
    '''
    extended MySQL functionality.
    '''
    def setschema(self, schema):
        self.execute(f'use "{schema}"')
        self.fetchall()

    def _do_set_resource_pool(self):
        pass

    def create_temp_table(self, name, columns):
        statement = f"CREATE TEMPORARY TABLE {name} ("
        statement += ", ".join((
            f"{col} {col_type}"
            for col, col_type in columns.items()
        ))

        statement += ");"

        self.execute(statement)
        self.fetchall()

