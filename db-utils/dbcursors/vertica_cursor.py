#!/usr/bin/env python3

'''
the vertica cursor module to plug in with the dbcursors.py abstractions
'''

import vertica_python
import io
import logging

from . import dbcursors, sql_serialization

class VerticaQuerySerializer(sql_serialization.QuerySerializer):
    def concat_ws(self, separator, field1, *fields):
        fields = (field1,) + tuple(fields)
        with_sep = sum(((field, f"'{separator}'") for field in fields), tuple())[:-1]
        # vertica doesn't support concat_ws()
        return ' || '.join(with_sep)

class RawVerticaCursor(dbcursors.DbCursor):
    '''
    access Vertica a vertica connection.
    this class doesn't handle the multigroup part (look at vertica_cusrsor() for the complete construction).
    '''

    @staticmethod
    def new_serializer():
        return sql_serialization.get_default_query_serializer(VerticaQuerySerializer)

    def setschema(self, schema):
        self.execute(f'SET SEARCH_PATH TO "{schema}"')

    def copycsv(self, table, csv_buffer):
        csv_stream = io.BytesIO(csv_buffer.encode() if isinstance(csv_buffer, str) else csv_buffer)
        # access a proprietary VerticaConnection method.
        self.connection.copy(f"COPY {table} FROM STDIN DELIMITER ',' ESCAPE AS '\\'", csv_stream, len(csv_buf))
        self.fetchall()

    def create_temp_table(self, name, columns):
        statement = f"CREATE LOCAL TEMPORARY TABLE {name} ("
        statement += ", ".join((
            f"{col} {col_type}"
            for col, col_type in columns.items()
        ))

        statement += ") ON COMMIT PRESERVE ROWS;"

        self.execute(statement)
        self.fetchall()

    def bulk_insert(self, table, values):
        # check if we have a vertica connection in our hands
        if self.connection.attributes.get('vertica_copy_csv', False):
            if len(values) == 0:
                return

            logging.warn("RawVerticaCursor.bulk_insert() hasn't been tested! make sure to test run it if you're going to use it")

            bulk_size = 100

            # make an iterable out of each value in case it's not a tuple or something like that (when we insert a single column)
            join_cols = (lambda v: v) if isinstance(values[0], Iterable) and not isinstance(values[0], str) else (lambda v: ",".join(map(str, v)))
            self.copycsv(table, "\n".join((join_cols(value) for value in values)))

        # we're using a non-vertica connection (probably a printer connection) so we should call the generic insertion method
        else:
            super().bulk_insert(table, values)

    def _do_set_resource_pool(self):
        self.execute("SET SESSION RESOURCE_POOL = quick_queries")
        self.fetchall()

@dbcursors.connections.register("vertica")
class VerticaConnection(dbcursors.DbConnection):
    def __init__(self, host, user, password, database, **attributes):
        super().__init__(host=host, user=user, password=password, database=database, **attributes)
        self.vertica_connection = vertica_python.connect(host=host, user=user, password=password, database=database).cursor('dict')

        # flag support for csv copying
        self.attributes['vertica_copy_csv'] = True

    def execute(self, query):
        self.vertica_connection.execute(query)
        return self

    def fetchmany(self, count):
        return self.vertica_connection.fetchmany(count)

    def fetchall(self):
        return self.vertica_connection.fetchall()

    def close(self):
        self.vertica_connection.close()

    # proprietary vertica function
    def copy(self, copy_directive, csv_stream, buffer_size):
        self.vertica_connection.copy(copy_directive, csv_stream, buffer_size=buffer_size)

@dbcursors.cursors.register("vertica")
def vertica_cursor(connection, serializer=None):
    '''
    wrap a connection with a vertica cursor (extended logic).
    '''
    return dbcursors.MultiGroupCursor(dbcursors.Md5SchemeAutoSettingCursor(RawVerticaCursor(connection, serializer)))

