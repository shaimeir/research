#!/usr/bin/env python3

import sys
import argparse

from dbcursors import default_dbs
from utilities import option_registry_util

class SimpleProperty:
    def __init__(self, prop_name):
        self.prop_name = prop_name
        self.__doc__ = f"""
            get the '{prop_name}' property
        """.strip()

    def __call__(self, db_name, db_desc):
        return db_desc[self.prop_name]

properties = option_registry_util.OptionRegistry()

@properties.register("name")
def db_name(db_name, db_desc):
    return db_name

def main():
    dbs = default_dbs.load_dbs_from_command_line()

    simple_properties = {prop for db_desc in dbs.values() for prop in db_desc.keys()}
    for prop in simple_properties:
        properties.register_option(prop, SimpleProperty(prop))

    parser = argparse.ArgumentParser(description="prints certain properties for given recipe files")
    parser.add_argument("-p", "--property", action="append", default=[], choices=properties.choices(), help="what to print.")
    parser.add_argument("database", choices=dbs.keys(), help="the database to print the property for.")
    default_dbs.add_standard_command_line_arg(parser)

    args = parser.parse_args()

    db_name = args.database
    db_desc = dbs[db_name]

    for prop in args.property:
        _prop = properties.option(prop)(db_name, db_desc)
        if isinstance(_prop, (list, tuple)):
            print("\n".join(map(str, _prop)))
        else:
            print(_prop)

if __name__ == "__main__":
    sys.exit(main())

