#!/usr/bin/env python3

'''
auto-generate an SQL query to dump data from the backend's database with various filters and augmentations.
'''

import sys
import logging
import csv
import tempfile
import contextlib
import os

import scheme
import dbcursors
from dbcursors import default_dbs
from utilities import argparse_config_file_support

import conditions_base
import query_elements
import sql_base
import conditions
import alerts_conditions
import augmentors

def scheme_to_selection(scheme):
    table = query_elements.SelectionTable(scheme.db_table)
    for col_name, col_desc in scheme.columns.items():
        table.add_column(col_desc.db_column, col_name, col_desc.type)

    return table

def build_selection_query(scheme, conditions_list, join_clauses):
    table = scheme_to_selection(scheme)
    selection_query = query_elements.Selection(table)

    logging.debug("adding conditions: %r", conditions_list)
    selection_query.add_join_clause(*join_clauses)
    selection_query.add_condition(*conditions_list)

    logging.debug("adding augmentors")

    return selection_query

def dump_scheme(cursor, output_csv, scheme, conditions_list, temp_tables, join_clauses, use_header, use_marker, marker_text):
    logging.info("creating temp tables")
    for temp_table in temp_tables:
        table_ops = sql_base.TableOperations(temp_table)
        cursor.create_temp_table(*table_ops.creation_specs)
        cursor.bulk_insert(temp_table.name, table_ops.insertion_specs(serialize=False)) # bulk_insert already performs serialization
        cursor.execute("COMMIT")

    logging.info("building selection query")
    selection_query = build_selection_query(scheme, conditions_list, join_clauses)

    if use_marker:
        logging.info("generating marker: %s", marker_text)
        marker_text = tempfile.mktemp(prefix=marker_text+"_", dir='')
        cursor.execute(f"SELECT '{marker_text}'")
        marker = cursor.fetchall()
        logging.info("marker query returned: %r", marker)

    if use_header:
        logging.info("generating CSV header")
        cursor.execute("SELECT " + ", ".join((
            col.as_col.name_statement for col in selection_query.columns
        )))

    logging.info("executing query!")
    query_counter = 1
    for _ in iter(selection_query):
        logging.debug("executing query #%d", query_counter)
        cursor.execute(selection_query.query)

    logging.info("fetching data!")
    sample_printed = False

    while True:
        logging.debug("fetching results...")
        results = cursor.fetchmany(1000)
        if len(results) == 0:
            break

        if order_is_valid is None:
            logging.debug("evaluating columns order")
            res = results[0]
            order_is_valid = False

            # if the package returns an ordered dict or it's python3.6 then the resultant dict is ordered so we can verify
            # it's order and save some lookup time
            if isinstance(res, OrderedDict) or (sys.version_info.major >= 3 and sys.version_info.minor >= 6):
                order_is_valid = tuple(res.keys()) == columns

            loggind.debug("order validity: %r", order_is_valid)

        if not sample_printed and len(results) > 0:
            sample_printed = True
            logging.info("sample output: %r", results[0])

        logging.debug("writing rows")
        if order_is_valid:
            output_stream.writerows((line.values() for line in results))
        else:
            output_stream.writerows(([line[col] for col in columns] for line in results))

    logging.info("dump query finished")

@contextlib.contextmanager
def auto_yield(o):
    yield o

def main():
    pre_set_logging = os.environ.get("DEBUG_LOG") is not None

    if pre_set_logging:
        logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s", level=logging.DEBUG)

    db_defaults = default_dbs.load_dbs_from_command_line(long_switch=True)

    parser = argparse_config_file_support.ConfigSupportedArgumentParser(description="dump company information for a specific scheme to a CSV file.")
    default_dbs.add_standard_command_line_arg(parser, long_switch=True)

    parser.add_argument("-d", "--db", required=True, choices=list(db_defaults.keys()), help="the database to dump the information from.")
    parser.add_argument("-o", "--output", default='-', help="the output CSV file. use '-' for stdout.")
    parser.add_argument("-v", "--verbose", action="store_true", help="show debug prints.")
    parser.add_argument("-V", "--very-verbose", action="store_true", help="show extra debug prints.")
    parser.add_argument("-s", "--scheme", required=True, help="the scheme to dump.\navailable defaults are: " + ", ".join(scheme.get_default_schemes_list()))
    parser.add_argument("-k", "--skip-augmentor", default=[], action='append', choices=augmentors.get_augmentors_list())
    parser.add_argument("-n", "--no-augmentation", action='store_true', help="don't augment output at all")
    parser.add_argument("-x", "--execute", action='store_true', help='by default queries are printed to the output stream. use this switch to actually connect to the db and execute them.')
    parser.add_argument("--no-header", action='store_true', help='don\'t yield a header line in the output.')
    parser.add_argument("--no-marker", action='store_true', help="don't yield a marker query int the output.")
    parser.add_argument("--marker-text", default="SCHEME_DUMP_MARKER", help="the text prefix to use as a marker line.")
    parser.add_argument("-c", "--comma-separation", action="store_true", help="use comma to separate the output")

    conditions.apply_condition_switches(parser)

    args = parser.parse_args()

    if not pre_set_logging:
        logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s", level=logging.DEBUG if args.very_verbose else logging.INFO if args.verbose else logging.WARN)

    db = db_defaults[args.db]
    table_scheme = scheme.load_default_scheme_file(args.scheme)

    assert table_scheme.db_table is not None, ValueError('scheme isn\'t tied to a database table', args.scheme)

    # create the augmentors
    augmentors_list = [] if args.no_augmentation else [aug_name for aug_name in augmentors.get_augmentors_list() if aug_name not in args.skip_augmentor]
    required_schemes_map = {
        aug_name : schemes_set
        for aug_name, schemes_set in augmentors.get_augmentors_schemes().items()
    }

    required_schemes = {scheme for schemes_set in required_schemes_map.values() for scheme in schemes_set}
    loaded_schemes = {
        scheme_name : scheme.load_default_scheme_file(scheme_name)
        for scheme_name in required_schemes
    }
    assert None not in loaded_schemes.values(), NameError("couldn't load a specified scheme", tuple((scheme_name for scheme_name, scheme in loaded_schemes.items() if scheme is None)))

    scheme_arguments = {
        scheme_name.replace("-","_") : scheme for scheme_name, scheme in loaded_schemes.items()
    }

    required_schemes_args_map = {
        aug_name : {scheme_name.replace("-", "_") for scheme_name in schemes_set}
        for aug_name, schemes_set in required_schemes_map.items()
    }

    for aug_name in augmentors_list:
        augmentors.get_augmentor(aug_name)(schemes=loaded_schemes,
                                            **{
                                                scheme_arg_name : scheme_arguments[scheme_arg_name]
                                                for scheme_arg_name in required_schemes_args_map[aug_name]
                                            })

    cursor = dbcursors.get_cursor_factory(db['dbtype'], db['dbtype'] if args.execute else 'print')(
        password=(db['password'] if 'password' in db else getpass.getpass('Database password: ')) if args.execute else '',
        **db
    )

    sql_base.set_serializer(cursor.serializer)

    # now we can build the augmentors
    with ((auto_yield(sys.stdout) if '-' == args.output else (open(args.output, "w")) if args.execute else auto_yield(io.StringIO()))) as output_stream:
        output_csv = csv.writer(output_stream, delimiter="," if args.comma_separation else "\t", quoting=csv.QUOTE_MINIMAL, dialect='unix')
        with cursor.context():
            dump_scheme(dbcursors.trace_cursor(cursor),
                        output_csv,
                        table_scheme,
                        conditions.conditions(),
                        conditions.temp_tables(),
                        conditions.join_clauses() + augmentors.join_clauses(),
                        not args.no_header,
                        not args.no_marker,
                        args.marker_text)

if __name__ == "__main__":
    with contextlib.suppress(BrokenPipeError):
        sys.exit(main())

    sys.exit(1) # broken pipe...

