#!/usr/bin/env python3

import logging
import csv
import pathlib
import sys
import tempfile
import contextlib
import datetime
import re
import dateutil.parser
from collections import defaultdict

import sql_base
import query_elements
import conditions_base

from utilities import switches_registry_util

condition_registry = sql_base.CommonConditionRegistry()
temp_table_registry = sql_base.TableRegistry(tempfile.mktemp(dir="", prefix=""))
join_clause_registry = sql_base.JoinClauseRegistry()

condition_switches = switches_registry_util.registrator()

def conditions():
    return condition_registry.conditions

def temp_tables():
    return temp_table_registry.tables

def join_clauses():
    return join_clause_registry.join_clauses

def apply_condition_switches(arg_parser):
    condition_switches.apply(arg_parser)

### timestamp filter

def timestamp_parser(timestamp):
    """
    extended parsing facility for timestamps - allow for both standard timestamp syntax and a relational syntax.
    standard format: "2017-11-01 01:30:00" and you can drop the hours if you want midnight.
    relational time: "2y3m1d4H5M6S" means 1 year, 3 months, 1 day, 4 hours, 5 minutes and 6 seconds before now.
                     you don't have to specify all of these compnents.
    now: just use "now".
    today: to get today's date with the hour set to midnight use "today".
    """

    if "now" == timestamp:
        return datetime.datetime.now()

    if "today" == timestamp:
        return datetime.datetime.combine(datetime.datetime.now().date(), datetime.time())

    # try and parse the argument as a standard timestamp
    with contextlib.suppress(Exception):
        return dateutil.parser.parse(timestamp)

    # parse relative time
    relative_quantities_letters = {
        'y' : 'years',
        'm' : 'months',
        'w' : 'weeks',
        'd' : 'days',
        'H' : 'hours',
        'M' : 'minutes',
        'S' : 'secinds',
    }
    quantities_descriptors = [desc for desc in re.split(r"(\d+[" + "".join(relative_quantities_letters.keys()) + "])", timestamp) if len(desc) > 0]
    relative_time = defaultdict(int)
    for desc in quantities_descriptors:
        assert len(desc) > 1, ValueError("invalid relative timestamp argument", desc, timestamp)
        quantity_type = desc[-1]
        assert quantity_type in relative_quantities_letters, ValueError("invalid quantity type", quantity_type, timestamp)
        try:
            value = int(desc[:-1])
        except Exception:
            raise ValueError("invalid relative time value", desc[:-1], quantity_type, timestamp)

        relative_time[relative_quantities_letters[quantity_type]] += value

    time_delta = dateutil.relativedelta.relativedelta(**relative_time)
    return datetime.datetime.combine(datetime.datetime.now().date(), datetime.time()) - time_delta

timestamp_condition = None

@condition_switches.register("-b", "--before", type=timestamp_parser)
def before_switch(before_timestamp):
    '''
    add a time upper bound filter
    '''
    global timestamp_condition
    if timestamp_condition is None:
        timestamp_condition = condition_registry.RangeCondition("timestamp", table="main")

    timestamp_condition.high = before_timestamp

@condition_switches.register("-a", "--after", type=timestamp_parser)
def after_switch(after_timestamp):
    '''
    add a time lower bound filter
    '''
    global timestamp_condition
    if timestamp_condition is None:
        timestamp_condition = condition_registry.RangeCondition("timestamp", table="main")

    timestamp_condition.low = after_timestamp

### process name condition
process_name_condition = None

@condition_switches.register("--pname", action='append')
def proc_name_filter(proc_name):
    '''
    limit the query to certain processes.
    requires the process-info augmentation.
    '''
    global process_name_condition
    if process_name_condition is None:
        process_name_condition = condition_registry.InCondition("tables.process.aliases.process_name")

    process_name_condition.add_value(proc_name)

@condition_switches.register("--pname-file", action='append')
def proc_name_file_filter(proc_name_file):
    '''
    limit the query to certain processes.
    read process name list from a file.
    requires the process-info augmentation.
    '''
    global process_name_condition
    if process_name_condition is None:
        process_name_condition = condition_registry.InCondition("tables.process.aliases.process_name")

    if 'standard' == proc_name_file:
        proc_name_file = pathlib.Path(__file__).parent/'standard-processes'

    with open(proc_name_file, "rt") as proc_file:
        process_name_condition.add_value(*[p.strip() for p in proc_file])

### instance condition

instances_table = None
instances_join_clause = None

def _init_instances_table():
    global instances_table
    global instances_join_clause

    if instances_table is not None:
        return

    instances_table = temp_table_registry.joint_table('instances')
    instances_table.add_on_column('host', col_type=str)
    instances_table.add_on_column('process_id', col_type=str)
    instances_table.add_on_column('instance', col_type=int)

    instances_join_clause = join_clause_registry.join_clause(instances_table, strict=True)
    instances_join_clause.add_on_condition(conditions_base.EqCondition('tables.main.aliases.host', 'tables.join.host'))
    instances_join_clause.add_on_condition(conditions_base.EqCondition('tables.main.aliases.process_id', 'tables.join.process_id'))
    instances_join_clause.add_on_condition(conditions_base.EqCondition('tables.main.aliases.instance', 'tables.join.instance'))

@condition_switches.register("--instance", action='append')
def instance_filter(instance_tuple):
    '''
    limit the queries to certain instances.
    use the format: <host-id>,<secdo-pid>,<instance-id>
    '''
    _init_instances_table()

    host, pid, instance = instance_tuple.split(",")
    instance = int(instance)
    instances_table.insert({
        'host' : host,
        'process_id' : pid,
        'instance' : instance
    })

@condition_switches.register("--instances-file", action='append', type=pathlib.Path)
def instance_file_filter(instances_csv_filename):
    '''
    limit the queries to certain instances.
    load the list of instances from a tab-separated CSV file where the column order is:
        * host id
        * secdo pid
        * instance id
    '''
    _init_instances_table()

    with open(instances_csv_filename, "rt") as instances_csv:
        instances = [
            {
                'host' : row[0],
                'process_id' : row[1],
                'instance' : int(row[2])
            }
            for row in csv.reader(instances_csv, delimiter="\t")
        ]

        instances_table.insert(*instances)

### break query by host id

host_id_breakage_condition = None

def _generate_suffix(suffix_len):
    if suffix_len <= 0:
        yield ""
        return

    for suffix in _generate_suffix(suffix_len - 1):
        for c in "0123456789abcdef":
            yield c + suffix

@condition_switches.register("--break-by-host", type=int)
def break_by_host(host_count):
    assert host_count in (16, 256), ValueError('currently host breakage is only supported for 16 and 256', host_count)

    global host_id_breakage_condition
    assert host_id_breakage_condition is None, AssertionError('--break-by-host used multiple times')

    suffix_len = {
        16 : 1, # log(16, base=16)
        256 : 2, # log(256, base=16)
    }[host_count]

    host_id_breakage_condition = condition_registry.GeneratedFormatCondition((
        f"RIGHT({{tables.main.aliases.host}}, {suffix_len}) = '{suffix}'" for suffix in _generate_suffix(suffix_len)
    ))

### temp table test switch

@condition_switches.register("--test-temp-table", action="append")
def test_temp_table_switch(table_name):
    table = temp_table_registry.table(table_name)
    table.add_column("instanceId", "instance", int)
    table.add_column("hostId", "host", str)

    table.insert({'instance' : 1337, 'host' : 'host-1'}, {'instance' : 1338, 'host' : 'host-2'})

### exact field condition

@condition_switches.register("--equal", action='append')
def equality_condition(value_equivalence):
    '''
    use the syntax:
        --equal field=value

    to add the following filter:
        main_table.field = 'value'
    '''
    field, value = value_equivalence.split("=", 1)
    condition_registry.EqCondition(f"tables.main.aliases.{field}", value, right_is_col=False)

    return value_equivalence

