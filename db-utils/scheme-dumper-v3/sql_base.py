#!/usr/bin/env python3

'''
base facilities to create standard sql filters.
this mostly ties the classes and objects defined in query_elements.py with the SQL serialization
facilities required and supplied by the dbcursors facility.
'''

import sys
import logging

try:
    from . import query_elements
    from .dbcursors import sql_serialization
except Exception:
    import query_elements
    from dbcursors import sql_serialization

# by default use the base sql serializer class. an application would have to set this global to the correct serializer.
_global_serializer = sql_serialization.get_default_query_serializer()

def set_serializer(new_serializer):
    _global_serializer = new_serializer

class TableOperations:
    '''
    this class wraps a query_elements.SelectionTable instance and provides creation and insertion facilities.
    these features use the sql serialization facilities.

    the operations are:
        * creation specs - accessible via the creation_sepcs property. generates the arguments to pass to dbcursors.DbCursor.create_temp_table().
        * insertion_specs - serializes data to pass to dbcursors.DbCursor.bulk_insert()
    '''

    def __init__(self, table, serializer=None):
        '''
        arguments:
            * table - a query_elements.SelectionTable instance to manage.
            * serializer - an optional SQL serializer instance. if None, the global serializer will be used.
        '''
        self._table = table
        self._serializer = serializer

    @property
    def serializer(self):
        global _global_serializer
        return _global_serializer if self._serializer is None else self._serializer

    @property
    def table(self):
        return self._table

    @property
    def creation_specs(self):
        '''
        generate the arguments a db cursor (defined by the dbcursors.dbcursors interface) accepts in it's table creation method.
        '''
        column_creation_spec = {
            col.raw_name : self.serializer.get_columns(col.type).db_type for col in self._table.get_all_columns()
        }

        return self._table.name, column_creation_spec

    class _serializer_wrapper:
        def __init__(self, _serializer, _type, col_name, col_alias, serialize):
            self._serializer = _serializer
            self._type = _type
            self.col_name = col_name
            self.col_alias = col_alias
            self._serialize = serialize

        def serialize(self, value):
            if not isinstance(self._type, type):
                value = self._type(value) # in case _type isn't a type, always call it
            elif not isinstance(value, self._type):
                value = self._type(value)

            if self._serialize:
                value = self._serializer.serialize(value)

            return value

        def serialize_from_row(self, row):
            if self.col_name in row:
                value = row[self.col_name]
            elif self.col_alias in row:
                value = row[self.col_alias]
            else:
                raise KeyError("couldn't find column in data", self.col_name, self.col_alias, row)

            return self.serialize(value)

    def insertion_specs(self, data=None, serialize=True):
        '''
        serialize a list of content dicts to the form used by dbcursors.DbCursor.bulk_insert().

        arguments:
            * data - a list of dictionaries that map from column name to value.

            e.g:
                data = [
                    {
                        'column1' : 'value1',
                        'column2 : 1337, # value 2
                    }
                ]

            data will be serialized into tuples by the table's column order.
        '''

        if data is None:
            data = self._table.table_data

        columns_order = tuple((col for col, _ in self._table.all_items()))
        serializers = {
            col_name : self._serializer_wrapper(self.serializer, column.type, col_name, column.alias, serialize)
            for col_name, column in self._table.all_items()
        }

        serialized_content = [
            tuple((serializers[col_name].serialize_from_row(instance) for col_name in columns_order))
            for instance in data
        ]

        return serialized_content

class CommonCondition(query_elements.Condition):
    '''
    a base class for most of the conditions.
    supports serialization facilities (via an sql serializer defined globally or locally) and some common helper functions.
    '''

    def __init__(self, serializer=None):
        super().__init__()
        self._serializer = serializer

    @property
    def serializer(self):
        global _global_serializer
        return _global_serializer if self._serializer is None else self._serializer

    def serialize(self, value):
        return self.serializer.serialize(value)

    def lower(self, column, value=None):
        if value is None:
            return self.serializer.to_lower_field(column.name) if self.serializer.is_lowerable(column.type) else column.name
        else:
            value = self.serializer.serialize(value if isinstance(value, column.type) else column.type(value))
            return self.serializer.to_lower_field(value) if self.serializer.is_lowerable(column.type) else value

    def serialize_field(self, value, field, table=None, alias=True):
        if isinstance(field, str):
            field = self._get_field(field, table, alias)

        # run a best-effort here as not all types are actual type (the timestamp parser for example is an instance)

        if not isinstance(field.type, type): # if the parser is an instance parse again and hope for the best (the parser should handle this properly)
            value = field.type(value)
        elif not isinstance(value, field.type): # if field.type is a type we can check that the value is as expected
            value = field.type(value)

        return self.serializer.serialize(value)

class CommonConditionRegistry:
    '''
    a class that allows access to all the common condition classes while tracking new instances
    of these classes generated via this class'es API.

    e.g.
        class RangeCondition(CommonCondition):
            ...

        registry = CommonConditionRegistry()
        new_condition = registry.RangeCondition("timestamp")

        print(registry.conditions) # will print (<new_condition: RangeCondition("timestamp")>,)
    '''

    class _ConditionCtor:
        def __init__(self, conditions_list, klass):
            self._klass = klass
            self._conditions_list = conditions_list

        def __call__(self, *args, **kwargs):
            new_obj = self._klass(*args, **kwargs)
            self._conditions_list.append(new_obj)
            return new_obj

    def __init__(self):
        self._conditions = []
        self._class_cache = {
            CommonCondition.__name__ : self._ConditionCtor(self._conditions,
                                                           CommonCondition)
        }

    def register(self, condition):
        self._conditions.append(condition)

    def __getattr__(self, klass):
        if klass in self._class_cache:
            return self._class_cache[klass]

        resolution_queue = [CommonCondition]
        while len(resolution_queue) > 0:
            subclass = resolution_queue.pop(0)
            child_classes = list(subclass.__subclasses__())

            for child_class in child_classes:
                if child_class.__name__ == klass:
                    self._class_cache[klass] = self._ConditionCtor(self._conditions, child_class)
                    return self._class_cache[klass]

            resolution_queue += child_classes

        raise ValueError("can't find CommonCondition subclass", klass)

    @property
    def conditions(self):
        return tuple(self._conditions)

class TableRegistry:
    '''
    a utility class to generate table specs and keep track of them.
    '''
    def __init__(self, suffix=""):
        self._suffix = "" if suffix in (None, "") else f"_{suffix}"
        self._tables = {}

    def _table(self, table_name, alias, is_joint):
        if table_name in self._tables:
            return self._tables[table_name]

        alias = table_name if alias is None else alias
        self._tables[table_name] = (query_elements.JointSelectionTable if is_joint else query_elements.SelectionTable)(table_name + self._suffix, alias)
        return self._tables[table_name]

    def table(self, table_name, alias=None):
        return self._table(table_name, alias, False)

    def joint_table(self, table_name, alias=None):
        return self._table(table_name, alias, True)

    @property
    def tables(self):
        return tuple(self._tables.values())

    def __getitem__(self, table_name):
        return self._tables[table_name]

class JoinClauseRegistry:
    '''
    a utility to generate join clauses and keep track of them.
    will be used for filtering.
    '''
    def __init__(self):
        self._join_clauses = []

    @property
    def join_clauses(self):
        return tuple(self._join_clauses)

    def join_clause(self, joint_table, strict=False, table_alias=None):
        clause = query_elements.JoinClause(joint_table, strict, table_alias)
        self._join_clauses.append(clause)
        return clause

def test():
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s", level=logging.DEBUG)

    # create a table registry
    table_registry = TableRegistry("auto_suffix")

    # create some test tables
    table = table_registry.table("file_system_events", "file_system_events")
    table_registry.table("process_instance_start", "psi")
    table_registry.table("registry_events")

    print("registered tables:")
    for table in table_registry.tables:
        print("\t", table.name, table.alias)

    # fill the table with columns
    host_col = table.add_column("hostId", "host", sql_serialization.HostId)
    instance_col = table.add_column("instanceId", "instance", int)
    file_col = table.add_column("filePath", "file") # default type is str
    access_col = table.add_column("fileAccessType", col_type=int) # intentionally without an alias

    common_condition = CommonCondition()
    common_condition.associate_table(table)
    table_ops = TableOperations(table)

    # print some serialized lowered values
    print("\nlower casing:")
    print("\thost:", common_condition.lower(host_col, "Sentimental-Lady"))
    print("\thost (field):", common_condition.lower(host_col))
    print("\tfile:", common_condition.lower(file_col, "BLOWING THROUGH MY LIFE again"))
    print("\tfile (field):", common_condition.lower(file_col))
    print("\tinstance:", common_condition.lower(instance_col, "1337"))
    print("\tinstance (field):", common_condition.lower(instance_col))
    print("\taccess:", common_condition.lower(instance_col, 7331))
    print("\taccess (field):", common_condition.lower(instance_col))

    # print the table's creation spec
    print("\ntable creation spec:")
    name, column_specs = table_ops.creation_specs
    print("name: ", name)
    for col_name, col_db_type in column_specs.items():
        print("\t", col_name, col_db_type)

    # serialize some content
    data = [
        {"hostId" : "host-1", "instance" : 1337, "file" : "file-1", "fileAccessType" : 0},
        {"host" : "host-2", "instanceId" : 1338, "file" : "file-2", "fileAccessType" : 1},
        {"host" : "host-3", "instance" : 1339, "filePath" : "file-3", "fileAccessType" : 2},
        {"hostId" : "host-4", "instanceId" : 1340, "filePath" : "file-4", "fileAccessType" : 3},
    ]

    serialized_content = table_ops.insertion_specs(data)

    print("\ninsertion specs:")
    for serialized_row in serialized_content:
        print("\t",*serialized_row)

    table.insert(
        {"hostId" : "X-host-1", "instance" : -1337, "file" : "X-file-1", "fileAccessType" : 0},
        {"host" : "X-host-2", "instanceId" : -1338, "file" : "X-file-2", "fileAccessType" : -1},
        {"host" : "X-host-3", "instance" : -1339, "filePath" : "X-file-3", "fileAccessType" : -2},
        {"hostId" : "X-host-4", "instanceId" : -1340, "filePath" : "X-file-4", "fileAccessType" : -3}
    )

    serialized_content = table_ops.insertion_specs()

    print("\ninsertion specs: (internal content)")
    for serialized_row in serialized_content:
        print("\t",*serialized_row)

    # condition registry
    cond_registry = CommonConditionRegistry()

    class MyCondition1(CommonCondition):
        counter = 1

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.count = MyCondition1.counter
            MyCondition1.counter += 1

        def __str__(self):
            return f"cond-1 <count: {self.count}>"

    class MyCondition2(MyCondition1):
        def __str__(self):
            return f"cond-2 <count: {self.count}>"

    cond_registry.MyCondition1() # count 1
    cond_registry.MyCondition1() # count 2
    cond_registry.MyCondition2() # count 3
    cond_registry.MyCondition2() # count 4

    print("\nregistry test:")
    for cond in cond_registry.conditions:
        print("\t", cond)

if __name__ == "__main__":
    sys.exit(test())

