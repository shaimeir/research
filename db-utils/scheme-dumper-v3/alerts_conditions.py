#!/usr/bin/env python3

'''
these conditions are tailored to the alerts table.
'''

import logging

import conditions
import sql_base
import query_elements
import conditions_base

# use these functions for the postgres as the event_ts field there is held as integer in milliseconds

alerts_before_timestamp_condition = None

@conditions.condition_switches.register("--alerts-before", type=conditions.timestamp_parser)
def before_switch(before_timestamp):
    '''
    add a time upper bound filter
    '''
    global alerts_before_timestamp_condition
    if alerts_before_timestamp_condition is None:
        alerts_before_timestamp_condition = conditions.condition_registry.RangeCondition("timestamp", table="main")

    alerts_before_timestamp_condition.high = before_timestamp.timestamp() * 1000

alerts_after_timestamp_condition = None

@conditions.condition_switches.register("--alerts-after", type=conditions.timestamp_parser)
def after_switch(after_timestamp):
    '''
    add a time lower bound filter
    '''
    global alerts_after_timestamp_condition
    if alerts_after_timestamp_condition is None:
        alerts_after_timestamp_condition = conditions.condition_registry.RangeCondition("timestamp", table="main")

    alerts_after_timestamp_condition.low = after_timestamp.timestamp() * 1000

alert_statuses = None

@conditions.condition_switches.register("--alerts-status", action='append')
def equality_condition(status):
    '''
    set the possible alert statuses
    '''
    global alert_statuses
    if alert_statuses is None:
        alert_statuses = conditions.condition_registry.InCondition("tables.main.aliases.status")

    alert_statuses.add_value(status)

    return status

