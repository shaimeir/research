#!/usr/bin/env python3

import sys
import logging
import termcolor

import query_elements
from query_elements import AndCondition
import sql_base
import query_elements

from dbcursors import sql_serialization

class RangeCondition(sql_base.CommonCondition):
    def __init__(self, field, low=None, high=None, alias=True, table=None, serializer=None):
        super().__init__(serializer)
        self._field = field
        self.low = low
        self.high = high
        self._alias = alias
        self._table = table

    def _condition_str(self):
        if self.low is None and self.high is None:
            return super()._condition_str()

        field = self._get_field(self._field, self._table, self._alias)
        low = None if self.low is None else self.serialize_field(self.low, field)
        high = None if self.high is None else self.serialize_field(self.high, field)

        condition = ""

        if low is not None:
            condition += f"{low} <= "

        condition += field.name

        if high is not None:
            condition += f" <= {high}"

        return condition

class EqCondition(sql_base.CommonCondition):
    def __init__(self, left, right, left_is_col=True, right_is_col=True):
        super().__init__()
        self.left = left
        self.right = right
        self.left_is_col = left_is_col
        self.right_is_col = right_is_col

    def _condition_str(self):
        if self.left_is_col and self.right_is_col:
            return self.format_condition(f"{{{self.left}}} = {{{self.right}}}")

        if not self.left_is_col and not self.right_is_col:
            left = self.serialize(self.left)
            right = self.serialize(self.right)
            return f"{left} = {right}"

        col = self.left if self.left_is_col else self.right
        value = self.left if self.right_is_col else self.right

        col = self.find_field(col)
        value = self.serialize(col.parse_value(value))

        return f"{col} = {value}"

class InCondition(sql_base.CommonCondition):
    def __init__(self, column, *values):
        super().__init__()
        self.column = column
        self.values = set(values)

    def add_value(self, *values):
        self.values.update(values)

    def _condition_str(self):
        column = self.find_field(self.column)
        values_clause = ", ".join([self.serialize(column.parse_value(value)) for value in self.values])

        return f"{column} IN ({values_clause})"

class GeneratedFormatCondition(sql_base.CommonCondition):
    '''
    takes a list of conditions (if it's a generator it will be flattened into a tuple)
    and iterates through them when iterated over, formatting each one on demand.
    '''
    def __init__(self, queries_iterable, **kwargs):
        super().__init__()
        self.queries = tuple(queries_iterable)
        self.kwargs = kwargs
        self.current_query = None

    def _condition_str(self):
        query = (super()._condition_str() if len(self.queries) == 0 else self.queries[0]) if self.current_query is None else self.current_query
        return self.format_condition(query, **self.kwargs)

    def __iter__(self):
        for query in self.queries:
            self.current_query = query
            yield self

        self.current_query = None

def test():
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s", level=logging.DEBUG)

    # create a test table
    table = query_elements.SelectionTable("file_system_events")
    host_col = table.add_column("hostId", "host", sql_serialization.HostId)
    instance_col = table.add_column("instanceId", "instance", int)
    file_col = table.add_column("filePath", "file") # default type is str
    access_col = table.add_column("fileAccessType", col_type=int) # intentionally without an alias

    # create a range condition
    range_condition = RangeCondition("instance", table="main")
    range_condition.associate_table(table, "main")

    print("range condition test:")
    print("\t", str(range_condition))
    range_condition.low = 1337
    print("\t", str(range_condition))
    range_condition.high = 1338
    print("\t", str(range_condition))
    range_condition.low = None
    print("\t", str(range_condition))

    # create equality conditions
    eq_cond_1 = EqCondition("tables.main.aliases.host", "fields.aliases.file", left_is_col=True, right_is_col=True)
    eq_cond_1.associate_table(table, "main")
    eq_cond_2 = EqCondition("tables.main.aliases.instance", "1337", left_is_col=True, right_is_col=False)
    eq_cond_2.associate_table(table, "main")
    eq_cond_3 = EqCondition("file-name", "fields.aliases.file", left_is_col=False, right_is_col=True)
    eq_cond_3.associate_table(table, "main")
    eq_cond_4 = EqCondition(1337, "1337", left_is_col=False, right_is_col=False)
    eq_cond_4.associate_table(table, "main")

    print("equality conditions:")
    print("\t", str(eq_cond_1))
    print("\t", str(eq_cond_2))
    print("\t", str(eq_cond_3))
    print("\t", str(eq_cond_4))

    # create simple containment conditions
    containment1 = InCondition("tables.main.aliases.instance", 1337, '1338', 1339, '1339') # the last one is double and the types are mixed to check casting
    containment1.associate_table(table, "main")
    containment2 = InCondition("tables.main.aliases.file", 'file-1', 'file-2', 'file-3')
    containment2.associate_table(table, "main")
    containment3 = InCondition("tables.main.aliases.host", 'host-1')
    containment3.associate_table(table, "main")

    print("containment conditions:")
    print("\t", str(containment1))
    print("\t", str(containment2))
    print("\t", str(containment3))

    print("generated condition:")
    generated_cond = GeneratedFormatCondition((f"{{tables.main.aliases.file}} ILIKE '%.{file_ext}'" for file_ext in ("exe", "vbs", "js")))
    generated_cond.associate_table(table, "main")

    print("\t", "pre iteration:", str(generated_cond), "\n")
    print("\t", "iteration:")
    for _ in iter(generated_cond):
        print("\t", "\t", str(generated_cond))

    print("\n", "\t", "post iteration:", str(generated_cond))

if __name__ == "__main__":
    sys.exit(test())

