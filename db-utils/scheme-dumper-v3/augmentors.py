#!/usr/bin/env python3

'''
the file defines augmentors.
augmentors return an query_elements.JoinClause to augment the main query.

the augmentor registers its name and its required schemes and it is called in the following manner:

    # the augmentor is defined as follows (requiring 3 schemes)
    @register_augmentor('my-augmentor', 'process', 'file-system', 'network')
    def augmentor_func(process, file_system, network, schemes):
        return ...

    # the main code will load multiple schemes
    registry = load_scheme('registry')
    file_system = load_scheme('file-system')
    network = load_scheme('network')
    process = load_scheme('process')

    # and the main code will call the augmentor in a form equivalent to the following:
    join_clause = get_augmentor('my-augmentor')(process=process,
                                                network=network,
                                                file_system=file_system,
                                                schemes = {
                                                    'file-system' : file_system,
                                                    'network' : network,
                                                    'registry' : registry,
                                                    'process' : process,
                                                })
'''

import logging
from collections import defaultdict
from recordclass import recordclass

import sql_base
import query_elements
import conditions_base

_augmentor_register = recordclass('_augmentor_register', ('augmentor', 'schemes'))
_required_schemes = defaultdict(lambda : _augmentor_register(None, set()))

def get_augmentors_list():
    global _required_schemes
    return tuple(_required_schemes.keys())

def get_augmentors():
    global _required_schemes
    return {
        aug_name : aug_reg.augmentor
        for aug_name, aug_reg in _required_schemes.items()
    }

def get_augmentor(name):
    global _required_schemes
    return _required_schemes[name].augmentor

def get_augmentors_schemes():
    global _required_schemes
    return {
        aug_name : tuple(aug_reg.schemes)
        for aug_name, aug_reg in _required_schemes.items()
    }

join_clause_registry = sql_base.JoinClauseRegistry()

def join_clauses():
    return join_clause_registry.join_clauses

def register_augmentor(name, *schemes):
    global _required_schemes
    _required_schemes[name].schemes.update(schemes)

    def _decorator(augmentor):
        global _required_schemes
        _required_schemes[name].augmentor = augmentor
        return augmentor

    return _decorator

@register_augmentor('process-info', 'process')
def process_info_augmentor(process, **kwargs):
    process_table = query_elements.JointSelectionTable(process.db_table)

    # add the columns to join on
    for col_name in ('process_id', 'host', 'instance'):
        col_desc = process.columns[col_name]
        process_table.add_on_column(col_desc.db_column, col_name, col_desc.type)

    # add the columns to augment
    for col_name in ('process', 'command', 'process_name'):
        col_desc = process.columns[col_name]
        process_table.add_column(col_desc.db_column, col_name, col_desc.type)

    # create the join clause
    join_clause = join_clause_registry.join_clause(process_table, strict=True, table_alias='process')

    # add the ON conditions
    join_clause.add_on_condition(conditions_base.EqCondition('tables.main.aliases.host', 'tables.join.aliases.host'))
    join_clause.add_on_condition(conditions_base.EqCondition('tables.main.aliases.process_id', 'tables.join.aliases.process_id'))
    join_clause.add_on_condition(conditions_base.EqCondition('tables.main.aliases.instance', 'tables.join.aliases.instance'))

