#!/usr/bin/env python3

'''
Containement objects to represent the various parts of a selection query.
These objects will become the building blocks for the auto-generated dump query.
'''

import argparse
import termcolor
import sys
import logging
import sqlparse
from collections import defaultdict

def cartesian_iterator(*iterables):
    if len(iterables) == 0:
        yield tuple()
        return

    options = iter(iterables[0])
    yield from ((value,) + entry for value in options for entry in cartesian_iterator(*iterables[1:]))

class SelectionAsClause:
    '''
    this is used to hold a selected column's alias and generate the AS statement for an auto-generated sql query.
    '''
    def __init__(self, col, alias=None):
        self.col = col
        self.alias = alias

    @property
    def statement(self):
        return self.col if self.alias is None else f'{self.col} AS "{self.alias}"'

    def __str__(self):
        return self.statement

    @property
    def name_statement(self):
        '''
        a helper function used to generate the CSV header for queries.
        '''
        name = self.col if self.alias is None else self.alias
        return f"'{name}' AS \"{name}\""

class SelectionColumn:
    '''
    a column to select from a table.
    this class encapsulates the formatting of the column's various access names (aliases, qualified via a atable, etc..)
    '''
    def __init__(self, name="*", table=None, alias=None, col_type=None):
        self._name = name # field name, unqualified
        self._table = table # the table of the column
        self._alias = alias # an alias for the field
        self._type = str if col_type is None else col_type # the data type

    @property
    def name(self):
        return self._name if self._table is None else f'{self._table}.{self._name}'

    def __str__(self):
        return self.name

    @property
    def raw_name(self):
        return self._name

    @property
    def as_col(self):
        return SelectionAsClause(self.name, self._alias)

    @property
    def alias(self):
        return self.raw_name if self._alias is None else self._alias

    # to generate a header for dumps
    @property
    def output_column(self):
        return self._name if self._alias is None else self._alias

    @property
    def type(self):
        return self._type

    def parse_value(self, value):
        if not isinstance(self._type, type):
            return self._type(value)
        elif not isinstance(value, self._type):
            return self._type(value)
        else:
            return value

    def __eq__(self, other):
        if self.name == other.name:
            assert self._alias == other._alias, ValueError('same column with different aliases compared', self.name, self._alias, other._alias)
            return True

        return False

    def __hash__(self):
        return hash(self.name)

    def __repr__(self):
        return f"SelectionColumn({self.name}, {self.alias})"

class FieldsAccessor:
    '''
    a utility class that allows access to fields (column objects) listed within it via a name and the '.' operator.
    for instance:

        accessor = FieldsAccessor(False) # use the field name and not the fields alias for access
        accessor.add_field(SelectionColumn('my_column', 'my_table', 'my_alias'))
        print(accessor.my_column) # will print "my_table.my_column"

        accessor = FieldsAccessor(True) # use the field alias and not the fields name for access
        accessor.add_field(SelectionColumn('my_column', 'my_table', 'my_alias'))
        print(accessor.my_alias) # will print "my_table.my_column"

    the purpose of this class is to provide syntactic sugar for string interpolation and formatting for condition building.
    e.g.
        '{self.table.alias_accessor.host} = {self.other_table.alias_accessor.host}'

    will format to the correct columns.
    '''

    def __init__(self, by_alias):
        self._fields = {}
        self.by_alias = by_alias

    def add_field(self, selection_column):
        '''
        manually add a field (column) to the internal access pool.
        '''
        self._fields[selection_column.alias if self.by_alias else selection_column.raw_name] = selection_column

    def add_table(self, table):
        '''
        add all the fields (columns) of a SelectionTable instance to the internal access pool.
        '''
        for selection_column in table.get_all_columns():
            self.add_field(selection_column)

    def __getattr__(self, field):
        '''
        allow the '.' access.
        '''
        return self._fields[field]

    def __str__(self):
        return str({'by_alias' : self.by_alias, 'fields' : self._fields})

    def __getitem__(self, field):
        '''
        allow the [] access.
        '''
        return self._fields[field]

class TablesAccessor:
    '''
    Expand the concept behind FieldsAccessor to a batch of tables by creating by-name and by-alias accessors
    for a table's columns and maintaining these tables internally and allowing access to them via the '.' operator.
    again, this is done to allow elegant string interpolation and formatting.

    e.g.
        # build two tables
        file_table = SelectTable("file_system_events")
        file_table.add_column("hostId", "host")

        proc_table = SelectTable("process_instance_start")
        proc_Table.add_column("processPath", "process")

        # create a new accessor for the two tables
        accessor = TablesAccessor()
        accessor.add_table(file_table, "file") # the second argument is an optional alias
        accessor.add_table(proc_table, "proc")

        # now access the fields via aliases and explicit names
        print(accessor.file.hostId) # prints "file_system_events.hostId"
        print(accessor.file.aliases.host) # prints "file_system_events.hostId"
        print(accessor.file_system_events.hostId) # prints "file_system_events.hostId"
        print(accessor.file_system_events.aliases.host) # prints "file_system_events.hostId"

        print(accessor.proc.processPath) # prints "process_instance_start.processPath"
        print(accessor.proc.aliases.process) # prints "process_instance_start.processPath"
        print(accessor.process_instance_start.processPath) # prints "process_instance_start.processPath"
        print(accessor.process_instance_start.aliases.process) # prints "process_instance_start.processPath"
    '''

    def __init__(self):
        self.tables_accessors = {}
        self.tables = {}

    def add_table(self, table, alias=None):
        alias_accessor = FieldsAccessor(True)
        alias_accessor.add_table(table)

        field_accessor = FieldsAccessor(False)
        field_accessor.add_table(table)

        # this is done because too many times i write 'tables.main.fields.aliases.host' and the 'fields' in there
        # is wrong, so i'll just manually add it to give an appearance.
        # same goes for fields_aliases.
        field_accessor.fields = field_accessor

        field_accessor.aliases = alias_accessor
        field_accessor.fields_aliases = alias_accessor

        field_accessor.table_name = table.name

        self.tables_accessors[table.name if alias is None else alias] = field_accessor
        self.tables[table.name if alias is None else alias] = table

    def __getitem__(self, table):
        return self.tables_accessors[table]

    def __getattr__(self, table):
        return self.tables_accessors[table]

    def __str__(self):
        return str(self.tables_accessors)

    def inform(self, *conditions):
        '''
        associate conditions with tables accessible via this object
        '''
        for alias, table in self.tables.items():
            for cond in conditions:
                cond.associate_table(table, alias)

class Condition:
    '''
    the base class to encapsulate conditions used in a WHERE and ON clauses.
    the class provides some built in accessors (2 FieldsAccessor - one by name and one by alias and a TablesAccessor) so you can easily
    use string interpolation and formatting in a child class to generate a condition string.
    the associate_table() and add_field() methods enrich the internal accessors.

    the condition is accessible via the 'condition' property (or str(condition_instance)) which calls the _condition_str method
    that a new filter should implement.
    '''
    def __init__(self):
        self.associate_tables = []
        self.fields_list = []

        self.fields_aliases = FieldsAccessor(True)
        self.fields = FieldsAccessor(False)
        self.fields.aliases = self.fields_aliases # tie it nicely together

        self.tables = TablesAccessor()

    def _get_accessor(self, alias):
        return self.fields_aliases if alias else self.fields

    def _get_field(self, field, table=None, alias=True):
        if table is not None:
            accessor = self.tables[table]
            if alias:
                accessor = accessor.aliases
        else:
            accessor = self._get_accessor(alias)

        return accessor[field]

    def add_field(self, selection_column):
        self.fields.add_field(selection_column)
        self.fields.append(selection_column)

    def associate_table(self, table, alias=None):
        if alias is not None:
            self.tables.add_table(table, alias)
        self.tables.add_table(table, None) # allow access to the table by name as well
        self.tables.add_table(table, table.alias) # allow access to the table by any inherent alias as well
        self.fields.add_table(table)
        self.fields_aliases.add_table(table)
        self.fields_list += list((col for _, col in table.all_items()))
        self.associate_tables.append(table)

    def format_condition(self, condition, **kwargs):
        return condition.format(tables=self.tables, fields=self.fields, fields_aliases=self.fields_aliases, **kwargs)

    def find_field(self, field_access_path):
        '''
        resolve a string in the form 'fields.aliases.host' to the actual column object.
        '''
        lookup_object = self
        for element_name in field_access_path.split("."):
            lookup_object = getattr(lookup_object, element_name)

        return lookup_object

    def __iter__(self):
        '''
        condition iterators are used to allow a condition to modify itself so the next time
        it's .condition property is called it will generate a new condition.
        the default condition simply generates one condition - itself.
        this mechanism allows us to create conditions that generate multiple queries.
        '''
        yield self

    def _condition_str(self):
        return "1=1"

    @property
    def condition(self):
        return self._condition_str()

    def __str__(self):
        return self.condition

class RawCondition(Condition):
    '''
    a raw string condition - the string will not be formatted and the field/table accessors will not be used.
    '''
    def __init__(self, condition_str):
        super().__init__()
        self.condition_str = condition_str

    def _condition_str(self):
        return self.condition_str

class SimpleFormatCondition(Condition):
    '''
    a simple condition described via a string formatted via the internal field and table accessors provided by Condition.
    '''
    def __init__(self, condition_str):
        super().__init__()
        self.condition_str = condition_str

    def _condition_str(self):
        return self.format_condition(self.condition_str)

class SequenceCondition(Condition):
    '''
    a utility condition that takes multiple conditions and goes through them when iterated.
    '''
    def __init__(self, *conditions, auto_associate=True):
        super().__init__()
        self.conditions = list(conditions)
        self.current_condition = None
        self.auto_associate = auto_associate

    def add_condition(self, *conditions):
        self.conditions += list(conditions)

        if self.auto_associate:
            self.tables.inform(*conditions)

    def associate_table(self, table, alias=None):
        super().associate_table(table, alias)

        if self.auto_associate:
            for cond in self.conditions:
                cond.associate_table(table, alias)

    def __iter__(self):
        '''
        go through the stacked conditions
        '''
        for cond in self.conditions:
            self.current_condition = cond
            for _ in iter(cond):
                yield self

        self.current_condition = None

    def _condition_str(self):
        return (self.conditions[0].condition if len(self.conditions) >= 1 else super()._condition_str()) if self.current_condition is None else self.current_condition.condition

class AndCondition(Condition):
    '''
    aggregate multiple conditions and generate one condition string containing the aggregated conditions concatenated by 'AND'.
    '''
    def __init__(self, *conditions, auto_associate=True):
        super().__init__()
        self.conditions = list(conditions)
        self.auto_associate = auto_associate

    def add_condition(self, *conditions):
        self.conditions += list(conditions)

        if self.auto_associate:
            self.tables.inform(*conditions)

    def associate_table(self, table, alias=None):
        super().associate_table(table, alias)

        if self.auto_associate:
            for cond in self.conditions:
                cond.associate_table(table, alias)

    def __iter__(self):
        '''
        generates the cartesian product of all the ANDed conditions
        '''

        for conditions in (cartesian_iterator(*self.conditions) if len(self.conditions) > 0 else (None,)):
            yield self

    @property
    def condition_count(self):
        return len(self.conditions)

    def _condition_str(self):
        return ("(" + " AND ".join((f"({cond})" for cond in self.conditions)) + ")") if self.condition_count > 0 else "(1=1)"

class SelectionTable:
    '''
    encapsulate an SQL table's interface - basically holds a table name and it's columns.
    '''
    def __init__(self, table_name, alias=None):
        self._name = table_name
        self._columns = {}
        self._table_data = []
        self._alias = alias

    @property
    def name(self):
        return self._name

    @property
    def alias(self):
        return self._name if self._alias is None else self._alias

    def add_column(self, col_name, alias=None, col_type=None):
        column = SelectionColumn(col_name, self.name, alias, col_type)

        self._columns[col_name] = column

        return column

    def items(self):
        yield from self._columns.items()

    @property
    def column_count(self):
        return len(self._columns)

    def get_columns(self):
        yield from self._columns.values()

    # child classes can define this method to yield columns that shouldn't be listed in self._columns (and therefor not selected in the output of a query).
    def all_items(self):
        yield from self.items()

    # the same idea as all_items() but only yield the column descriptors.
    def get_all_columns(self):
        yield from self.get_columns()

    # the same as column_count() but for all columns...
    @property
    def full_column_count(self):
        return self.column_count

    def all_items_with_aliases(self):
        '''
        iterates over the the columns (yielding name/column pairs) similarly to get_all_columns()
        but yields alias/column pairs as well.
        '''
        for col_name, column in self.all_items():
            yield (col_name, column)
            alias = column.alias
            if alias != col_name and alias is not None:
                yield (alias, column)

    def insert(self, *values):
        self._table_data += list(values)

    @property
    def table_data(self):
        return tuple(self._table_data)

class JointSelectionTable(SelectionTable):
    '''
    an extension of the selection table abstraction.
    this class provides an extra grouping of columns called 'on columns' that are not columns to select (on an output)
    but columns used for comparison in an ON and WHERE section of a JOIN clause.
    '''
    def __init__(self, table_name, alias=None):
        super().__init__(table_name, alias)
        self.on_columns = SelectionTable(table_name, alias)

    def add_on_column(self, col_name, alias=None, col_type=None):
        return self.on_columns.add_column(col_name, alias, col_type)

    def all_items(self):
        yield from self.items()
        yield from self.on_columns.items()

    def get_all_columns(self):
        yield from self.get_columns()
        yield from self.on_columns.get_columns()

    @property
    def full_column_count(self):
        return self.column_count + self.on_columns.column_count

class JoinClause:
    '''
    an abstraction of a join clause that holds a table to join with the main table and a collection of conditions.
    the clause holds an internal list of reference tables to update the accessors of all the conditions that will
    be associated with this clause.

    a join can be strict (INNER JOIN) or loose (LEFT JOIN).
    '''

    def __init__(self, joint_table, strict=False, table_alias=None):
        '''
        the joint table is the table to add with a join clause to the main query (e.g. a table that holds the process name etc...).
        the strict argument determines whether the join should be a filtering join (INNER JOIN, a join that when the criteria aren't met
        the entire row in the main query will be dropped) or a loose one (LEFT JOIN, a join that keeps the main query's row
        even if there's no match for the join condition).
        '''
        # a JointSelectionTable instance whose columns are to be added to the main query
        self.joint_table = joint_table
        self.joint_table_alias = table_alias
        # the tables this join clause joins the joint table with
        self.ref_tables = {}
        # set the strictness of the join
        self._strict = strict

        # the WHERE clause filters to use for the JOIN clause
        self._conditions = []
        # the ON clause filters to use for the JOIN clause
        self._on_conditions = []

        self.add_ref_table(self.joint_table, table_alias)

    @property
    def strict(self):
        return self._strict

    @property
    def conditions(self):
        return tuple(self._conditions)

    @property
    def on_conditions(self):
        return tuple(self._on_conditions)

    def add_ref_table(self, table, alias=None):
        self.ref_tables[table.alias] = table
        self.ref_tables[table.name] = table
        if alias is not None:
            self.ref_tables[alias] = table

        for condition in self._conditions:
            condition.associate_table(table, alias)

        for condition in self._on_conditions:
            condition.associate_table(table, alias)

    def add_main_table(self, table):
        self.add_ref_table(table, 'main')

    @property
    def table(self):
        return self.joint_table

    def add_on_condition(self, condition):
        condition.associate_table(self.joint_table, 'join')

        for table_alias, ref_table in self.ref_tables.items():
            condition.associate_table(ref_table, table_alias)

        self._on_conditions.append(condition)

    def add_condition(self, *conditions):
        for condition in conditions:
            condition.associate_table(self.joint_table, 'join')

            for table_alias, ref_table in self.ref_tables.items():
                condition.associate_table(ref_table, table_alias)

            self._conditions.append(condition)

    def inform_condition(self, *conditions):
        '''
        associate the joint table with a list of given conditions
        '''
        for condition in conditions:
            condition.associate_table(self.joint_table, self.joint_table_alias)

    def __iter__(self):
        '''
        go through all the conditions options for both regular conditions and on conditions.
        '''
        for active_on_conditions in (cartesian_iterator(*self._on_conditions) if len(self._on_conditions) > 0 else (None,)):
            for active_conditions in (cartesian_iterator(*self._conditions) if len(self._conditions) > 0 else (None,)):
                yield self

class Selection:
    '''
    an abstraction of a selection query.
    this holds an internal main table, a bunch of conditions and a list of join clauses.
    use the 'query' property to build a query from all of them put together.
    '''
    def __init__(self, main_table):
        self.main_table = main_table
        self._conditions = []
        self.join_clauses = []

        self.update_columns()

    @property
    def conditions(self):
        return tuple(self._conditions)

    def update_columns(self):
        # to maintain a consistent column order in the output order (to allow for generation
        # of CSV headers for dumps) always cache the current column order in a variable.
        self.cached_columns_order = tuple(self._columns())

    def add_condition(self, *conditions):
        for condition in conditions:
            self._conditions.append(condition)
            condition.associate_table(self.main_table, "main")

        for join_clause in self.join_clauses:
            join_clause.inform_condition(*conditions)

    def add_join_clause(self, *join_clauses):
        for join_clause in join_clauses:
            join_clause.add_main_table(self.main_table)
            for existing_join_clause in self.join_clauses:
                join_clause.add_ref_table(existing_join_clause.table)

            self.join_clauses.append(join_clause)
            join_clause.inform_condition(*self._conditions)

            self.update_columns()

    def _columns(self):
        for join_clause in self.join_clauses:
            yield from join_clause.table.get_columns()

        # if no columns were specified then yield an "all columns" selection
        if self.main_table.column_count == 0:
            yield SelectionColumn(table=self.main_table.name)

        # otherwise yield the specified columns
        else:
            yield from self.main_table.get_columns()

    @property
    def columns(self):
        return self.cached_columns_order

    def __iter__(self):
        for join_clause_sequence in cartesian_iterator(*self.join_clauses) if len(self.join_clauses) > 0 else (None,):
            for active_conditions in cartesian_iterator(*self.conditions) if len(self.conditions) > 0 else (None,):
                yield self

    # used for testing for now. proper SQL serialization should take into account the database cursor and database semantics.
    @property
    def query(self):
        query = "SELECT "

        query += ", ".join((col.as_col.statement for col in self.cached_columns_order))

        query += f" FROM {self.main_table.name}"
        join_conditions = []
        for join_clause in self.join_clauses:
            join_type = "INNER" if join_clause.strict else "LEFT"
            query += f" {join_type} JOIN {join_clause.table.name}"

            on_conditions = AndCondition(*join_clause.on_conditions, auto_associate=False) # don't auto associate as this condition is just for query generation
            if on_conditions.condition_count > 0:
                query += f" ON {on_conditions}"

            join_conditions += list(join_clause.conditions)

        conditions_clause = AndCondition(*self.conditions, *join_conditions, auto_associate=False) # don't auto associate as this condition is just for query generation
        if conditions_clause.condition_count > 0:
            query += f" WHERE {conditions_clause}"

        query += ";"

        return query

    def __str__(self):
        return self.query

def _format_selection(select, indentation=None):
    query = termcolor.colored(sqlparse.format(select.query, reindent=True, keyword_case='upper'), 'magenta')
    if indentation is not None:
        query = indentation + query.replace("\n", "\n" + indentation)
    return query

def test():
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s", level=logging.DEBUG)

    # a simple empty selection
    table = SelectionTable("file_system_events", "fs")
    select = Selection(table)

    print(termcolor.colored("empty object:", "red"))
    print(_format_selection(select))

    # select specific columns
    table.add_column("hostId", "host")
    table.add_column("instanceId", "instance", int)
    table.add_column("filePath", "file")
    table.add_column("fileAccessType", col_type=int) # intentionally without an alias
    select.update_columns()

    print(termcolor.colored("\nwith columns:", "red"))
    print(_format_selection(select))

    # add a few conditions
    select.add_condition(RawCondition("1337 = 1337"))
    select.add_condition(SimpleFormatCondition("{fields.aliases.host} = 'some-host'"))
    select.add_condition(SimpleFormatCondition("{tables.main.aliases.file} = 'field-access-by-table-alias'"))
    select.add_condition(SimpleFormatCondition("{tables.fs.aliases.file} = 'table-access-by-table-alias'")) # access by table alias

    print(termcolor.colored("\nwith conditions:", "red"))
    print(_format_selection(select))

    # join with a tables
    proc_joint_table = JointSelectionTable("process_instance_start", "process")
    proc_joint_table.add_on_column("hostId", "host")
    proc_joint_table.add_on_column("instanceId", "instance", int)
    proc_joint_table.add_column("processPath", "process")
    proc_joint_clause = JoinClause(proc_joint_table, True)
    select.add_join_clause(proc_joint_clause)

    print(termcolor.colored("\nwith one join:", "red"))
    print(_format_selection(select))

    # join with another table
    hosts_joint_table = JointSelectionTable("hosts")
    hosts_joint_table.add_on_column("hostId", "host")
    hosts_joint_table.add_column("host_name")
    hosts_joint_clause = JoinClause(hosts_joint_table, False)
    select.add_join_clause(hosts_joint_clause)

    print(termcolor.colored("\nwith two join:", "red"))
    print(_format_selection(select))

    # add some ON conditions
    proc_joint_clause.add_on_condition(SimpleFormatCondition("{tables.main.aliases.host} = {tables.join.aliases.host}")) # access by default table aliases
    proc_joint_clause.add_on_condition(SimpleFormatCondition("{tables.file_system_events.aliases.instance} = {tables.process_instance_start.aliases.instance}")) # access by explicit table names
    hosts_joint_clause.add_on_condition(SimpleFormatCondition("{tables.file_system_events.aliases.host} = {tables.join.aliases.host}")) # mix it up with table field alias and explicit table name
    hosts_joint_clause.add_on_condition(SimpleFormatCondition("{tables.fs.aliases.host} > {tables.process.aliases.host}")) # mix it up with two table aliases

    print(termcolor.colored("\nwith join ON conditions:", "red"))
    print(_format_selection(select))

    # add some WHERE conditions to the JOIN clauses
    proc_joint_clause.add_condition(SimpleFormatCondition("{tables.main.aliases.file} IN ('a.txt', 'b.txt')"))
    proc_joint_clause.add_condition(SimpleFormatCondition("{tables.main.aliases.instance} IN (1, 2, 3)"))
    hosts_joint_clause.add_condition(SimpleFormatCondition("{tables.process_instance_start.aliases.process} = {tables.join.aliases.host}")) # since the hosts clause was added after the proc clause, proc should be visible to it

    print(termcolor.colored("\nwith join WHERE conditions:", "red"))
    print(_format_selection(select))

    # print the full column/alias spec
    print(termcolor.colored("\nfull column spec (with aliases):", "red"))
    for col_name_or_alias, column in table.all_items_with_aliases():
        print("\t", col_name_or_alias, repr(column))

    # test the cartesian iterator
    print(termcolor.colored("\ncartesian iterator test:", "red"))
    for a, b, c in cartesian_iterator(('a-1', 'a-2'), ('b-1', 'b-2'), ('c-1', 'c-2')):
        print("\t", a, b, c)

    # test the sequence condition
    print(termcolor.colored("\nsequence conditions:"))
    sequence_cond = SequenceCondition()
    print("\t", "empty:", str(sequence_cond))
    sequence_cond.add_condition(RawCondition("1337 = 1337"), RawCondition("1338 = 1338"), RawCondition("1339 = 1339"))
    print("\t", "outside of iteration:", str(sequence_cond))
    print("\t", "iteration:")
    for _ in iter(sequence_cond):
        print("\t", "\t", termcolor.colored(str(sequence_cond), "magenta"))
    print("\t", "post iteration:", str(sequence_cond))

    stacked_sequence_cond = SequenceCondition(RawCondition("melancholy_blues = 'dead and gone'"), sequence_cond, RawCondition("ive_got_the_scars = 'from those dives ive played in'"))
    print("\t", "stacked sequence:")
    for _ in iter(stacked_sequence_cond):
        print("\t", "\t", termcolor.colored(str(stacked_sequence_cond), "magenta"))

    # test the AndCondition iterator
    complex_and = AndCondition(RawCondition("this_baby_grand = 'has been good to me'"), stacked_sequence_cond, SequenceCondition(RawCondition("had_fame > 'but it doesnt stay'"), RawCondition("but_only_songs_like_these <= 'played in minor keys'")))
    print(termcolor.colored("\nAND condition iteration:", "red"))
    for _ in iter(complex_and):
        print("\t", termcolor.colored(str(complex_and), "magenta"))

    # test selection iteration
    print(termcolor.colored("\nselection iteration test", "red"))
    proc_joint_clause.add_on_condition(
        SequenceCondition(
            SimpleFormatCondition("RIGHT({tables.fs.aliases.host}, 1) == 'a'"),
            SimpleFormatCondition("RIGHT({tables.fs.aliases.host}, 1) == 'b'"),
        )
    )
    proc_joint_clause.add_condition(
        SequenceCondition(
            SimpleFormatCondition("RIGHT({tables.fs.aliases.host}, 1) == 'C'"),
            SimpleFormatCondition("RIGHT({tables.fs.aliases.host}, 1) == 'D'"),
        )
    )
    select.add_condition(
        SequenceCondition(
            SimpleFormatCondition("RIGHT({tables.fs.aliases.host}, 1) == 'E'"),
            SimpleFormatCondition("RIGHT({tables.fs.aliases.host}, 1) == 'F'"),
        )
    )

    print(termcolor.colored("\twithout iteration:", "green"))
    print(_format_selection(select, "\t\t"))

    print(termcolor.colored("\twith iteration:", "green"))
    i = 0
    for _ in iter(select):
        i += 1
        print(termcolor.colored(f"\t\titeration #{i}:", "blue"))
        print(_format_selection(select, "\t\t\t"))

    print(termcolor.colored("\tpost iteration:", "green"))
    print(_format_selection(select, "\t\t"))

if __name__ == "__main__":
    sys.exit(test())

