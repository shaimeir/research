#!/usr/bin/awk -f

function print_help(exit_code) {
    print "split an input CSV file to seperate files by their process name." > "/dev/stderr"
    print "--column [column] - set process name column. (default: 1)" > "/dev/stderr"
    print "--column-name [column] - set the process column to the column with this name." > "/dev/stderr"
    print "--output [directory] - set the output directory. (required)" > "/dev/stderr"
    print "--extra [column] - additional columns to split by."
    print "--extra-name [column] - additional columns to split by given by name."
    print "[dump file] - the file to seperate. assumed to have a header. leave empty for stdin." > "/dev/stderr"
    print "" > "/dev/stderr"
    print "--column-name and --column are mutually exclusive." > "/dev/stderr"
    print "--extra-name and --extra are mutually exclusive." > "/dev/stderr"
    exit exit_code
}

function infer_scheme(line) {
    if (0 < index(line, "\t")) {
        FS="\t"
        _strip_pattern = ".*\\\\"
    } else if (1 == index(line, "\"")) {
        FS="\",\""
        _strip_pattern = "(^\\\")|(.*\\\\)|(\\\"$)"
    } else {
        FS=","
        _strip_pattern = ".*\\\\"
    }

    print "inferred separator: '" FS "'" > "/dev/stderr"

    if (-1 == proc_column && "" != proc_column_name) {
        found = 0
        col_count = split($0, split_header)

        print "looking for the \"" proc_column_name "\" column" > "/dev/stderr"
        for(i = 1; i <= col_count; i++) {
            col_name = split_header[i]
            gsub(_strip_pattern, "", col_name)

            print "\tcolumn " i " is \"" col_name "\"" > "/dev/stderr"

            if (col_name == proc_column_name) {
                proc_column = i
                found = 1
                break
            }
        }

        if (found == 0) {
            print "error: couldn't infer column from name: \"" proc_column_name "\" and column index not specified." > "/dev/stderr"
            print_help(1)
        }

        print "inferred column: " proc_column > "/dev/stderr"
    }

    if (2 == extra_col_type) {
        print "resolving extra columns" > "/dev/stderr"

        resolved_count = 0
        col_count = split($0, split_header)

        for(i = 1; i<= col_count; i++) {
            col_name = split_header[i]
            gsub(_strip_pattern, "", col_name)

            for (j = 1; j <= extra_columns_count; j++) {
                if (col_name == extra_column[j]) {
                    print "\tresolved " col_name " to " i > "/dev/stderr"
                    resolved_extra_column[j] = i
                    extra_column[j] = ""
                    resolved_count += 1

                    # don't break to allow for repetitions
                }
            }
        }

        if (resolved_count != extra_columns_count) {
            print "not all columns were resolved" > "/dev/stderr"
            for (j = 1; j <= extra_columns_count; j++) {
                if (extra_column[j] != "") {
                    print "\tcouldn't resolve " extra_column[j] > "/dev/stderr"
                }
            }

            print_help(1)
        }

        for (i = 1; i <= extra_columns_count; i++) {
            extra_column[i] = resolved_extra_column[i]
        }
        print "all extra columns were resolved (" extra_columns_count " columns)" > "/dev/stderr"
    }

    return _strip_pattern
}

BEGIN {
    proc_column=-1
    proc_column_name=""
    extra_columns_count=0
    extra_col_type=0 # 0 - unspecified, 1 - integers, 2 - names

    for (i = 1; i < ARGC; i++) {
        if (ARGV[i] == "help") {
            print_help()
        }

        else if (ARGV[i] == "--column") {
            if (ARGV[i+1] == "") {
                print "error: --column missing argument." > "/dev/stderr"
                print_help(1)
            }

            if ("" != proc_column_name) {
                print "error: --column and --column-name are mutually exclusive." > "/dev/stderr"
                print_help(1)
            }

            proc_column = int(ARGV[i+1])
            delete ARGV[i]
            delete ARGV[i+1]
            i++
        }

        else if (ARGV[i] == "--column-name") {
            if (ARGV[i+1] == "") {
                print "error: --column-name missing argument" > "/dev/stderr"
                print_help(1)
            }

            if (-1 != proc_column) {
                print "error: --column and --column-name are mutually exclusive." > "/dev/stderr"
                print_help(1)
            }

            proc_column_name = ARGV[i+1]
            delete ARGV[i]
            delete ARGV[i+1]
            i++
        }

        else if (ARGV[i] == "--output") {
            if (ARGV[i+1] == "") {
                print "error: --output missing argument" > "/dev/stderr"
                print_help(1)
            }

            output_dir=ARGV[i+1]
            system("mkdir -p " output_dir)

            delete ARGV[i]
            delete ARGV[i+1]
            i++
        }

        else if (ARGV[i] == "--extra") {
            if (ARGV[i+1] == "") {
                print "error: --extra missing argument" > "/dev/stderr"
                print_help(1)
            }

            if (extra_col_type == 2) {
                print "error: --extra and --extra-name are mutually exclusive." > "/dev/stderr"
                print_help(1)
            }

            extra_col_type = 1

            extra_columns_count += 1
            extra_column[extra_columns_count] = int(ARGV[i+1])
            delete ARGV[i]
            delete ARGV[i+1]
            i++
        }

        else if (ARGV[i] == "--extra-name") {
            if (ARGV[i+1] == "") {
                print "error: --extra-name missing argument" > "/dev/stderr"
                print_help(1)
            }

            if (extra_col_type == 1) {
                print "error: --extra and --extra-name are mutually exclusive." > "/dev/stderr"
                print_help(1)
            }

            extra_col_type = 2

            extra_columns_count += 1
            extra_column[extra_columns_count] = ARGV[i+1]
            delete ARGV[i]
            delete ARGV[i+1]
            i++
        }

        else
        {
            break
        }
    }

    if (output_dir == "") {
        print "error: missing output directory" > "/dev/stderr"
        print_help(1)
    }
}

FNR == 1 {
    first_line=$0
    strip_pattern=infer_scheme($0)
}

FNR > 1 {
    if (proc_column != -1) {
        proc_name = $proc_column
        gsub(strip_pattern, "", proc_name)
    } else {
        proc_name = "noproc"
    }

    if (extra_columns_count > 0) {
        for (_i in extra_column) {
            proc_name = proc_name "-" $extra_column[_i]
        }
    }

    output_file = output_dir "/split-" proc_name
    if (!(proc_name in already_initialized)) {
        print first_line > output_file
        already_initialized[proc_name] = 1
    }

    gsub(/[^a-zA-Z_0-9.-]+/,"", proc_name)
    print $0 > output_file
}

END {
}
