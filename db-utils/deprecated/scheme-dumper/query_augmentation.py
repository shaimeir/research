#!/usr/bin/env python3

'''
actual query augmentation code based on the framework layed out in query_augmentation_base.
'''

import sys
import logging

try:
    from . import query_augmentation_base, dump_filters_base
    from .query_augmentation_base import query_augmentors
except Exception:
    import query_augmentation_base, dump_filters_base
    from query_augmentation_base import query_augmentors

from dbcursors import sql_serialization

@query_augmentation_base.query_augmentors.register("process-info")
def process_info_augmentation(columns_translation_dict={},
                              main_query_name=None,
                              process_events_table='process_instance_start',
                              host_id="hostId",
                              secdo_process_id='secdoProcessId',
                              instance_id="instanceId",
                              process_path='processPath',
                              command_line='cmd'):
    '''
    creates a join augmentation that adds the process path and command line to a query.
    '''
    columns = {
        host_id : query_augmentation_base.new_col_rec(columns_translation_dict.get(host_id, host_id), host_id, col_type=sql_serialization.HostId),
        secdo_process_id : query_augmentation_base.new_col_rec(columns_translation_dict.get(secdo_process_id, secdo_process_id), secdo_process_id, col_type=sql_serialization.SecdoProcessId),
        instance_id : query_augmentation_base.new_col_rec(columns_translation_dict.get(instance_id, instance_id), instance_id, col_type=int),
        process_path : query_augmentation_base.new_col_rec(columns_translation_dict.get(process_path, 'process'), process_path, col_type=sql_serialization.ProcessPath, case_sensitive=True),
        command_line : query_augmentation_base.new_col_rec(columns_translation_dict.get(command_line, 'command'), command_line, col_type=sql_serialization.CommandLine, case_sensitive=True),
    }

    return query_augmentation_base.BasicJoinAugmentation(
                # the process events table
                process_events_table,
                # list all the columns we're going to need
                list(columns.values()),
                # columns that will be added to the scheme
                (process_path, command_line),
                # colums that will be compared
                (host_id, secdo_process_id, instance_id),
                # give the temp table a better name
                name='ProcessInfo',
                # set the main query's name
                main_query_name=main_query_name,
                # remove output lines that can't be augmented with this augmentor!
                strong_augmentation=True
           )

@query_augmentation_base.query_augmentors.register("host-name")
def host_name_augmentation(columns_translation_dict={},
                           main_query_name=None,
                           hosts_table='hosts',
                           host_id='hostId',
                           host_name='host_name'):
    '''
    creates a join augmentation that adds the host name to to a query.
    '''
    return query_augmentation_base.BasicJoinAugmentation(
                # the host events table
                hosts_table,
                # list all the columns we're going to need
                [
                    # host name column
                    query_augmentation_base.new_col_rec(columns_translation_dict.get(host_name, host_name), host_name),
                    # host id column
                    query_augmentation_base.new_col_rec(columns_translation_dict.get(host_id, host_id), host_id),
                ],
                # columns that will be added to the scheme
                [host_name],
                # colums that will be compared
                [host_id],
                # give the temp table a better name
                name='HostName',
                # set the main query's name
                main_query_name=main_query_name
           )

### test

def test():
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s [%(levelname)s] %(message)s')

    column_translation = {
        'secdoProcessId' : 'process_id',
        'processPath' : 'process',
        'cmd' : 'command',
        'host_name' : 'hostname',
    }

    for augmentor_name in query_augmentation_base.query_augmentors.choices():
        augmentor = query_augmentation_base.query_augmentors.option(augmentor_name)(column_translation)
        print()
        print(augmentor_name + ":")
        query_augmentation_base.print_augmentor(augmentor, "\t")

if __name__ == "__main__":
    sys.exit(test())

