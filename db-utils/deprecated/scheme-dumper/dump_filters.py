#!/usr/bin/env python3

'''
this script utilizes the generic facilities introduced in dump_filters_base.py and
builds specific filters based on them (such as host id filter, timestamp filter, etc..).
'''

import dateutil.parser
import dateutil.relativedelta
import datetime
import sys
import csv
import logging
import argparse
import hashlib
import sqlparse
import contextlib
import pathlib
import re
from collections import defaultdict
from utilities import switches_registry_util

import dump_filters_base
from dbcursors import sql_serialization
from dump_filters_base import get_filters, set_filter_serializer

# this will help us add the switches to the argument parser dynamically and from this file
# instead of having the main program import this file and assigining each switch a manually.
filter_switches = switches_registry_util.deferred_parsing_registrator()

def consume_filters(args):
    switches_registry_util.apply_deferred_parsing(args)

### timestamp filter

def timestamp_parser(timestamp):
    """
    extended parsing facility for timestamps - allow for both standard timestamp syntax and a relational syntax.
    standard format: "2017-11-01 01:30:00" and you can drop the hours if you want midnight.
    relational time: "2y3m1d4H5M6S" means 1 year, 3 months, 1 day, 4 hours, 5 minutes and 6 seconds before now.
                     you don't have to specify all of these compnents.
    now: just use "now".
    today: to get today's date with the hour set to midnight use "today".
    """

    if "now" == timestamp:
        return datetime.datetime.now()

    if "today" == timestamp:
        return datetime.datetime.combine(datetime.datetime.now().date(), datetime.time())

    # try and parse the argument as a standard timestamp
    with contextlib.suppress(Exception):
        return dateutil.parser.parse(timestamp)

    # parse relative time
    relative_quantities_letters = {
        'y' : 'years',
        'm' : 'months',
        'w' : 'weeks',
        'd' : 'days',
        'H' : 'hours',
        'M' : 'minutes',
        'S' : 'secinds',
    }
    quantities_descriptors = [desc for desc in re.split(r"(\d+[" + "".join(relative_quantities_letters.keys()) + "])", timestamp) if len(desc) > 0]
    relative_time = defaultdict(int)
    for desc in quantities_descriptors:
        assert len(desc) > 1, ValueError("invalid relative timestamp argument", desc, timestamp)
        quantity_type = desc[-1]
        assert quantity_type in relative_quantities_letters, ValueError("invalid quantity type", quantity_type, timestamp)
        try:
            value = int(desc[:-1])
        except Exception:
            raise ValueError("invalid relative time value", desc[:-1], quantity_type, timestamp)

        relative_time[relative_quantities_letters[quantity_type]] += value

    time_delta = dateutil.relativedelta.relativedelta(**relative_time)
    return datetime.datetime.combine(datetime.datetime.now().date(), datetime.time()) - time_delta

last_timestamp_range_filter = None
timestamp_composite_filter = dump_filters_base.OrFilter()

@filter_switches.register("-b", "--before", action="append", type=timestamp_parser)
def before_switch(before_timestamp):
    '''
    add a time upper bound filter
    '''
    global last_timestamp_range_filter
    global timestamp_composite_filter

    if last_timestamp_range_filter is None or last_timestamp_range_filter.high is not None:
        last_timestamp_range_filter = dump_filters_base.RangeFilter('timestamp', field_type=datetime.datetime, register=False) # this filter is registered through timestamp_composite_filter
        timestamp_composite_filter.add_filter(last_timestamp_range_filter)

    last_timestamp_range_filter.high = before_timestamp

@filter_switches.register("-a", "--after", action="append", type=timestamp_parser)
def after_switch(after_timestamp):
    '''
    add a time lower bound filter
    '''
    global last_timestamp_range_filter
    global timestamp_composite_filter

    if last_timestamp_range_filter is None or last_timestamp_range_filter.low is not None:
        last_timestamp_range_filter = dump_filters_base.RangeFilter('timestamp', field_type=datetime.datetime, register=False) # this filter is registered through timestamp_composite_filter
        timestamp_composite_filter.add_filter(last_timestamp_range_filter)

    last_timestamp_range_filter.low = after_timestamp

### host id filter

host_id_filter = dump_filters_base.InFilter('host_id', sql_serialization.HostId)

@filter_switches.register("-i", "--host-id", action='append')
def host_id_filter_handler(host_id):
    '''
    restrict the query to an explicit list of host ids.
    '''
    host_id_filter.add_item(host_id)

### process name filter
class ProcessNameFilter(dump_filters_base.LargeInFilter):
    def __init__(self, secdo_process_id_field='secdoProcessId', process_name_field='processName',  process_creation_table="process_instance_start", **kwargs):
        super().__init__((secdo_process_id_field, sql_serialization.ProcessName), **kwargs)
        self.process_names = set()
        self.process_creation_table = process_creation_table
        self.secdo_process_id_field = secdo_process_id_field
        self.process_name_field = process_name_field

    def add_names(self, *names):
        self.process_names.update((n.lower() for n in names))

    def _get_init_queries(self):
        if len(self.process_names) == 0:
            return None

        secdo_process_id_field = self.serializer.to_lower_field(self.secdo_process_id_field)
        process_name_field = self.serializer.to_lower_field(self.process_name_field)

        return f"""INSERT INTO {self.raw_data_table}
                   SELECT DISTINCT {secdo_process_id_field}
                   FROM {self.process_creation_table}
                   WHERE {process_name_field} IN (""" + ", ".join((f"'{name}'" for name in self.process_names)) + ")"

    def _get_filter(self):
        if len(self.process_names) == 0:
            return None

        return super()._get_filter()

    def _load_temp_tables(self):
        if len(self.process_names) == 0:
            return None

        return super()._load_temp_tables()

pname_filter = ProcessNameFilter()

@filter_switches.register("--pname", action='append')
def proc_name_filter(proc_name):
    global pname_filter
    pname_filter.add_names(proc_name)

@filter_switches.register("--pname-file", action='append')
def proc_name_file_filter(proc_name_file):
    global pname_filter

    if 'standard' == proc_name_file:
        proc_name_file = pathlib.Path(__file__).parent/'standard-processes'

    with open(proc_name_file, "rt") as proc_file:
        pname_filter.add_names(*[p.strip() for p in proc_file])

### secdo process id filter

class SecdoProcessIdFilter(dump_filters_base.InFilter):
    def __init__(self, secdo_process_id_field='secdoProcessId', **kwargs):
        super().__init__(secdo_process_id_field, **kwargs)

    def _flatten_multi_pid(self, multi_pid):
        if multi_pid[0] == '!':
            return [hashlib.md5(multi_pid[1:].encode()).hexdigest()]

        return multi_pid.lower().split(",")

    def add_content(self, content):
        super().add_content({
            pid for multi_pid in content for pid in self._flatten_multi_pid(multi_pid)
        })

secdo_pid_filter = SecdoProcessIdFilter()

@filter_switches.register("--pid", action='append')
def pid_filter(multi_pid):
    '''
    add a filter by secdo process id.
    use the following formats:
        * <secdo pid 1>,<secdo pid 2>,...
        * !<path to generate a secdo proc id for>
    '''
    secdo_pid_filter.add_item(multi_pid)

### instance filter

instances_filter = dump_filters_base.LargeLocalContentInFilter('hostId', 'secdoProcessId', ('instanceId', int), name="instance_filter")

@filter_switches.register("--instance", action='append')
def instance_switch(instance_id_tuple):
    host, secdo_pid, instance_id = tuple(instance_id_tuple.split(","))
    instances_filter.add_item((host, secdo_pid, int(instance_id)))
    return instance_id_tuple

@filter_switches.register("--instances-file", action='append')
def instances_file_switch(instance_id_csv_file_name):
    with open(instance_id_csv_file_name, "rt") as instance_id_csv_file:
        csv_reader = csv.reader(instance_id_csv_file, delimiter='\t')
        instances_filter.add_content([(host, secdo_pid, int(instance_id)) for host, secdo_pid, instance_id in csv_reader])

    return  instance_id_csv_file_name

### os filter

class OsFilter(dump_filters_base.FieldInQueryFilter):
    options = ("windows", "osx", "linux")
    def __init__(self, hosts_table="hosts", host_id_col="hostId", os_name_col="OS_Name", **kwargs):
        super().__init__(host_id_col, str, **kwargs)
        self.os_list = set()
        self.hosts_table = hosts_table
        self.host_id_col = host_id_col
        self.os_name_col = os_name_col

    def _get_in_query(self):
        if len(self.os_list) == 0:
            return None

        os_constraint = dump_filters_base.OrFilter(register=False)
        for os_type in self.os_list:
            os_constraint.add_filter(dump_filters_base.RawFilter(f"{self.serializer.to_lower_field(self.os_name_col)} like '%{os_type}%'", register=False))
        return f"SELECT DISTINCT {self.host_id_col} FROM {self.hosts_table} WHERE {os_constraint.get_filter()}"

    def add_os(self, os_name):
        assert os_name.lower() in self.options
        self.os_list.add(os_name.lower())

os_filter = OsFilter()

@filter_switches.register("--os", action='append', choices=OsFilter.options)
def os_constraint(os_name):
    global os_filter
    os_filter.add_os(os_name)
    return os_name

### test

def test():
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s [%(levelname)s] %(message)s')

    parser = argparse.ArgumentParser(description="test the resultant filters generated by the registered filters.")
    filter_switches.apply(parser)
    parser.parse_args()

    filters = dump_filters_base.get_filters()

    print("filters:")
    for flt in filters:
        query = flt.get_filter()
        if query is not None:
            print("\t", query)

    print("")
    print("temp tables:")
    for flt in filters:
        for tbl in flt.load_temp_tables():
            dump_filters_base.print_temp_table_desc(tbl)
            print("")

    print("")
    print("join clauses:")
    for flt in filters:
        for join_clause in flt.join_clauses('LEFT_TABLE'):
            dump_filters_base.print_join_clause(join_clause)
            print("")

    print("")
    print("init queries:")
    for flt in filters:
        for query in flt.get_init_queries():
            query = "\t" + sqlparse.format(query, reindent=True, keyword_case='upper').replace("\n","\n\t").strip()
            print(query)
            print("")

if __name__ == "__main__":
    sys.exit(test())

