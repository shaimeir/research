#!/usr/bin/env python3

"""
auto generate dump
"""

import argparse
import contextlib
import sys
import sqlparse
import termcolor
import getpass
import re
import logging
import csv
import tempfile
import io
from recordclass import recordclass
from collections import OrderedDict, defaultdict

import dbcursors
from dbcursors import default_dbs
from utilities import argparse_config_file_support, call_closure

import dump_filters
import query_augmentation
import scheme

MAIN_QUERY_NAME = "main_query"

def generate_dump_query(db_table, column_translation, query_filters, extra_available_columns=set(), verify_filter=False):
    '''
    generate a query that dumps the information from a table.
    this is used twice - once to dump the data from the scheme table and again to build an
    augmentation query that will be used to add extra information to the scheme dump.

        db_table - the database table to dump the information from.
        column_translation - {"output column name" : "database column name"}.
                             if the value is None then there no AS clause is generated for the column.
        query_filters - command line filters to apply.
        extra_available_columns - a list/set/tuple/whatever of additional columns available in the table dumped (used
                                  when verifying the filters).
        verify_filter - remove all filters that take irrelevant columns into account.
                        filters with no specified columns are also removed.
                        filters that access both available and unavailable columns generate an exception.
    '''
    if isinstance(column_translation, (tuple, set, list)):
        column_translation = dict(zip(column_translation, [None] * len(column_translation)))

    # generate the basic select
    db_table_prefix = db_table + ("." if db_table != "" else "")
    query = "SELECT " + ", ".join((col if db_col is None else f'{db_table_prefix}{db_col} AS "{col}"' for col, db_col in column_translation.items()))
    query += f" FROM {db_table}"

    if verify_filter:
        available_cols = {col for col in column_translation.keys()} | set(extra_available_columns)
        filtered_query_filters = []
        for query_filter in query_filters:
            if available_cols.issuperset(query_filter.affected_columns):
                filtered_query_filters.append(query_filter)
            else:
                assert len(available_cols.intersection(query_filter.affected_columns)), ValueError('verification of filters failed - a mixed filter detected', db_table, available_cols, query_filter.affected_columns)

        query_filters = filtered_query_filters

    # add join filters
    join_clauses = [join_clause for flt in query_filters for join_clause in flt.join_clauses(db_table)]
    if len(join_clauses) > 0:
        for join_clause in join_clauses:
            query += f" INNER JOIN {join_clause.right_table}"

            if len(join_clause.conditions) > 0:
                conditions_clause = dump_filters.dump_filters_base.AndFilter(register=False)
                conditions_clause.add_filter(*[dump_filters.dump_filters_base.RawFilter(condition, register=False) for condition in join_clause.conditions])
                query += " ON " + conditions_clause.get_filter()

    # add the basic filters
    if len(query_filters) > 0:
        joined_filters = dump_filters.dump_filters_base.AndFilter(register=False) # make sure this filter doesn't reference itself
        joined_filters.add_filter(*query_filters)
        filter_clause = joined_filters.get_filter()
        if filter_clause is not None:
            query += f" WHERE {filter_clause}"

    return query

def augment_query(main_query_name, query, augmentors):
    if len(augmentors) == 0:
        return query

    new_query = f"WITH {main_query_name} AS ({query})"

    # just collect the augmentation criteria
    augmentations = defaultdict(lambda : {'aug_query' : None, 'join_clauses' : [], 'columns' : OrderedDict()})
    join_clause_found = False
    for augmentor in augmentors:
        for table, added_columns in augmentor.get_added_columns(False).items():
            augmentations[table]['columns'].update(added_columns)

        for join_clause in augmentor.join_clauses(main_query_name):
            join_clause_found = True
            augmentations[join_clause.right_table]['join_clauses'].append(join_clause)
            augmentations[join_clause.right_table]['columns'].update({
                col_name : col_name
                for col_name in join_clause.right_columns
            })

        for table, augmentation_query in augmentor.get_augmentation_queries().items():
            augmentations[table]['aug_query'] = augmentation_query

    if not join_clause_found:
        return query

    assert all((aug['aug_query'] is not None and len(aug['join_clauses']) > 0 for aug in augmentations.values())), ValueError('found an augmentation table with no augmentation query or join clause', augmentations)

    # add all the base queries that the augmentation queries rely on.
    for augmentor in augmentors:
        for base_query_name, base_query in augmentor.get_base_queries().items():
            new_query += f", {base_query_name} AS ({base_query})"

    # build all the augmentation queries (the fetching of extra information).
    augmentation_queries = []
    sql_join_clauses = []
    for table, augmentation in augmentations.items():
        aug_query = augmentation['aug_query']
        join_clauses = augmentation['join_clauses']
        columns = augmentation['columns']

        # construct the columns clause
        conditions_clause = dump_filters.dump_filters_base.AndFilter(register=False)
        filters_clause = dump_filters.dump_filters_base.AndFilter(register=False)
        for join_clause in join_clauses:
            conditions_clause.add_filter(*[dump_filters.dump_filters_base.RawFilter(cond) for cond in join_clause.conditions])
            filters_clause.add_filter(*join_clause.filters)

        # the distinct here is very important as the augmentation will otherwise have duplicate entries (the same amount as
        # the relevant entries in the main query). this in turn will cause the augmentation in the finishing query to square the number of total entries!
        columns_clause = ", ".join((f"{table}.{col_name}" for col_name in columns.keys()))

        # the augmentation query can simply be a table name or it can be a query.
        # if it is a query, it needs to be enclosed in parentheses.
        # if it's not a query, but a table name, then it mustn't be enclosed when formulating the query.
        if re.search(r"\s", aug_query.strip()) is not None:
            aug_query = "(" + aug_query + ")"

        aug_table_query = f"SELECT DISTINCT {columns_clause} FROM {aug_query} {table} INNER JOIN {main_query_name}"

        if conditions_clause.filter_count() > 0:
            cond_clause = conditions_clause.get_filter()
            aug_table_query += f" ON {cond_clause}"

        if filters_clause.filter_count() > 0:
            flt_clause = filters_clause.get_filter()
            aug_table_query += f" WHERE {flt_clause}"

        new_query += f", {table} AS ({aug_table_query})"

    # build the nested join chains
    nested_query = main_query_name
    query_name = main_query_name
    nested_query_counter = 1
    for augmentor in augmentors:
        columns = ", ".join([f'{col_name} AS "{col_alias}"' for table, added_columns in augmentor.get_added_columns().items() for col_name, col_alias in added_columns.items() ] + [f"{query_name}.*"])
        nested_query = f"""
            SELECT {columns}
            FROM {nested_query}
        """

        for join_clause in augmentor.join_clauses(query_name):
            nested_query += f" LEFT JOIN {join_clause.right_table}"
            conditions = join_clause.conditions
            if len(conditions) > 0:
                conditions_clause = dump_filters.dump_filters_base.AndFilter(register=False)
                conditions_clause.add_filter(*(dump_filters.dump_filters_base.RawFilter(cond) for cond in conditions))
                nested_query += f" ON {conditions_clause.get_filter()}"

        nested_query_counter += 1
        query_name = f"nested_augmentation_query_{nested_query_counter}"
        nested_query = f"({nested_query}) {query_name}"

    new_query += f' SELECT * FROM {nested_query}'

    return new_query

def generate_dump_query_from_scheme(scheme, query_filters):
    return generate_dump_query(scheme.db_table, scheme.column_translation(), query_filters)

def dump_scheme(cursor, scheme, output_stream, query_filters, augmentors, use_header, use_marker, marker_text, verbose):
    # first we'll create all the temp tables
    logging.info("initializing query filters")
    for query_filter in query_filters:
        for temp_table_desc in query_filter.load_temp_tables():
            logging.info("creating temp table: %s", temp_table_desc.table)
            # create the table
            columns = OrderedDict([(col_name, col_type.db_type) for col_name, col_type in temp_table_desc.columns.items()])
            cursor.create_temp_table(temp_table_desc.table, columns)
            # fill it up with whatever data we have loaded locally
            cursor.bulk_insert(temp_table_desc.table, temp_table_desc.content) # the filters pre-format everything so don't reformat
            cursor.fetchall()

        for init_query in query_filter.get_init_queries():
            # run initialization queries of the filters (usually initialization of temp tables with data from other tables)
            logging.info("running init query:\n%s", termcolor.colored(sqlparse.format(init_query, reindent=True, keyword_case='upper'), 'blue'))
            cursor.execute(init_query)
            cursor.fetchall()

    logging.info("finished initializing query filters")

    # and initialize all the augmentors as well
    logging.info("initializing query augmentors")
    for augmentor in augmentors:
        for temp_table_desc in augmentor.load_temp_tables():
            # create the table
            columns = OrderedDict([(col_name, col_type.db_type) for col_name, col_type in temp_table_desc.columns.items()])
            logging.info("creating temp table: %s", temp_table_desc.table)
            cursor.create_temp_table(temp_table_desc.table, columns)
            # fill it up with whatever data we have loaded locally
            cursor.bulk_insert(temp_table_desc.table, temp_table_desc.content) # the filters pre-format everything so don't reformat
            cursor.fetchall()

        for init_query in augmentor.get_init_queries():
            # run initialization queries of the filters (usually initialization of temp tables with data from other tables)
            logging.info("running init query:\n%s", termcolor.colored(sqlparse.format(init_query, reindent=True, keyword_case='upper'), 'blue'))
            cursor.execute(init_query)
            cursor.fetchall()

    logging.info("finished initializing query augmentors")

    # make sure all the uploaded data is well-loaded and committed so the following queries will succeed
    cursor.commit()

    if use_marker:
        logging.info("generating marker: %s", marker_text)
        marker_text = tempfile.mktemp(prefix=marker_text+"_", dir='')
        cursor.execute(f"SELECT '{marker_text}'")
        marker = cursor.fetchall()
        logging.info("marker query returned: %r", marker)

    # make sure we discard everything on the input queue when we write to the output stream
    cursor.fetchall()

    # create the columns order
    column_translation = scheme.column_translation()

    columns = list(column_translation.keys())
    # if we have augmentations prepend their columns as that's the order the augmentations will be generated in the SQL
    # query (the first augmentor nested at the lowest level and the last at the top level having their added columns added
    # at the beginning of every column)
    for augmentor in augmentors:
        for table, added_columns in augmentor.get_added_columns(False).items():
            columns = list(added_columns.values()) + columns
    columns = tuple(columns)
    logging.info("columns order: %s", ", ".join(columns))

    if use_header:
        logging.info("generating header")
        cursor.execute("SELECT " +", ".join((f"'{col}' AS \"{col}\"" for col in columns)))
        header = cursor.fetchall()
        if len(header) > 0: # for a printer cursor this will be 0 so it's an acceptible output
            logging.info("raw header = %r", header)
            header = header[-1]
            output_stream.writerow((header[col] for col in columns))
        else:
            logging.info("header query didn't return anything")

    logging.info("composing dump query")
    dump_query = generate_dump_query_from_scheme(scheme, query_filters)
    if len(augmentors) > 0:
        logging.info("augmenting dump query")
        dump_query = augment_query(MAIN_QUERY_NAME, dump_query, augmentors)

    logging.info("executing dump query")
    cursor.execute(dump_query)
    results = [None]
    order_is_valid = None

    logging.info("fetching data!")
    sample_printed = False
    lines_fetched = 0
    if verbose:
        print(f"\rfetched {lines_fetched} lines", end='', flush=True, file=sys.stderr)

    while True:
        logging.debug("fetching results...")
        results = cursor.fetchmany(1000)
        if len(results) == 0:
            break

        logging.debug("results fetched!")
        if verbose:
            lines_fetched += len(results)
            print(f"\rfetched {lines_fetched} lines", end='', flush=True, file=sys.stderr)

        if order_is_valid is None:
            logging.debug("evaluating columns order")
            res = results[0]
            order_is_valid = False

            # if the package returns an ordered dict or it's python3.6 then the resultant dict is ordered so we can verify
            # it's order and save some lookup time
            if isinstance(res, OrderedDict) or (sys.version_info.major >= 3 and sys.version_info.minor >= 6):
                order_is_valid = tuple(res.keys()) == columns

            loggind.debug("order validity: %r", order_is_valid)

        if not sample_printed and len(results) > 0:
            sample_printed = True
            logging.info("sample output: %r", results[0])

        logging.debug("writing rows")
        if order_is_valid:
            output_stream.writerows((line.values() for line in results))
        else:
            output_stream.writerows(([line[col] for col in columns] for line in results))

    logging.info("dump query finished")

@contextlib.contextmanager
def auto_yield(o):
    yield o

def main():
    db_defaults = default_dbs.load_dbs_from_command_line(long_switch=True)

    parser = argparse_config_file_support.ConfigSupportedArgumentParser(description="dump network entries information to a CSV file.")
    default_dbs.add_standard_command_line_arg(parser, long_switch=True)

    parser.add_argument("-d", "--db", required=True, choices=list(db_defaults.keys()), help="the database to dump the information from.")
    parser.add_argument("-o", "--output", default='-', help="the output CSV file. use '-' for stdout.")
    parser.add_argument("-v", "--verbose", action="store_true", help="show debug prints.")
    parser.add_argument("-V", "--extra-verbose", action="store_true", help="show extra debug prints.")
    parser.add_argument("-s", "--scheme", required=True, help="the scheme to dump.\navailable defaults are: " + ", ".join(scheme.get_default_schemes_list()))
    parser.add_argument("-x", "--execute", action='store_true', help='by default queries are printed to the output stream. use this switch to actually connect to the db and execute them.')
    parser.add_argument("-n", "--no-augmentation", action='store_true', help="don't augment output at all")
    parser.add_argument("-k", "--skip-augmentation", action='append', default=[], choices=query_augmentation.query_augmentors.choices(), help="skip a specific augmentation." + query_augmentation.query_augmentors.doc())
    parser.add_argument("--no-header", action='store_true', help='don\'t yield a header line in the output.')
    parser.add_argument("--no-marker", action='store_true', help="don't yield a marker query int the output.")
    parser.add_argument("--marker-text", default="SCHEME_DUMP_MARKER", help="the text prefix to use as a marker line.")
    parser.add_argument("-c", "--comma-separation", action="store_true", help="use comma to separate the output")

    dump_filters.filter_switches.apply(parser)
    args = parser.parse_args()

    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s", level=logging.DEBUG if args.extra_verbose else logging.INFO if args.verbose else logging.WARN)

    db = db_defaults[args.db]
    table_scheme = scheme.load_default_scheme_file(args.scheme)

    assert table_scheme.db_table is not None, ValueError('scheme isn\'t tied to a database table', args.scheme)

    cursor = dbcursors.get_cursor_factory(db['dbtype'], db['dbtype'] if args.execute else 'print')(
        password=(db['password'] if 'password' in db else getpass.getpass('Database password: ')) if args.execute else '',
        **db
    )

    dump_filters.set_filter_serializer(cursor.serializer)

    # now build all the filters!
    dump_filters.consume_filters(args)

    # now we can build the augmentors
    columns_translation_dict = table_scheme.inverse_column_translation()
    augmentors = [] if args.no_augmentation else [
                    query_augmentation.query_augmentors.option(augmentor_name)(columns_translation_dict, MAIN_QUERY_NAME)
                    for augmentor_name in query_augmentation.query_augmentors.choices()
                    if augmentor_name not in args.skip_augmentation
                 ]

    with ((auto_yield(sys.stdout) if '-' == args.output else (open(args.output, "w")) if args.execute else auto_yield(io.StringIO()))) as output_stream:
        output_csv = csv.writer(output_stream, delimiter="," if args.comma_separation else "\t", quoting=csv.QUOTE_MINIMAL, dialect='unix')
        with cursor.context():
            dump_scheme(dbcursors.trace_cursor(cursor),
                        table_scheme,
                        output_csv,
                        dump_filters.get_filters(),
                        augmentors,
                        not args.no_header,
                        not args.no_marker,
                        args.marker_text,
                        args.verbose or args.extra_verbose)

if __name__ == "__main__":
    with contextlib.suppress(BrokenPipeError):
        sys.exit(main())

    sys.exit(1) # broken pipe...


