#!/usr/bin/env python3

'''
this file deals with some abstractions of filters (WHERE clause conditions, JOIN conditions, etc...) that will be used when auto-generating SQL
queries for dumping one of the content tables (file system, network, registry, etc...).
'''

from recordclass import recordclass
from collections import defaultdict, OrderedDict
import datetime
import logging
import sqlparse
import tempfile
import re
from dbcursors import sql_serialization

def get_filters():
    return [flt for flt in DumpFilter.filters_registry] # so if DumpFilter.filters_registry changes then the resultant list won't change as well

_temp_index = 0
_temp_marker = tempfile.mktemp(prefix='', suffix='', dir='')
def _get_temp_name(prefix):
    global _temp_index, _temp_marker

    _temp_index += 1
    name = f"ResearchTable_{prefix}_{_temp_marker}_{_temp_index}"

    return name

# a descriptor to hold the information for a temporary table
TempTableDescriptor = recordclass('TempTableDescriptor',
                                    (
                                        'table',   # temp table name
                                        'columns', # the table's columns. a dict from column name to it's class
                                        'content', # the content to upload to the table
                                    )
                            )

# a descriptor to hold the information for a join operation.
# this class is designed like this for compatability with a previously defined recordclass...
class TableJoinDescriptor:
    def __init__(self, left_table=None, right_table=None, left_columns=None, right_columns=None, conditions=None, filters=None, case_sensitive=True):
        self._real_right_table = right_table
        self._real_left_table = left_table

        self._right_table = right_table
        self._left_table = left_table

        serializer = self.serializer
        is_lowerable = lambda col_type: False if col_type is None else serializer.is_lowerable(col_type)

        left_columns = [] if left_columns is None else left_columns
        if not isinstance(left_columns, dict):
            left_columns = OrderedDict([(f[0], is_lowerable(f[1])) if isinstance(f, (list, tuple)) else (f, False) for f in left_columns])
        else:
            left_columns = OrderedDict([(col, is_lowerable(col_type)) for col, col_type in left_columns.items()])

        right_columns = [] if right_columns is None else right_columns
        if not isinstance(right_columns, dict):
            right_columns = OrderedDict([(f[0], is_lowerable(f[1])) if isinstance(f, (list, tuple)) else (f, False) for f in right_columns])
        else:
            right_columns = OrderedDict([(col, is_lowerable(col_type)) for col, col_type in right_columns.items()])

        self.left_columns = OrderedDict(left_columns)
        self.right_columns = OrderedDict(right_columns)

        self._conditions = conditions

        self._filters = filters if isinstance(filters, (list, tuple)) else ([] if filters is None else [filters])
        self._filters = [RawFilter(flt) if isinstance(flt, str) else flt for flt in self._filters]
        assert all((isinstance(flt, (DumpFilter)) for flt in self._filters)), TypeError('invalidly typed filter found', self._filters)

        self.case_sensitive = case_sensitive

    @property
    def serializer(self):
        return DumpFilter.serializer

    @property
    def left_table(self):
        return self._left_table

    @property
    def right_table(self):
        return self._right_table

    @property
    def conditions(self):
        if self._conditions is None:
            left_columns = self.right_columns if len(self.left_columns) == 0 else self.left_columns
            right_columns = self.left_columns if len(self.right_columns) == 0 else self.right_columns

            if left_columns is None: # this means both left columns and right columns are none
                return []

            left_table_prefix = "" if self._left_table is None or self._left_table == "" else f"{self.left_table}."
            right_table_prefix = "" if self._right_table is None or self._right_table == "" else f"{self.right_table}."

            serializer = self.serializer
            to_lower = (lambda field, field_name: field) if self.case_sensitive else (lambda field, field_name: (serializer.to_lower_field(field) if left_columns[field_name] else field))

            return [
                to_lower(f"{left_table_prefix}{lcol}", lcol) + f" = {right_table_prefix}{rcol}"
                for lcol, rcol in zip(left_columns.keys(), right_columns.keys())
            ]

        elif isinstance(self._conditions, str):
            return [self._conditions]

        else:
            return self._conditions

    @property
    def filters(self):
        return self._filters

def set_filter_serializer(serializer):
    DumpFilter.serializer = serializer

class DumpFilter:
    '''
    a query filter abstraction.
    '''

    # the global serializer that will be used to serialize the filters into SQL queries.
    # initialized with defaults but globally settable via set_filter_serializer.
    serializer = sql_serialization.get_default_query_serializer()

    # a list of all the filters instanciated
    filters_registry = []

    def __init__(self, *affected_fields, register=True, max_recursion=1, default_type=str):
        self.default_type = default_type
        self.affected_fields = []
        self.types = []
        self.field_types = OrderedDict()
        self.field = None
        self.field_type = None
        self.lower_columns = set()

        self._add_affected_fields(affected_fields)

        # recursion means how many times this filter will be valid when invoked.
        # some queries are executed multiple times and this counter elaborates
        # on how many times this filter returns an actual filter.
        # set self.max_recursion to None to ignore the recursion counter.
        self.recursion_level = 0
        self.max_recursion = max_recursion

        # register this filter with the global registry
        if register:
            self.filters_registry.append(self)

    def _add_affected_fields(self, affected_fields):
        affected_fields = [tuple(field) if isinstance(field, (list, tuple)) else (field, self.default_type) for field in affected_fields]
        affected_fields = [(field, field_type) if isinstance(field_type, (type(None), sql_serialization.TypeAssociationTuple)) else (field, self.serializer.get_db_type(field_type)) for field, field_type in affected_fields]

        self.field_types.update(OrderedDict(affected_fields))
        self.affected_fields = list(self.field_types.keys())
        self.types = list(self.field_types.values())

        # for convinience
        self.field = None if len(self.affected_fields) == 0 else self.affected_fields[0]
        self.field_type = None if len(self.types) == 0 else self.types[0]

        self.lower_columns = {i for i, col_type in enumerate(self.types) if col_type is not None and self.serializer.is_lowerable(col_type.type)}

    def _apply_default_type(self, default_type):
        self.default_type = default_type
        self._add_affected_fields([field for field, field_type in self.field_types.items() if field_type is None])

    def get_filter(self):
        '''
        add recursion depth validation to _get_filter.
        '''
        # some filters will be applied recursively
        if self.max_recursion is not None and self.recursion_level >= self.max_recursion:
            return None

        self.recursion_level += 1 # this is meaningless if self.max_recursion is None
        query = self._get_filter()
        if query is not None:
            query = query.strip()
        return None if query == "" else query

    def set_recursion(self, max_recursion=None):
        self.max_recursion = max_recursion

    # a utility method to generate a temp
    def _get_temp_table_name(self, table_name=''):
        return _get_temp_name(table_name)

    def load_temp_tables(self):
        '''
        augment the result of _load_temp_tables with some defaults.
        '''
        temp_tables = self._load_temp_tables()

        if temp_tables is None:
            return []

        if isinstance(temp_tables, TempTableDescriptor):
            return [temp_tables]

        return temp_tables

    def join_clauses(self, left_table, alt_right_table=None):
        '''
        augment the result of _join_clauses with some defaults.
        '''
        join_clauses = self._join_clauses(left_table)
        if join_clauses is None:
            return []

        if isinstance(join_clauses, TableJoinDescriptor):
            join_clauses = [join_clauses]

        if len(join_clauses) == 0:
            return[]

        for clause in join_clauses:
            assert clause.right_table is not None, ValueError('join clause must specify a right-side table for the join operation')

            if clause.left_columns is None:
                clause.left_columns = self.affected_fields

            if clause.right_columns is None:
                clause.right_columns = clause.left_columns

        return join_clauses

    def get_init_queries(self):
        '''
        augment _get_init_queries with some defaults.
        '''
        queries = self._get_init_queries()

        if queries is None:
            return []

        if isinstance(queries, str):
            queries = [queries]

        return [query.strip().strip(";") + ";" for query in queries]

    def _join_clauses(self, left_table):
        '''
        add a join clause to the originally queried table.
        '''
        return None

    def _load_temp_tables(self):
        '''
        returns a table name, column names, column types and content to load into a temp table wrapped in a TempTableDescriptor.
        '''
        return None

    def _get_filter(self):
        '''
        returns a 'WHERE' clause condition.
        '''
        return None

    def _get_init_queries(self):
        '''
        some filters require dedicated SQL queries for initialization (like filling up one of the temp tables with values from the db).
        this method should return a list of these queries.
        '''
        return None

class RawFilter(DumpFilter):
    '''
    a constant string filter. this field obviously won't support the affected columns functionality.
    '''
    def __init__(self, filter_query, **kwargs):
        super().__init__(**kwargs)
        self.filter_query = filter_query

    def _get_filter(self):
        return self.filter_query

class OrFilter(DumpFilter):
    '''
    a filter which concatenates multiple filters using an OR construct.
    '''
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        super().set_recursion() # remove recursion limits
        self.filters = []

    def filter_count(self):
        return len(self.filters)

    def set_recursion(self, max_recursion):
        pass

    def add_filter(self, *filters):
        self.filters += list(filters)
        self._add_affected_fields([(col, col_type) for flt in filters for col, col_type in flt.field_types.items()])

    def _get_filter(self):
        filter_queries = [f"({flt_query.strip()})" for flt_query in (flt.get_filter() for flt in self.filters) if flt_query is not None and flt_query.strip() != ""]
        if len(filter_queries) == 0: # post removal of queries with None filter
            return None

        return "(" + " OR ".join(filter_queries) + ")"

class AndFilter(DumpFilter):
    '''
    a filter which concatenates multiple filters using an AND construct.
    '''
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        super().set_recursion() # remove recursion limits
        self.filters = []

    def filter_count(self):
        return len(self.filters)

    def set_recursion(self, max_recursion):
        pass

    def add_filter(self, *filters):
        self.filters += list(filters)
        self._add_affected_fields([(col, col_type) for flt in filters for col, col_type in flt.field_types.items()])

    def _get_filter(self):
        filter_queries = [f"({flt_query.strip()})" for flt_query in (flt.get_filter() for flt in self.filters) if flt_query is not None and flt_query.strip() != ""]
        if len(filter_queries) == 0: # post removal of queries with None filter
            return None

        return "(" + " AND ".join(filter_queries) + ")"

class RangeFilter(DumpFilter):
    '''
    an encapsulation of a field range filter.
    check if something is between an optional low and an optional high values.
    '''
    def __init__(self, field, field_type=str, low=None, high=None, **kwargs):
        super().__init__((field, field_type), **kwargs)
        self.low = low
        self.high = high

    def _get_filter(self):
        if self.low is None and self.high is None:
            return None

        _filter = []

        if self.low is not None:
            _filter.append("(" + self.serializer.serialize(self.low) + " <= " + self.field + ")")

        if self.high is not None:
            _filter.append("(" + self.field + " <= " + self.serializer.serialize(self.high) + ")")

        return " AND ".join(_filter)

class InTableFilter(DumpFilter):
    def __init__(self, field, table, table_col="*", case_sensitive=False, **kwargs):
        super().__init__((field, str), max_recursion=None, **kwargs)
        self.table_col = table_col
        self.table = table
        self.inspected_field = self.serializer.to_lower_field(field) if case_sensitive else field

    def _get_filter(self):
        return f"{self.field} IN (SELECT {self.table_col} FROM {self.table})"

class InFilter(DumpFilter):
    '''
    an encapsulation of a 'field in a value list' filter.
    this only supports a single field test because MemSQL sucks...
    '''

    def __init__(self, field, field_type=None, case_sensitive=False, relaxed=True, **kwargs):
        super().__init__((field, field_type), default_type=None, **kwargs)
        self.type_inferral = (self.field_type is None)
        self.case_sensitive = case_sensitive
        self.relaxed = relaxed

        self.content = set()

    def add_content(self, content):
        self.content.update((c.lower() for c in content) if not self.case_sensitive and self.field_type is str else content)

    def add_item(self, item):
        self.add_content({item})

    def _infer_type(self):
        # note that even for fields that are process path/name or command line it's fine to infer
        # them as strings as the output query will be the same.

        if not self.type_inferral:
            return True

        if self.field_type is not None:
            self.type_inferral = False
            return True

        if len(self.content) == 0 and self.relaxed: # don't raise an exception if you can't infer the data type in relaxed mode
            return False

        assert len(self.content) > 0, ValueError('can\'t infer data type with no content')

        # this is an idiom for fetching some item from a set.
        # this is used because you can't index a set.
        for value in self.content:
            break

        self._add_affected_fields([(self.field, self.serializer.get_db_type(value))])
        self.type_inferral = False

        return True

    def _get_filter(self):
        # in relaxed mode just don't generate an exception if there's no data to infer from
        if not self._infer_type():
            return None

        if len(self.content) == 0:
            return None

        field = self.field
        in_clause = ", ".join((self.serializer.serialize(c) for c in self.content))
        if not self.case_sensitive and self.serializer.is_lowerable(self.field_type.type):
            field = self.serializer.to_lower_field(field)
            in_clause = in_clause.lower()

        return f"{field} IN (" + in_clause + ")"

class LargeInFilter(DumpFilter):
    '''
    an encapsulation of a 'field in a large value list' filter.
    this filter uses a temp folder for it's going ons and may be unsuitable for small comparison
    (causing join overheads).

    this is principally here to support "IN" clauses
    '''

    def __init__(self, *fields, case_sensitive=False, name=None, default_type=str, relaxed=False, **kwargs):
        super().__init__(*fields, default_type=None, **kwargs)

        # the first type inferral occurres during construction and we're not given a chance to infer the
        # types from the content so we'll save the default type until we can have data to infer from
        self.real_default_type = default_type

        self.case_sensitive = case_sensitive
        self.lower_columns = set()
        self.content = set()

        self.type_inferral = None in self.types
        self.multicolumn = (len(fields) > 1)

        # the name of the temp table to hold the IN values
        self.raw_data_table = self._get_temp_table_name(type(self).__name__ if name is None else name)
        self.concatenated_columns_table = self._get_temp_table_name(type(self).__name__ if name is None else name)

    def add_content(self, content):
        if self.case_sensitive:
            self.content.update(content)
        else:
            self.content.update({self._to_lower(row) for row in content})

    def _to_lower(self, row):
        if isinstance(row, str):
            return row.lower()

        elif isinstance(row, (list, tuple)):
            return tuple((v.lower() if isinstance(v, str) else v for v in row))

        else:
            return row

    def add_item(self, item):
        self.add_content({item})

    def _infer_types(self):
        # note that even for fields that are process path/name or command line it's fine to infer
        # them as strings as the output query will be the same.

        if not self.type_inferral:
            return

        if len(self.content) == 0:
            self._apply_default_type(self.real_default_type)
            self.type_inferral = False
            return

        # this is an idiom for fetching some item from a set.
        # this is used because you can't index a set
        for value in self.content:
            break

        assert len(value) == len(self.types), ValueError('not enough values to infer types', value, self.fields, self.types)
        assert len({len(v) for v in self.content}) == 1, ValueError('content of varying lengths')

        self._add_affected_fields([(self.affected_fields[i], self.serializer.get_db_type(value[i])) for i, _type in enumerate(self.types) if _type is None])
        self._apply_default_type(self.real_default_type)

    def _load_temp_tables(self):
        self._infer_types()
        assert None not in self.types

        temp_tables = [TempTableDescriptor(self.raw_data_table, self.field_types, self.content)]

        if self.multicolumn:
            temp_tables.append(TempTableDescriptor(self.concatenated_columns_table, {'concatenated' : sql_serialization.LargeString}, []))

        return temp_tables

    def _get_init_queries(self):
        if not self.multicolumn:
            return None

        field = self.serializer.to_lower_field(self.serializer.concat_ws('|', *self.affected_fields))
        return f"""
            INSERT INTO {self.concatenated_columns_table}
            SELECT DISTINCT {field} FROM {self.raw_data_table}
        """

    def _get_filter(self):
        field = self.field
        field_type = self.field_type
        table = self.raw_data_table

        if self.multicolumn:
            table = self.concatenated_columns_table
            field = self.serializer.to_lower_field(self.serializer.concat_ws('|', *self.affected_fields))
            field_type = str

        if not self.case_sensitive and self.serializer.is_lowerable(field_type):
            field = self.serializer.to_lower_field(field)

        return f"""
            {field} IN (SELECT * FROM {table})
        """

class LargeLocalContentInFilter(LargeInFilter):
    '''
    a regular LargeInFilter doesn't validate that content was supplied before yielding the temp tables and the join clauses.
    it is useful for when the temp table is initialized via a query.

    this class asserts that content was supplied locally (via add_content) before yielding these items.
    '''

    def _load_temp_tables(self):
        if len(self.content) == 0:
            return None

        return super()._load_temp_tables()

    def _get_init_queries(self):
        if len(self.content) == 0:
            return None

        return super()._get_init_queries()

    def _get_filter(self):
        if len(self.content) == 0:
            return None

        return super()._get_filter()

class FieldInQueryFilter(DumpFilter):
    def __init__(self, field, field_type):
        super().__init__((field, field_type))

    def _get_in_query(self):
        raise NotImplementedError('_get_in_query() not implemented')

    def _get_filter(self):
        in_query = self._get_in_query()
        if in_query is None:
            return None

        return f"{self.field} IN ({in_query})"

### test

def print_temp_table_desc(temp_table, prefix="\t"):
    print(prefix, f"temp table '{temp_table.table}'")

    print(prefix, "\t", f"columns:")
    for col, tp in temp_table.columns.items():
        print(prefix, "\t\t", col, tp)

    print(prefix, "\t", f"content:")
    for row in temp_table.content:
        print(prefix, "\t\t", ', '.join(map(str, row)))

def print_join_clause(join_descriptor, prefix="\t"):
    print(prefix, f"join clause with {join_descriptor.right_table}")

    print(prefix, "\t", "left columns:")
    for col in join_descriptor.left_columns:
        print(prefix, "\t\t", col)

    print(prefix, "\t", "conditions:")
    for condition in join_descriptor.conditions:
        print(prefix, "\t\t", condition)

    print(prefix, "\t", "filters:")
    for flt in join_descriptor.filters:
        print(prefix, "\t\t", flt)

def test():
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s [%(levelname)s] %(message)s')

    print("range filter test:")
    rng_filt = RangeFilter('timestamp')
    rng_filt.set_recursion(1000) # just so we won't bog down during the test...
    print("\t", rng_filt.get_filter())
    rng_filt.low = 'lower bound'
    print("\t", rng_filt.get_filter())
    rng_filt.high = 'upper bound'
    print("\t", rng_filt.get_filter())
    rng_filt.low = None
    print("\t", rng_filt.get_filter())

    print()
    print("in clause test:")
    in_filt = InFilter('host_id', relaxed=False)
    in_filt.set_recursion(1000)
    try:
        print("\t", in_filt.get_filter())
    except AssertionError as e:
        print("\t","caught expected assertion error!", e)
    in_filt.add_content(["HOST-1"])
    print("\t", in_filt.get_filter())
    in_filt.add_content(["HOST-2", "HOST-3"])
    print("\t", in_filt.get_filter())
    in_filt.case_sensitive = True

    print()
    print("in clause test: (case sensitive)")
    print("\t", in_filt.get_filter())

    print()
    print("large in clause test:")
    large_in_filt = LargeLocalContentInFilter('host_id', 'process_id', ('instance_id', int))
    large_in_filt.set_recursion(1000)
    assert len(large_in_filt.load_temp_tables()) == 0
    assert len(large_in_filt.join_clauses('input_table')) == 0
    print("passed no-content assertion")
    large_in_filt.add_content([("host-1", "proc-1", 1337)])
    print_temp_table_desc(large_in_filt.load_temp_tables()[0], prefix="temp-table one item: \t")
    print("\t", "filters:")
    print("\t" + sqlparse.format(large_in_filt.get_filter().strip().strip(";") + ";", reindent=True, keyword_case='upper').replace("\n","\n\t"))
    print()
    large_in_filt.add_content([("host-2", "proc-2", 1338), ("host-3", "proc-3", 1339)])
    print_temp_table_desc(large_in_filt.load_temp_tables()[0], prefix="temp-table three items: \t")
    # print()
    # print("large in clause join clause:")
    # print_join_clause(large_in_filt.join_clauses('input_table')[0], prefix="join clause: \t")
    # print()
    # print("large in clause join clause: (case sensitive)")
    # large_in_filt.case_sensitive = True
    # print_join_clause(large_in_filt.join_clauses('input_table')[0], prefix="join clause: \t")

    print()
    print("or clause test:")
    or_filter = OrFilter()
    assert or_filter.get_filter() is None
    print("passed no-content assertion")
    rng_filt = RangeFilter('timestamp')
    rng_filt.set_recursion(1000)
    rng_filt_nothing = RangeFilter('timestamp')
    rng_filt_nothing.set_recursion(1000)
    or_filter.add_filter(rng_filt, rng_filt_nothing)
    assert or_filter.get_filter() is None
    print("passed second no-content assertion")
    rng_filt.low = '2017-11-01'
    print("\tsingle constraint: \t", or_filter.get_filter())
    rng_filt = RangeFilter('timestamp')
    rng_filt.set_recursion(1000)
    rng_filt.high = '2017-12-01'
    or_filter.add_filter(rng_filt)
    print("\ttwo constraints: \t", or_filter.get_filter())
    in_filt = InFilter('host_id')
    in_filt.set_recursion(1000)
    in_filt.add_content(["host-1", "host-2", "host-3"])
    or_filter.add_filter(in_filt)
    print("\tthree constraints: \t", or_filter.get_filter())

    print()
    print("max recursion test (max 2):")
    rng_filt = RangeFilter('instance_id', field_type=int, low=1337, high=7331)
    rng_filt.set_recursion(2)
    print("\t", rng_filt.get_filter())
    print("\t", rng_filt.get_filter())
    print("\t", rng_filt.get_filter())
    print("\t", rng_filt.get_filter())

if __name__ == "__main__":
    test()

