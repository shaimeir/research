#!/usr/bin/env python3

'''
register and manage query augmentation functionality (extend regular query with additional data
such as host name, process path etc..)
'''

from collections import namedtuple, OrderedDict
import sqlparse
import sys
import logging

from utilities import option_registry_util
from dbcursors import sql_serialization

try:
    from . import dump_filters_base
    from .dump_filters_base import set_filter_serializer
except Exception:
    import dump_filters_base
    from dump_filters_base import set_filter_serializer

query_augmentors = option_registry_util.OptionRegistry(False)

# an augmentation element can be similar in implementation to dump_filters_base.DumpFilter
# as it can have initialization queries, temp tables and a join clause.
# we may also benefit from using the same SQL serializer as the dump filters.
# it only needs to export functionality for a set of columns added to the base query (augmentation elements).
class AugmentationElement(dump_filters_base.DumpFilter):
    '''
    encapsulates query augmentation - the addition of extra information to a generated dump query.
    '''

    def __init__(self, *affected_fields, main_query_name=None, default_type=str, strong_augmentation=False, case_sensitive=True):
        # don't register these type of filters as they are not really filters
        super().__init__(*affected_fields, default_type=default_type, register=False, max_recursion=None)
        self.strong_augmentation = strong_augmentation
        self.case_sensitive = case_sensitive

        # this field allows us to use extended filters that rely on the actual main query within our query.
        self.main_query_name = main_query_name

    def get_filter(self):
        raise NotImplementedError('AugmentationElement doesn\'t support get_filter()')

    def _fix_added_columns_format(self, added_columns):
        if isinstance(added_columns, dict):
            return OrderedDict(added_columns)

        if isinstance(added_columns, str):
            added_columns = [added_columns]
        elif added_columns is None:
            added_columns = []

        return OrderedDict([(col, col.rsplit(".")[-1]) if isinstance(col, str) else col for col in added_columns])

    def get_added_columns(self, full=True):
        complete_added_columns = self._get_added_columns(full)
        return {
            table : self._fix_added_columns_format(added_columns)
            for table, added_columns in ({} if complete_added_columns is None else complete_added_columns).items()
        }

    def _get_added_columns(self, full=True):
        '''
        get a dictionary of the new columns the augmentation and their aliases will add.
        the full argument determines if the columns will be qualified with their table.
        '''
        raise NotImplementedError('_get_added_columns() isn\'t implemented')

    def get_augmentation_queries(self):
        aug_queries = self._get_augmentation_queries()

        if aug_queries is None:
            return {}

        return aug_queries

    def _get_augmentation_queries(self):
        '''
        this function should yield a dictionary of a dict to join the main query and it's defining query.
        e.g.
            'process-name' : 'SELECT DISTINCT secdoProcessId, processName FROM process_instance_start'

        return None if no queries are given.
        '''
        return None

    def get_base_queries(self):
        base_queries = self._get_base_queries()
        return {} if base_queries is None else OrderedDict(base_queries)

    def _get_base_queries(self):
        '''
        this function should return an ordered dict of helper query name and a query in the same format of _get_augmentation_queries().
        these queries will also be placed in the WITH clause of the final query before the augmentation queries returned by this augmentation element.

        e.g.
            if the dict returned from this method looks like this:
                'bq1' : 'base-query-1',
                'bq2' : 'base-query-2'

            and the augmentation queries returned by _get_augmentation_queries are:
                'rq1' : 'select * from bq1' # real query 1
                'rq2' : 'select * from bq2 where bq2.col in bq1' # real query 2

            then the resultant WITH clause will be:

                WITH main_query AS (...),
                     bq1 AS (base-query-1),
                     bq2 AS (base-query-2),
                     rq1 AS (select * from bq1),
                     rq2 AS (select * from bq2 where bq2.col in bq1)

        the main difference from _get_augmentation_queries() is that the queries returned by _get_augmentation_queries()
        may be modified as the main routine sees fit (and they are!) and these queries aren't changed.
        '''

        return None

BasicJoinColumnRecord = namedtuple('BasicJoinColumnRecord',
                                    (
                                        # the name of the target column in the dump query (e.g. "process" - our standard name for process path column in dumped CSVs)
                                        'left_column',
                                        # the name of the target in the table we join with (e.g. "processPath" - the column name in the database of the full process path)
                                        'right_column',
                                        # the native (python) type of this column (e.g. - str, int, datetime.datetime, etc...)
                                        'type',
                                        # applied only to strings - is this string case sensitive
                                        'case_sensitive'
                                    )
                                )

def basic_join_col_record(left_column, right_column=None, col_type=str, case_sensitive=False):
    return BasicJoinColumnRecord(
                left_column,
                left_column if right_column is None else right_column, # right column defaults to left column value
                col_type,
                case_sensitive
            )

new_col_rec = basic_join_col_record

class BasicJoinAugmentation(AugmentationElement):
    '''
    the simplest augmentation mechanism - the addition of a few columns via a join on another table.
    '''

    def __init__(self,
                augmentation_table, # the table to join with (process_instance_start, hosts, etc...)
                columns,            # a list of BasicJoinColumnRecord that describe all the columns of
                new_columns,        # a list of names of columns specified in 'columns' that are the columns added to the resultant query
                comparison_columns, # a list of names of columns specified in 'columns' columns to join the main query on
                default_type=str,
                main_query_name=None,
                name=None,
                query_filters=None, # a list of filters to apply to the augmentation query
                strong_augmentation=False,
                case_sensitive=False):
        super().__init__(default_type=default_type, strong_augmentation=strong_augmentation, main_query_name=main_query_name, case_sensitive=case_sensitive)

        if not isinstance(new_columns, (list, tuple)):
            new_columns = (new_columns,)

        if not isinstance(comparison_columns, (list, tuple)):
            comparison_columns = (comparison_columns,)

        assert all((isinstance(col_record, BasicJoinColumnRecord) for col_record in columns)), ValueError('all column records must be described via BasicJoinColumnRecord', columns)

        assert len(new_columns) > 0, ValueError('invalid new columns argument', new_columns)
        assert len(comparison_columns) > 0, ValueError('invalid comparison columns argument', comparison_columns)

        self.augmentation_table = augmentation_table
        self.columns = OrderedDict({col.right_column : col for col in columns})
        self.reverse_column_map = OrderedDict({col.left_column : col for col in columns})

        self.new_columns = new_columns
        assert all((col_name in self.columns for col_name in self.new_columns)), ValueError('missing column descriptor', 'new_columns', self.new_columns)
        self.comparison_columns = comparison_columns
        assert all((col_name in self.columns for col_name in self.comparison_columns)), ValueError('missing column descriptor', 'comparison_columns', self.comparison_columns)

        self.name = type(self).__name__ if name is None else name
        self.work_table = self._get_temp_table_name(self.name)

        self.query_filters = [] if query_filters is None else (list(query_filters) if isinstance(query_filters, (list, tuple)) else [query_filters])

    def add_query_filter(self, *args):
        self.query_filters += list(args)

    def _get_augmentation_queries(self):
        columns_clause = ", ".join((
            col_name if col_desc.case_sensitive or not self.serializer.is_lowerable(col_desc.type) else (self.serializer.to_lower_field(col_name) + f" AS {col_name}")
            for col_name, col_desc in self.columns.items()
        ))

        if len(self.query_filters) > 0:
            # if there are filters we can pre-incorporate them into a filter (frankly, this appears to be sub-optimal from empiric testing)
            query = f"""
                SELECT DISTINCT {columns_clause}
                FROM {self.augmentation_table}
            """

            query_filters = dump_filters_base.AndFilter()
            query_filters.add_filter(*(
            dump_filters_base.RawFilter(flt, register=False) if isinstance(flt, str) else flt
            for flt in self.query_filters
            ))

            query += f" WHERE {query_filters.get_filter()}"

        else:
            query = self.augmentation_table

        return {self.work_table : query}

    def _get_added_columns(self, full=True):
        prefix = f'{self.work_table}.' if full else ''
        return {self.work_table : [(f'{prefix}{col.right_column}', col.left_column) for col in (self.columns[col] for col in self.new_columns)]}

    def _join_clauses(self, left_table):
        left_columns = [(self.columns[col].left_column, self.columns[col].type) for col in self.comparison_columns]
        right_columns = [(self.columns[col].right_column, self.columns[col].type) for col in self.comparison_columns]
        return dump_filters_base.TableJoinDescriptor(left_table, self.work_table, left_columns, right_columns, case_sensitive=self.case_sensitive)

### test

def print_augmentor(augmentor, prefix=''):
    print(prefix, "added columns:")
    for table, added_columns in augmentor.get_added_columns().items():
        for col, alias in added_columns.items():
            print(prefix, "\t", col, "AS", alias, " -- table")

    print()
    print(prefix, "temporary tables:")
    for table_desc in augmentor.load_temp_tables():
        dump_filters_base.print_temp_table_desc(table_desc, prefix=prefix+"\t")

    print()
    print(prefix, "initialization queries:")
    for query in augmentor.get_init_queries():
        print(prefix, prefix + "\t" + sqlparse.format(query, reindent=True, keyword_case='upper').replace("\n",f"\n{prefix}\t").strip())
        print()

    print()
    print(prefix, "join clause:")
    for clause in augmentor.join_clauses('dump_query'):
        dump_filters_base.print_join_clause(clause, prefix=prefix+"\t")


def test():
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s [%(levelname)s] %(message)s')

    augmentor = BasicJoinAugmentation(
        "process_instance_start",
        [   # columns descriptors
            new_col_rec('host', 'hostId'),
            new_col_rec('process_id', 'secdoProcessId'),
            new_col_rec('instance', 'instanceId', int),

            new_col_rec('process_name', 'processName', sql_serialization.ProcessName),
            new_col_rec('command', 'cmd', sql_serialization.CommandLine),
        ],
        [   # the columns that will be added to the scheme
            'processName',
            'cmd',
        ],
        [   # the columns compared by the ON clause
            'hostId',
            'secdoProcessId',
            'instanceId'
        ]
    )

    print_augmentor(augmentor)

if __name__ == "__main__":
    sys.exit(test())

