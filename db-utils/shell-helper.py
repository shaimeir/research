#!/usr/bin/env python3

import bpython
import dill
import pathlib

ResultsPath = pathlib.Path("/tmp/results.json")

def main():
    locals_=None
    if ResultsPath.is_file():
        with open(ResultsPath, "rb") as  res_file:
            locals_ = dill.load(res_file)

    bpython.embed(locals_)

if __name__ == "__main__":
    main()

