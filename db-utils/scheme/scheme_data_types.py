#!/usr/bin/env python3
'''
hold the data types supported by schemes.
this file also allows a generic mechanism of registerring new methods without modifying scheme.py.
'''

import ipaddress
import dateutil.parser
import datetime
from utilities import option_registry_util

scheme_types = option_registry_util.OptionRegistry(set_option_field=False)

class BasicEnumeration:
    '''
    a utility class to wrap the (legacy) enumerations introduced for various scheme data types.
    '''
    def __init__(self, name, enumeration_dict, *args):
        assert len(args) <= 1, ValueError('BasicEnumeration constructor accepts at most 1 default value yet multiple values were passed', args)
        self.name = name
        self.enumeration_dict = {str(k) : v for k, v in enumeration_dict.items()}
        self.has_default = len(args) == 1
        self.default = None if len(args) == 0 else args[0]

    def __call__(self, value):
        return self.enumeration_dict.get(value, self.default) if self.has_default else self.enumeration_dict[value]

    def __str__(self):
        s = f"{self.name} enumeration"
        if self.has_default:
            s += f" (with default value '{self.default}')"

        return s

    def __repr__(self):
        return str(self)

def register_enumeration(type_name, enumeration_dict, *default):
    global scheme_types
    scheme_types.register_option(type_name, BasicEnumeration(type_name, enumeration_dict, *default))

# register standard data types
scheme_types.register_option("str", str)
scheme_types.register_option("string", str)
scheme_types.register_option("int", int)
scheme_types.register_option("ipaddress", ipaddress.ip_address)

@scheme_types.register("bool")
def bool_data_type(value):
    value = value.lower()
    return value == "t" or value == "true"

@scheme_types.register("silent-int")
def soft_int_type(value):
    '''
    only attempt integer conversion if the field isn't empty
    '''
    return None if value is None or value == '' else int(value)

class TimestampParser:
    def __init__(self, skip_parsing):
        self.skip_parsing = skip_parsing

    def __call__(self, timestamp):
        return timestamp if self.skip_parsing or isinstance(timestamp, datetime.datetime) else dateutil.parser.parse(timestamp)

    def set_timestamp_parsing_skip(self, skip_parsing):
        self.skip_parsing = skip_parsing

    def __str__(self):
        return "timestamp parser (" + ("disabled" if self.skip_parsing else "enabled") + ")"

# by default don't parse timestamps
_timestamp_parser = TimestampParser(True)
scheme_types.register_option("timestamp", _timestamp_parser)

# manually sallow timestamp parsing
def enable_timestamp_parsing():
    global _timestamp_parser
    _timestamp_parser.set_timestamp_parsing_skip(False)

# manually disallow timestamp parsing
def disable_timestamp_parsing():
    global _timestamp_parser
    _timestamp_parser.set_timestamp_parsing_skip(True)

# register common enums

register_enumeration("network-access", {
	0 : 'unknown',
    1 : 'tcp-connect',
    2 : 'tcp-accept',
    3 : 'tcp-failed-connect',
    4 : 'tcp-disconnect',
	5 : 'bytes-sent',
    6 : 'bytes-recieved',
    7 : 'udp-send',
    8 : 'icmp-connect',
    9 : 'tcp-statistics',
    10: 'udp-statistics',
    11: 'udp-connect',
    12: 'udp-disconnect',
    13: 'udp-listen',
    14: 'tcp-listen',
    15: 'set-conn-hostname',
    16: 'control-bind',
    17: 'control-listen',
    18: 'udp-rsc-release',
    19: 'tcp-rsc-release',
    20: 'raw-socket',
}, 'unknown')

register_enumeration("module-access", {
    0 : 'unknown',
    1 : 'load',
    2 : 'unload',
})

register_enumeration("file-access", {
    0 : 'unknown',
    1 : 'open',
    2 : 'write',
    3 : 'create',
    4 : 'rename',
    5 : 'delete',
    6 : 'move-dir',
    7 : 'new-dir',
    8 : 'delete-dir',
    9 : 'copy',
}, 'unknown')

register_enumeration("registry-access", {
    '0' : "unknown",
    '1' : "create",
    '2' : "delete",
    '3' : "setvalue",
    '4' : "deletevalue",
    '5' : "renamekey",
    '6' : "load",
    '7' : "unload",
    '8' : "save",
    '9' : "restore",
})
