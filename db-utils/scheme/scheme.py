#!/usr/bin/env python3

"""
this file allows you to define a scheme to dump (file system, network, etc...) via dictionaries which can
in turn be serialized into JSON, YAML or whatever you like.
"""

import json
import ipaddress
import os
import logging
import pathlib
import csv
import sys
import argparse
import contextlib
from collections import OrderedDict
from recordclass import recordclass

try:
    from . import scheme_data_types
except Exception:
    import scheme_data_types

_SchemeColumn = recordclass('_SchemeColumn', ('name', 'type', 'db_column'))

class _ColumnParser:
    def __init__(self, scheme, col_name, parser):
        self.scheme = scheme
        self.col_name = col_name
        self.parser = None if parser is None else parser.type

    def __call__(self, value):
        if self.parser is None:
            raise ValueError('couldn\'t parse column', self.col_name, value)

        return self.parser(value)

class Scheme:
    '''
    an abstraction for a scheme (file-system, network, etc...) to dump.
    a scheme is described in a simple dict of the following form:

        {
            # the table in the database this scheme is associated with.
            # the scheme isn't necessarily associated with any table so this can be None or omitted (and then it will be set to None implicitly).
            'db_table' : 'file_system_events',

            # 'columns' must appear
            'columns' : {

                # the keys are the scheme column names.
                # this name is used in the output CSV as the column header when dumping the scheme.
                'instance_id' : {
                    # db_column specifies the column in the database this column translates to
                    'db_column' : 'instanceId',

                    # type is the name of the column type parser. supported types are registered with scheme_data_types
                    # and the 'enums' descriptor within the scheme dictionary.
                    'type' : 'int'
                },

                # if type is not specified it is assumed to be a string
                'host_id' : {
                    'db_column' : 'hostId',
                },
                # this will evaluate to {'db_column' : 'hostId', 'type' : 'str'}

                # if db_column is not specified, it is assumed to be the same as the key name (the output column name)
                'secdo_process_id' : {
                    'type' : 'str'
                },
                # this will evaluate to {'db_column' : 'secdo_process_id', 'type' : 'str'}

                # if no descriptor is specified, it is assumed to be an empty dict and is evaluated by the previous rules
                'process_name' : None,
                # this will evaluate to {'db_column' : 'process_name', 'type' : 'str'}

                # if the value set is a string then it will be inferred to be the db_column value and expanded by the previous rules
                'file' : 'filePath',
                # this will evaluate to {'db_column' : 'filePath', 'type' : 'str'}
            },

            # an optional default type entry. if not specified, the default type handler will be 'str'.
            # use None if you don't want a default type handler.
            'default_type' : 'int'
        }
    '''

    def __init__(self, scheme_dict):
        self.description = scheme_dict.get('description')
        self.default_type_name = scheme_dict.get('default_type', 'str')
        self.default_type = None if self.default_type_name is None else scheme_data_types.scheme_types.option(self.default_type_name)
        self.db_table = scheme_dict.get('db_table', None)

        assert 'columns' in scheme_dict, ValueError('poorly defined scheme - no columns section')

        # we'll store the columns descriptors in two distinct members - _columns as a regular dict and columns which is an OrderedDict.
        # since python3.6 OrderedDict seems to be deprecated as the regular dict is now ordered, but i won't rely on that in case
        # this is run via an older python.
        # we use _columns for quick access when parsing a line and columns the reset of the time to maintain proper order of columns.

        self._columns = {}
        for col_name, col_desc in scheme_dict['columns'].items():
            # fill in the blanks...
            if col_desc is None:
                col_desc = col_name

            if isinstance(col_desc, str):
                col_desc = {'db_column' : col_desc}

            col_desc.setdefault('db_column', col_name)
            col_desc.setdefault('type', 'str')

            # store internally as a scheme column
            self._columns[col_name] = _SchemeColumn(col_name, scheme_data_types.scheme_types.option(col_desc['type']), col_desc['db_column'])

        # this is not required in python3.6 but it never hurts to be explicit
        self.columns = OrderedDict(self._columns)
        # this can be done more nicely with dict comprehension but again - explicitness of order is important to me
        self._column_translation = OrderedDict(((col_name, col_desc.db_column) for col_name, col_desc in self.columns.items()))

    def parse_column(self, col_name, col_value):
        parser = self._columns.get(col_name, None)
        if parser is not None:
            return parser.type(col_value)
        elif self.default_type is not None:
            return self.default_type(col_value)
        else:
            raise ValueError('couldn\'t parse column', col_name)

    def get_db_column(self, col_name):
        parser = self._columns.get(col_name, None)
        return col_name if parser is None else parser.db_column

    def parse_row(self, row):
        return {k : self.parse_column(k, v) for k, v in row.items()}

    def translate_row(self, row):
        return {self.get_db_column(k) : v for k, v in row.items()}

    def column_translation(self):
        return self._column_translation

    def inverse_column_translation(self):
        return {db_col : scheme_col for scheme_col, db_col in self._column_translation.items()}

    def column_parsers(self):
        return {col : _ColumnParser(self, col, self._columns.get(col, self.default_type)) for col in self._columns}

    def represent(self):
        return {
            'description' : self.description,

            'db-table' : self.db_table,

            'columns-trnaslation' : self.column_translation(),
            'inverse-columns-trnaslation' : self.inverse_column_translation(),

            'types' : {
                col_name : str(col_desc.type) for col_name, col_desc in self.columns.items()
            },

            'default' : self.default_type_name
        }

    def __eq__(self, other):
        return isinstance(other, Scheme) and other.represent() == self.represent()

    def __repr__(self):
        return json.dumps(self.represent(), indent=4)

    def __str__(self):
        return repr(self)

def load_json_stream(stream):
    return Scheme(json.load(stream))

def load_json_scheme(file_name):
    with open(file_name, "rt") as scheme_file:
        return Scheme(json.load(scheme_file))

def get_default_schemes_directory():
    # resolve __file__ beforehand to avoid soft-link related bullcrap
    return pathlib.Path(__file__).resolve().parent/'db-schemes'

def load_scheme_file(file_or_scheme_name, *scheme_dirs):
    logging.info("resolving %s", file_or_scheme_name)

    scheme_file = pathlib.Path(file_or_scheme_name)
    if not scheme_file.is_file():
        logging.info("'%s' is not a file, trying to resolve it with search paths", scheme_file)

        for scheme_dir in scheme_dirs:
            scheme_file = pathlib.Path(scheme_dir)/file_or_scheme_name
            logging.info("trying %s...", scheme_file)
            if scheme_file.is_file():
                logging.info("found scheme file!")
                break

            scheme_file = pathlib.Path(scheme_dir)/(file_or_scheme_name + '.json')
            logging.info("trying %s...", scheme_file)
            if scheme_file.is_file():
                logging.info("found scheme file!")
                break

    # now that we've tried to resolve the file name
    assert scheme_file.is_file(), FileNotFoundError('couldn\'t resolve scheme', file_or_scheme_name, scheme_dirs)

    return load_json_scheme(scheme_file)

def load_default_scheme_file(file_or_scheme_name):
    return load_scheme_file(file_or_scheme_name, get_default_schemes_directory())

def get_schemes_list(*scheme_dirs):
    return [filename.rsplit(".")[0] for scheme_dir in scheme_dirs for filename in os.listdir(scheme_dir) if (pathlib.Path(scheme_dir)/filename).is_file() and filename.endswith(".json")]

def get_default_schemes_list():
    return get_schemes_list(get_default_schemes_directory())

@contextlib.contextmanager
def auto_yield(o):
    yield o

def parse_main(args):
    for scheme_file_name in args.scheme:
        if '-' == scheme_file_name:
            scheme = load_json_stream(sys.stdin)
        else:
            scheme = load_scheme_file(scheme_file_name, *args.directory)
        print(repr(scheme))

def print_main(args):
    if 0 < len(args.directory):
        logging.info("available schemes:")
        print("\n".join(get_schemes_list(*args.directory)))
    else:
        logging.info("schemes available in default search path")
        print("\n".join(get_schemes_list(get_default_schemes_directory())))

def csv_main(args):
    scheme = load_scheme_file(args.scheme, get_default_schemes_directory())

    for csv_file_name in args.csv:
        with auto_yield(sys.stdin) if "-" == csv_file_name else open(csv_file_name, "rt") as csv_file:
            reader = csv.DictReader(csv_file, delimiter="\t")
            for row in reader:
                print(scheme.parse_row(row))

def main():
    parser = argparse.ArgumentParser(description="parse and describe a scheme stored in a JSON file")
    parser.add_argument("-e", "--enable-timestamp-parsing", action='store_true', help='enable datetime parsing of timestamps.')

    subparsers = parser.add_subparsers(dest='action')

    parse_parser = subparsers.add_parser('parse', help="parse scheme files and describe them.")
    parse_parser.add_argument("-d", "--directory", action='append', default=[get_default_schemes_directory()], help='directories to look for schemes in.')
    parse_parser.add_argument("scheme", nargs="*", default=["-"], help="a list of JSON formatted schemes.")

    print_parser = subparsers.add_parser('print', help="print search paths and possible recipes")
    print_parser.add_argument("directory", nargs="*", help='directories to look for schemes in.')

    csv_parser = subparsers.add_parser('csv', help='parse the rows of a given tab-separated csv.')
    csv_parser.add_argument("-s", "--scheme", required=True, help='the scheme to load.')
    csv_parser.add_argument("csv", nargs="*", default=["-"], help='csv (with header) to parse.')

    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG, format="%(asctime)s [%(levelname)s] %(message)s")

    if args.enable_timestamp_parsing:
        scheme_data_types.enable_timestamp_parsing()

    if args.action == 'parse':
        parse_main(args)
    elif args.action == 'print':
        print_main(args)
    elif args.action == 'csv':
        csv_main(args)
    else:
        print(f'invalid action: {args.action}', file=sys.stderr)
        return 1

if __name__ == "__main__":
    sys.exit(main())

