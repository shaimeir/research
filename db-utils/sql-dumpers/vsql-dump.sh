#!/usr/bin/env bash

set -eu

export LANG=en_US.utf-8
export LC_ALL=en_US.utf-8

company=
output_file=
quote=
seperator="$(echo -en '\t')"
uncompressed=0

function print_help()
{
    echo "perform a dump from a database via vsql. the query is taken from stdin."
    echo "if the query has '_group_token' in it, the groups will be iterated over (every time '_group_token' is replaced uniformly!)."
    echo " * -c - [mandatory] the company to dump the information for."
    echo " * -o - set the output file (default is stdout)."
    echo " * -q - use quotation and comma seperation in the output."
    echo " * -u - generate uncompressed output."
    echo " * -s - vertica's host. [default: 127.0.0.1]"
    echo " * -m [marker] - set the expected marker pattern (i.e. when the actual output begins)."
    echo " * -r - don't expect an output marker and just take the query output as-is."
    echo " * -x - set bash tracing on."
    echo " * -C - compatability with other dump scripts conventions. does nothing."
}

marker="SCHEME_DUMP_MARKER"
while getopts ":c:s:o:quhm:rxC" option
do
    case "${option}" in
        "c") company="${OPTARG}"
             ;;

        "o") exec >"${OPTARG}"
             output_file="${OPTARG}"
             ;;

        "q") quote=1
             seperator='","'
             ;;

        "u") uncompressed=1
             ;;

        "s") remote_host="${OPTARG}"
             ;;

        "h") print_help >&2
             exit 0
             ;;

        "m") marker="${OPTARG}"
             ;;

        "r") marker=""
             ;;

        "x") set -x
             ;;
    esac
done

if [ "${company}" == "" ]
then
    echo "missing company." >&2
    exit 1
fi

# some vsql bullshit
export LANG=en_US.UTF-8

vsql_proc_marker="$(mktemp -u OHAD-REMOTE-RUN-VSQL-XXXXXXXX)"

vsql=/opt/vertica/bin/vsql
if which vsql >/dev/null
then
    vsql="$(which vsql)"
fi

function do_query()
{
    "${vsql}" -v CLEANUP_MARKER="${vsql_proc_marker}" -h "${remote_host:-127.0.0.1}" -U "$(echo -n ${company} | md5sum | cut -d' ' -f1)_user" -w '$ecDO1601' -P null='' -F"${seperator}" -P footer=off -AtXn |
        awk -v marker="${marker}" -v quote="${quote}" '
            BEGIN {
                marker_caught = (marker == "") ? 1 : 0
                quote_char = (quote == "") ? "" : "\""
            }

            marker_caught == 1 {print quote_char $0 quote_char}
            marker_caught == 0 && $0 ~ marker {marker_caught = 1}
        '
}

tmp_sql="$(mktemp /tmp/ohad-remote-sql-XXXXXX)"
delete_output_file=1

function cleanup()
{
    rm -f ${tmp_sql}
    if [ "${delete_output_file}" == "1" -a "${output_file}" != "" ]
    then
        rm -f ${output_file}
    fi

    pkill -TERM -f "${vsql_proc_marker}" || true
}
trap cleanup EXIT

(echo 'SET SESSION RESOURCE_POOL = quick_queries;' ; cat) >${tmp_sql}

if grep -q "_group_token" <${tmp_sql} &>/dev/null
then
    echo "generating sql queries..." >&2
    for group in $(seq 0 127)
    do
        (cat ${tmp_sql}; echo -n ';') | sed "s/_group_token/_group_${group}/g"
    done
else
    cat ${tmp_sql}
fi | do_query |
     if [ "${uncompressed}" == "1" ]
     then
        cat
     else
        gzip
     fi

delete_output_file=0

