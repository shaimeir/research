#!/usr/bin/env bash

# this script takes a PostgreSQL query and executes it via psql on a given server for a given company.
# this script utilizes the 'sqlparse' python3 module so be sure to have it installed before using this.

set -eu

# Stop psql's complains that "Setting locale failed"
export LANG=en_US.utf-8
export LC_ALL=en_US.utf-8

function print_help()
{
    echo "perform a dump from a database via psql."
    echo " * -c - the company to dump the information for."
    echo " * -o - set the output file."
    echo " * -s - set the host name."
    echo " * [query-file] - a file with a query to execute. if not specified, the query is taken from stdin."
    echo "Example: ./psql-dump.sh -s 100.64.64.88 -c safesystemspoc -o /tmp/safesystemspoc.alerts.csv"
}

while getopts ":c:o:hs:" option
do
    case "${option}" in
        "c") company="${OPTARG}"
             ;;
        "s") hostname="${OPTARG}"
             ;;
        "o") output_file="${OPTARG}"
             ;;
        "h") print_help >&2
             exit 0
             ;;
    esac
done
shift $((OPTIND-1))

if [ "${company:-}" == "" ]
then
    echo "missing company." >&2
    print_help >&2
    exit 1
fi

# set the query stream to be stdin
if [ "${#}" -eq 0 ]
then
    : # nop, do nothing - the query will be taken from stdin.
elif [ "${#}" -eq 1 ]
then
    # redirect our stdin to take the query from a file
    exec <"${1}"
else
    echo "error: too many query files!" >&2
    print_help >&2
    exit 1
fi

# check if we can eliminate comments from the sql query using the python sqlparse module
if python -m sqlparse -h &>/dev/null
then
    # sqlparse let's us strip the comments away.
    # we'll reset our input stream to be post comment-stripping using a sub-shell redirection.
    # sqlparse will read the query from it's stdin and feed us with a comment-less version.
    exec < <(python -m sqlparse --strip-comments -)
else
    echo "WARNING: can't remove comments from SQL statement. query may turn corrupted. please install python's sqlparse module." >&2
fi

scheme_name="db_$(echo -n ${company} | md5sum | awk '{print $1}')"

username=postgres
database=secdo

delete_output_file=1

function cleanup()
{
    if [ "${delete_output_file}" == "1" -a "${output_file:-}" != "" ]
    then
        rm -f ${output_file}
    fi
}
trap cleanup EXIT

# build the complete PostgreSQL query with scheme selection and \COPY if necssary.
(
    # set the scheme to be for the correct company.
    echo "SET SEARCH_PATH = ${scheme_name};"

    if [ "${output_file:-}" != "" ]
    then
        # use the '\COPY' function to execute the command given via stdin.
        # the \COPY syntax requires the command be given in one line, so first remove comments to
        # avoid hazard (already done by now) and replace all new-lines with a single space (using tr).
        echo "\\COPY ($(tr '\n' ' ')) TO ${output_file} DELIMITER E'\t' CSV HEADER;"
    else
        # in this case where the output is stdout there's no need to filter out comments so just pipe stdin through to psql.
        cat
    fi
) | psql -h "${hostname:-127.0.0.1}" -U ${username} ${database} # run on the given target machine with the built-in user.

delete_output_file=0

