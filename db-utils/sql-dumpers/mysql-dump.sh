#!/usr/bin/env bash

# this script takes a MySQL query and executes it via the mysql client on a given server for a given company.

set -eu

export LANG=en_US.utf-8
export LC_ALL=en_US.utf-8

function print_help()
{
    echo "perform a dump from a database via mysql client."
    echo " * -c [company] - the company to dump the information for."
    echo " * -o [out-file] - set the output file."
    echo " * -s [host] - set the host name."
    echo " * -C - compress the traffic between the server and mysql."
    echo " * -u - generate decompressed output."
    echo " * -m [marker] - set the expected marker pattern (i.e. when the actual output begins)."
    echo " * -r - don't expect an output marker and just take the query output as-is."
    echo " * [query-file] - a file with a query to execute. if not specified, the query is taken from stdin."
}

marker="SCHEME_DUMP_MARKER"
compressor="gzip -f"
while getopts ":c:o:hs:uxm:rC" option
do
    case "${option}" in
        "c") company="${OPTARG}"
             ;;

        "s") hostname="${OPTARG}"
             ;;

        "o") output_file="${OPTARG}"
             exec >"${output_file}"
             ;;

        "u") compressor="cat"
             ;;

        "h") print_help >&2
             exit 0
             ;;

        "x") set -x
             ;;

        "m") marker="${OPTARG}"
             ;;

        "r") marker=""
             ;;

        "C") server_compression_flag="-C"
             ;;
    esac
done
shift $((OPTIND-1))

if [ "${company:-}" == "" ]
then
    echo "missing company." >&2
    print_help >&2
    exit 1
fi

# set the query stream to be stdin
if [ "${#}" -eq 0 ]
then
    : # nop, do nothing - the query will be taken from stdin.
elif [ "${#}" -eq 1 ]
then
    # redirect our stdin to take the query from a file
    echo "input taken from ${1}" >&2
    exec <"${1}"
else
    echo "error: too many query files!" >&2
    print_help >&2
    exit 1
fi

username=root
database="db_$(echo -n ${company} | md5sum | awk '{print $1}')"

delete_output_file=1

function cleanup()
{
    if [ "${delete_output_file}" == "1" -a "${output_file:-}" != "" ]
    then
        rm -f "${output_file}"
    fi
}
trap cleanup EXIT

# mysql is fed the query through stdin
echo "$(date) starting query execution" >&2
mysql ${server_compression_flag:-} --quick -s -h "${hostname:-127.0.0.1}" -u ${username} ${database} | \
    if [ "${marker}" != "" ]
    then
        awk -v marker="${marker}" '
            BEGIN {marker_caught = 0}
            marker_caught == 1 {print} # this line is first so the marker itself will be skipped in the output
            marker_caught == 0 && $0 ~ marker {marker_caught = 1}
        ' | ${compressor}
    else
        ${compressor}
    fi

echo "$(date) query execution finished" >&2

delete_output_file=0

