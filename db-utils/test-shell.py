#!/usr/bin/env python3

import cmd
import argparse
import sqlparse
import getpass
import termcolor
import tabulate
import logging
import hashlib
import csv
import traceback
import subprocess
import sys
import os
import contextlib
import readline
import pathlib
import json
import dill

from utilities import text_utils
import dbcursors
from dbcursors import default_dbs

HistoryFile = os.path.expanduser("~/.test-shell-hist")

@contextlib.contextmanager
def history_context():
    if os.path.isfile(HistoryFile):
        with contextlib.suppress(Exception):
            readline.read_history_file(HistoryFile)

    try:
        yield

    finally:
        with contextlib.suppress(Exception):
            readline.set_history_length(1000)
            readline.write_history_file(HistoryFile)

class TestShell(cmd.Cmd):
    def __init__(self, cursor, prompt, prompt2):
        super().__init__(
            completekey='tab',
            stdin=text_utils.MultilineStdin(sys.stdin),
            stdout=text_utils.ColoredText(sys.stdout)
        )

        self.cursor = cursor
        self.prompt = prompt
        self.prompt1 = prompt
        self.prompt2 = prompt2
        self.lastSet = []
        self.broken_output = False

        self.aggregated_command = ''

        self.helperShell = pathlib.Path(__file__).absolute().parent/'shell-helper.py'
        self.resultsJson = pathlib.Path("/tmp/results.json")

        self.manual = False

    def default(self, arg):
        self.prompt = self.prompt1

        if arg.startswith('#'):
            return

        if arg.endswith("\\"):
            self.aggregated_command += arg[:-1] + "\n"
            self.prompt = self.prompt2
            return

        if len(self.aggregated_command) > 0:
            arg = self.aggregated_command + arg
            self.aggregated_command = ''

        # assume arg to be an SQL query
        try:
            try:
                parsed_query = sqlparse.parse(arg)
            except Exception:
                logging.warn("couldn't parse query for sequential execution!")
                self.cursor.execute(arg)
                if self.manual:
                    return
                self.lastSet = self.cursor.fetchall()
                self.broken_output = False
                content = tabulate.tabulate(self.lastSet) + "\n"
            else:
                self.lastSet = []
                for sub_query in parsed_query:
                    self.cursor.execute(str(sub_query))
                    if self.manual and len(parsed_query) == 1:
                        return

                    self.lastSet.append(self.cursor.fetchall())

                content = "\n\n".join(map(lambda s: tabulate.tabulate(s), self.lastSet)) + "\n"

                if len(self.lastSet) == 1:
                    self.lastSet = self.lastSet[0]
                    self.broken_output = False
                else:
                    self.broken_output = True

            with self.stdout.colorContext('yellow'):
                self.stdout.write(content)

        except Exception:
            with self.stdout.colorContext('red'):
                self.stdout.write(traceback.format_exc() + "\n")

    def do_manual(self, arg):
        """
        set manual mode (don't fetch results after execution)
        """
        self.manual = True

    def do_automatic(self, arg):
        """
        set automatic mode (fetch results after execution)
        """
        self.manual = False

    def do_flush(self, arg):
        """
        Fetch all pending results
        """
        self.lastSet = self.cursor.fetchall()
        self.broken_output = False
        content = tabulate.tabulate(self.lastSet) + "\n"

        with self.stdout.colorContext('yellow'):
            self.stdout.write(content)

    def do_dumpcsv(self, arg):
        """
        Dump a query result into a csv file.
        """
        if arg is None:
            arg = ''
        arg = arg.strip()

        if '' == arg:
            self.stdout.write("enter dump file name: ")
            self.stdout.flush()
            output_file = self.stdin.readline().strip()
            with self.stdout.colorContext('blue'):
                self.stdout.write("dumping last result to file '{}'\n".format(output_file))
        else:
            arg_decomposition = arg.split(" ", 1)
            output_file = arg_decomposition[0]

            if len(arg_decomposition) == 2:
                try:
                    self.cursor.execute(arg_decomposition[1])
                    self.lastSet = self.cursor.fetchall()
                    self.broken_output = False

                except Exception:
                    with self.stdout.colorContext('red'):
                        self.stdout.write(traceback.format_exc() + "\n")

                    return

        last_set = self.lastSet
        name_suffix_generator = map(lambda n: f".{n}", range(len(last_set)))
        if not self.broken_output:
            last_set = [last_set]
            name_suffix_generator = ("",)

        name_iter = iter(name_suffix_generator)
        for result_set in last_set:
            if len(result_set) > 0:
                write_order = tuple(result_set[0].keys())
            else:
                write_order = (1,) # place holder
            with open(output_file + next(name_iter), "wt") as output_csv_file:
                csv_stream = csv.writer(output_csv_file, delimiter='\t', dialect='unix')
                csv_stream.writerows((tuple((row[ordinal] for ordinal in write_order)) for row in result_set))

    def do_csvdump(self, arg):
        """
        Dump a query result into a csv file.
        """
        return self.do_dumpcsv(arg)

    def do_sh(self, arg):
        arg = arg.strip()
        if arg == '':
            logging.info("opening shell")
            subprocess.run(["bash"])
        else:
            logging.info("executing shell command: %s", arg)
            subprocess.run(["bash", "-c", arg])

    def do_py(self, arg):
        logging.info("dumping last set to %s")
        with open(self.resultsJson, "wb") as results_file:
            dill.dump({'results' : self.lastSet}, results_file)

        logging.info("executing helper shell")
        subprocess.run([self.helperShell])

    def do_python(self, arg):
        return self.do_py(arg)

    def do_bpython(self, arg):
        return self.do_py(arg)

    def do_less(self, arg):
        """
        Show the last result set in less.
        """
        content = tabulate.tabulate(self.lastSet) + "\n"
        subprocess.run(["less", "-fR"], input=content.encode())

    def do_vless(self, arg):
        """
        Show the last result set in less.
        """
        content = tabulate.tabulate(self.lastSet) + "\n"
        subprocess.run(["vim", "-", "+set nowrap"], input=content.encode())

    def emptyline(self):
        pass

    def do_quit(self, arg):
        """
        Quit the shell.
        """
        return True

    def do_EOF(self, arg):
        """
        Quit the shell.
        """
        return True

    def do_exit(self, arg):
        """
        Quit the shell.
        """
        return True

def main():
    db_defaults = default_dbs.load_dbs_from_command_line()

    parser = argparse.ArgumentParser(description="a mini shell that abstracts distributed data tables")
    default_dbs.add_standard_command_line_arg(parser)
    parser.add_argument("-v", "--verbose", action="store_true")
    parser.add_argument("db", choices=list(db_defaults.keys()), help="the database to connect to (and schema to use)")
    args = parser.parse_args()

    logging.basicConfig(format="%(asctime)s %(message)s", level=logging.INFO if args.verbose else logging.WARN)

    db = db_defaults[args.db]

    # this is a call to dbcursors.vertica_cursor & friends
    cursor = dbcursors.get_cursor_factory(db['dbtype'], None, True)(
        password=db['password'] if 'password' in db else getpass.getpass('Database password: '),
        **db
    )

    with history_context():
        with cursor.context():
            TestShell(cursor,
                      termcolor.colored("[", 'red') +
                      termcolor.colored(args.db, 'cyan') +
                      termcolor.colored("/", 'red') +
                      termcolor.colored(db.get('company',db.get('schema',"no-schema")), 'cyan') +
                      termcolor.colored("] ", 'red'),
                      termcolor.colored("> ", 'red')
            ).cmdloop(intro=termcolor.colored(f"Connected to {args.db}", 'magenta'))

    print()

if __name__ == "__main__":
    main()

