#!/usr/bin/env python3

import termcolor
import contextlib
import subprocess

class ColoredText:
    @staticmethod
    def red(text):
        return termcolor.colored(text, 'red')

    @staticmethod
    def yellow(text):
        return termcolor.colored(text, 'yellow')

    @staticmethod
    def magenta(text):
        return termcolor.colored(text, 'magenta')

    @staticmethod
    def cyan(text):
        return termcolor.colored(text, 'cyan')

    @staticmethod
    def blue(text):
        return termcolor.colored(text, 'blue')

    @staticmethod
    def green(text):
        return termcolor.colored(text, 'green')

    def __init__(self, stdout):
        self.stdout = stdout
        self.color = []

    def write(self, s):
        self.stdout.write(s if len(self.color) == 0 else termcolor.colored(s, self.color[-1]))

    def flush(self):
        self.stdout.flush()

    @contextlib.contextmanager
    def colorContext(self, color):
        try:
            self.color.append(color)
            yield

        finally:
            self.color.pop()

class MultilineStdin:
    def __init__(self, stdin):
        self.stdin = stdin

    def readline(self):
        line = "\\"

        while line.endswith("\\"):
            line = line[:-1] # drop terminating \\
            line += self.stdin.readline().rstrip("\n")

        return line + "\n"

def display_in_less(content):
    with subprocess.Popen("less -fR", stdin=subprocess.PIPE, shell=True) as less:
        less.stdin.write(content.encode())
        less.stdin.flush()
        less.wait()

