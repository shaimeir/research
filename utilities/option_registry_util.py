#!/usr/bin/env python3

import logging

class OptionRegistry:
    '''
    Arkady: this is a nicely wrapped collection (dictionary) of registered methods
    The registration is triggered usually by a funciton decorator

    A key in the dictionary is a decorated and registered function name
    The data contains function's help and the function reference. Every time 
    the code calls a registered function it will go via the dictionary
    '''
    def __init__(self, set_option_field=True):
        self.options = {}
        self.set_option_field = set_option_field

    def choices(self):
        return {choice: self.options[choice]['help'] for choice in self.options}

    def choicesList(self):
        '''
        Arkady: appears to be a qucik&dirty hack which returns all dictionary keys 
        '''
        return list(self.options.keys())

    def doc(self):
        return "\n".join((f' * {choice} - {docstring}.' for choice, docstring in self.choices().items()))

    def register(self, option_name):
        def _register_option(func):
            self.register_option(option_name, func)
            return func

        return _register_option

    def register_option(self, option_name, func):
        '''
        manually register a type without having to do.
        '''
        if self.set_option_field:
            setattr(func, '__option__', option_name)
        _doc = getattr(func, "__doc__", None)
        self.options[option_name] = {
            'function' : func,
            'help' : "<no doc>" if _doc is None else _doc.strip(),
        }

    def option(self, option_name):
        return self.options[option_name]['function']

    def choicesFunctions(self):
        return {choice: self.options[choice]['function'] for choice in self.options}

