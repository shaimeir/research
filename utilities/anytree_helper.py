#!/usr/bin/env python3

import anytree
import termcolor
import sys
import pathlib
from collections import Iterable

class LazyStr:
    def __init__(self, node, names=True):
        self.node = node
        self.names = names

    def __str__(self):
        return "/" + "/".join((n.name for n in self.node.path)).lstrip("/") if self.names else str(self.node.path)

    def __repr__(self):
        return "/" + "/".join((n.name for n in self.node.path)).lstrip("/") if self.names else str(self.node.path)

def lazy_str(node):
    return LazyStr(node)

class NicelyRenderedNode(anytree.Node):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    _WHITE_SEP = termcolor.colored("/", "white")
    def __str__(self):
        return self._WHITE_SEP + self._WHITE_SEP.join(map(repr, self.path))

    def __repr__(self):
        args = []
        for key, value in filter(lambda item: not item[0].startswith("_"),
                                 sorted(self.__dict__.items(),
                                        key=lambda item: item[0])):
            if isinstance(value, Iterable) and not isinstance(value, str) and not isinstance(value, bytes) and len(value) == 0:
                continue
            args.append(f"{key}={value}")

        if len(args) > 0 and not getattr(self, 'hide_attributes', False):
            _repr = "{} ({})".format(self.name, ", ".join(args))
        else:
            _repr = self.name

        color = self.__dict__.get("_color")
        if color is not None:
            _repr = termcolor.colored(_repr, color)

        return _repr

class ColorfulNicelyRenderedNode(NicelyRenderedNode):
    # Colors = ['magenta', 'green', 'cyan', 'yellow', 'blue', 'red', 'white'] # this includes darker colors
    Colors = {'magenta' : 0, 'green' : 1, 'cyan' : 2, 'yellow' : 3, 'red' : 4, 'white' : 5}
    InverseColors = {index : color for color, index in Colors.items()}

    def __init__(self, *args, **kwargs):
        parent = kwargs.get('parent', None)
        color = kwargs.pop('color', None)

        if parent is None:
            self.color = 0 if color is None else color
        else:
            self.color = ((getattr(parent, '_color_index', -1) + 1) % len(self.Colors)) if color is None else color

        super().__init__(*args, **kwargs)

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, new_color):
        if isinstance(new_color, str):
            self._color_index = self.Colors[new_color]
            self._color = new_color
        else:
            self._color = self.InverseColors[new_color]
            self._color_index = new_color

TreeNode = ColorfulNicelyRenderedNode

class IndexedChildrenNode(ColorfulNicelyRenderedNode):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._children_table = {}

    def _post_detach(self, parent):
        parent._children_table.pop(self.name)

    def _post_attach(self, parent):
        parent._children_table[self.name] = self

    def __getitem__(self, child_name):
        return self._children_table[child_name]

    def get_child(self, child_name, default=None):
        return self._children_table.get(child_name, default)

class NonPreservingIteratorRenderTree(anytree.RenderTree):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __iter__(self):
        return self.__next(self.node, tuple())

    def __next(self, node, continues):
        yield self.__item(node, continues, self.style)
        children = node.children
        if children:
            enumerated_children = list(enumerate(self.childiter(children))) # hogging memory but who cares
            if len(enumerated_children) > 0:
                lastidx = len(enumerated_children) - 1
                for idx, child in enumerated_children:
                    for grandchild in self.__next(child, continues + (idx != lastidx, )):
                        yield grandchild

    @staticmethod
    def __item(node, continues, style):
        if not continues:
            return u'', u'', node
        else:
            items = [style.vertical if cont else style.empty for cont in continues]
            indent = ''.join(items[:-1])
            branch = style.cont if continues[-1] else style.end
            pre = indent + branch
            fill = ''.join(items)
            return pre, fill, node

def windows_path_like_parser(path):
    return pathlib.PureWindowsPath(path).parts

def paths_to_tree(paths_iterable, root_name='root', node_class=ColorfulNicelyRenderedNode, path_parser=windows_path_like_parser, parent=None, **kwargs):
    if hasattr(path_parser, 'deconstruct'):
        _path_parser = path_parser
        path_parser = lambda *a, **kw: _path_parser.deconstruct(*a, **kw)

    root = node_class(root_name, parent=parent, **kwargs)
    for path in paths_iterable:
        node = root
        for part in path_parser(path):
            child = None
            for _child in node.children:
                if _child.name == part:
                    child = _child
                    break

            if child is None:
                node = node_class(part, parent=node, **kwargs)
            else:
                node = child

    return root

def tree_to_paths(root, path_parser=pathlib.PureWindowsPath):
    queue = list(root.children)
    paths = []

    if hasattr(path_parser, 'reconstruct'):
        _path_parser = path_parser
        path_parser = lambda *a, **kw: _path_parser.reconstruct(*a, **kw)

    while len(queue) > 0:
        node = queue.pop(0)

        if node.is_leaf:
            paths.append(path_parser(*(_n.name for _n in node.path[1:])))
        else:
            for child in node.children:
                if child.is_leaf:
                    paths.append(str(path_parser(*(_n.name for _n in child.path[1:]))))
                else:
                    queue.append(child)

    return paths

def traverse_leaves(root, callback):
    queue = [root]
    while len(queue) > 0:
        node = queue.pop(0)
        if node.is_leaf:
            callback(node)
        else:
            queue = list(node.children) + queue

def get_tree_leaves(root):
    leaves = []
    traverse_leaves(root, lambda leaf: leaves.append(leaf))
    return leaves

def test():
    root = ColorfulNicelyRenderedNode("when i think of heaven")
    n1 = ColorfulNicelyRenderedNode("deliver me in a black wing bird", parent=root, just_an_attribute=1337)
    s1 = ColorfulNicelyRenderedNode("i think of flying", parent=n1, just_an_attribute=1337)
    s2 = ColorfulNicelyRenderedNode("down into a sea of pens and feathers", parent=n1, just_an_attribute=1337)
    n2 = ColorfulNicelyRenderedNode("and all kind of instruments", parent=root, just_an_attribute=1337)
    print(anytree.RenderTree(root))

    paths = [
        r'c:\windows\temp',
        r'c:\windows\system\config.ini',
        r'd:\file.txt',
        r'd:\program files\program1\program.exe',
        r'c:\windows\temp', # a repetition on purpose
        r'c:\windows\temp\temp_file',
    ]
    root = paths_to_tree(paths, seperator='\\')
    print(anytree.RenderTree(root))

    def _child_iter(children):
        for child in children:
            if not getattr(child, 'secret', False):
                yield  child

    root = ColorfulNicelyRenderedNode("she'll let in her house")
    n1 = ColorfulNicelyRenderedNode("if you come knocking late at night", parent=root)
    s1 = ColorfulNicelyRenderedNode("she'll let you in her mouth", parent=n1)
    s2 = ColorfulNicelyRenderedNode("if the words you say are right", parent=n1)
    n2 = ColorfulNicelyRenderedNode("But there's a secret garden she hides", parent=root, secret=True)
    print(NonPreservingIteratorRenderTree(root, childiter=_child_iter))

if __name__ == "__main__":
    sys.exit(test())

