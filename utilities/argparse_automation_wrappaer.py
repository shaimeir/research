#!/usr/bin/env python3

import argparse
import sys
import json
import types
import copy

ArgumentsDumpOption = "--dump-arguments"

def wrap_parser(parser):
    _ArgumentParserWrapper(parser)
    return parser

def ArgumentParser(*args, **kwargs):
    return wrap_parser(argparse.ArgumentParser(*args, **kwargs))

class _SubparsersTracker:
    @staticmethod
    def _add_parser(self, name, **kwargs):
        return self.tracker.addParser(name, **kwargs)

    def __init__(self, subparsers_obj, subparsers_kwargs):
        self.subparsersObj = subparsers_obj
        self.subparsers = {}
        self.subparsersKwargs = subparsers_kwargs

        self.origAddParser = self.subparsersObj.add_parser

        setattr(self.subparsersObj, 'tracker', self)
        self.subparsersObj.add_parser = types.MethodType(self._add_parser, self.subparsersObj)

    def addParser(self, name, **kwargs):
        subparser = self.origAddParser(name, **kwargs)
        self.subparsers[name] = {
            'kwargs' : kwargs,
            'parser' : _ArgumentParserWrapper(subparser)
        }

        return subparser

    def dumpArguments(self):
        return  {
            'kwargs' : self.subparsersKwargs,

            'parsers' : {
                name : {
                    'kwargs' : subparser['kwargs'],
                    'parser' : subparser['parser'].dumpArguments(),
                }

                for name, subparser in self.subparsers.items()
            },
        }

class _ArgumentParserWrapper:
    @staticmethod
    def _add_argument(self, *args, **kwargs):
        self.tracker.addArgument(*args, **kwargs)

    @staticmethod
    def _add_subparsers(self, **kwargs):
        return self.tracker.addSubparsers(**kwargs)

    @staticmethod
    def _parse_args(self, args=None, namespace=None):
        return self.tracker.parseArgs(args, namespace)

    @staticmethod
    def _parse_known_args(self, args=None, namespace=None):
        return self.tracker.parseKnownArgs(args, namespace)

    def __init__(self, parser):
        self.parser = parser

        self.subparsers = None

        self.argTracker = {
            'init' : {
            },

            'arguments' : {
            },
        }

        init = self.argTracker['init']
        if parser.description is not None:
            init['description'] = parser.description

        self.origAddArgument = parser.add_argument
        self.origAddSubparsers = parser.add_subparsers
        self.origParseArgs = parser.parse_args
        self.origParseKnownArgs = parser.parse_known_args

        setattr(self.parser, 'tracker', self)
        self.parser.add_argument = types.MethodType(self._add_argument, self.parser)
        self.parser.add_subparsers = types.MethodType(self._add_subparsers, self.parser)
        self.parser.parse_args = types.MethodType(self._parse_args, self.parser)
        self.parser.parse_known_args = types.MethodType(self._parse_known_args, self.parser)

    def addArgument(self, *args, **kwargs):
        is_output = kwargs.pop('output', False)
        is_file = kwargs.pop('file', False)
        arg_name = max(filter(lambda a: isinstance(a, str), args), key=len).strip("-").replace("-","_")
        self.argTracker['arguments'][arg_name] = {
            'args' : args,
            'kwargs' : kwargs,
            'is_output' : is_output,
            'is_file' : is_file,
        }

        return self.origAddArgument(*args, **kwargs)

    def addSubparsers(self, **kwargs):
        subparsers = self.origAddSubparsers(**kwargs)
        self.subparsers = _SubparsersTracker(subparsers, kwargs)
        return subparsers
    
    def parseArgs(self, args, namespace):
        if (args is None and ArgumentsDumpOption in sys.argv) or (args is not None and ArgumentsDumpOption in args):
            print(json.dumps(self.dumpArguments(), indent=4))
            sys.exit(0)

        return self.origParseArgs(args, namespace)

    def parseKnownArgs(self, args, namespace):
        if (args is None and ArgumentsDumpOption in sys.argv) or (args is not None and ArgumentsDumpOption in args):
            print(json.dumps(self.dumpArguments(), indent=4))
            sys.exit(0)

        return self.origParseKnownArgs(args, namespace)
        
    def dumpArguments(self):
        dump = copy.deepcopy(self.argTracker)

        if self.subparsers is not None:
            dump['subparsers'] = self.subparsers.dumpArguments()

        arguments = dump['arguments']
        for arg_name, arg_desc in arguments.items():
            arg_desc['kwargs'].pop('type', None)

        return dump

def test():
    parser = wrap_parser(argparse.ArgumentParser(description=f"use {ArgumentsDumpOption} to dump the arguments for these parser as a JSON"))
    parser.add_argument("-a", "--alpha", required=True, type=int, help="alpha argument")
    parser.add_argument("-b", "--beta", required=True, type=int, help="beta argument")

    subparsers = parser.add_subparsers(title="the subparsers title")

    sub1 = subparsers.add_parser("parser-2")
    sub1.add_argument("-g", "--gamma", action="store_true")
    sub1.add_argument("delta", metavar="_delta_", nargs="+")

    sub2 = subparsers.add_parser("parser-2")
    sub2.add_argument("-e", "--epsilon", action="append", default=[])
    sub2.add_argument("zeta", nargs="?")

    parser.parse_args()

if __name__ == "__main__":
    test()
