#!/usr/bin/env python3

import logging
import argparse
from collections import namedtuple

class _SwitchesRegistrar:
    class _Parser:
        def __init__(self, handler, type_parser):
            self.handler = handler
            self.type_parser = type_parser

        def __call__(self, arg):
            try:
                if self.type_parser is not None:
                    arg = self.type_parser(arg)

                return self.handler(arg)
            except Exception:
                logging.error("error while parsing command line argument (handler=%r, type=%r)", self.handler, self.type_parser, exc_info=True)
                raise

    _SwitchEntry = namedtuple('_SwitchEntry', ('switches', 'kwargs', 'handler'))

    def __init__(self):
        self.switches = []

    def register(self, *switches, **kwargs):
        def _register(switch_handler):
            type_parser = kwargs.pop('type', None)
            handler = _SwitchesRegistrar._Parser(switch_handler, type_parser)
            self.switches.append(self._SwitchEntry(switches, kwargs, handler))

            return switch_handler

        return _register

    def apply(self, parser):
        for switch_entry in self.switches:
            _help = getattr(switch_entry.handler, '__doc__', None)
            parser.add_argument(
                *switch_entry.switches,
                type=switch_entry.handler,
                **switch_entry.kwargs,
                help=('no help specified.' if _help is None else _help).strip()
            )

class _DeferredParsingRegistrar(_SwitchesRegistrar):
    class _DeferredArgument:
        deferred_parsing = True

        def __init__(self, switch_handler, type_parser, arg):
            self.switch_handler = switch_handler
            self.type_parser = type_parser
            self.arg = arg

        def __call__(self):
            return self.switch_handler(self.type_parser(self.arg))

        def __str__(self):
            return f"{type(self).__name__}({self.switch_handler}, {self.type_parser}, {self.arg})"

        def __repr__(self):
            return str(self)

    class _DeferredArgumentParser:
        def __init__(self, switch_handler, type_parser):
            self.switch_handler = switch_handler
            self.type_parser = type_parser

        def __call__(self, arg):
            return _DeferredParsingRegistrar._DeferredArgument(self.switch_handler, self.type_parser, arg)

        def __str__(self):
            return f"{type(self).__name__}({self.switch_handler}, {self.type_parser})"

        def __repr__(self):
            return str(self)

    def register(self, *switches, **kwargs):
        type_parser = kwargs.pop('type', lambda s: s)
        registration_function = super().register(*switches, **kwargs)
        def _register(switch_handler):
            registration_function(_DeferredParsingRegistrar._DeferredArgumentParser(switch_handler, type_parser))
            return switch_handler

        return _register

def registrator():
    return _SwitchesRegistrar()

def deferred_parsing_registrator():
    return _DeferredParsingRegistrar()

def apply_deferred_parsing(args):
    for arg_name in (a for a in dir(args) if not a.startswith('_')):
        arg = getattr(args, arg_name)

        logging.debug('examining %s for deferred parsing - %r', arg_name, arg)
        is_parsed = False
        if isinstance(arg, (list, tuple, set)):
            is_parsed = any((getattr(sub_arg, 'deferred_parsing', False) for sub_arg in arg))
            arg = [
                sub_arg() if getattr(sub_arg, 'deferred_parsing', False) else sub_arg
                for sub_arg in arg
            ]
        else:
            is_parsed = getattr(arg, 'deferred_parsing', False)
            if is_parsed:
                arg = arg()

        if is_parsed:
            logging.debug("post-parsing %s: %r", arg_name, arg)
            setattr(args, arg_name, arg)

def test():
    reg = registrator()

    @reg.register("-x", "--xoption", action="append")
    def handle_x(value):
        """
        X-Option documentation
        """
        print("handling -x", value)
        return {'x-value' : value}

    @reg.register("-y", "--yoption")
    def handle_y(value):
        print("handling -y", value)
        return {'y-value' : value}

    parser = argparse.ArgumentParser()
    reg.apply(parser)
    args = parser.parse_args("--xoption x1 -x x2 --yoption y1".split())
    args = parser.parse_args("-h".split())

if __name__ == "__main__":
    test()

