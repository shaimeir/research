#!/usr/bin/env python3

'''
allow an interface similar to argparse.ArgumentParser that 
'''

import argparse
import contextlib
import pathlib
import configobj
import sys
from collections import defaultdict

class ConfigSupportedArgumentParser(argparse.ArgumentParser):
    '''
    an argument parser interface that supports the fetching of command line arguments from an INI configuration file.
    assume a program main.py supports the following arugmnets:
        -a, --arg-a - a boolean argument.
        -b, --arg-b - an aggregate argument (i.e. an argument that can appear multiple times in the command line).
        -c, --arg-c - a multiple arguments switch.
        -d, --arg-d - some auxiliary arg we won't use in the ini.
    
    the ini file format should be as follows:
        [args-section-name]
        # for boolean switches don't specify a value
        arg-a=

        # for aggregating switches specify an index next to the argument name
        arg-b:1=hello
        arg-b:2=world

        # for multi-value switches specify the arguments as a comma-separated list and use quotation
        # marks to escape a comma
        arg-c="hello, world", "well hello to you too"

    so if main.py uses this class then you can specify the above ini file in the following syntax:
        ./main.py --config-file args.ini args-section-name -d some_value -b 'yet another value'

    and it will become equivalent to:
        ./main.py --arg-a --arg-b hello --arg-b world --arg-c 'hello, world' 'well hello to you too' -d some_value -b 'yet another value'

    use the --strict-config-file to enforce that all the files and sections specified via the multiple --config-file switches
    are asserted to exist.

    NOTE: using a single letter alias is supported but this doesn't make the connection between -b and --arg-b so
          they may not be ordered properly in the resultant command line.
    '''

    ConfigSwitchHelp = """take extra command line arguments from an INI config file.
    the first argument is the config file and the rest describe which (sub)section to use.
    """

    ReferenceKeyHelp = """use a given key as a reference to another section within the config file
    that contains additional switches to add to the command line.
    """

    ReferenceBaseSectionHelp = "the section under which the sections referred by the reference key switch are located."

    StrictConfigSwitchHelp = "assert the validity of the arguments specified with the configuration file switch"

    def __init__(self,
                 *args,
                 config_file_switch="--config-file",
                 strict_config_switch="--strict-config-file",
                 reference_key_switch="--reference-key",
                 reference_base_section_switch="--reference-base-section",
                 **kwargs):
        super().__init__(*args, **kwargs)

        self.add_argument(config_file_switch, required=False, nargs="+", help=self.ConfigSwitchHelp)
        self.add_argument(strict_config_switch, action="store_true", help=self.StrictConfigSwitchHelp)
        self.add_argument(reference_key_switch, default=None, dest="ref_key", help=self.ReferenceKeyHelp)
        self.add_argument(reference_base_section_switch, default=[], nargs="+", dest="ref_base_section", help=self.ReferenceBaseSectionHelp)

        self.config_switch_parser = argparse.ArgumentParser() 
        self.config_switch_parser.add_argument(config_file_switch, action="append", default=[], nargs="+", dest="config_file", help=self.ConfigSwitchHelp)
        self.config_switch_parser.add_argument(strict_config_switch, action="store_true", dest="strict_config", help=self.StrictConfigSwitchHelp)
        self.config_switch_parser.add_argument(reference_key_switch, default=None, dest="ref_key", help=self.ReferenceKeyHelp)
        self.config_switch_parser.add_argument(reference_base_section_switch, default=[], nargs="+", dest="ref_base_section", help=self.ReferenceBaseSectionHelp)

        self.command_line_in_processing = False

    @contextlib.contextmanager
    def process_command_line(self):
        try:
            self.command_line_in_processing = True
            yield

        finally:
            self.command_line_in_processing = False

    def parse_args(self, args=None, namespace=None):
        # parse_args and parse_known_args may (and infact in 3.6 they do) use each other as an implementation detail so
        # we use this flag as a context marker to avoid double parsing of the command line.
        if self.command_line_in_processing:
            return super().parse_args(args, namespace)

        with self.process_command_line():
            args = sys.argv[1:] if args is None else args

            # extract config file from the command line
            config_args = self.config_switch_parser.parse_known_args([a for a in args if a not in ('-h', '--help')])[0]
            config_file_descs = config_args.config_file
            strict = config_args.strict_config
            args = [arg for config_file_desc in config_file_descs for arg in self._handle_config_desc(config_file_desc, strict, config_args.ref_key, config_args.ref_base_section)] + list(args)

            return super().parse_args(args, namespace)

    def parse_known_args(self, args=None, namespace=None):
        # parse_args and parse_known_args may (and infact in 3.6 they do) use each other as an implementation detail so
        # we use this flag as a context marker to avoid double parsing of the command line.
        if self.command_line_in_processing:
            return super().parse_known_args(args, namespace)

        with self.process_command_line():
            args = sys.argv[1:] if args is None else args

            # extract config file from the command line
            config_args = self.config_switch_parser.parse_known_args([a for a in args if a not in ('-h', '--help')])[0]
            config_file_descs = config_args.config_file
            strict = config_args.strict_config
            args = [arg for config_file_desc in config_file_descs for arg in self._handle_config_desc(config_file_desc, strict, config_args.ref_key, config_args.ref_base_section)] + list(args)

            return super().parse_known_args(args, namespace)

    def _handle_config_desc(self, config_file_desc, strict, ref_key, ref_base_section):
        # config file specified!
        top_config_file = configobj.ConfigObj(config_file_desc[0])

        # load the proper sub-section in the config file
        config_file = top_config_file
        for section_name in config_file_desc[1:]:
            if strict:
                assert section_name in config_file.sections, FileNotFoundError('invalid (sub)section name', config_file_desc[0], config_file_desc, section_name)
            elif section_name not in config_file.sections:
                config_file = None
                break

            config_file = config_file[section_name]

        if config_file is None:
            return []

        # load the proper reference base section
        ref_base_config = top_config_file
        for section_name in ref_base_section:
            if section_name in ref_base_config.sections:
                ref_base_config = ref_base_config[section_name]
            else:
                # can't find the reference base section
                ref_base_config = None
                break

        decorate_switch = lambda s: f"-{s}" if len(s) == 1 else f"--{s}"

        # translate configuration entries into switches
        switches = []
        ordered_switches = defaultdict(lambda : defaultdict(list))
        config_objects_queue = [config_file]

        visited_configs = set()

        while len(config_objects_queue) > 0:
            current_config_file = config_objects_queue.pop(0)

            for switch, value in current_config_file.items():
                if isinstance(value, dict):
                    # sub-section...
                    continue
                elif switch == ref_key:
                    if ref_base_config is not None:
                        if not isinstance(value, (list, tuple)):
                            value = [value]

                        for ref_section in value:
                            if ref_section in ref_base_config.sections and ref_section not in visited_configs:
                                visited_configs.add(ref_section)
                                config_objects_queue.append(ref_base_config[ref_section])

                    continue

                value = list(value) if isinstance(value, (list, tuple)) else [value]
                value = [v.strip() for v in value if len(v.strip()) > 0]
                if ':' in switch:
                    # an ordered switch
                    switch, order = tuple(switch.split(':'))
                    order = int(order)
                    ordered_switches[switch][order] = value
                else:
                    # an unordered switch
                    switches += [decorate_switch(switch)] + value

        # merge the ordered switch into the switches list
        for switch, ordered_values in ordered_switches.items():
            for order, value in sorted(ordered_values.items(), key=lambda _order_value: _order_value[0]):
                switches.append(decorate_switch(switch))
                switches += value

        return switches

def main():
    file_dir = pathlib.Path(__file__).parent/'sample-config.ini'
    parser = ConfigSupportedArgumentParser(description=f"test this program using a sample configuration file ({file_dir}, section: command-line, sub-section: args).")
    parser.add_argument("-a", "--arg-a", action="store_true", help="a boolean flag.")
    parser.add_argument("-b", "--arg-b", action="append", help="an aggregating switch.")
    parser.add_argument("-c", "--arg-c", nargs="+", help="a multi-value switch.")
    parser.add_argument("-d", "--arg-d", nargs="*", action="append", help="an aggregating multi-valued switch.")
    parser.add_argument("-e", "--arg-e", action="store_true", help="another boolean flag.")
    args = parser.parse_args()

    for attr in dir(args):
        if attr.startswith("_"):
            continue

        value = getattr(args, attr)
        if value is not None:
            print(attr, "=", value)

if __name__ == "__main__":
    sys.exit(main())

