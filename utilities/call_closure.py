#!/usr/bin/env python3

import copy
import sys

class DeferredCallClosure:
    def __init__(self, callable_obj, *args, **kwargs):
        self.callable_obj = callable_obj
        self.args = args
        self.kwargs = kwargs
        self.is_closure = True

    def __call__(self, **kwargs):
        # join the previous kwargs and the current kwargs and give precedence to the current kwargs in case of conflict
        kwargs.update({
            k : v
            for k, v in self.kwargs.items()
            if k not in kwargs
        })

        return self.callable_obj(*self.args, **kwargs)

def is_closure(obj):
    return getattr(obj, 'is_closure', False)

def _test_callable(a, b, c, **kwargs):
    print("callable args:")
    print("\t a =", a)
    print("\t b =", b)
    print("\t c =", c)
    print("kwargs:")
    for k, v in kwargs.items():
        print("\t", k, "=", v)

def test():
    DeferredCallClosure(_test_callable, 1337, 1338, some_kwarg="some_kw_value", some_other_kwarg="some_other_kwarg")(c=1339)

if __name__ == "__main__":
    sys.exit(test())

