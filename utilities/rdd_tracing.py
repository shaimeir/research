#!/usr/bin/env python3

import types
import contextlib
import pyspark
import logging
import os
import dis
import inspect
import multiprocessing
import pprint
import sys
import copy

RddDecorations = {}
ReferenceGlobals = sys.modules.get('__main__')
RddCallbackLogging = False

def toggle_rdd_callback_logging():
    global RddCallbackLogging
    RddCallbackLogging = True

def register_globals(_globals):
    global ReferenceGlobals
    ReferenceGlobals = _globals.copy()

def rdd_decorator(rdd_name, operation_name = None):
    def _rdd_decorator(callback):
        global RddDecorations

        logging.info("registering %s to handle %r/%r", getattr(callback, '__name__', repr(callback)), rdd_name, operation_name)

        RddDecorations[(rdd_name, operation_name)] = callback
        return callback
    return _rdd_decorator

rdd_trace = rdd_decorator

def post_operation(callback):
    def _callback(*args, **kwargs):
        c = capture()
        yield c
        callback(c.value, *args, **kwargs)
    return _callback

def rdd_post(rdd_name, operation_name = None):
    def _rdd_decorator(callback):
        return rdd_decorator(rdd_name, operation_name)(post_operation(callback))

    return _rdd_decorator

# decorates a callback passed to an RDD transformation operation (map, reduceByKey, ...)
# and sets the name of the rdd_decorator to use when tracing the given function
def rdd_as(new_rdd_name):
    def _rdd_decorator(func):
        setattr(func, '__rdd_name__', new_rdd_name)
        return func
    return _rdd_decorator

def _lookup_rdd_decoration(rdd_name, operation_name):
    global RddDecorations
    global RddCallbackLogging

    option1 = RddDecorations.get((rdd_name, operation_name))
    option2 = RddDecorations.get((rdd_name, None))

    if option1 is None and option2 is not None:
        RddDecorations.pop((rdd_name, None))

    def _callback_logging_default_handler(*a,**kw):
        yield

    return option1 if option1 is not None else option2 if option2 is not None or not RddCallbackLogging else _callback_logging_default_handler

def _get_lambda_internal_callback(_lambda):
    instructions = list(dis.get_instructions(_lambda))
    if len([inst for inst in instructions if inst.opname in ('CALL_FUNCTION', 'CALL_FUNCTION_KW', 'CALL_FUNCTION_EX')]) != 1:
        # either no function calls or too many of them
        return None

    load_instructions = [inst for inst in instructions if inst.opname in ('LOAD_GLOBAL', 'LOAD_ATTR', 'LOAD_DEREF')]
    internal_callback = None
    looking_for_attrs = False
    for inst in load_instructions:
        if looking_for_attrs:
            if inst.opname == 'LOAD_ATTR':
                internal_callback += (inst.argval,)
            else:
                return internal_callback

        elif inst.opname in ('LOAD_DEREF', 'LOAD_GLOBAL'):
            looking_for_attrs = True
            internal_callback = (inst.argval,)

    return internal_callback

def _get_global_callable_by_name(name):
    if name is None:
        return None

    assert len(name) > 0
    if not hasattr(ReferenceGlobals, name[0]):
        return None

    obj = getattr(ReferenceGlobals, name[0])
    if obj is None:
        return None

    for element in name[1:]:
        if hasattr(obj, element):
            obj = getattr(obj, element)
        else:
            return None

    return obj

class _ResultCapture:
    def __init__(self):
        self.__captured = False
        self.__value = None

    @property
    def value(self):
        assert self.__captured
        return self.__value

    def capture(self, value):
        self.__captured = True
        self.__value = value

def capture():
    return _ResultCapture()

@contextlib.contextmanager
def _allow_result_capture(method_proxy_context):
    if isinstance(method_proxy_context, types.GeneratorType):
        for res_capture in method_proxy_context:
            if res_capture is None:
                res_capture = _ResultCapture()
            yield res_capture

    else:
        yield _ResultCapture()

@contextlib.contextmanager
def _callback_logging(rdd_name, method_name):
    global RddCallbackLogging

    if RddCallbackLogging:
        rddlog("rddlog: calling %r/%r", rdd_name, method_name)

    try:
        yield

    except BaseException:
        if RddCallbackLogging:
            rddlog("rddlog: rdd error %r/%r", rdd_name, method_name, exc_info=True)
        raise

    else:
        if RddCallbackLogging:
            rddlog("rddlog: %r/%r called successfully", rdd_name, method_name)

def _callback_proxy(callback, proxy, rdd_name, method_name):
    def __callback_proxy(*args, **kwargs):
        with _callback_logging(rdd_name, method_name):
            with _allow_result_capture(proxy(*args, **kwargs)) as res_capture:
                result = callback(*args, **kwargs)
                res_capture.capture(result)

        return result

    return __callback_proxy

def _create_method_proxy(method_name, method):
    def _method_proxy(rdd, *args, **kwargs):
        _callables = list(filter(callable, tuple(args) + tuple(kwargs.values())))
        if len(_callables) == 1:
            callback = _callables[0]
            rdd_name = rdd.name()
            if rdd_name is None:
                _callback = callback
                if isinstance(callback, types.LambdaType):
                    _callback = _get_global_callable_by_name(_get_lambda_internal_callback(callback))
                    _callback = callback if _callback is None else _callback

                rdd_name = getattr(_callback, '__rdd_name__', getattr(_callback, '__name__', None))

            operation_decorator = _lookup_rdd_decoration(rdd_name, method_name)
            if operation_decorator is not None:
                logging.info("RDD Trace: found a trace for %r(%r)/%r", rdd_name, rdd.name(), method_name)
                decorated_callback = _callback_proxy(callback, operation_decorator, rdd_name, method_name)

                if callback in args:
                    _i = args.index(callback)
                    args = tuple(args[:_i]) + (decorated_callback,) + tuple(args[_i+1:])
                else: # callback in kwargs
                    keys = [k for k, v in kwargs.items() if v is callback]
                    for _k in keys:
                        kwargs[_k] = decorated_callback

        result = method(*args, **kwargs)
        if isinstance(result, pyspark.RDD):
            result = trace_rdd(result)

        return result

    return _method_proxy

def _comment(rdd, *arg, **kwargs):
    return rdd

def trace_rdd(rdd):
    if not hasattr(rdd, '__traced_methods__'):
        setattr(rdd, '__traced_methods__', [])

    for method_name in [name for name in dir(rdd) if not name.startswith("_")]:
        if method_name in ("setName", "name") or method_name in rdd.__traced_methods__:
            # we use these and there's no point in overriding them (it causes an infinite loop)
            continue

        method = getattr(rdd, method_name)
        if not isinstance(method, types.MethodType):
            continue

        setattr(rdd, method_name, types.MethodType(_create_method_proxy(method_name, method), rdd))
        rdd.__traced_methods__.append(method_name)

    setattr(rdd, 'comment', types.MethodType(_comment, rdd))

    return rdd

def rddlog(*args, **kwargs):
    logging.error(*args, **kwargs)

def test():
    logging.basicConfig(format="%(message)s", level=logging.INFO)
    logging.info("testing RDD tracing - main process is %d", os.getpid())

    @rdd_decorator("test_rdd_1", "map")
    def map_print_before_and_after(x):
        rddlog("proc[%d] map before = %d", os.getpid(), x)
        c = capture()
        yield c
        rddlog("proc[%d] map after = %d", os.getpid(), c.value)

    @rdd_decorator("test_rdd_1_1")
    def filter_print_before_and_after(*x, **xx):
        rddlog("proc[%d] filter = %r,%r", os.getpid(), x,xx)
        c = capture()
        yield c

        rddlog("proc[%d] filter result = %r", os.getpid(), c.value)

    @rdd_decorator("test_rdd_2", "flatMap")
    def rdd2_map_print_before_and_after(x):
        rddlog("RDD #2 proc[%d] before = %d", os.getpid(), x)
        c = capture()
        yield c
        rddlog("RDD #2 proc[%d] after = %r", os.getpid(), c.value)

    @rdd_decorator("test_rdd_3", "reduceByKey")
    def rdd3_map_print_before_and_after(x, y):
        rddlog("RDD #3 proc[%d] reduce by key before = %r, %r", os.getpid(), x, y)
        c = capture()
        yield c
        rddlog("RDD #3 proc[%d] reduce by key after = %r", os.getpid(), c.value)

    @rdd_decorator("identity_step")
    def id_step_tracing(x):
        rddlog("id step: %r", x)
        yield

    @rdd_decorator("times_two")
    def times_2_trace(x):
        c = capture()
        yield c
        rddlog("mult by 2: %r, %r", x, c.value)

    global identity_step
    def identity_step(x):
        return x

    @rdd_as("times_two")
    def times_2(x):
        return x*2

    with pyspark.SparkContext("local[{}]".format(multiprocessing.cpu_count()), "pyspark") as sc:
        traced_rdd_1 = trace_rdd(sc.parallelize([1,2,3])).setName("test_rdd_1").comment("create the RDD")

        logging.info("testing specific decorator")
        logging.info("rdd #1 map: %r", traced_rdd_1.map(lambda x: x*2).comment("multiply by 2").collect())

        logging.info("testing generic decorator")
        logging.info("rdd #1 filter: %r", traced_rdd_1.setName("test_rdd_1_1").filter(lambda x: x%2 == 0).comment("just even numbers").collect())

        logging.info("testing chaining")
        collection = traced_rdd_1.map(lambda x: x*2).\
                map(lambda x: identity_step(x)).\
                map(lambda x: x).\
                map(times_2).\
                setName("test_rdd_2").flatMap(lambda x: [(1, x)]).comment("some more comments").\
                setName("test_rdd_3").reduceByKey(lambda x, y: x+y).comment("even more comments").\
                collect()

        logging.info("chain collection result = %r", collection)

if __name__ == "__main__":
    test()

