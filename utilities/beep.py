#!/usr/bin/env python3

# Generate a 440 Hz square waveform in Pygame by building an array of samples and play
# it for 5 seconds.  Change the hard-coded 440 to another value to generate a different
# pitch.
#
# Run with the following command:
#   python pygame-play-tone.py

import contextlib
import os
import pygame
import random
import logging

def _play_mp3(mp3):
    logging.info("playing %s", mp3)
    try:
        pygame.init()
        pygame.mixer.music.load(mp3)
        pygame.mixer.music.play(0)
        pygame.event.wait()
    except pygame.error:
        logging.info("couldn't play sound %s", mp3)
    finally:
        pygame.quit()

@contextlib.contextmanager
def play_random_sound_on_completion():
    try:
        yield

    finally:
        sounds_dir = os.sep.join((os.path.dirname(os.path.abspath(__file__)), 'sounds'))
        sounds = [os.path.sep.join((sounds_dir, f)) for f in os.listdir(sounds_dir) if f.lower().endswith(".mp3")]

        if len(sounds) > 0:
            _play_mp3(random.choice(sounds))
        else:
            logging.info("sounds list is empty (%s)", sounds_dir)

def main():
    logging.basicConfig(level=logging.DEBUG)
    with play_random_sound_on_completion():
        print("playing sound for a test")

if __name__ == "__main__":
    main()

