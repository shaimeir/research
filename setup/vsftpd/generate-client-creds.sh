#!/usr/bin/env bash

set -eu

readonly program_dir=$(dirname $(readlink -f ${BASH_SOURCE[0]}))
readonly openssl_conf="${program_dir}/openssl.cnf"

readonly default_server_certificate="/etc/vsftpd/certificate.pem"
readonly default_server_key="/etc/vsftpd/private.pem"

function script_help()
{
    (
        cat <<-EOFSEQ
            generate a new client private key and certificate for vsftpd access.

            usage: ${BASH_SOURCE[0]} [-h] [-x] [-o OUTPUT-FILE] [-n CLIENT-NAME] [-c SERVER-CERTIFICATE] [-k SERVER-KEY]
            | * -h - print this help.
            | * -x - set bash's -x flag.
            | * -o OUTPUT-FILE - where to write the client certificate to (prints to the screen if this flag isn't specified).
            | * -n CLIENT-NAME - the name of the client to use in the certificate. (required)
            | * -c SERVER-CERTIFICATE - the server certificate to generate the client creds for. (default: ${default_server_certificate})
            | * -k SERVER-KEY - the server key to use for signing. (default: ${default_server_key})
EOFSEQ
    ) | sed -r 's/^\s*\|?//' >&2
}

function perror()
{
    echo "error: ${@}" >&2
    script_help
    exit 1
}

function cleanup()
{
    if [ -n "${tmp_dir:-}" ]
    then
        rm -rf "${tmp_dir}"
    fi

    true # make sure cleanup always succeeds
}

trap cleanup EXIT

server_certificate="${default_server_certificate}"
server_key="${default_server_key}"

while getopts ":hxo:n:c:k:" option
do
    case "${option}" in
        "h") script_help
             exit 0
             ;;

        "x") set -x
             ;;

        "o") exec >"${OPTARG}"
             ;;

        "n") client_name="${OPTARG}"
             ;;

        "c") server_certificate="${OPTARG}"
             ;;

        "k") server_key="${OPTARG}"
             ;;

        *) perror "unknown command line switch ${OPTARG}"
    esac
done

[ -n "${client_name:-}" ] || perror "you must specify a client name (-n)."

# generate a temp work dir
tmp_dir="/tmp/client-creds-tmp-dir/"
rm -rf "${tmp_dir}"
mkdir -p "${tmp_dir}"

# define the CA directory defined in openssl.cnf and prepare it's directory structure
export CA_DIR="${tmp_dir}/demoCA/"
mkdir -p "${CA_DIR}"

mkdir -p "${CA_DIR}" "${CA_DIR}/newcerts/"
touch "${CA_DIR}/index.txt"
head -c 16 /dev/urandom | xxd -ps >"${CA_DIR}/serial"

# define intermediate file names
private_key="${tmp_dir}/private_key.pem"
csr_file="${tmp_dir}/csr.pem"
certificate="${tmp_dir}/signed-cert.pem"

# create the private key
openssl genrsa -out "${private_key}" 2048 >&2

# create the certificate sign request file (CSR, an unsigned certificate...)
openssl req -config "${openssl_conf}" -key "${private_key}" -new -sha256 -out "${csr_file}" <<-PARAMSEND
IL
Israel
Ra'anana
Secdo
Research (${client_name})
${client_name}



PARAMSEND

mkdir -p "${CA_DIR}" "${CA_DIR}/newcerts/"
touch "${CA_DIR}/index.txt"
head -c 16 /dev/urandom | xxd -ps >"${CA_DIR}/serial"

# now sign the CSR to finish creating the certificate
yes | openssl ca -config "${openssl_conf}" -extensions usr_cert -notext -cert "${server_certificate}" -keyfile "${server_key}" -md sha256 -in "${csr_file}" -out "${certificate}"

# and now we can write the creds to the output file/screen
cat "${private_key}" "${certificate}"

