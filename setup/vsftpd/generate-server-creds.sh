#!/usr/bin/env bash

set -eu

# this script generates a new key and certificate for vsftpd.
# requires root privileges

readonly default_service_name="FTPS service"
readonly default_vsftpd_config_dir="/etc/vsftpd/"

function script_help()
{
    (
        cat <<-EOFSEQ
            generate a server's credentials.

            usage: ${BASH_SOURCE[0]} [-h] [-x] [-o OUTPUT-DIRECTORY] [-n SERVICE-NAME] [-c COMMON-NAME]
            | * -h - print this help.
            | * -x - turn on bash's -x flag.
            | * -o OUTPUT-DIRECTORY - vsftpd's configuration directory (default: ${default_vsftpd_config_dir}).
            | * -n SERVICE-NAME - the service name to use in the certificate (default: ${default_service_name}).
            | * -c COMMON-NAME - the name to use in the certificate so it can be verified by the clients. if not specified, ip will be inferred.
EOFSEQ
    ) | sed -r 's/^\s*\|?//' >&2
}

function perror()
{
    echo "error: ${@}" >&2
    script_help
    exit 1
}

vsftpd_config_dir="${default_vsftpd_config_dir}"
service_name="${default_service_name}"

while getopts ":hxo:n:c:" option
do
    case "${option}" in
        "h") script_help
             exit 0
             ;;

        "x") set -x
             ;;

        "o") vsftpd_config_dir="${OPTARG}"
             ;;

        "n") service_name="${OPTARG}"
             ;;

        "c") common_name="${OPTARG}"
             ;;

        *) perror "unknown command line swithc ${OPTARG}"
    esac
done

# infer IP address if needed
if [ -z "${common_name:-}" ]
then
    candidate_ip=$(
        ifconfig | awk '
            # this awk script attempts to extract the first non-loopback IP
            # from the output of the ifconfig command
            $1 == "inet" && $2 !~ /^(0|127)\./ {
                print $2
                exit 0
            }
        '
    )

    [ -n "${candidate_ip}" ] || perror "couldn't extract IP address from ifconfig"

    read -r -n 1 -p "Would you like to use '${candidate_ip}' as the common name for the certificate? [y/n]"

    [ "${REPLY/Y/y}" == 'y' ] || perror "use the -c switch to specify a common name."

    common_name="${candidate_ip}"
fi

# generate the certificate

vsftpd_key="${vsftpd_config_dir}/private.pem"
vsftpd_certifacte="${vsftpd_config_dir}/certificate.pem"

mkdir -p "${vsftpd_config_dir}"

# generate the private key
openssl req -x509 -nodes -newkey rsa:4096 -keyout "${vsftpd_key}" -out "${vsftpd_certifacte}" <<-EOFSEQ
IL
Israel
Ra'anana
Secdo
Research (${service_name})
${common_name}
touch.shai@sec.do
EOFSEQ

chmod 444 "${vsftpd_key}" "${vsftpd_certifacte}"

openssl x509 -in "${vsftpd_certifacte}" -text

