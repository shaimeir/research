#!/usr/bin/env bash

set -eu

col=1
output="&1"

exec 3<&0
exec 4>&1

while getopts ":o:c:i:" option
do
    case "${option}" in
        "o") exec 4>"${OPTARG}"
             ;;
        "c") col="${OPTARG}"
             ;;
        "i") exec 3<"${OPTARG}"
             ;;
    esac
done

shift $((${OPTIND}-1))

gawk -v col=${col} -F, '{
        col=int(col)
        if (col == 0) {
            col=1
        }

        print $(col) " " $0
    }' <&3 | sed -r 's/^"([^"]+)"/\1/g' | sort -n | cut -d' ' -f2- >&4


