#!/usr/bin/env python3

import csv
import argparse
import sys
import contextlib

@contextlib.contextmanager
def auto_context(x):
    yield x

def main():
    parser = argparse.ArgumentParser(description='print a column from a CSV file.')
    parser.add_argument("-c", "--column", type=int, default=1, help="column index to print.")
    parser.add_argument("-o", "--output", default="-", help="output file. '-' for stdout.")
    parser.add_argument("csv", nargs="?", default="-", help="the CSV file to parse. '-' for stdin.")

    args = parser.parse_args()

    with contextlib.suppress(BrokenPipeError):
        with (auto_context(sys.stdout) if '-' == args.output else open(args.output, "wt")) as output_stream:
            with (auto_context(sys.stdin) if '-' == args.csv else open(args.csv, "rt")) as input_stream:
                reader = csv.reader(input_stream, delimiter=',', quotechar='"')
                for line in reader:
                    print(line[args.column - 1], file=output_stream)

if __name__ == "__main__":
    main()

