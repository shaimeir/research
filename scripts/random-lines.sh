#!/usr/bin/env bash

set -eu

input_range=
head_count=

function print_help()
{
    echo "print random lines from a file."
    echo " * -h - print this help."
    echo " * -o - output file. default to stdout."
    echo " * -n - the number of lines to print."
    echo " * -i - LOW-HIGH. the range of lines to print from (mandatory)."
    echo " * -f - the file to print the lines from. default to stdin."
}

while getopts ":o:n:i:f:h" option
do
    case "${option}" in
        "o") exec >"${OPTARG}"
             ;;
        "n") head_count="-n ${OPTARG}"
             ;;
        "i") input_range="-i ${OPTARG}"
             ;;
        "f") exec <"${OPTARG}"
             ;;
        "h") print_help >&2
             exit 0
             ;;
    esac
done

[ "${input_range}" != "" ] || (echo "-i is a mandatory switch" >&2 && exit 1)

line_printing_script="$(mktemp -u /tmp/line-print-XXXXXX.sed)"

function cleanup()
{
    rm -f ${line_printing_script}
}
trap cleanup EXIT

shuf ${input_range} ${head_count} | sort -n | awk '{print $0 "p"}' >${line_printing_script}
sed -n -f ${line_printing_script}

