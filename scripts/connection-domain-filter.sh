#!/usr/bin/env bash

set -eu

exec 3<&0
exec 4>&1

domain="internal"

function perror()
{
    echo ${@} >&2
    exit 1
}

col=
while getopts ":o:c:i:x" option
do
    case "${option}" in
        "o") exec 4>"${OPTARG}"
             ;;
        "i") exec 3<"${OPTARG}"
             ;;
        "c") col="${OPTARG}"
             ;;
        "x") domain="external"
             ;;
    esac
done

shift $((${OPTIND}-1))

read first_line <&3

if [ "${col}" == "" ]
then
    col=$(echo "${first_line}" | awk -F, '{
        word="not found"
        for (i=1; i<= NF; i++) {
            if ($i == "\"remote\"") {
                print i
                exit
            } else if ($i ~ /\"[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\"/) {
                word=i
            }
        }

        print word
    }')

    if grep -P '^\d+$' <<<"${col}" &>/dev/null
    then
        echo "inferred the remote column #${col}" >&2
    else
        echo 'could not infer column, defaulting to 1' >&2
        col=1
    fi
fi

if [ "${#}" -eq 1 ]
then
    domain=$(echo "${1}" | tr '[:upper:]' '[:lower:]')
    ([ "${d}" == "internal" ] || [ "${d}" == "external" ]) || perror "invalid -d argument '${d}'"
fi

echo "filtering connections in '${domain}' domain" >&2

filter='$'${col}' ~ /"(172\.16|172\.17|172\.18|172\.19|172\.20|172\.21|172\.22|172\.23|172\.24|172\.25|172\.26|172\.27|172\.28|172\.29|172\.30|172\.31|192\.168|10)\./'
if [ "${domain}" == "external" ]
then
    filter='!('${filter}')'
fi

awk_filter='{
    if ('${filter}') {
        print $0
    }
}'
awk_filter=$(echo "${awk_filter}" | tr -d '\n')

(echo ${first_line}; cat <&3) | awk -F, "${awk_filter}" >&4

