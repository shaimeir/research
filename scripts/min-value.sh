#!/usr/bin/env bash

set -eu

col=1
output="&1"
value=1

exec 3<&0
exec 4>&1

while getopts ":o:c:i:v:" option
do
    case "${option}" in
        "o") exec 4>"${OPTARG}"
             ;;
        "c") col="${OPTARG}"
             ;;
        "v") value="${OPTARG}"
             ;;
        "i") exec 3<"${OPTARG}"
             ;;
    esac
done

shift $((${OPTIND}-1))

gawk -v col=${col} -v bar_value=${value} -F, '{
        col=int(col)
        if (col == 0) {
            col=1
        }

        value=$(col)
        gsub(/\"/,"",value)

        if (int(value) >= int(bar_value)) {
            print  " " $0
        }
    }' <&3



