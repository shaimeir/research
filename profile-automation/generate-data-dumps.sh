#!/usr/bin/env bash

# this file takes a unified configuration file and generates all the dumps specified inside it's dumps section.
# the ini configuration file must  have the following sections in order to conform to this scripts mechanics:
#
#   -------------
#
#   # this section describes the company similarly to ~/.secdo-dbs in the format specified in db-utils/dbcursors/default_dbs.py
#   [companies]
#
#   ## each sub-section is a company in the standard format and with the semantics specific to the database
#   ## (e.g. companies on memsql servers can have the "compress" key)
#   [[lab.bioc]]
#   company=bioc
#   dbtype=memsql
#   ...
#
#   ## yet another company
#   [[lab.feeder]]
#   ...
#
#   ## a company we won't perform a dump for
#   [[lab.not-interesting]]
#   ...
#
#   # the next section is the dumps section which specify which dumps are required
#   # to be performed via 3 sub-sections.
#   [dumps]
#
#   ## this first section defines the dump details (which is in fact the dump parameters).
#   ## these shouldn't specify a company as it will be specified in the dump-specs sub-section.
#   ## sub-sections of this section will be referenced in the dump-specs section.
#   [[dump-details]]
#
#   ### these sections specify the dump filters
#   [[[file-system-dump]]]
#   scheme=file-system # mandatory field!
#   after:1=3d # dump up to 3 days ago
#   pname=svchost.exe, services.exe # only dump these processes
#   ...
#
#   ### yet another dump type
#   [[[network-dump]]]
#   #### the detail-sets field refers to detail setss defined in the dumps.dump-detail-sets sub-section
#   #### and applies the filters specified in them to the the current dump.
#   #### this key is optional.
#   detail-sets=detail-set-1, detail-set-2, ...
#
#   #### and now additional dump-specific filters can be added.
#   scheme=network
#   after:1=1w # since one week ago
#   before:1=2d # until 2 days ago
#   ...
#
#   ### and this dump will not be used
#   [[[different-network-dump]]]
#   scheme=network
#   after:1=1M # since one month ago
#   before:1=2w # until 2 weeks ago
#   instance=host_id,4fd0f617a9c59a49999f060478dd62fe,11235 # just for this instance id
#   ...
#
#   ## this optional section simply defines filters that will be used in all the dumps (if present)
#   [[global-dump-details]]
#   after:1="2017-01-01 13:00:00" # all the dumps will be filtered by entries only after the - 01/01/2017 at 13:00
#   ...
#
#   ## this defines sets of filters that can be referenced from dumps and included only in the specified dumps
#   [[dump-detail-sets]]
#
#   [[[detail-set-1]]]
#   pname:10=services.exe
#   pname:11=chrome.exe
#
#   [[[detail-set-2]]]
#   after:10=10d
#   before:10=7d
#
#   ## this section connects the dump-details section with the companies section and
#   ## specifies which dumps are relevant and to which companies.
#   ## the reason this section is used instead of automatically generating all possible company/dump pairs
#   ## is for the convinience of control (and allowing certain companies to have different dumps in the future
#   ## if necessary).
#   [[dump-specs]]
#   companies=lab.bioc, lab.feeder # note that we don't specify lab.not-interesting so it's dump won't be generated
#   dumps=file-system-dump, network-dump
#
#   -------------
#
# the section names "companies" and "dumps" can be modified via command line.
#
# the output path is given to this script via a command line and the dumps will be placed in a dumps/ directory within that output path.
#
# ------------
# SUMMARY FILE
# ------------
# along with the dumps themselves this script generates a summary file that is structured as follows:
#   <company> <dump-detail> <args-signature> <dump-file>
#
# the values are space separated and the values are pretty self explanatory:
#   company - the company specified in the configuration's [companies] section for which the dump was generated.
#   dump-detail - the dump type, as specified in the configuration's [dumps][[dump-details]] sub-section, generated for that company.
#   args-signature - a hueristic hash of the arguments used to create the dump. this can also hold the string 'accept' (explained in
#                    the following paragraph about dump resumption).
#   dump-file - the full path of the compressed CSV dump.
#
# ---------------
# DUMP RESUMPTION
# ---------------
#
# the summary file is used internally to enable the continue a previously interrupted dump (if this script was abruptly terminated or if it finished execution properly).
# if an existing dump listed in a pre-existing summary file from a previous execution located in the path of the current execution's expected summary file then it's
# evaluated via the continuation strategy specified in the command line (the default is 'validate-args').
#
# first, an existing dump will have it's date compared to the pre-existing summary file's date.
# the summary file should be newer (by design) as it is modified after every dump.
# if the dump is newer or doesn't exist then it is removed (deleted from the file system and removed from the summary file).
#
# after the date verification the following continuation
# there's a command line switch (-t) to select the continuation strategy.
# the continuation strategies are:
#
#       'always-accept' - consider the date verification alone and accept/decline the dump on it's basis.
#       'never-accept' - never consider a pre-existing summary file.
#       'validate-args' - generate a hueristic signature of the args a new dump will be generated with and compare it against the signature written in the pre-existing summary file.
#                         if the summary file signature is the magic string 'accept' then the dump is accepted anyway.
#                         if the signatures match then the dump is accepted.
#                         the hueristic signature is explained below.
#
# how the args signature works:
# the args are specified in an ini configuration file as pairs of key and value.
# simply list these arguments in the format <key><tab><value> (via ini-query.py -Ktq), sort them and calculate an MD5 on the resultant text.
# this technique is not bullet proof but it'll do just fine in most cases (and those that require manual tailoring can use
# the 'accept' magic signature in the summary file where this comparison is expected to fail).
#

set -eu

current_dir="$(dirname $(readlink -f ${BASH_SOURCE[0]}))"
db_utils_dir="${current_dir}/db-utils"
ini_query="${current_dir}/ini-query.py"

sql_dumper="${current_dir}/db-utils/sql-dump.sh"
sql_query_script="${current_dir}/db-utils/scheme-dumper-v3/dump-scheme-v3.py"
summary_script="${current_dir}/summary-file/summary-ops.py"

function print_help()
{
    echo "perform data dumps based on a central configuration file."
    echo ""
    echo "usage: ${BASH_SOURCE[0]} [-x] [-k] [-C] [-s DATABASES-SECTION] [-d DUMPS-SECTION] [-m SUMMARY-FILE-NAME] [-t CONTINUATION-STRATEGY] -o OUTPUT-PATH CONFIGURATION-FILE"
    echo " * -x - set bash's -x flag."
    echo " * -k - keep intermediate dir."
    echo " * -s COMPANIES-SECTION - the companies sections in the configuration file (as usually specified in the database config file ~/.secdo-dbs). (default: companies)"
    echo " * -d DUMPS-SECTION - the dumps sections in the configuration file. (default: dumps)"
    echo " * -C - turn on traffic compression when dumping."
    echo " * -m SUMMARY-FILE-NAME - a name for the dump summary file. (default: dump-summary.json)"
    echo " * -t CONTINUATION-STRATEGY - how to validate an exisiting dump with regard to configuration. this switch can accept 'always-accept', 'validate-args' or 'never-accept'. (default: 'validate-args')"
    echo " *                            *   'always-accept' - don't consider the configuration at all when encountering an existing dump with a valid timestamp."
    echo " *                            *   'validate-args' - compare arguments signature in summary file with configuration's signature and skip the dump in case they match."
    echo " *                            *   'never-accept' - always ignore an existing dump summary file and dump everything from scratch."
    echo " * -o OUTPUT-PATH - the directory to write the dumps to. (mandatory)"
    echo " * CONFIGURATION-FILE - the ini configuration file with the servers and dumps configuration."
}

function perror()
{
    echo "error: ${@}" >&2
    exit 1
}

function trace()
{
    echo "$(date) ${@}" >&2
}

companies_section="companies"
dumps_section="dumps"
dump_summary_file_name="dumps-summary.json"

x_flag=""

continuation_strategy="validate-args"

while getopts ":hxkCs:d:m:t:o:" option
do
    case "${option}" in
        "h") print_help >&2
             exit 0
             ;;

        "x") set -x
             x_flag="-x"
             ;;

        "k") keep_intermediate=1
             ;;

        "C") traffic_compression_switch="-C"
             ;;

        "s") companies_section="${OPTARG}"
             ;;

        "d") dumps_section="${OPTARG}"
             ;;

        "m") dump_summary_file_name="${OPTARG}"
             ;;

        "t") continuation_strategy="${OPTARG}"
             ;;

        "o") output_path="${OPTARG}"
             ;;

        "*") print_help >&2
             perror "invalid switch encountered."
    esac
done
shift $((${OPTIND}-1))

if [ -z "${output_path:-}" ]
then
    print_help >&2
    perror "missing output directory."
fi

if [ "${#}" -ne "1" ]
then
    print_help >&2
    perror "invalid number of configuration specified."
fi

ignore_signatures=""
case "${continuation_strategy}" in
    "always-accept") ignore_signatures="--ignore-signatures"
                     ;;

    "validate-args"|"never-accept") :
                                    ;;

    *) print_help >&2
       perror "invalid value specified with -t ('${continuation_strategy}')"
esac

configuration_file="${1}"

intermediate_dir="${output_path}/intermediate-dir/"
dumps_dir="${output_path}/dumps/"
logs_dir="${output_path}/logs"
dump_summary_file="${output_path}/${dump_summary_file_name}"

# register cleanup routine to remove our temp directory
function cleanup()
{
    [ -z "${keep_intermediate:-}" ] && rm -rf "${intermediate_dir}"
    return 0
}
trap cleanup EXIT

# create a temp directory we'll put intermidate and utility files in
trace "creating intermediate directory on: ${intermediate_dir}"
mkdir -p "${intermediate_dir}" "${dumps_dir}" "${logs_dir}"

# generate the dump specs file - a file where each row holds:
#   * company name (as specified in the databases section)
#   * dump type (as specified in the dumps section)
#
# the dumps will be performed for all specified companies.
#
# a per-company dump configuration is possible by simply adding another section that will describe
# a company/dump pair and that data simply needs to be added to the bottom of the spec file.
# currently there's no need for such a feature.
trace "generating dumps spec file"
companies_file="${intermediate_dir}/companies-list"
"${ini_query}" -k companies "${configuration_file}" "${dumps_section}" "dump-specs" >"${companies_file}"

dumps_file="${intermediate_dir}/dumps-list"
"${ini_query}" -k dumps "${configuration_file}" "${dumps_section}" "dump-specs" >"${dumps_file}"

specs_file="${intermediate_dir}/dump-specs"
while read company
do
    while read dump
    do
        echo -e "${company}\t${dump}"
    done < "${dumps_file}"
done < "${companies_file}" > "${specs_file}"

# if the continuation strategy is "never-accept" then just remove any pre-existing summary file
if [ "never-accept" == "${continuation_strategy}" ]
then
    rm -f "${dump_summary_file}"
fi

# initialize the summary file - keep any pre-existing summary.
"${summary_script}" -v ${ignore_signatures} init -q -f company value -f dump value -f args-sig signature -f dump-file path "${dump_summary_file}"
# clear any non-existing files pointed to by the summary file or files dated after the timestamp in the summary file
"${summary_script}" -v ${ignore_signatures} validate-file-dates -d "${dump_summary_file}"
# clear lines with any dumps not specified in the specs file (remove unwanted files from the file system as well!)
"${summary_script}" -v ${ignore_signatures} filter-by-expectation -d -c "${specs_file}" -f company -f dump "${dump_summary_file}"

# now perform the dump using the sql dump-script.
queries_dir="${logs_dir}/sql-queries/"
error_logs="${logs_dir}/errors/"
successful_logs_dir="${intermediate_dir}/success-logs/"
mkdir -p "${queries_dir}" "${error_logs}" "${successful_logs_dir}"

cat "${specs_file}" | while IFS="$(echo -en '\t')" read company dump
do
    # the output dump will be placed in the dumps directory, prefixed with "dump-" and will have a "<timestamp>.csv.gz" extension
    # as the sql dump script will generate a compressed output.
    timestamp=$(date +"%Y-%m-%d")
    dump_file="${dumps_dir}/dump-${company}-${dump}-${timestamp}.csv.gz"
    query_file="${queries_dir}/query-${company}-${dump}-${timestamp}.sql"
    error_record="${error_logs}/log-${company}-${dump}-${timestamp}.log"

    # prepare the arguments signature
    args_sig=$(
        (
            "${ini_query}" -qtK "${configuration_file}" "${dumps_section}" "global-dump-details"
            "${ini_query}" -r "detail-sets" -b "dumps" "dump-detail-sets" -qtK "${configuration_file}" "${dumps_section}" "dump-details" "${dump}"
        ) | sort | md5sum | cut -d' ' -f1
    )

    # check if the dump already exists in the dump summary file
    if "${summary_script}" -v ${ignore_signatures} query -f company "${company}" -f dump "${dump}" -f args-sig "${args_sig}" "${dump_summary_file}"
    then
        trace "the '${dump}' dump for company '${company}' already exists - skipping."
        continue
    fi

    # remove the relevant line from the summary file, if it exists (as it may be invalid)
    "${summary_script}" -v ${ignore_signatures} remove -f company "${company}" -f dump "${dump}" "${dump_summary_file}"

    trace "dumping ${dump} for ${company}"

    # this one iteration loop allows us to stop on error and continue without failing the entire script
    succeeded=""
    for _ in 1
    do
        # generate the sql query
        ${sql_query_script} --db-config "${configuration_file}" "${companies_section}" \
                            --config-file "${configuration_file}" "${dumps_section}" "global-dump-details" \
                            --config-file "${configuration_file}" "${dumps_section}" "dump-details" "${dump}" \
                            --reference-key "detail-sets" \
                            --reference-base-section "${dumps_section}" "dump-detail-sets" \
                            -v -d "${company}" >"${query_file}" || break

        # run the query
        ${sql_dumper} -o "${dump_file}" \
                      -d "${configuration_file}" -s "${companies_section}" \
                      -c "${company}" \
                      ${traffic_compression_switch} ${x_flag} \
                      "${query_file}" || break

        succeeded=1
    done 2> >(tee "${error_record}")

    if [ -n "${succeeded}" ]
    then
        # in case of successful termination move the error log to the intermediate directory for cleanup on script termination
        trace "successfuly dumped ${dump} for ${company}"
        mv "${error_record}" "${query_file}" "${successful_logs_dir}"

        # record this dump in the dumps summary
        "${summary_script}" -v add-line -f company "${company}" -f dump "${dump}" -f args-sig "${args_sig}" -f dump-file "${dump_file}" "${dump_summary_file}"
    else
        # remove the dump if it exists in case of failure to make sure we don't process corrupt data
        trace "failed to dump ${dump} for ${company}"
        rm -f "${dump_file}"
    fi
done

trace "done"

