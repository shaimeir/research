#!/usr/bin/env bash

set -eu

# this script takes a dump (as generated by generate-data-dumps.sh) and runs all the recipes specified for that given dump in the central configuration file.
# the configuration holds the following information:
#
# ---------------
#
#       # the split policy - tells us how to split the dump
#       [split]
#
#       ## this sub-section holds for all dumps
#       [[general]]
#       ### a list of columns that contain a windows path from which the file name should be extracted.
#       ### this will usually be the "process" column as the process name will be extracted from it when run using
#       ### this key.
#       process=process
#
#       ### this key is a list of additional columns to split by that will not have their content parsed as a Windows file
#       ### path and their file name extracted.
#       extra=instance, host
#
#       ## you may also specify a per-dump-type (the dumps are specified in the [dumps] section as documented in generate-data-dumps.sh.
#       ## these keys will be merged with the keys in general-policy and so both will generate the split criteria.
#       ## the section must be named as "dump-type-name". e.g. for a dump called "file-system-dump" the matching split section
#       ## will be "file-system-dump-split"
#       [[dump-name-split]]
#       extra=access
#
#       # describes the recipes to execute and their respective arguments
#       [recipes]
#
#       ## this part defines general configuration to be applied with all the recipes.
#       ## as opposed to [split][[general]] certain keys within this section are *replaced* by the specific recipe descriptors
#       ## instead of being *added* to the keys specified in the recipe descriptor.
#       [[general]]
#       ### this field is mandatory as it defines the recipe (and therefor usualy will not be found in the general section).
#       ### use a full file path or a name of a recipe in the default recipe directory (network-anomalies/recipes).
#       ### this key will be *overridden* by a key of the same name specified in a recipe-specific section.
#       recipe=/recipes/network.json
#
#       ### the sub recipe to execute in the specific recipe. (optional)
#       ### this key will be *overridden* by a key of the same name specified in a recipe-specific section.
#       subrecipe=main
#
#       ### the normalization sub-recipe (as defined by generate-standard-profiles-v2.sh). (optional)
#       ### this key will be *overridden* by a key of the same name specified in a recipe-specific section.
#       normalization=main.normalize
#
#       ## this sub-section will define general arguments to be passed *by alias* to all the recipes
#       ## executed for a certain dump. (support of non-aliased arguments will be added as needed).
#       [[[arguments-by-alias]]]
#       ### a useful example is the "min-occurrences" alias which is found in almost all recipes.
#       min-occurrences=30
#
#
#       ## you can also have a per-recipe section
#       [[file-sytem-profile]]
#       ### set the correct profile recipe file
#       recipe=/recipes/file-system.json
#
#       ### recipe-specific arguments
#       [[[arguments-by-alias]]]
#       ### a useful example is the english language model used with file system profile construction.
#       language-model=/recipes/arguments/language-model.pickle
#
#       ### yet another profile recipe....
#       [[network-profile]]
#       ...
#
#       # this section will tie everything up. we'll use it to connect the dump with the profile.
#       [profile-specs]
#
#       ## this sub-section defines the recipes to run on all dumps.
#       [[general]]
#       ### here we've used for example two domains that share the same dump type (scheme) - network and domain (they will be resolved
#       ### using the default recipes directory. the third recipe (file-system) doesn't share the same scheme but the profile generation script
#       ### (i.e. generate-standard-profiles-v2.sh) checks if a recipe is suitable to be executed on a dump before setting off to work, so no harm done.
#       recipes=network-profile, domain-profile, file-system-profile
#
#       ## this section will define the dump-specific recipes.
#       [[dump-specs]]
#
#       ### the sub-sections must have the same name as the dump they're meant for (as specified in the dumps section).
#       [[[file-system-dump]]]
#       recipes=file-sytem-profile, network-profile
#
# ---------------
#

# define common paths
automation_dir="$(dirname $(readlink -f ${BASH_SOURCE[0]}))"
db_utils_dir="${automation_dir}/db-utils"
csv_processing_dir="${automation_dir}/network-anomalies"

# define important script paths
dump_splitter="${db_utils_dir}/split-dump-by-process.awk"
ini_query="${automation_dir}/ini-query.py"
profile_generation_script="${csv_processing_dir}/generate-standard-profiles-v2.sh"
infer_scheme="${automation_dir}/infer-scheme.py"

function print_help()
{
    echo "generate profiles based on a central configuration file from a single (possibly compressed) dump file."
    echo ""
    echo "usage: ${BASH_SOURCE[0]} [-x] [-k] [-K] [-S] [-r RECIPES-SECTION] [-u DUMPS-SECTION] [-s SPLIT-SECTION] [-f PROFILE-SPECS-SECTION] [-p NUM-CPUS] [-i RECIPE-TO-SKIP [...]] [-P PROC-CHAIN-PROCESSES] -o OUTPUT-PATH -c COMPANY-NAME -d DUMP-NAME -g CONFIGURATION-FILE DUMP-FILE"
    echo " * -x - set bash's -x flag."
    echo " * -k - keep intermediate dir."
    echo " * -K - just like -k but potentially large files are also kept (profile generation files). (implies -k)"
    echo " * -S - the DUMP-FILE argument is actually a pre-split directory."
    echo " * -r RECIPES-SECTION - the recipes sector in the configuration file. (default: recipes)"
    echo " * -s SPLIT-SECTION - the split section in the configuration file. (default: split)"
    echo " * -u DUMPS-SECTION - the dumps section in the configuration file. (default: dumps)"
    echo " * -f PROFILE-SPECS-SECTION - the profile specs section in the configuration file. (default: profile-specs)"
    echo " * -p NUM-CPUS - limit the number of CPUs to use when creating the profiles."
    echo " * -c COMPANY-NAME - the name of the company the dump was created for. (mandatory)"
    echo " * -d DUMP-NAME - the name of the type of the dump (dump detail name). (mandatory)"
    echo " * -i RECIPE-TO-SKIP - the name of a recipe to skip. (optional, repeating)"
    echo " * -P PROC-CHAIN-PROCESSES - set the file with the list of processes to generate process chains profiles for. (default: switch not passed to the next script)"
    echo " * -o OUTPUT-PATH - the directory to write the dumps to. (mandatory)"
    echo " * -g CONFIGURATION-FILE - the ini configuration file with the recipes and dumps configuration."
    echo " * DUMP-FILE - the dump file to generate the profile from."
}

function perror()
{
    echo "error: ${@}" >&2
    exit 1
}

function perror_help()
{
    print_help >&2
    perror "${@}"
}

function trace()
{
    echo "$(date) ${@}" >&2
}

# a utility function that will help resolve a recipe name to a recipe file name.
# e.g.: file-system ==> network-anomalies/recipes/file-system.json
default_recipe_resolution_dir="${automation_dir}/default-recipes"
function resolve_recipe_file_name()
{
    recipe_file_name="${1}"
    if [ -f "${recipe_file_name}" ]
    then
        echo "${recipe_file_name}"

    elif [ -f "${default_recipe_resolution_dir}/${recipe_file_name}" ]
    then
        echo "${default_recipe_resolution_dir}/${recipe_file_name}"

    elif [ -f "${default_recipe_resolution_dir}/${recipe_file_name}.json" ]
    then
        echo "${default_recipe_resolution_dir}/${recipe_file_name}.json"

    else
        return 1
    fi
}

recipes_section="recipes"
dumps_section="dumps"
split_section="split"
profile_specs_section="profile-specs"

x_flag=""

recipes_to_skip=""

while getopts ":hxkSKr:s:f:c:d:g:i:P:o:p:u:" option
do
    case "${option}" in
        "h") print_help >&2
             exit 0
             ;;

        "x") set -x
             x_flag="-x"
             ;;

        "k") keep_intermediate=1
             ;;

        "K") keep_large_files=1
             ;;

        "S") pre_split_dir="1"
             ;;

        "c") company="${OPTARG}"
             ;;

        "r") recipes_section="${OPTARG}"
             ;;

        "s") split_section="${OPTARG}"
             ;;

        "f") profile_specs_section="${OPTARG}"
             ;;

        "P") proc_chain_processes="-P ${OPTARG}"
             ;;

        "o") output_path="${OPTARG}"
             ;;

        "d") dump_name="${OPTARG}"
             ;;

        "g") configuration_file="${OPTARG}"
             ;;

        "i") recipes_to_skip="${recipes_to_skip} ${OPTARG}"
             ;;

        "p") num_cpus="-p ${OPTARG}" # the corresponding switch in generate-standard-profiles-v2.sh is -p as well
             ;;

        "u") dumps_section="${OPTARG}"
             ;;

        "*") print_help >&2
             perror "invalid switch encountered -${option}."
    esac
done
shift $((${OPTIND}-1))

[ "${#}" -ne "1" ] && perror_help "invalid number of dump files."
[ -z "${output_path:-}" ] && perror_help "missing output directory."
[ -z "${company:-}" ] && perror_help "missing company name."
[ -z "${dump_name:-}" ] && perror_help "missing dump name."
[ -z "${configuration_file:-}" ] && perror_help "missing configuration file."

dump_file="${1}"

# set up a cleanup trap
function cleanup()
{
    [ -z "${keep_intermediate:-}" -a -z "${keep_large_files:-}" ] && [ -n "${intermediate_dir:-}" ] && rm -rf "${intermediate_dir}"
    [ -z "${keep_large_files:-}" ] && [ -n "${large_intermediate_dir}" ] && rm -rf "${large_intermediate_dir}"
    return 0
}
trap cleanup EXIT

# create directory structure
intermediate_dir="${output_path}/intermediate-files"
large_intermediate_dir="${output_path}/intermediate-large-files"
dump_split_dir="${large_intermediate_dir}/dump-split/"
recipes_record="${intermediate_dir}/recipes-list"
unrefined_recipes_record="${intermediate_dir}/unrefined-recipes-list"

mkdir -p "${intermediate_dir}" "${large_intermediate_dir}" "${output_path}" "${dump_split_dir}"

trace "created ${output_path}"

# infer the scheme for the dump-file
trace "inferring scheme for ${dump_file}"

dump_sample="${intermediate_dir}/dump-sample"
if file "${dump_file}" | grep -iq gzip
then
    gzip -cd "${dump_file}"
else
    dd if="${dump_file}"

# note that $process_columns and $extra_columns are expanded without quotation!
fi | head -10 >"${dump_sample}"

dump_scheme=$(
    "${infer_scheme}" -t dump "${dump_sample}" | cut -d' ' -f2
)

[ -n "${dump_scheme}" ] || perror "couldn't infer scheme for ${dump_file}"
[ "${dump_scheme}" == "process" ] && trace "the dump is a process dump and requires special attention."

trace "listing recipes for ${dump_file}"

# write down the list of recipes to skip
recipes_to_skip_file="${intermediate_dir}/recipes-to-skip"
rm -f "${recipes_to_skip_file}"
for recipe_to_skip in ${recipes_to_skip}
do
    trace "adding ${recipe_to_skip} to skip list in ${recipes_to_skip_file}"
    echo "${recipe_to_skip}"
done > "${recipes_to_skip_file}"

# list the unbuilt recipes. since grep will fail if all recipes are already built use '|| true', otherwise the script will halt
(
    (
    "${ini_query}" -q -k recipes "${configuration_file}" "${profile_specs_section}" general
    "${ini_query}" -q -k recipes "${configuration_file}" "${profile_specs_section}" dump-specs "${dump_name}"
    ) | grep -vFf "${recipes_to_skip_file}" || true
) > "${unrefined_recipes_record}"

trace "refining recipes to only those that operate on a '${dump_scheme}' scheme dump."
while read recipe_name
do
    trace "listing recipe files for recipe: ${recipe_name}"

    recipe_files_file="${intermediate_dir}/recipe-files.${recipe_name}"
    (
        "${ini_query}" -k recipe "${configuration_file}" "${recipes_section}" "${recipe_name}" || \
        "${ini_query}" -q -k recipe "${configuration_file}" "${recipes_section}" general
    ) | \
    while read recipe_file
    do
        resolved_recipe_file=$(resolve_recipe_file_name "${recipe_file}" || true)

        if [ -z "${resolved_recipe_file}" ]
        then
            trace "can't resolve recipe file name ${recipe_file}"
        else
            [ "${resolved_recipe_file}" != "${recipe_file}" ] && trace "resolved ${recipe_file} to ${resolved_recipe_file}"
            echo "${resolved_recipe_file}"
        fi
    done > "${recipe_files_file}"

    if [ "$(wc -l ${recipe_files_file} | cut -d' ' -f1)" -eq 0 ]
    then
        trace "${recipe_name} has no recipe files specified!"
        continue
    fi

    subrecipe_switch=$(
        (
            ${ini_query} -k subrecipe "${configuration_file}" "${recipes_section}" "${recipe_name}" || \
            ${ini_query} -q -k subrecipe "${configuration_file}" "${recipes_section}" general
        ) | xargs -r -n1 echo "" -s # results in "-s <subrecipe>"
    )

    relevant_recipe_found="0"
    while read recipe_file
    do
        recipe_scheme=$(
            ( "${infer_scheme}" -t recipe ${subrecipe_switch} "${recipe_file}" | cut -d' ' -f2 ) || \
            true
        )

        # some weird error occurred (probably a malformed recipe file)
        if [ -z "${recipe_scheme}" ]
        then
            trace "couldn't extract the scheme for ${recipe_file}"

        # this is a special case - if we're given a process schemed dump then the profile generation process
        # tailors a profile specifically for it by  transforming it into a proc-chain-csv dump and so we should accept the recipe in this case.
        elif [ "${dump_scheme}" == "process" -a "${recipe_scheme}" == "proc-chain-csv" ]
        then
            trace "process dump scheme matched with a proc-chain-csv recipe (${recipe_name})!"
            echo "${recipe_name}"

            relevant_recipe_found="1"

        # this case is a general mismatch
        elif [ "${recipe_scheme}"  != "${dump_scheme}" ]
        then
            trace "skipping recipe '${recipe_file}' which works on scheme '${recipe_scheme}' instead of '${dump_scheme}'"

        # and finally - we have a match!
        else
            trace "'${recipe_name}' has '${recipe_file}' which works on '${recipe_scheme}' - add it to relevant recipes record"
            echo "${recipe_name}"

            relevant_recipe_found="1"
            break
        fi
    done < "${recipe_files_file}"

    [ "${relevant_recipe_found}" == "0" ] && trace "'${recipe_name}' skipped"
done < "${unrefined_recipes_record}" > "${recipes_record}"

if [ "$(wc -l ${recipes_record} | cut -d' ' -f1)" -eq 0 ]
then
    trace "no recipes to build! (post skip filtering)"
    exit 0
fi

trace "building recipes:"
awk '{print "\t--- " $0}' "${recipes_record}" >&2

# for process dumps simply decompress the dump and let generate-standard-profiles-v2.sh deal with it
# split the dump into per-process name files
if [ "${dump_scheme}" == "process" ]
then
    decompressed_process_dump="${dump_split_dir}/$(basename ${dump_file} | sed -r 's/\.gz$//g')"
    trace "process dump scheme - just decompressing into ${decompressed_process_dump}"
    gzip -cd "${dump_file}" >"${decompressed_process_dump}"

elif [ -z "${pre_split_dir:-}" ] # if -S wasn't specified then the dump file is a CSV with mixed lines that should be split to ease the processing load
then
    # construct the split criteria
    process_columns=$( (
        "${ini_query}" -q -k process "${configuration_file}" "${split_section}" general
        "${ini_query}" -q -k process "${configuration_file}" "${split_section}" "${dump_name}-split"
    ) | grep -Pv '^\s*$' | xargs -r -n1 echo "--column-name")

    extra_columns=$( (
        "${ini_query}" -q -k extra "${configuration_file}" "${split_section}" general
        "${ini_query}" -q -k extra "${configuration_file}" "${split_section}" "${dump_name}-split"
    ) | grep -Pv '^\s*$' | xargs -r -n1 echo "--extra-name")

    trace "splitting ${dump_file} with the following criteria:"
    trace "    ${process_columns}"
    trace "    ${extra_columns}"

    if file "${dump_file}" | grep -iq gzip
    then
        gzip -cd "${dump_file}"
    else
        dd if="${dump_file}"

    # note that $process_columns and $extra_columns are expanded without quotation!
    fi | "${dump_splitter}" ${process_columns} ${extra_columns} --output "${dump_split_dir}"
else
    trace "pre-split specified in ${dump_file} - linking content with ${dump_split_dir}"

    # make sure it's a directory
    readlink -e "${dump_file}" || perror "can't resolve ${dump_file}"

    # to skip any pitfalls that may occur by the dump directory being a soft link itself (some commands that don't recieve
    # a / after the softlink's name will not work the same as with the /, e.g. 'ls soft-link' vs 'ls soft-link/')
    (
        find "${dump_file}" -maxdepth 1 -type l
        find "${dump_file}" -maxdepth 1 -type f
    ) | # list the files (may be in relative path)
        while read dump_file_entry # filter out directories (including through link resolval), broken links
        do
            resolved_dump_file_entry="$(readlink -e ${dump_file_entry})" || continue
            [ -f "${resolved_dump_file_entry}" ] && echo "${dump_file_entry}"
        done |
        xargs -n1 readlink -f | # resolve relative paths
        xargs -n1 -I\{\} ln -s \{\} "${dump_split_dir}" # link!
fi

trace "split for ${dump_file} generated in ${dump_split_dir}"

trace "building the profile generation command line"

# constructing all the required switches here is quite tedious and this specific part may be easier and more quickly implemented
# in python but the rest of the script leverages bash for clearer and shorter code so we'll stay consistent.

trace "building a profile for ${dump_file}"

# build the command line arguments to pass to generate-standard-profiles-v2.sh

# this variable will aggregate all the switches to pass to generate-standard-profiles-v2.sh.
# needless to say this will impair us with space sensitivity so... don't use spaces...
complete_cmdline=""

while read recipe_name
do
    # fetch the profile recipe file
    recipes=$(
        # try and fetch the recipe file specified in the recipe section. if it's not specified, try the general sub-section.
        # if it's not specified leave it to the defaults.
        "${ini_query}" -k recipe "${configuration_file}" "${recipes_section}" "${recipe_name}" || \
        "${ini_query}" -qk recipe "${configuration_file}" "${recipes_section}" general
    )

    if [ -z "${recipes}" ]
    then
        trace "recipe '${recipe_name}' has no recipe file set!"
        continue
    fi

    # fetch the profile sub-recipe
    sub_recipe=$(
        (
            # try and fetch the sub recipe specified in the recipe section. if it's not specified, try the general sub-section.
            # if it's not specified leave it to the defaults.
            "${ini_query}" -k subrecipe "${configuration_file}" "${recipes_section}" "${recipe_name}" || \
            "${ini_query}" -k subrecipe "${configuration_file}" "${recipes_section}" general
        ) | xargs -r -n 1 echo -s) # this will result in sub_recipe holding "-s sub_recipe_name")

    # fetch the normalization sub-recipe
    normalization_sub_recipe=$(
        (
            # try and fetch the normalization sub recipe specified in the recipe section. if it's not specified, try the general sub-section.
            # if it's not specified leave it to the defaults.
            "${ini_query}" -k normalization "${configuration_file}" "${recipes_section}" "${recipe_name}" || \
            "${ini_query}" -k normalization "${configuration_file}" "${recipes_section}" general

        # NOTE: "-n" is an echo switch so we have to give echo an empty string before "-n" so it won't use it as switch
        #       and actually output it to the stdout (which wil end up as part of normalization_sub_recipe).
        ) | xargs -r -n 1 echo "" -n) # this will result in normalization_sub_recipe holding "-n normalization_sub_recipe_name")

    # note that at this point sub_recipe and normalization_sub_recipe are defined but may be empty if the queried keys don't exist

    # fetch arguments
    arguments=$(
        # note that the -K switch means --all-key as opposed to -k which specifies a specific key.
        # the tr command normalizes the ini query output (which is lines filled with tab separated tuples)
        # to something xargs can digest (there's no standard switch to set the in-line separator).
        "${ini_query}" -qKt "${configuration_file}" "${recipes_section}" "${recipe_name}" arguments-by-alias | \
        tr '\t' '\0' | tr '\n' '\0' | \
        xargs -r -0 -n 2 echo -l
    )

    for recipe in ${recipes}
    do
        resolved_recipe=$(resolve_recipe_file_name "${recipe}" || true)

        if [ -z "${resolved_recipe}" ]
        then
            trace "couldn't resolve ${recipe} to a file name - not adding it to the generated command line"
        else
            complete_cmdline="${complete_cmdline} -r ${resolved_recipe} ${sub_recipe} ${normalization_sub_recipe} ${arguments}"
        fi
    done

done < "${recipes_record}"

# add the global aliases for good measure
global_arguments=$(
    # note the comment above the arguments variable in the above loop
    "${ini_query}" -qKt "${configuration_file}" "${recipes_section}" general arguments-by-alias | \
    tr '\t' '\0' | tr '\n' '\0' | \
    xargs -r -0 -n 2 echo -L
)

complete_cmdline="${complete_cmdline} ${global_arguments}"

# add the switches implied by the command line
if [ -n "${keep_large_files:-}" ]
then
    keep_flag="-K"
elif [ -n "${keep_intermediate:-}" ]
then
    keep_flag="-k"
fi

complete_cmdline="${x_flag} ${num_cpus:-} ${keep_flag:-} ${proc_chain_processes:-} ${complete_cmdline}"

# add the output path
complete_cmdline="${complete_cmdline} -o ${output_path}"

# drop any existing new line
complete_cmdline="$(echo ${complete_cmdline} | tr '\n' ' ')"

trace "built profile generation command line: ${complete_cmdline}"

trace "building profiles from the dumps"

"${profile_generation_script}" ${complete_cmdline} -d "${dump_split_dir}"

trace "done"

