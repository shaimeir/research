#!/usr/bin/env bash

set -eu

# this script utilizes generate-data-dumps.sh and generate-profiles-for-dumps.sh to fully automate the dump, processing and cleanup of
# profiles configured in a central configuration.
#
# all the switches supported by this script can be accessed via the centralized configuration file.
# the default centralized configuration file is: /etc/secdo/research/profile-machine-config
# if a switch is specified then it overrides the correlating value in the configuration.
# mandatory switches that aren't specified in the command line are checked for in the configuration file and only if they're
# missing there as well then the script will fail with an error.
#
# boolean switches don't have their values checked in the configuration.
# if a boolean switch (such as -x, -k, etc...) are set if their corresponding configuration identity is present.
# i.e. to set the -x flag just place the key
#   bash-x=1
# in the configuration without regard to the value (it can simply be empty or anything else, not necessarily 1).
#
#
# the configuration structure required for this script to run is:
#
#   -------------
#
#   # the name of this section can be chosen using the -m switch
#   [general]
#
#   # the directory of a python virtual environment to use. (cmdline -e)
#   # if the value is 'default' then the environment will load from the repository root/.virtualenv directory.
#   virtualenv=default
#
#   # to turn on bash's -x flag (cmdline: -x)
#   bash-x=1
#
#   # keep intermediate files (cmdline: -k)
#   keep-temp=1
#
#   # keep large intermediate files (cmdline: -K)
#   keep-large-temp=1
#
#   # turn compression on when dumping (cmdline: -C)
#   compress-traffic=1
#
#   # set the companies section for this configuration file (cmdline: -c)
#   companies=companies_section_name
#
#   # set the dumps section for this configuration file (cmdline: -d)
#   dumps=dumps_section_name
#
#   # set the recipes section for this configuration file (cmdline: -r)
#   recipes=recipes_section_name
#
#   # set the split section for this configuration file (cmdline: -s)
#   split=split_section_name
#
#   # set the profile-specs section for this configuration file (cmdline: -f)
#   profile-specs=profile_specs_section_name
#
#   # set the max number of concurrent processes when building a profile. (cmdline: -p)
#   # drop this value to use all available cpus.
#   cpu-count=14
#
#   # set the continuation strategy for generate-data-dumps.sh (cmdline: -t)
#   dump-continuation-strategy=always-accept
#
#   # set the continuation strategy for generate-profiles-for-dumps.sh (cmdline: -T)
#   profile-continuation-strategy=never-accept
#
#   # set the output directory path using the date utility's format. (cmdline: -o)
#   output=/secdo/large-storage-mount/profiles-machine/profile-%d-%m-%Y
#
#   # set the file with the list of processes to create process chains profile for. (cmdline: -P)
#   proc-chain-processes=/secdo/proc-chain-processes
#

default_config_file="/etc/secdo/research/profile-machine-config"

automation_dir="$(dirname $(readlink -f ${BASH_SOURCE[0]}))"
generate_data_dumps="${automation_dir}/generate-data-dumps.sh"
generate_profiles_for_dumps="${automation_dir}/generate-profiles-for-dumps.sh"
ini_query="${automation_dir}/ini-query.py"
default_virtualenv_dir="$(readlink -f ${automation_dir}/../.virtualenv)"

pyenv_validation_script="${automation_dir}/test-python-requirements.py"
python_requirements_file="${automation_dir}/requirements.txt"

function print_help()
{
    echo "generate dumps and profiles as specified by a centralized configuration file."
    echo "any of the switches (including the mandatory ones such as -o) will be taken from the configuration file if not specified."
    echo ""
    echo "usage: ${BASH_SOURCE[0]} [-x] [-k] [-K] [-C] [-e VIRTUAL-ENV-DIR] [-m MAIN-SECTION] [-c COMPANIES-SECTION] [-d DUMPS-SECTION] [-r RECIPES-SECTION] [-s SPLIT-SECTION] [-f PROFILE-SPECS-SECTION] [-p NUM-CPUS] [-t DUMP-CONTINUATION-STRATEGY] [-T PROFILE-CONTINUATION-STRATEGY] [-P PROC-CHAIN-PROCESSES] -o OUTPUT-DIRECTORY [CENTRAL-CONFIGURATION-FILE]"
    echo " * -x - set bash's -x flag."
    echo " * -k - keep intermediate files."
    echo " * -K - keep large intermediate files (potentially very large files will be kept)."
    echo " * -C - turn on traffic compression when dumping."
    echo " * -e VIRTUAL-ENV-DIR - use the virtual environment in the given directory. use the value 'default' to load the virtual env from ${default_virtualenv_dir}."
    echo " * -m MAIN-SECTION - the name of the main section in the configuration file. (default: general)"
    echo " * -c COMPANIES-SECTION - the name of the companies section in the configuration file. (default: companies)"
    echo " * -d DUMPS-SECTION - the dumps sections in the configuration file. (default: dumps)"
    echo " * -r RECIPES-SECTION - the recipes sector in the configuration file. (default: recipes)"
    echo " * -s SPLIT-SECTION - the split section in the configuration file. (default: split)"
    echo " * -f PROFILE-SPECS-SECTION - the profile specs section in the configuration file. (default: profile-specs)"
    echo " * -p NUM-CPUS - limit the number of CPUs to use when creating the profiles."
    echo " * -t DUMP-CONTINUATION-STRATEGY - the continuation strategy to pass to generate-data-dumps.sh. (default: don't pass the switch)"
    echo " * -T PROFILE-CONTINUATION-STRATEGY - the continuation strategy to pass to generate-profiles-for-dumps.sh. (default: don't pass the switch)"
    echo " * -P PROC-CHAIN-PROCESSES - set the file with the list of processes to generate process chains profiles for. (default: switch not passed to the next script)"
    echo " * -o OUTPUT-DIRECTORY - a date-style format string of the output directory. (see "man date" for details)"
    echo " * CENTRAL-CONFIGURATION-FILE - the configuration file detailing the dumps and profiles required. (default ${default_config_file})"
}

function perror()
{
    echo "error: ${@}" >&2
    exit 1
}

function perror_help()
{
    print_help >&2
    perror "${@}"
}

function trace()
{
    echo "$(date) ${@}" >&2
}

configuration_file="${default_config_file}"
main_section="general"

companies_section="companies"
dumps_section="dumps"
dump_summary_file_name="dumps-summary"
recipes_section="recipes"
split_section="split"
profile_specs_section="profile-specs"

while getopts ":hxKkCe:m:c:d:r:s:f:p:t:T:P:o:" option
do
    case "${option}" in
        "h") print_help >&2
             exit 0
             ;;

        "x") set -x
             x_flag="-x"
             ;;

        "k") keep_intermediate="-k"
             ;;

        "K") keep_large_intermediate="-K"
             ;;

        "C") compress_traffic="-C"
             ;;

        "e") virtualenv_dir="${OPTARG}"
             ;;

        "m") main_section="${OPTARG}"
             ;;

        "c") companies_section="${OPTARG}"
             companies_section_specified="1"
             ;;

        "d") dumps_section="${OPTARG}"
             dumps_section_specified="1"
             ;;

        "r") recipes_section="${OPTARG}"
             recipes_section_specified="1"
             ;;

        "s") split_section="${OPTARG}"
             split_section_specified="1"
             ;;

        "f") profile_specs_section="${OPTARG}"
             profile_specs_section_specified="1"
             ;;

        "p") num_cpus="${OPTARG}" # the corresponding switch in generate-standard-profiles-v2.sh is -p as well
             ;;

        "t") dump_continuation_strategy="${OPTARG}"
             ;;

        "T") profile_continuation_strategy="${OPTARG}"
             ;;

        "P") proc_chain_processes="-P ${OPTARG}"
             ;;

        "o") output_path="${OPTARG}"
             ;;

        "*") print_help >&2
             perror "invalid switch encountered."
    esac
done
shift $((${OPTIND}-1))

[ "${#}" -le 1 ] || perror_help "too many configuration files specified!"

if [ "${#}" -eq 1 ]
then
    configuration_file="${1}"
fi

[ -f "${configuration_file}" ] || perror_help "invalid configuration file ${configuration_file} - not a file!"
trace "using configuration file: ${configuration_file}"

# before we can do anything (as ini-query.py) requires us to load a virtual environment first, if one is specified) we need to prepare python
if [ -z "${virtualenv_dir:-}" ]
then
    virtualenv_dir=$(
        awk -F'=' '
            $0 ~ /^\s*virtualenv\s*=/ {
                virtualenv=$2
                gsub(/(^\s*)|(\s*)$/, "", virtualenv)
                print virtualenv
                exit 0
            }
        ' "${configuration_file}"
    )
fi

if [ -n "${virtualenv_dir:-}" ]
then
    [ "${virtualenv_dir}" == "default" ] && virtualenv_dir="${default_virtualenv_dir}" && trace "using default virtual environment directory"
    virtualenv_activate="${virtualenv_dir}/bin/activate"

    trace "loading virtual environment from ${virtualenv_activate}"

    if [ -d "${virtualenv_dir}" ]
    then
        set +eu # we cant control how well the activation script is written
        source "${virtualenv_activate}"
        set -eu

        virtual_env_loaded=1
        trace "loaded virtual env successfully! (${virtualenv_dir})"
    else
        trace "virtual env not found (${virtualenv_dir})"
    fi
fi

if ! "${pyenv_validation_script}" -s "3.6" -p "${python_requirements_file}"
then
    trace "python is not setup properly!"
    if [ -n "${virtual_env_loaded:-}" ]
    then
        trace "(loaded virtual env at ${virtualenv_dir:-})"
    elif [ -n "${virtualenv_dir}" ]
    then
        trace "(couldn't load virtual env)"
    else
        trace "(no virtual env used)"
    fi

    exit 1
fi

# load boolean switches from the configuration
[ -z "${x_flag:-}" ] && "${ini_query}" -k "bash-x" "${configuration_file}" "${main_section}" &>/dev/null && x_flag="-x"
[ -z "${keep_intermediate:-}" ] && "${ini_query}" -k "keep-temp" "${configuration_file}" "${main_section}" &>/dev/null && keep_intermediate="-k"
[ -z "${keep_large_intermediate:-}" ] && "${ini_query}" -k "keep-large-temp" "${configuration_file}" "${main_section}" &>/dev/null && keep_large_intermediate="-K"
[ -z "${compress_traffic:-}" ] && "${ini_query}" -k "compress-traffic" "${configuration_file}" "${main_section}" &>/dev/null && compress_traffic="-C"

[ -n "${x_flag:-}" ] && set -x

# load value switches from the configuration
function load_value_from_config()
{
    key="${1}"
    default="${2}"

    "${ini_query}" -k "${key}" "${configuration_file}" "${main_section}" || echo "${default}"
}

[ -z "${companies_section_specified:-}" ] && companies_section=$(load_value_from_config "companies" "${companies_section}")
[ -z "${dumps_section_specified:-}" ] && dumps_section=$(load_value_from_config "dumps" "${dumps_section}")
[ -z "${recipes_section_specified:-}" ] && recipes_section=$(load_value_from_config "recipes" "${recipes_section}")
[ -z "${split_section_specified:-}" ] && split_section=$(load_value_from_config "split" "${split_section}")
[ -z "${profile_specs_section_specified:-}" ] && profile_specs_section=$(load_value_from_config "profile-specs" "${profile_specs_section}")

[ -z "${dump_continuation_strategy:-}" ] && dump_continuation_strategy=$("${ini_query}" -q -k "dump-continuation-strategy" "${configuration_file}" "${main_section}")
[ -z "${profile_continuation_strategy:-}" ] && profile_continuation_strategy=$("${ini_query}" -q -k "profile-continuation-strategy" "${configuration_file}" "${main_section}")
[ -z "${num_cpus:-}" ] && num_cpus=$("${ini_query}" -q -k "cpu-count" "${configuration_file}" "${main_section}")
[ -z "${output_path:-}" ] && output_path=$("${ini_query}" -q -k "output" "${configuration_file}" "${main_section}")
[ -z "${proc_chain_processes:-}" ] && proc_chain_processes=$("${ini_query}" -q -k "proc-chain-processes" "${configuration_file}" "${main_section}")

# validate arguments
[ -z "${num_cpus:-}" -o "${num_cpus:-1}" -ge 1 ] || perror_help "invalid number of cpus: ${num_cpus}"
[ -n "${output_path:-}" ] || perror_help "output path not specified"

output_path=$(date +"${output_path}")
dumps_output_path="${output_path}/dumps"
profiles_output_path="${output_path}/profiles"

trace "running dump phase"
[ -n "${companies_section}" ] && companies_section_dump_switch="-s ${companies_section}"
[ -n "${dumps_section}" ] && dumps_section_dump_switch="-d ${dumps_section}"
[ -n "${dump_continuation_strategy}" ] && dump_continuation_strategy="-t ${dump_continuation_strategy}"

"${generate_data_dumps}" ${x_flag:-} ${keep_intermediate:-} ${compress_traffic:-} \
                         ${companies_section_dump_switch} \
                         ${dumps_section_dump_switch} \
                         -m "dump-summary" \
                         ${dump_continuation_strategy} \
                         -o "${dumps_output_path}" \
                         "${configuration_file}"

trace "compiling profiles"
[ -n "${recipes_section}" ] && recipes_section_profile_switch="-r ${recipes_section}"
[ -n "${split_section}" ] && split_section_profile_switch="-s ${split_section}"
[ -n "${profile_specs_section}" ] && profile_specs_section_profile_switch="-f ${profile_specs_section}"
[ -n "${profile_continuation_strategy}" ] && profile_continuation_strategy="-t ${profile_continuation_strategy}"
[ -n "${num_cpus}" ] && num_cpus="-p ${num_cpus}"
[ -n "${proc_chain_processes}" ] && proc_chain_processes="-P ${proc_chain_processes}"

"${generate_profiles_for_dumps}" ${x_flag:-} ${keep_intermediate:-} ${keep_large_intermediate:-} \
                                 ${recipes_section_profile_switch} \
                                 ${split_section_profile_switch} \
                                 ${profile_specs_section_profile_switch} \
                                 ${profile_continuation_strategy} \
                                 ${num_cpus} \
                                 ${proc_chain_processes} \
                                 -m "profile-summary" \
                                 -o "${profiles_output_path}" \
                                 -g "${configuration_file}" \
                                 "${dumps_output_path}/dump-summary"

trace "done - profiles built!"

