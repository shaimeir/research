#!/usr/bin/env python3

import Crypto.Cipher.AES
import platform
import sys
import os
import argparse
import contextlib
import json

@contextlib.contextmanager
def auto_context(obj):
    yield obj

class DataProcessor(object):
    PROCESSING_KEY = "52l7fhn3n349h3q9"

    @staticmethod
    def process(data):
        processing_key = DataProcessor.PROCESSING_KEY
        cipher = Crypto.Cipher.AES.new(processing_key, Crypto.Cipher.AES.MODE_ECB)
        padding_length = 16 - (len(data) % 16)
        data = data + " " * padding_length
        output_content = cipher.encrypt(data)
        return output_content

    @staticmethod
    def unprocess(data):
        processing_key = DataProcessor.PROCESSING_KEY
        # Note - ECB is relatively weak, if we ever need a stronger method, use chained methods
        cipher = Crypto.Cipher.AES.new(processing_key, Crypto.Cipher.AES.MODE_ECB)
        content = cipher.decrypt(data)
        return content

def main():
    parser = argparse.ArgumentParser(description="decrypt the agent configuration.")
    parser.add_argument("-o", "--output", default="-", help="output file. use '-' for stdout.")
    if platform.system().lower() == 'windows':
        parser.add_argument("configuration", nargs="?", default=r"C:\ProgramData\Secdo\Config\storage", help="the configuration file to decrypt.")
    else:
        parser.add_argument("configuration", nargs="?", default="-", help="the configuration file to decrypt.")

    args = parser.parse_args()

    with (auto_context(sys.stdin) if '-' == args.configuration else open(args.configuration, "rb")) as input_stream:
        config = DataProcessor.unprocess(input_stream.read()).decode("utf-8")

    with contextlib.suppress(Exception):
        config = json.dumps(json.loads(config), indent=4)

    with (auto_context(sys.stdout) if '-' == args.output else open(args.output, "wt")) as output_stream:
        output_stream.write(config+"\n")

if __name__ == "__main__":
    sys.exit(main())

