#!/usr/bin/env python3

import argparse
import json
import csv
import sys
from collections import defaultdict
import contextlib

@contextlib.contextmanager
def auto_yield(o):
    yield o

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--malicious", action='append')
    parser.add_argument("-b", "--benign", action='append')
    parser.add_argument("-o", "--output", default="-")
    parser.add_argument("-f", "--features", required=True)
    args = parser.parse_args()

    assert len(args.benign) > 0 and len(args.malicious) > 0

    with open(args.features, "rt") as features_file:
        feature_names = json.load(features_file)

    cids = defaultdict(lambda : [0] * len(feature_names))
    for files, tag in ((args.malicious, 1), (args.benign, 0)):
        for fname in files:
            with open(fname, "rt") as data_file:
                for entry in csv.DictReader(data_file, delimiter='\t'):
                    cids[(entry['cid'], tag)][feature_names[entry['alert_name']]] = 1

    with (open(args.output, "wt") if args.output != "-" else auto_yield(sys.stdout)) as output_file:
        json.dump([{'cid' : key[0], 'tag' : key[1], 'features' : value} for key, value in cids.items()], output_file, indent=4)

if __name__ == "__main__":
    main()

