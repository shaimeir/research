#!/usr/bin/env python3

import csv
import argparse
import json

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output", required=True)
    parser.add_argument('biocs')
    args = parser.parse_args()

    with open(args.biocs, "rt") as bioc_names_file:
        names = sorted([bioc.strip() for bioc in bioc_names_file])
        names = dict(zip(names, range(len(names))))

    with open(args.output, "wt") as output_file:
        json.dump(names, output_file, indent=4)

if __name__ == "__main__":
    main()

