#!/bin/bash

#set -eux
set -eu

light_red="\033[1;31m"
light_yellow="\033[33;5;7m"
no_colour="\033[0m"

print_usage () {
	echo "This script will remove all files under a company folder."
	echo "You need to specifiy the company name as an argument."
	echo "You can use the -l switch to list all the companies"
}
	
print_companies () {
		all_companies=`ls -l /secdo/agent-mgmt | awk '{ print $9 }'`
		echo "Found the following companies: $all_companies"
}

warning () {
	echo -e "Confirming the below is going to delete all files under a specifc company named "$light_yellow"$company." $no_colour
	echo -e $light_red "+++ Are you sure you want to continue (y/n)? +++" $no_colour
	read
	if [ ! "$REPLY" = "y" ] && [ ! "$REPLY" == "Y" ] ; then
	    echo "Aborting."
	    exit 1
	fi
}

if [ $# -eq 0 ] ; then
	print_usage "input"
	exit 1
fi

if [ "$1" = "-l" ] ; then 
	print_companies 
	exit 0
fi

company=$1

if [ ! -d /secdo/agent-mgmt/$company ] ; then
	echo "$company is not an existing company"
	print_usage
	exit 1
fi

warning $company


for f in $(ls -l /secdo/agent-mgmt/$company | awk '{ print $9}') ; do echo "Removing /secdo/agent-mgmt/$company/$f and all its files and sub-folders..." && rm -r /secdo/agent-mgmt/$company/$f ; done
