#!/bin/sh
latest_version=`pip install secdo-dist== |& sed -r 's/([0-9.]+memsql-master.dev)/\n\1\n/g' | sed -r 's/^,.*//g' | grep -vP '^\s*$' | sort | tail -4 | head -1`
latest_itsec_version=`pip install secdo-dist== |& sed -r 's/([0-9.]+itsec-memsql-stable.dev)/\n\1\n/g' | sed -r 's/^,.*//g' | grep -vP '^\s*$' | sort | tail -4 | head -1`

current_version=`pip freeze | grep -P 'secdo-dist===\d+\.\d+\.\d+\.memsql-(dev|master)\.dev' | sed -r 's/secdo-dist===//g'`
echo "Installed version is $current_version"
echo "There is also an ITSec stable version $latest_itsec_version"
echo "Going to install version $latest_version, continue (y/n)?"
read ans
if [ "$ans" == "y" ]; then
	echo "OK, installing..."
	sh .local/secdo/scripts/upgrade_secdo.sh $latest_version
else
	echo "Aborting..."
fi
