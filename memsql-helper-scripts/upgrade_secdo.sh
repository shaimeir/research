#! /bin/bash

# Define locations
WARFILE=${HOME}/.local/secdo/war/secdo
DEST=${HOME}/secdo-server/webapps/ROOT
CONFIG=/tmp/
UNZIP="unzip -u -q -d"
RMRF="rm -rf"
SECDOPYPI=https://orca.sec.do/simple/
SCTL=supervisorctl
VERSION=$1
PIPCONF="$HOME/.pip/pip.conf"

if ! curl -o /dev/null -s $SECDOPYPI
then
    echo "no access to Orca, quitting"
    exit
fi

if [ ! -d $HOME/.pip ] 
then
     mkdir -p $HOME/.pip
fi

if [ ! -d $(dirname $PIPCONF) ]
then
     mkdir -p $HOME/.pip
fi

if [ ! -e $PIPCONF ]
then
   cat > $PIPCONF<<EOF
[global]
timeout = 60
index-url = https://secdo:private_beta@orca.sec.do/simple
EOF
fi

if [ x$VERSION != "x" ]
then
     INSTALL="==${VERSION}"
fi

# Stop all processes, except redis

echo "Stopping all processes, hang on"
# $SCTL stop all
# $SCTL start redis
for a in $(supervisorctl status  | grep -v redis | awk '{print $1}') ; do supervisorctl stop $a ; done

tar cf /tmp/secdo-config.$$.tar ${HOME}/.local/lib/python2.7/site-packages/configs

# Backup secdo-server config
cp $DEST/WEB-INF/classes/config/*.properties $CONFIG

# bring updated packages from secdo 
echo "Upgrading packages from secdo repo"
# pip install --index-url $SECDOPYPI --upgrade --user secdo_dist${INSTALL}
pip install --trusted-host orca.sec.do --upgrade --user secdo_dist${INSTALL}

# Remove secdo-server webapp
$RMRF ~/secdo-server/webapps/ROOT

# Extract webapp from war
echo "Extracting WAR to webapps"
# $UNZIP $DEST $WARFILE-$(pip freeze | grep secdo-war | awk -F== '{print $2}' | s/=//g).war
$UNZIP $DEST $WARFILE-$(pip freeze | grep secdo-war | egrep -o "[0-9]+\.[0-9]+\.[0-9]+").war

# Copy config back to webapp
cp $CONFIG/*.properties $DEST/WEB-INF/classes/config/

# Start all
echo "Starting all processes"
$SCTL update
$SCTL start all

$SCTL status
echo "upgrade completed, current versions:"
pip freeze | grep -e  "infra\|data-model\|agent-server\|secdo"

