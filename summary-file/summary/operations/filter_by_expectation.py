#!/usr/bin/env python3

'''
filter out lines from a summary file by matching a list of expected tuples given via a CSV.
'''

import logging
import contextlib
import shutil
import os
import csv
import sys

from .  import operations_registry
from .. import summary
from .. import summary_streamers

def init_subparser(subparser):
    subparser.add_argument("-f", "--field", action="append", required=True, help="the fields that comprise the tuples.")
    subparser.add_argument("-d", "--delete-file", action="store_true", help="delete the files associated with lines that were filtered out.")
    subparser.add_argument("-c", "--csv", default="-", help="the CSV file with the expected tuples. use '-' for stdin (default).")
    subparser.add_argument("--csv-separator", default="\t", help="the CSV column separator.")
    subparser.add_argument("--dry", action='store_true', help="don't store filtered results - the summary file won't be modified.")
    subparser.add_argument("summary_file", metavar="summary-file", help='the summary file to clear outdated entries from.')

@contextlib.contextmanager
def auto_stdin():
    yield sys.stdin

class _row_tuplizer:
    '''
    takes a CSV row and converts its columns to the proper expected types.
    '''

    def __init__(self, scheme, fields):
        self.scheme = scheme
        self.fields = tuple(fields)

        types = scheme.types
        self.tuple_sequence = tuple((types[field] for field in fields))

        logging.debug("tuplization types: %r", self.tuple_sequence)

    def tuplize(self, row):
        assert len(row) == len(self.tuple_sequence), ValueError('column count mismatch', row, self.fields)
        return tuple((_type.value(value) for _type, value in zip(self.tuple_sequence, row)))

    def tuplize_line(self, summary_line):
        return tuple((summary_line[field] for field in self.fields))

@operations_registry.register("filter-by-expectation", init_subparser)
def main(args):
    '''
    filter out lines from a summary file by matching a list of expected tuples given via a CSV.
    '''
    streamer = summary_streamers.get_streamer(args.summary_file)
    summary_file = summary.SummaryFile(args.summary_file, streamer)
    summary_file.load()

    _summary = summary_file.summary
    scheme = _summary.scheme

    # validate the fields
    assert set(scheme.fields).issuperset(args.field), ValueError('got an invalid field name with --field', tuple(args.field))

    # get the path columns in case we need to delete some files
    paths = tuple()
    if args.delete_file:
        paths = tuple((field for field, _type in scheme.types.items() if _type.name == 'Path'))

    tuplizer = _row_tuplizer(scheme, args.field)

    # load expected tuples
    with (auto_stdin() if "-" == args.csv else open(args.csv, "rt")) as tuples_file:
        expected_tuples = {
            tuplizer.tuplize(csv_line)
            for csv_line in csv.reader(tuples_file, quoting=csv.QUOTE_NONE, delimiter=args.csv_separator)
        }

    for t in expected_tuples:
        logging.debug("\texpected tuple: %r", t)

    if len(expected_tuples) == 0:
        logging.info("no expected tuples specified - not filtering.")
        return

    assert min(map(len, expected_tuples)) == max(map(len, expected_tuples)) == len(args.field), ValueError('inconsistent line lengths in CSV or between columns and command line fields')

    # filter out the lines
    filtered_summary = summary.Summary(scheme)
    for line in _summary.lines:
        evaluation_tuple = tuplizer.tuplize_line(line)
        logging.debug("evaluating tuple: %r", evaluation_tuple)

        if evaluation_tuple in expected_tuples:
            filtered_summary.add_line(line)
        else:
            logging.info("dropping line: %s", line)
            for path_field in paths:
                path = line[path]
                logging.warn("deleting '%s' specified in '%s' column.", path, path_field)
                try:
                    if path.is_dir():
                        shutil.rmtree(path)
                    else:
                        os.remove(path)
                except FileNotFoundError:
                    pass
                except (PermissionError, NotADirectoryError) as exc:
                    logging.warn("failed to remove %s - %r", path, exc)

    if not args.dry:
        summary_file.save(filtered_summary)

