#!/usr/bin/env python3

'''
export the creation and validation operations of a summary file.
'''

import logging
import pathlib
import contextlib

from .  import operations_registry
from .. import summary
from .. import summary_streamers

def init_subparser(subparser):
    subparser.add_argument("-l", "--validate-only", action='store_true', help="don't create a new config file - only make sure the existing one fits the requested scheme.")
    subparser.add_argument("-r", "--override", action='store_true', help='remove any pre-existing')
    subparser.add_argument("-q", "--quiet-override", action='store_true', help="if the file doens't exist, create it. if it exists, validate it. if it's invalid - override it.")
    subparser.add_argument("-t", "--tag", help="the scheme tag.")
    subparser.add_argument("-f", "--field", action='append', nargs=2, default=[], help='add a field to the summary scheme. the first argument is the name and the second is the type.')
    subparser.add_argument("summary_file", metavar="summary-file", help='the summary file to create or validate')

def scheme_from_args(args):
    element_type_translation = {
        _type.name.lower() : _type.name
        for _type in summary.ElementType
    }

    assert len(args.field) > 0, ValueError('missing field definitions')

    scheme = summary.SummaryScheme(args.tag)
    for field_tuple in args.field:
        field, _type = tuple(field_tuple)
        scheme.add_element(field, element_type_translation[_type.lower()])

    return scheme

def validate_summary(args):
    if not pathlib.Path(args.summary_file).is_file():
        raise FileNotFoundError("summary file either doesn't exist or isn't a file", args.summary_file)

    streamer = summary_streamers.get_streamer(args.summary_file)
    summary_file = summary.SummaryFile(args.summary_file, streamer)
    summary_file.load()

    if summary_file.summary.scheme == scheme_from_args(args):
        logging.info("the scheme in pre-existing summary file %s is valid", args.summary_file)
        return 0
    else:
        logging.info("the scheme in pre-existing summary file %s is invalid", args.summary_file)
        return 1 # failure

def create_summary(args):
    new_summary = summary.Summary(scheme_from_args(args))

    streamer = summary_streamers.get_streamer(args.summary_file)
    summary_file = summary.SummaryFile(args.summary_file, streamer)
    summary_file.save(new_summary)

@operations_registry.register("init", init_subparser)
def main(args):
    '''
    create or validate a summary file.
    '''
    if len([a for a in (args.validate_only, args.override, args.quiet_override) if a]) > 1: # make sure at most one switch is set to true
        logging.error("conflicting arguments: --quiet-override, --override and --validate-only")
        return 1

    elif args.quiet_override:
        validate_result = 1
        with contextlib.suppress(Exception):
            validate_result = validate_summary(args)

        if 1 == validate_result: # on error, override!
            return create_summary(args)

    elif not args.override and (args.validate_only or pathlib.Path(args.summary_file).is_file()):
        return validate_summary(args)

    else:
        return create_summary(args)

