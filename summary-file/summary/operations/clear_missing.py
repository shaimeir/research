#!/usr/bin/env python3

'''
remove lines from a summary where one of the path columns point to a non-exisiting file
'''

import logging
import pathlib

from .  import operations_registry
from .. import summary
from .. import summary_streamers

def init_subparser(subparser):
    subparser.add_argument("-f", "--field", action='append', default=None, help="specify specific fields to check (if none specified then all path columns are verified).")
    subparser.add_argument("summary_file", metavar="summary-file", help='the summary file to clear missing files from.')

@operations_registry.register("clear-missing-files", init_subparser)
def main(args):
    '''
    removes lines from a summary file where the specified path columns point to non-existing files.
    '''
    streamer = summary_streamers.get_streamer(args.summary_file)
    summary_file = summary.SummaryFile(args.summary_file, streamer)
    summary_file.load()

    _types = summary_file.summary.scheme.types
    if args.field is None:
        fields = tuple((field for field, _type in _types.items() if _type.name == 'Path'))
    else:
        assert all((_type[field].name == 'Path' for field in args.field)), ValueError('non-path column specified', args.field)
        fields = tuple(args.field)

    filtered_summary = summary.Summary(summary_file.summary.scheme)
    for line in summary_file.summary.lines:
        if all((line[field].exists() for field in fields)):
            filtered_summary.add_line(line)
        else:
            logging.info("removing line: %r", line)

    summary_file.save(filtered_summary)

