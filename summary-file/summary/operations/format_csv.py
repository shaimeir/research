#!/usr/bin/env python3

'''
print lines of a summary file in CSV format.
'''
import logging
import pathlib
import contextlib
import sys
import csv
from collections import defaultdict

from .  import operations_registry
from .. import summary
from .. import summary_streamers

@contextlib.contextmanager
def auto_yield(obj):
    yield obj

def init_subparser(subparser):
    subparser.add_argument("-u", "--unique", action='store_true', help='make sure an output line appears at most once.')
    subparser.add_argument("-f", "--field", action='append', default=None, help="a field to print.")
    subparser.add_argument("-m", "--on-match", nargs=2, action='append', default=[], help="accepts a field name and a value and only prints lines where the value of that field matched.")
    subparser.add_argument("-o", "--output", default="-", help="the file to write to. use '-' for stdout (default).")
    subparser.add_argument("--separator", default="\t", help="the output separator (default to tab).")
    subparser.add_argument("summary_file", metavar="summary-file", help='the summary file to clear missing files from.')

@operations_registry.register("format-csv", init_subparser)
def main(args):
    '''
    print lines of a summary file in CSV format.
    '''
    streamer = summary_streamers.get_streamer(args.summary_file)
    summary_file = summary.SummaryFile(args.summary_file, streamer)
    summary_file.load()

    _summary = summary_file.summary
    scheme = _summary.scheme

    on_match_index = defaultdict(set)
    for match_tuple in args.on_match:
        on_match_index[match_tuple[0]].add(scheme.types[match_tuple[0]].value(match_tuple[1]))

    logging.debug("match filters:")
    for field, values in on_match_index.items():
        logging.debug("\t%s %r", field, values)

    if args.field is None:
        fields = tuple(scheme.field)
    else:
        fields = tuple(args.field)
        assert set(fields).issubset(scheme.fields), ValueError('an unknown field specified', fields, scheme.columns)

    lines_printed = False
    printed_lines = set()
    with (auto_yield(sys.stdout) if args.output == "-" else open(args.output, "wt")) as output_stream:
        csv_writer = csv.writer(output_stream, dialect="unix", quoting=csv.QUOTE_NONE, delimiter=args.separator)

        for line in _summary.lines:
            if all((line[field] in values for field, values in on_match_index.items())):
                logging.debug("accepted line: %r", line)
                csv_line = tuple((line[field] for field in fields))

                if not args.unique or csv_line not in printed_lines:
                    csv_writer.writerow(csv_line)
                    lines_printed = True
                    printed_lines.add(csv_line)

                else:
                    logging.debug("the line was already printed")

            else:
                logging.debug("dropped line: %r", line)

    return 1 if len(on_match_index) > 0 and len(_summary.lines) > 0 and not lines_printed else 0

