#!/usr/bin/env python3

'''
take a summary file and a reference summary file.
filter out lines that don't (or do) appear in the reference summary file from the other summary file.

usually you'd use this feature in the following manner:
given an existing summary file, you create an "expected" summary file and use it to filter out any
lines in the regular summary file that aren't as expected to validate what is actually finished
(e.g. assume you re-run the same process but change an argument of a single product
so now you need to remove the stale version and re-create the process).
if you then reverese the roles you can create a summary file of all the missing products and iterate over them.
'''

import logging
import pathlib
import os
import shutil

from .  import operations_registry
from .. import summary
from .. import summary_streamers

def init_subparser(subparser):
    subparser.add_argument("-i", "--inverse-filter", action='store_true', help='filter out only lines that do appear in the reference summary')
    subparser.add_argument("-r", "--reference-summary", required=True, type=pathlib.Path, help="the reference summary to filter by.")
    subparser.add_argument("-d", "--delete-files", action="store_true", help="remove files pointed to by filtered lines.")
    subparser.add_argument("-o", "--output", help="don't override the existing summary file and use this file for output instead.")
    subparser.add_argument("summary_file", metavar="summary-file", type=pathlib.Path, help='the summary to filter.')

def delete_files(summary_line):
    for field, field_type in summary_line.scheme.types.items():
        if field_type.name != 'Path':
            continue

        path = summary_line[field]

        if path.exists():
            logging.warn("removing file pointed to by %r: %s", summary_line, path)
            try:
                if path.is_dir():
                    shutil.rmtree(path)
                else:
                    os.remove(path)
            except FileNotFoundError:
                pass
            except (PermissionError, NotADirectoryError) as exc:
                logging.warn("failed to remove %s - %r", path, exc)

@operations_registry.register("filter-by-reference", init_subparser)
def main(args):
    '''
    remove lines that don't/do appear in a reference summary from a summary file
    '''
    streamer = summary_streamers.get_streamer(args.summary_file)

    ref_summary = summary.SummaryFile(args.reference_summary, streamer)
    ref_summary.load()

    summary_file = summary.SummaryFile(args.summary_file, streamer)
    summary_file.load()

    assert ref_summary.summary.scheme == summary_file.summary.scheme, ValueError("the reference and the summary file have different schemes.")

    filtered_summary = summary.Summary(ref_summary.summary.scheme)
    ref_lines = ref_summary.summary.lines

    for line in summary_file.summary.lines:
        if (args.inverse_filter and line not in ref_lines) or (not args.inverse_filter and line in ref_lines):
            filtered_summary.add_line(line)
        else:
            logging.debug("dropping line: %s", line)
            if args.delete_files:
                delete_files(line)

    if args.output is None:
        summary_file.save(filtered_summary)
    else:
        summary.SummaryFile(args.output, streamer).save(filtered_summary)

