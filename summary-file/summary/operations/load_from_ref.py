#!/usr/bin/env python3

'''
import lines from a reference summary file that fit given constraints.
'''

import logging
import pathlib
from collections import defaultdict

from .  import operations_registry
from .. import summary
from .. import summary_streamers

def init_subparser(subparser):
    subparser.add_argument("-r", "--reference-summary", required=True, type=pathlib.Path, help="the reference summary to import from.")
    subparser.add_argument("-m", "--on-match", action="append", nargs=2, help="add a constraint. the first value is a field name and the second value is a value to match for the field.")
    subparser.add_argument("summary_file", metavar="summary-file", type=pathlib.Path, help='the summary to update.')

@operations_registry.register("load-from-reference", init_subparser)
def main(args):
    '''
    import lines from a reference summary file that fit given constraints.
    '''
    streamer = summary_streamers.get_streamer(args.summary_file)

    ref_summary = summary.SummaryFile(args.reference_summary, streamer)
    ref_summary.load()

    summary_file = summary.SummaryFile(args.summary_file, streamer)
    summary_file.load()

    assert ref_summary.summary.scheme == summary_file.summary.scheme, ValueError("the reference and the summary file have different schemes.")

    _summary = summary_file.summary
    scheme = _summary.scheme
    ref_summary = ref_summary.summary

    assert {field for field, value in args.on_match}.issubset(scheme.fields), ValueError('unknown field name specified with -m/--on-match', args.on_match, scheme.fields)

    constraints = defaultdict(set)
    for field, value in args.on_match:
        constraints[field].add(scheme.types[field].value(value))

    for summary_line in ref_summary.lines:
        if all((summary_line[field] in acceptable_values for field, acceptable_values in constraints.items())):
            logging.info("importing: %r", summary_line)
            _summary.add_line(summary_line)
        else:
            logging.debug("not importing %r", summary_line)

    summary_file.save()

