#!/usr/bin/env python3

'''
load and save a profile. used to update summary format if the code is backwards compatible.
'''

import logging
import pathlib

from .  import operations_registry
from .. import summary
from .. import summary_streamers

def init_subparser(subparser):
    subparser.add_argument("summary_file", metavar="summary-file", help='the summary file to create or validate')

@operations_registry.register("reload", init_subparser)
def main(args):
    '''
    load and save a profile. used to update summary format if the code is backwards compatible.
    '''
    streamer = summary_streamers.get_streamer(args.summary_file)
    summary_file = summary.SummaryFile(args.summary_file, streamer)
    summary_file.load()

    summary_file.save()

