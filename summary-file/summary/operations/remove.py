#!/usr/bin/env python3

'''
remove lines fitting a certain field combination.
'''

import logging

from .  import operations_registry
from .. import summary
from .. import summary_streamers

def init_subparser(subparser):
    subparser.add_argument("-f", "--field", nargs=2, action="append", required=True, help="the expected values of the fields. first argument is the field name and the second is the expected value.")
    subparser.add_argument("summary_file", metavar="summary-file", help='the summary file to clear outdated entries from.')

@operations_registry.register("remove", init_subparser)
def main(args):
    '''
    remove lines fitting a certain field combination.
    '''

    streamer = summary_streamers.get_streamer(args.summary_file)
    summary_file = summary.SummaryFile(args.summary_file, streamer)
    summary_file.load()

    _summary = summary_file.summary
    scheme = _summary.scheme

    # validate the fields
    assert set(scheme.fields).issuperset({field for field, value in args.field}), ValueError('got an invalid field name with --field', tuple(args.field))

    expected_parameters = {
        field : scheme.types[field].value(value)
        for field, value in args.field
    }

    logging.debug("looking for a line fitting the following arguments in %s: %r", args.summary_file, expected_parameters)

    filtered_summary = summary.Summary(scheme)
    for summary_line in _summary.lines:
        line_params = {
            field : summary_line[field]
            for field in expected_parameters
        }

        if line_params == expected_parameters:
            logging.info("removing line: %r", summary_line)
        else:
            logging.debug("keeping line: %r", summary_line)
            filtered_summary.add_line(summary_line)

    summary_file.save(filtered_summary)

