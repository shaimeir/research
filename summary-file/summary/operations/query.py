#!/usr/bin/env python3

'''
check if there's a line matching certain expected parameters in a summary file.
'''

import logging

from .  import operations_registry
from .. import summary
from .. import summary_streamers

def init_subparser(subparser):
    subparser.add_argument("-f", "--field", nargs=2, action="append", required=True, help="the expected values of the fields. first argument is the field name and the second is the expected value.")
    subparser.add_argument("summary_file", metavar="summary-file", help='the summary file to query.')

@operations_registry.register("query", init_subparser)
def main(args):
    '''
    check if there's a line matching certain expected parameters in a summary file.
    '''

    streamer = summary_streamers.get_streamer(args.summary_file)
    summary_file = summary.SummaryFile(args.summary_file, streamer)
    summary_file.load()

    _summary = summary_file.summary
    scheme = _summary.scheme

    # validate the fields
    assert set(scheme.fields).issuperset({field for field, value in args.field}), ValueError('got an invalid field name with --field', tuple(args.field))

    expected_parameters = {
        field : scheme.types[field].value(value)
        for field, value in args.field
    }

    logging.debug("looking for a line fitting the following arguments in %s: %r", args.summary_file, expected_parameters)

    for summary_line in _summary.lines:
        line_params = {
            field : summary_line[field]
            for field in expected_parameters
        }

        if line_params == expected_parameters:
            logging.info("found a matching line for the specified query: %r", summary_line)
            return 0 # success
        else:
            logging.debug("line %r mismatched filter", summary_line)

    # no matching lines found
    return 1

