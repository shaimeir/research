#!/usr/bin/env python3

from ..utilities import option_registry_util

operations_registry = option_registry_util.OptionRegistry()

class Operation:
    def __init__(self, subparser_init, main, docstring):
        self.subparser_init = subparser_init
        self.main = main
        self.docstring = docstring
        self.__doc__ = docstring

def register(name, subparser_init):
    def _dec(main):
        global operations_registry

        docstring = getattr(main, '__doc__', None)
        operations_registry.register_option(name, Operation(subparser_init, main, docstring.strip()))

        return main

    return _dec

