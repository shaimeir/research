#!/usr/bin/env python3

'''
print a summary file's tag.
'''

import logging
import pathlib

from .  import operations_registry
from .. import summary
from .. import summary_streamers

def init_subparser(subparser):
    subparser.add_argument("summary_file", metavar="summary-file", help='the summary file to print the tag for.')

@operations_registry.register("get-tag", init_subparser)
def main(args):
    '''
    print a summary file's tag.
    '''
    streamer = summary_streamers.get_streamer(args.summary_file)
    summary_file = summary.SummaryFile(args.summary_file, streamer)
    summary_file.load()

    print(summary_file.summary.tag)

