#!/usr/bin/env python3

'''
add a line to a summary file.
'''

import logging
import pathlib

from .  import operations_registry
from .. import summary
from .. import summary_streamers

def init_subparser(subparser):
    subparser.add_argument("-f", "--field", action='append', nargs=2, default=[], help="set a value of an unsigned field. accepts 2 arguments - field name and value.")
    subparser.add_argument("summary_file", metavar="summary-file", help='the summary file to add the line to.')

@operations_registry.register("add-line", init_subparser)
def main(args):
    '''
    add a line to a summary file. all fields must be specified.
    '''
    streamer = summary_streamers.get_streamer(args.summary_file)
    summary_file = summary.SummaryFile(args.summary_file, streamer)
    summary_file.load()

    scheme = summary_file.summary.scheme

    specified_fields = {field[0] for field in args.field}
    expected_fields = set(scheme.fields)
    assert specified_fields == expected_fields, ValueError('missing/unknown fields specified in the command line',
                                                           tuple(expected_fields.difference(specified_fields)),
                                                           tuple(specified_fields.difference(expected_fields)))

    summary_file.summary.new_line(**dict(args.field))
    summary_file.save()

