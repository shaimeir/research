#!/usr/bin/env python3

'''
load a profile. used to verify summary format by parsing it.
'''

import logging
import pathlib

from .  import operations_registry
from .. import summary
from .. import summary_streamers

def init_subparser(subparser):
    subparser.add_argument("summary_file", metavar="summary-file", help='the summary file to validate.')

@operations_registry.register("validate-format", init_subparser)
def main(args):
    '''
    load a profile. used to verify summary format by parsing it.
    '''
    streamer = summary_streamers.get_streamer(args.summary_file)
    summary_file = summary.SummaryFile(args.summary_file, streamer)
    try:
        summary_file.load()
    except Exception:
        logging.warn("%s is not a properly formatted summary file.", args.summary_file)
        return 1 # fail
    else:
        logging.info("%s is a properly formatted summary file.", args.summary_file)
        return 0 # succeed

