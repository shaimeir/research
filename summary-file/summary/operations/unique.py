#!/usr/bin/env python3

'''
drop summary lines with duplicate patterns.
'''

import logging
import pathlib
from collections import defaultdict

from .  import operations_registry
from .. import summary
from .. import summary_streamers

def init_subparser(subparser):
    subparser.add_argument("-f", "--field", action="append", help="add a field that consists the uniqueness pattern. if none is specified then all the fields are taken.")
    subparser.add_argument("summary_file", metavar="summary-file", type=pathlib.Path, help='the summary to filter.')

@operations_registry.register("unique", init_subparser)
def main(args):
    '''
    drop summary lines with duplicate patterns.
    '''

    streamer = summary_streamers.get_streamer(args.summary_file)
    summary_file = summary.SummaryFile(args.summary_file, streamer)
    summary_file.load()

    _summary = summary_file.summary
    scheme = _summary.scheme

    if args.field is None:
        fields = scheme.fields
    else:
        assert set(args.field).issubset(scheme.fields), ValueError('unknown field specified with -f', args.field, scheme.field)
        fields = tuple(args.field)

    filtered_summary = summary.Summary(scheme)
    keys = set()

    for line in _summary.lines:
        key = tuple((line[field] for field in fields))
        if key in keys:
            logging.info("dropping line: %r", line)
        else:
            logging.debug("keeping line: %r", line)
            filtered_summary.add_line(line)
            keys.add(key)

    summary_file.save(filtered_summary)

