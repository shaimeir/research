#!/usr/bin/env python3

'''
remove lines from a summary file where the timestamp is older than the file mentioned in the line.
'''

import logging
import pathlib
import datetime
import shutil
import os

from .  import operations_registry
from .. import summary
from .. import summary_streamers

def init_subparser(subparser):
    subparser.add_argument("-f", "--field", default=None, help="the path field to test the timestamp for.")
    subparser.add_argument("-d", "--delete-file", action="store_true", help="delete the files associated with outdated lines.")
    subparser.add_argument("summary_file", metavar="summary-file", help='the summary file to clear outdated entries from.')

@operations_registry.register("validate-file-dates", init_subparser)
def main(args):
    '''
    remove lines from a summary file where the timestamp is older than the file mentioned in the line.
    '''
    streamer = summary_streamers.get_streamer(args.summary_file)
    summary_file = summary.SummaryFile(args.summary_file, streamer)
    summary_file.load()

    scheme = summary_file.summary.scheme

    # try and infer the field if a field wasn't specified
    if args.field is None:
        possible_fields = [field for field, _type in scheme.types.items() if _type.name == 'Path']
        assert len(possible_fields) == 1, ValueError('scheme either has no path column or more than one', tuple(possible_fields))
        field = possible_fields[0]
    else:
        field = args.field
        assert scheme.types[field].name == 'Path', ValueError('specified field is not of path type')

    filtered_summary = summary.Summary(summary_file.summary.scheme)
    for line in summary_file.summary.lines:
        path = line[field]
        if path.exists():
            creation_date = datetime.datetime.fromtimestamp(path.stat().st_ctime)
            if creation_date <= line.timestamp:
                filtered_summary.add_line(line)
            else:
                logging.info("dropping line: %s - file %s was created on %s (and the summary line on %s)", line, path, creation_date, line.timestamp)
                if args.delete_file:
                    logging.warn("found a file newer than it's summary - deleting it! %s [is dir: %r]", path, path.is_dir())
                    try:
                        if path.is_dir():
                            shutil.rmtree(path)
                        else:
                            os.remove(path)
                    except FileNotFoundError:
                        pass
                    except (PermissionError, NotADirectoryError) as exc:
                        logging.warn("failed to remove %s - %r", path, exc)
        else:
            logging.info("dropping line: %s - file %s doesn't exist", line, path)

    summary_file.save(filtered_summary)

