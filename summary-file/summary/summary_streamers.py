#!/usr/bin/env python3

'''
utility classes to serialize and de-serialize a summary to and from a string.
used to store the summary as a file.
'''

import json
import sys
import dateutil.parser
import io

try:
    from . import summary
except ImportError:
    import summary

class JsonSummaryStreamer(summary.SummaryStreamer):
    def read(self, read_stream):
        return json.load(read_stream)

    def write(self, summary_dict, write_stream):
        json.dump(summary_dict, write_stream, indent=4)

def get_streamer(filename):
    '''
    get the appropriate streamer for a given file.
    '''
    return JsonSummaryStreamer() # there's only one streamer so far

def test():
    json_streamer = JsonSummaryStreamer()

    _summary = json_streamer.read(io.StringIO(json.dumps({
        'scheme' : {
            'eric' : 'Path',
            'clapton' : 'Signature',
        },

        'lines' : [
            {
                'content' : {
                    'eric' : 'what will you do',
                    'clapton' : {
                        'value' : 'when you get lonely?',
                        'passthrough' : True,
                    }
                },
            },
            {
                'content' : {
                    'eric' : 'you have been running',
                    'clapton' : {
                        'value' : 'hiding much too long',
                        'passthrough' : False,
                    }
                },
            }
        ]
    })))

    output = io.StringIO()
    json_streamer.write(_summary, output)
    output.seek(0)
    print(output.read())

if __name__ == "__main__":
    sys.exit(test())
