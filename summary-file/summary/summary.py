#!/usr/bin/env python3

'''
defines an abstraction class to manage a summary.
the abstractions are:
    SummaryLine - the summary of a single file or directory.
    SummaryScheme - defines the expected content of a summary line.
    Summary - the aggregation of multiple summary lines of a specific scheme.
'''

from collections import namedtuple
import enum
import logging
import termcolor
import datetime
import dateutil
import sys
import pathlib

class Signature:
    global_passthrough = False

    @staticmethod
    def to_serializable(signature):
        return {
            'value' : signature.value,
            'passthrough' : signature.is_passthrough,
        }

    @staticmethod
    def from_serializable(signature):
        return Signature(signature.get('value', ''), signature.get('passthrough', False))

    def __init__(self, value, passthrough=False):
        self._value = str(value)
        self._passthrough = passthrough

    @property
    def value(self):
        return self._value

    def __str__(self):
        return self._value

    def __repr__(self):
        return f"Signature('{self}', '{self._passthrough}')"

    def __hash__(self):
        # to support the pass-through magic signature we require that all signatures have the same hash
        # so set operations will forcibly compare all the signatures via the comparison operator.
        return 1337

    def __eq__(self, other):
        if isinstance(other, str):
            other = Signature(other)

        return isinstance(other, Signature) and (self._value == other._value or self._passthrough or other._passthrough or Signature.global_passthrough)

    def set_passthrough(self):
        self._passthrough = True

    def unset_passthrough(self):
        self._passthrough = False

    @property
    def is_passthrough(self):
        return self._passthrough

def set_signature_global_passthrough(mode=True):
    Signature.global_passthrough = mode

class PathType(pathlib.PosixPath):
    @staticmethod
    def to_serializable(path):
        # make sure summary files have full paths so they are accessible from different working directories
        return str(path.resolve())

    def __eq__(self, other):
        return super().__eq__(other) or (isinstance(other, pathlib.Path) and super(pathlib.Path, self.resolve()) == super(pathlib.Path, other.resolve()))

    def __hash__(self):
        return 1338 # again, force set operations to use explicit comparison

# the element types in a summary object
class ElementType(enum.Enum):
    Value = str
    Path = PathType
    Signature = Signature

# a tuple that defines a summary element
SummaryElement = namedtuple("SummaryElement", (
    "name", # str - the name of the summary element
    "type", # ElementType - the field type as defined in ElementType
))

class SummaryLine:
    def __init__(self, scheme, timestamp=None):
        self._scheme = scheme
        self._types = scheme.types

        self._line_content = {
            field : None
            for field in self._types
        }

        self.update_time(timestamp)

    def update_time(self, timestamp=None):
        self._timestamp = datetime.datetime.now() if timestamp is None else timestamp

    @property
    def fields(self):
        return self.scheme.fields

    @property
    def timestamp(self):
        return self._timestamp

    @property
    def scheme(self):
        return self._scheme

    def __getitem__(self, field):
        return self._line_content[field]

    def __setitem__(self, field, value):
        assert field in self._line_content, KeyError("can't set non-scheme column", field, self.scheme.columns)
        _type = self._types[field].value
        self._line_content[field] = value if isinstance(value, _type) else _type(value)

    @property
    def line(self):
        assert None not in self._line_content.values(), ValueError('missing value in line', self._line_content)
        return self._line_content.copy()

    def __eq__(self, other):
        return isinstance(other, SummaryLine) and self._types == other._types and self._line_content == other._line_content

    def __str__(self):
        fields = []
        for field, value in self._line_content.items():
            _type = self._types[field].name
            fields.append(f"{field}={_type}({value!r})")
        fields.append(f"timestamp={self._timestamp}")
        return type(self).__name__ + "(" + ", ".join(fields) + ")"

    def __repr__(self):
        return str(self)

class SummaryScheme:
    '''
    defines the scheme (fields and their types) of a summary.
    '''
    def __init__(self, tag=None):
        self._types = {}
        self._tag = str(tag)

    def add_element(self, name, elem_type=ElementType.Value):
        if isinstance(elem_type, str):
            elem_type = getattr(ElementType, elem_type)

        self._types[name] = ElementType(elem_type)

    @property
    def tag(self):
        return self._tag

    @tag.setter
    def tag_setter(self, tag):
        self._tag = '' if tag is None else str(tag)

    @property
    def fields(self):
        return tuple(self._types.keys())

    @property
    def types(self):
        return self._types

    def new_line(self, *args, timestamp=None, **kwargs):
        new_line = SummaryLine(self, timestamp)

        assert len(args) <= len(self._types), ValueError('too many arguments passed to new_line()', args, self.fields)
        for field, value in zip(self._types.keys(), args):
            new_line[field] = value

        for field, value in kwargs.items():
            new_line[field] = value

        return new_line

    def __eq__(self, other):
        return isinstance(other, type(self)) and self.types == other.types

class Summary:
    def __init__(self, scheme):
        self._scheme = scheme
        self._lines = []

    @property
    def scheme(self):
        return self._scheme

    @property
    def tag(self):
        return self._scheme.tag

    def new_line(self, *args, timestamp=None, **kwargs):
        line = self._scheme.new_line(*args, **kwargs, timestamp=timestamp)
        self._lines.append(line)

        return line

    def add_line(self, *summary_lines):
        for summary_line in summary_lines:
            assert isinstance(summary_line, SummaryLine), ValueError("got an unexpected object instead of a SummaryLine instance", summary_line)

            line = summary_line.line
            assert summary_line.scheme == self._scheme, ValueError('line scheme is different from the summary scheme', line.scheme, self._scheme)

        self._lines += list(summary_lines)

    @property
    def lines(self):
        return tuple(self._lines)

class SummaryStreamer:
    def read(self, read_stream):
        raise NotImplementedError()

    def write(self, summary, write_stream):
        raise NotImplementedError()

class SummaryFile:
    def __init__(self, filename, summary_streamer):
        self._filename = pathlib.Path(filename)
        self._summary = False
        self._summary_streamer = summary_streamer

    @property
    def summary(self):
        return self._summary

    def _serializable_scheme(self, scheme):
        return {
            'tag' : scheme.tag,
            'fields' : {
                field : _type.name
                for field, _type in scheme.types.items()
            }
        }

    def _deserialize_scheme(self, scheme_dict):
        scheme = SummaryScheme(scheme_dict.get('tag', None))
        for field, _type in scheme_dict.get('fields', {}).items():
            scheme.add_element(field, _type)

        return scheme

    def _serialzable_rows(self, _summary):
        serialization_dict = {
            field : _type.value.to_serializable if hasattr(_type.value, 'to_serializable') else str
            for field, _type in _summary.scheme.types.items()
        }

        return [
            {
                'content' : {
                    field : serialization_dict[field](value)
                    for field, value in summary_line.line.items()
                },

                'timestamp' : str(summary_line.timestamp),
            }

            for summary_line in _summary.lines
        ]

    def _deserialize_lines(self, _summary, lines):
        deserialization_dict = {
            field : _type.value.from_serializable if hasattr(_type.value, 'from_serializable') else _type.value
            for field, _type in _summary.scheme.types.items()
        }

        for line in lines:
            timestamp = line.get('timestamp', None)
            line = {
                field : deserialization_dict[field](serialized_value)
                for field, serialized_value in line.get('content', {}).items()
            }

            _summary.new_line(
                **line,
                timestamp=None if timestamp is None else dateutil.parser.parse(timestamp)
            )

    def _to_serializable_format(self, _summary):
        return {
            'scheme' : self._serializable_scheme(_summary.scheme),
            'lines' : list(self._serialzable_rows(_summary)),
        }

    def _from_serializable_format(self, summary_dict):
        scheme = self._deserialize_scheme(summary_dict.get('scheme', {}))
        self._summary = Summary(scheme)
        self._deserialize_lines(self._summary, summary_dict.get('lines', []))

    def load(self):
        with open(self._filename, "rt") as summary_file:
            self._from_serializable_format(self._summary_streamer.read(summary_file))

        return self._summary

    def save(self, summary=None):
        if summary is None:
            summary = self._summary

        with open(self._filename, "wt") as summary_file:
            if summary is None: # if both summary and self._summary are none there's nothing to save - write an empty file
                pass # the file will be truncated
            else:
                self._summary_streamer.write(self._to_serializable_format(summary), summary_file)

        self._summary = summary

def test():
    logging.basicConfig(level=logging.DEBUG, format="%(asctime)s [%(levelname)s] %(message)s")

    scheme = SummaryScheme('some-tag')
    scheme.add_element('lindsey', ElementType.Path)
    scheme.add_element('stevie', ElementType.Value)
    scheme.add_element('christine', ElementType.Signature)

    print("scheme test")
    print("\t", "types:")
    for field, _type in scheme.types.items():
        print("\t", "\t", termcolor.colored(field, "green"), _type.name)

    print()

    test_summary_line = scheme.new_line("i'm telling you people, i tell you no lie", "my heart was breaking, i'm telling you why", "Back and forth, lies unfurl")
    test_line = test_summary_line

    print("a single line test:")
    print("\t", "content:")
    for field, value in test_line.line.items():
        print("\t", "\t", termcolor.colored(field, "green"), value)

    print()

    summary = Summary(scheme)
    summary.add_line(test_summary_line)
    summary.new_line("In the eyes, in the eyes, in the eyes", "in the eyes of the world", "Eyes...eyes...eyes...eyes...")
    summary.new_line("Mondays children", "are filled with", "face")
    summary.new_line("Tuesday's children", "are filled with", "grace")

    print("summary test:")
    line_num = 0
    for summary_line in summary.lines:
        line = summary_line.line
        line_num += 1
        print("\t", termcolor.colored(f"line #{line_num}", "cyan"))

        for field, value in line.items():
            print("\t", "\t", termcolor.colored(field, "red"), value)

if __name__ == "__main__":
    sys.exit(test())

