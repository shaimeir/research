#!/usr/bin/env python3

import sys
import argparse
import logging

import summary

def main():
    parser = argparse.ArgumentParser(description="create, validate, manipulate and query a summary file.")
    parser.add_argument("-v", "--verbose", action='store_true', help='show debug prints.')
    parser.add_argument("-V", "--very-verbose", action='store_true', help='show verbose debug prints.')
    parser.add_argument("--ignore-signatures", action='store_true', help='ignore signatures when comparing lines and values.')

    subparsers = parser.add_subparsers(dest='action')
    for op_name, operation in summary.operations.operations_registry.operations_registry.choicesFunctions().items():
        operation.subparser_init(subparsers.add_parser(op_name, help=operation.docstring))

    args = parser.parse_args()

    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s", level=logging.DEBUG if args.very_verbose else (logging.INFO if args.verbose else logging.WARN))

    if args.ignore_signatures:
        logging.info("setting signatures to global passthrough mode.")
        summary.set_signature_global_passthrough()

    try:
        return summary.operations.operations_registry.operations_registry.option(args.action).main(args)
    except Exception:
        logging.warn("an error occurred", exc_info=True)
        return 1

if __name__ == "__main__":
    sys.exit(main())

