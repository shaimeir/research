import sys
import os
import dropbox
from dropbox.files import WriteMode
from dropbox.exceptions import ApiError, AuthError


APP_TOKEN = 'NRaJxdjEz6AAAAAAAAAAMB793sVzwjHxikQLL6o3J2TB6N5ci3Em5ZWFULh2LIo7'


def main():
    # Check for an access token
    if (len(APP_TOKEN) == 0):
        sys.exit(
            "ERROR: Looks like you didn't add your access token.")

    # Create a backup of the current settings file
    if not 1 ==len(sys.argv[1:]):
        sys.exit("ERROR: Expecting one argument as file name to upload."
                 )

    # Create an instance of a Dropbox class, which can make requests to the API.
    print("Creating a Dropbox object...")
    dbx = dropbox.Dropbox(APP_TOKEN)

    # Check that the access token is valid
    try:
        dbx.users_get_current_account()
    except AuthError as err:
        sys.exit("ERROR: Invalid access token; try re-generating an access token from the app console on the web.")

    upload(dbx, sys.argv[1:][0])

    print("Done!")


def upload(dbx, file_name):
    if not os.path.isfile(file_name):
        sys.exit("ERROR: Argument provided is not a file.")
    uploaded_file_name = '/' + file_name
    # Uploads contents of LOCALFILE to Dropbox
    with open(file_name, 'rb') as f:
        # We use WriteMode=overwrite to make sure that the settings in the file
        # are changed on upload
        print("Uploading " + file_name + " to Dropbox as " + uploaded_file_name + "...")
        try:
            dbx.files_upload(f.read(), uploaded_file_name, mode=WriteMode('overwrite'))
        except ApiError as err:
            # This checks for the specific error where a user doesn't have
            # enough Dropbox space quota to upload this file
            if (err.error.is_path() and
                    err.error.get_path().error.is_insufficient_space()):
                sys.exit("ERROR: Cannot back up; insufficient space.")
            elif err.user_message_text:
                print(err.user_message_text)
                sys.exit()
            else:
                print(err)
                sys.exit()


if __name__=="__main__":
    main()
