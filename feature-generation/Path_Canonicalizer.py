#!/usr/bin/env python2.7
import re
import threading
import multiprocessing
import copy
from genericpath import isfile
import argparse
import pprint
import logging
import pathlib
from collections import namedtuple, OrderedDict
import six

import os
from operator import add

if six.PY3:
    xrange=range

"""
 Experimental, concatenates two regular expressions, flags are bitwise ANDed
 Assumes, r1 is first and that the regular expressions will form a valid new regular expression.
"""
def re_add(re1, re2):
    r_pattern = re1.pattern + re2.pattern
    r_flags = re1.flags & re2.flags
    return re.compile(r_pattern, r_flags)

"""
    Experimental, concatenates two regular expressions and creates an OR condition between them.
    Flags are again ANDed.
"""
def re_or(re1, re2):
    r_pattern = '(' + re1.pattern + '|' + re2.pattern + ')'
    r_flags = re1.flags & re2.flags
    return re.compile(r_pattern, r_flags)

"""
    Experimental, accepts a regular expression and encloses it with braces that will contain the match group 0, allegedly the largest
"""
def re_enclose(rex):
    r_pattern = rex.pattern
    r_flags = rex.flags
    return re.compile('(' + r_pattern + ')', r_flags)

MIN_LEAF_THRESHOLD = 0.025  # Determines the minimum number of leaves a tree node must have before we stop
                           # iterating it and trying to find additional similarities. The value is in percent
                           # of the total data of the root node (upper most bucket)

PARENT_TO_CHILD_LEAF_RATIO = 0.33  # Determines the ratio between a parent node and its child. If a node splits
                                 # to less than the defined ratio then we can stop tracing it even if it contains more
                                 # than MIN_LEAF_THRESHOLD

PARENT_TO_CHILD_LEAF_ABSOLUTE = 2  # Determines an actual value that prevents splitting of a branch for small numbers. For example: parent = 7,
                                  # child = 6 ==> =~ 0.85 > 0.33 == PARENT_TO_CHILD_LEAF_RATIO

CHILD_TO_PARENT_RATIO = 0.9  # Determines the minimum percentage between a child node and a parent node.
                            # Results is that if 'w' and 'windows' nodes are less than 10% apart we'll delete 'w'

MAX_PATH_ELEM_LEN = 35  # Maximum length of a path element, beyond that length we don't care what the element is

MAX_CHUNKS = multiprocessing.cpu_count()  # The number of consecutive thread that will be used

THREADS = []

OUTPUTS = []

MAX_STRING_LEN = 65535

GLOBAL_OUTPUT_LOCK = multiprocessing.Lock()

path_sep = '\\'
find_first_non_alpha_num = r'((?:[a-z0-9\\]+([^a-z0-9\\])){%d})'

re_ads = re.compile(r'.*?(:[^:]+:\$.+$)')
re_path83 = re.compile(r'^([^\\]{6}~\d)$', re.I)
re_uuid = re.compile(r'(\{?[0-9a-f]{8}\-(?:[0-9a-f]{4}\-){3}[0-9a-f]{12}\}?)', re.I)
re_hash = re.compile(r'((?:[0-9a-f]{16,}){1,16}|[0-9a-f]{40,})', re.I)
re_sid = re.compile(r'(s-\d-\d-\d+-\d{,10}-\d{,10}-\d{,10}-\d{4,5})', re.I)
re_version = re.compile(r'(?!<[0-9a-f])((?:\d+)(?:\.(?:\d+|[0-9a-f]+))+)(?:\-(?:\d+|[0-9a-f]+))*(?=\b|_)', re.I)
re_language = re.compile(r'(?!<[0-9a-z])[a-z]{2}\-[a-z]{2}(?![0-9a-z])', re.I)
re_prefetch_suffix = re.compile(r'\-([0-9a-f]{8})\.pf', re.I)
re_KB = re.compile(r'\bkb\d+\b', re.I)
# A blob, a string with mixed alpha/num (must have both) but not start with amd or win or lin or x (exclude for amd64, win32, x64/86 )
re_BLOB = re.compile(r'(?<![0-9a-z])(?!amd\d+|win\d+|x\d+|lin\d+)((?:\d+[a-z]+[0-9a-z]*|[a-z]+\d+[0-9a-z]*))(?![0-9a-z])', re.I)
re_machine = re.compile(r'(?<![0-9a-z])(amd\d+|win\d+|x\d+|lin\d+)', re.I)
re_number = re.compile(r'(?<![0-9a-z])(\d+)(?![0-9a-z])', re.I)  # We accept '_' as a boundary so \b cannot be used

re_drive = re.compile(r'^(?:\\\\\?\\)?[a-z]:\\', re.I)
re_network = re.compile(r'^(?:\\\\(?!\?))', re.I)
re_windows = re_add(re_drive, re.compile(r'(?:Windows|WinNT|Win32)\\', re.I))
re_program_files = re_add(re_drive, re.compile(r'Program Files(?: \(x86\))?\\', re.I))
re_system = re_add(re_windows, re.compile(r'(?:System32|SysWow64)\\', re.I))
re_drivers = re_add(re_system, re.compile(r'Drivers\\', re.I))
re_programdata = re_add(re_drive, re.compile(r'ProgramData\\', re.I))
re_temp = re_add(re_windows, re.compile(r'Temp\\', re.I))
re_users = re_add(re_drive, re.compile(r'(?:(?:Users|Documents And Settings)' + path_sep*2 + '[^' + path_sep*2 + ']+' + path_sep*2 +')', re.I))
re_appdata = re_add(re_users,  re.compile(r'AppData\\', re.I))
re_local_temp = re_add(re_appdata, re.compile(r'(?:Local|Roaming)\\Temp\\', re.I))
re_system_temp = re_add(re_windows, re.compile(r'Temp\\', re.I))
re_prefetch_folder = re_add(re_windows, re.compile(r'Prefetch\\', re.I))
re_system32_config = re_add(re_system, re.compile(r'config\\', re.I))
# Separated for readability
re_system_file_drive = re.compile(r'(pagefile|hiberfil|io|msdos|config)\.sys|autoexec\.bat', re.I)
re_system_file_user = re.compile(r'ntuser\.dat', re.I)
re_system_file_config = re.compile(r'(default|components|sam|security|software|system)', re.I)
re_system_files = re_or(
    re_or(re_add(re_drive, re_system_file_drive), re_add(re_users, re_system_file_user)),
    re_add(re_system32_config, re_system_file_config))
re_offline = re_add(re_windows, re.compile(r'csc' + path_sep*2 + r'v\d+\.\d+\.\d+' + path_sep*2, re.I))


re_doc_temp = re.compile('~\$[^.]+\.[^.]+')
re_docs = re.compile(r'doc[xm]?|xls[xm]?|ppt[xms]?|txt|pdf|rtf|od[bcmpst]|css|html?|mhtl|xml|xslt')
re_media = re.compile(r'jpe?g|mpe?g|mp[234]|png|bmp|dib|gif|ico|icn|flac|aac|mkv|ogg|mid|avi|psd|svg|mov|wm[av]|dwg')
re_temp = re.compile(r'te?mp|cache|\$\$\$')
re_backup = re.compile('bak|mdbackup|')
re_recycle_bin_temp = re.compile('\$.+$')
re_shortcut = re.compile(r'lnk|url|pif|scf|shs|shb|xnk')
re_executable = re.compile(r'exe|sys|dll|msi|ocx|bin')
re_scripts = re.compile(r'bat|cmd|ps1[m]?|js|vbs|py|php|rb')
re_db = re.compile(r'db[af]?|dat|accdb|mdf|mdb|kdb|hsql|hkdb|syncdb|itdb')
re_archives = re.compile(r'zip|ace|arc|7z|gz(ip)?|tar|bz(2|ip)?|rar|iso|bin|cab')
re_cfg = re.compile(r'inf|cfg|config|ini|')
re_prefetch = re.compile(r'pf')

re_trim_long_path = re.compile(r'(.{,%s}).*' % MAX_PATH_ELEM_LEN, re.I)

re_system_users = re.compile(r'NT AUTHORITY\\SYSTEM')
re_local_service = re.compile(r'NT AUTHORITY\\(?:LOCAL SERVICE|SERVICIO LOCAL)')
re_network_service = re.compile(r'NT AUTHORITY\\(?:NETWORK SERVICE|Servicio de red)')
re_administrator = re.compile(r'[^\\]+\\Administrator$')
ru_unknown_username = re.compile(r'^[^\\]+\\[^\\]+$')

known_canon_forms = OrderedDict()
known_canon_forms[re_enclose(re_drive)] = 'C:'
known_canon_forms[re_enclose(re_network)] = 'NETWROK'
known_canon_forms[re_enclose(re_windows)] = 'Windows'
known_canon_forms[re_enclose(re_system_temp)] = 'Windows_TEMP'
known_canon_forms[re_enclose(re_system)] = 'System32'
known_canon_forms[re_enclose(re_drivers)] = 'Drivers'
known_canon_forms[re_enclose(re_program_files)] = 'Program Files'
known_canon_forms[re_enclose(re_programdata)] = 'ProgramData'
known_canon_forms[re_enclose(re_users)] = 'Users'
known_canon_forms[re_enclose(re_temp)] = 'TEMP'
known_canon_forms[re_enclose(re_local_temp)] = 'LOCAL_TEMP'
known_canon_forms[re_enclose(re_appdata)] = 'APPDATA'
known_canon_forms[re_enclose(re_prefetch_folder)] = 'PREFETCH'
known_canon_forms[re_enclose(re_system_files)] = 'SYS_CONFIG'
known_canon_forms[re_enclose(re_offline)] = 'OFFLINE'

unknown_canon_forms = {
    # re_path83: 'PATH8_3',
    10: (re_uuid, 'UUID'),
    20: (re_sid, 'SID'),
    30: (re_hash, 'HASH'),
    40: (re_doc_temp, 'TEMP'),
    50: (re_language, 'LANG'),
    60: (re_recycle_bin_temp, 'RECYCLE_TEMP'),
    70: (re_KB, 'KB#'),
    80: (re_version, 'VERSION'),
    90: (re_prefetch_suffix, 'PREFETCH_SUFFIX'),
    95: (re_machine, 'MACHINE_TYPE'),
    100: (re_BLOB, 'BLOB'),
    110: (re_number, 'NUM'),
}

max_path_element_length = {
    re_trim_long_path: '_TRIMMED',
}

ext_ads_canon_form = OrderedDict()
ext_ads_canon_form[re_ads] = '.ADS'

ext_canon_forms = OrderedDict()
ext_canon_forms[re_docs] = 'DOCUMENTS'
ext_canon_forms[re_media] = 'MEDIA'
ext_canon_forms[re_temp] = 'TEMP'
ext_canon_forms[re_backup] = 'BACKUP'
ext_canon_forms[re_executable] = 'EXECUTABLES'
ext_canon_forms[re_scripts] = 'SCRIPTS'
ext_canon_forms[re_db] = 'DB'
ext_canon_forms[re_archives] = 'ARCHIVE'
ext_canon_forms[re_cfg] = 'CONFIG'
ext_canon_forms[re_prefetch] = 'PREFETCH'
ext_canon_forms[re_shortcut] = 'SHORTCUT'

known_users = {
    10: (re_system_users, 'NT AUTHORITY\\SYSTEM'),
    20: (re_local_service, 'NT AUTHORITY\\LOCAL SERVICE'),
    30: (re_network_service, 'NT AUTHORITY\\NETWORK SERVICE'),
    40: (re_administrator, 'HOSTNAME\\Administrator'),
    50: (ru_unknown_username, 'HOSTNAME\\USERNAME')
}

path_cardinalities = {}



def canonicalize_user_name(username):
    for kun in sorted(known_users.keys()):
        regex, substr  = known_users[kun]
        m = re.match(regex, username)
        if m:
            return substr
    return username


# Tries to find a canonical form for a path against known canonical forms.
# Order matters but opt-out for finding longest match
def find_canonical_form(path, path_sep):
    results = []
    # print path
    # print path
    for c_form in known_canon_forms.keys():
        # print path, '==>', c_form.pattern, c_form.flags
        m = re.search(c_form, path)
        if m is None:
            results.append([None, None])
        else:
            # print m.groups()
            results.append([m.group(0), len(m.group(0))])
            if 0 == len(m.group(0)):
                print('Got 0 len group len on %s with %s' % (path, c_form.pattern))
                # print results[-1]
    # print results
    # No match? return None
    if any(map(lambda r: r[0], results)):
        # Find longest match...
        results_len = list(map(lambda x: len(x.split(path_sep) if x else []), [y[0] for y in results]))
        # print results_len
        index_max = max(xrange(len(results_len)), key=results_len.__getitem__)
        # print index_max, results[index_max][1]
        # print results
        # Also return the length of the match in the string...
        res = known_canon_forms[list(known_canon_forms.keys())[index_max]], results[index_max][1]
        # print res
        # print '****', results[index_max], results
        return res[0], res[1]
    else:
        return None, None


# Tries to find a canonical form for a path against known canonical forms.
def find_unknown_canonical_form(path, path_sep, fname):
    # Match against file extensions....
    file_ext = ''
    orig_name = path
    is_file = 0 < len(fname)
    file_name = ''
    if is_file:
        re_file_name_with_ext = re.compile(r'[^' + path_sep*2 + ']+\.' + '[^' + path_sep*2 + ']+$', re.I)
        if not fname.endswith(path_sep) and re.match(re_file_name_with_ext, fname):
            # To handle multiple '.' in file name
            tmp = fname.split('.')
            file_name, file_ext = ".".join(tmp[:-1]), tmp[-1]
            # Canonicalize extensions
            ext_results = []
            # Special case for ADS - no need for a for loop now
            ads_extension = ''
            ads_ext = list(ext_ads_canon_form.keys())[0]
            m = re.match(ads_ext, file_ext)
            if m:
                ads_extension = ext_ads_canon_form[ads_ext]
            for exts in  ext_canon_forms:
                m = re.match(exts, file_ext)
                if m is None:
                    ext_results.append(None)
                else:
                    ext_results.append(m.group(0))
            if any(ext_results):
                results_len = list(map(lambda x: len(x) if x else 0, ext_results))
                index_max = max(xrange(len(results_len)), key=results_len.__getitem__)
                res = ext_canon_forms[list(ext_canon_forms.keys())[index_max]], ext_results[index_max]
                file_ext = res[0]
            else:
                file_ext = 'UNKNOWN'
            if 0 < len(ads_extension):
                file_ext += ads_extension

    # print path

    results = []
    for element in sorted(unknown_canon_forms.keys()):
        u_c_form = unknown_canon_forms[element][0]
        subbedstring = unknown_canon_forms[element][1]
        path = re.sub(u_c_form, subbedstring, path)


    res = path + (path_sep + 'FILENAME' if is_file else '') + ('.' if is_file and 0 < len(file_ext) else '') + file_ext

    return res


def is_file(path_name, path_sep):
    if path_sep in path_name or '.' not in path_name:
        return False
    return True
        # Not having a definite choice here
        # (in the future we may use events to distinguish between a file action and a directory action
        # by looking at the fileAccessType field - Only introduced in late Nov 2016.)


def process_one_path_to_bucket(canon_path, path_sep):
    global buckets
    buckets = {}

    path_elems = canon_path.split(path_sep)
    inner_part = re.sub(re.compile(r'[^0-9a-z\\]', re.I), '.', path_sep.join(path_elems[1:-1])).split(path_sep)
    new_path_elems = []
    in_FOLDER_sub_path = False
    key = path_elems[0]
    buckets.setdefault(key, set([]))
    for i in range(len(inner_part)):
        if 'FOLDER' == inner_part[i] and not in_FOLDER_sub_path:
            in_FOLDER_sub_path = True
            new_path_elems.append('FOLDER')
        elif not 'FOLDER' == inner_part[i]:
            in_FOLDER_sub_path = False
            new_path_elems.append(inner_part[i])
    buckets[key] |= set([path_sep.join(new_path_elems + [path_elems[-1]])])


def pretty_print(canon_paths):
    pass

def reduce(v1, v2):
    path_1, action_1, card_1 = v1
    path_2, action_2, card_2 = v2
    if path_1 == path_2 and action_1 == action_2:
        return (path_1, action_1, card_1 + card_2)

def create_path_buckets_with_cardinality(canon_paths, path_sep):
    pass
    bucket = {}
    for c_p, card in canon_paths:
        path_elems = c_p.split(path_sep)
        key = path_elems[0]
        buckets.setdefault(key, set([]))
        inner_part = path_elems[1:-1]
        buckets[key] |= set([inner_part])

        pass

def create_path_buckets(canon_paths, path_sep):
    # Key-Value of buckets composed of the following:
    # Key - Concatenation of canon suffix and prefix - to simplify the process
    # Value - the inner string between the suffix and the prefix
    # We will also find consecutive occurrences of FOLDER in the inner element and convert it to single
    # Note we replace every non alpha character to a '.' character

    buckets = {}
    bucket_cardinality = {}

    #for c_p, path_card in canon_paths:
    for c_p in canon_paths:
        path_card = 10
        # canon_paths elements are tuples of [path, cardinality]
        path_elems = c_p.split(path_sep)
        inner_part = re.sub(re.compile(r'[^0-9a-z_\\]', re.I), '.', path_sep.join(path_elems[1:-1])).split(path_sep)
        new_path_elems = []
        in_FOLDER_sub_path = False
        key = path_elems[0]
        buckets.setdefault(key, set([]))
        for i in range(len(inner_part)):
            if 'FOLDER' == inner_part[i] and not in_FOLDER_sub_path:
                in_FOLDER_sub_path = True
                new_path_elems.append('FOLDER')
            elif not 'FOLDER' == inner_part[i]:
                in_FOLDER_sub_path = False
                new_path_elems.append(inner_part[i])

        inner_path = path_sep.join(new_path_elems + [path_elems[-1]])
        bucket_cardinality.setdefault(path_sep.join([key] + [inner_path]), 0)
        bucket_cardinality[path_sep.join([key] + [inner_path])] += int(path_card)
        buckets[key] |= set([inner_path])
        logging.debug("buckets[%s] = %s -> %s", key, c_p, inner_path)

    logging.debug(pprint.pformat(buckets))
    logging.debug(pprint.pformat(bucket_cardinality))
    return buckets, bucket_cardinality


def build_canon_paths_from_csv(csv_file_name):
    canon_paths_cardinality = []
    with open(csv_file_name) as f:
        data = f.readlines()
        # path, cardinality
        for l in data:
            idx = l.rfind(',')
            p, c = l[:idx].strip(), l[idx+1:].strip()
            canon_paths_cardinality.append((p, c))
    return canon_paths_cardinality

def find_index_of_first_non_num_alpha(s, split_distance=0):
    non_alpha_num_char = re.search(set_split_distance_to_re(re_find_first_non_alpha_num, split_distance), s)
    if non_alpha_num_char is None:
        return None
    char_idx = 0
    while 0 < split_distance:
        tmp_idx = s.find(non_alpha_num_char, char_idx)
        if tmp_idx:
            split_distance -= 1
            char_idx += tmp_idx
        else:
            return None
    return char_idx


def max_len_in_list(l):
    return max(map(lambda x: len(x), [y for y in l]))


def set_split_distance_to_re(re_, split_distance):
    re_ = re.compile(re_.pattern % split_distance, re_.flags)

    return re_

def find_largest_similar_tree(similarities):
    # These are the initial buckets we define
    buckets = similarities.keys()
    similar_paths = []
    # print similarities
    def find_largest_similar_tree_inner(similarities_inner, root_cardinality, curr_bucket, prev_path):
        inner_buckets = similarities_inner.keys()
        # print inner_buckets
        for inner_bucket in inner_buckets:
            if root_cardinality is None:
                root_cardinality = similarities_inner[inner_bucket][1]
            # print root_cardinality, inner_bucket, similarities_inner[inner_bucket]
            current_cardinality = similarities_inner[inner_bucket][1]
            # We met the splitting threshold OR no more nodes in the subtree
            if float(current_cardinality)/root_cardinality < MIN_LEAF_THRESHOLD or \
                    0 == len(similarities_inner[inner_bucket][0].keys()):
                return similar_paths.append((curr_bucket[0], current_cardinality))
            else:
                # print similarities_inner[inner_bucket][0]
                # print root_cardinality
                # print similarities_inner[inner_bucket]
                # print inner_bucket
                find_largest_similar_tree_inner(similarities_inner[inner_bucket][0], root_cardinality, similarities_inner[inner_bucket], inner_bucket)
        return similar_paths

    for bucket in buckets:
        # print bucket, similarities[bucket]
        if not (bucket == 'Windows'):
            continue
        logging.info("similarities[%r] = %r", bucket, similarities[bucket])
        similar_paths = find_largest_similar_tree_inner(similarities[bucket], None, None, None)

    return similar_paths

def get_path_no_suffix(path, split_path):
    split_path = path.split(path_sep)
    idx = -1 if '.' in split_path[-1] else None
    return path_sep.join(split_path[:idx])


# Returns None, None if the path is without a file name, else returns path, filename
def get_path_and_filename(path, path_sep):
    split_path = path.split(path_sep)
    idx = -1 if '.' in split_path[-1] else None
    # We have something that maybe a filename, verify...
    if idx and 'FILENAME' in split_path[idx]:
        return [path_sep.join(split_path[:idx]), split_path[idx]]
    return [None, None]


def build_inner_similarities_with_full_cardinality_threshold(canon_path_buckets, canon_path_buckets_cardinality):
    """
    :param canon_path_buckets: Buckets of canonicalized paths
    :param canon_path_buckets_cardinality: Cardinality of canonicalized paths
    :return: A Dictionary with the highest frequency of paths and their files as leaf nodes
    """
    similarites = {}
    # Prefixes of the canonicalized paths will be compared against this list
    cardinal_keys = canon_path_buckets_cardinality.keys()

    for key in canon_path_buckets.keys():
        inner_paths = canon_path_buckets[key]
        # print inner_paths
        matches_dict = {}
        if 1 >= len(inner_paths):
            continue
        split_distance = 0
        root_cardinality = len(inner_paths)
        # Initialize the set of paths and return only those elements which are not [None, None]
        inner_paths_no_suffix = filter(lambda z: z[0] and z[1], [get_path_and_filename(x, path_sep) for x in list(inner_paths)])

        while True:
            split_distance += 1


def build_inner_similarities_with_cardinality_threshold(canon_path_buckets):
    # This approach will run multiple times on inner paths, throwing away those that are not included in the
    # minimum threshold
    # Start
    similarities = {}
    for key in canon_path_buckets.keys():
        # print key, len(canon_path_buckets[key]), list(canon_path_buckets[key])[:10] if 1 < len(canon_path_buckets[key]) else ''
        bucket_size = len(canon_path_buckets[key])
        inner_paths = canon_path_buckets[key]
        # print inner_paths
        matches_dict = {}
        if 1 >= len(inner_paths):
            continue
        split_distance = 0
        root_cardinality = len(inner_paths)
        # Initialize the set of paths and return only those elements which are not [None, None]
        inner_paths_no_suffix = filter(lambda z: z[0] and z[1], [get_path_and_filename(x, path_sep) for x in list(inner_paths)])
        # print inner_paths_no_suffix
        # print key

        # Get the sum of cardinalities for all file types


        while True:
            split_distance += 1
            # print split_distance,
            # print len(inner_paths_no_suffix), split_distance
            # Have we processed all paths?
            if 0 == len(inner_paths_no_suffix):
                break
            for y in inner_paths_no_suffix:
                inner_path, filename = y
                if split_distance > len(inner_path):
                    inner_paths_no_suffix.remove(y)
                    continue
                # Quickly do the first character
                match = inner_path[:split_distance]
                matches_dict.setdefault(match, [0, set()])  # A dictionary to hold next matches and a cardinality counter
                inner_data = matches_dict[match]
                cardinality, file_types = inner_data
                file_types |= set([filename])
                matches_dict[match] = [cardinality + 1, file_types]  # Add 1 to the cardinality
                # print match, matches_dict[match], split_distance

            # Sorting out phase, any key that its cardinality is low should be dumped and the relevant inner_paths
            # should be removed
            # print matches_dict
            leaves_to_remove = []
            for z in inner_paths_no_suffix:
                remove = False
                inner_path, inner_file_types = z
                match = inner_path[:split_distance]
                try:
                    cardinality, file_types = matches_dict[match]
                    if 1 < len(match):
                        # Must exist...
                        parent_cardinality, parent_file_types = matches_dict[match[:-1]]
                    else:
                        parent_cardinality = cardinality
                    if parent_cardinality - cardinality < PARENT_TO_CHILD_LEAF_ABSOLUTE:
                        logging.info("Removed %s with cardinality ratio %f - PARENT_TO_CHILD_LEAF_ABSOLUTE ", inner_path, parent_cardinality - cardinality)
                        remove = True
                    elif(cardinality)/parent_cardinality < PARENT_TO_CHILD_LEAF_RATIO:
                        logging.info("Removed %s with cardinality ratio %f - PARENT_TO_CHILD_LEAF_RATIO ", inner_path, float(cardinality)/parent_cardinality)
                        remove = True
                    # print float(cardinality)/root_cardinality, '<', MIN_LEAF_THRESHOLD
                    elif float(cardinality)/root_cardinality < MIN_LEAF_THRESHOLD:
                        logging.info("Removed %s  with cardinality ratio %f - MIN_LEAF_THRESHOLD",inner_path, float(cardinality)/parent_cardinality)
                        remove = True
                except Exception as e:
                    # print "Removed %s due to exception %s" % (inner_path, e)
                    # remove = True
                    pass

                if remove:
                    leaves_to_remove.append(z)
                    matches_dict.pop(match)

            for z in leaves_to_remove:
                inner_paths_no_suffix.remove(z)
                

        # Clean up of redundant nodes, for example if a\b\c and a\b\c\d has same cardinality then
        # a\b\c is removed
        all_current_keys = matches_dict.keys()
        for k in all_current_keys:
            k_cardinality, k_file_types = matches_dict[k]
            if float(k_cardinality)/root_cardinality < MIN_LEAF_THRESHOLD:
                # print "Popd item %s - method 1" % (k)
                matches_dict.pop(k)
                pass
            else:
                for y in all_current_keys:
                    # It is contained but not itself, and the cardinality is equal - if it isn't then it must be smaller, so do not remove -
                    if k != y and k in y and matches_dict.has_key(y):
                        y_cardinality, y_file_types = matches_dict[y]
                        if (float(y_cardinality)/k_cardinality > CHILD_TO_PARENT_RATIO) or (k_cardinality - y_cardinality) < PARENT_TO_CHILD_LEAF_ABSOLUTE:
                            # print "Popd item %s - method 2" % (k)
                            try:
                                matches_dict.pop(k)
                            except KeyError as ke:
                                # Could be that it was already removed if there is a split of the path, like, 'a\b'
                                # and 'a\c' and you want to remove 'a\'
                                pass
                            except Exception as e:
                                raise(e)

        similarities.setdefault(key, matches_dict)

    return similarities

def find_inner_similarities(canon_path_buckets):
    # Idea - for every string that appears in the bucket find the first non alpha numeric character.
    # Cut the string according to this char and feed it into the matches_dict with a count.
    # At this point prev_matches_count is [None] and therefor we do prev_matches_count <== matches_count
    # Iterate this process for the next non alpha num character.
    # Compare the amount of prefixes added or removed from previous step - len(prev...) - len(curr...)
    # As the group size increases or decreases find a way to reduce the number of different paths, that is
    # we want to find the minimal size of the group.
    # TODO - Implement a smarter approach that track which group from the previous stage may have diverged to
    # TODO - multiple parts instead of converge. As it makes sense we might have different group lengths that
    # TODO - behave differently and therefor benefit from different splitting distances.

    global re_find_first_non_alpha_num
    # Start
    similarities = {}
    for key in canon_path_buckets.keys():
        # print key, len(canon_path_buckets[key]), list(canon_path_buckets[key])[:10] if 1 < len(canon_path_buckets[key]) else ''
        bucket_size = len(canon_path_buckets[key])
        inner_paths = canon_path_buckets[key]
        matches_dict = {}
        if 1 >= len(inner_paths):
            continue
        for inner_path in inner_paths:
            split_distance = 1
            inner_path_no_suffix = path_sep.join(inner_path.split(path_sep)[:-1])

            # Quickly do the first character
            match = inner_path_no_suffix[:split_distance]
            matches_dict.setdefault(match, [{}, 0]) # A dictionary to hold next matches and a cardinality counter
            inner_data = matches_dict[match]
            matches_dict[match] = [inner_data[0], inner_data[1] + 1] # Add 1 to the cardinality

            while split_distance < len(inner_path_no_suffix):
                # For the next iteration
                split_distance += 1
                match = inner_path_no_suffix[:split_distance]
                # print inner_path_no_suffix, split_distance, match
                # Find the deepest node that we need to update and put the match as data and update the counter
                prev_match = match[:-1]
                j = 1
                tree_node = matches_dict[prev_match[:j]][0]
                # print prev_match, match, matches_dict, tree_node
                while j < len(prev_match):
                    tree_node = tree_node[prev_match[:j + 1]][0]
                    j += 1
                # print "Updating ", tree_node
                tree_node.setdefault(match, [{}, 0])
                inner_data = tree_node[match]
                tree_node[match] = [inner_data[0], inner_data[1] + 1]  # Add 1 to the cardinality

        # if done:
        # print "Cannot split path any more. Reulst for key %s are:" % (key)
        # print matches_dict
        similarities.setdefault(key, matches_dict)
    return similarities

def canonicalize_path(path):
    # try:
    original_path = path
    path_sep = '/' if '/' in path else '\\'
    re.sub(re.compile(path_sep*2 + '+', re.I), path_sep*2, path)
    path.replace('"', '')
    path_elems = path.split(path_sep)
    # print path
    # Check the last part for file name, drop it for the path canonicalization.
    # Returns None on failure.
    i = None
    if is_file(path_elems[-1], path_sep):
        i = -1

    # Make sure there is a trailing path separator
    canonical_form, match_len = find_canonical_form(path_sep.join(path_elems[:i]) + path_sep, path_sep)

    extra_path = []
    if canonical_form is None:
        match_len = 0
        canonical_form = ''
    path_elems = path[match_len:].split(path_sep)
    root_elem = path[:match_len]
    last_path_idx = None
    if path.endswith(path_sep):
        file_part = ''
    else:
        last_path_idx = path.rfind('\\')
        file_part = path[path.rfind('\\') + 1:]

    leftover_path = path[match_len:last_path_idx]

    unknown_canon_path = find_unknown_canonical_form(leftover_path, path_sep, file_part)
    # Assemble the path to be disassembled again ;)
    res = path_sep.join([canonical_form] + [unknown_canon_path])

    logging.debug("canon translzation: %s -> %s", original_path, res)
    return res


def set_output_of_worker_thread(id, output):
    global OUTPUTS
    logging.info("Adding output of thread id #%d", id)
    GLOBAL_OUTPUT_LOCK.acquire()
    OUTPUTS += output
    GLOBAL_OUTPUT_LOCK.release()


def WorkDistributer(fn, data_set):
    global MAX_CHUNKS, THREADS
    logging.info("Doing %d parallel workers", MAX_CHUNKS)
    THREADS = []
    chunk_size = int(len(data_set) + MAX_PATH_ELEM_LEN)/MAX_CHUNKS  # Just make sure the last chunk gets all of the remaining data
    logging.info("Decided on chunk_size of %d", chunk_size)
    for i in range(MAX_CHUNKS):
        THREADS.append(WorkerThread(i, fn, data_set[chunk_size * i:chunk_size * (i+1)]))
        THREADS[-1].start()
    for i in range(len(THREADS)):
        THREADS[i].join()
    return OUTPUTS

def extract_path_from_csv_line(line):
    try:
        # Multiple commas forces us to use this method to extract the file names from the service name
        i = line.lower().find('svchost.exe')
        p = line[:line[:i].rfind(',')]
        # paths |= set([p])
        return p
    except Exception as e:
        logging.error("an error occurred", exc_info=True)


class WorkerThread(threading.Thread):
    def __init__(self, tid, fn, data_set):
        threading.Thread.__init__(self)
        self.tid = tid
        self.fn = fn
        self.data_set = data_set
        self.num_items_to_report = 500000

    def run(self):
        output = []
        # for e in self.data_set:
        for i in range(len(self.data_set)):
            e = self.data_set[i]
            output.append(self.fn(e))
            if i > 0 and 0 == i % self.num_items_to_report:
                logging.info("Thread #%d completed %d iterations", self.tid, self.num_items_to_report)
        set_output_of_worker_thread(self.tid, output)

def process_with_spark(svchost_paths_csv, output_csv, limit, trunc):
    from pyspark import SparkContext
    from pyspark.sql.functions import col
    from pyspark.sql import SQLContext

    logging.info("creating spark environment...")
    sc = SparkContext("local[{}]".format(multiprocessing.cpu_count()), "Path Canonicalizer")
    # sc.addPyFile(__file__)
    sqlContext = SQLContext(sc)
    logging.info("spark context created!")

    logging.info("loading %s", svchost_paths_csv)
    csv_path = pathlib.Path(os.path.abspath(svchost_paths_csv)).as_uri()
    svchost_paths_view = sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true', delimiter='\t', mode='DROPMALFORMED').load(csv_path)
    svchost_paths_view.createOrReplaceTempView('svchost_paths')
    logging.info("created svchost paths view")

    # data transformation functions for mapping and reduction
    def transform_raw_row(filePath, fileAccessType, cmd, username, sessionId, parentFullFilePath):
        service_type = cmd.split("-k", 1)[-1].strip()
        return (service_type, [{(canonicalize_path(filePath), fileAccessType) : 1}, # dictionary of possible file actions and their cardinality
                               {username}, # set of possible users
                               {sessionId}, # set of session ids
                               {parentFullFilePath}, # set of possible parents
                               ]) 

    def aggregate_rows(row1, row2):
        actions1 = row1[0]
        actions2 = row2[0]

        for file_action in actions2:
            actions1[file_action] = actions1.get(file_action, 0) + actions2[file_action]

        row1[1] |= row2[1] # aggregate user
        row1[2] |= row2[2] # aggregate session id
        row1[3] |= row2[3] # aggregate parent

        return row1

    def deflate_entry(krow):
        import agent_constants

        key, row = krow
        
        actions = row[0]
        users = " ".join(row[1]) # concatenate user names
        session_id = " ".join((str(_session_id) for _session_id in row[2])) # concatenate session ids
        parent = " ".join(row[3]) # concatenate parent processes

        total_file_operations = float(sum(actions[f_op] for f_op in actions)) # so devision won't yield 0 (damn python 2! damn it to hell!)

        return [(e[0], # canon file name
                 agent_constants.FileAccessTypes.get(e[1], "bad type"), # access type
                 key,  # service type
                 users,
                 session_id,
                 parent,
                 actions[e], # cardinality
                 round(100 * actions[e]/total_file_operations, 2), # percentage - prominence of file operation for service type
                 ) for e in actions] # for each file operation

    

    logging.info("building query")
    query_df  = sqlContext.sql("select filePath, fileAccessType, cmd, username, sessionId, parentFullFilePath from svchost_paths").\
       rdd.map(lambda row: transform_raw_row(**row.asDict())).\
       reduceByKey(aggregate_rows).\
       flatMap(deflate_entry).\
       toDF().\
       withColumnRenamed("_1","CanonPathForm").\
       withColumnRenamed("_2","AccessType").\
       withColumnRenamed("_3","ServiceType").\
       withColumnRenamed("_4","Username").\
       withColumnRenamed("_5","SessionId").\
       withColumnRenamed("_6","ParentProcess").\
       withColumnRenamed("_7","Cardinality").\
       withColumnRenamed("_8","Percentage")
    
    result_view = query_df.sort(col("Cardinality").desc()) # sort by service type

    if output_csv is None:
        result_view.show(n=result_view.count() if limit is None else limit, truncate=trunc)
    else:
        logging.info("number of partitions = %d", result_view.rdd.getNumPartitions())
        (result_view if limit is None else result_view.limit(limit)).repartition(1).write.csv(output_csv, sep='\t', header=True)

    sc.stop()

def test(svchost):
    paths = [
     'c:\\windows\\softwaredistribution\\scanfile\\4309c5d0-b054-401b-8985-bdd984c0c03a\\package35.cab',
     'c:\\windows\\softwaredistribution\\scanfile\\2ab9954e-6061-423f-96f8-4f234666846c\\package8.cab',
     'c:\\windows\\softwaredistribution\\download\\6ff54a8c2dcbfe8950a708ec65ea9536\\$dpx$.tmp\\5e8a173ca5482e46a8fceeda0ad008a2.tmp',
     'c:\\windows\\softwaredistribution\\scanfile\\659850b0-5e35-49ed-a3b5-0868cede93dd\\package45.cab',
     'c:\\windows\\softwaredistribution\\scanfile\\83f49b2d-0a0c-465b-8b46-2aeda62a7605\\package20.cab',
     # 'c:\\programdata\\microsoft\\windows\\apprepository\\7fe98657-6665-43b7-8554-3db733902295_s-1-5-21-4186299712-438228993-2498690145-33892_1.rslc',
     'c:\\windows\\softwaredistribution\\scanfile\\b5c883f6-2c15-485c-8c7e-b7f818de26e4\\package44.cab',
     'c:\\windows\\softwaredistribution\\scanfile\\f659c3fe-e7ef-46e9-b276-6dae7c3cac4f\\package16.cab',
     'c:\\windows\\softwaredistribution\\scanfile\\428ded3e-6159-4410-8d45-b811f90e0ecc',
     'c:\\windows\\softwaredistribution\\wuredir\\9482f4b4-e343-43b6-b170-9a65bc822c77\\tmp474f.tmp',
     'c:\\windows\\softwaredistribution\\scanfile\\3d335b5c-a70c-482a-bacb-8226fddd4c0f\\package5.cab',
     'c:\\windows\\softwaredistribution\\scanfile\\f56b3068-1dfa-4a2c-841e-dbe129f1befe\\package39.cab',
     'c:\\windows\\softwaredistribution\\scanfile\\0b833875-7ee8-4c5c-9c5a-0e2cc7df1dc8\\package44.cab',
     'c:\\windows\\softwaredistribution\\scanfile\\86d06ede-7bbd-4dfb-877c-0c6b7287ae73\\package21.cab',
     'c:\\windows\\softwaredistribution\\scanfile1\\package21.cab',
     'c:\\windows\\softwaredistribution\\scanfile1\\package21.tmp',
     'c:\\windows\\softwaredistribution\\scanfile2\\package21.cab',
     'c:\\windows\\softwaredistribution\\scanfile2\\package21.tmp',
     'c:\\windows\\softwaredistribution\\scanfile3\\package21.cab',
     'c:\\windows\\softwaredistribution\\scanfile3\\package21.tmp',
     'c:\\windows\\softwaredistribution\\scanfile4\\package21.cab',
     'c:\\windows\\softwaredistribution\\scanfile4\\package21.tmp',
     'c:\\windows\\softwaredistribution\\scanfile5\\package21.cab',
     'c:\\windows\\softwaredistribution\\scanfile5\\package21.tmp',
     'c:\\windows\\softwaredistribution\\scanfile6\\package21.cab',
     'c:\\windows\\softwaredistribution\\scanfile6\\package21.tmp',
        # 'c:\\windows\\system32\\wdi\\{86432a0b-3c7d-4ddf-a89c-172faa90485d}\\{7dc11415-35da-4548-a29e-f5410daa0151}\\snapshot.etl',
     # 'c:\\programdata\\microsoft\\windowsdefender\\scans\\failtelemetry\\4fdff350797eb744916aac6197b434b7\\',
     # 'c:\\programdata\\microsoft\\windowsdefender\\scans\\failtelemetry\\7a09ec30c095f944b7c1e5f9b29b2217\\',
     # 'c:\\programdata\\microsoft\\windowsdefender\\scans\\failtelemetry\\8b038578e5994c4095c79c7475c1f4f0\\',
     # 'c:\\programdata\\microsoft\\windowsdefender\scans\\failtelemetry\\540784ab05eaaa48b9805c1e05d78da1\\',
     'c:\\windows\\softwaredistribution\\scanfile\\653cf18f-394e-455b-955c-c1b2c972a50d\\package2.cab',
     'c:\\windows\\softwaredistribution\\scanfile\\c5299120-1d21-44ed-b831-f28d1b13929f\\package12.cab',
     'c:\\windows\\softwaredistribution\\wuredir\\7971f918-a847-4430-9279-4a52d1efe18d\\tmpbbba.tmp',
     'c:\\windows\\softwaredistribution\\scanfile\\6c6abb0a-15e2-410d-b23e-d5960dc309df\\package51.cab',
     'c:\\windows\\softwaredistribution\\scanfile\\1c12ff7e-4fac-4a20-93e2-69d221556355\\package45.cab',
     'c:\\windows\\softwaredistribution\\download\\5a72bab1dcaa3c7b284a85a14fbc355d\\amd64_microsoft-windows-grouppolicy-base_31bf3856ad364e35_6.1.7601.23452_none_86a5927916e2461a',
     'c:\\windows\\softwaredistribution\\download\\4765b938a42606618ff5fe4b01259d93\\package_65_for_kb3153731~31bf3856ad364e35~x86~~6.1.1.0.mum',
     'c:\\windows\\softwaredistribution\\scanfile\\cfc52a4d-0734-4579-9033-040bddd82736\\package38.cab',
     'c:\\windows\\softwaredistribution\\scanfile\\6d78b994-9345-4351-857f-1fb201cad9ba\\package12.cab',
     'c:\\windows\\softwaredistribution\\download\\e0d35359863de1965b16bbcb601bc2a0\\windows6.1-kb3156016-x86.psf.cix.xml',
     'c:\\windows\\softwaredistribution\\scanfile\\1d1a07f0-e4e8-4216-be6f-838dc91e4652\\files\\d1',
     'c:\\windows\\softwaredistribution\\wuredir\\7971f918-a847-4430-9279-4a52d1efe18d\\tmp38fd.tmp',
     'c:\\windows\\softwaredistribution\\wuredir\\9482f4b4-e343-43b6-b170-9a65bc822c77\\tmp2fb6.tmp',
     'c:\\windows\\softwaredistribution\\download\\40da6b02af752cdf8b76e39b81c57e26\\package_444_for_kb3161608~31bf3856ad364e35~amd64~~6.1.1.1.cat',
     'c:\\windows\\softwaredistribution\\download\\55290fd6a3000889376b619cfd805f14\\x86_f9e53a4853f120ed1727d8fb6ad9ad8a_31bf3856ad364e35_6.1.7601.23390_none_032e81b33dff055d.manifest',
     # 'c:\\windows\\system32\\config\\systemprofile\\appdata\\locallow\\microsoft\\cryptneturlcache\\metadata\\1bb09beec155258835c193a7aa85aa5b_4183cafd68d28a371063c955101e5f47'
     # '\\\\corpfilerb.ixtelecom.com$nocsc$\\profile\\mvenino.v2\\imremote\\icons\\206.20.208.97\\prf2bb8.tmp',
     # '\\\\corpfilerb.ixtelecom.com$nocsc$\\profile\\mvenino.v2\\appdata\\roaming\\microsoft\\windows\\recent\\customdestinations\\prfab3.tmp',
     # '\\\\corpfilerb.ixtelecom.com$nocsc$\\profile\\mvenino.v2\\imremote\\prf171a.tmp',
     # '\\\\corpfilerb.ixtelecom.com$nocsc$\\profile\\mvenino.v2\\imremote\\icons\\206.20.208.97\\prfbbd.tmp',
     # '\\\\corpfilerb.ixtelecom.com$nocsc$\\profile\\mvenino.v2\\appdata\\roaming\\microsoft\\office\\recent\\intraday_4psa_platinum_location_cost_report_(sca_bqy)-20160715.lnk',
     # '\\\\corpfilerb.ixtelecom.com$nocsc$\\profile\\mvenino.v2\\appdata\\roaming\\microsoft\\excel\\mymso_test_accounts_12-30-15%20(21)305282773652672114',
     # '\\\\corpfilerb.ixtelecom.com$nocsc$\\profile\\mvenino.v2\\appdata\\roaming\\microsoft\\excel\\mymso_test_accounts_12-30-15%20(21)305282773652672114\\mymso_test_accounts_12-30-15%20(21)((unsaved-305282441386282882)).xlsb',
     # '\\\\corpfilerb.ixtelecom.com$nocsc$\\profile\\mvenino.v2\\appdata\\roaming\\microsoft\\excel\\mymso_test_accounts_12-30-15%20(21)305282773652672114\\prfa7a2.tmp',
     # '\\\\corpfilerb.ixtelecom.com$nocsc$\\profile\\mvenino.v2\\appdata\\roaming\\microsoft\\excel\\mymso_test_accounts_12-30-15%20(21)305282773652672114\\prfa7a1.tmp',
     # '\\\\corpfilerb.ixtelecom.com$nocsc$\\profile\\mvenino.v2\\appdata\\roaming\\microsoft\\excel\\mymso_test_accounts_12-30-15%20(21)305282773652672114\\mymso_test_accounts_12-30-15%20(21).xlsx.lnk',
           ]
    #f_in = open(r'/Users/smeir/secdo/data-model/vertica_extractor/50M_svchost_paths.csv', 'r').readlines()

    f_in = open(svchost, 'r').readlines()
    paths = set()
    paths = []
    for e in f_in:
        # Multiple commas forces us to use this method to extract the file names from the service name
        paths.append(extract_path_from_csv_line(e))

    canon_paths = set([])
    canon_paths = []
    for path in paths:
        c_p = canonicalize_path(path)
        # print c_p
        # canon_paths |= set([c_p])
        canon_paths.append(c_p)
    buckets, card_bukets = create_path_buckets(canon_paths, path_sep)
    for key in buckets:
        logging.info("bucket[%s] = %r", key, buckets[key])

    # similarities = find_inner_similarities(buckets)
    # print similarities
    # similar_paths = find_largest_similar_tree(similarities)
    # print similar_paths

    logging.info('='*100)
    # print similarities
    similarities = build_inner_similarities_with_cardinality_threshold(buckets)
    for key in similarities.keys():
        logging.info("len(similarities[%s] = %d", key, len(similarities[key]))
        for k in similarities[key]:
            logging.info('    %s, %s', k, similarities[key][k])
        logging.info("")

def main():
    parser=argparse.ArgumentParser()
    
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-t", "--test", action="store_true",help="use test rountine (non-pyspark)")
    group.add_argument("-l", "--limit", default=None, type=int, help="how many results should be taken when using spark")

    parser.add_argument("-o", "--output-csv", default=None, help="store results as CSV in an output file")
    parser.add_argument("-v", "--verbose", action="store_true",help="display verbose debug info")
    parser.add_argument("--trunc", action="store_true", help="truncate output to fit nicely into table")
    parser.add_argument("svchost", help="svchost paths CSV file")
    args = parser.parse_args()

    logging.basicConfig(format=("\n" if args.verbose else "") + "%(levelname)s: %(message)s", level=logging.DEBUG if args.verbose else logging.INFO)

    if args.test:
        test(args.svchost)
    else:
        # process_with_spark_unused(args.svchost, args.limit, args.trunc)
        process_with_spark(args.svchost, args.output_csv, args.limit, args.trunc)

if __name__=="__main__":
    main()

