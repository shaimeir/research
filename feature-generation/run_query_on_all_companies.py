#!/bin/python
import subprocess
import os
import sys
from hashlib import md5

# Note, company preceded with an r (raw string) are known to have malware executions
companies_list = {'testeu' :
					  ['lab',],

	'cloud' : [
			'5ltech',
			'cahill',
			'checkpoint',
			'Check Point',
			'ciphertecks',
			'CipherTechs',
			'ciphertecs_poc',
			'demo',
            r'digitrust',
			'dlsg',
			'egged',
			'foresite',
			'glesec',
			'gruss',
			'hitachi',
            r'idt-corp',
			'lab',
			'leumi',
			'metapacket',
			'mock',
			'preciseleads',
            r'ruitingtech',
			's3',
            r'safesystems',
			'secdo',
			'sertv',
			'stagingtest',
			'stagingtest2',
			'stgeorgesbank',
			'tcs',
			'telnetworks',
			'ten10',
			'unknown_company',
		],
    'cloudeu' :
            [
            r'2bsecure',
            'ciphertechs',
            'cygnia',
            r'egged',
            'fibi',
            'FIBI',
            'intel',
            'lab',
            'longsight',
            r'msh',
            r'payoneer',
            r'poalim',
            'preciseleads',
            'raf',
            'rafael',
            'russell',
            'sattrix',
            'secdo',
            'secoz',
            'ten10',
            'test',
            r'trustnet',
            r'ubi',
            r'xnes',
            'Default',
            ]
}

active_company = 'cloud'
companies = [(company, md5(company).hexdigest()) for company in companies_list[active_company]]
delim = '	'
output_folder = '/vertica/data/private_beta/query/'
output_filename = 'sds_cloud_svchost_file_reg.csv'
dbuser = 'dbadmin'
password = 'secdo1337'
resource_pool = '' if '' == active_company else '; set resource_pool = quick_queries'


# Get all file and registry events of svchost
query = "select process_instance.hostId, process_instance.secdoProcessId, process_instance.instanceId, (EXTRACT(EPOCH FROM process_instance.timestamp)*1000)::INT as timestamp, " \
		"(EXTRACT(EPOCH FROM process_instance.endDate)*1000)::INT as endDate, process_instance.cmd, process_instance.processPath, process_instance.username," \
		"process_instance.sessionUser, process_instance.sessionId,process_instance.sessionInstanceId, process_instance.parentFullFilePath, process_instance.parentInstanceId, processSigned, processSignedValid," \
		"processSignedCompanyName, processSignedProductName, processHash, process_instance.pid, event_stream.threadId, event_stream.eventType, event_stream.filePath, event_stream.userpresence," \
		"event_stream.fileAccessType, event_stream.filesize, event_stream.fileextension, event_stream.networkaccesstype, event_stream.localipv4, event_stream.localport, event_stream.proxy," \
		"event_stream.remoteipv4, event_stream.remotePort, event_stream.remoterealport, event_stream.remoteRealHostname, event_stream.country, event_stream.connectionClosed," \
		" event_stream.smbUsername, event_stream.download, event_stream.upload, event_stream.keyName, event_stream.keyValue, event_stream.valueType, event_stream.deleted, event_stream.groupType," \
		"event_stream.moduleAccessType, event_stream.moduleMemSize, event_stream.moduleBaseAddr, event_stream.injectedPid, event_stream.injectedTid, event_stream.injectedFullFilePath, " \
		"event_stream.injectedFullFilePathId, event_stream.injectedInstanceId, event_stream.injectionType, event_stream.secdoChildId, event_stream.secdoChildInstanceId, event_stream.childFilePath, " \
		"event_stream.childPid from process_instance right outer join event_stream ON process_instance.hostId = event_stream.hostId and process_instance.secdoProcessId = event_stream.secdoProcessId " \
		"and process_instance.instanceId = event_stream.instanceId where  process_instance.endDate is not NULL and " \
		"(" \
		"(event_stream.eventType in ('file_system', 'registry') and REGEXP_LIKE (process_instance.processPath, '.*\\\\\\\\svchost\.exe', 'i')) " \
		"OR " \
		"(event_stream.eventType = 'executed_by' and REGEXP_LIKE (process_instance.parentFullFilePath, '.*\\\\\\\\svchost\.exe', 'i'))) limit 50000000;"

# Find hostids
# query = " select hostid, name, agentversion, interfaces from hosts where lower(agentversion) like '3.1%' limit 100;"

#exec_cmd = ['vsql', '-U', dbuser, '-w', password, '-F', '"'+delim+'"', '-A', '-o', output_folder+output_filename,  '-c', '"set search_path to '+company+' ; set resource_pool = quick_queries; '+query+'"']

if __name__=="__main__":
    for company_name, company_md5 in companies:
        if not company_name == 'idt-corp':
            continue
#        p = subprocess.Popen(exec_cmd)
#        p.communicate()
	try:
		os.makedirs(output_folder)
	except:
		pass
	exec_cmd = 'vsql -U %s  -w %s -F "%s" -A -o %s%s  -c "set search_path to  \'%s\' %s; %s"' % (dbuser, password, delim, output_folder, company_name+'_'+company_md5+'_'+output_filename, company_md5, resource_pool, query)

	print "Going to execute on company \033[91m\033[0m%s:\n%s" %(company_name, exec_cmd)
	os.system(exec_cmd)
        
