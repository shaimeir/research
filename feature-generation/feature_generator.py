#!/usr/local/Cellar/python/2.7.10_2/bin/python
import re
import os
import sys
from pyspark.sql import SparkSession, DataFrame, functions, DataFrameReader


if __name__=="__main__":
	spark = SparkSession.builder.appName("SecdoDataScience").master("local").getOrCreate()
	svchost_cloud = spark.read.csv(path='/Users/smeir/secdo/data-model/vertica_extractor/sds_cloud_svchost_and_children_tab_40M.csv', sep='\t', header=True, inferSchema=True, mode='DROPMALFORMED')
	print svchost_cloud.count()
	print svchost_cloud.schema
	print svchost_cloud.show()
