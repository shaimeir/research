#!/usr/bin/evn python2.7

FileAccessTypes = {
	0 : "FileAccessUnknown",
	1 : "FileAccessOpen",
	2 : "FileAccessWrite",
	3 : "FileAccessCreateNew",
	4 : "FileAccessRename",
	5 : "FileAccessDelete",
	6 : "FileAccessDirMove",
    7 : "FileAccessDirCreateNew",
    8 : "FileAccessDirDelete",
    9 : "FileAccessCopy",
}
