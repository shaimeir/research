import os
from operator import add
from pyspark import SparkContext
from pyspark.sql.functions import col
from pyspark.sql import SQLContext

logFile = '/Users/smeir/spark-2.0.1-bin-hadoop2.7/logs/pyspark_canon.log'
sc = SparkContext("local", "Simple App")
logData = sc.textFile(logFile).cache()

sc.addPyFile('/Users/smeir/secdo/data-model/vertica_extractor/Path_Canonicalizer.py')
os.chdir('/Users/smeir/secdo/data-model/vertica_extractor')
os.path.join('/Users/smeir/secdo/data-model/vertica_extractor')
from Path_Canonicalizer import canonicalize_path

sqlContext = SQLContext(sc)
#svchost_paths = sqlContext.read.format('com.databricks.spark.csv').options(header='false', inferschema='true', delimiter='\t', mode='DROPMALFORMED').load('/Users/smeir/secdo/data-model/vertica_extractor/Paths.txt')
# svchost_paths = sqlContext.read.format('com.databricks.spark.csv').options(header='false', inferschema='true', delimiter=',', mode='DROPMALFORMED').load('/Users/smeir/secdo/data-model/vertica_extractor/50M_svchost_paths/50M_svchost_paths_filepath_cmd_eventtype_fileAccessType_username.csv')
svchost_paths = sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true', delimiter='\t', mode='DROPMALFORMED').load('file:///Users/smeir/secdo/data-model/vertica_extractor/50M_svchost_paths/idt_corp_279c92924a8a09ee60a8fb9810288a14_sds_cloud_svchost_file_activity_TAB.csv')
svchost_paths.createOrReplaceTempView('svchost_paths')
svchost_paths = sqlContext.sql("select * from svchost_paths where ")

# svchost_1M_paths = sqlContext.sql("select filePath, cmd, eventType, fileAccessType, username from svchost_paths limit 1000000")
# svchost_1M_paths.count()
# counted_svchost_paths = svchost_1M_paths.rdd.map(lambda p: (canonicalize_path(p[0]), 1)).toDF()
# counted_svchost_paths = counted_svchost_paths.rdd.reduceByKey(add).toDF()
# counted_svchost_paths.sort(col("_2").desc()).show(truncate=False, n=100)

# svchost_paths = svchost_paths.rdd.map(lambda p: (canonicalize_path(p[0]), 1)).toDF()
# counted_svchost_paths = svchost_paths.rdd.reduceByKey(add).toDF()
# counted_svchost_paths.sort(col("_2").desc()).show(truncate=False, n=100)


# svchost_paths = sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true', delimiter='\t', mode='DROPMALFORMED').load('file:///Users/smeir/secdo/data-model/vertica_extractor/50M_svchost_paths/idt_corp_279c92924a8a09ee60a8fb9810288a14_sds_cloud_svchost_file_activity_TAB.csv')
# svchost_paths.createOrReplaceTempView('svchost_paths')
svchost_paths_to_canon = svchost_paths.rdd.map(lambda p: (canonicalize_path(p[21]), 1)).toDF()
svchost_count_canon_paths = svchost_paths_to_canon.rdd.reduceByKey(add).toDF()
svchost_count_canon_paths.createOrReplaceTempView("svchost_count_canon_paths")
sorted_svchost_count_canon_paths = svchost_count_canon_paths.sort(col("_2").desc())
sorted_svchost_count_canon_paths.createOrReplaceTempView("sorted_svchost_count_canon_paths")
svchost_paths_to_canon_paths = svchost_paths.rdd.map(lambda p: (p[0], canonicalize_path(p[0]))).toDF()


sc.stop()