#
#	Will hold the tables to be created or referenced to
#
TABLES = {}
TEMP_TABLES = {}

INTERESTING_PROCESSES_FULL_PATHS_RE = [
# (r'c:\\windows\\(?:system32|syswow64)\\notepad.exe', 'i'),
(r'c:\\windows\\(?:system32|syswow64)\\svchost.exe', 'i'),
(r'c:\\Program Files(?: \(x86\))\\Google\\Chrome\\Application\\chrome.exe', 'i')
]

#
#	Note that it is a list because we need tho keep the order
#
# Map from Vertica columns to MySQL columns for process events
process_instance_columns_map = [
  {'hostId' : 'hostId'},
  {'secdoProcessId' : 'secdoProcessId'},
  {'instanceId' : 'instanceID'},
  {'EXTRACT(EPOCH FROM timestamp)::INT*1000 as timestamp' : 'ProcessEvtStart'},
  {'EXTRACT(EPOCH FROM endDate)::INT*1000 as endDate' : 'ProcessEvtEnd'},
  {'cmd' : 'cmd'},
  {'processPath' : 'processPath'},
  {'username' : 'username'},
  {'sessionUser' : 'sessionUser'},
  {'sessionId' : 'sessionId'},
  {'sessionInstanceId' : 'sessionInstanceId'},
  {'parentFullFilePath' : 'parentFullFilePath'},
  {'parentInstanceId' : 'parentInstanceId'},
  {'processSigned' : 'processSigned'},
  {'processSignedValid' : 'processSignedValid'},
  {'processSignedCompanyName' : 'processSignedCompanyName'},
  {'processSignedProductName' : 'processSignedProductName'},
  {'processHash' : 'processHash'},
  {'pid' : 'PID'},
  ]

# Generic columns for all evetns
event_generic = [
{'eventType' : 'eventType'},
{'filePath' : 'filePath'},
{'userpresence' : 'userPresence'},
]

# Map from Vertica columns to MySQL columns for file events
file_system_columns_map = event_generic + [
{'fileAccessType' : 'fileAccessType'},
{'filesize' : 'fileSize'},
{'fileextension' : 'fileextension'},
]

# Map from Vertica columns to MySQL columns for network events
network_columns_map = event_generic + [
{'networkaccesstype' : 'networkaccesstype'},
{'localipv4' : 'localipv4'},
{'localport' : 'localport'},
{'proxy' : 'proxy'},
{'remoteipv4' : 'networkRemoteipv4'},
{'remotePort' : 'networkRemotePort'},
{'remoterealport' : 'remoteRealPort'},
{'smbUsername' : 'smbUsername'},
{'download' : 'download'},
{'upload' : 'upload'},

]

# Map from Vertica columns to MySQL columns for registry events
registry_columns_map = event_generic + [
{'keyName' : 'keyName'},
{'keyValue' : 'keyValue'},
{'valueType' : 'valueType'},
{'deleted' : 'deleted'},
{'groupType' : 'groupType'}
]

# Map from Vertica columns to MySQL columns for process loaded modules events 
modules_columns_map = event_generic + [
{'moduleAccessType' : 'moduleAccessType'},
{'moduleMemSize' : 'moduleMemSize'},
{'moduleBaseAddr' : 'moduleBaseAddress'},
]

# Map from Vertica columns to MySQL columns for injection events 
injection_columns_map = event_generic + [
{'injectedPid' : 'injectedPid'},
{'injectedTid' : 'injectedTid'},
{'injectedFullFilePath' : 'injectedFullFilePath'},
{'injectedFullFilePathId' : 'injectedFullFilePathId'},
{'injectedInstanceId' : 'injectedInstanceId'},
{'injectionType' : 'injectionType'},
]

executed_by_columns_map = event_generic + [
{'secdoChildId' : 'secdoChildId'},
{'secdoChildInstanceId' : 'secdoChildInstanceId'},
{'childFilePath' : 'childFilePath'},
{'childPid' : 'childPid'},
]
# Map from Vertica columns to MySQL columns for stop process events (children of the current process)
stopproces_child_columns_map = []
# Map from Vertica columns to MySQL columns for process events (children of the current process)
process_child_columns_map = []
# Map from Vertica columns to MySQL columns for process performance events (children of the current process)
performance_columns_map = []
# Map from Vertica columns to MySQL columns for startup events (children of the current process)
startup_columns_map = []
# Map from Vertica columns to MySQL columns for tasksched events (children of the current process)
tasksched_child_columns_map = []
# Map from Vertica columns to MySQL columns for window activity events (children of the current process)
windowactivity_child_columns_map = []
# Map from Vertica columns to MySQL columns for suspicious behavior events (children of the current process)
suspicious_behavior_columns_map = []



PROCESS_EVENTS_COLUMNS = """
            hostId VARCHAR(255) NOT NULL,
            secdoProcessId VARCHAR(40) NOT NULL,
            instanceID INT SIGNED,
            ProcessEvtStart BIGINT UNSIGNED,
            ProcessEvtEnd BIGINT UNSIGNED,
            cmd TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci, 
            processPath TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci,
            userName VARCHAR(255),
            sessionUser VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
            sessionId INT UNSIGNED,
            sessionInstanceId INT SIGNED,
            parentFullFilePath TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci,
            parentInstanceId INT SIGNED,
            processSigned BIT,
            processSignedValid BIT,
            processSignedCompanyName VARCHAR(255),
            processSignedProductName VARCHAR(255) CHARACTER SET utf8,
            processHash VARCHAR(40),
            PID INT UNSIGNED,
            
            /* Event Generic */

            eventType VARCHAR(255),
            filePath TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci,
			userpresence BIT,
            
            /* File System Events */
            
            fileAccessType TINYINT,
            filesize BIGINT UNSIGNED,
            fileextension VARCHAR(255),
            
            /* Network Events */
            
            networkAccessType VARCHAR(2),
            localipv4 VARCHAR(15),
            localport VARCHAR(5),
            proxy VARCHAR(255),
            networkRemoteipv4 VARCHAR(15),
            networkRemotePort VARCHAR(5),
            remoteRealPort VARCHAR(5),
            smbUsername VARCHAR(255),
            download BIGINT UNSIGNED,
            upload BIGINT UNSIGNED,
            
            /* Registry Events */
            
            keyName TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci,
            keyValue VARCHAR(255),
            valueType VARCHAR(2),
            deleted BIT,
            groupType VARCHAR(255),
            
            /* Modules events*/

			moduleAccessType TINYINT UNSIGNED,
			moduleMemSize BIGINT UNSIGNED,
			moduleBaseAddress BIGINT UNSIGNED,
			
			/* Code Injection events*/
			
			injectedPid INT UNSIGNED,
			injectedTid INT UNSIGNED,
			injectedFullFilePath TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci,
			injectedFullFilePathId TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci,
			injectedInstanceId INT SIGNED,
			injectionType TINYINT UNSIGNED,

			/* Executed By events */

			secdoChildId VARCHAR(40),
			secdoChildInstanceId INT SIGNED,
			childFilePath TEXT  CHARACTER SET utf8 COLLATE utf8_unicode_ci,
			childPid INT UNSIGNED
"""
#               
#             ,CONSTRAINT pk_host_spid_iid PRIMARY KEY (hostId,secdoProcessId,instanceId, fileEvtTime, networkEvtTime, registryEvtTime)
TABLES ['PROCESS_EVENTS'] = (False, PROCESS_EVENTS_COLUMNS)

TEMP_TABLES ['PROCESS_EVENTS_tmp'] = (True, PROCESS_EVENTS_COLUMNS)

def get_value_type(v):
  try:
    if v is None:
      return '%s'
    else:
      return "%s"
      try:
        i = int(v)
        # It's a number...
        if type(v) in [unicode, str]:
          if '.' in v:
            return "'%f'"
          else:
            return "'%d'"
        elif type(v) in [float]:
          return "%f"
        return "%d"
      except Exception, e:
        print e
        return "'%s'"
  except Exception, e:
    print e
    return "'%s'"



# FILE_EVENT_ROW = 
# FILE_EVENT_VALUES = get_value_type()
