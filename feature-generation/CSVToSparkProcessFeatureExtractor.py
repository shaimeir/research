    IGNORE_ERRORS = True
verbose = False

class SparkToObjectTypeException(Exception):
    pass

class process_features(object):
    def __init__(self, row):
        if not isinstance(row, pyspark.sql.types.Row):
            error =  "Expected 'pyspark.sql.types.Row' type, got %s" % (type(row))
            print error
            raise SparkToObjectTypeException(error) 
        self.process_name = row['processPath'][row['processPath'].rfind('\\')+1:]
        self.process_path = row['processPath']
        self.process_start_time = row['timestamp']
        self.process_end_time = row['endDate']
        self.process_cmd_line = row['cmd']
        self.process_signed = row['processSigned']
        self.userName = row['username']
        self.session_user = row['sessionUser']
        self.session_id = row['sessionId']
        self.session_instance_id = row['sessionInstanceId']
        self.parent_fullFile_path = row['parentFullFilePath']
        self.parent_instance_id = row['parentInstanceId']
        self.processs_signedValid = row['processSignedValid']
        self.processs_signedCompanyName = row['processSignedCompanyName']
        self.processs_signedProductName = row['processSignedProductName']
        self.processs_hash = row['processHash']
        self.pid = row['pid']
        self.thread_id = row['threadId']
        self.event_type = row['eventType']
        self.file_path = row['filePath']
        self.user_presence = row['userpresence']
        self.file_access_type = row['fileAccessType']
        self.file_size = row['filesize']
        self.file_extension = row['fileextension']
        self.networkAccessType = row['networkaccesstype']
        self.localipv4 = row['localipv4']
        self.local_port = row['localport']
        self.proxy = row['proxy']
        self.remoteipv4 = row['remoteipv4']
        self.remotePort = row['remotePort']
        self.remote_real_port = row['remoterealport']
        self.remote_real_hostname = row['remoteRealHostname']
        self.smb_username = row['smbUsername']
        self.download = row['download']
        self.upload = row['upload']
        self.key_name = row['keyName']
        self.key_value = row['keyValue']
        self.valueType = row['valueType']
        self.deleted = row['deleted']
        self.group_type = row['groupType']
        self.module_access_type = row['moduleAccessType']
        self.module_mem_size = row['moduleMemSize']
        self.module_base_address = row['moduleBaseAddr']
        self.injected_pid = row['injectedPid']
        self.injected_tid = row['injectedTid']
        self.injected_full_file_path = row['injectedFullFilePath']
        self.injected_full_file_path_id = row['injectedFullFilePathId']
        self.injected_instance_id = row['injectedInstanceId']
        self.injection_type = row['injectionType']
        self.secdo_child_id = row['secdoChildId']
        self.secdo_child_instance_id = row['secdoChildInstanceId']
        self.child_file_path = row['childFilePath']
        self.child_pid = row['childPid']

class process_class_features(object):
    def __init__(self, processes_list):
        if not isinstance(processes_list, list):
            error = "Expected a list, got %s" % (type(processes_list))
            raise SparkToObjectTypeException(error)
        if not isinstance(processes_list[0], pyspark.sql.types.Row):
            error = "Expected 'pyspark.sql.types.Row' type, got %s" % (type(processes_list[0]))
            raise SparkToObjectTypeException(error)
        self.processes_list = []
        for process in processes_list:
            try:
                self.processes_list.append(process_features(process))
            except Exception, e:
                if verbose:
                    print e
                if not IGNORE_ERRORS:
                    raise e
                pass
        print "Successfully extracted %d processes from given list." % len(self.processes_list)

class svchost_features(process_class_features):
    # Expected the processes_list to contain processes from the same family type. Note that in the case of svchost we
    # are looking at sub-types according to the command line svchost was executed with (or module that it loads, see
    # later which works best...)
    def __init__(self, processes_list, filter = None):
        super(svchost_features, self).__init__()
        self.filter = filter
        self.WINDOW = 10 * 1000 # milliseconds
        self.net_avg_conn_per_tm_window = None
        self.net_avg_conn_up_per_type_per_tm_window = None
        self.net_avg_conn_down_per_type_per_tm_window = None
        self.net_domain_in_A1M = None
        self.net_num_new_conn_per_tm_window = None
        self.net_num_failed_conn_per_tm_window = None
        self.net_domain_looks_like_dga = None
        self.net_avg_failed_dns_req_per_tm_window = None
        self.net_longest_conn_duration = None
        self.net_listen_port_type = None
        self.net_is_remote_domain_proxy = None
        self.is_svc_local_service = None # process user is NT AUTHORITY\\LOCAL SERVICE
        self.net_has_network_activity = None
        self.__process_data()
    def __process_data(self):
        for proc in self.processes_list:
# print "Collecting remote hosts and port number for %s" % (svc_with_net_name)
svc_net_hist = {}
svchost_with_network = []

for row in svchost_with_network:
    svcType = row['substring(cmd, locate(svchost, cmd, 1), 2147483647)'].replace('"', '')
    hostId = row['hostId']
    secdoProcessId = row['secdoProcessId']
    instanceId = row['instanceId']
    is_svc_instance_in_done_list = sqlContext.sql("""select count(*) from done_unique_svc_inst where hostId='%s' and secdoProcessId = '%s' and instanceId = '%s' and svcType = '%s'""" % (hostId, secdoProcessId, instanceId, svcType)).collect()[0]
    if 0 == is_svc_instance_in_done_list:
        sqlContext.sql("""insert into  done_unique_svc_inst values(hostId='%s', secdoProcessId = '%s', instanceId = '%d', svcType = '%s')""" % (hostId, secdoProcessId, instanceId, svcType))
    all_svc_events = sqlContext.sql("""select * from cloud_svchost where hostId='%s' and secdoProcessId='%s' and instanceId = '%d'""").collect()
    # Start creating features from the data collected

    if svc_name in svchost_with_network_hist:
        svc_net_hist.setdefault(svc_name, {})
        remote_hosts_ports = svc_net_hist[svc_name]
        domain_name = extract_domain(row['remoterealhostname'])
        remote_hosts_ports.setdefault(domain_name, set([]))
        # if row['remotePort'] not in remote_hosts_ports[domain_name]:
        remote_hosts_ports[domain_name] = remote_hosts_ports[domain_name].union(set([row['remotePort']]))
        svc_net_hist[svc_name] = remote_hosts_ports



class process_features(object):
    def __init__(self, processName, hostId, secdoProcessId, instanceId, timestamp, endDate, username, accesstype, eventtype, upload, download, number_of_instances):
        self.processName = processName.strip()
        self.hostId = hostId.strip()
        self.secdoProcessId = secdoProcessId.strip()
        self.instanceId = int(instanceId.strip())
        self.timestamp = int(timestamp.strip())
        self.endDate = int(endDate.strip())
        self.username = username.strip()
        self.accessType = int(-1 if "" == accesstype.strip() else accesstype.strip())
        self.eventType = eventtype.strip()
        self.upload = int(upload.strip())
        self.download = int(download.strip())
        self.number_of_instances = int(number_of_instances.strip()[:-2])
    def __str__(self):
        s = "Process Name: {processName}\n".format(processName=self.processName)
        s += "Host ID: {hostId}\n".format(hostId=self.hostId)
        s += "Secdo Process ID: {secdoProcessId}\n".format(secdoProcessId=self.secdoProcessId)
        s += "Instance ID: {instanceId}\n".format(instanceId=self.instanceId)
        s += "Start time: {timestamp}\n".format(timestamp=self.timestamp)
        s += "End time: {endDate}\n".format(endDate=self.endDate)
        s += "Username: {username}\n".format(username=self.username)
        s += "Access Type: {accessType}\n".format(accessType=self.accessType)
        s += "Event Type: {eventType}\n".format(eventType=self.eventType)
        s += "Upload: {upload}\n".format(upload=self.upload)
        s += "Download: {download}\n".format(download=self.download)
        s += "Total Instances: {number_of_instances}\n".format(number_of_instances=self.number_of_instances)
        return s
    def get_csv(self):
        return ','.join([self.processName, self.hostId, str(self.secdoProcessId), str(self.instanceId), str(self.timestamp), \
                         str(self.endDate), self.username, str(self.accessType), self.eventType, str(self.upload), str(self.download), str(self.number_of_instances), '\n'])

svchost_collection = {}

#for l in open(r'/Users/smeir/spark-2.0.1-bin-hadoop2.7/~/secdo/data-model/vertica-extractor/test.csv/part-r-00000-996eff90-0579-404d-8665-11dfb5ca275e.csv').readlines():

for l in open(r'/Users/smeir/spark-2.0.1-bin-hadoop2.7/~/secdo/data-model/vertica-extractor/test3.csv/part-r-00000-bc84d76e-4119-4f35-b542-45104991ba76.csv').readlines():
    try:
        svchost_instance, upload, download, count = l.split(',')
        print svchost_instance.split('~%~')
        hostid, secdoprocessid,instanceid,timestamp,endDate,svchosttype,username,eventtype,netaccesstype = svchost_instance.split('~%~')
        svchost_collection.setdefault(svchosttype, [])
        svchost_collection[svchosttype].append(process_features(svchosttype,hostid, secdoprocessid,instanceid,timestamp,endDate,username,netaccesstype,eventtype,upload,download,count))
    except Exception, e:
        print svchost_instance
        raise e