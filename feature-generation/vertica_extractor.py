#!//usr/local/Cellar/python/2.7.10_2/bin/python
from vertica_python import connect
import string
import hashlib
from data_set_definition import INTERESTING_PROCESSES_FULL_PATHS_RE, process_instance_columns_map, \
file_system_columns_map, network_columns_map, registry_columns_map, modules_columns_map, injection_columns_map, event_generic, executed_by_columns_map, \
TABLES, TEMP_TABLES, PROCESS_EVENTS_COLUMNS, get_value_type
import datetime, time
import mysql.connector
import sys
import logging
#Open/Create database

"""
  Install MySQL database and MySQL connector package for Python. Change the default root password. After that run the below to create the initial databse and the user.
  Alternatively import the databse from a previously generateed sql file.
  
  @~@~@~@~@~@~@~@~@~@~@~@~            SDS ~ SecdoDataScience               @~@~@~@~@~@~@~@~@~@~@~@~
  
  CREATE DATABASE SDS;
  USE SDS;
  CREATE USER 'sds'@'localhost' identified by '1234';
  ALTER USER 'sds'@'localhost' password expire never;
  GRANT ALL PRIVILEGES ON *.* to 'sds'@'localhost';
"""
logging.basicConfig(filename='SDS_Verticata_to_MySQL_Importer.log')


# The table will be an event type table
mysql_conn = mysql.connector.connect(user='sds', password='1234', host='localhost', database='SDS')
print "Got connection to database..."
mysql_cusror = mysql_conn.cursor()
print "Got a cursor for the connection..."



def create_bootstrap_table(table_name, columns, is_temp):
  try:
    stmt = """
            CREATE{is_temp}TABLE IF NOT EXISTS  `{table_name}` ( {columns} )
          """.format(is_temp=' TEMPORARY ' if is_temp else ' ', table_name=table_name, columns=columns)
    mysql_cusror.execute(stmt)
  except Exception, e:
    logging.exception( "Got exception %s during table bootstrap.\nSql statement%s" % (e, stmt))
    print stmt
    sys.exit(-1)

for table_name, table_data in TABLES.iteritems():
  is_temp, columns = table_data
  create_bootstrap_table(table_name, columns, is_temp)
for table_name, table_data in TEMP_TABLES.iteritems():
  is_temp, columns = table_data
  create_bootstrap_table(table_name, columns, is_temp)

#Vertica connection
conn_dict = {
                 'host': 'testeu.pod03.secdo.net', 
                    'port': 5433, 
                    'user': 'dbadmin', 
                    'password': 'secdo1337', 
                    'database': 'private_beta'
    }
     
vertica_conn = connect(**conn_dict)
vertica_cursor = vertica_conn.cursor('dict')    
company = 'lab'
schema = hashlib.md5(company).hexdigest()
internal_conn = connect(**conn_dict)
internal_cursor = internal_conn.cursor('dict')


proc_instances_statement_last_24h = """
                                        SELECT {columns} FROM "{schema}".process_instance where REGEXP_LIKE (processPath, '{interesting_process_re}', '{re_modifiers}') and endDate is not NULL limit 10000;
                                    """

file_system_events_per_instance_stmt = """
                                        SELECT {columns} FROM "{schema}".event_stream where eventType='file_system' and hostId='{hostId}' and secdoProcessId='{secdoProcessId}' and instanceId={instanceId} ;
                                    """
network_connections_per_instance_stmt = """
                                        SELECT {columns} FROM "{schema}".event_stream where eventType='network' and hostId='{hostId}' and secdoProcessId='{secdoProcessId}' and instanceId={instanceId} ;
                                        """
registry_events_stmt_instance = """
                                        SELECT {columns} FROM "{schema}".event_stream where eventType='registry_list' and hostId='{hostId}' and secdoProcessId='{secdoProcessId}' and instanceId={instanceId} ;
                                  """

module_events_stmt_instance = """
                                        SELECT {columns} FROM "{schema}".event_stream where eventType='module' and hostId='{hostId}' and secdoProcessId='{secdoProcessId}' and instanceId={instanceId} ;
                                  """
code_injection_events_stmt_instance = """
                                        SELECT {columns} FROM "{schema}".event_stream where eventType='code_injection' and hostId='{hostId}' and secdoProcessId='{secdoProcessId}' and instanceId={instanceId} ;
                                  """
executed_by_events_stmt_instance = """
                                        SELECT {columns} FROM "{schema}".event_stream where eventType='executed_by' and hostId='{hostId}' and secdoProcessId='{secdoProcessId}' and instanceId={instanceId} ;
                                  """


all_events_one_query = """
      SELECT {columns} FROM "{schema}".event_stream where hostId='{hostId}' and secdoProcessId='{secdoProcessId}' and instanceId={instanceId} and eventType in ('file_system','network','registry_list','module','code_injection','executed_by');
                      """

parents = """
          """
children = """
           """
registered_tasks = """
                  """
startup_fodler =  """
                  """
def mysql_insert(cursor, stmt, values):
  try:
    cursor.execute(stmt, values)
  except Exception, e:
    logging.error( "\n(!) Dropped a database line: Error:\n%s\n" % (e))
    logging.error( values)
    logging.error( stmt)
    pass

def gen_mysql_insert_into_process_event(values, mysql_map_name = process_instance_columns_map):
  mysql_stmt = "INSERT INTO PROCESS_EVENTS (" + ",".join([x.values()[0] for x in mysql_map_name]) + ','\
  + ",".join([x.values()[0] for x in event_generic]) + ','\
  + ",".join([x.values()[0] for x in file_system_columns_map[len(event_generic):]]) + ','\
  + ",".join([x.values()[0] for x in network_columns_map[len(event_generic):]]) + ','\
  + ",".join([x.values()[0] for x in registry_columns_map[len(event_generic):]]) + ','\
  + ",".join([x.values()[0] for x in modules_columns_map[len(event_generic):]]) + ','\
  + ",".join([x.values()[0] for x in injection_columns_map[len(event_generic):]]) + ','\
  + ",".join([x.values()[0] for x in executed_by_columns_map[len(event_generic):]]) \
   + ") VALUES (" + ",".join(['%s'] * len(values)) + ")"
  return mysql_stmt

#AND timestamp >= NOW - '24 hours'::INTERVAL
for  interesting_process_re, re_modifiers in INTERESTING_PROCESSES_FULL_PATHS_RE:

  stmt = proc_instances_statement_last_24h.format(columns=", ".join([x.keys()[0] for x in process_instance_columns_map]), schema=schema, interesting_process_re=interesting_process_re, re_modifiers=re_modifiers)
  vertica_cursor.execute(stmt)
  number_of_process_instances = 0
  for row in vertica_cursor.iterate():
      print row['hostId'], row['processPath'], row['instanceId'], row['parentFullFilePath']
      number_of_process_instances += 1
      PROCESS_STATIC = [row[x.keys()[0]] for x in process_instance_columns_map[:3]] + [row['timestamp']] + [row['endDate']] + [row[x.keys()[0]] for x in process_instance_columns_map[5:]]
      #######################
      ### File Events
      #######################
      print 'Extracting file events...'
      internal_cursor.execute(file_system_events_per_instance_stmt.format(columns=", ".join([x.keys()[0] for x in file_system_columns_map]), schema=schema, hostId=row['hostId'], secdoProcessId=row['secdoProcessId'], instanceId=row['instanceId']))
      for internal_row in internal_cursor.iterate():
          print '\t', internal_row['filePath']
          FILE_EVENT_ROW = PROCESS_STATIC + \
            [
            internal_row[x.keys()[0]] for x in event_generic
            ] + \
            [ 
            internal_row[x.keys()[0]] for x in file_system_columns_map[len(event_generic):]
            ] + \
            [None] * len(network_columns_map[len(event_generic):]) + \
            [None] * len(registry_columns_map[len(event_generic):]) + \
            [None] * len(modules_columns_map[len(event_generic):]) + \
            [None] * len(injection_columns_map[len(event_generic):]) + \
            [None] * len(executed_by_columns_map[len(event_generic):])
          # Push to MySQL
          mysql_stmt = gen_mysql_insert_into_process_event(FILE_EVENT_ROW)
          mysql_insert(mysql_cusror, mysql_stmt, FILE_EVENT_ROW)
      #######################
      ### Network Events
      #######################
      print 'Extracting network events...'
      internal_cursor.execute(network_connections_per_instance_stmt.format(columns=", ".join([x.keys()[0] for x in network_columns_map]), schema=schema, hostId=row['hostId'], secdoProcessId=row['secdoProcessId'], instanceId=row['instanceId']))
      for internal_row in internal_cursor.iterate():
          print '\t', internal_row['remoteipv4']
          NETWORK_EVENT_ROW = PROCESS_STATIC + \
            [
            internal_row[x.keys()[0]] for x in event_generic
            ] + \
            [None] * len(file_system_columns_map[len(event_generic):]) + \
            [ 
            internal_row[x.keys()[0]] for x in network_columns_map[len(event_generic):]
            ] + \
            [None] * len(registry_columns_map[len(event_generic):]) + \
            [None] * len(modules_columns_map[len(event_generic):]) + \
            [None] * len(injection_columns_map[len(event_generic):]) + \
            [None] * len(executed_by_columns_map[len(event_generic):])
          # Push to MySQL
          mysql_stmt = gen_mysql_insert_into_process_event(NETWORK_EVENT_ROW)
          mysql_insert(mysql_cusror, mysql_stmt, NETWORK_EVENT_ROW)
      #######################
      ### Registry Events
      #######################
      print 'Extracting registry events...'
      internal_cursor.execute(registry_events_stmt_instance.format(columns=", ".join([x.keys()[0] for x in registry_columns_map]), schema=schema, hostId=row['hostId'], secdoProcessId=row['secdoProcessId'], instanceId=row['instanceId']))
      for internal_row in internal_cursor.iterate():
          print '\t', internal_row['keyName']
          REGISTRY_EVENT_ROW = PROCESS_STATIC + \
            [
            internal_row[x.keys()[0]] for x in event_generic
            ] + \
            [None] * len(file_system_columns_map[len(event_generic):]) + \
            [None] * len(network_columns_map[len(event_generic):]) + \
            [
            internal_row[x.keys()[0]] for x in registry_columns_map
            ] +\
            [None] * len(modules_columns_map[len(event_generic):]) + \
            [None] * len(injection_columns_map[len(event_generic):]) + \
            [None] * len(executed_by_columns_map[len(event_generic):])
          # Push to MySQL
          mysql_stmt = gen_mysql_insert_into_process_event(REGISTRY_EVENT_ROW)
          mysql_insert(mysql_cusror, mysql_stmt, REGISTRY_EVENT_ROW)
      #######################
      ### Modules Events
      #######################
      print 'Extracting modules events...'
      internal_cursor.execute(module_events_stmt_instance.format(columns=", ".join([x.keys()[0] for x in modules_columns_map]), schema=schema, hostId=row['hostId'], secdoProcessId=row['secdoProcessId'], instanceId=row['instanceId']))
      for internal_row in internal_cursor.iterate():
          print '\t', internal_row['filePath']
          MODULE_EVENT_ROW = PROCESS_STATIC + \
          [
          internal_row[x.keys()[0]] for x in event_generic
          ] + \
          [None] * len(file_system_columns_map[len(event_generic):]) + \
          [None] * len(network_columns_map[len(event_generic):]) + \
          [None] * len(registry_columns_map[len(event_generic):]) + \
          [
          internal_row[x.keys()[0]] for x in modules_columns_map[len(event_generic):]
          ] + \
          [None] * len(injection_columns_map[len(event_generic):]) + \
          [None] * len(executed_by_columns_map[len(event_generic):])
          # Push to MySQL
          mysql_stmt = gen_mysql_insert_into_process_event(MODULE_EVENT_ROW)
          mysql_insert(mysql_cusror, mysql_stmt, MODULE_EVENT_ROW)
      ##########################
      ### Code Injection Events
      ##########################
      print 'Extracting code injection events...'
      internal_cursor.execute(code_injection_events_stmt_instance.format(columns=", ".join([x.keys()[0] for x in injection_columns_map]), schema=schema, hostId=row['hostId'], secdoProcessId=row['secdoProcessId'], instanceId=row['instanceId']))
      for internal_row in internal_cursor.iterate():
          print '\t', internal_row['injectedFullFilePath']
          CODE_INJECTION_EVENT_ROW = PROCESS_STATIC + \
          [
          internal_row[x.keys()[0]] for x in event_generic
          ] + \
          [None] * len(file_system_columns_map[len(event_generic):]) + \
          [None] * len(network_columns_map[len(event_generic):]) + \
          [None] * len(registry_columns_map[len(event_generic):]) + \
          [None] * len(modules_columns_map[len(event_generic):]) + \
          [
          internal_row[x.keys()[0]] for x in injection_columns_map[len(event_generic):]
          ] + \
          [None] * len(executed_by_columns_map[len(event_generic):])
          # Push to MySQL
          mysql_stmt = gen_mysql_insert_into_process_event(CODE_INJECTION_EVENT_ROW)
          mysql_insert(mysql_cusror, mysql_stmt, CODE_INJECTION_EVENT_ROW)
          # internal_cursor.execute(file_system_events_per_instance_stmt.format(columns=", ".join(file_system_columns_map), schema=schema, hostId=row['hostId'], secdoProcessId=row['secdoProcessId'], instanceId=row['instanceId']))
      ##########################
      ### Executed By Events
      ##########################
      print 'Extracting executed by events...'
      internal_cursor.execute(executed_by_events_stmt_instance.format(columns=", ".join([x.keys()[0] for x in executed_by_columns_map]), schema=schema, hostId=row['hostId'], secdoProcessId=row['secdoProcessId'], instanceId=row['instanceId']))
      for internal_row in internal_cursor.iterate():
          print '\t', internal_row['childFilePath']
          EXECUTED_BY_EVENT_ROW = PROCESS_STATIC + \
          [
          internal_row[x.keys()[0]] for x in event_generic
          ] + \
          [None] * len(file_system_columns_map[len(event_generic):]) + \
          [None] * len(network_columns_map[len(event_generic):]) + \
          [None] * len(registry_columns_map[len(event_generic):]) + \
          [None] * len(modules_columns_map[len(event_generic):]) + \
          [None] * len(injection_columns_map[len(event_generic):]) + \
          [
          internal_row[x.keys()[0]] for x in executed_by_columns_map[len(event_generic):] 
          ]
          # Push to MySQL
          mysql_stmt = gen_mysql_insert_into_process_event(EXECUTED_BY_EVENT_ROW)
          mysql_insert(mysql_cusror, mysql_stmt, EXECUTED_BY_EVENT_ROW)
          # internal_cursor.execute(file_system_events_per_instance_stmt.format(columns=", ".join(file_system_columns_map), schema=schema, hostId=row['hostId'], secdoProcessId=row['secdoProcessId'], instanceId=row['instanceId']))
      print 'Commiting...'  
      mysql_conn.commit()
logging.info("Got back a total of {proc_inst_num}".format(proc_inst_num=number_of_process_instances))
mysql_conn.commit()
mysql_cusror.close()
mysql_conn.close()
vertica_cursor.close
vertica_conn.close
internal_cursor.close()
internal_conn.close()