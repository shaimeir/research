import Crypto.Cipher.AES
import sys
import shutil


def write_encrypted_content(path, result_path, cipher):
    with open(path, 'rb') as fp:
        input_content = fp.read().strip()

    padding_length = 16 - (len(input_content) % 16)
    input_content = input_content + b" " * padding_length
    output_content = cipher.encrypt(input_content)

    with open(result_path, 'wb') as fp:
        fp.write(output_content)


print("WARNING : We assume text files so pad with spaces and remove them with strip in decrypt")
print("WARNING : So this won't work well for binary files")
print("WARNING : Encryption is not very strong, used chained blocks if strong encryption is needed")

encryption_key = "52l7fhn3n349h3q9"
cipher = Crypto.Cipher.AES.new(encryption_key, Crypto.Cipher.AES.MODE_ECB)

if sys.version_info >= (3,5) and sys.version_info < (3,7):
    from pathlib import Path
    encrypted_data_dir = Path("encrypted_data")
    data_dir = Path("data")

    if encrypted_data_dir.exists():
        shutil.rmtree(str(encrypted_data_dir))
    encrypted_data_dir.mkdir()

    for path in data_dir.iterdir():
        result_path = Path(encrypted_data_dir, path.name)
        print(r"Encrypting {0} to {1}".format(str(path), str(result_path)))
        write_encrypted_content(path, result_path, cipher)
elif sys.version_info >= (2,7):
    import os
    if not os.path.isdir(os.path.abspath('encrypted_data')):
        os.makedirs(os.path.abspath('.') + os.path.sep + 'encrypted_data')
    encrypted_data_dir = os.path.abspath('encrypted_data')
    if not os.path.abspath('data'):
        print "Can't find data folder"
        sys.exit(1)
    for f in [x for x in os.listdir(os.path.abspath('data')) if x.lower().endswith('.txt')]:
        target_file = os.path.abspath('encrypted_data') + os.path.sep + f
        print(r"Encrypting {0} to {1}".format(f, target_file))
        write_encrypted_content(os.path.join(os.path.abspath('data'), f), target_file, cipher)
else:
    print ("Need python 2.7, 3.5 or 3.6. Good luck!")
