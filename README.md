#  How to run 



Dump the data from the Payoneer data base [^9] 

	./dump-scheme.py -p -s process -d cloud1.il.payoneer -r -a '2017-09-01' -b '2017-09-15'  | remote -r cloud1.il -f vsql-dump.sh -c payoneer -o /secdo/vertica/research-dumps/payoneer-process-creation-0-14.09.csv.gz
	#Example for IDT 
	./dump-scheme.py -p -s process -d cloud1.us.idt -r -a '2017-09-01' -b '2017-09-15'  | remote -r cloud1.us -f vsql-dump.sh -c idt-corp -o /secdo/vertica/research-dumps/idt-process-creation-0-14.09.csv.gz

Copy the data to the local server from the cloud. Do not forget to limit the bandwidth 

	scp -l 10240 cloud1.il:/secdo/vertica/research-dumps/payoneer-process-creation-0-14.09.csv.gz /secdo2/scratch/payoneer/.
	# For IDT:
	scp -l 10240 cloud1.us:/secdo/vertica/research-dumps/idt-process-creation-0-14.09.csv.gz /secdo2/scratch/idt/.
	
Script dump-scheme.py generates the query. 

	ssh spark@100.64.0.129
	cd projects/research-2/
	git checkout profile-generations
	cd db-utils
	# llh is an alias for ll...
	llh scratch2/idt/
	

Choose a file, for example idt-reg-svchost-regsvr32-12-17.09.csv.gz Process name is always column 1 and the argument can be skipped when calling split-dump-by-process.awk 

	gzip -cd scratch2/idt/idt-reg-svchost-regsvr32-12-17.09.csv.gz |  ./split-dump-by-process.awk --output scratch2/idt/arkady-reg  
	
Provide prevalence -p scratch2/idt/proc-prev-12-18.09.csv

	./generate-standarad-profiles.sh -o ./scratch2/arkady -p scratch2/idt/proc-prev-12-18.09.csv -r scratch2/idt/arkady-reg `ls -1 scratch2/idt/arkady-reg`
	
Results in the  ./scratch2/arkady/

	vi cross-validation-summary	
	vi split-regsvr32.exe/registry-profile-by-* -p
	
The cross-validation-summary file contains data looking like this [^13]:

         percentages summary (mismatches/total instances):
                                   	[process-chain/occurrences]
         ------------------------  -----------------------------	
         	cscript.exe               0.00% (0/37929)
         	explorer.exe              0.00% (0/1042)
         	services.exe              0.00% (0/528)
         	svchost.exe               0.00% (1/35819)
         	wmiprvse.exe              0.00% (0/154318)


# General idea 

Collect file access patterns in the network. Sort the collected data by application and host. See if there are some patterns which tend to repeat themselves. For example ieplorer can write to the cache files with randomly generate names and download folder. These events will be part of the iexplorer "profile". 

When there is a security alert we can check the chain of events just before the alert with the application profile and see if there any significant differences. The goal is to filter some alerts and ultimately to filer more than 99% of alerts.

The framework developed by Ohad does the following:

* Generate SQL queries and dump the data to CSV files
* Generate profiles for filesystem. network, process execution events
* Validation - attempt to apply the generated profile to the out of sample data.

Validation requires more explanation. Let's split the raw data into two blocks 90% and 10%. Collect patterns which appear often [^11] in a "profile" of the executable [^12]. Let's see how many patterns in the 10% sample fit the profile and how many patterns fall outside the profile. We will call it "false positives" - patterns outside of the profile are positives. We assume that the data does not come from contaminated machines (the data is benign). 

We can get data from contaminated machines and see if we are able to generate "true" positives. The goal is to see if the profile of the executable helps to catch security related events.


# Source code tips (Arkady)

Some stats first:

*  ~30K lines mainly in Python: 14K of Python, 950 lines of comments, the rest is whitespace 
*  Created over a period of 8 months starting March 2017
*  Most of the code was written by a single person - Ohad - in the roughly 4 months period at the very impressive 350 lines/day rate (https://dzone.com/articles/programmer-productivity)

A final note: The code was written in the rather aggressive Agile environment.  

In the shell script generate-standard-profiles.sh pay attention to the getopts arguments. For example, switch -F disables cross validation for the file system profile. Option -p sets CSV containing "prevalence" data - more details below. 

Bash tip. The following line checks if the value of the variable process_prevalence_csv is not null

	[ -n "${process_prevalence_csv}" ]
	
The shell script expects list of executables for which the profile is going to be generated. It can be a single name, for example iexplorer.exe. The folders specified by -f options shall contain the data files which names are listed in the script arguments. Order of the folders and order of the script parameters should be the same 

Bash tip. Array of arguments of the script and number of arguments 

	echo args=${@} args-count=${#}

Some options can be provided multiple times and allow to setup lists of directories. For example -f,-n,-r can be used multiple times and will setup array fs_dump_dirs&co. The data files to process depend on the content of arrays/variables process_dump_file, fs_dump_dirs, net_dump_dirs, reg_dump_dirs

Bash tip. The following is size of the array fs_dump_dirs

	${#fs_dump_dirs[@]} 
	

Cross validation - fs_cross_validate&friends - is usually required, unless -F or -S switches are specified. If the cross validation is required the script splits the data file in two sets - a large learning sample and a smaller data set for validation. The idea is that after we generated a profile we can try the profile on a different data set and get sense of a number of false positives [^6]. Switches -1 and -2 in the command below specify two output file names. The resulting filename will look like iexplorer.exe.fs.profile-generation somewhere in the output folder specified by -o switch. The profile generation will use the larger file *profile-generation

	"${sampling_script}" -2 "${profile_generation_data}" -1 "${cross_validation_data}" -i ${program_file} 

Function generate_fs_profile() will execute all required steps for generating of the file system profiles. The read profile[^1] file will be generated on the fly by awk script. The function generate_fs_profile will be called for all (usually *.exe) files in the folders specified by -f,-n,-r (filesystem, network, registry) switches  

Next up is a dense Python script profile-digest.py (the main goal appears to be to type in VIM as few characters as possible) which applies a couple of simple filters to an input CSV and writes the rows to the files *.digest.json (TODO - add an example of diffs). Dany asked Ohad to generate a "readable" profile - something he, Dany could proudly show to Gil/Shai. Dany apparently forgot about the feature?

Finally cross_validate_fs will be called. 

### Generating profile - generate_fs_profile() & friends

Function generate_fs_profile executes csv-stats.py multiple times for generating profiles with a different cut off parameter - number of hosts where a specific file access pattern appears [^2]

The script cvs-stats.py is not for weak hearts. It starts with dynamic script argument list just because we can. Why not indeed? For example, line

	parser.add_argument("-t", "--transformation", default=[], choices=aggregation.data_transformations.choicesList(), action="append", help="transformation to use on the data.\n" + aggregation.data_transformations.doc()) 
	
Another example is class class _local_filter(_Filter) which invokes it's own arguments parser.

will set the choice list depending on the registered methods. You probably wonder how dictionary aggregation.data_transformations and others are getting populated. I will not withhold the truth even for a moment. Pay attention to the method aggregation.make_transformation() and decorators (are you starting to see the light?) like

	@aggregation.make_transformation("col-as-prevalence", _parse_col_as_prevalence_args)
	@aggregation.register_aggregation_factor("fs-canonize")

and others [^3] [^14]. 

You will see in the code functions with the same name and different decorators [^10]. Python is a dynamic language. A declaration of function adds an object to the dictionary of the functions or replace an existing entry in the dictionary. The replacement of the function in the system dictionary happens *after* the function reference was added to one of the dictionaries in the aggregation.py. I suspect that many people will want to read this paragraph more than once.  

While parsing the arguments the script collects parameters. For example, in the samples below -b fs-access-category and -b process-name will be collected in the args.by list. The script will generate a list of objects of type csv_processing.aggregation._AggregationFactorImpl for each entry of -b (aggregate_by) and -a (aggregate_values) switches

TODO it would be nice to be able to drop the -b flags and let the script to guess the fields to aggregate by. It is possible to guess the data - file system events - from the provided CSV file and process name is a natural default.    

Example of execution of the script csv-stats.py:

    scripts_dir="./network-anomalies" 
    fs_prevalence_percent=-5 
    fs_min_occurrences=-20
    csv_stats_script="${scripts_dir}/csv-stats.py"
    	${csv_stats_script} -m -v --serial-transformation \
        -s registry \
        -a reg-canon \
        -b process-name \
        -a host \
        -t csv-to-column \
        -x csv-to-column=$HOME/Downloads/proc-prev-12-18.09.csv \
        -t boolean-entries \
        -t statistics-autogen \
        -x statistics-autogen=${scripts_dir}/random-detection/english-model,1 \
        -t canonize-paths-by-proc-prevalence-v2 \
        -x canonize-paths-by-proc-prevalence-v2="1,-1,${fs_prevalence_percent},${fs_min_occurrences}" \
        -t sum-by-secondary-key \
        -V \
        -o results.csv split-regsvr32.exe
  
Data file to process is something like split-regsvr32.exe from https://secdoit.atlassian.net/secure/attachment/27095/27095_programs.zip

File proc-prev-12-18.09.csv contains "prevalence" data - it is used to get a list of hosts [^4] [^5]. 

For the file system events you use:

        -s file-system \
        -b fs-access-category \
        -a fs-canonize-with-ext-dir \

An example of generating a file system events profile:

    scripts_dir="./network-anomalies" 
    fs_prevalence_percent=-5 
    fs_min_occurrences=-20
    time csv_stats_script="${scripts_dir}/csv-stats.py"
	${csv_stats_script} -m -v --serial-transformation \
        -s file-system \
        -b fs-access-category \
        -a fs-canonize-with-ext-dir \
        -b process-name \
        -a host \
        -t csv-to-column \
        -x csv-to-column=$HOME/Downloads/proc-prev-12-18.09.csv \
        -t boolean-entries \
        -t statistics-autogen \
        -x statistics-autogen=${scripts_dir}/random-detection/english-model,1 \
        -t canonize-paths-by-proc-prevalence-v2 \
        -x canonize-paths-by-proc-prevalence-v2="1,-1,${fs_prevalence_percent},${fs_min_occurrences}" \
        -t sum-by-secondary-key \
        -o results.csv explorer.exe

Switch -m enables multiprocessing and leverages the system cores if available. Switch -V will print lot of debug information. Switch -t (transformation) can be used multiple times. For example
 
        -t filter-key-by-col -x filter-key-by-col=2,false,read,unknown

Output - a profile - can look like this 

	"regsvr32.exe","528","[AUTOGEN]\software\classes\[AUTOGEN]\clsid\[GUID]\inprocserver32\(Default)","249"
	"regsvr32.exe","528","[AUTOGEN]\software\classes\[AUTOGEN]\clsid\[GUID]\inprochandler32\(Default)","9"
	"regsvr32.exe","528","[AUTOGEN]\software\classes\*\shellex\contextmenuhandlers\anotepad++[NUM]\(Default)","1"
	"regsvr32.exe","528","[AUTOGEN]\software\classes\*\shellex\contextmenuhandlers\anotepad++\(Default)","1"
	"regsvr32.exe","528","[AUTOGEN]\software\classes\*\shellex\contextmenuhandlers\prltoolsshellext\(Default)","1"
	"regsvr32.exe","528","[AUTOGEN]\software\classes\*\shellex\propertysheethandlers\[GUID]\(Default)","3"
	
There are ~40 possible transformations - functions decorated with @aggregation.make_transformation. For example -t filter-key-by-col means that there is somewhere a function which name is filter_key_by_col. Pay attention to the name convention. File names and decorators do not use underscore. Python identifier name has to use an underscore.  

Performance of the script is ~120K lines/min per core. For example, processing 2.5M lines on a 2 cores/4 hyperthreads machine requires 8 minutes. The resulting file - a profile - contains ~300K lines. One week of data for explorer.exe in IDT is ~2.5M lines. Internet explorer generates ~15x more data over the same period

Normally -m (multiprocessing) switch is specified and  csv-stats.py will fork Python instances to process different parts of the data file [^7] 

Let's follow a couple of popular transformations:

csv-to-column

See add_column_from_csv in the transformations.py[^8] - the function selects (?) relevant columns from the CSV file (TODO what this function is for?)

boolean-entries

Function boolean_entries_transformation forces value 1 TODO?  

statistics-autogen

canonize-paths-by-proc-prevalence-v2

sum-by-secondary-key
 
   
#   E-mail from Ohad

I've integrated process chain profile generation into the generate-standard-profile.sh script. To create the process chain profile you can just run:

	./generate-standard-profiles.sh -p <process prevalence csv> -s <payoneer 2 week process dump - raw> -o <output directory>

You can also create it along with other profiles, like so:

	./generate-standard-profiles.sh -p <process prevalence csv> -s <payoneer 2 week process dump - raw> -o <output directory> -r <registry split directory> -f <file system split directory> split-chrome.exe split-svchost.exe

For the coming week you may need to understand how the cross validation machinery works so here is a rundown of how the process works. The cross validation works in 4 (5 for process chains) steps:

For process-chains - conversion of cross validation data into standard csv with process chains

1. normalization of input
2. application of profile
3. cross-validation generation (json output)
4. cross-validation formatting (conversion of multiple jsons into the summary we review in the meetings)

I've attached the output of the intermediate steps of this process for process chains https://secdoit.atlassian.net/secure/attachment/27094/27094_intermediate-steps.tar.gz

### step 0 - conversion to standard csv form

The raw dump from the database is process creation info. First we need to ran arkady's  process_creation_tree.py to convert it to standard form.
The input of this step can be found in the raw-data/ directory in the attached tar. It was split from the original dump by arkady's split_by_host_id.py.
The output of this step, i.e. the csv to through into the normalization step can be found in prepared-input.csv.

### step 1 - normalization

This step takes the the raw data from the dumps for regular profiles and runs the canonization and extracts other relevant fields. for example, for the following line from a file system dump:

	chrome.exe, file_access_2, my_host_id, instance_id, secdo_process_id, c:\program files (x86)\chrome\config.ini
 
The output line will be

	chrome.exe, "modify", my_host_id, instance_id, secdo_process_id, "ProgramFiles\chrome\[EXTENSION:config]\config.ini", 1

The 1 at the end is a meaningless counter and is ignored for the reminder of the process. This is done so in the next step we can check which paths fall in the profile and which instances had how many exceptions to the profile. This phase is done by running csv-stats.py with the same -b/-a arguments used to generate the profile + host id, instance id and secdo process id

Again, the input for this phase is prepared-input.csv. The output of this phase (for process chains though) is normalized-input.csv.

### step 2 - application of profile

For each line in the output from the previous step we need to tell if it was in the profile or it wasn't. Fr example, if ProgramFiles\.... is in the profile then for the following line

	chrome.exe, "modify", my_host_id, instance_id, secdo_process_id, "ProgramFiles\chrome\[EXTENSION:config]\config.ini", 1

the output would be:

	chrome.exe, "modify", my_host_id, instance_id, secdo_process_id, "ProgramFiles\chrome\[EXTENSION:config]\config.ini", "matched"

if it wasn't in the profile, the output would be:

	chrome.exe, "modify", my_host_id, instance_id, secdo_process_id, "ProgramFiles\chrome\[EXTENSION:config]\config.ini", "unmatched"

this is done by running "canon-path-applier.py apply" on the input file with the following switches:

	-k - the columns in the input csv that identify the profile. for file-system it would be "-k 1 -k 2" - the first column is the process name and the second the access type
	-g - the columns in the input which are tags and will just be added to the output line. in the example it would be "-g 3 -g 4 -g 5" - the host id, instance id and secdo process id
	-c - the column to compare against the profile. in the example: "-c 6" - the path column
	-t - the profile type (file-system/network/domain/process-chain)
	-v - show debug prints
	-p - the profile to compare against

the input to this step is normalized-input.csv and the output is post-profile-application.csv.

### step 3 - cross-validation generation (json output)

This step takes the output of the last step and does the statistics - which instance had how many "unmatched" and "matched" lines. It then summarizes everything in json format as can be seen in cross-validation-digest.json

This step is achieved by executing "cross-validation-summary.py generate" with the following switches:

	-i - the columns that identify the instance (host, instance, secdo process id)
	-p - the columns that identify the profile (process name for most, process name and file access for file system)
	-m - the column that holds the "matched"/"unmatched" value (defaults to the last column)
	-t - a tag - just some extra strings to put in the summary (i put the word "occurrences" or "percentages" to know what type of profile it was)
	-c - the content that was matched against the profile (the normalized path)

### step 4 - cross validation formatting

Summarizes a bunch of jsons generated by running the previous step multiple times and outputs the file "cross-validation-summary". Done by executing:

	cross-validation-summary format -f summary -o output_file json1 json2...

You can examine the cross valid process by looking at the "cross_validate_proc" function in generate-standard-profile.sh. You can look at other cross_validate_* functions as well. the only difference is they use a for loop to iterate over multiple input files for the cross validation.

**Also note that in cross_validate_proc there's an extra if between the pipes that i didn't put anywhere else. These if-s, if the "-i" flag is specified in the command line just save the intermediate content (what goes through the pipe) to side files for later examination. you can ignore them for all intents and purposes.**

Hope you find this useful.

Arkady - I think you should focus for the coming time on familiarizing yourself with these scripts and these steps. They are crucial and they are not trivial and they are very important if you'll need to integrate.


#  Legacy content of the file

* you should install a DOT graph visualizer like xdot (on Mac)

# Another e-mail from Ohad - shed some light on what the research team does


i've attached the standard cross validation results from the data we've been working on so far (around a week of data from September i think).
i have been trying to understand the source of the poor cross validation results and how to better aid the situation and i would like to point out a few points i've taken about the attached results.
for the sake of this debate i am only considering the file system profile based on the usual 20 occurrences bar.
also, though less than 1% error rate is considered the gold standard, i'm going to consider anything with less than 5% error rate as acceptable just so we can talk in terms
of "absolute garbage" and "i'd eat that" terms.

first off, i'll explain exactly what it is we see in the tables and how it was calculated.
each file has 3 tables:
1. percentage summary (yes, poorly named)
2. exception summary
3. events summary

the first table (percentage summary) shows the percent of instances from the total cross validation instances (relevant to the particular profile, of course, e.g firefox.exe/modify).
this was calculated by counting the total number of instances that were matched against the profile, counting the number of those instances that had at least 1
event (file) that didn't fall in the profile, and dividing the two numbers.
the format in the table is: bad instance percent (bad instance count/total instance count).

the second table (exception summary) shows the information detailing the per-instance errors.
for every instance (i.e. a specific iexplore.exe instance) in every profile (e.g. iexplore.exe/modify) the total number of events (files, both that matched the profile and that didn't) and the number of events that didn't match the profile were counted and divided to yield the percent of events that didn't fit the profile.
from that series of percents the following details were extracted: average, standard deviation, min and max.
the format in the table file is: average (std., min-max)

the third table (events summary) details the number of events in total (regardless of whether that event matched the profile or not) of every instance.
for every process instance in every profile the total number of events was counted.
from that series i've extracted as before: average, standard deviation, min and max.
the format in the table is the same as before.

obviously, these tables were extracted from the cross-validation data that was set aside during the profile creation process.

in this mail i will focus on certain results i think we can learn from.
i'd cover those with a total instance count of at least 50 (and maybe others that display interesting phenomenons) .

so let's review what we see (the conclusion section will have the practical implications and suggestions along with a summary of the raised questions):

(you can see the raw data in the attached files - i've reorganized everything into one table and removed irrelevant lines)

                                     percentages summary          events                        exceptions
    ------------------------------  ---------------------------  ---------------------------   -------------------------------
    cmd.exe/modify                  81.45% (5500/6753)           3.50 (1.61, 1-69)             81.08% (38.94%, 0.00%-100.00%)
    cmd.exe/modify-dir              11.67% (7/60)                2.08 (0.99, 1-4)              11.67% (32.10%, 0.00%-100.00%)
    conhost.exe/modify              0.54% (6/1108)               1.59 (0.66, 1-3)              0.54% (7.34%, 0.00%-100.00%)
    cscript.exe/modify              2.97% (305/10256)            2.00 (1.43, 1-15)             2.13% (12.41%, 0.00%-100.00%)
    csrss.exe/modify                0.00% (0/50)                 1.00 (0.00, 1-1)              0.00% (0.00%, 0.00%-0.00%)
    dllhost.exe/modify              43.71% (153/350)             10.04 (52.87, 1-768)          36.37% (46.30%, 0.00%-100.00%)
    excel.exe/modify                3.78% (21/556)               28.41 (74.41, 1-844)          0.31% (2.69%, 0.00%-44.44%)
    excel.exe/modify-dir            2.98% (11/369)               5.31 (3.79, 1-31)             1.23% (8.87%, 0.00%-87.50%)
    explorer.exe/modify             8.71% (25/287)               73.01 (479.38, 1-7871)        1.79% (10.74%, 0.00%-100.00%)
    explorer.exe/modify-dir         7.62% (16/210)               19.25 (236.50, 1-3431)        4.47% (17.05%, 0.00%-100.00%)
    firefox.exe/modify              56.72% (38/67)               206.06 (255.12, 1-968)        30.10% (44.77%, 0.00%-100.00%)
    iexplore.exe/modify             0.47% (7/1484)               127.05 (404.36, 1-6508)       0.02% (0.42%, 0.00%-15.38%)
    iexplore.exe/modify-dir         4.44% (4/90)                 2.20 (3.12, 1-22)             1.52% (8.78%, 0.00%-66.67%)
    logonui.exe/modify              29.41% (30/102)              2.16 (1.19, 1-8)              21.57% (36.55%, 0.00%-100.00%)
    lsass.exe/modify                1.11% (3/270)                3.69 (1.43, 1-10)             0.29% (2.80%, 0.00%-33.33%)
    msmpeng.exe/modify              10.23% (9/88)                30.06 (19.28, 2-116)          8.11% (26.55%, 0.00%-100.00%)
    msmpeng.exe/modify-dir          11.63% (10/86)               2.59 (0.84, 1-5)              9.01% (27.47%, 0.00%-100.00%)
    onedrive.exe/modify             13.64% (6/44)                13.00 (7.05, 1-36)            6.57% (18.27%, 0.00%-70.37%)
    outlook.exe/modify              12.04% (43/357)              148.20 (568.99, 1-8755)       0.63% (2.76%, 0.00%-28.08%)
    outlook.exe/modify-dir          5.30% (7/132)                4.33 (7.58, 1-28)             2.88% (13.38%, 0.00%-100.00%)
    powershell.exe/modify           0.31% (3/958)                3.83 (2.26, 3-19)             0.11% (2.35%, 0.00%-68.42%)
    regsvr32.exe/modify             20.41% (10/49)               33.96 (9.72, 1-45)            10.03% (27.31%, 0.00%-100.00%)
    rundll32.exe/modify             2.53% (48/1898)              18.49 (11.03, 1-135)          0.96% (8.14%, 0.00%-100.00%)
    rundll32.exe/modify-dir         1.08% (16/1481)              1.25 (3.42, 1-92)             0.66% (6.68%, 0.00%-100.00%)
    runtimebroker.exe/modify        5.80% (4/69)                 11.65 (8.07, 2-47)            0.95% (4.16%, 0.00%-27.27%)
    runtimebroker.exe/modify-dir    7.46% (5/67)                 1.45 (1.44, 1-10)             4.71% (17.41%, 0.00%-87.50%)
    services.exe/modify             0.00% (0/208)                6.23 (2.63, 1-12)             0.00% (0.00%, 0.00%-0.00%)
    spoolsv.exe/modify              2.07% (3/145)                15.28 (35.51, 1-320)          0.78% (5.65%, 0.00%-54.63%)
    sppsvc.exe/modify               2.33% (4/172)                2.49 (2.45, 1-8)              1.45% (9.98%, 0.00%-100.00%)
    taskhost.exe/modify             0.28% (13/4689)              39.37 (545.65, 1-20045)       0.10% (2.44%, 0.00%-100.00%)
    taskhost.exe/modify-dir         0.40% (1/249)                3.12 (1.96, 1-11)             0.13% (2.11%, 0.00%-33.33%)
    taskhostex.exe/modify           9.23% (6/65)                 662.00 (2364.44, 1-17468)     0.65% (3.39%, 0.00%-25.00%)
    taskhostw.exe/modify            0.97% (6/618)                62.67 (418.38, 1-9580)        0.38% (5.56%, 0.00%-100.00%)
    taskhostw.exe/modify-dir        2.38% (3/126)                11.79 (17.51, 1-82)           1.34% (10.44%, 0.00%-100.00%)
    wmiprvse.exe/modify             0.38% (10/2611)              3.44 (9.20, 1-429)            0.32% (5.53%, 0.00%-100.00%)
    wmpnetwk.exe/modify             0.00% (0/49)                 2.84 (2.54, 1-13)             0.00% (0.00%, 0.00%-0.00%)

cmd.exe/modify

cmd.exe/modify poses an apparent aberration to the profile with a whopping 81.45% (5500/6753) of erroneous instances.
this seems to make sense as cmd.exe is just a container for functionality that belongs to an ancestor and therefor it's file accesses should be attributed to it's parent.
we've talked about it before but we've never gotten around to implementing it.
for this reason i suggest that for the time being we stop looking into cmd.exe, rundll32.exe, regsvr32, dllhost.exe (maybe) and powershell.exe (despite powershell having an amazingly good cross validation score) for anything but execution chains.

another thing that leapt at me when i was looking at cmd.exe: on average it does 3.5 events on average and has a standard deviation of just 1.61 events!
this is so little that it'll basically skyrocket the standard deviation (which it did - it's just shy of 39%).
this means basically that there's a huge tail of cmd.exe executables that do very little and their "distance" (a currently mathematically vague and loosely defined notion but you understand it) means
nothing as far as the validity of the instance is concerned. an instance can deviate 100% from the profile and still be perfectly fine.
this got me thinking: if a process, on average, does very few file actions then it should either have a very stable profile (i.e. an acceptable result with relatively low deviation average and std.) or a super-crappy one (where the error std. goes through the roof).
indeed, this somewhat follows as you can see by examining the following relatively stable profiles of processes that don't do many file ops (single digit amounts):
* conhost.exe/modify
* cscript.exe/modify
* csrss.exe/modify
* excel.exe/modify-dir
* iexplore.exe/modify-dir
* lsass.exe/modify
* services.exe/modify
* sppsvc.exe/modify
* taskhost.exe/modify-dir
* wmiprvse.exe/modify
* wmpnetwk.exe/modify

sample borderline acceptable profiles:
* outlook.exe/modify-dir (5.3% (7/132) failure rate, 4.3 actions on average)
* runtimebroker.exe/modify (5.8% (4/69) failure rate, 11 actions on average - this is a very steep value but i've included it anyway because i couldn't decide whether it should be here or not)
* runtimebroker.exe/modify-dir (7.46% (5/67) failure rate, 1.45 actions on average)

conversely you may look at these unstable profiles:
* cmd.exe/modify
* dllhost.exe/modify
* logonui.exe/modify

some borderline cases i wasn't sure how to classify and you can put your vote in for:
* cmd.exe/modify-dir
* msmpeng.exe/modify-dir (well this is a bit borderline and may fall any which way)

there seem to a trend where the crappy profiles have, for processes with few events, where crappy cross validation goes hand in hand with (surprise surprise) a relatively large std. (over 20%).
i should note in advance that as you may have heard me talk about this before so i don't think i'm super objective here so feel free to contest this indication.

outlook.exe/modify


another profile that points out a flaw/constraint is outlook.exe/modify.
you can see it has a failure rate of 12% (43/357) which is a lot.
examining the cross-validation dataset showed that 27 of those 43 instances had a mcafee agent that caused the exception (probably DLP) and they were on about 5 hosts.
the profile generation dump had only 24 hosts with mcafee entries in the outlook.exe dump.
this means that the cross validation fell victim to a sort of a caveat, as the spread of this program is very sparse and yet it was considerably present in the cross-validation.
to refine the point: the fact that we use host prevalence to generate the profile (which is well justified) and instance count to measure errors in cross validation (which again, is justified)
put us in a place where i'm not entirely sure the 12% error is reflective of anything.


firefox.exe/modify


this one sort of baffles me. with 56% failure rate this pretty much sucks.
i'll break down the bad paths that appear in the cross-validation:
* 24 instances on the same machine that access ProgramData\nvidia
* 9 instances that wrote to c:\user\<user>\downloads which didn't get into the profile (for a good reason - it barely showed in the profile generation data.
   this leads me to think that maybe i'm not using sufficient data (though even if we doubled it i doubt this directory would get a place in the profile).
* 4 instances that dealt with flash/macromedia

and that's it roughly.
now the data for firefox was of decent size (1.3G for profile generation), but it solely consisted of 31 hosts, though the process creation records showed 128 hosts that actually ran firefox
at the proximity of those dates (i use 2 weeks worth of process creation information to determine to process prevalence).

i'm not sure how this dissonance came to be or what these firey foxes do.
i'll look into the cause on sunday.


discussion

1. should we discard profiles of processes that do few file operations but have high variance? (i vote yay for now)
2. should we pursue in the immediate time frame for consolidation of cmd and such into their parent processes? (i vote nay for now; this is not a simple implementation detail. this is a tedious and complex modification [i think])
3. should we only generate the profile on data collected from instances that did more or less than a certain number of events? (not sure what to think of this)
3. how can we better address and resolve the discordance between host prevalence profile generation and instance-count based cross validation process? (still wondering about that)
4. how do we address the conflicting process prevalence collected from process creation and from the file access data?
5. i talked with Shai about re-thinking the host profile concept for certain processes. maybe it's worth a shot (the modification to the code should be slight, but cross validation will be a b****).

i've had a few more questions but it's late and i'm tired and i can't remember them so feel free to comment and raise your own questions and doubts.

have a great weekend. 


# Footnotes

[^1]: A copy&paste from from Ohad's Slack (slack, slack, slack)

	strict - is a profile that should be fully contained in an instance (?), demands 85% percent of the hosts, see switch -x canonize-paths...

	by occurences - something has to appear 20 times to be included in thr profile.

	by prevalence - something has to appear on 5% of the hosts to be included in this profile.

	read profile - a profile for file read operations (deprecated, should be empty)

	write profile - a profile for file modification operations

[^2]: Prevalence of an event is defined as a number of hosts in the network where the event was registered. A cut off number is often defined for the prevalence. For example, a specific event shall appear at least on 20 hosts to enter an application profile. The cut off number can be a ratio between matching hosts and total hosts.    

[^3]: Python decorators are leveraged by cool frameworks PyTest, Flask and many others. This is a cornerstone of the Ohad code and a great Python lesson for us, uninspired mortals.

[^4]: You do need this "prevalence" because the data in the the processed file does not necessary contain all hosts in the network. The prevalence is calculated as a ratio between number of hosts matching the profile (hosts where a specific pattern was witnessed) to the total number of hosts in the organisation. I suspect that a large enough data file will contain events from all or most of the hosts and prevalence can be calculated without switch -p. 

[^5]: I can imagine a scenario where hosts which do not appear in the processed data file, but appear in the "prevalence" data should not impact the calculation. Is there any other reason behind providing the data?

[^6]: This is kinda insane if you ask me. When we generate a profile we know how good it is going to fit the actual data. There is zero machine learning involved. Everything is predefined. The (only?) benefit here is that we can catch some bugs in the profile generation. Shai Meir: this is a scientific approach. 

[^7]: Because we usually process more than one data file (events generated by more than one executable) I expected processing of multiple files is done in parallel. This is a not the case. In reality there is a function  generate_splices() which splits the data file into chunks and runs processing for them. It means also that there is a stage in the pipeline when parallel processing of the data is not possible.

[^8]: Tip: grep for make_transformation and grep csv-to-column

[^9]: See list of the companies on the sparc ~/.secdo-dbs. For example IDT will be idt-corp on cloud1.us

[^10]: An example _format_summarize() in cross-validation-summary.py. I am not sure if C++ multiple virtual inheritance is a worse idea than this function overload design pattern. You must be wondering what functions of the two is going to be called. Check the execution of the cross-validation-summary.py in the generate-standard-profiles.sh and pay attention to the switch -f. Functions with decorators "csv" and "summary" are going to be called. Easy peasy. 

[^11]: Often a threshold of 20 hosts is used

[^12]: Theoretically we can build profiles for executables, users, machines, days of week, IP sub nets

[^13]: If you see zero false positives like in the example above (explorer.exe with zero false positives is a telling sign) check the file process-tree-profile/process-tree-profile.csv 
There is a fair chance you will see things like "conhost.exe","1220","conhost.exe|[PARENT]","203" This lines means that any process can run conhost.exe and any execution chain is valid. Tag [PARENT] - a garbage sink, or "trap" - means that there was lot of noise, lot of different execution chains on many hosts and the algorithm united them all under single anonymous [PARENT] group.

[^14]: For filenames/folders we introduce yet another word "morpheme". A file path is built from morphemes - smallest chunks of letters which have a meaning. When we canonize a file path we attempt to figure out the  morphemes. For example, C:\Windows\System32 and C:\Windows\System are two different ways to express a morpheme "Windows System Folder"