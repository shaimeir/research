#!/usr/bin/env python3

import sys
import pathlib
import pip
import pkg_resources
import logging
import argparse

def main():
    parser = argparse.ArgumentParser(description='verify the python version and the dependencies specified in pip requirements file.')
    parser.add_argument("-p", "--pip-requirements", help="a requirements file to verify.")
    parser.add_argument("-P", "--default-pip-requirements", action='store_true', help="test the project's default requirements.txt.")
    parser.add_argument("-s", "--python-version", help="the minimal required python 3 version")
    args = parser.parse_args()

    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s", level=logging.INFO)

    requirements = str(pathlib.Path(__file__).resolve().parent/'requirements.txt') if args.default_pip_requirements else args.pip_requirements
    if requirements is not None:
        logging.info("validating pip requirements (%s)", requirements)
        requirements = pip.req.parse_requirements(requirements, session=pip.download.PipSession())
        requirements = [str(r.req) for r in requirements]
        pkg_resources.require(requirements)

    if args.python_version is not None:
        logging.info("validating python version")
        assert args.python_version <= sys.version.split(" ", 1)[0], ValueError('invalid python version', args.python_version, sys.version)

if __name__ == "__main__":
    sys.exit(main())

