#!/usr/bin/env python3

"""
this script queries an INI file for a specific field's value.
the functionality is less general than ini-query.py and makes some tasks easier in the shell.
"""

import sys
import configobj
import argparse
import logging

def field_exists(config, field):
    if field in config and not field in config.sections:
        logging.info("the field %s exists", field)
        print("1")
        return 0

    logging.info("the field %s doesn't exist", field)
    return 1

def query_field(config, field, default):
    if field in config:
        value = config[field]
        logging.info("the field %s is defined in the configuration: %r", field, value)
        print(value)
    elif default is not None:
        logging.info("the field %s is not defined in the configuration - using the default: %s", field, default)
        print(default)
    else:
        logging.info("the field %s is not defined in the configuration and no default was defined.", field)
        return 1 # failure

def main():
    parser = argparse.ArgumentParser(description='query a specific field in an INI file.')
    parser.add_argument("-v", "--verbose", action='store_true', help='show debug prints.')
    parser.add_argument("-q", "--quiet", action='store_true', help='never fail.')
    parser.add_argument("-e", "--exists", action='store_true', help='only check for existance of the field. prints "1" if the field exists.')
    parser.add_argument("-f", "--field", required=True, help='the field to query.')
    parser.add_argument("-d", "--default", help="use this default value if the field doesn't exist.")
    parser.add_argument("-r", "--override", default="", help="if this argument is not an empty string then automatically use it.")
    parser.add_argument("ini", nargs="+", help="the INI file to query (followed by the sub-sections path).")

    args = parser.parse_args()

    logging.basicConfig(level=logging.INFO if args.verbose else logging.WARN, format='%(asctime)s [%(levelname)s] %(message)s')

    if args.override != "":
        logging.info("using override: %s", args.override)
        print(args.override)
        return 0

    logging.info("loading config object from %s", args.ini[0])
    config = configobj.ConfigObj(args.ini[0])
    for section in args.ini[1:]:
        logging.info("fetching sub-section %s", section)
        if args.quiet and section not in config.sections:
            return 0 # silently fail
        config = config[section]

    exit_code = 0

    if args.exists:
        exit_code = field_exists(config, args.field)
    else:
        exit_code = query_field(config, args.field, args.default)

    return 0 if args.quiet else exit_code

if __name__ == "__main__":
    sys.exit(main())

