#!/usr/bin/env python3

import configobj
import argparse
import sys
import contextlib

@contextlib.contextmanager
def ignore_missing_keys():
    try:
        yield

    except KeyError:
        pass

@contextlib.contextmanager
def fail_on_missing_key():
    yield

def get_value(config, key, ref_key, ref_base, quiet):
    if ref_key is None or ref_base is None:
        if key in config.sections:
            # skip sub-sections
            raise KeyError('sub-sections are not supported with -k switch', key)
        return config[key]

    resolution_queue = [config]
    checked_references = set()
    while len(resolution_queue) > 0:
        current_config = resolution_queue.pop(0)
        if key in current_config:
            if key in current_config.sections:
                # skip sub-sections
                raise KeyError('sub-sections are not supported with -k switch', key)
            return current_config[key]

        references = current_config.get(ref_key, [])
        if not isinstance(references, (list, tuple)):
            references = [references]

        for ref in references:
            if (quiet and ref not in ref_base.sections) or ref in checked_references:
                continue

            checked_references.add(ref)
            resolution_queue.append(ref_base[ref])

    # if we've gotten here then it's game over - raise a missing key error
    raise KeyError("couldn't resolve a key", key, ref_key, ref_base is not None)

def config_items(config, ref_key, ref_base, quiet, exclusion_list):
    resolution_queue = [config]
    checked_references = set()

    while len(resolution_queue) > 0:
        current_config = resolution_queue.pop(0)
        yield from ((k, v) for k, v in current_config.items() if k != ref_key and k not in current_config.sections and k not in exclusion_list)

        if ref_base is not None and ref_key in current_config:
            refs = current_config[ref_key]
            if not isinstance(refs, (list, tuple)):
                refs = [refs]

            for ref in refs:
                if (quiet and ref not in ref_base.sections) or ref in checked_references:
                    continue

                checked_references.add(ref)
                resolution_queue.append(ref_base[ref])

def format_key(key, value, tuplize):
    if tuplize and isinstance(value, (list, tuple, set)):
        return "\n".join((f"{key}\t{v}" for v in value))
    elif isinstance(value, (list, tuple, set)):
        return "\n".join(value)
    elif tuplize:
        return f"{key}\t{value}"
    else:
        return value

def main():
    parser = argparse.ArgumentParser(description="query values in an ini file's sections and sub-sections.")
    parser.add_argument("-k", "--key", default=[], action="append", help="the values to query.")
    parser.add_argument("-K", "--all-keys", action="store_true", help="print all the keys in the requested ini/section.")
    parser.add_argument("-s", "--sub-sections", action="store_true", help="print sub-sections of the given section.")
    parser.add_argument("-t", "--tuples", action="store_true", help="print the output values along with the keys using tab separation.")
    parser.add_argument("-x", "--exclude", action="append", default=[], help="skip keys with the specified names.")
    parser.add_argument("-r",  "--ref-key", help="this specified key's values are names of other sections to serve as extensions of the examined section. look for keys there as well.")
    parser.add_argument("-b", "--ref-base-section", default=[], nargs="+", help="the sections pointed by the key specified with --ref-key are sub-sections of this given section.")
    parser.add_argument("ini", nargs="+", help="the INI file to query (followed by the sub-sections path).")

    conflicting_switches = parser.add_mutually_exclusive_group()
    conflicting_switches.add_argument("-q", "--quiet", action="store_true", help="ignore missing keys and sections.")
    conflicting_switches.add_argument("-e", "--exist", action='store_true', help="validate the existence")

    args = parser.parse_args()

    missing_key_policy = ignore_missing_keys if args.quiet else fail_on_missing_key

    base_config = configobj.ConfigObj(args.ini[0])

    config = base_config
    for section in args.ini[1:]:
        if args.quiet and section not in config.sections:
            return 0 # silently fail
        config = config[section]

    ref_base = base_config
    for section in args.ref_base_section:
        if args.quiet and section not in ref_base.sections:
            ref_base = None
            break
        ref_base = ref_base[section]

    if args.exist:
        try:
            for key in args.key:
                get_value(config, key, args.ref_key, ref_base, False)
        except Exception:
            return 1
    elif args.all_keys:
        for key, value in config_items(config, args.ref_key, ref_base, args.quiet, args.exclude):
            print(format_key(key, value, args.tuples))
    else:
        for key in [key for key in args.key if key != args.ref_key]:
            with missing_key_policy():
                print(format_key(key, get_value(config, key, args.ref_key, ref_base, args.quiet), args.tuples))

    if args.sub_sections and config.sections is not None:
        for section in config.sections:
            print(section)

if __name__ == "__main__":
    with contextlib.suppress(KeyError):
        sys.exit(main())

    sys.exit(1)

