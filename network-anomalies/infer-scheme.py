#!/usr/bin/env python3
'''
this utility script takes a recipe or a dump file and outputs their scheme in the following format:

<file name> <scheme>

this format is easily readable with various shell utilities and allows us to create separate lists for every dump
and recipe types in an automated shell environment (generate-standard-profiles-v2.sh)
'''

import sys
import argparse
import pathlib
import os
import logging
import contextlib

from csv_processing import scheme, profile_recipe

@contextlib.contextmanager
def auto_obj(obj):
    yield obj

class DumpInferralUtility:
    def __init__(self):
        self.schemes = {}
        for scheme_name in scheme.get_default_schemes_list():
            scheme_obj = scheme.load_default_scheme_file(scheme_name)
            self.schemes[scheme_name] = (set(scheme_obj.column_translation().keys()), scheme_obj.default_type)

    def infer_first_row(self, columns):
        columns = set(columns)
        candidate = None
        logging.debug("checking columns: %r", columns)
        for scheme_name, scheme_columns_tuple in self.schemes.items():
            scheme_columns, default_type = scheme_columns_tuple
            logging.debug("testing %s: %r, %r", scheme_name, scheme_columns, default_type)
            if columns == scheme_columns:
                return scheme_name
            elif columns.issuperset(scheme_columns) and default_type is not None:
                if candidate is None or len(candidate[0]) < len(columns):
                    candidate = (columns, scheme_name)

        if candidate is None:
            raise ValueError('unknown scheme', columns)

        return candidate[1]

    def columns_from_first_line(self, first_line):
        return {col.strip('"') for col in first_line.strip().split("\t" if "\t" in first_line else ",")}

inferral_utility = DumpInferralUtility()

def infer_dump_type(filename, **kwargs):
    global inferral_utility

    with open(filename, "rt") as dump_file:
        first_line = dump_file.readline()

    return inferral_utility.infer_first_row(inferral_utility.columns_from_first_line(first_line))

def infer_recipe_type(filename, sub_recipe=None, **kwargs):
    recipe = profile_recipe.load_recipe_from_json(filename)
    if sub_recipe is not None:
        recipe = recipe.get_subrecipe(sub_recipe)

    assert recipe.scheme_name is not None, ValueError("scheme not specified in receipt", filename, sub_recipe)
    return recipe.scheme_name

type_func_dict = {
    'dump' : infer_dump_type,
    'recipe' : infer_recipe_type,
}

def list_files(file_or_dir):
    if '-' == file_or_dir.strip(): # read files list from stdin
        yield from {line.strip() for line in sys.stdin} # make sure each file appears only once
        return

    file_or_dir = pathlib.Path(file_or_dir)
    if file_or_dir.is_file():
        yield file_or_dir
        return

    yield from {filename for filename in (file_or_dir/filename for filename in os.listdir(file_or_dir)) if pathlib.Path(filename).is_file()} # make sure each file appears only once

def main():
    parser = argparse.ArgumentParser(description="this utility script inferrs the scheme of a dump/recipe file.")
    parser.add_argument("-v", "--verbose", action="store_true", help="show debug prints.")
    parser.add_argument("-q", "--quiet", action="store_true", help="don't fail if the scheme is unrecognizable, simply output 'unknown'.")
    parser.add_argument("-t", "--type", default="dump", choices=list(type_func_dict.keys()), help="what kind of file are we dealing with - a dump or a recipe.")
    parser.add_argument("-s", "--sub-recipe", help="sub-recipe for recipe files")
    parser.add_argument("files", nargs="*", default=["-"], help="the file to infer the scheme for. use '-' to list files in stdin.")

    args = parser.parse_args()

    # a workaround as someone accesses the logger in one of the imports before it's initialization....
    if logging.getLogger() is not None:
        logging.getLogger().handlers = []
    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.WARN, format="%(asctime)s [%(levelname)s] %(message)s")

    for file_or_dir in args.files:
        for filename in list_files(file_or_dir):
            try:
                print(filename, type_func_dict[args.type](filename, sub_recipe=args.sub_recipe))
            except Exception:
                logging.debug("failed to open %s quiet=%r", filename, args.quiet, exc_info=True)
                if args.quiet:
                    print(filename, 'unknown')
                    continue
                else:
                    raise

if __name__ == "__main__":
    sys.exit(main())

