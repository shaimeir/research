#!/usr/bin/env python3

import argparse
import sys
import pprint
import re
import logging
import contextlib
from collections import namedtuple

class FileSystemCanonizer:
    PlaceholderToken = "SECDO"

    SensitiveExtensionsCanonization = {
        'executable' : r'exe|sys|dll|msi|ocx|bin', # ['exe', 'dll'],
        'shell-script' : r"ps1m?|bat|cmd", # ['ps1', 'bat', 'cmd'],
        'script' : r'js|vbs|py|php|rb',
        'tmp' : r'te?mp|cache|\$\$\$', # ['tmp'],
        'backup' : r'bak|mdbackup',
        'office' :r'doc[xm]?|xls[xm]?|ppt[xms]?|pdf|rtf|od[bcmpst]|xml|xslt', # ['doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'dot', 'docm', 'dotx', 'odt', 'rtf', 'xps'],
        'web' : r'css|html?|mhtl|aspx?',
        'media' : r'jpe?g|mpe?g|mp[234]|png|bmp|dib|gif|ico|icn|flac|aac|mkv|ogg|mid|avi|psd|svg|mov|wm[av]|dwg', # ['png', 'jpg', 'jpeg', 'bmp', 'gif'], # image
        'shortcut' : r'lnk|url|pif|scf|shs|shb|xnk', # ['lnk'],
        # 'dat' : ['dat'],
        'db' : r'db[af]?|accdb|mdf|mdb|kdb|hsql|hkdb|syncdb|itdb',
        'config' : r'inf|cfg|config|ini|',
        'txt' : r'txt|log\.?\d*',
        'registry' : r'reg',
        'prefetch' : r'pf',
    }

    KnownWindowsContainerPaths = {
        'ProgramDataMs' : r".:\\(ProgramData|AppData)\\Microsoft",
        'UserAppDataSubdir' : r".:\\Users\\[^\\]+\\AppData(\\(Roaming|Local|LocalLow))?",
        'ProgramData' : r".:\\(ProgramData|AppData)",
        'ProgramFiles' : r".:\\Program Files( \(x86\))?(\\Common Files)?",
        'UserAppDataMicrosoftSubdir' : r".:\\Users\\[^\\]+\\AppData\\Roaming\\Microsoft",
        'WindowsDir' : r".:\\Windows",
        'System' : r".:\\Windows\\(system32|syswow64)",
        'DifferentDrive' : r'[abd-z]:',
    }

    KnownWindowsPaths = {
        'StartupFolder' : r".:\\(Users\\[^\\]+\\AppData\\Roaming\\Microsoft|ProgramData|AppData)\\Windows\\Start Menu\\Programs\\Startup",
        'WindowsTemplates' : r".:\\(Users\\[^\\]+\\AppData\\Roaming|ProgramData|AppData)\\Microsoft\\Windows\\Templates",
        'SidebarGadgets' : r".:\\(Program Files( \(x86\))?|Users\\[^\\]+\\AppData\\Local)\\Microsoft\\Windows Sidebar\\Gadgets",
        'UserCookies' : r".:\\Users\\[^\\]+\\AppData\\Roaming\\Microsoft\\Windows\\Cookies",
        'CdBurner' : r".:\\Users\\[^\\]+\\AppData\\Local\\Microsoft\\Windows\\Burn\\Burn",
        'UserTempInetFiles' : r".:\\Users\\[^\\]+\\AppData\\Local\\Microsoft\\Windows\\Temporary Internet Files",
        'UserContent' : r".:\\(Users\\[^\\]+|ProgramData\\Microsoft\\Windows\\(GameExplorer|Ringtones|Start Menu))",
        'RecycleBin' : r".:\\\$recycle\.bin",
    }

    def __init__(self):
        # compile the regexes
        canon_re = [
            (
                canon_form,
                re.compile(
                    r"^(?P<canon_folder>{}\\(?P<subfolder>[^\\]+\\)?)".format(container_folder_re),
                    re.I
                )
            )
            for canon_form, container_folder_re in self.KnownWindowsContainerPaths.items()
        ]

        canon_re += [
            (
                canon_form,
                re.compile(
                    r"^(?P<canon_folder>{}\\)".format(known_folder_re),
                    re.I
                )
            )
            for canon_form, known_folder_re in self.KnownWindowsPaths.items()
        ]

        canon_re += [
            (
                canon_form + "[TEMP]",
                re.compile(
                    str(folder_re.pattern) + r"(?P<temp_tail>(.+\\|)temp\\)",
                    re.I
                )
            )
            for canon_form, folder_re in canon_re
        ]

        self.canon_re = dict(canon_re)
        self.backslash_coalesce = re.compile(r"\\+")
        self.guid_re = re.compile(r"[{(]?[0-9A-F]{8}-([0-9A-F]{4}-){3}[0-9A-F]{12}[)}]?", re.I)
        self.temp_folder_re = re.compile(r"^(?P<temp_path>(.*\\)?temp\\)", re.I)
        self.drive_re = re.compile(r"^(?P<drive>[A-Z]:\\([^\\]+\\)?)", re.I)
        self.smb_share = re.compile(r"^(?P<share>\\\\[^\\]+)\\", re.I)
        self.hidden_smb_share = re.compile(r"^(?P<share>\\\\[^\\]+\\[^\\]+\$)\\", re.I)
        self.non_canonized_device = re.compile(r"^(?P<device>\\device)\\", re.I)
        self.version_re = re.compile(r'(\b|_)(\d+[\._]\d+([\._]\d+)*)(?=\b|_)', re.I)
        # self.version_re = re.compile(r'(?!<[0-9a-f])(((?:\d+)(?:[._](?:\d+|[0-9a-f]+))+)(?:\-(?:\d+|[0-9a-f]+))*)(?=([^0-9._\-a-f]+|\b))', re.I)
        self.sid_re = re.compile(r'\b(?P<sid>s-\d+-\d+-\d+(-\d{,10})*)\b', re.I)
        # a blob, a string with mixed alpha/num (must have both) but not start with amd or win or lin or x (exclude for amd64, win32, x64/86 ).
        # as a restriction, we demand that the blob start with a digit and be at least 4 letters long
        self.blob_re = re.compile(r'(\b|_)((?=[a-z0-9]{4})\d+[a-z][0-9a-z]*|[a-z]+\d+[a-z][a-z0-9]*)(\b|_)', re.I)
        self.hash_re = re.compile(r'(\b|_)((?:[0-9a-f]{16,}){1,16}|[0-9a-f]{40,})(?=\b|_)', re.I)
        self.number_re = re.compile(r'(\b|_)(\d+)(?=\b|_)', re.I)

        self.extensions = {}
        for ext_type, ext_re in self.SensitiveExtensionsCanonization.items():
            self.extensions[ext_type] = re.compile(r"\.(" + ext_re + r")$", re.I)

        self.guid_placeholder = f"[{self.PlaceholderToken}:GUID]"
        self.sid_placeholder = f"[{self.PlaceholderToken}:SID]"
        self.hash_placeholder = f"[{self.PlaceholderToken}:HASH]"
        self.num_placeholder = f"[{self.PlaceholderToken}:NUM]"
        self.version_placeholder = f"[{self.PlaceholderToken}:VERSION]"

    def regexes(self):
        return {
            'normalizers' : {
                'backslash' : self.backslash_coalesce,
                'guid' : self.guid_re,
            },
            'smb_shares' : {
                'regular' : self.smb_share,
                'hidden' : self.hidden_smb_share,
            },
            'non_canonized_dev' : self.non_canonized_device,
            'temp_folder' : self.temp_folder_re,
            'canonizers' : self.canon_re,
        }

    def canonizeExtension(self, path):
        path = path.strip().lower()
        filename = path.rsplit("\\",1)[-1]

        if '.' in filename:
            for ext_type, ext_re in self.extensions.items():
                if ext_re.search(filename) is not None:
                    return ext_type

            return 'some-extension'

        return 'no-extension'

    def canonizePath(self, path):
        ret = self.canonize(path)
        return ret[0], ret[2]

    def canonize(self, path):
        '''
        returns:
            * canonized name
            * the matching prefix
            * the normalized path
        '''
        logging.debug("canonizing %s", path)

        path = path.strip().lower()
        path = self.guid_re.sub(self.guid_placeholder, path)
        path = self.sid_re.sub(self.sid_placeholder, path)
        path = self.hash_re.sub(r"\1" + self.hash_placeholder, path)
        #path = self.blob_re.sub(r"\1[BLOB]", path)

        prefix_match = self.hidden_smb_share.match(path)
        if prefix_match is not None:
            return "HiddenSmbShare", prefix_match.group('share').rstrip("\\"), path

        prefix_match = self.smb_share.match(path)
        if prefix_match is not None:
            return "SmbShare", prefix_match.group('share').rstrip("\\"), path

        prefix_match = self.non_canonized_device.match(path)
        if prefix_match is not None:
            return "NonCanonizedDevice", prefix_match.group('device').rstrip("\\"), path

        path = self.backslash_coalesce.sub(r"\\", path)
        path = self.version_re.sub(r"\1" + self.version_placeholder, path) # this might corrupt ip addresses so it can't be done before the smb checking
        path = self.number_re.sub(r"\1" + self.num_placeholder, path)

        match = None
        for canon_form, regex in self.canon_re.items():
            m = regex.match(path)
            if m is None:
                continue

            logging.debug("matched with [%r] %r", canon_form, regex)
            m = m.groupdict()
            canon_folder = m['canon_folder']
            if m.get('temp_tail', None) is not None:
                canon_folder += m['temp_tail']
            logging.debug("matching part is %s", canon_folder)

            if match is not None:
                if len(match[0]) > len(canon_folder):
                    logging.debug("previous matches are better")
                    continue
                elif len(match[0]) == len(canon_folder) and len(match[2]) >= len(regex.pattern):
                    logging.debug("same match, shorter regex")
                    continue

            subfolder = m.get('subfolder', None) # note that the key *can* be present and none so we can't use '' for a default
            if subfolder is not None:
                canon_form += "\\" + subfolder

            logging.debug("updated canonization choice to %s", canon_form)
            match = (canon_folder, canon_form, regex.pattern)

        if match is None:
            temp_match = self.temp_folder_re.match(path)
            if temp_match is not None:
                logging.debug("matched with temp folder")
                temp_path = temp_match.group('temp_path').rstrip("\\")
                return temp_path, temp_path, path

            drive_match = self.drive_re.match(path)
            if drive_match is not None:
                logging.debug("matched with drive pattern")
                drive_path = drive_match.group('drive').rstrip("\\")
                return drive_path, drive_path, path

            return path, path, path

        return match[1].rstrip("\\"), match[0].rstrip("\\"), path

def get_canonizer():
    return FileSystemCanonizer()

@contextlib.contextmanager
def _auto_context(obj):
    yield obj

def test():
    parser = argparse.ArgumentParser(description="test file system canonization.")
    parser.add_argument("-f", "--file", action='append', default=[], help="a file with a list of paths to canonize.")
    parser.add_argument("-d", "--dump", action='store_true', help='print the canonization regexes to stderr.')
    parser.add_argument("-v", "--verbose", action='store_true', help='show debug prints')
    parser.add_argument('paths', nargs='*', default=[], help="a list of paths to canonize.")
    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.WARN, format="%(asctime)s %(message)s")

    def _generate_paths():
        for _file in args.file:
            with (_auto_context(sys.stdin) if "-" == _file else open(_file, "rt")) as __file:
                yield from __file

        yield from args.paths

    canonizer = get_canonizer()
    if args.dump:
        pprint.pprint(canonizer.regexes(), stream=sys.stderr)

    for path in _generate_paths():
        canonized_dir, normalized_path = canonizer.canonizePath(path)
        canonized_ext = canonizer.canonizeExtension(path)
        print(canonized_dir + "\\." + canonized_ext, normalized_path)

if __name__ == "__main__":
    test()

