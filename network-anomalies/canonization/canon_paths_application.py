#!/usr/bin/env python3

from collections import namedtuple
import logging
import anytree
import sys

from utilities import anytree_helper
from utilities.anytree_helper import lazy_str

from . import matchable_nodes
from . import profile_tree_json_serialization
from . import path_parsing

_ProfileSuiteEntry = namedtuple('_ProfileSuiteEntry', (
                                                'name',
                                                'path_parser',
                                                'serializer',
                                                'node_class',
                                                'foldable',
                                                )
                                            )
_profile_suite = {
    'file-system' : _ProfileSuiteEntry(
                        'file-system',
                        path_parsing.WindowsPathParser,
                        profile_tree_json_serialization.SimpleTreeJsonSerialization,
                        matchable_nodes.RegexPathMatchableNode,
                        False
                    ),
    'registry' : _ProfileSuiteEntry(
                        'registry',
                        path_parsing.RegistryPathParser,
                        profile_tree_json_serialization.SimpleTreeJsonSerialization,
                        matchable_nodes.RegexPathMatchableNode,
                        False
                    ),
    'network' : _ProfileSuiteEntry(
                        'network',
                        path_parsing.NetworkIp4PathParser,
                        profile_tree_json_serialization.SimpleTreeJsonSerialization,
                        matchable_nodes.NetworkMatchableNode,
                        False # TODO: implement tree folding and set this to True
                    ),
    'domain' : _ProfileSuiteEntry(
                        'domain',
                        path_parsing.DomainPathParser,
                        profile_tree_json_serialization.SimpleTreeJsonSerialization,
                        matchable_nodes.DomainRegexMatchableNode,
                        False
                    ),
    'process-chain' : _ProfileSuiteEntry(
                        'process-chain',
                        path_parsing.simple_path_parser("|"),
                        profile_tree_json_serialization.SimpleTreeJsonSerialization,
                        # for this type of tree both every trap is both 'file' and 'subfolder'
                        matchable_nodes.simple_matchable_node(('[PARENT]', '[ANCESTOR]'), ('[PARENT]', '[ANCESTOR]')),
                        False
                    )
}

def get_supported_profile_types():
    return list(_profile_suite.keys())

def get_profile_type_suite(profile_type):
    return _profile_suite[profile_type]

def is_path_in_tree(root, path):
    logging.debug("looking up %r in %r", path, root)

    if root is None:
        logging.debug("no root given")
        return False, None

    check_subdir_trap = False
    subdir_trap = None
    file_trap = None

    path = tuple(path)
    resolution_queue = [(path, root)]
    matches = []
    matched_subdir_traps = []
    matched_file_traps = []
    parts_matched = 0

    while len(resolution_queue) > 0:
        subpath, node = resolution_queue.pop(0)

        if not node.is_root and len(subpath) == 0 or node.is_leaf:
            matches.append(node)

        part = subpath[0]
        for child in node.children:
            is_leaf = (len(subpath) == 1)

            if not child.match(part, trap_file=is_leaf, trap_subdir=not is_leaf):
                logging.debug("%r didn't match %r", subpath, lazy_str(child))
                continue

            if child.is_trap:
                logging.debug("matched %r with trap node: %r", subpath, lazy_str(child))
                if child.is_subdir_trap and not is_leaf:
                    matched_subdir_traps.append(child)
                elif child.is_file_trap and is_leaf:
                    matched_file_traps.append(child)

            elif len(subpath) == 1:
                logging.debug("matched %r to %r", subpath, lazy_str(child))
                matches.append(child)

            else:
                logging.debug("continuing lookup for %r with %r", subpath, lazy_str(child))
                resolution_queue.append((subpath[1:], child))

    if len(matches) > 0:
        logging.debug("matched %r to a regular profile node", path)
    elif len(matched_file_traps) > 0:
        logging.debug("matched %r to a file trap", path)
    elif len(matched_subdir_traps) > 0:
        logging.debug("matched %r to a subdir trap", path)
    else:
        logging.debug("couldn't match %r to profile at all", path)

    matches = matches + matched_file_traps + matched_subdir_traps

    return len(matches) > 0, matches if len(matches) > 0 else None

