#!/usr/bin/env python3

import sys
import re
import anytree
import ipaddress
import logging
import contextlib
import recordclass

from utilities import anytree_helper

class MatchableNode(anytree_helper.TreeNode):
    def __init__(self, *args, is_file_trap=False, is_subdir_trap=False, **kwargs):
        super().__init__(*args, **kwargs)
        self._is_trap = is_file_trap or is_subdir_trap
        self._is_subdir_trap = is_subdir_trap
        self._is_file_trap = is_file_trap

    def match(self, node_or_name, trap_file=True, trap_subdir=True):
        if isinstance(node_or_name, anytree.Node):
            node = node_or_name

            if not node.is_leaf and self.is_subdir_trap:
                return True

            if node.is_leaf and self.is_file_trap:
                return True

            return self.do_match_node(node)

        else:
            if trap_subdir and self.is_subdir_trap:
                return True

            if trap_file and self.is_file_trap:
                return True

            return self.do_match(node_or_name)

    def do_match_node(self, node):
        return self.do_match(node.name)

    def do_match(self, name):
        return self.name == name

    # add the is_trap as a property so it won't appear when rendering the tree
    @property
    def is_trap(self):
        return self._is_trap

    @property
    def is_subdir_trap(self):
        return self._is_subdir_trap

    @is_subdir_trap.setter
    def is_subdir_trap(self, _is_subdir_trap):
        self._is_subdir_trap = _is_subdir_trap
        self._is_trap = self._is_file_trap or self._is_subdir_trap

    @property
    def is_file_trap(self):
        return self._is_file_trap

    @is_file_trap.setter
    def is_file_trap(self, _is_file_trap):
        self._is_file_trap = _is_file_trap
        self._is_trap = self._is_file_trap or self._is_subdir_trap

    def serialization_attributes(self):
        attributes = self.do_serialization_attributes()

        if self.is_trap:
            attributes.setdefault('is_subdir_trap', self.is_subdir_trap)
            attributes.setdefault('is_file_trap', self.is_subdir_trap)

        if hasattr(self, 'extra_content'):
            attributes.setdefault('extra_content', self.extra_content)

        return attributes

    def do_serialization_attributes(self):
        return {}

def simple_matchable_node(file_trap_names, subdir_trap_names):
    with contextlib.suppress(Exception):
        file_trap_names = set(file_trap_names)

    with contextlib.suppress(Exception):
        subdir_trap_names = set(subdir_trap_names)

    class _simple_matchable_node(MatchableNode):
        _file_trap_names = file_trap_names
        _subdir_trap_names = subdir_trap_names

        def __init__(self, name, *args, **kwargs):
            if name in self._file_trap_names:
                kwargs['is_file_trap'] = True

            if name in self._subdir_trap_names:
                kwargs['is_subdir_trap'] = True

            super().__init__(name, *args, **kwargs)

    return _simple_matchable_node

class RegexPathMatchableNode(MatchableNode):
    _PlaceholderProperties = recordclass.recordclass('_PlaceholderProperties', ('regex', 'is_file_trap', 'is_subdir_trap'))

    _PlaceholderTranslationTable = {
        # file system
        'AUTOGEN' : _PlaceholderProperties(r"[0-9a-z]+", False, False),
        'FILE' : _PlaceholderProperties(r"[^\\]+", True, False),
        'SUBFOLDER' : _PlaceholderProperties(r"([^\\]+\\)+[^\\]+", False, True),

        # registry
        'SUBKEY' : _PlaceholderProperties(r"([^\\]+\\)+[^\\]+", False, True),
        'VALUE' : _PlaceholderProperties(r"[^\\]+", True, False),

        # network domains
        'NESTEDSUBDOMAIN' : _PlaceholderProperties(r"([^\.\\/\:]+\.)+[^\.\\/\:]+", False, True),
        'SUBDOMAIN' : _PlaceholderProperties(r"[^\.\\/\:]+", True, False),
    }

    PlaceholderTranslationTable = {
        re.escape(f"[{marker}]") : regex
        for marker, regex in _PlaceholderTranslationTable.items()
    }

    def __init__(self, *args, sep='/', **kwargs):
        super().__init__(*args, **kwargs)

        self._regex = None
        self._node_regex = None
        self._tree_regex = None
        self._separator = re.escape(sep)

        name = re.escape(self.name)

        regex_properties = self.PlaceholderTranslationTable.get(name, None)
        if regex_properties is not None:
            self._regex = regex_properties.regex
            self.is_file_trap = regex_properties.is_file_trap
            self.is_subdir_trap = regex_properties.is_subdir_trap
        else:
            self._regex = name
            for placeholder, _regex in self.PlaceholderTranslationTable.items():
                self._regex = self._regex.replace(placeholder, _regex.regex)

        self._node_regex = re.compile(f"^{self._regex}$", re.I)

    def do_serialization_attributes(self):
        return {
            'tree_regex' : self.tree_regex,
            'regex' : self.node_regex,
        }

    @property
    def node_regex(self):
        return self._node_regex.pattern

    @property
    def tree_regex(self):
        if self._tree_regex is not None:
            return self._tree_regex

        if self.is_leaf:
            self._tree_regex = self._regex
            return self._tree_regex

        queue = [self]
        while len(queue) > 0:
            node = queue.pop(0)

            if not hasattr(node, '_tree_regex') or node._tree_regex is not None:
                continue
            elif node.is_leaf:
                node._tree_regex = node._regex
                continue

            children = node.children
            unresolved_children = []
            resolved_regexes = []

            for child in children:
                if not hasattr(child, '_tree_regex'):
                    continue

                if child._tree_regex is not None:
                    resolved_regexes.append(child._tree_regex)
                else:
                    unresolved_children.append(child)

            if len(unresolved_children) > 0:
                queue = unresolved_children + [node] + queue # re-queue node so it can be re-evaluated when all sub-regexes are resolved
            else:
                node._tree_regex = node._regex + (self._separator if len(node._regex) > 0 else "") + "(" + '|'.join((f"({_regex})" for _regex in resolved_regexes)) + ")"

        if self.is_root:
            self._tree_regex = f"^{self._tree_regex}$"

        return self._tree_regex

    @property
    def do_regex_representation(self):
        return self.tree_regex

    def do_match(self, name):
        return self._node_regex.match(name) is not None

class DomainRegexMatchableNode(RegexPathMatchableNode):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('sep', '.')
        super().__init__(*args, **kwargs)

class NetworkMatchableNode(MatchableNode):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        name = self.name

        self._ip_network = None
        with contextlib.suppress(ValueError):
            self._ip_network = ipaddress.ip_network(name)

    def do_match(self, name):
        # taking a leap of faith here there weren't any problems and the nodes are fine.
        # will be made robust in the future.
        return self.name == name if self._ip_network is None else ipaddress.ip_address(name) in self._ip_network

def test():
    real_root = RegexPathMatchableNode('')
    root = RegexPathMatchableNode('root', parent=real_root)
    child1 = RegexPathMatchableNode('child-1', parent=root)
    child2 = RegexPathMatchableNode('child-2', parent=root)
    gchild11 = RegexPathMatchableNode('grandchild-1-1', parent=child1)
    gchild12 = RegexPathMatchableNode('grandchild-1-2', parent=child1)
    gchild11 = RegexPathMatchableNode('grandchild-2-1', parent=child2)
    gchild11 = RegexPathMatchableNode('grandchild-2-2', parent=child2)

    print(anytree.RenderTree(root))
    print("root tree regex", root.tree_regex)
    print("real root tree regex", real_root.tree_regex)
    print("child 1 tree regex", child1.tree_regex)
    print("child 2 node regex", child2.node_regex)

if __name__ == "__main__":
    sys.exit(test())

