#!/usr/bin/env python3

import argparse
import sys
import anytree
import re
import logging
import contextlib

from utilities import anytree_helper

class _AutogenPartContainerNode(anytree_helper.IndexedChildrenNode):
    def __init__(self, *args, prevalence=None, autogenable=True, autogen=False, **kwargs):
        assert prevalence is not None
        self._prevalence = prevalence

        super().__init__(*args, **kwargs)

        self._autogenable = autogenable
        self.autogen = autogen

    @property
    def autogenable(self):
        return self._autogenable

    @property
    def prevalence(self):
        return self._prevalence

    # an optimization specific to this classes use
    def _post_detach(self, parent):
        pass

    def _post_attach(self, parent):
        super()._post_attach(parent)
        parent._prevalence.update(self._prevalence)

    def add_children(self, *new_children):
        if len(new_children) == 0:
            return

        queue = []
        for new_child in new_children:
            name = new_child.name
            child = self.get_child(name, None)
            if child is None:
                new_child.parent = self
            else:
                queue.append((child, new_child))

        while len(queue) > 0:
            stem, branch = queue.pop(0)
            stem.prevalence.update(branch.prevalence)

            for branch_child in branch.children:
                stem_child = stem.get_child(branch_child.name)
                if stem_child is None:
                    branch_child.parent = stem
                else:
                    queue.append((stem_child, branch_child))

def build_autogen_tree(paths_iterable, min_variations, trap_node_in_autogen_predicate, empty_prevalence_factory=lambda : set()):
    logging.info("building autogen tree - min variations = %d", min_variations)

    root = _AutogenPartContainerNode('root', prevalence=empty_prevalence_factory(), autogenable=False)

    # paths_iterable yields:
    #   path_iterable - yields a part in the path and whether or not that path can be coalesced into a auto-gen placeholder
    #   prevalence - a set-like object that contains prevalence information of the given path
    logging.info("expanding paths from iterable")
    for path_iterable, prevalence in paths_iterable:
        node = root
        root.prevalence.update(prevalence)
        for part, autogenable, properties in path_iterable:
            next_node = node.get_child(part, None)
            if next_node is None:
                next_node = _AutogenPartContainerNode(part, prevalence=empty_prevalence_factory(), autogenable=autogenable, parent=node, **({} if properties is None else properties))

            next_node.prevalence.update(prevalence)
            node = next_node

    logging.info("collapsing autogen nodes")
    queue = [root]
    while len(queue) > 0:
        node = queue.pop(0)
        children = list(node.children)

        trap_candidates = []
        aggregate_prevalence = empty_prevalence_factory()
        for child in children:
            if child.autogenable and trap_node_in_autogen_predicate(child):
                trap_candidates.append(child)
                aggregate_prevalence.update(child.prevalence)

        if min_variations <= len(trap_candidates):
            autogen_node = _AutogenPartContainerNode('autogen', prevalence=aggregate_prevalence, autogenable=False, autogen=True, parent=node)

            grandchildren = sum((list(child.children) for child in trap_candidates), [])
            autogen_node.add_children(*grandchildren)

            for child in trap_candidates:
                child.parent = None

        queue += list(node.children)

    logging.info("done building autogen tree")
    return root

@contextlib.contextmanager
def auto_context(obj):
    yield obj

def _test_tree(args):
    simple_separator = re.compile(r"([0-9a-z]+|[^a-z0-9]+)", re.I)
    chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    _path_parser = lambda path: tuple(((part, part[0] in chars, None) for part in simple_separator.findall(path)))

    def _line_gen():
        _line = 0
        while True:
            _line += 1
            yield _line
    line_gen = _line_gen()

    with (auto_context(sys.stdin) if args.content == '-' else open(args.content)) as input_stream:
        paths_iterable = [(_path_parser(path.strip()), {next(line_gen)}) for path in input_stream]

    autogen_tree = build_autogen_tree(paths_iterable, args.variations, lambda node: len(node.prevalence) <= args.repetitions)

    with (auto_context(sys.stdout) if args.output == "-" else open(args.output, "wt")) as output_stream:
        print(anytree.RenderTree(autogen_tree), file=output_stream)

def test():
    parser = argparse.ArgumentParser(description="test autogen detection.")
    parser.add_argument("-v", "--verbose", action='store_true', help='show debug prints.')
    parser.add_argument("-o", "--output", default="-", help="output file.")

    subparsers = parser.add_subparsers(dest='action')

    tree_parser = subparsers.add_parser('tree', help='render the autogen decomposition tree')
    tree_parser.add_argument("-r", "--repetitions", default=2, type=int, help="the max prevalence for a word to be considered auto-generated.")
    tree_parser.add_argument("-t", "--variations", default=10, type=int, help="the minimum number of variations for a word to be deemed auto-generated.")
    tree_parser.add_argument("content", nargs="?", default="-", help="a file containing a list of strings to ")

    args = parser.parse_args()
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s", level=logging.DEBUG if args.verbose else logging.WARN)

    if args.action == 'tree':
        _test_tree(args)
    else:
        print("invalid action", args.action, file=sys.stderr)
        return 1

if __name__ == "__main__":
    sys.exit(test())

