#!/usr/bin/env python3

import dill
import base64
import logging

import json
import anytree
import sys

from utilities import anytree_helper

class ProfileTreeJsonSerializer:
    '''
    convert a tree to and from a form that is json serializable.
    '''
    def to_json_form(self, tree):
        raise NotImplementedError()

    def from_json_form(self, tree, node_class, **kwargs):
        raise NotImplementedError()

class DefaultProfileTreeJsonSerializer(ProfileTreeJsonSerializer):
    '''
    use this when all else fails.
    '''
    def to_json_form(self, tree):
        logging.warning("Using 'dill' serialization")
        return base64.encodebytes(dill.dumps(regex_tree)).decode()

    def from_json_form(self, json_representation, node_class=None, **kwargs):
        logging.warning("Using 'dill' deserialization - use for development only")
        return dill.loads(base64.decodebytes(json_representation.encode()))

class SimpleTreeJsonSerialization(ProfileTreeJsonSerializer):
    def to_json_form(self, root):
        json_tree = {'children' : {}}
        queue = [(root, json_tree)]
        while len(queue) > 0:
            node, representation = queue.pop(0)
            if hasattr(node, 'serialization_attributes'):
                serialization_attributes = node.serialization_attributes()
                if len(serialization_attributes) > 0:
                    representation['attributes'] = serialization_attributes

            children_container = representation['children']
            for child in node.children:
                child_rep = {'children' : {}}
                children_container[str(child.name)] = child_rep
                queue.append((child, child_rep))

        return {
            'children' : {
                str(root.name) : json_tree
            }
        }

    def from_json_form(self, json_representation, node_class, **kwargs):
        aux_root = node_class('aux_root', **kwargs)
        queue = [(aux_root, json_representation)]
        while len(queue) > 0:
            node, representation = queue.pop(0)
            for child_name, child_desc in representation['children'].items():
                _kwargs = {}
                _kwargs.update(kwargs)
                _kwargs.update(child_desc.get('attributes', {}))
                child_node = node_class(child_name, parent=node, **_kwargs)
                queue.append((child_node, child_desc))

        aux_children = aux_root.children
        assert len(aux_children) == 1

        root = aux_children[0]
        root.parent = None # detach the auxiliary root

        return root

def test():
    root = anytree_helper.TreeNode('it\'s not time')
    child1 = anytree_helper.TreeNode('to make change', parent=root)
    grandchild11 = anytree_helper.TreeNode('just relax', parent=child1)
    grandchild12 = anytree_helper.TreeNode('take it easy', parent=child1)
    child2 = anytree_helper.TreeNode('you\'re still young', parent=root)
    grandchild21 = anytree_helper.TreeNode('that\'s your fault', parent=child2)
    grandchild22 = anytree_helper.TreeNode('there\'s so much you have to know', parent=child2)

    print(anytree.RenderTree(root))
    print(json.dumps(SimpleTreeJsonSerialization().to_json_form(root), indent=4))
    print(anytree.RenderTree(SimpleTreeJsonSerialization().from_json_form(
        json.loads(json.dumps(SimpleTreeJsonSerialization().to_json_form(root), indent=4)),
        anytree_helper.TreeNode
    )))

if __name__ == "__main__":
    sys.exit(test())

