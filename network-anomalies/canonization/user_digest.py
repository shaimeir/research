#!/usr/bin/env python3

import re
import argparse

class UsernameNormalizer:
    def __init__(self):
        # what can i say, we need to support spanish...
        well_known_users = {
            r'^(NT Authority\\)?(Network Service|Servicio de Red)$' : 'Network Service',
            r'^(NT Authority\\)?(Local Service|Servicio Local)$'    : 'Local Service',
            r'^(NT Authority\\)?System$'                            : 'System',
            r'(^|\\)(Administrator|Administrador)$'                 : 'Local Admin',
            r'^NT SERVICE\\'                                        : 'NT Service',
            r'^NT VIRTUAL MACHINE\\'                                : 'NT Virtual Machine',
            r'^NT AUTHORITY\\ANONYMOUS LOGON$'                      : 'Anonymous Logon',
        }

        self.well_known_users = {re.compile(user, re.I) : canonized for user, canonized in well_known_users.items()}
        self.generic_nt_re = re.compile(r'^NT Authority\\', re.I)

    def canonize(self, user):
        for user_re, canonized in self.well_known_users.items():
            if user_re.search(user) is not None:
                return canonized

        if self.generic_nt_re.search(user) is not None:
            return 'Other NT User'

        return 'Generic User'

def test():
    parser = argparse.ArgumentParser(description='canonize user names')
    parser.add_argument('user', nargs='+', help='a list of usernames to canonize')
    args = parser.parse_args()

    canonizer = UsernameNormalizer()

    for username in args.user:
        print(username, '->', canonizer.canonize(username))

if __name__ == "__main__":
    test()

