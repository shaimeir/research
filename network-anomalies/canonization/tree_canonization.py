#!/usr/bin/env python3

import anytree
import recordclass
import logging
import argparse
import sys
import pathlib
import os
from collections import defaultdict

# since this script is executable in and of itself we can't use relative imports in it
from utilities import anytree_helper

class _default_factory:
    def __init__(self, cls, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        self.cls = cls

    def __call__(self, *args, **kwargs):
        return self.cls(*args, *self.args, **self.kwargs, **kwargs)

def make_default_factory(cls):
    return _default_factory(cls)

class PrevalenceCounter:
    def feed(self, entry):
        raise NotImplementedError()

    def consume(self, other):
        raise NotImplementedError()

    def is_empty(self):
        raise NotImplementedError()

class CanonizationMethod:
    def is_canon(self, node):
        raise NotImplemented()

    def __call__(self, node):
        return self.is_canon(node)

def _mandatory_argument(name):
    def _asserter(*a, **kw):
        raise ValueError(f"must specify {name}")

    return _asserter

class CanonizationNode(anytree_helper.ColorfulNicelyRenderedNode):
    def __init__(self, *args, is_trap=False, prevalence_counter_factory=_mandatory_argument("prevalence_counter_factory"), trap_branch_name='[BRANCH]', trap_leaf_name='[LEAF]', **kwargs):
        self.attributes = {}

        self._prevalence_counter_factory = prevalence_counter_factory
        self._prevalence = prevalence_counter_factory()
        self._is_trap = is_trap

        self._scratch = None
        self._created_as_leaf = True

        self._creation_args = args[1:]
        parent = kwargs.pop('parent', None)
        self._creation_kwargs = kwargs

        # the above fields are used when attaching to a parent so they have to be constructed before theanytree.Node constructor is called
        super().__init__(*args, parent=parent, **kwargs)

        if is_trap:
            self._children_map = None
            self._trap_branch = None
            self._trap_leaf = None

        else:
            self._children_map = {}
            self._trap_branch = CanonizationNode(trap_branch_name,
                                                 *self._creation_args,
                                                 parent=self,
                                                 prevalence_counter_factory=prevalence_counter_factory,
                                                 is_trap=True,
                                                 trap_branch_name=None,
                                                 trap_leaf_name=None,
                                                 **self._creation_kwargs)
            self._trap_leaf = CanonizationNode(trap_leaf_name,
                                               *self._creation_args,
                                               parent=self,
                                               prevalence_counter_factory=prevalence_counter_factory,
                                               is_trap=True,
                                               trap_branch_name=None,
                                               trap_leaf_name=None,
                                               **self._creation_kwargs)

    @property
    def created_as_leaf(self):
        return self._created_as_leaf

    @property
    def is_trap(self):
        return self._is_trap

    @property
    def prevalence(self):
        return self._prevalence

    @property
    def is_leaf(self):
        if self._is_trap:
            return True

        return len(self._children_map) == 0 and \
               (self._trap_branch is None or self._trap_branch._prevalence.is_empty()) and \
               (self._trap_leaf is None or self._trap_leaf._prevalence.is_empty())

    def _modify_prevalence(self, prevalence):
        node = self
        while node is not None:
            node._prevalence.consume(prevalence)
            node = node.parent

    def _pre_attach(self, parent):
        if not self._is_trap:
            assert self.name not in parent._children_map, ValueError(f"{self.name} already in {anytree.Node.__str__(parent)}")
            parent._children_map[self.name] = self

    def _post_attach(self, parent):
        parent._modify_prevalence(self._prevalence)
        if not self._is_trap:
            parent._created_as_leaf = False

    _TRACE_DETACHMENTS = (os.getenv("TRACE_DETACHMENTS") is not None)
    def _pre_detach(self, parent):
        if self._TRACE_DETACHMENTS:
            logging.info("detaching %s", self)

    def _post_detach(self, parent):
        if not self._is_trap:
            parent._children_map.pop(self.name)
        # parent._modify_prevalence(-self._prevalence) # this makes stuff more complicated...

    def __getitem__(self, child):
        return self._children_map.get(child, None)

    def __setitem__(self, child_name, child):
        assert child_name == child.name
        current = self[child_name]
        if current is not None:
            current.parent = None

        child.parent = self

    def add(self, child):
        if isinstance(child, str):
            _child = self[child]
            if _child is None:
                _child = CanonizationNode(child,
                                          *self._creation_args,
                                          prevalence_counter_factory=self._prevalence_counter_factory,
                                          parent=self,
                                          trap_branch_name=self._trap_branch.name,
                                          trap_leaf_name=self._trap_leaf.name,
                                          **self._creation_kwargs)
            return _child
        else:
            child.parent = self
            return child

    def trap(self):
        parent = self.parent
        leaf = self.is_leaf

        assert parent is not None
        self.parent = None

        if leaf:
            _trap = parent._trap_leaf
        else:
            _trap = parent._trap_branch

        if _trap is not None:
            _trap._prevalence.consume(self._prevalence)

        return parent

    def _drop_trap(self, is_branch):
        trap = self._trap_branch if is_branch else self._trap_leaf
        if trap is None:
            return

        trap.parent = None
        if is_branch:
            self._trap_branch = None
        else:
            self._trap_leaf = None

    def drop_branch_trap(self):
        self._drop_trap(True)

    def drop_leaf_trap(self):
        self._drop_trap(False)

    @property
    def branch_trap(self):
        return self._trap_branch

    @property
    def leaf_trap(self):
        return self._trap_leaf

    def abandon_all(self):
        if self._is_trap:
            return

        for child in list(self._children_map.values()):
            child.parent = None

        self._children_map = {}

        self._trap_branch = None
        self._trap_leaf = None

@make_default_factory
class IdentifiersPrevalenceCounter(PrevalenceCounter):
    def __init__(self):
        self._identifiers_set = set()

    def feed(self, identifiers):
        if isinstance(identifiers, set) or isinstance(identifiers, frozenset):
            self._identifiers_set |= identifiers
        else:
            self._identifiers_set.add(identifiers)

    def consume(self, other):
        self._identifiers_set |= other._identifiers_set

    def is_empty(self):
        return len(self._identifiers_set) == 0

    @property
    def identifiers(self):
        return self._identifiers_set

    def __len__(self):
        return len(self._identifiers_set)

    def __str__(self):
        return f'IdentifiersPrevalenceCounter({self._identifiers_set})'

    def __repr__(self):
        return str(self)

@make_default_factory
class CountingPrevalenceCounter(PrevalenceCounter):
    def __init__(self):
        self._count = 0

    def feed(self, count):
        self._count += count

    def consume(self, other):
        self._count += other._count

    def is_empty(self):
        return self._count == 0

    @property
    def count(self):
        return self._count

    def __str__(self):
        return f'CountingPrevalenceCounter({self._count})'

    def __repr__(self):
        return str(self)

def build_tree(iterable, prevalence_counter_factory, trap_branch_name='[BRANCH]', trap_leaf_name='[LEAF]', **kwargs):
    root = CanonizationNode('root',
                            prevalence_counter_factory=prevalence_counter_factory,
                            trap_branch_name=trap_branch_name,
                            trap_leaf_name=trap_leaf_name,
                            **kwargs)
    for path_iterable, identifiers in iterable:
        node = root
        for part in path_iterable:
            node.prevalence.feed(identifiers)
            node = node.add(part)

        node.prevalence.feed(identifiers)

    # the root has no meaning for traps because canonization screwes things up a bit in that regard
    root.drop_branch_trap()
    root.drop_leaf_trap()

    return root

def traverse_canon_tree(root):
    queue = [root]

    logging.info("traversing canon tree")
    while len(queue) > 0:
        node = queue.pop(0)

        if node.prevalence.is_empty():
            continue

        if node.is_leaf and not node.is_root: # if it's an empty tree, don't yield the root node
            logging.debug("found a prevalent node %s", node)
            yield node
            continue

        queue = list(node.children) + queue

class _TrapRespectingCanonizationMethod(CanonizationMethod):
    def __init__(self, canonizer):
        super().__init__()
        self.canonizer = canonizer

    def is_canon(self, node):
        if not node.created_as_leaf and not node.is_trap and len(node._children_map) == 0:
            logging.info("%r - checking node traps (%r, %r)", node, self.canonizer(node._trap_branch), self.canonizer(node._trap_leaf))
            return self.canonizer(node._trap_branch) or self.canonizer(node._trap_leaf)
        else:
            return self.canonizer(node)

# in the conext of this function, the _scratch member is used as a flag to tell if a node is queued or not.
_QUEUED_MARKER = 1
def canonize_tree(root, canonizer, debug_print=False):
    logging.info("canonizing tree")

    assert _QUEUED_MARKER is not None # sanity...

    def _enqueue_node(q, node):
        if node._scratch != _QUEUED_MARKER:
            q.append(node)
            node._scratch = _QUEUED_MARKER

    canonizer = _TrapRespectingCanonizationMethod(canonizer)

    logging.info("map non-trap canon paths.")
    queue = [root]
    root._scratch = _QUEUED_MARKER
    while len(queue) > 0:
        node = queue.pop(0)
        node._scratch = None # remove the scratch state

        logging.debug("handling %s, %r", node, node.prevalence)

        if not canonizer(node):
            if node.is_root:
                logging.error("skipping root")
                node.abandon_all()
                break
            else:
                logging.debug("trapping %s", node)
                parent = node.trap()
                _enqueue_node(queue, parent)
                continue

        post_processing_queue = []
        for child in list(node._children_map.values()):
            if not canonizer(child):
                logging.debug("trapping %s", child)
                child.trap()
            else:
                post_processing_queue.append(child)

        # trap the node if we're left with a leaf (no children and no traps) which didn't start out as such
        if not node.is_root and node.is_leaf and not node.created_as_leaf:
            parent = node.trap()
            _enqueue_node(queue, parent)
        else:
            for _n in post_processing_queue:
                _enqueue_node(queue, _n)

    logging.info("removing irrelevant traps")
    queue = [root]
    while len(queue) > 0:
        node = queue.pop(0)

        if node.created_as_leaf or (node._trap_branch is not None and not canonizer(node._trap_branch)):
            logging.debug("%s - dropping branch trap", node)
            node.drop_branch_trap()

        if node.created_as_leaf or (node._trap_leaf is not None and not canonizer(node._trap_leaf)):
            logging.debug("%s - dropping leaf trap", node)
            node.drop_leaf_trap()

        queue += list(node._children_map.values())

    logging.info("tree is canonized!")
    if debug_print:
        print(anytree.RenderTree(root), file=sys.stderr)

    return root

# utility functions and objects that do most of the work for canonizing transformations.
# works only with IdentifiersPrevalenceCounter.

class RelativePrevalenceCountCanonization(CanonizationMethod):
    def __init__(self, relative_prevalence_reference, relative_prevalence_percent, min_occurrences):
        super().__init__()
        self.canon_benchmark = max(relative_prevalence_reference * relative_prevalence_percent, min_occurrences * 100)

    def is_canon(self, node):
        prevalence_count = len(node.prevalence)
        return prevalence_count * 100 >= self.canon_benchmark

def get_relative_prevalence_count_canonizer(relative_prevalence_reference, relative_prevalence_percent, min_occurrences):
    '''
    a utility function that makes sure all input arguments aren't None.
    used with canonizer factories.
    '''
    if relative_prevalence_reference is None or relative_prevalence_percent is None or min_occurrences is None:
        # missing arguments - don't run canonization
        return None

    return RelativePrevalenceCountCanonization(relative_prevalence_reference, relative_prevalence_percent, min_occurrences)

class AbsolutePrevalenceCountCanonization(CanonizationMethod):
    def __init__(self, min_occurrences):
        super().__init__()
        self.min_occurrences = min_occurrences

    def is_canon(self, node):
        logging.info("%r - prevalence: %d", node, len(node.prevalence))
        return len(node.prevalence) >= self.min_occurrences

class PathBuilder:
    def build_path(self, value):
        raise NotImplementedError()

    def reconstruct_path(self, node):
        raise NotImplementedError()

class SimplePathBuilder(PathBuilder):
    def __init__(self, path_columns):
        super().__init__()
        self.path_columns = path_columns

    def _path_decompose(self, *args):
        raise NotImplementedError()

    def build_path(self, value):
        path_cols = tuple((value[col] for col in self.path_columns))
        identifier = tuple((value[col] for col in range(len(value)) if col not in self.path_columns))

        return (self._path_decompose(*path_cols), identifier)

    def _path_reconstruct(self, node):
        raise NotImplementedError()

    def reconstruct_path(self, node):
        path = self._path_reconstruct(node)

        if path is None:
            return

        if not isinstance(path, tuple):
            path = (path, )
        yield from (path + (identifier if isinstance(identifier, tuple) else (identifier, )) for identifier in node.prevalence.identifiers)

class WindowsPathLikePathBuilder(SimplePathBuilder):
    def __init__(self, column):
        super().__init__((column,))

    def _path_decompose(self, path):
        return pathlib.PureWindowsPath(path).parts

    def _path_reconstruct(self, node):
        return str(pathlib.PureWindowsPath(*(_node.name for _node in node.path[1:])))

def canonize_value_map(value_map, canonizer, path_builder, trap_branch_name='[SUBFOLDER]', trap_leaf_name='[FILE]', debug_print=False):
    '''
    value_map - a map from a tuple of aggregated elements to a prevalence count. the prevalence count will be discarded.
    canonization_method - a CanonizationMethod implementation instance.
    path_builder - takes a value (a key in value_map) and transforms it into an iterator that yields the parts of the path with it's identifiers.
                   an instance of an implementation of PathBuilder

    uses an IdentifiersPrevalenceCounter counter.
    '''
    tree = build_tree(
        (path_builder.build_path(value) for value in value_map),
        IdentifiersPrevalenceCounter,
        trap_branch_name=trap_branch_name,
        trap_leaf_name=trap_leaf_name
    )

    canon_paths = canonize_tree(tree, canonizer, debug_print)

    new_value_map = defaultdict(lambda : 0)
    for canon_node in traverse_canon_tree(canon_paths):
        for value in path_builder.reconstruct_path(canon_node):
            new_value_map[value] += 1

    return new_value_map

def canonizing_transformation(aggregator, canonizer_factory, path_builder, trap_branch_name='[SUBFOLDER]', trap_leaf_name='[FILE]', debug_print=False):
    aggregates = aggregator.aggregates
    keys = list(aggregates.keys())
    for key in keys:
        value_map = aggregates.pop(key)

        canonizer = canonizer_factory(key)
        if canonizer is None:
            # values that have no canonizer (e.g. no process prevalence in the process prevalence csv) are
            # removed from the aggregator.
            logging.info("%r was removed because it has no canonizer", key)
            continue

        new_value_map = canonize_value_map(value_map,
                                           canonizer,
                                           path_builder,
                                           trap_branch_name=trap_branch_name,
                                           trap_leaf_name=trap_leaf_name,
                                           debug_print=debug_print)

        if len(new_value_map) > 0:
            aggregates[key] = new_value_map

######### Testing

class _test_canonizer(CanonizationMethod):
    def __init__(self):
        super().__init__()

    def is_canon(self, node):
        if node.is_trap:
            return 'trap' in node.parent.name
        return node.is_root or node.name.startswith('canon')

def _test_path_generator():
    paths = (
        (r"not_ca_non:\not_ca_non_windows\canon_file", "h_1"),
        (r"canon:\canon_root_with_trap_in_name_file", "h_1"),
        (r"canon:\not_ca_non_windows\canon_file", "h_1"),
        (r"canon:\canon_windows\canon_file", "h_1"),
        (r"canon:\canon_windows\not_ca_non_file", "h_1"),
        (r"canon:\canon_windows\non_ca_non_file", "h_1.5"),
        (r"canon:\canon_windows\non_ca_non_dir\f1", "h_1.5"),
        (r"canon:\canon_windows\non_ca_non_dir\f2", "h_1.5"),
        (r"canon:\canon_windows\non_ca_non_dir\f3", "h_1.5"),
        (r"canon:\canon_windows\non_ca_non_dir\d1\f_a", "h_1.5"),
        (r"canon:\canon_windows\non_ca_non_dir\d1\f_b", "h_1.5"),
        (r"canon:\canon_windows\canon_keep_trap\not_ca_non_1", "h_2"),
        (r"canon:\canon_windows\canon_keep_trap\not_ca_non_2", "h_3"),
        (r"canon:\canon_windows\canon_keep_trap\not_ca_non_dir\f1", "h_2_1"),
        (r"canon:\canon_windows\canon_keep_trap\not_ca_non_dir\f2", "h_3_2"),
        (r"canon:\canon_windows\canon_keep_trap\not_ca_non_dir\f1", "h_3_2"),
        (r"canon:\canon_windows\canon_dont_keep_tr_ap\canon_file_1", "h_4"),
        (r"canon:\canon_windows\canon_dont_keep_tr_ap\canon_file_1", "h_5"),
        (r"canon:\canon_windows\canon_dont_keep_tr_ap\not_ca_non_file_1", "h_6"),
        (r"canon:\canon_windows\canon_dont_keep_tr_ap\not_ca_non_file_2", "h_7"),
        (r"canon:\canon_windows\canon_dont_keep_tr_ap\not_ca_non_dir\f_1", "h_6"),
        (r"canon:\canon_windows\canon_dont_keep_tr_ap\not_ca_non_dir\f_2", "h_7"),
    )

    yield from ((pathlib.PureWindowsPath(path).parts, host) for path, host in paths)

def test():
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s", level=logging.DEBUG)

    print("build_tree():")
    root = build_tree(_test_path_generator(), IdentifiersPrevalenceCounter, '[SUBFOLDER]', '[FILE]')
    print(anytree.RenderTree(root))

    print("canonize_tree():")
    root = canonize_tree(root, _test_canonizer())
    print(anytree.RenderTree(root))

if __name__ == "__main__":
    test()

