#!/usr/bin/env python3

'''
Parsing utilities for creation of paths and their reconstruction for profile generation purposes.
'''

import pathlib
import ipaddress
import logging
import argparse
import anytree
import sys

class PathParser:
    def deconstruct(self, entry):
        raise NotImplementedError()

    def reconstruct(self, node):
        raise NotImplementedError()

    # sometimes we need to differentiate the deconstruction of a path from
    # the deconstruction of an entry to check if it's in a tree
    def deconstruct_sample(self, entry):
        return self.deconstruct(entry)

def simple_path_parser(separator):
    class _simple_path_parser(PathParser):
        _separator = separator

        def deconstruct(self, entry):
            return entry.split(self._separator)

        def reconstruct(self, node):
            return self._separator.join((n.name for n in node.path[1:]))

    return _simple_path_parser

class _WindowsPathLikeParser(PathParser):
    def deconstruct(self, entry):
        return pathlib.PureWindowsPath(entry).parts

    def reconstruct(self, node):
        return str(pathlib.PureWindowsPath(*(n.name for n in node.path[1:]))) # skip the root

class WindowsPathParser(_WindowsPathLikeParser):
    pass

class RegistryPathParser(_WindowsPathLikeParser):
    pass

class NetworkIp4PathParser(PathParser):
    def __init__(self, separator=":"):
        self.separator = separator

    def deconstruct(self, entry):
        access, port, subnet = tuple(entry.split(self.separator))
        if "/" not in subnet:
            subnet += "/32"

        subnet = ipaddress.ip_network(subnet)
        return (access, port, ) + tuple((subnet.supernet(prefix) for prefix in range(subnet.prefixlen, -1, -1)))

    def reconstruct(self, node):
        node_path = node.path[1:] # skip the root

        access = "unknown"
        port = "0"
        subnet = "0.0.0.0/0"

        if len(node_path) >= 1:
            access = node_path[0].name

        if len(node_path) >= 2:
            port = node_path[1].name

        if len(node_path) >= 3:
            subnet = node.name

        return f"{access}:{port}:{subnet}"

    # deconstructing an entry for checking if it's in a path in network context is different from deconstructing a profile entry
    def deconstruct_sample(self, entry):
        logging.debug("deconstructing %r", entry)
        access, port, address = tuple(entry.split(self.separator))
        return (access, port,) + (address,) * 33 # 0-32

class DomainPathParser(PathParser):
    def __init__(self, separator=":", domain_sep="."):
        self.separator = separator
        self.domain_sep = domain_sep

    def deconstruct(self, entry):
        access, port, domain = tuple(entry.split(self.separator))
        return (access, port,) + tuple(domain.split(self.domain_sep)[::-1])

    def reconstruct(self, node):
        node_path = node.path[1:]

        access = "unknown"
        port = "0"
        domain = "<unknown>"

        if len(node_path) >= 1:
            access = node_path[0].name

        if len(node_path) >= 2:
            port = node_path[1].name

        if len(node_path) >= 3:
            domain = self.domain_sep.join(reversed([n.name for n in node_path[2:]]))

        return f"{access}:{port}:{domain}"

def test():
    def _to_node(path):
        root = anytree.Node('root')
        node = root
        for element in path:
            node = anytree.Node(element, parent=node)

        return node

    fs_path_parser = WindowsPathParser()
    fs_path = r"c:\a\b\c\d"
    fs_path_decomposition = fs_path_parser.deconstruct(fs_path)
    fs_path_reconstruction = fs_path_parser.reconstruct(_to_node(fs_path_decomposition))
    print(fs_path, "==>", fs_path_decomposition, "==>", fs_path_reconstruction)

    reg_path_parser = RegistryPathParser()
    reg_path = r"hklm\a\b\c\d"
    reg_path_decomposition = reg_path_parser.deconstruct(reg_path)
    reg_path_reconstruction = reg_path_parser.reconstruct(_to_node(reg_path_decomposition))
    print(reg_path, "==>", reg_path_decomposition, "==>", reg_path_reconstruction)

    net_path_parser = NetworkIp4PathParser()
    net_path = r"some-access:1337:1.2.3.0/24"
    net_path_decomposition = net_path_parser.deconstruct(net_path)
    net_path_reconstruction = net_path_parser.reconstruct(_to_node(net_path_decomposition))
    print(net_path, "==>", net_path_decomposition, "==>", net_path_reconstruction)

    domain_path_parser = DomainPathParser()
    domain_path = r"some-access:1337:scenes.from.an.italian.resturant"
    domain_path_decomposition = domain_path_parser.deconstruct(domain_path)
    domain_path_reconstruction = domain_path_parser.reconstruct(_to_node(domain_path_decomposition))
    print(domain_path, "==>", domain_path_decomposition, "==>", domain_path_reconstruction)

if __name__ == "__main__":
    sys.exit(test())

