#!/usr/bin/env python3

import argparse
import sys
import pprint
import re
import logging
import contextlib
import termcolor
from collections import namedtuple

class RegistryCanonizer:
    def __init__(self):
        self.backslash_coalesce = re.compile(r"\\+")
        self.guid_re = re.compile(r"[{(]?[0-9A-F]{8}-([0-9A-F]{4}-){3}[0-9A-F]{12}[)}]?", re.I)
        self.version_re = re.compile(r'(\b|_)(\d+[\._]\d+([\._]\d+)*)(?=\b|_)', re.I)
        # self.version_re = re.compile(r'(?!<[0-9a-f])(((?:\d+)(?:[._](?:\d+|[0-9a-f]+))+)(?:\-(?:\d+|[0-9a-f]+))*)(?=([^0-9._\-a-f]+|\b))', re.I)
        self.sid_re = re.compile(r'\b(?P<sid>s-\d+-\d+-\d+(-\d{,10})*)\b', re.I)
        # a blob, a string with mixed alpha/num (must have both) but not start with amd or win or lin or x (exclude for amd64, win32, x64/86 ).
        # as a restriction, we demand that the blob start with a digit and be at least 4 letters long
        self.hash_re = re.compile(r'(\b|_)((?:[0-9a-f]{16,}){1,16}|[0-9a-f]{40,})(?=\b|_)', re.I)
        self.number_re = re.compile(r'(\b|_)(\d+)(?=\b|_)', re.I)

    def canonize(self, registry_path):
        registry_path = registry_path.strip().lower()

        registry_path = self.guid_re.sub(r"[GUID]", registry_path)
        registry_path = self.sid_re.sub(r"[SID]", registry_path)
        registry_path = self.hash_re.sub(r"\1[HASH]", registry_path)
        registry_path = self.backslash_coalesce.sub(r"\\", registry_path)
        registry_path = self.version_re.sub(r"\1[VERSION]", registry_path) # this might corrupt ip addresses so it can't be done before the smb checking
        registry_path = self.number_re.sub(r"\1[NUM]", registry_path)

        if registry_path.endswith("\\"):
            registry_path += '(Default)'

        return registry_path

def get_canonizer():
    return RegistryCanonizer()

@contextlib.contextmanager
def _auto_context(obj):
    yield obj

def main():
    parser = argparse.ArgumentParser(description="test registry path canonization.")
    parser.add_argument("-f", "--file", action='append', default=[], help="a file with a list of registry paths to canonize.")
    parser.add_argument("-v", "--verbose", action='store_true', help='show debug prints')
    parser.add_argument('paths', nargs='*', default=[], help="a list of registry paths to canonize.")
    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.WARN, format="%(asctime)s %(message)s")

    def _generate_reg_paths():
        for _file in args.file:
            with (_auto_context(sys.stdin) if "-" == _file else open(_file, "rt")) as __file:
                yield from __file

        yield from args.paths

    canonizer = get_canonizer()

    for reg_path in _generate_reg_paths():
        canon_reg = canonizer.canonize(reg_path)
        print(termcolor.colored(reg_path, 'cyan'), termcolor.colored(canon_reg, 'yellow'))

if __name__ == "__main__":
    sys.exit(main())

