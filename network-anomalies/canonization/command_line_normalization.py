#!/usr/bin/env python3

import shlex
import sys
import logging
import pathlib
import re
import contextlib
import argparse
from collections import defaultdict

try:
    import detect_random
except Exception:
    from . import detect_random

class CommandLineNormalizer:
    MAX_ARG_LEN = 31 # this conviniently catches the longest svchost.exe -k argument, what a lucky coincidence

    def __init__(self, strong, random_word_chain_threshold=None, cmd_type='windows'):
        cmd_type = cmd_type.strip().lower()
        assert cmd_type in ('windows', 'posix')

        self.random_word_chain_threshold = random_word_chain_threshold
        self.cmd_type = cmd_type
        self.is_posix = cmd_type == 'posix'
        self.path_parser = pathlib.PurePosixPath if self.is_posix else pathlib.PureWindowsPath
        self.is_switch = (lambda arg: arg.startswith("-")) if self.is_posix else (lambda arg: arg.startswith("-") or arg.startswith("/"))

        self.switch_breakdown = self._posix_switch_breakdown if self.is_posix else self._windows_switch_breakdown
        self.normalize = self.strong_normalization if strong else self.weak_normalization

        self.non_alphanum = re.compile(r"[^a-z0-9_\-]+", re.I)
        self.hash_re = re.compile(r'^((?:[0-9a-f]{16,}){1,16}|[0-9a-f]{40,})$', re.I)
        self.number_re = re.compile(r'^(\d+)$', re.I)
        self.url_separator = re.compile(r"^\:\/\/+$", re.I) # this might not be good enough for linux paths with a directory that contains ':' in it's name

        self.win_arg_separators = re.compile("[" + re.escape(":,") + "]")
        self.posix_arg_separators = re.compile("[" + re.escape("=") + "]")

    def _normalize_arg(self, argument):
        # if there's anything like :;, etc it's a composite argument as far as we're concerned
        non_alphanum_seqs = self.non_alphanum.findall(argument)
        if non_alphanum_seqs is not None and len(non_alphanum_seqs) > 0:
            if self.url_separator.match(non_alphanum_seqs[0]):
                return '[URL]'

            return '[ARGUMENT]'

        if len(argument) > self.MAX_ARG_LEN:
            return '[ARGUMENT]'

        if self.hash_re.match(argument):
            return '[HASH]'

        if self.number_re.match(argument):
            return '[ARGUMENT]'

        # evaluate the randomness if the
        return argument if self.random_word_chain_threshold is None or detect_random.check(argument, self.random_word_chain_threshold) else "[ARGUMENT]"

    def __switch_breakdown(self, switch, prefix, inline_arg_sep):
        if not switch.startswith(prefix):
            return (None, None, switch)

        breakdown = tuple(re.split(inline_arg_sep, switch, 1))

        arg = None if len(breakdown) == 1 else breakdown[1]
        switch = breakdown[0]

        _prefix_len = 0
        for _c in switch:
            if _c != prefix:
                break

            _prefix_len += 1

        return (switch[:_prefix_len], switch[_prefix_len:], arg)

    def _posix_switch_breakdown(self, switch):
        return self.__switch_breakdown(switch, "-", self.posix_arg_separators)

    def _windows_switch_breakdown(self, switch):
        prefix, _switch, arg = self.__switch_breakdown(switch, "/", self.win_arg_separators)
        return ("/", _switch, arg) if prefix is not None else self.__switch_breakdown(switch, "-", self.posix_arg_separators)

    def _prepare_cmd_for_parsing(self, cmd):
        cmd = cmd.strip().lower()
        return cmd if self.is_posix else cmd.replace("\\","\\\\")

    def weak_normalization(self, cmd_line, process_name=None):
        '''
        unify the command line argument convention to:
            --switch arg
            -s arg
            /switch arg

        and perform argument normalization, but keep original switches order and occurrences.
        '''
        logging.debug("normalizing (weak): %r", cmd_line)

        to_parse = cmd_line
        parse_state = 'none'
        while to_parse is not None:
            try:
                cmd_parts = shlex.split(self._prepare_cmd_for_parsing(to_parse), posix=True) # always parse as a posix command line because the non-posix parsing just isn't good enough
                to_parse = None

            except ValueError as err:
                if err.args[0] == 'No closing quotation':
                    if parse_state == 'none':
                        parse_state = 'double-quote'
                        to_parse = cmd_line + '"'
                    elif 'double-quote' == parse_state:
                        parse_state = 'single-quote'
                        to_parse = cmd_line + "'"
                    else:
                        raise
                else:
                    raise

        logging.debug("cmd parts: %r", cmd_parts)

        if len(cmd_parts) == 0:
            return ''

        executable_name = self.path_parser(cmd_parts[0].strip("\"'")).name # a decent assumption - we may need to handle the case where the command line is fully arbitrary (as the case in linux)
        logging.debug("exe name: %r", executable_name)

        normalized_cmd = [executable_name]

        cmd_parts = cmd_parts[1:]
        while len(cmd_parts) > 0:
            prefix, switch, argument = self.switch_breakdown(cmd_parts.pop(0).replace('"', '')) # quotes will be used for quoting the command line later

            if argument is None and len(cmd_parts) > 0:
                # check if the next part can be an argument
                _arg = cmd_parts[0]
                _prefix, _, __ = self.switch_breakdown(_arg)
                if _prefix is None:
                    argument = cmd_parts.pop(0)

            if prefix is not None:
                normalized_cmd.append(prefix + switch)

            if argument is not None:
                normalized_cmd.append(self._normalize_arg(argument))

        return ' '.join((f"'{c}'" for c in normalized_cmd))

    def strong_normalization(self, cmd_line):
        '''
        unify the command line argument convention to:
            --switch arg
            -s arg
            /switch arg

        discard original command line entries order (between switches and arguments as well)
        and transform the order (while discarding repetitions) into the following form:
            -a -b -c --long-switch-1 --long-switch-2 --long-switch-3 arg1 arg2 arg3
        '''
        logging.debug("normalizing (strong): %r", cmd_line)

        cmd_parts = shlex.split(self._prepare_cmd_for_parsing(cmd_line), posix=True) # always parse as a posix command line because the non-posix parsing just isn't good enough
        logging.debug("cmd parts: %r", cmd_parts)

        if len(cmd_parts) == 0:
            return ''

        executable_name = self.path_parser(cmd_parts[0].strip("\"'")).name # a decent assumption
        logging.debug("exe name: %r", executable_name)

        cmd_parts = cmd_parts[1:]
        switches = defaultdict(set)
        args = set()
        while len(cmd_parts) > 0:
            prefix, switch, argument = self.switch_breakdown(cmd_parts.pop(0).replace('"', '')) # quotes will be used for quoting the command line later

            if argument is not None:
                args.add(self._normalize_arg(argument))

            if prefix is not None:
                switches[prefix].add(switch)

        normalized_cmd = [executable_name]

        prefixes = sorted(switches.keys())
        for prefix in prefixes:
            normalized_cmd += sorted((prefix + switch for switch in switches[prefix]))

        normalized_cmd += sorted(args)

        return ' '.join((f"'{c}'" for c in normalized_cmd))

@contextlib.contextmanager
def auto_context(obj):
    yield obj

def test():
    parser = argparse.ArgumentParser(description="test command line normalization.")
    parser.add_argument("-v", "--verbose", action="store_true", help="show debug prints.")
    parser.add_argument("-r", "--random-detection", type=float, default=None, help="use the random word detection module with a given threshold.")
    parser.add_argument("-s", "--strong-normalization", action="store_true", help="use strong normalization.")
    parser.add_argument("-d", "--default", action='store_true', help="run on built in test list instead of stdin.")

    args = parser.parse_args()

    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s", level=logging.DEBUG if args.verbose else logging.WARN)

    detect_random.init()

    normalizer = CommandLineNormalizer(args.strong_normalization, args.random_detection, 'windows')

    default_test_windows_cmds = [
        r'"C:\some-directory\chrome.exe" --type=renderer --disable-background-timer-throttling --use-gl=osmesa --service-pipe-token=0BA3B407429B40654EA118D0FCFD49C3 --lang=en-US --headless --enable-pinch --device-scale-factor=1 --num-raster-threads=4 --enable-main-frame-before-activation --content-image-texture-target=0,0,3553;0,1,3553;0,2,3553;0,3,3553;0,4,3553;0,5,3553;0,6,3553;0,7,3553;0,8,3553;0,9,3553;0,10,3553;0,11,3553;0,12,3553;0,13,3553;0,14,3553;0,15,3553;0,16,3553;0,17,3553;1,0,3553;1,1,3553;1,2,3553;1,3,3553;1,4,3553;1,5,3553;1,6,3553;1,7,3553;1,8,3553;1,9,3553;1,10,3553;1,11,3553;1,12,3553;1,13,3553;1,14,3553;1,15,3553;1,16,3553;1,17,3553;2,0,3553;2,1,3553;2,2,3553;2,3,3553;2,4,3553;2,5,3553;2,6,3553;2,7,3553;2,8,3553;2,9,3553;2,10,3553;2,11,3553;2,12,3553;2,13,3553;2,14,3553;2,15,3553;2,16,3553;2,17,3553;3,0,3553;3,1,3553;3,2,3553;3,3,3553;3,4,3553;3,5,3553;3,6,3553;3,7,3553;3,8,3553;3,9,3553;3,10,3553;3,11,3553;3,12,3553;3,13,3553;3,14,3553;3,15,3553;3,16,3553;3,17,3553;4,0,3553;4,1,3553;4,2,3553;4,3,3553;4,4,3553;4,5,3553;4,6,3553;4,7,3553;4,8,3553;4,9,3553;4,10,3553;4,11,3553;4,12,3553;4,13,3553;4,14,3553;4,15,3553;4,16,3553;4,17,3553;5,0,3553;5,1,3553;5,2,3553;5,3,3553;5,4,3553;5,5,3553;5,6,3553;5,7,3553;5,8,3553;5,9,3553;5,10,3553;5,11,3553;5,12,3553;5,13,3553;5,14,3553;5,15,3553;5,16,3553;5,17,3553 --disable-accelerated-video-decode --disable-gpu-compositing --enable-gpu-async-worker-context --service-request-channel-token=0BA3B407429B40654EA118D0FCFD49C3 --renderer-client-id=2 --mojo-platform-channel-handle=1392 /prefetch:1',
        r'"C:\some-directory\chrome.exe" --type=renderer --disable-background-timer-throttling --use-gl=osmesa --service-pipe-token=0DFB7EDDCD64EBACB3FDE50511B8B174 --lang=en-US --headless --enable-pinch --device-scale-factor=1 --num-raster-threads=4 --enable-main-frame-before-activation --content-image-texture-target=0,0,3553;0,1,3553;0,2,3553;0,3,3553;0,4,3553;0,5,3553;0,6,3553;0,7,3553;0,8,3553;0,9,3553;0,10,3553;0,11,3553;0,12,3553;0,13,3553;0,14,3553;0,15,3553;0,16,3553;0,17,3553;1,0,3553;1,1,3553;1,2,3553;1,3,3553;1,4,3553;1,5,3553;1,6,3553;1,7,3553;1,8,3553;1,9,3553;1,10,3553;1,11,3553;1,12,3553;1,13,3553;1,14,3553;1,15,3553;1,16,3553;1,17,3553;2,0,3553;2,1,3553;2,2,3553;2,3,3553;2,4,3553;2,5,3553;2,6,3553;2,7,3553;2,8,3553;2,9,3553;2,10,3553;2,11,3553;2,12,3553;2,13,3553;2,14,3553;2,15,3553;2,16,3553;2,17,3553;3,0,3553;3,1,3553;3,2,3553;3,3,3553;3,4,3553;3,5,3553;3,6,3553;3,7,3553;3,8,3553;3,9,3553;3,10,3553;3,11,3553;3,12,3553;3,13,3553;3,14,3553;3,15,3553;3,16,3553;3,17,3553;4,0,3553;4,1,3553;4,2,3553;4,3,3553;4,4,3553;4,5,3553;4,6,3553;4,7,3553;4,8,3553;4,9,3553;4,10,3553;4,11,3553;4,12,3553;4,13,3553;4,14,3553;4,15,3553;4,16,3553;4,17,3553;5,0,3553;5,1,3553;5,2,3553;5,3,3553;5,4,3553;5,5,3553;5,6,3553;5,7,3553;5,8,3553;5,9,3553;5,10,3553;5,11,3553;5,12,3553;5,13,3553;5,14,3553;5,15,3553;5,16,3553;5,17,3553 --disable-accelerated-video-decode --disable-gpu-compositing --enable-gpu-async-worker-context --service-request-channel-token=0DFB7EDDCD64EBACB3FDE50511B8B174 --renderer-client-id=3 --mojo-platform-channel-handle=1616 /prefetch:1',
        r'"C:\some-directory\chrome.exe" --type=renderer --disable-background-timer-throttling --use-gl=osmesa --service-pipe-token=14F44035742EB69CF48B0C331BE7A6CB --lang=en-US --headless --enable-pinch --device-scale-factor=1 --num-raster-threads=4 --enable-main-frame-before-activation --content-image-texture-target=0,0,3553;0,1,3553;0,2,3553;0,3,3553;0,4,3553;0,5,3553;0,6,3553;0,7,3553;0,8,3553;0,9,3553;0,10,3553;0,11,3553;0,12,3553;0,13,3553;0,14,3553;0,15,3553;0,16,3553;0,17,3553;1,0,3553;1,1,3553;1,2,3553;1,3,3553;1,4,3553;1,5,3553;1,6,3553;1,7,3553;1,8,3553;1,9,3553;1,10,3553;1,11,3553;1,12,3553;1,13,3553;1,14,3553;1,15,3553;1,16,3553;1,17,3553;2,0,3553;2,1,3553;2,2,3553;2,3,3553;2,4,3553;2,5,3553;2,6,3553;2,7,3553;2,8,3553;2,9,3553;2,10,3553;2,11,3553;2,12,3553;2,13,3553;2,14,3553;2,15,3553;2,16,3553;2,17,3553;3,0,3553;3,1,3553;3,2,3553;3,3,3553;3,4,3553;3,5,3553;3,6,3553;3,7,3553;3,8,3553;3,9,3553;3,10,3553;3,11,3553;3,12,3553;3,13,3553;3,14,3553;3,15,3553;3,16,3553;3,17,3553;4,0,3553;4,1,3553;4,2,3553;4,3,3553;4,4,3553;4,5,3553;4,6,3553;4,7,3553;4,8,3553;4,9,3553;4,10,3553;4,11,3553;4,12,3553;4,13,3553;4,14,3553;4,15,3553;4,16,3553;4,17,3553;5,0,3553;5,1,3553;5,2,3553;5,3,3553;5,4,3553;5,5,3553;5,6,3553;5,7,3553;5,8,3553;5,9,3553;5,10,3553;5,11,3553;5,12,3553;5,13,3553;5,14,3553;5,15,3553;5,16,3553;5,17,3553 --disable-accelerated-video-decode --disable-gpu-compositing --enable-gpu-async-worker-context --service-request-channel-token=14F44035742EB69CF48B0C331BE7A6CB --renderer-client-id=3 --mojo-platform-channel-handle=1620 /prefetch:1',
        r'"C:\some-directory\chrome.exe" --type=renderer --disable-background-timer-throttling --use-gl=osmesa --service-pipe-token=1B4788E0014CC2A145F2A024E0890339 --lang=en-US --headless --enable-pinch --device-scale-factor=1 --num-raster-threads=4 --enable-main-frame-before-activation --content-image-texture-target=0,0,3553;0,1,3553;0,2,3553;0,3,3553;0,4,3553;0,5,3553;0,6,3553;0,7,3553;0,8,3553;0,9,3553;0,10,3553;0,11,3553;0,12,3553;0,13,3553;0,14,3553;0,15,3553;0,16,3553;0,17,3553;1,0,3553;1,1,3553;1,2,3553;1,3,3553;1,4,3553;1,5,3553;1,6,3553;1,7,3553;1,8,3553;1,9,3553;1,10,3553;1,11,3553;1,12,3553;1,13,3553;1,14,3553;1,15,3553;1,16,3553;1,17,3553;2,0,3553;2,1,3553;2,2,3553;2,3,3553;2,4,3553;2,5,3553;2,6,3553;2,7,3553;2,8,3553;2,9,3553;2,10,3553;2,11,3553;2,12,3553;2,13,3553;2,14,3553;2,15,3553;2,16,3553;2,17,3553;3,0,3553;3,1,3553;3,2,3553;3,3,3553;3,4,3553;3,5,3553;3,6,3553;3,7,3553;3,8,3553;3,9,3553;3,10,3553;3,11,3553;3,12,3553;3,13,3553;3,14,3553;3,15,3553;3,16,3553;3,17,3553;4,0,3553;4,1,3553;4,2,3553;4,3,3553;4,4,3553;4,5,3553;4,6,3553;4,7,3553;4,8,3553;4,9,3553;4,10,3553;4,11,3553;4,12,3553;4,13,3553;4,14,3553;4,15,3553;4,16,3553;4,17,3553;5,0,3553;5,1,3553;5,2,3553;5,3,3553;5,4,3553;5,5,3553;5,6,3553;5,7,3553;5,8,3553;5,9,3553;5,10,3553;5,11,3553;5,12,3553;5,13,3553;5,14,3553;5,15,3553;5,16,3553;5,17,3553 --disable-accelerated-video-decode --disable-gpu-compositing --enable-gpu-async-worker-context --service-request-channel-token=1B4788E0014CC2A145F2A024E0890339 --renderer-client-id=3 --mojo-platform-channel-handle=1524 /prefetch:1',
        r'"C:\some-directory\chrome.exe" --type=renderer --disable-background-timer-throttling --use-gl=osmesa --service-pipe-token=31A1F0A92E8EF20A815157DFC7909D52 --lang=en-US --headless --enable-pinch --device-scale-factor=1 --num-raster-threads=4 --enable-main-frame-before-activation --content-image-texture-target=0,0,3553;0,1,3553;0,2,3553;0,3,3553;0,4,3553;0,5,3553;0,6,3553;0,7,3553;0,8,3553;0,9,3553;0,10,3553;0,11,3553;0,12,3553;0,13,3553;0,14,3553;0,15,3553;0,16,3553;0,17,3553;1,0,3553;1,1,3553;1,2,3553;1,3,3553;1,4,3553;1,5,3553;1,6,3553;1,7,3553;1,8,3553;1,9,3553;1,10,3553;1,11,3553;1,12,3553;1,13,3553;1,14,3553;1,15,3553;1,16,3553;1,17,3553;2,0,3553;2,1,3553;2,2,3553;2,3,3553;2,4,3553;2,5,3553;2,6,3553;2,7,3553;2,8,3553;2,9,3553;2,10,3553;2,11,3553;2,12,3553;2,13,3553;2,14,3553;2,15,3553;2,16,3553;2,17,3553;3,0,3553;3,1,3553;3,2,3553;3,3,3553;3,4,3553;3,5,3553;3,6,3553;3,7,3553;3,8,3553;3,9,3553;3,10,3553;3,11,3553;3,12,3553;3,13,3553;3,14,3553;3,15,3553;3,16,3553;3,17,3553;4,0,3553;4,1,3553;4,2,3553;4,3,3553;4,4,3553;4,5,3553;4,6,3553;4,7,3553;4,8,3553;4,9,3553;4,10,3553;4,11,3553;4,12,3553;4,13,3553;4,14,3553;4,15,3553;4,16,3553;4,17,3553;5,0,3553;5,1,3553;5,2,3553;5,3,3553;5,4,3553;5,5,3553;5,6,3553;5,7,3553;5,8,3553;5,9,3553;5,10,3553;5,11,3553;5,12,3553;5,13,3553;5,14,3553;5,15,3553;5,16,3553;5,17,3553 --disable-accelerated-video-decode --disable-gpu-compositing --enable-gpu-async-worker-context --service-request-channel-token=31A1F0A92E8EF20A815157DFC7909D52 --renderer-client-id=3 --mojo-platform-channel-handle=1612 /prefetch:1',
        r'"C:\some-directory\chrome.exe" --type=renderer --disable-background-timer-throttling --use-gl=osmesa --service-pipe-token=3337BBB4EF1F4FE597DF194B483F98F2 --lang=en-US --headless --enable-pinch --device-scale-factor=1 --num-raster-threads=4 --enable-main-frame-before-activation --content-image-texture-target=0,0,3553;0,1,3553;0,2,3553;0,3,3553;0,4,3553;0,5,3553;0,6,3553;0,7,3553;0,8,3553;0,9,3553;0,10,3553;0,11,3553;0,12,3553;0,13,3553;0,14,3553;0,15,3553;0,16,3553;0,17,3553;1,0,3553;1,1,3553;1,2,3553;1,3,3553;1,4,3553;1,5,3553;1,6,3553;1,7,3553;1,8,3553;1,9,3553;1,10,3553;1,11,3553;1,12,3553;1,13,3553;1,14,3553;1,15,3553;1,16,3553;1,17,3553;2,0,3553;2,1,3553;2,2,3553;2,3,3553;2,4,3553;2,5,3553;2,6,3553;2,7,3553;2,8,3553;2,9,3553;2,10,3553;2,11,3553;2,12,3553;2,13,3553;2,14,3553;2,15,3553;2,16,3553;2,17,3553;3,0,3553;3,1,3553;3,2,3553;3,3,3553;3,4,3553;3,5,3553;3,6,3553;3,7,3553;3,8,3553;3,9,3553;3,10,3553;3,11,3553;3,12,3553;3,13,3553;3,14,3553;3,15,3553;3,16,3553;3,17,3553;4,0,3553;4,1,3553;4,2,3553;4,3,3553;4,4,3553;4,5,3553;4,6,3553;4,7,3553;4,8,3553;4,9,3553;4,10,3553;4,11,3553;4,12,3553;4,13,3553;4,14,3553;4,15,3553;4,16,3553;4,17,3553;5,0,3553;5,1,3553;5,2,3553;5,3,3553;5,4,3553;5,5,3553;5,6,3553;5,7,3553;5,8,3553;5,9,3553;5,10,3553;5,11,3553;5,12,3553;5,13,3553;5,14,3553;5,15,3553;5,16,3553;5,17,3553 --disable-accelerated-video-decode --disable-gpu-compositing --enable-gpu-async-worker-context --service-request-channel-token=3337BBB4EF1F4FE597DF194B483F98F2 --renderer-client-id=5 --mojo-platform-channel-handle=1648 /prefetch:1',
        r'"C:\some-directory\chrome.exe" --type=renderer --disable-background-timer-throttling --use-gl=osmesa --service-pipe-token=39A62562ED27DA23BE530DD82CB251B4 --lang=en-US --headless --enable-pinch --device-scale-factor=1 --num-raster-threads=4 --enable-main-frame-before-activation --content-image-texture-target=0,0,3553;0,1,3553;0,2,3553;0,3,3553;0,4,3553;0,5,3553;0,6,3553;0,7,3553;0,8,3553;0,9,3553;0,10,3553;0,11,3553;0,12,3553;0,13,3553;0,14,3553;0,15,3553;0,16,3553;0,17,3553;1,0,3553;1,1,3553;1,2,3553;1,3,3553;1,4,3553;1,5,3553;1,6,3553;1,7,3553;1,8,3553;1,9,3553;1,10,3553;1,11,3553;1,12,3553;1,13,3553;1,14,3553;1,15,3553;1,16,3553;1,17,3553;2,0,3553;2,1,3553;2,2,3553;2,3,3553;2,4,3553;2,5,3553;2,6,3553;2,7,3553;2,8,3553;2,9,3553;2,10,3553;2,11,3553;2,12,3553;2,13,3553;2,14,3553;2,15,3553;2,16,3553;2,17,3553;3,0,3553;3,1,3553;3,2,3553;3,3,3553;3,4,3553;3,5,3553;3,6,3553;3,7,3553;3,8,3553;3,9,3553;3,10,3553;3,11,3553;3,12,3553;3,13,3553;3,14,3553;3,15,3553;3,16,3553;3,17,3553;4,0,3553;4,1,3553;4,2,3553;4,3,3553;4,4,3553;4,5,3553;4,6,3553;4,7,3553;4,8,3553;4,9,3553;4,10,3553;4,11,3553;4,12,3553;4,13,3553;4,14,3553;4,15,3553;4,16,3553;4,17,3553;5,0,3553;5,1,3553;5,2,3553;5,3,3553;5,4,3553;5,5,3553;5,6,3553;5,7,3553;5,8,3553;5,9,3553;5,10,3553;5,11,3553;5,12,3553;5,13,3553;5,14,3553;5,15,3553;5,16,3553;5,17,3553 --disable-accelerated-video-decode --disable-gpu-compositing --enable-gpu-async-worker-context --service-request-channel-token=39A62562ED27DA23BE530DD82CB251B4 --renderer-client-id=2 --mojo-platform-channel-handle=1400 /prefetch:1',
        r'"C:\some-directory\chrome.exe" --type=renderer --disable-background-timer-throttling --use-gl=osmesa --service-pipe-token=3B9B248A7B78657A289D74DC68EA3526 --lang=en-US --headless --enable-pinch --device-scale-factor=1 --num-raster-threads=4 --enable-main-frame-before-activation --content-image-texture-target=0,0,3553;0,1,3553;0,2,3553;0,3,3553;0,4,3553;0,5,3553;0,6,3553;0,7,3553;0,8,3553;0,9,3553;0,10,3553;0,11,3553;0,12,3553;0,13,3553;0,14,3553;0,15,3553;0,16,3553;0,17,3553;1,0,3553;1,1,3553;1,2,3553;1,3,3553;1,4,3553;1,5,3553;1,6,3553;1,7,3553;1,8,3553;1,9,3553;1,10,3553;1,11,3553;1,12,3553;1,13,3553;1,14,3553;1,15,3553;1,16,3553;1,17,3553;2,0,3553;2,1,3553;2,2,3553;2,3,3553;2,4,3553;2,5,3553;2,6,3553;2,7,3553;2,8,3553;2,9,3553;2,10,3553;2,11,3553;2,12,3553;2,13,3553;2,14,3553;2,15,3553;2,16,3553;2,17,3553;3,0,3553;3,1,3553;3,2,3553;3,3,3553;3,4,3553;3,5,3553;3,6,3553;3,7,3553;3,8,3553;3,9,3553;3,10,3553;3,11,3553;3,12,3553;3,13,3553;3,14,3553;3,15,3553;3,16,3553;3,17,3553;4,0,3553;4,1,3553;4,2,3553;4,3,3553;4,4,3553;4,5,3553;4,6,3553;4,7,3553;4,8,3553;4,9,3553;4,10,3553;4,11,3553;4,12,3553;4,13,3553;4,14,3553;4,15,3553;4,16,3553;4,17,3553;5,0,3553;5,1,3553;5,2,3553;5,3,3553;5,4,3553;5,5,3553;5,6,3553;5,7,3553;5,8,3553;5,9,3553;5,10,3553;5,11,3553;5,12,3553;5,13,3553;5,14,3553;5,15,3553;5,16,3553;5,17,3553 --disable-accelerated-video-decode --disable-gpu-compositing --enable-gpu-async-worker-context --service-request-channel-token=3B9B248A7B78657A289D74DC68EA3526 --renderer-client-id=5 --mojo-platform-channel-handle=1640 /prefetch:1',
        r'"C:\some-directory\chrome.exe" --type=renderer --disable-background-timer-throttling --use-gl=osmesa --service-pipe-token=3D24CF9ECCAD56C0D609B6AB4B4518C8 --lang=en-US --headless --enable-pinch --device-scale-factor=1 --num-raster-threads=4 --enable-main-frame-before-activation --content-image-texture-target=0,0,3553;0,1,3553;0,2,3553;0,3,3553;0,4,3553;0,5,3553;0,6,3553;0,7,3553;0,8,3553;0,9,3553;0,10,3553;0,11,3553;0,12,3553;0,13,3553;0,14,3553;0,15,3553;0,16,3553;0,17,3553;1,0,3553;1,1,3553;1,2,3553;1,3,3553;1,4,3553;1,5,3553;1,6,3553;1,7,3553;1,8,3553;1,9,3553;1,10,3553;1,11,3553;1,12,3553;1,13,3553;1,14,3553;1,15,3553;1,16,3553;1,17,3553;2,0,3553;2,1,3553;2,2,3553;2,3,3553;2,4,3553;2,5,3553;2,6,3553;2,7,3553;2,8,3553;2,9,3553;2,10,3553;2,11,3553;2,12,3553;2,13,3553;2,14,3553;2,15,3553;2,16,3553;2,17,3553;3,0,3553;3,1,3553;3,2,3553;3,3,3553;3,4,3553;3,5,3553;3,6,3553;3,7,3553;3,8,3553;3,9,3553;3,10,3553;3,11,3553;3,12,3553;3,13,3553;3,14,3553;3,15,3553;3,16,3553;3,17,3553;4,0,3553;4,1,3553;4,2,3553;4,3,3553;4,4,3553;4,5,3553;4,6,3553;4,7,3553;4,8,3553;4,9,3553;4,10,3553;4,11,3553;4,12,3553;4,13,3553;4,14,3553;4,15,3553;4,16,3553;4,17,3553;5,0,3553;5,1,3553;5,2,3553;5,3,3553;5,4,3553;5,5,3553;5,6,3553;5,7,3553;5,8,3553;5,9,3553;5,10,3553;5,11,3553;5,12,3553;5,13,3553;5,14,3553;5,15,3553;5,16,3553;5,17,3553 --disable-accelerated-video-decode --disable-gpu-compositing --enable-gpu-async-worker-context --service-request-channel-token=3D24CF9ECCAD56C0D609B6AB4B4518C8 --renderer-client-id=5 --mojo-platform-channel-handle=2564 /prefetch:1',
        r'"C:\some-directory\chrome.exe" --type=renderer --disable-background-timer-throttling --use-gl=osmesa --service-pipe-token=3E86D0603BCD97F64B784FA03FE8FF12 --lang=en-US --headless --enable-pinch --device-scale-factor=1 --num-raster-threads=4 --enable-main-frame-before-activation --content-image-texture-target=0,0,3553;0,1,3553;0,2,3553;0,3,3553;0,4,3553;0,5,3553;0,6,3553;0,7,3553;0,8,3553;0,9,3553;0,10,3553;0,11,3553;0,12,3553;0,13,3553;0,14,3553;0,15,3553;0,16,3553;0,17,3553;1,0,3553;1,1,3553;1,2,3553;1,3,3553;1,4,3553;1,5,3553;1,6,3553;1,7,3553;1,8,3553;1,9,3553;1,10,3553;1,11,3553;1,12,3553;1,13,3553;1,14,3553;1,15,3553;1,16,3553;1,17,3553;2,0,3553;2,1,3553;2,2,3553;2,3,3553;2,4,3553;2,5,3553;2,6,3553;2,7,3553;2,8,3553;2,9,3553;2,10,3553;2,11,3553;2,12,3553;2,13,3553;2,14,3553;2,15,3553;2,16,3553;2,17,3553;3,0,3553;3,1,3553;3,2,3553;3,3,3553;3,4,3553;3,5,3553;3,6,3553;3,7,3553;3,8,3553;3,9,3553;3,10,3553;3,11,3553;3,12,3553;3,13,3553;3,14,3553;3,15,3553;3,16,3553;3,17,3553;4,0,3553;4,1,3553;4,2,3553;4,3,3553;4,4,3553;4,5,3553;4,6,3553;4,7,3553;4,8,3553;4,9,3553;4,10,3553;4,11,3553;4,12,3553;4,13,3553;4,14,3553;4,15,3553;4,16,3553;4,17,3553;5,0,3553;5,1,3553;5,2,3553;5,3,3553;5,4,3553;5,5,3553;5,6,3553;5,7,3553;5,8,3553;5,9,3553;5,10,3553;5,11,3553;5,12,3553;5,13,3553;5,14,3553;5,15,3553;5,16,3553;5,17,3553 --disable-accelerated-video-decode --disable-gpu-compositing --enable-gpu-async-worker-context --service-request-channel-token=3E86D0603BCD97F64B784FA03FE8FF12 --renderer-client-id=2 --mojo-platform-channel-handle=1388 /prefetch:1',
        r'"C:\some-directory\chrome.exe" --type=renderer --disable-background-timer-throttling --use-gl=osmesa --service-pipe-token=3EB022DC0FC1EBBE4976303A92FB2C3C --lang=en-US --headless --enable-pinch --device-scale-factor=1 --num-raster-threads=4 --enable-main-frame-before-activation --content-image-texture-target=0,0,3553;0,1,3553;0,2,3553;0,3,3553;0,4,3553;0,5,3553;0,6,3553;0,7,3553;0,8,3553;0,9,3553;0,10,3553;0,11,3553;0,12,3553;0,13,3553;0,14,3553;0,15,3553;0,16,3553;0,17,3553;1,0,3553;1,1,3553;1,2,3553;1,3,3553;1,4,3553;1,5,3553;1,6,3553;1,7,3553;1,8,3553;1,9,3553;1,10,3553;1,11,3553;1,12,3553;1,13,3553;1,14,3553;1,15,3553;1,16,3553;1,17,3553;2,0,3553;2,1,3553;2,2,3553;2,3,3553;2,4,3553;2,5,3553;2,6,3553;2,7,3553;2,8,3553;2,9,3553;2,10,3553;2,11,3553;2,12,3553;2,13,3553;2,14,3553;2,15,3553;2,16,3553;2,17,3553;3,0,3553;3,1,3553;3,2,3553;3,3,3553;3,4,3553;3,5,3553;3,6,3553;3,7,3553;3,8,3553;3,9,3553;3,10,3553;3,11,3553;3,12,3553;3,13,3553;3,14,3553;3,15,3553;3,16,3553;3,17,3553;4,0,3553;4,1,3553;4,2,3553;4,3,3553;4,4,3553;4,5,3553;4,6,3553;4,7,3553;4,8,3553;4,9,3553;4,10,3553;4,11,3553;4,12,3553;4,13,3553;4,14,3553;4,15,3553;4,16,3553;4,17,3553;5,0,3553;5,1,3553;5,2,3553;5,3,3553;5,4,3553;5,5,3553;5,6,3553;5,7,3553;5,8,3553;5,9,3553;5,10,3553;5,11,3553;5,12,3553;5,13,3553;5,14,3553;5,15,3553;5,16,3553;5,17,3553 --disable-accelerated-video-decode --disable-gpu-compositing --enable-gpu-async-worker-context --service-request-channel-token=3EB022DC0FC1EBBE4976303A92FB2C3C --renderer-client-id=3 --mojo-platform-channel-handle=1620 /prefetch:1',
        r'"C:\some-directory\chrome.exe" --type=renderer --disable-background-timer-throttling --use-gl=osmesa --service-pipe-token=420D24746827847FD345D34D832A6938 --lang=en-US --headless --enable-pinch --device-scale-factor=1 --num-raster-threads=4 --enable-main-frame-before-activation --content-image-texture-target=0,0,3553;0,1,3553;0,2,3553;0,3,3553;0,4,3553;0,5,3553;0,6,3553;0,7,3553;0,8,3553;0,9,3553;0,10,3553;0,11,3553;0,12,3553;0,13,3553;0,14,3553;0,15,3553;0,16,3553;0,17,3553;1,0,3553;1,1,3553;1,2,3553;1,3,3553;1,4,3553;1,5,3553;1,6,3553;1,7,3553;1,8,3553;1,9,3553;1,10,3553;1,11,3553;1,12,3553;1,13,3553;1,14,3553;1,15,3553;1,16,3553;1,17,3553;2,0,3553;2,1,3553;2,2,3553;2,3,3553;2,4,3553;2,5,3553;2,6,3553;2,7,3553;2,8,3553;2,9,3553;2,10,3553;2,11,3553;2,12,3553;2,13,3553;2,14,3553;2,15,3553;2,16,3553;2,17,3553;3,0,3553;3,1,3553;3,2,3553;3,3,3553;3,4,3553;3,5,3553;3,6,3553;3,7,3553;3,8,3553;3,9,3553;3,10,3553;3,11,3553;3,12,3553;3,13,3553;3,14,3553;3,15,3553;3,16,3553;3,17,3553;4,0,3553;4,1,3553;4,2,3553;4,3,3553;4,4,3553;4,5,3553;4,6,3553;4,7,3553;4,8,3553;4,9,3553;4,10,3553;4,11,3553;4,12,3553;4,13,3553;4,14,3553;4,15,3553;4,16,3553;4,17,3553;5,0,3553;5,1,3553;5,2,3553;5,3,3553;5,4,3553;5,5,3553;5,6,3553;5,7,3553;5,8,3553;5,9,3553;5,10,3553;5,11,3553;5,12,3553;5,13,3553;5,14,3553;5,15,3553;5,16,3553;5,17,3553 --disable-accelerated-video-decode --disable-gpu-compositing --enable-gpu-async-worker-context --service-request-channel-token=420D24746827847FD345D34D832A6938 --renderer-client-id=3 --mojo-platform-channel-handle=1616 /prefetch:1',
        r'"C:\some-directory\chrome.exe" --type=renderer --disable-background-timer-throttling --use-gl=osmesa --service-pipe-token=43C8CB6E131FB5F7E290BDD300253042 --lang=en-US --headless --enable-pinch --device-scale-factor=1 --num-raster-threads=4 --enable-main-frame-before-activation --content-image-texture-target=0,0,3553;0,1,3553;0,2,3553;0,3,3553;0,4,3553;0,5,3553;0,6,3553;0,7,3553;0,8,3553;0,9,3553;0,10,3553;0,11,3553;0,12,3553;0,13,3553;0,14,3553;0,15,3553;0,16,3553;0,17,3553;1,0,3553;1,1,3553;1,2,3553;1,3,3553;1,4,3553;1,5,3553;1,6,3553;1,7,3553;1,8,3553;1,9,3553;1,10,3553;1,11,3553;1,12,3553;1,13,3553;1,14,3553;1,15,3553;1,16,3553;1,17,3553;2,0,3553;2,1,3553;2,2,3553;2,3,3553;2,4,3553;2,5,3553;2,6,3553;2,7,3553;2,8,3553;2,9,3553;2,10,3553;2,11,3553;2,12,3553;2,13,3553;2,14,3553;2,15,3553;2,16,3553;2,17,3553;3,0,3553;3,1,3553;3,2,3553;3,3,3553;3,4,3553;3,5,3553;3,6,3553;3,7,3553;3,8,3553;3,9,3553;3,10,3553;3,11,3553;3,12,3553;3,13,3553;3,14,3553;3,15,3553;3,16,3553;3,17,3553;4,0,3553;4,1,3553;4,2,3553;4,3,3553;4,4,3553;4,5,3553;4,6,3553;4,7,3553;4,8,3553;4,9,3553;4,10,3553;4,11,3553;4,12,3553;4,13,3553;4,14,3553;4,15,3553;4,16,3553;4,17,3553;5,0,3553;5,1,3553;5,2,3553;5,3,3553;5,4,3553;5,5,3553;5,6,3553;5,7,3553;5,8,3553;5,9,3553;5,10,3553;5,11,3553;5,12,3553;5,13,3553;5,14,3553;5,15,3553;5,16,3553;5,17,3553 --disable-accelerated-video-decode --disable-gpu-compositing --enable-gpu-async-worker-context --service-request-channel-token=43C8CB6E131FB5F7E290BDD300253042 --renderer-client-id=2 --mojo-platform-channel-handle=1404 /prefetch:1',
        r'"C:\some-directory\chrome.exe" --type=renderer --disable-background-timer-throttling --use-gl=osmesa --service-pipe-token=4520867474F1BEB4082E5541EBCB6039 --lang=en-US --headless --enable-pinch --device-scale-factor=1 --num-raster-threads=4 --enable-main-frame-before-activation --content-image-texture-target=0,0,3553;0,1,3553;0,2,3553;0,3,3553;0,4,3553;0,5,3553;0,6,3553;0,7,3553;0,8,3553;0,9,3553;0,10,3553;0,11,3553;0,12,3553;0,13,3553;0,14,3553;0,15,3553;0,16,3553;0,17,3553;1,0,3553;1,1,3553;1,2,3553;1,3,3553;1,4,3553;1,5,3553;1,6,3553;1,7,3553;1,8,3553;1,9,3553;1,10,3553;1,11,3553;1,12,3553;1,13,3553;1,14,3553;1,15,3553;1,16,3553;1,17,3553;2,0,3553;2,1,3553;2,2,3553;2,3,3553;2,4,3553;2,5,3553;2,6,3553;2,7,3553;2,8,3553;2,9,3553;2,10,3553;2,11,3553;2,12,3553;2,13,3553;2,14,3553;2,15,3553;2,16,3553;2,17,3553;3,0,3553;3,1,3553;3,2,3553;3,3,3553;3,4,3553;3,5,3553;3,6,3553;3,7,3553;3,8,3553;3,9,3553;3,10,3553;3,11,3553;3,12,3553;3,13,3553;3,14,3553;3,15,3553;3,16,3553;3,17,3553;4,0,3553;4,1,3553;4,2,3553;4,3,3553;4,4,3553;4,5,3553;4,6,3553;4,7,3553;4,8,3553;4,9,3553;4,10,3553;4,11,3553;4,12,3553;4,13,3553;4,14,3553;4,15,3553;4,16,3553;4,17,3553;5,0,3553;5,1,3553;5,2,3553;5,3,3553;5,4,3553;5,5,3553;5,6,3553;5,7,3553;5,8,3553;5,9,3553;5,10,3553;5,11,3553;5,12,3553;5,13,3553;5,14,3553;5,15,3553;5,16,3553;5,17,3553 --disable-accelerated-video-decode --disable-gpu-compositing --enable-gpu-async-worker-context --service-request-channel-token=4520867474F1BEB4082E5541EBCB6039 --renderer-client-id=2 --mojo-platform-channel-handle=1408 /prefetch:1',
        r'"C:\some-directory\chrome.exe" --type=renderer --disable-background-timer-throttling --use-gl=osmesa --service-pipe-token=48F0011D3DE7447F102B52DD811C0F97 --lang=en-US --headless --enable-pinch --device-scale-factor=1 --num-raster-threads=4 --enable-main-frame-before-activation --content-image-texture-target=0,0,3553;0,1,3553;0,2,3553;0,3,3553;0,4,3553;0,5,3553;0,6,3553;0,7,3553;0,8,3553;0,9,3553;0,10,3553;0,11,3553;0,12,3553;0,13,3553;0,14,3553;0,15,3553;0,16,3553;0,17,3553;1,0,3553;1,1,3553;1,2,3553;1,3,3553;1,4,3553;1,5,3553;1,6,3553;1,7,3553;1,8,3553;1,9,3553;1,10,3553;1,11,3553;1,12,3553;1,13,3553;1,14,3553;1,15,3553;1,16,3553;1,17,3553;2,0,3553;2,1,3553;2,2,3553;2,3,3553;2,4,3553;2,5,3553;2,6,3553;2,7,3553;2,8,3553;2,9,3553;2,10,3553;2,11,3553;2,12,3553;2,13,3553;2,14,3553;2,15,3553;2,16,3553;2,17,3553;3,0,3553;3,1,3553;3,2,3553;3,3,3553;3,4,3553;3,5,3553;3,6,3553;3,7,3553;3,8,3553;3,9,3553;3,10,3553;3,11,3553;3,12,3553;3,13,3553;3,14,3553;3,15,3553;3,16,3553;3,17,3553;4,0,3553;4,1,3553;4,2,3553;4,3,3553;4,4,3553;4,5,3553;4,6,3553;4,7,3553;4,8,3553;4,9,3553;4,10,3553;4,11,3553;4,12,3553;4,13,3553;4,14,3553;4,15,3553;4,16,3553;4,17,3553;5,0,3553;5,1,3553;5,2,3553;5,3,3553;5,4,3553;5,5,3553;5,6,3553;5,7,3553;5,8,3553;5,9,3553;5,10,3553;5,11,3553;5,12,3553;5,13,3553;5,14,3553;5,15,3553;5,16,3553;5,17,3553 --disable-accelerated-video-decode --disable-gpu-compositing --enable-gpu-async-worker-context --service-request-channel-token=48F0011D3DE7447F102B52DD811C0F97 --renderer-client-id=5 --mojo-platform-channel-handle=1652 /prefetch:1',
        r'"C:\some-directory\chrome.exe" --type=renderer --disable-background-timer-throttling --use-gl=osmesa --service-pipe-token=4E37B68B088181E696DC64A080777C18 --lang=en-US --headless --enable-pinch --device-scale-factor=1 --num-raster-threads=4 --enable-main-frame-before-activation --content-image-texture-target=0,0,3553;0,1,3553;0,2,3553;0,3,3553;0,4,3553;0,5,3553;0,6,3553;0,7,3553;0,8,3553;0,9,3553;0,10,3553;0,11,3553;0,12,3553;0,13,3553;0,14,3553;0,15,3553;0,16,3553;0,17,3553;1,0,3553;1,1,3553;1,2,3553;1,3,3553;1,4,3553;1,5,3553;1,6,3553;1,7,3553;1,8,3553;1,9,3553;1,10,3553;1,11,3553;1,12,3553;1,13,3553;1,14,3553;1,15,3553;1,16,3553;1,17,3553;2,0,3553;2,1,3553;2,2,3553;2,3,3553;2,4,3553;2,5,3553;2,6,3553;2,7,3553;2,8,3553;2,9,3553;2,10,3553;2,11,3553;2,12,3553;2,13,3553;2,14,3553;2,15,3553;2,16,3553;2,17,3553;3,0,3553;3,1,3553;3,2,3553;3,3,3553;3,4,3553;3,5,3553;3,6,3553;3,7,3553;3,8,3553;3,9,3553;3,10,3553;3,11,3553;3,12,3553;3,13,3553;3,14,3553;3,15,3553;3,16,3553;3,17,3553;4,0,3553;4,1,3553;4,2,3553;4,3,3553;4,4,3553;4,5,3553;4,6,3553;4,7,3553;4,8,3553;4,9,3553;4,10,3553;4,11,3553;4,12,3553;4,13,3553;4,14,3553;4,15,3553;4,16,3553;4,17,3553;5,0,3553;5,1,3553;5,2,3553;5,3,3553;5,4,3553;5,5,3553;5,6,3553;5,7,3553;5,8,3553;5,9,3553;5,10,3553;5,11,3553;5,12,3553;5,13,3553;5,14,3553;5,15,3553;5,16,3553;5,17,3553 --disable-accelerated-video-decode --disable-gpu-compositing --enable-gpu-async-worker-context --service-request-channel-token=4E37B68B088181E696DC64A080777C18 --renderer-client-id=2 --mojo-platform-channel-handle=1404 /prefetch:1',
        r'"C:\some-directory\chrome.exe" --type=renderer --disable-background-timer-throttling --use-gl=osmesa --service-pipe-token=532E72574630238D4377E2D478161719 --lang=en-US --headless --enable-pinch --device-scale-factor=1 --num-raster-threads=4 --enable-main-frame-before-activation --content-image-texture-target=0,0,3553;0,1,3553;0,2,3553;0,3,3553;0,4,3553;0,5,3553;0,6,3553;0,7,3553;0,8,3553;0,9,3553;0,10,3553;0,11,3553;0,12,3553;0,13,3553;0,14,3553;0,15,3553;0,16,3553;0,17,3553;1,0,3553;1,1,3553;1,2,3553;1,3,3553;1,4,3553;1,5,3553;1,6,3553;1,7,3553;1,8,3553;1,9,3553;1,10,3553;1,11,3553;1,12,3553;1,13,3553;1,14,3553;1,15,3553;1,16,3553;1,17,3553;2,0,3553;2,1,3553;2,2,3553;2,3,3553;2,4,3553;2,5,3553;2,6,3553;2,7,3553;2,8,3553;2,9,3553;2,10,3553;2,11,3553;2,12,3553;2,13,3553;2,14,3553;2,15,3553;2,16,3553;2,17,3553;3,0,3553;3,1,3553;3,2,3553;3,3,3553;3,4,3553;3,5,3553;3,6,3553;3,7,3553;3,8,3553;3,9,3553;3,10,3553;3,11,3553;3,12,3553;3,13,3553;3,14,3553;3,15,3553;3,16,3553;3,17,3553;4,0,3553;4,1,3553;4,2,3553;4,3,3553;4,4,3553;4,5,3553;4,6,3553;4,7,3553;4,8,3553;4,9,3553;4,10,3553;4,11,3553;4,12,3553;4,13,3553;4,14,3553;4,15,3553;4,16,3553;4,17,3553;5,0,3553;5,1,3553;5,2,3553;5,3,3553;5,4,3553;5,5,3553;5,6,3553;5,7,3553;5,8,3553;5,9,3553;5,10,3553;5,11,3553;5,12,3553;5,13,3553;5,14,3553;5,15,3553;5,16,3553;5,17,3553 --disable-accelerated-video-decode --disable-gpu-compositing --enable-gpu-async-worker-context --service-request-channel-token=532E72574630238D4377E2D478161719 --renderer-client-id=4 --mojo-platform-channel-handle=1624 /prefetch:1',
        r'"C:\some-directory\chrome.exe" --type=renderer --disable-background-timer-throttling --use-gl=osmesa --service-pipe-token=5832BB97837634498CF5C5C1CE366A30 --lang=en-US --headless --enable-pinch --device-scale-factor=1 --num-raster-threads=4 --enable-main-frame-before-activation --content-image-texture-target=0,0,3553;0,1,3553;0,2,3553;0,3,3553;0,4,3553;0,5,3553;0,6,3553;0,7,3553;0,8,3553;0,9,3553;0,10,3553;0,11,3553;0,12,3553;0,13,3553;0,14,3553;0,15,3553;0,16,3553;0,17,3553;1,0,3553;1,1,3553;1,2,3553;1,3,3553;1,4,3553;1,5,3553;1,6,3553;1,7,3553;1,8,3553;1,9,3553;1,10,3553;1,11,3553;1,12,3553;1,13,3553;1,14,3553;1,15,3553;1,16,3553;1,17,3553;2,0,3553;2,1,3553;2,2,3553;2,3,3553;2,4,3553;2,5,3553;2,6,3553;2,7,3553;2,8,3553;2,9,3553;2,10,3553;2,11,3553;2,12,3553;2,13,3553;2,14,3553;2,15,3553;2,16,3553;2,17,3553;3,0,3553;3,1,3553;3,2,3553;3,3,3553;3,4,3553;3,5,3553;3,6,3553;3,7,3553;3,8,3553;3,9,3553;3,10,3553;3,11,3553;3,12,3553;3,13,3553;3,14,3553;3,15,3553;3,16,3553;3,17,3553;4,0,3553;4,1,3553;4,2,3553;4,3,3553;4,4,3553;4,5,3553;4,6,3553;4,7,3553;4,8,3553;4,9,3553;4,10,3553;4,11,3553;4,12,3553;4,13,3553;4,14,3553;4,15,3553;4,16,3553;4,17,3553;5,0,3553;5,1,3553;5,2,3553;5,3,3553;5,4,3553;5,5,3553;5,6,3553;5,7,3553;5,8,3553;5,9,3553;5,10,3553;5,11,3553;5,12,3553;5,13,3553;5,14,3553;5,15,3553;5,16,3553;5,17,3553 --disable-accelerated-video-decode --disable-gpu-compositing --enable-gpu-async-worker-context --service-request-channel-token=5832BB97837634498CF5C5C1CE366A30 --renderer-client-id=4 --mojo-platform-channel-handle=1932 /prefetch:1',
        r'"C:\some-directory\chrome.exe" --type=renderer --disable-background-timer-throttling --use-gl=osmesa --service-pipe-token=629A2CA87B89BECDE10C7F71CD4E8927 --lang=en-US --headless --enable-pinch --device-scale-factor=1 --num-raster-threads=4 --enable-main-frame-before-activation --content-image-texture-target=0,0,3553;0,1,3553;0,2,3553;0,3,3553;0,4,3553;0,5,3553;0,6,3553;0,7,3553;0,8,3553;0,9,3553;0,10,3553;0,11,3553;0,12,3553;0,13,3553;0,14,3553;0,15,3553;0,16,3553;0,17,3553;1,0,3553;1,1,3553;1,2,3553;1,3,3553;1,4,3553;1,5,3553;1,6,3553;1,7,3553;1,8,3553;1,9,3553;1,10,3553;1,11,3553;1,12,3553;1,13,3553;1,14,3553;1,15,3553;1,16,3553;1,17,3553;2,0,3553;2,1,3553;2,2,3553;2,3,3553;2,4,3553;2,5,3553;2,6,3553;2,7,3553;2,8,3553;2,9,3553;2,10,3553;2,11,3553;2,12,3553;2,13,3553;2,14,3553;2,15,3553;2,16,3553;2,17,3553;3,0,3553;3,1,3553;3,2,3553;3,3,3553;3,4,3553;3,5,3553;3,6,3553;3,7,3553;3,8,3553;3,9,3553;3,10,3553;3,11,3553;3,12,3553;3,13,3553;3,14,3553;3,15,3553;3,16,3553;3,17,3553;4,0,3553;4,1,3553;4,2,3553;4,3,3553;4,4,3553;4,5,3553;4,6,3553;4,7,3553;4,8,3553;4,9,3553;4,10,3553;4,11,3553;4,12,3553;4,13,3553;4,14,3553;4,15,3553;4,16,3553;4,17,3553;5,0,3553;5,1,3553;5,2,3553;5,3,3553;5,4,3553;5,5,3553;5,6,3553;5,7,3553;5,8,3553;5,9,3553;5,10,3553;5,11,3553;5,12,3553;5,13,3553;5,14,3553;5,15,3553;5,16,3553;5,17,3553 --disable-accelerated-video-decode --disable-gpu-compositing --enable-gpu-async-worker-context --service-request-channel-token=629A2CA87B89BECDE10C7F71CD4E8927 --renderer-client-id=3 --mojo-platform-channel-handle=1624 /prefetch:1',
        r'"C:\some-directory\chrome.exe" --type=renderer --disable-background-timer-throttling --use-gl=osmesa --service-pipe-token=68DCD19EFBAE62D6CDF5F88DD36228CA --lang=en-US --headless --enable-pinch --device-scale-factor=1 --num-raster-threads=4 --enable-main-frame-before-activation --content-image-texture-target=0,0,3553;0,1,3553;0,2,3553;0,3,3553;0,4,3553;0,5,3553;0,6,3553;0,7,3553;0,8,3553;0,9,3553;0,10,3553;0,11,3553;0,12,3553;0,13,3553;0,14,3553;0,15,3553;0,16,3553;0,17,3553;1,0,3553;1,1,3553;1,2,3553;1,3,3553;1,4,3553;1,5,3553;1,6,3553;1,7,3553;1,8,3553;1,9,3553;1,10,3553;1,11,3553;1,12,3553;1,13,3553;1,14,3553;1,15,3553;1,16,3553;1,17,3553;2,0,3553;2,1,3553;2,2,3553;2,3,3553;2,4,3553;2,5,3553;2,6,3553;2,7,3553;2,8,3553;2,9,3553;2,10,3553;2,11,3553;2,12,3553;2,13,3553;2,14,3553;2,15,3553;2,16,3553;2,17,3553;3,0,3553;3,1,3553;3,2,3553;3,3,3553;3,4,3553;3,5,3553;3,6,3553;3,7,3553;3,8,3553;3,9,3553;3,10,3553;3,11,3553;3,12,3553;3,13,3553;3,14,3553;3,15,3553;3,16,3553;3,17,3553;4,0,3553;4,1,3553;4,2,3553;4,3,3553;4,4,3553;4,5,3553;4,6,3553;4,7,3553;4,8,3553;4,9,3553;4,10,3553;4,11,3553;4,12,3553;4,13,3553;4,14,3553;4,15,3553;4,16,3553;4,17,3553;5,0,3553;5,1,3553;5,2,3553;5,3,3553;5,4,3553;5,5,3553;5,6,3553;5,7,3553;5,8,3553;5,9,3553;5,10,3553;5,11,3553;5,12,3553;5,13,3553;5,14,3553;5,15,3553;5,16,3553;5,17,3553 --disable-accelerated-video-decode --disable-gpu-compositing --enable-gpu-async-worker-context --service-request-channel-token=68DCD19EFBAE62D6CDF5F88DD36228CA --renderer-client-id=4 --mojo-platform-channel-handle=1628 /prefetch:1',

        r'"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" ',
        r'"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"  "http://localhost:63342/Hakathon17-faceRecognition-manager/index.html?_ijt=sg8726j95mqrpl9n0ttb2l7i7p"',
        r'"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"  "http://localhost:63342/hackaton17-sabitzel-team-ui/hackaton17-sabitzel-team-ui/app/views/home.html?_ijt=30kj7oje65gg4f65j22rl1m5gr"',
        r'"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"  "http://localhost:63342/hackaton17-sabitzel-team-ui/hackaton17-sabitzel-team-ui/app/views/home.html?_ijt=5aka3op56lvtea7c0s0gi74au4"',
        r'"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"  "http://localhost:63342/hackaton17-sabitzel-team-ui/hackaton17-sabitzel-team-ui/index.html?_ijt=8k187abc2r3npno02pbc3r0r75"',
        r'"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"  "http://localhost:63342/hackaton17-sabitzel-team-ui/hackaton17-sabitzel-team-ui/index.html?_ijt=o4cr44hg2ibuqloq80g4qb7nka"',
        r'"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"  "http://localhost:63342/hybrid-admin-free-web-template/BTSP.html?_ijt=jnst940r55ghorj2ckkn38a0o6"',
        r'"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"  "http://localhost:63342/hybrid-admin-free-web-template/BTSQ.html?_ijt=abjlu0epfob8f06asqmravf38f"',
        r'"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"  "http://localhost:63342/hybrid-admin-free-web-template/BTSQ.html?_ijt=b0p0npbgtluqbb84ies91mm1kf"',
        r'"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"  "http://localhost:63342/hybrid-admin-free-web-template/BTSQ.html?_ijt=jnst940r55ghorj2ckkn38a0o6"',
        r'"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"  "http://localhost:63342/hybrid-admin-free-web-template/BTSQ.html?_ijt=kcmff3g7u1g8e7v3q3n47c91f7"',
        r'"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"  "http://localhost:63342/hybrid-admin-free-web-template/BTSQ.html?_ijt=ncdecrt6usd97v2job9te34sir"',
        r'"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"  --profile-directory="Profile 1" --app-id=bgkodfmeijboinjdegggmkbkjfiagaan',
        r'"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"  --profile-directory="Profile 1" --app-id=fhbjgbiflinjbdggehcddcbncdddomop',
        r'"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"  --profile-directory="Profile 1" --app-id=hmjkmjkepdijhoojdojkdfohbdgmmhki',
        r'"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"  --profile-directory=Default --app-id=bgjohebimpjdhhocbknplfelpmdhifhd',
        r'"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"  --profile-directory=Default --app-id=fahmaaghhglfmonjliepjlchgpgfmobi',
        r'"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"  --profile-directory=Default --app-id=menkifleemblimdogmoihpfopnplikde',
        r'"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"  --profile-directory=Default --app-id=nbjdhgkkhefpifbifjiflpaajchdkhpg',

        r'C:\WINDOWS\System32\svchost.exe -k LocalService -s LicenseManager',
        r'C:\Windows\system32\svchost.exe -k LocalServiceNetworkRestricted -s WFDSConMgrSvc',
        r'"C:\Windows\System32\svchost.exe" -k unistacksvcgroup',
        r'"C:\Windows\System32\svchost.exe" -k localserviceandnoimpersonation -s FDResPub',
        r'C:\Windows\System32\svchost.exe -k Camera',
        r'C:\windows\System32\svchost.exe -k Camera',
        r'"c:\windows\system32\\svchost.exe"',
        r'"C:\Windows\System32\svchost.exe" -k swprv',
        r'"C:\Windows\system32\svchost.exe"',
        r'C:\Windows\system32\svchost.exe -k LocalService -s FontCache',
        r'C:\windows\System32\svchost.exe -k UnistackSvcGroup',
        r'"C:\Windows\System32\svchost.exe" -k utcsvc',
        r'C:\windows\system32\svchost.exe -k appmodel',
        r'"svchost.exe"',
        r'C:\windows\system32\svchost.exe -k UnistackSvcGroup',
        r'C:\Windows\SysWoW64\svchost -k QQLiveService',
        r'"C:\Windows\System32\svchost.exe" -k LocalSystemNetworkRestricted',
        r'"C:\WINDOWS\System32\svchost.exe" -k NetworkService',
        r'"C:\Windows\System32\svchost.exe"',
        r'"C:\Windows\System32\svchost.exe" -k networkservicenetworkrestricted -s PolicyAgent',
    ]

    with auto_context(default_test_windows_cmds if args.default else sys.stdin) as input_stream:
        cmds = sorted({normalizer.normalize(cmd) for cmd in input_stream})

    for cmd in cmds:
        print(cmd)

if __name__ == "__main__":
    sys.exit(test())

