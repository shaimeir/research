#!/usr/bin/env bash

# take as input a list of directories to inspect along with a list of names to run through and generate all the
# profiles specified in the command line for as many of them as possible.
# the script requires recipes to be supplied externally and therefor contains no profile logic but rather order of actions.
# when done, run cross-validation and generate a summary.

# some bash technicalities you should be aware of if you're not fully up-to-date on your shell programming ;) :
#   1. bash functions have access to the global namespace (within certain limitations which i will specify should they arise)
#      so you can define and modify variables from within a function call and they will be reflected in the main context.
#
#   2. the syntax ${abc:-def} means "if the variable $abc isn't defined, yield the value "def"".
#      specifically you can see the following use: ${abc:-} which means "yield an empty string if $abc is not defined".
#      we use this because we use "set -u" to make sure that access to undefined variables will halt the script's execution.
#
#   3. 'declare -A x' defines the variable 'x' to be an associative array.
#
#   4. 'x=(1 2 3)' declares 'x' to be a list - an associative array where the keys are set to be the 0-based inidices of the specified list.
#
#   5. for an associative array x (or a list, which is a specific case of an associative array) the following access rules hold:
#       * ${x[@]} - a list of all the array's values
#       * ${!x[@]} - a list of all the array's keys
#       * ${#x[@]} - number of entries in the array
#       * ${x[3]}, ${x["hey"]} - access the value associated with the keys "3" or "hey"
#
#   6. getopts black magic - you can learn how to use getopts here: http://wiki.bash-hackers.org/howto/getopts_tutorial
#      i wanted to add support for multi-argument switches of the form: -a arg1 arg2 arg3
#      in order to support this i use a small hack - modify OPTIND (the variable getopts uses to track what component of the command line it needs to evaluate):
#           getopts "a:" option # regular getopts with arguments
#           case $option in
#               a) first_arg=$OPTARG # read the first argument
#                  second_arg=${@:$OPTIND:1} # take the next-to-be-evaluated component of the command line
#                  third_arg=${@:$OPTIND+1:1} # and the one after that
#                  let "OPTIND += 2" # tell getopts to skip the next two components as we've already processed them
#                  ;;
#           esac
#
#   7. the 'while read x y z' construct - 'while read x y z' iterates on the loops input stream, reads a line
#      and assigns it's first word to a variable called x, it's second to y, and the *reminder* (i.e. may be more than 1 word) to z.
#      these variables are then available for access within the loop.
#
#      e.g. assume /tmp/text holds the following lines:
#           a b c d e f g
#           1 2
#           alpha beta gamma
#
#       and consider the following code
#       cat /tmp/text | while read x y z
#       do
#           echo first: $x second: $y reminder: $z
#       done
#
#       then the output will be:
#           first: a second: b reminder: c d e f g
#           first: 1 second: 2 reminder:
#           first: alpha second: beta reminder: gamma
#
#   8. in "older" bash versions (e.g. 3.2.25(1)) accessing an array defined as an empty list (e.g. "x=()" as in note #4) was fine.
#      in newer bash versions (e.g. 4.4.7(1)) it's also fine.
#      there are however some versions (e.g. 4.2.46(2) - tested on CentOS 7.3) where an empty array is considered an undefined variable.
#
#       for example:
#           x=()
#           echo ${x[@]}
#
#       would fail with an unbound variable error.
#       even pre-declaring these variables attributes with "declare -A" or "declare -a" doesn't work.
#
#       this means that we can't just define an empty array and append elements to it (as with $recipe_files and $sub_recipes in the main section).
#       to support these intermediate bash versions we have to hold a flag that tells us whether the arrays were initialized or not and create them
#       when we first try to insert an element, like so:
#
#           if [ -z "${arraies_initialized:-}" ] # this means "if ${arraies_initialized:-} yields an empty string"
#                                                # where ${arraies_initialized:-} means "if arraies_initialized is defined then use it's value, otherwise default to an empty string.
#           then
#               array=("elemet") # after this line the array will indeed be well defined
#
#               arraies_initialized="1" # the if condition will no longer evaluate to true
#           else
#               array=("element2" ${array[@]}) # at this point the array is well defined
#
#       you can read more over here: https://stackoverflow.com/a/28058737
#
#   9. readlink -m SOME_PATH - resolves all the possible symbolic links of the path and print the full resultant path (starting at /).
#                              not all the directories must actually exist. output the resolved path.
#   10 . readlink -f SOME_PATH - like "readlink -m SOME_PATH" except that all but the last element in SOME_PATH must exist.
#
#   11. subshells - i use the following bash-specific syntax in certain cases in the code (see apply_recipes_to_dump):
#           command &> >(tee file >&2) || break
#
#       this is a bit obscure at first but it's quite simple. this feature functions very similarly to a pipe but is called a subshell.
#       it tells bash to redirect command's stdout and stderr (uniformly noted as "&>") to the "tee file >&2" command.
#       one would normally use a pipe but i would like to skip the later steps if a command fails (but not exit the entire program due to bash's -e flag).
#       with the pipe syntax:
#           command |& tee file >&2
#
#       the tee determines the commands return code and it'll always be successfull (probably).
#       using the subshell syntax the || logical operation will take the command's error code instead of the tee's when evaluating the expression
#       and therefor the loop used will stop because the break command will be reached.
#
#   12. fancy redirection - you can see in the self logging part that i use the syntax:
#           exec 3>&1 4>&2 &> >(tee log-file >&2)
#
#       this syntax means do the following in the following order:
#           3>&1 - bind fd 3 to the current stdout (that means that writing to fd 3 (e.g.: echo bla >/proc/self/fd/3; echo bla >&3) will now print to the screen)
#           4>&2 - bind fd 4 to the current stderr
#           &> >(tee log-file >&2) - create a new tee process and bind the bash's stderr and stdout to the tee's stdin (and redirect the tee's output to it's stderr which is the screen)
#
#       note that the order in which these bindings is sensitive and the &> part should appear last (otherwise 3 and 4 will bind to tee's stdin as well).

set -ue

# utility function for debug tracing
function trace()
{
    echo "$(date) [$(whoami)] ${@}" >&2
}

# print an error message and exit
function perror()
{
    trace "error: ${@}"
    exit 1
}

# a utility function for audited 'rm'
function rm_audit()
{
    if [ -n "${keep_intermediate_files:-}" ]
    then
        trace "skipping: rm ${@}"
    else
        trace "removing: rm ${@}"
        rm ${@}
    fi
}

# audit removal of large file
function rm_large_audit()
{
    if [ -n "${keep_everything:-}" ]
    then
        trace "skipping (large files): rm ${@}"
    else
        trace "removing (large files): rm ${@}"
        rm ${@}
    fi
}

############# the steps

function print_help()
{
    echo "this script generates the standard profiles for given recipes from CSV dumps."
    echo " * -h - print help."
    echo " * -H - print extra verbose help."
    echo " * -o - output directory. [mandatory]"
    echo " * -i - ignore the argument. (can be used to modify the command line easily while debugging)"
    echo " * -g - don't turn on self logging. (saves to the errors directory)"
    echo " * -x - set bash -x flag."
    echo " * -k - keep the tmp directory when done (along with it's content)."
    echo " * -K - just like -k but potentially large files are also kept (profile generation files). (implies -k)"
    echo " * -e - set the archive directory name. use 'none' to prevent archiving. [default: .archive]"
    echo " * -p [num-of-cpus] - limit the number of CPUs used by csv-stats.py."
    echo " * -d [dump-directory-or-file [...]] - used to list the directories/files holding the dumps of interest. (must be listed at least once)."
    echo " * -r [recipe-directory-or-file [...]] - used to list the directories/files holding the recipes of interest. (must be listed at least once)."
    echo " * -s [sub-recipe] - the sub-recipe to use with the last -r switch. if not mentioned no sub-recipe is used."
    echo " * -n [normalization-sub-recipe] - the sub-recipe to use with the last -r switch for the normalization step in the cross-validation phase. if not mentioned then 'main.normalize' is assumed."
    echo " * -F - force the usage of default constants and ignore environment variables."
    echo " * -P [standard-proc-chain-roots] - set the file containing the list of processes to generate process chains for. (default: ${default_standard_processes_file})"
    echo " * [dump-filename-pattern [...]] - used to limit the dump file names in the directories to the specified patterns. if not mentioned, file names won't be filtered."
    echo ""
    echo "recipe specific argument support:"
    echo " * -a [element-id] [argument] [value] - specify an argument for an aggregation element of the last specified recipe."
    echo " * -A [element-id] [argument] [value] - globaly specify an argument for an aggregation element that will be passed to all the recipes."
    echo " * -b [by-element-id] [argument] [value] - specify an argument for an aggregation-by element of the last specified recipe."
    echo " * -B [by-element-id] [argument] [value] - globaly specify an argument for an aggregation-by element that will be passed to all the recipes."
    echo " * -t [transformation-id] [argument] [value] - specify an argument for a transformation of the last specified recipe."
    echo " * -T [transformation-id] [argument] [value] - globaly specify an argument for a transformation that will be passed to all the recipes."
    echo " * -l [argument-alias] [value] - specify an argument value by argument alias for last specified recipe."
    echo " * -L [argument-alias] [value] - globaly specify an argument value by argument alias that will be passed to all the recipes."
    echo " * -S - force strict aliases arguments. don't supply --ignore-unknown-aliases to csv-stats.py."
}

function verbose_help()
{
    print_help

    echo ""
    echo "Environment variables:"
    echo "CROSS_VALIDATION_PARTITION = the percentage of a dump file allocated for cross-validation."
}

# define constants and configuration variables
function define_constants()
{
    # check whether to force-set the globals
    if [ "${#}" -gt 0 -a "${1}" == "force" ]
    then
        CROSS_VALIDATION_PARTITION=""
    fi

    # the percentage from a dump fule allocated to cross-validation
    CROSS_VALIDATION_PARTITION=${CROSS_VALIDATION_PARTITION:-10}
}

# assert that all the script's arguments are set
function validate_execution_env()
{
    [ -n "${output_directory:-}" ] || perror "missing output directory."
    [ -n "${dump_files:-}" ] || perror "missing dump directories/files."
    [ -n "${recipes_initialized:-}" ] || perror "missing recipe directories/files." # this is a duplicate of the next one because it's an improvement
                                                                                    # required to support some versions of bash (e.g. 4.2.46)
    [ "${#recipe_files[@]}" -gt "0" ] || perror "missing recipe directories/files."
    [ "${#sub_recipes[@]}" -eq "${#recipe_files[@]}" ] || perror "mismatching lengths recipes and sub-recipes lists"
    [ "${#normalization_sub_recipes[@]}" -eq "${#recipe_files[@]}" ] || perror "mismatching lengths recipes and sub-recipes lists"
    [ "${#arguments[@]}" -eq "${#recipe_files[@]}" ] || perror "mismatching lengths recipes and arguments lists"
}

# post execution cleanup function to be execute when the script terminates
function cleanup()
{
    # make sure the output directory was defined
    [ -n "${output_directory:-}" ] || return

    # save any in-the works logs
    [ -f "${trace_log}" ] && mv "${trace_log}" "${remnant_logs_dir}"
    [ -f "${execution_log}" ] && mv "${execution_log}" "${remnant_logs_dir}"
    [ -f "${compilation_log}" ] && mv "${compilation_log}" "${remnant_logs_dir}"
    [ -f "${normalization_log}" ] && mv "${normalization_log}" "${remnant_logs_dir}"
    [ -f "${profile_application_log}" ] && mv "${profile_application_log}" "${remnant_logs_dir}"
    [ -f "${cross_validation_summary_log}" ] && mv "${cross_validation_summary_log}" "${remnant_logs_dir}"

    # archive any errors
    [ -z "${errors_dir:-}" ] || tar -czf "${output_directory}/errors.tar.gz" -C "${tmp_dir}" "$(basename ${errors_dir})"

    # unless a special flag is set, remove the tmp dir
    [ -z "${tmp_dir:-}" ] || rm_audit -rf "${tmp_dir}"

    # unless specified by -K delete the large files directories
    if [ -z "${keep_everything:-}" ]
    then
        trace "removing large files."
        [ -n "${large_files_dirs:-}" ] && rm -rf ${large_files_dirs}
    else
        trace "keeping large files."
    fi

    return 0
}

# export the paths of the scripts we'll
function publish_framework_scripts()
{
    scripts_dir="$(dirname $(readlink -f ${BASH_SOURCE[0]}))"

    # csv-stats.py is the main processing framework we'll use to generate the profiles
    csv_stats="${scripts_dir}/csv-stats.py"

    # canon-paths-applier.py is the script we'll use to apply the profiles to our cross validation data-set and get the hit/miss percentages
    canon_paths_applier="${scripts_dir}/canon-paths-applier.py"

    # cross-validation-summary.py is the script that will accept the application result from canon-paths-applier.py and
    # generate/format the cross validation statistics
    cross_validation_summary="${scripts_dir}/cross-validation-summary.py"

    # infer-scheme.py allows us to to automate the process of classifying to which category ("scheme") dump files and
    # recipe files belong and therefor we'll manage those lists dynamically via intermediate files.
    infer_scheme="${scripts_dir}/infer-scheme.py"

    # sampler.py splits a given dump file into 2 separate file by a requested ratio.
    # we use this to generate the cross-validation sample and the profile data sample from a dump file.
    sampling_script="${scripts_dir}/sampler.py"

    # this script transforms 'process' scheme dumps to 'proc-chain-csv' typed dumps so we can create process creation profile trees.
    process_dump_conversion_script="${scripts_dir}/process-creation-tree.py"

    # this is the default list of processes of interest for process chains profiling
    default_standard_processes_file="${scripts_dir}/standard-processes"

    # the default recipe resolution directory
    default_recipe_resolution_dir="${scripts_dir}/recipes"
}

# a utility function that will help resolve a recipe name to a recipe file name.
# e.g.: file-system ==> network-anomalies/recipes/file-system.json
function resolve_recipe_file_name()
{
    recipe_file_name="${1}"
    if [ -f "${recipe_file_name}" ]
    then
        echo "${recipe_file_name}"

    elif [ -f "${default_recipe_resolution_dir}/${recipe_file_name}" ]
    then
        echo "${default_recipe_resolution_dir}/${recipe_file_name}"

    elif [ -f "${default_recipe_resolution_dir}/${recipe_file_name}.json" ]
    then
        echo "${default_recipe_resolution_dir}/${recipe_file_name}.json"

    else
        return 1
    fi
}

# a utility function to build the directory layout for everything we'll do
function build_directory_layout()
{
    output_directory="$(readlink -m ${output_directory})"

    # create the output directory.
    mkdir -p "${output_directory}"

    # create the intermediate tmp directory
    tmp_dir="${output_directory}/intermediate-dir"
    # a directory to store the execution logs
    successful_logs_dir="${tmp_dir}/execution-logs-store"

    # log files!
    trace_log="${tmp_dir}/trace-log"
    execution_log="${tmp_dir}/execution-log"
    compilation_log="${tmp_dir}/compilation-log"
    normalization_log="${tmp_dir}/normalization-log"
    profile_application_log="${tmp_dir}/profile-application-log"
    cross_validation_summary_log="${tmp_dir}/cross-validation-summary-log"

    rm -rf "${tmp_dir}" # make sure stale information can't corrupt our state
    mkdir -p "${tmp_dir}" "${successful_logs_dir}"

    # errors directory
    errors_dir="${tmp_dir}/errors/"
    error_logs_dir="${errors_dir}/failed-execution-logs/"
    remnant_logs_dir="${error_logs_dir}/last-incomplete-execution"
    mkdir -p "${errors_dir}" "${error_logs_dir}" "${remnant_logs_dir}"

    # the self log file
    self_log_file="${errors_dir}/self-log-file"

    # a directory to store unwanted by-products
    by_products_dir="${tmp_dir}/by-products"
    mkdir -p "${by_products_dir}"

    # create the profiles output directory
    profiles_dir="${output_directory}/profiles"
    mkdir -p "${profiles_dir}"

    # this directory will hold the classification of every dump into it's scheme.
    # the scheme will be used to determine which recipes will be applied to the dump.
    classification_dir="${tmp_dir}/classification"
    dump_classification_dir="${classification_dir}/dumps"
    recipe_classification_dir="${classification_dir}/recipes"
    mkdir -p "${classification_dir}" "${dump_classification_dir}" "${recipe_classification_dir}"

    # output directories
    profiles_dir="${output_directory}/profiles" # where the profiles will be written to
    cross_validation_samples_dir="${tmp_dir}/cross-validation-samples" # where the cross-validation samples will be kept (extracted from dumps)
    profile_samples_dir="${tmp_dir}/profile-samples" # where the profile generation samples will be kept (extracted from dumps)
    cross_validation_side_products="${tmp_dir}/cross-validation-side-products" # where the intermediate products of cross-validation will be written to
    mkdir -p "${profiles_dir}" "${cross_validation_samples_dir}" "${profile_samples_dir}" "${cross_validation_side_products}"

    # register the large files directories for strict removal (i.e. even if -k is specified, but unless -K is specified in the command line)
    large_files_dirs=("${profile_samples_dir}")

    # this file will track all the cross-validation files used at the end of build_recipes to suumarize cross-validation
    cross_validation_files_list="${tmp_dir}/cross-validation-files"
    touch "${cross_validation_files_list}"

    # an intermediary directory for the transformation of "process" scheme dumps into "proc-chain-csv" dumps
    proc_chain_csv_tmp_dir="${tmp_dir}/proc-chain-csv-intermediary-dir"
    mkdir -p "${proc_chain_csv_tmp_dir}"
}

# classify the dumps and the recipes
function classify_dumps_and_recipes()
{
    # go through the dumps and feed them into infer-scheme.py and then dump the files into separate files for each scheme.
    # the output is a file called 'file-system' with a list of files of file-system scheme, a file called 'network' with
    # a list of dumps of network scheme, etc...
    trace "classifying dump files"
    (
        find ${dump_files} -maxdepth 1 -type l
        find ${dump_files} -maxdepth 1 -type f
    ) |
        grep -P "${dump_filename_pattern:-}" |
        while read file_name
        do
            if [ -h "${file_name}" ]
            then
                resolved_path="$(readlink -m ${file_name})"
                [ -f "${resolved_path}" ] && echo "${file_name}"
            else
                echo "${file_name}"
            fi
        done |
        sort -u                              |
        ${infer_scheme} -q -t dump -         |
        awk -v classification_dir="${dump_classification_dir}" '{print $1 > classification_dir "/" $2}'

    for classified_dump in "${dump_classification_dir}"/*
    do
        # note that this is ok (giving the input file as the output file). sort is smart enough to first sort and then overwrite the input file
        sort -u -o "${classified_dump}" "${classified_dump}"
    done

    # do the same for recipes.
    # note that since we need to append the sub-recipe to each recipe line we need to call infer-scheme.py which is a bit
    # slow in general but since there shouldn't be too many recipes this is fine (the real problem is the dumps, and there
    # we do, as we should, everything in a single execution)
    trace "classifying recipe files"
    for index in ${!recipe_files[@]}
    do
        # the indices in recipe_files, sub_recipes, normalization_sub_recipes and arguments should match
        recipe_file="${recipe_files[${index}]}"
        sub_recipe="${sub_recipes[${index}]}"
        normalization_sub_recipe="${normalization_sub_recipes[${index}]}"
        recipe_arguments="${global_arguments:-} ${arguments[${index}]}" # note that this probably contains spaces so always quote it!

        # the output of infer-scheme.py is lines where the first word is the recipe file and the second word is the scheme the recipe uses
        (
            find "${recipe_file}" -maxdepth 1 -type l
            find "${recipe_file}" -maxdepth 1 -type f
        )|
        while read recipe_file_name
        do
            if [ -h "${recipe_file_name}" ]
            then
                resolved_path="$(readlink -m ${recipe_file_name})"
                [ -f "${resolved_path}" ] && echo "${recipe_file_name}"
            else
                echo "${recipe_file_name}"
            fi
        done |
        ${infer_scheme} -q -t recipe | awk -v classification_dir="${recipe_classification_dir}"      \
                                           -v sub_recipe="${sub_recipe}"                             \
                                           -v normalization_sub_recipe="${normalization_sub_recipe}" \
                                           -v arguments="${recipe_arguments}"                        \
                                           '{
                                               recipe_file=$1
                                               recipe_scheme=$2
                                               print recipe_file " " sub_recipe " " normalization_sub_recipe " " arguments >> classification_dir "/" recipe_scheme
                                            }'
    done

    for classified_recipe in ${recipe_classification_dir}/*
    do
        # note that this is ok (giving the input file as the output file). sort is smart enough to first sort and then overwrite the input file
        sort -u -o "${classified_recipe}" "${classified_recipe}"
    done

    # special care is required for 'proc-chain-csv' type recipes as they require us to generate the 'proc-chain-csv' dump from a raw 'process'
    # scheme typed dump, and therefor we have to be aware of that situation.
    # we therefor augment any existing pre-supplied 'proc-chain-csv' dumps with the ones available in the 'process' dumps.
    # later, when building the recipe, special care will also be taken to check what kind of dump we're processing and transform it accordingly.
    [ -f "${dump_classification_dir}/process" ] && cat "${dump_classification_dir}/process" >> "${dump_classification_dir}/proc-chain-csv"

    # move any unknown to the by-products directory
    [ -f "${recipe_classification_dir}/unknown" ] && trace "moving unknown recipes to errors directory" && mv "${recipe_classification_dir}/unknown" "${errors_dir}/unknown-recipes"
    [ -f "${dump_classification_dir}/unknown" ] && trace "moving unknown dumps to errors directory" && mv "${dump_classification_dir}/unknown" "${errors_dir}/unknown-dumps"

    # since the last operations of this function are conditionals that may indeed fail, we must specify explicitly "return 0" to indicate success.
    # otherwise, the return code is the return code of the last operation (which if ${dump_classification_dir}/unknown doesn't exist would indicate failure)
    return 0
}

# run the recipes on a dump file
function apply_recipes_to_dump()
{
    dump_file="${1}"
    recipes_file="${2}"
    trace "applying recipes in ${recipes_file} on ${dump_file}"

    cross_validation_sample="${cross_validation_samples_dir}/$(basename ${dump_file}).cross-validation"
    profile_sample="${profile_samples_dir}/$(basename ${dump_file}).profile"

    # split the dump file into profile data and cross-validation data
    trace "splitting sampling ${CROSS_VALIDATION_PARTITION}% from ${dump_file}"
    "${sampling_script}" -v -p "${CROSS_VALIDATION_PARTITION}" -i "${dump_file}" -1 "${cross_validation_sample}" -2 "${profile_sample}"

    # make the output directory
    dump_base_name="$(basename ${dump_file})"
    profile_output_dir="${profiles_dir}/${dump_base_name}"
    compiled_profile_output_dir="${profile_output_dir}/compiled"
    cross_validation_output_dir="${profile_output_dir}/cross-validation"
    cross_validation_side_products_subdir="${cross_validation_side_products}/${dump_base_name}"
    mkdir -p "${profile_output_dir}" "${compiled_profile_output_dir}" "${cross_validation_output_dir}" "${cross_validation_side_products_subdir}"

    # now go through the recipes and build the profiles.
    # note the clearification on the usage of the 'read' command in the preface comment.
    cat "${recipes_file}" | while read recipe_file sub_recipe normalization_recipe extra_arguments
    do
        trace "building a profile from the recipe ${recipe_file} with the sub-recipe ${sub_recipe} (normalization by ${normalization_recipe}), dump file ${dump_file} and extra arguments: ${extra_arguments}"

        # the profile file name will be "<recipe file name without extension>-<sub-recipe>.csv".
        profile_base_file_name="$(basename ${recipe_file} | sed -r 's/\.[^.]*$//')-${sub_recipe}"
        profile_output="${profile_output_dir}/${profile_base_file_name}.csv"
        compiled_profile_output="${compiled_profile_output_dir}/${profile_base_file_name}.json"

        # create the log files!
        echo >"${trace_log}"
        echo >"${execution_log}"
        echo >"${compilation_log}"
        echo >"${normalization_log}"
        echo >"${profile_application_log}"
        echo >"${cross_validation_summary_log}"

        # notes if we've successfully finished the entire process of generating the profile and cross validating it
        successfully_completed=0

        # predefine some variables used inside the loop
        normalized_cv_sample_file="${cross_validation_side_products_subdir}/${profile_base_file_name}.normalized-sample"
        profile_application_file="${cross_validation_side_products_subdir}/${profile_base_file_name}.profile-applied"

        # this loop construct has exactly one iteration and it allows us to use 'break' to stop the loop mid-stride in case one of the commands fail and
        # continue to saving the logs in the errors directory without exiting the entire program (a sort of a goto alternative).
        for stub in 1
        do
            # generate the profile!
            #
            # note that since extra_arguments contains *multiple* switches for the command line (it's content is something like "--transformation-arg trans_id arg_name value --alias-arg alias value ...")
            # we can't use quotation around it when invoking the command line! (otherwise it will evaluate to a single argument transferred to the script)

            # IMPORTANT NOTE: see the bash cheat sheet bulletin #11 if you don't understand the redirection construct at the end of the command!
            #                 it's a subshell redirection and a separate 'or' clause on the executed command.
            ${csv_stats} -v -m ${parallelization:-} -o "${profile_output}" --recipe "${recipe_file}" --sub-recipe "${sub_recipe}" ${aliases_argument_strictness} ${extra_arguments} "${profile_sample}" &> >(tee "${execution_log}" >&2) || break

            # compile the generated profile!
            trace "compiling ${recipe_file} (sub-recipe: ${sub_recipe}) into ${compiled_profile_output}" |& tee -a "${trace_log}" >&2
            ${canon_paths_applier} -v -o "${compiled_profile_output}" compile-by-recipe -r "${recipe_file}" -s "${sub_recipe}" "${profile_output}" &> >(tee "${compilation_log}" >&2) || break

            # cross validate!
            trace "cross-validating ${recipe_file} (sub-receip: ${sub_recipes}) on ${dump_file} (sample: ${cross_validation_sample})" |& tee -a "${trace_log}" >&2

            # the normalization phase - take the cross-validation sample and transform it into a standard form that
            # the profiles can be applied to.
            trace "${dump_file} - $(basename ${recipe_file}):${sub_recipe}: normalizing cross-validation sample in ${cross_validation_sample} to ${normalized_cv_sample_file}" |& tee -a "${trace_log}" >&2
            "${csv_stats}" -v -m ${parallelization:-} -o "${normalized_cv_sample_file}" --recipe "${recipe_file}" --sub-recipe "${normalization_recipe}" ${aliases_argument_strictness} "${cross_validation_sample}" &> >(tee "${normalization_log}" >&2) || break

            # apply the profile
            trace "${dump_file} - $(basename ${recipe_file}):${sub_recipe}: applying ${compiled_profile_output} to ${normalized_cv_sample_file} and writing results to ${profile_application_file}" |& tee -a "${trace_log}" >&2
            "${canon_paths_applier}" -v -o "${profile_application_file}" apply-by-recipe -r "${recipe_file}" -s "${normalization_recipe}" -p "${compiled_profile_output}" "${normalized_cv_sample_file}" &> >(tee "${profile_application_log}" >&2) || break

            # summarize the cross-validation results
            cross_validation_results="${cross_validation_output_dir}/${profile_base_file_name}.cross-validation.json"
            trace "${dump_file} - $(basename ${recipe_file}):${sub_recipe}: calculating cross-validation percentages." |& tee -a "${trace_log}" >&2
            "${cross_validation_summary}" -v generate-by-recipe -o "${cross_validation_results}" -r "${recipe_file}" -s "${normalization_recipe}" -t "${sub_recipe}" "${profile_application_file}" &> >(tee "${cross_validation_summary_log}" >&2) || break

            echo "${cross_validation_results}" >> "${cross_validation_files_list}"

            trace "done with ${recipe_file} and sub-recipe ${sub_recipe}" |& tee -a "${trace_log}" >&2

            successfully_completed=1
        done

        if [ "${successfully_completed}" == "0" ]
        then
            execution_logs_dir="$(mktemp -d ${error_logs_dir}/execution.XXXXXX)"
            trace "profile generation generation failed for ${dump_file}: ${recipe_file}[${sub_recipe}, ${normalization_recipe}] - saving error logs to ${execution_logs_dir}."
        else
            execution_logs_dir="$(mktemp -d ${successful_logs_dir}/execution.XXXXXX)"
            trace "profile generation generation succeeded for ${dump_file}: ${recipe_file}[${sub_recipe}, ${normalization_recipe}] - saving logs to ${execution_logs_dir}."
        fi

        mv "${trace_log}" "${execution_log}" "${compilation_log}" "${normalization_log}" "${profile_application_log}" "${cross_validation_summary_log}" "${execution_logs_dir}"

        rm_large_audit -f "${normalized_cv_sample_file}" "${profile_application_file}"
    done

    trace "finished with ${dump_file}!"

    # remove the split dump
    rm_large_audit -rf "${profile_sample}"
    rm_audit -rf "${cross_validation_sample}"
}

function apply_proc_chain_recipes_to_dump()
{
    # this function accepts 2 kinds of dumps: proc-chain-csv dumps and process dumps.
    # proc-chain-csv dumps are passed to apply_recipes_to_dump as-is while process dump files
    # are transformed via a utility script into an intermediary file typed proc-chain-csv.
    # the intermediate file is then discarded.

    dump_file="${1}"
    recipes_file="${2}"
    trace "applying recipes in ${recipes_file} on ${dump_file}"

    # extract the file's scheme using infer-scheme.py.
    # the output is formatted "<file> <scheme>". it is taken to a simple awk command that just extracts the scheme
    scheme_name="$(${infer_scheme} -t dump ${dump_file} | awk '{print $NF}')"

    if [ "${scheme_name}" == "proc-chain-csv" ]
    then
        apply_recipes_to_dump "${dump_file}" "${recipes_file}"
    elif [ "${scheme_name}" == "process" ]
    then
        proc_chain_dump_file_tmp_dir="${proc_chain_csv_tmp_dir}/$(basename ${dump_file})-$(mktemp -u XXXX)"
        proc_chain_dump_file="${proc_chain_dump_file_tmp_dir}/$(basename ${dump_file})"
        proc_chain_tmp_dir="${proc_chain_dump_file_tmp_dir}/tmp"

        mkdir -p "${proc_chain_dump_file_tmp_dir}" "${proc_chain_tmp_dir}"

        # transform dump_file into proc-chain-csv scheme
        trace "transforming ${dump_file} to proc-chain-csv scheme by an intermediary file ${proc_chain_dump_file}"
        standard_processes_file="${standard_processes_file:-${default_standard_processes_file}}"
        "${process_dump_conversion_script}" -o "${proc_chain_dump_file}" --leaves -i "${dump_file}" -p "${standard_processes_file}" --csv --heading --temp-dir "${proc_chain_tmp_dir}" -c process_id:parent_id:instance:parent_instance:host:process:command:process_name:parent_path

        trace "applying ${recipes_file} to ${proc_chain_dump_file}"
        apply_recipes_to_dump "${proc_chain_dump_file}" "${recipes_file}"

        rm_large_audit -rf "${proc_chain_dump_file_tmp_dir}"
    else
        perror "invalid scheme passed to proc-chain-csv handling: ${scheme_name}"
    fi
}

# run through the dumps and build the recipes
function build_recipes()
{
    trace "building recipes for ${dump_classification_dir}"

    # go through the categorized dump files
    for dump_category_file in ${dump_classification_dir}/*
    do
        # get the scheme name
        scheme_name="$(basename ${dump_category_file})"

        trace "handling the dumps in ${dump_category_file} (${scheme_name} scheme)"

        # if there are no recipes for these dumps then skip them
        recipes_file="${recipe_classification_dir}/${scheme_name}"
        if ! [ -f "${recipes_file}" ]
        then
            trace "no recipes file for ${scheme_name}"
            continue
        fi

        trace "recipes file found at ${recipes_file}"

        # read the dump files list and run the recipes for all of them
        cat "${dump_category_file}" | while read dump_file
        do
            trace "processing ${dump_file} with the recipes in ${recipes_file}"
            if [ "${scheme_name}" == "proc-chain-csv" ]
            then
                apply_proc_chain_recipes_to_dump "${dump_file}" "${recipes_file}"
            else
                apply_recipes_to_dump "${dump_file}" "${recipes_file}"
            fi
        done
    done

    trace "done building the profiles!"

    # wrap-up the cross validation
    sort -u -o "${cross_validation_files_list}" "${cross_validation_files_list}"
    if [ "$(wc -l ${cross_validation_files_list} | cut -d' ' -f1)" -gt 0 ] # wc outputs "<line count> <file name>" and the 'cut' just prints the line count
    then
        trace "summarizing cross-validation"
        # note the lack of quotations around $(cat "${cross_validation_files_list}") - we need it to expand to multiple arguments!
        "${cross_validation_summary}" format -o "${output_directory}/cross-validation-summary.txt" -f summary $(cat "${cross_validation_files_list}")
    else
        trace "no cross-validation results to summarize"
    fi
}

function archive_results()
{
    # set the default archive dir
    archive_dir="${archive_dir:-.archive}"

    if [ "${archive_dir}" == "none" ]
    then
        trace "skipping archiving by command line specification"
        return
    fi

    if ! [ -d "${archive_dir}" ]
    then
        trace "can't archive. ${archive_dir} doesn't exist."
        return
    fi

    results_archive="${archive_dir}/profile-archive-$(date +'%Y_%m_%d_%I_%M_%p').tar.gz"
    trace "archiving results to ${results_archive}"
    tar -czf "${results_archive}" -C "${output_directory}" $(ls -1 "${output_directory}" | grep -v $(basename ${tmp_dir}))
    trace "done archving to ${results_archive}!"
}

#### main

recipe_files=() # a list of recipe files
sub_recipes=() # the matching list of sub-recipes
normalization_sub_recipes=() # the matching list of normalization sub-recipes for cross-validation

# this array will hold the aggregation elements, aggregation-by elements and transformation arguments passed to the recipes.
# since the entries of this array will be of the form "--elem-arg elem-id some-argument value --by-elem-arg ..." (i.e. a string with spaces)
# we can't use the regular list expansion trick to add an item to it's beginning: ("new-item" ${arguments[@]})
# e.g.:
#       x=("b c" d)
#       x=(a ${x[@]})
#
# now instead of x having 3 arguments:
#   1. a
#   2. "b c"
#   3. d
# it has 4 arguments:
#   1. a
#   2. b
#   3. c
#   4. d
#
# so we'll aggregate the
arguments=() # the arguments to be passed to aggregation elements in a recipe
# this string is the arguments string to be passed to all recipes (in contrast to arguments that's per-recipe
global_arguments=""

# this variable determines what happens if csv-stats.py gets an argument by alias that wasn't used.
# by default we use the --ignore-unknown-aliases switch to allow unused aliases to be glossed over silently.
# if this will be set to "" unused aliases will cause the script to fail.
#
# when this is kept at the default lenient state then you can supply all the argument aliases any of the recipes requires via the -L switch and -l switches.
# any unnecessary ones (deemed necessary or not by the recipe executed by csv-stats.py) argument aliases will be discarded.
# if this is set to "" then you will only be able to use argument aliases that appear accross all the recipes.
aliases_argument_strictness="--ignore-unknown-aliases"

# pushes a new element to the bottom of 'arguments'
function push_argument()
{
    # we're going to do this straight forward and stupid

    # copy the arguments
    new_arguments=("")
    for key in ${!arguments[@]}
    do
        new_arguments[$((key+1))]=${arguments[${key}]}
    done

    # copy the new arguments back
    for key in ${!new_arguments[@]}
    do
        arguments[${key}]=${new_arguments[${key}]}
    done
}

# by default define_constants() should not override anything pre-set by environment variables
constants_definition_policy="soft"

# load the paths of the scripts we'll use
publish_framework_scripts

while getopts ":hHFo:kKp:e:d:r:s:n:a:A:b:B:t:T:l:L:Sgi:xP:" option
do
    case "${option}" in
        "h") print_help >&2
             exit 0
             ;;

        "H") verbose_help >&2
             exit 0
             ;;

        "F") constants_definition_policy="forced"
             ;;

        "o") output_directory="${OPTARG}"
             ;;

        "i") : # nop
             ;;

        "g") self_logging=0
             ;;

        "x") set -x
             ;;

        "k") keep_intermediate_files="1"
             ;;

        "K") keep_intermediate_files="1"
             keep_everything="1"
             ;;

        "p") parallelization="--parallelization ${OPTARG}"
             ;;

        "e") archive_dir="${OPTARG}"
             ;;

        "d") dump_files="${dump_files:-} ${OPTARG}"
             ;;

        "r") resolved_recipe=$(resolve_recipe_file_name "${OPTARG}") || perror "couldn't resolve recipe ${OPTARG}"
             [ "${resolved_recipe}" != "${OPTARG}" ] && trace "recipe '${OPTARG}' was resolved to '${resolved_recipe}'"

             if [ -z "${recipes_initialized:-}" ]
             then
                 # see note 8 in the "bash notes" prologue of this file for an elaboration on why this if is required.
                 # (to be clear, in up-to-date bash versions you could just run the 'else' section as the variables were
                 #  already initialized to  empty lists but some bash versions have a problem with empty lists)

                 recipe_files=("${resolved_recipe}")
                 sub_recipes=("main")
                 normalization_sub_recipes=("main.normalize")
                 arguments=("")

                 recipes_initialized="1"
             else
                 recipe_files=("${resolved_recipe}" ${recipe_files[@]}) # prepend the recipe to the list (will make the slicing in -s a little less ugly)
                 sub_recipes=("main" ${sub_recipes[@]}) # the default sub-recipe is the profile itself
                 normalization_sub_recipes=("main.normalize" ${normalization_sub_recipes[@]}) # the default normalization sub-recipe
                 push_argument
             fi
             ;;

        "s") [ "${#recipe_files[@]}" -gt 0 ] || perror "-s ${OPTARG} doesn't match a recipe (-r) switch"
             sub_recipes=("${OPTARG}" ${sub_recipes[@]:1})
             ;;

        "n") [ "${#recipe_files[@]}" -gt 0 ] || perror "-n ${OPTARG} doesn't match a recipe (-r) switch"
             normalization_sub_recipes=("${OPTARG}" ${normalization_sub_recipes[@]:1})
             ;;

        # switches dedicated to passing specific arguments to the recipes

        "a") [ "${#arguments[@]}" -gt "0" ] || perror "you must specify a recipe (-r) before an aggregation element argument (-a)"
             element_id="${OPTARG}" # get the element id
             argument_name="${@:${OPTIND}:1}" # the argument to set
             argument_value="${@:${OPTIND}+1:1}" # the value to use
             let "OPTIND += 2" # skip 2 elements when continuing the parsing

             # make sure the arguments are not switches!
             echo "${argument_name}" | grep -qP '^\s*-' && perror "missing 2 arguments for -a switch"
             echo "${argument_value}" | grep -qP '^\s*-' && perror "missing 1 argument for -a switch"

             arguments[0]+=" --elem-arg ${element_id} ${argument_name} ${argument_value} "
             ;;

        "A") element_id="${OPTARG}" # get the element id
             argument_name="${@:${OPTIND}:1}" # the argument to set
             argument_value="${@:${OPTIND}+1:1}" # the value to use
             let "OPTIND += 2" # skip 2 elements when continuing the parsing

             # make sure the arguments are not switches!
             echo "${argument_name}" | grep -qP '^\s*-' && perror "missing 2 arguments for -A switch"
             echo "${argument_value}" | grep -qP '^\s*-' && perror "missing 1 argument for -A switch"

             global_arguments+=" --elem-arg ${element_id} ${argument_name} ${argument_value} "
             ;;

        "b") [ "${#arguments[@]}" -gt "0" ] || perror "you must specify a recipe (-r) before an aggregation-by element argument (-b)"
             by_element_id="${OPTARG}" # get the by-element id
             argument_name="${@:${OPTIND}:1}" # the argument to set
             argument_value="${@:${OPTIND}+1:1}" # the value to use
             let "OPTIND += 2" # skip 2 elements when continuing the parsing

             # make sure the arguments are not switches!
             echo "${argument_name}" | grep -qP '^\s*-' && perror "missing 2 arguments for -b switch"
             echo "${argument_value}" | grep -qP '^\s*-' && perror "missing 1 argument for -b switch"

             arguments[0]+=" --by-elem-arg ${by_element_id} ${argument_name} ${argument_value} "
             ;;

        "B") by_element_id="${OPTARG}" # get the by-element id
             argument_name="${@:${OPTIND}:1}" # the argument to set
             argument_value="${@:${OPTIND}+1:1}" # the value to use
             let "OPTIND += 2" # skip 2 elements when continuing the parsing

             # make sure the arguments are not switches!
             echo "${argument_name}" | grep -qP '^\s*-' && perror "missing 2 arguments for -B switch"
             echo "${argument_value}" | grep -qP '^\s*-' && perror "missing 1 argument for -B switch"

             global_arguments+=" --by-elem-arg ${by_element_id} ${argument_name} ${argument_value} "
             ;;

        "t") [ "${#arguments[@]}" -gt "0" ] || perror "you must specify a recipe (-r) before a transformation argument (-t)"
             transformation_id="${OPTARG}" # get the transformation id
             argument_name="${@:${OPTIND}:1}" # the argument to set
             argument_value="${@:${OPTIND}+1:1}" # the value to use
             let "OPTIND += 2" # skip 2 elements when continuing the parsing

             # make sure the arguments are not switches!
             echo "${argument_name}" | grep -qP '^\s*-' && perror "missing 2 arguments for -t switch"
             echo "${argument_value}" | grep -qP '^\s*-' && perror "missing 1 argument for -t switch"

             arguments[0]+=" --transformation-arg ${transformation_id} ${argument_name} ${argument_value} "
             ;;

        "T") transformation_id="${OPTARG}" # get the transformation id
             argument_name="${@:${OPTIND}:1}" # the argument to set
             argument_value="${@:${OPTIND}+1:1}" # the value to use
             let "OPTIND += 2" # skip 2 elements when continuing the parsing

             # make sure the arguments are not switches!
             echo "${argument_name}" | grep -qP '^\s*-' && perror "missing 2 arguments for -T switch"
             echo "${argument_value}" | grep -qP '^\s*-' && perror "missing 1 argument for -T switch"

             global_arguments+=" --transformation-arg ${transformation_id} ${argument_name} ${argument_value} "
             ;;

        "l") [ "${#arguments[@]}" -gt "0" ] || perror "you must specify a recipe (-r) before a by-alias argument (-l)"
             arg_alias="${OPTARG}" # get the alias
             argument_value="${@:${OPTIND}:1}" # the value to use
             let "OPTIND += 1" # skip 1 element when continuing the parsing

             # make sure the arguments are not switches!
             echo "${argument_value}" | grep -qP '^\s*-' && perror "missing 1 argument for -l switch"

             arguments[0]+=" --arg-by-alias ${arg_alias} ${argument_value} "
             ;;

        "L") arg_alias="${OPTARG}" # get the alias
             argument_value="${@:${OPTIND}:1}" # the value to use
             let "OPTIND += 1" # skip 1 element when continuing the parsing

             # make sure the arguments are not switches!
             echo "${argument_value}" | grep -qP '^\s*-' && perror "missing 1 argument for -L switch"

             global_arguments+=" --arg-by-alias ${arg_alias} ${argument_value} "
             ;;

        "S") aliases_argument_strictness="" # don't pass --ignore-unknown-aliases to csv-stats.py
             ;;

        "P") standard_processes_file="${OPTARG}"
             ;;
    esac
done
shift $((${OPTIND}-1))

# load the constants!
define_constants "${constants_definition_policy}"

# read the filename patterns supplied in the command line (applied just to dump-files)
dump_filename_pattern=""
for pattern in ${@}
do
    if [ -z "${dump_filename_pattern}" ]
    then
        dump_filename_pattern="(${pattern})"
    else
        dump_filename_pattern="${dump_filename_pattern}|(${pattern})"
    fi
done

[ -n "${dump_filename_pattern}" ] && dump_filename_pattern="/[^/]*(${dump_filename_pattern})[^/]*$" # if the filter isn't empty make sure it's applied to the end of the file name
trace "applying the following filter to dump file names: \"${dump_filename_pattern}\""

# make sure we have all the necessary input we need to start working
trace "validating execution environment"
validate_execution_env

# register cleanup
trap cleanup EXIT

# build the work directory layout
trace "building output directory layout"
build_directory_layout

# enable self logging
if [ "${self_logging:-1}" == "1" ]
then
    trace "enabling self logging into ${self_log_file}"
    exec 3>&1 4>&2 &> >(tee "${self_log_file}" >&2)
fi

# classify dumps and receips
trace "classifying the dumps and the recipes"
classify_dumps_and_recipes

# build the profiles! (specified in the recipes...)
trace "building the profiles"
build_recipes

# disable self logging
if [ "${self_logging:-0}" == "1" ]
then
    trace "disabling self logging into ${self_log_file}"
    exec >&3 2>&4
    sleep 2 # wait for the self-logging tee to finish it's job
fi

# archive the results
trace "archiving profiles"
archive_results

trace "finished successfully"

