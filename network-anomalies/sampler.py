#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""
Read the CSV file, create dictionary of instances, split the instances into two groups as specified
by the 'partition' argument

The script is single pass, processes the input file one line at time.
Performance complexity is O(rows), space complexity is O(instances)

Usage:
  sampler.py -h | --help
  sampler.py [-v] [-k <REGEX>] -i <FILES> [-c <INT:INT>] [-p <INT>] [-1 <FILE> -2 <FILE>] [-s <STR>]

Options:
  -h --help                      Show this screen.
  -v --verbose                   Show debug prints.
  -i --infile=<FILES>            Input files separated by column ':'
  -k --key=<REGEX>               Make sure that process path matches this regex [default: .*].
  -p --partition=<FLOAT>         Percentage of the whole data in a sample [default: 10.0]
  -1 --sample1=<FILE>            Output file 1
  -2 --sample2=<FILE>            Output file 2
  -c --columns=<STR>             Index/header of columns processPath, instanceID, HostID [default: process:instance:host]
  -s --splitby=<STR>             Split by 'hostid' or by 'instanceid' [default: hostid]

Example:
  ./sampler.py -k ".+chrome.+" -i test-raw-data.csv -c 0:6:5 -p 10 -1 test-raw-data.10.csv -2 test-raw-data.90.csv
"""

import logging
import re
import random
import csv
import sys
from docopt import docopt

class NoNullByteFile:
    def __init__(self, file_obj):
        self.file_obj_iter = iter(file_obj)

    def __iter__(self):
        return self

    def __next__(self):
        return next(self.file_obj_iter).replace("\x00", "x00")

def process_arguments(arguments):
    '''
    Parse command line arguments set defaults value where possible
    '''
    filenames = arguments["--infile"]

    if arguments["--key"]:
        process_name_pattern = arguments["--key"]

    partition_str = arguments["--partition"]
    if partition_str:
        partition = float(partition_str)

    columns_str = arguments["--columns"]
    column_by_name = False

    columns_str = columns_str.split(":")
    try:
        column_process_path = int(columns_str[0])
        column_process_instance = int(columns_str[1])
        column_host = int(columns_str[2])
    except ValueError:
        column_process_path = columns_str[0]
        column_process_instance = columns_str[1]
        column_host = columns_str[2]
        column_by_name = True

    filenames = filenames.split(":")
    filename_1, filename_2 = filenames[0] + ".1", filenames[0] + ".2"
    if arguments["--sample1"]:
        filename_1 = arguments["--sample1"]
    if arguments["--sample2"]:
        filename_2 = arguments["--sample2"]

    split_by_host_id = arguments["--splitby"] == "hostid"

    return {
        "filenames":filenames,
        "column_process_path":column_process_path,
        "column_process_instance":column_process_instance,
        "column_host":column_host,
        "column_by_name":column_by_name,
        "partition":partition,
        "filename_1":filename_1,
        "filename_2":filename_2,
        "process_name_pattern":process_name_pattern,
        "split_by_host_id":split_by_host_id,
        "verbose" : arguments["--verbose"],
        }

class Sampler(object):
    '''
    Usage: Sampler(logger, filenames=configuration["filenames"]).do_job()
    '''
    def __init__(self, logger, **kwargs):
        '''
        This is the module API
        @param logger: I need a logger to print things out
        @param kwargs["filenames"]: a list of files to process, can not be empty
        @return: dictionary with debug stats and things
        '''
        self.filenames = kwargs.get("filenames", [])
        assert isinstance(self.filenames, list), "Filenames argument should be a list"
        assert len(self.filenames) > 0, "No files to process"
        self.logger = logger
        self.column_process_path = kwargs["column_process_path"]
        self.column_process_instance = kwargs["column_process_instance"]
        self.column_host = kwargs["column_host"]
        self.column_by_name = kwargs['column_by_name']
        self.columns_inferred = False
        self.partition = kwargs.get("partition", 10.0)
        self.filename_1 = kwargs.get("filename_1", self.filenames[0] + ".1")
        self.filename_2 = kwargs.get("filename_2", self.filenames[0] + ".2")
        self.process_name_pattern = kwargs.get("process_name_pattern", ".*")
        self.delimiter = kwargs.get("delimiter", "\t")
        self.heading_copied = False
        self.headings_presents = kwargs.get("headings_presents", True)
        self.split_by_host_id = kwargs.get("split_by_host_id", True)
        self.row_set_pre_process_hook(Sampler._dummy_hook)
        self.row_set_post_process_hook(Sampler._dummy_hook)
        self.files_info = {}
        self.counters = [0, 0]
        self.groups_count = [0, 0]
        self.instance_group = {}
        self.skipped_lines = 0

    def do_job(self):
        '''
        Call the method to process the input files
        '''
        self._log_status()

        f1, f2 = open(self.filename_1, "wt"), open(self.filename_2, "wt")
        writer_1, writer_2 = csv.writer(f1, delimiter=self.delimiter), csv.writer(f2, delimiter=self.delimiter)

        lines_total = 0
        for filename in self.filenames:
            if not filename in self.files_info:   # Protect against situation the same file is mentioned more than once
                self.files_info[filename] = {"instances":0, "lines":0, "errors":0}
            else:
                self.logger.info("File {0} already processed".format(filename))
                continue

            self.logger.info("Processing {0} ... ".format(filename))

            processing_stats = {"lines":0}
            with open(filename, "rt") as infile:
                reader = csv.reader(NoNullByteFile(infile), delimiter=self.delimiter, quoting=csv.QUOTE_NONE)
                self._process_headings(filename, reader, (writer_1, writer_2), processing_stats)
                self._process_file(filename, reader, (writer_1, writer_2), processing_stats)
            lines = processing_stats["lines"]
            self.logger.info("Processed {0} lines in {1}".format(lines, filename))
            lines_total = lines_total + lines
            self.files_info[filename]["lines"] = lines

        f1.close()
        f2.close()
        self._log_results(lines_total)

        return {"file1":self.filename_1, # output files
                "file2:":self.filename_2,
                "lines_total":lines_total, # total rows processed
                "rows":self.counters, # number of rows in the two groups
                "split":self.groups_count, # number of instances in the two groups
                "instances":self.instance_group, # total instances
                "skipped":self.skipped_lines, # skipped lines because of an error
                "files_info":self.files_info} # per file stats

    def _log_status(self):
        self.logger.info("Running for files {0}, process path {1}, process instance {2}, split {3}%, create {4} and {5}".format(
            self.filenames,
            self.column_process_path,
            self.column_process_instance,
            self.partition,
            self.filename_1,
            self.filename_2))

    def _log_results(self, lines_total):
        self.logger.info("Stats per file")
        for filename, file_info in self.files_info.items():
            self.logger.info("{0}: {1}".format(filename, file_info))

        max_rows_per_instance = 0
        min_rows_per_instance = lines_total + 1
        for _, (_, rows) in self.instance_group.items():
            max_rows_per_instance = max(max_rows_per_instance, rows)
            min_rows_per_instance = min(min_rows_per_instance, rows)
        self.logger.info("Max/min rows per instance {0}/{1}".format(max_rows_per_instance, min_rows_per_instance))

        skipped_lines = self.skipped_lines
        if skipped_lines:
            self.logger.error("Events {0}, instances {1}  from total {2} rows, instances {3}, skipped {4} rows".format(self.counters, self.groups_count, lines_total, len(self.instance_group), skipped_lines))
        else:
            self.logger.info("Events {0}, instances {1}  from total {2} rows, instances {3}".format(self.counters, self.groups_count, lines_total, len(self.instance_group)))


    def row_set_pre_process_hook(self, hook):
        '''
        The hook will be called right after a row is read from the input file
        '''
        if hook:
            self.row_pre_process_hook = hook
        else:
            self.row_pre_process_hook = Sampler._dummy_hook

    def row_set_post_process_hook(self, hook):
        '''
        The hook will be called just before a row is written to the output file
        '''
        if hook:
            self.row_post_process_hook = hook
        else:
            self.row_post_process_hook = Sampler._dummy_hook

    @staticmethod
    def _dummy_hook(**_):
        return

    @staticmethod
    def _match_process_path(process_name_pattern, path):
        '''
        Apply regex to the process name or path. This is mainly a sanity check
        '''
        m = re.match(process_name_pattern, path)
        return m is not None

    def _get_group(self, filename, row):
        '''
        If (instance, host) tuple is in the dictionary instance_group I return the stored partition index - 0 or 1
        If the tuple is not in the instance_group dictionary I choose a random number between 1 and 100
        If the random number is larger than the partition size (default value 10%) I choose partition 1
        otherwise 0
        If the data set is very large and the number of instances is significant this code will produce reasonable
        number of rows (events) in each partition. The idea is to have complexity O(n) and not O(2*n) and
        space complexity O(1)
        '''
        instance = row[self.column_process_instance]
        _ = int(instance)  # sanity check - instance is integer
        host = row[self.column_host]
        if self.split_by_host_id:
            key = host
        else:
            key = (instance, host)
        (group, rows) = self.instance_group.get(key, (None, None))  # what group (partition) the instance belongs to?
        if group is None:  # if I have never seen (host, instance) before
            random_integer = random.uniform(1, 100)  # generate a random number
            if random_integer <= self.partition:  # and assign a group to the process instance
                group = 0
            else:
                group = 1
            rows = 0
            self.instance_group[key] = (group, rows)  # Store the partition index in the dictionary
            self.groups_count[group] = self.groups_count[group] + 1  # Update a counter of instances belonging to the specific partition
            self.files_info[filename]["instances"] = self.files_info[filename]["instances"] + 1

        # update distribution of the rows across the instances
        rows = rows + 1
        self.instance_group[key] = (group, rows)
        return group

    def _process_headings(self, filename, reader, writers, processing_stats):
        if self.headings_presents:
            headings = next(reader)
            if self.column_by_name and not self.columns_inferred:
                self._infer_columns(headings)
            processing_stats["lines"] = processing_stats["lines"] + 1
            if not self.heading_copied:  # copy heading line to the split files if I did not already
                for writer in writers:
                    writer.writerow(headings)
                self.heading_copied = True
                self.logger.debug("Copy the headings line {0} from {1}".format(headings, filename))

    def _process_file(self, filename, reader, writers, processing_stats):
        lc = 0
        resume = True
        exceptions = {}
        unicode_errors = 0

        while resume:
            try:
                resume = False
                for row in reader:
                    lc += 1
                    line_index = processing_stats["lines"] + 1
                    processing_stats["lines"] = line_index
                    self.row_pre_process_hook(filename=filename, row=row, line=line_index)
                    # if isinstance(row, list):
                    #    row = row[0]
                    process_path = row[self.column_process_path]
                    if not Sampler._match_process_path(self.process_name_pattern, process_path):
                        self.logger.error("process path {0} does not match pattern {1} skip line {2}:{3}".format(process_path, self.process_name_pattern, filename, line_index))
                        self.skipped_lines = self.skipped_lines + 1
                        self.files_info[filename]["errors"] = self.skipped_lines
                        continue
                    try:
                        group = self._get_group(filename, row)
                    except Exception as e:
                        self.logger.error("Failed to process line {0}:{1} '{2}', {3}".format(filename, line_index, row, e))
                        self.skipped_lines = self.skipped_lines + 1
                        self.files_info[filename]["errors"] = self.files_info[filename]["errors"] + 1
                        continue
                    self.row_post_process_hook(filename=filename, row=row, line=line_index)
                    writers[group].writerow(row)
                    self.counters[group] = self.counters[group] + 1
            except UnicodeDecodeError:
                unicode_errors += 1
            except csv.Error as error:
                arg = str(error.args)
                if arg in exceptions:
                    exceptions[arg] += 1
                else:
                    exceptions[arg] = 1
                    self.logger.info("failed to parse line #%d", lc, exc_info=True)
                resume = True

        for exception, count in exceptions.items():
            logging.warn("csv.Error %s encountered %d times", exception, count)
        logging.warn("unicode_errors %d", unicode_errors)

    def _infer_columns(self, header):
        header_indices = {header[index] : index for index in range(len(header))}
        self.column_process_path = header_indices[self.column_process_path]
        self.column_process_instance = header_indices[self.column_process_instance]
        self.column_host = header_indices[self.column_host]

        self.columns_inferred = True


def main():
    '''
    A placeholder
    '''
    arguments = docopt(__doc__, version='process')
    configuration = process_arguments(arguments)

    logging.basicConfig(level=logging.DEBUG if configuration['verbose'] else logging.WARN, format="%(asctime)s [%(levelname)s] %(message)s")
    logger = logging.getLogger('process')

    configuration["headings_presents"] = True
    configuration["delimiter"] = "\t"

    results = Sampler(logger, **configuration).do_job()
    # results = Sampler(logger, filenames=configuration["filenames"]).do_job()

    # no need to fail the program if lines were skipped
    # return -results["skipped"]  # return 0 if nothing skipped

if __name__ == '__main__':
    sys.exit(main())

