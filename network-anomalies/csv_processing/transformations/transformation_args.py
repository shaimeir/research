#!/usr/bin/env python3

def column_arg(column=-1):
    column = int(column)
    assert column != 0
    return column - 1 if column > 0 else column

def bool_arg(value):
    if isinstance(value, bool):
        return value

    assert isinstance(value, str)
    value = value.strip().lower()
    assert value in ("true", "false")
    return value == "true"

def positive_int(value, can_be_zero=True):
    value = int(value)
    assert (value >= 0) if can_be_zero else (value > 0)
    return value

def number_arg(value):
    try:
        return int(value)
    except:
        return float(value)

