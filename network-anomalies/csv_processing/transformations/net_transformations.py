#!/usr/bin/env python3

import ipaddress
import re
import logging
import sys
import pprint
import datetime
import pathlib
import ipaddress
from collections import Iterable

from .. import aggregation
from . import transformation_args
from ..canonization import autogen_name_classifier, tree_canonization

_known_ports = [
    1029, # MS-DCOM
    1080, # SOCKS proxy
    1194, # Open VPN
    1293, # IPSec
    1443, # MS-SQL
    1688, # MS-KMS
    1900, # SSDP
    2483, # Oracle DB
    2484, # Oracle DB (SSL)
    3306, # MySQL
    3268, # LDAP
    3269, # LDAP (SSL)
    3389, # RDP
    5443, # Vertica
    5500, # VNC
    5722, # MS-SYSVOL
    5800, # VNC stuff
    5900, # VNC stuff
    5901, # VNC stuff
    8080, # HTTP proxy
    8172, # MS-IIS remote administration
    8443, # HTTPS proxy
    9389, # MS-Bullshit (powershell and stuff, bitch)
] + list(range(4502,4535)) # silverlight
@aggregation.make_transformation("coalesce-unknown-ports")
def coalesce_unkown_ports(aggregator):
    '''
    assumes the values are ports (a dict of port and prevalence) and changes every unknown port to 77777
    '''

    for key, value_map in aggregator.aggregates.items():
        values = list(value_map.keys())
        unknown_ports_prevalence = 0
        for value in values:
            if value >= 1024 and value not in _known_ports:
                unknown_ports_prevalence += value_map.pop(value)

        if unknown_ports_prevalence > 0:
            value_map[77777] = unknown_ports_prevalence

############################## Unified Canon Tree Spanning

def _parse_network_canon_tree_args(min_prevalence_percent, min_occurrences, prevalence_map_field, ip_col=1, port_col=2, protocol_col=3, prevalent_key_field=1, debug_print=False):
    return (transformation_args.column_arg(ip_col),
            transformation_args.column_arg(port_col),
            transformation_args.column_arg(protocol_col),
            transformation_args.positive_int(min_prevalence_percent),
            transformation_args.positive_int(min_occurrences),
            prevalence_map_field,
            transformation_args.column_arg(prevalent_key_field),
            transformation_args.bool_arg(debug_print))

class _NetworkPathBuilder(tree_canonization.SimplePathBuilder):
    def __init__(self, ip_col, port_col, protocol_col):
        super().__init__((ip_col, port_col, protocol_col))

    def _path_decompose(self, ip_addr, port, protocol):
        address = ipaddress.ip_network(ip_addr) # will automatically assume 32-bit prefix
        assert address.prefixlen == 32
        parts = []
        while address.prefixlen > 0:
            parts.append(str(address))
            address = address.supernet()

        return [str(protocol), str(port)] + parts[::-1]

    def _path_reconstruct(self, node):
        node_path = node.path
        while len(node_path) > 0 and node_path[-1].name == '[SUBNET]':
            node_path = node_path[:-1]

        if len(node_path) <= 1:
            return None

        protocol = node_path[1].name
        port = node_path[2].name if len(node_path) > 2 else '0'
        address = node_path[-1].name if len(node_path) > 3 else '0.0.0.0/0'

        return (protocol, port, address)

@aggregation.make_transformation("network-canon-tree", _parse_network_canon_tree_args)
def network_canon_tree(aggregator, ip_column, port_column, protocol_column, min_percent, min_occurrences, prevalence_map_field, prevalent_key_field, debug_print):
    '''
    span the network canon tree.
    '''
    prevalence_map = getattr(aggregator, prevalence_map_field)
    canonizer_factory = lambda key: tree_canonization.get_relative_prevalence_count_canonizer(0 if prevalence_map is None else prevalence_map.get(key[prevalent_key_field], None), min_percent, min_occurrences)

    tree_canonization.canonizing_transformation(
        aggregator,
        canonizer_factory,
        _NetworkPathBuilder(ip_column, port_column, protocol_column),
        trap_branch_name='[SUBNET]',
        trap_leaf_name='[SUBNET]',
        debug_print=debug_print
    )

def _parse_domain_canon_tree_args(min_prevalence_percent, min_occurrences, prevalence_map_field, domain_col=1, port_col=2, protocol_col=3, prevalent_key_field=1, debug_print=False):
    return (transformation_args.column_arg(domain_col),
            transformation_args.column_arg(port_col),
            transformation_args.column_arg(protocol_col),
            transformation_args.positive_int(min_prevalence_percent),
            transformation_args.positive_int(min_occurrences),
            prevalence_map_field,
            transformation_args.column_arg(prevalent_key_field),
            transformation_args.bool_arg(debug_print))

class _DomainPathBuilder(tree_canonization.SimplePathBuilder):
    def __init__(self, domain_col, port_col, protocol_col):
        super().__init__((domain_col, port_col, protocol_col))

    def _path_decompose(self, domain, port, protocol):
        return [str(protocol), str(port)] + domain.lower().split(".")[::-1]

    def _path_reconstruct(self, node):
        node_path = node.path
        if len(node_path) <= 1:
            return None

        protocol = node_path[1].name
        port = node_path[2].name if len(node_path) > 2 else '0'
        address = ".".join((_n.name for _n in node_path[3:][::-1])) if len(node_path) > 3 else '<domain>'

        return (protocol, port, address)

@aggregation.make_transformation("domain-canon-tree", _parse_domain_canon_tree_args)
def domain_canon_tree(aggregator, domain_column, port_column, protocol_column, min_percent, min_occurrences, prevalence_map_field, prevalent_key_field, debug_print):
    '''
    span the domain canon tree.
    '''
    prevalence_map = getattr(aggregator, prevalence_map_field)
    canonizer_factory = lambda key: tree_canonization.get_relative_prevalence_count_canonizer(0 if prevalence_map is None else prevalence_map.get(key[prevalent_key_field], None), min_percent, min_occurrences)

    tree_canonization.canonizing_transformation(
        aggregator,
        canonizer_factory,
        _DomainPathBuilder(domain_column, port_column, protocol_column),
        trap_branch_name='[NESTEDSUBDOMAIN]',
        trap_leaf_name='[SUBDOMAIN]',
        debug_print=debug_print
    )

