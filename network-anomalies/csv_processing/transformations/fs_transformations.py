#!/usr/bin/env python3

import logging
import pathlib
import recordclass
import sys
import pprint
import json
import re
import anytree
from collections import namedtuple, defaultdict
import re

from .. import aggregation
from . import transformation_args
from ..canonization import autogen_name_classifier, tree_canonization, autogen_normalization, detect_random, fs_digests
from ..utilities import anytree_helper

def _debug_print_tree_dump(root):
    root_tree_node = anytree.Node("root")
    queue = [(root, root_tree_node)]
    while len(queue) > 0:
        node, tree_node = queue.pop(0)
        if node.subtree is not None:
            queue += [
                (
                    subnode,
                    anytree.Node(subnode.name,
                                 parent=tree_node,
                                 prevalence=subnode.prevalence,
                                 local_prevalence=subnode.local_prevalence,
                                 trap=subnode.children_trap,
                                 absolute=subnode.absolute_prevalence,
                                 is_file=subnode.is_file),
                )
                for subnode in node.subtree.values()
            ]

    print(anytree.RenderTree(root_tree_node), file=sys.stderr)

def _parse_shorthand_notation_args(column, is_key = False):
    return transformation_args.column_arg(column), transformation_args.bool_arg(is_key)

_shorthand_translation = {
    'read'   : 'R',
    'write'  : 'W',
    'open'   : 'O',
    'create' : 'C',
    'copy'   : 'P',
    'rename' : 'N',
    'delete' : 'D',
}

@aggregation.make_transformation("fs-shorthand-notation", _parse_shorthand_notation_args)
def shorthand_notation(aggregator, column=-1, is_key=False):
    '''
    convert an element either in the key or the value to shorthand notation
    '''
    aggregates = aggregator.aggregates

    if is_key:
        keys = list(aggregates.keys())
        for key in keys:
            if column < 0:
                column += len(key)

            col = key[column]
            _key = key[:column] + (_shorthand_translation.get(col.lower(), col),) + key[column+1:]
            aggregates[_key] = aggregates.pop(key)
    else:
        for key, value_map in aggregates.items():
            values = list(value_map.keys())

            col = column
            for value in values:
                if col < 0:
                    col += len(value)

                _col = value[col]
                _value = value[:col] + (_shorthand_translation.get(_col.lower(), _col),) + value[col+1:]
                value_map[_value] = value_map.pop(value)

@aggregation.make_transformation("fs-canonize-column", transformation_args.column_arg)
def canonize_column(aggregator, column=-1):
    '''
    perform FS canonization on a column.
    '''
    for key, value_map in aggregator.aggregates.items():
        col = column
        values = list(value_map.keys())
        for value in values:
            if col < 0:
                col += len(value)

            path = value[col]
            canon, path_match, normalized_path = _fs_canonizer.canonize(path)
            _value = value[:col] + (canon + normalized_path[len(path_match):],) + value[col+1:]

            value_map.setdefault(_value, 0)
            value_map[_value] += value_map.pop(value)

def _parse_coalesce_columns_args(*columns):
    if len(columns) == 0:
        return None

    normalize_col = lambda col: col-1 if col > 0 else col
    return (tuple((normalize_col(int(col)) for col in columns)),)

@aggregation.make_transformation("coalesce-columns", _parse_coalesce_columns_args)
def coalesce_columns(aggregator, columns=None):
    '''
    take a list of columns and coalesce them with a Windows path seperator.
    '''

    aggregates = aggregator.aggregates
    keys = tuple(aggregates.keys())

    for key in keys:
        value_map = aggregates[key]
        new_value_map = defaultdict(lambda : 0)

        columns_list_cache = None
        first_col = None
        num_cols_in_value = None
        for value, prevalence in value_map.items():
            if columns_list_cache is None:
                num_cols_in_value = len(value)
                if columns is None:
                    columns_list_cache = tuple(range(len(value)))
                    first_col = 0
                else:
                    columns_list_cache = tuple(sorted((col if col > 0 else col + len(value) for col in columns)))
                    first_col = min(columns_list_cache)

            coalesced_col = pathlib.ntpath.join(*(value[col] for col in columns_list_cache))
            new_value = tuple((value[col] for col in range(num_cols_in_value) if col not in columns_list_cache))
            new_value = new_value[:first_col] + (coalesced_col,) + new_value[first_col:]
            new_value_map[new_value] += prevalence

        aggregates[key] = new_value_map

@aggregation.make_transformation("expand-fs-path", transformation_args.column_arg)
def expand_fs_path(aggregator, column=-1):
    r'''
    transform a value entry of the form:
        ('a\b\c', 'bla', 12) : 1337
    into:
        ('a', 'bla', 12) : 1337,
        ('a\b', 'bla', 12) : 1337,
        ('a\b\c', 'bla', 12) : 1337
    '''

    aggregates = aggregator.aggregates
    keys = list(aggregates.keys())
    for key in keys:
        value_map = aggregates.pop(key)
        new_value_map = defaultdict(lambda : 0)

        col = None
        for value, prevalence in value_map.items():
            if col is None:
                col = column if column >= 0 else column + len(value)

            path = pathlib.PureWindowsPath(value[col])
            while path.name != '':
                new_value_map[value[:col] + (str(path),) + value[col + 1:]] += prevalence
                path = path.parent

        aggregates[key] = new_value_map

@aggregation.make_transformation("file-name", transformation_args.column_arg)
def file_name(aggregator, column=-1):
    r'''
    convert: a\b\c -> c
    '''

    aggregates = aggregator.aggregates
    keys = list(aggregates.keys())
    for key in keys:
        value_map = aggregates.pop(key)
        new_value_map = defaultdict(lambda : 0)

        col = None
        for value, prevalence in value_map.items():
            if col is None:
                col = column if column >= 0 else column + len(value)

            path = pathlib.PureWindowsPath(value[col])
            new_value_map[value[:col] + (path.name,) + value[col+1:]] += prevalence

        aggregates[key] = new_value_map

def _parse_canonize_autogen_names_args(column=-1,
                                       token='[AUTOGEN]',
                                       min_variations=10,
                                       min_prevalence_for_inspection=10,
                                       max_prevalence_for_suspicion=3,
                                       debug_prints=False):
    return (
        transformation_args.column_arg(column),
        token,
        int(min_variations),
        int(min_prevalence_for_inspection),
        int(max_prevalence_for_suspicion),
        transformation_args.bool_arg(debug_prints),
    )

_ExecutionDistributionEntry = recordclass.recordclass('_ExecutionDistributionEntry',
    (
        'original',
        'expanded',
    )
)
_DirectoryEntry = recordclass.recordclass('_DirectoryEntry',
    (
        'name',
        'subtree',
        'prevalence',
        'groups',
    )
)
def _merge_directory_entries(src, dst):
    merge_queue = [(src, dst)]
    while len(merge_queue) > 0:
        src, dst = merge_queue.pop(0)
        dst.prevalence += src.prevalence

        for sub_entry_name, sub_entry_record in src.subtree.items():
            if sub_entry_name not in dst.subtree:
                dst.subtree[sub_entry_name] = sub_entry_record
            elif sub_entry_record.subtree is None or len(sub_entry_record.subtree) == 0:
                dst.subtree[sub_entry_name].prevalence += sub_entry_record.prevalence
            else:
                merge_queue.append((sub_entry_record, dst.subtree[sub_entry_name]))

def _debug_print_directory_tree(root, where=sys.stderr):
    root_tree_node = anytree.Node("root")
    queue = [(root, root_tree_node)]
    while len(queue) > 0:
        node, tree_node = queue.pop(0)
        if node.subtree is not None:
            queue += [
                (
                    subnode,
                    anytree.Node(subnode.name,
                                 parent=tree_node,
                                 prevalence=subnode.prevalence,
                                 groups=subnode.groups),
                )
                for subnode in node.subtree.values()
            ]

    print(anytree.RenderTree(root_tree_node), file=where)

def _do_canonize_autogen_names(value_map, column, normalizer, min_variations, min_prevalence_for_inspection, max_prevalence_for_suspicion, debug_prints):
    # build execution distribution with expansion
    execution_distribution = defaultdict(lambda : _ExecutionDistributionEntry(set(), set()))
    for value in value_map:
        execution_id = value[:column] + value[column+1:]
        execution = execution_distribution[execution_id]
        execution.original.add(value[column])

        path = pathlib.PureWindowsPath(value[column])
        while path.name != '':
            execution.expanded.add(str(path))
            path = path.parent

    if debug_prints:
        logging.error("expanded file access")
        json.dump({
            "_".join(map(str, execution_id)) : {
                'original' : sorted(list(execution.original)),
                'expanded' : sorted(list(execution.expanded)),
            }
            for execution_id, execution in execution_distribution.items()
        }, sys.stderr, indent=4)
        print(file=sys.stderr)

    # build directory tree
    root = _DirectoryEntry('', {}, 0, None)
    for execution in execution_distribution.values():
        for sub_path in execution.expanded:
            node = root
            for part in pathlib.PureWindowsPath(sub_path).parts:
                if part not in node.subtree:
                    node.subtree[part] = _DirectoryEntry(part, {}, 0, None)
                node = node.subtree[part]
            node.prevalence += 1

    if debug_prints:
        logging.error("non-canonized tree")
        _debug_print_directory_tree(root)

    # canonize all levels
    translation_map_index = 1 # for profiling purposes
    dir_queue = [root]
    while len(dir_queue) > 0:
        directory = dir_queue.pop(0)

        if len(directory.subtree) == 0:
            continue

        dir_list = {
            dir_name: dir_record.prevalence
            for dir_name, dir_record in directory.subtree.items()
        }
        if debug_prints:
            logging.error("building translation table for: %s", ',\n        '.join((f"'{dir_name}'" for dir_name in dir_list)))

        logging.info("building translation map #%d", translation_map_index)
        translation_table = normalizer.buildTranslationMap(dir_list,
                                                           min_variations,
                                                           min_prevalence_for_inspection,
                                                           max_prevalence_for_suspicion,
                                                           True)

        logging.info("translation map #%d built", translation_map_index)

        if debug_prints:
            logging.error("translation table:")
            autogen_name_classifier.print_translation_table(translation_table)

        logging.info("collapsing similar branches, iteration #%d", translation_map_index)
        new_subtree = {}
        for new_entry, groups in translation_table.items():
            if len(groups) == 0:
                continue
            _new_entry = _DirectoryEntry(new_entry, {}, 0, groups)
            new_subtree[new_entry] = _new_entry
            for group in groups:
                _merge_directory_entries(directory.subtree[group], _new_entry)

        logging.info("done collapsing similar branches, iteration #%d", translation_map_index)
        translation_map_index += 1

        directory.subtree = new_subtree
        dir_queue += list(directory.subtree.values())

    if debug_prints:
        logging.error("canonized tree")
        _debug_print_directory_tree(root)

    # reconstruct the original file set
    logging.info("reconstructing original file set")
    new_value_files = defaultdict(lambda : 0)
    for execution_id, execution in execution_distribution.items():
        values_updated = set()
        for path in execution.original:
            node = root
            out_path = pathlib.PureWindowsPath()
            successful = False
            for entry_name in pathlib.PureWindowsPath(path).parts:
                successful = False
                for sub_node_name, sub_node in node.subtree.items():
                    if entry_name in sub_node.groups:
                        successful = True
                        out_path /= sub_node_name
                        node = sub_node
                        break

                if not successful:
                    break

            if successful:
                value = execution_id[:column] + (str(out_path),) + execution_id[column:] # note that we don't need column+1
                if value not in values_updated:
                    new_value_files[value] += 1
                    values_updated.add(value)

    logging.info("done reconstructing original file set")

    if debug_prints:
        logging.error("final files list")
        for value, prevalence in new_value_files.items():
            print(value[column], "->", prevalence, file=sys.stderr)

    return new_value_files

@aggregation.make_transformation("canonize-autogen-names", _parse_canonize_autogen_names_args)
def canonize_autogen_names(aggregator, column, token, min_variations, min_prevalence_for_inspection, max_prevalence_for_suspicion, debug_prints):
    r'''
    replace auto-generated names in paths with a token.
    this will work properly only for host prevalence and if the host id is attached to the paths column.
    also, this booleanizes the entries so every file/host tuple will have a prevalence of 1.
    '''

    normalizer = autogen_name_classifier.AutoGeneratedNameNormalizer(token)

    aggregates = aggregator.aggregates
    keys = list(aggregates.keys())
    for key in keys:
        value_map = aggregates[key]
        if len(value_map) == 0:
            continue

        col = column + (0 if column >= 0 else len(next(iter(value_map))))
        new_value_map = _do_canonize_autogen_names(value_map,
                                                   col,
                                                   normalizer,
                                                   min_variations,
                                                   min_prevalence_for_inspection,
                                                   max_prevalence_for_suspicion,
                                                   debug_prints)
        aggregates[key] = new_value_map

def _parse_statistics_autogen_args(column, model_file_name=str(pathlib.Path(__file__).parent/'default-fs-english-model'), threshold=0.03, min_word_len=0):
    detect_random.init(model_file_name)
    return float(threshold), transformation_args.column_arg(column), int(min_word_len)

characters = set('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
parser_re = re.compile(r"(\[[^\]]+\]|[0-9a-z]+|[^\[0-9a-z]+)", re.I)
sub_part_parser_re = re.compile(r"[0-9a-z]+|[^0-9a-z]+", re.I)
placeholder_token_head = f"[{fs_digests.FileSystemCanonizer.PlaceholderToken}:"

@aggregation.make_transformation("statistics-autogen", _parse_statistics_autogen_args)
def statistics_autogen(aggregator, threshold, column, min_word_len):
    aggregates = aggregator.aggregates
    keys = list(aggregates.keys())

    for key in keys:
        value_map = aggregates[key]
        new_value_map = defaultdict(lambda : 0)
        for value, prevalence in value_map.items():
            path = value[column]
            parts = parser_re.findall(path)
            new_parts = []
            for part in parts:
                if len(part) > min_word_len and part[0] in characters and not detect_random.check(part, threshold):
                    part = '[AUTOGEN]'
                elif not part.startswith(placeholder_token_head) and part.startswith("["):
                    _part = []
                    for subpart in sub_part_parser_re.findall(part):
                        if len(subpart) > min_word_len and subpart[0] in characters and not detect_random.check(subpart, threshold):
                            subpart = '[AUTOGEN]'

                        _part.append(subpart)

                    part = _part

                if isinstance(part, list):
                    new_parts += part
                else:
                    new_parts.append(part)

            new_value = value[:column] + (''.join(new_parts), ) + value[column+1:]
            new_value_map[new_value] += prevalence

        aggregates[key] = new_value_map


##################### Autogen Detection V2

def _parse_canonize_autogen_names_v2_args(column, max_repetitions=3, min_variations=10, path_type='windows'):
    parser = {
        'windows' : (pathlib.PureWindowsPath, '\\'),
        'posix' : (pathlib.PurePosixPath, '/'),
    }[path_type.lower()]
    return transformation_args.column_arg(column), int(max_repetitions), int(min_variations), parser[0], parser[1]

_name_split_re = re.compile(r"([0-9a-z]+|[^0-9a-z]+)", re.I)
_word_characters = set('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
def _generate_fs_path_parts(path, path_parser_class, separator):
    path_parts = path_parser_class(path).parts
    if len(path_parts) == 0:
        return tuple()

    path_iterable = [(path_parts[0].strip(separator), False, None)] # keep the root directory (it is not autogenable)
    for part in path_parts[1:]:
        part = part.strip(separator)
        path_iterable.append((separator, False, {'tag' : 'separator'}))

        directory_decomposition = _name_split_re.findall(part)
        is_first_separator = directory_decomposition[0][0] not in _word_characters
        is_last_separator = directory_decomposition[-1][0] not in _word_characters
        separators_sequence = tuple((directory_decomposition[sep_index] for sep_index in range(0 if is_first_separator else 1, len(directory_decomposition), 2)))

        path_iterable.append(((is_first_separator, is_last_separator) + separators_sequence, False, {'tag' : 'sep_sequence'}))

        sep_index_oddity = 0 if is_first_separator else 1
        path_iterable += [(directory_decomposition[index], index % 2 != sep_index_oddity, None) for index in range(0, len(directory_decomposition))]

    return path_iterable

def _reconstruct_new_path(node, autogen_marker, column):
    prevalence = node.prevalence
    if len(prevalence) == 0:
        return []

    path = ''.join((
        autogen_marker if _n.autogen else _n.name
        for _n in node.path[1:]
        if getattr(_n, 'tag', None) != 'sep_sequence'
    )) # skip the root

    yield from (_p[:column] + (path,) + _p[column:] for _p in node.prevalence)

def _do_canonize_autogen_names_v2(value_map, column, max_repetitions, min_variations, path_parser, separator):
    logging.info("building path prevalence map")
    path_prevalence_map = defaultdict(lambda : set())
    for value in value_map:
        path_prevalence_map[value[column]].add(value[:column] + value[column + 1:])

    logging.info("building autogen tree")
    autogen_root = autogen_normalization.build_autogen_tree(
        ((_generate_fs_path_parts(path, path_parser, separator), prevalence) for path, prevalence in path_prevalence_map.items()),
        min_variations,
        lambda node: len(node.prevalence) <= max_repetitions
    )

    logging.info("reconstructing value map")
    new_value_map = defaultdict(lambda : 0)
    def handle_leaf(leaf):
        for new_value in _reconstruct_new_path(leaf, '[AUTOGEN]', column):
            new_value_map[new_value] += 1

    anytree_helper.traverse_leaves(autogen_root, handle_leaf)

    logging.info("done with autogen canonization")
    return new_value_map

@aggregation.make_transformation("canonize-autogen-names-v2", _parse_canonize_autogen_names_v2_args)
def _canonize_autogen_names_v2(aggregator, column, max_repetitions, min_variations, path_parser, separator):
    '''
    DEPRECATED.
    try and detect auto-generated words in the paths list and substitute them for a stand-in token.
    this method looks at word distribution and it is *very* slow.
    '''
    aggregates = aggregator.aggregates
    keys = list(aggregates.keys())
    for key in keys:
        value_map = aggregates[key]
        logging.info("canonizing %r", key)
        aggregates[key] = _do_canonize_autogen_names_v2(value_map, column, max_repetitions, min_variations, path_parser, separator)

############################# Unified Canon Tree Spanning Transformation

def _parse_file_system_canon_tree_args(min_prevalence_percent, min_occurrences, prevalence_map_field, file_path_col=1, prevalent_key_field=1, debug_print=False):
    return (transformation_args.column_arg(file_path_col),
            transformation_args.positive_int(min_prevalence_percent),
            transformation_args.positive_int(min_occurrences),
            prevalence_map_field,
            transformation_args.column_arg(prevalent_key_field),
            transformation_args.bool_arg(debug_print))

@aggregation.make_transformation("file-system-canon-tree", _parse_file_system_canon_tree_args)
def file_system_canon_tree(aggregator, file_path_column, min_percent, min_occurrences, prevalence_map_field, prevalent_key_field, debug_print):
    '''
    span the file system canon tree.
    '''
    prevalence_map = getattr(aggregator, prevalence_map_field)
    canonizer_factory = lambda key: tree_canonization.get_relative_prevalence_count_canonizer(0 if prevalence_map is None else prevalence_map.get(key[prevalent_key_field], None), min_percent, min_occurrences)

    tree_canonization.canonizing_transformation(
        aggregator,
        canonizer_factory,
        tree_canonization.WindowsPathLikePathBuilder(file_path_column),
        debug_print=debug_print
    )

