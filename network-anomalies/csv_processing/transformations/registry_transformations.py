#!/usr/bin/env python3

import logging
import recordclass
import sys
import pprint
import json
import anytree
from collections import namedtuple, defaultdict

from .. import aggregation
from . import transformation_args
from ..canonization import autogen_name_classifier, tree_canonization

################################### Unified Canon Tree Spanning

def _parse_registry_canon_tree_args(min_prevalence_percent, min_occurrences, prevalence_map_field, registry_path_field=1, prevalent_key_field=1,debug_print=False):
    return (transformation_args.column_arg(registry_path_field),
            transformation_args.positive_int(min_prevalence_percent),
            transformation_args.positive_int(min_occurrences),
            prevalence_map_field,
            transformation_args.column_arg(prevalent_key_field),
            transformation_args.bool_arg(debug_print))


@aggregation.make_transformation("registry-canon-tree", _parse_registry_canon_tree_args)
def registry_cannon_tree(aggregator, registry_path_field, min_percent, min_occurrences, prevalence_map_field, prevalent_key_field, debug_print):
    '''
    span the canon tree for registry paths.
    '''
    prevalence_map = getattr(aggregator, prevalence_map_field)
    canonizer_factory = lambda key: tree_canonization.get_relative_prevalence_count_canonizer(0 if prevalence_map is None else prevalence_map.get(key[prevalent_key_field], None), min_percent, min_occurrences)

    tree_canonization.canonizing_transformation(
        aggregator,
        canonizer_factory,
        tree_canonization.WindowsPathLikePathBuilder(registry_path_field),
        trap_branch_name='[SUBKEY]',
        trap_leaf_name='[VALUE]',
        debug_print=debug_print
    )

