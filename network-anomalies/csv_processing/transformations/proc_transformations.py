#!/usr/bin/env python3

import logging
import pathlib
import recordclass
import sys
import re
import anytree
from collections import namedtuple, defaultdict
import re

from .. import aggregation
from . import transformation_args
from ..canonization import autogen_name_classifier, tree_canonization, autogen_normalization, detect_random, fs_digests
from ..utilities import anytree_helper

def _parse_canonize_proc_chains_args(proc_chain_column, min_prevalence_percent, min_occurrences, prevalence_column=-1, separator="|"):
    return transformation_args.column_arg(proc_chain_column), int(min_prevalence_percent), int(min_occurrences), transformation_args.column_arg(prevalence_column), separator

class TreeCsvChainPathBuilder(tree_canonization.SimplePathBuilder):
    def __init__(self, separator, column):
        super().__init__((column,))
        self.separator = separator

    def _path_decompose(self, path):
        return path.split(self.separator)

    def _path_reconstruct(self, node):
        return self.separator.join((_node.name for _node in node.path[1:]))

@aggregation.make_transformation("canonize-proc-chains", _parse_canonize_proc_chains_args)
def canonize_proc_chains(aggregator, proc_chain_column, min_percent, min_occurrences, prevalence_column, separator):
    '''
    take the output from process_creation_tree.py and build the process creation canon tree.
    '''
    canonizer_factory = lambda key: (None if (key[prevalence_column] is None or key[prevalence_column] == '') else tree_canonization.RelativePrevalenceCountCanonization(int(key[prevalence_column]), min_percent, min_occurrences))
    tree_canonization.canonizing_transformation(
        aggregator,
        canonizer_factory,
        TreeCsvChainPathBuilder(separator, proc_chain_column),
        trap_branch_name='[PARENT]',
        trap_leaf_name='[PARENT]',
        debug_print=False
    )

##########################

def _parse_proc_chains_canon_tree_args(min_prevalence_percent, min_occurrences, prevalence_map_field, proc_chain_column=1, prevalent_key_field=1, separator="|"):
    return (transformation_args.column_arg(proc_chain_column),
            transformation_args.positive_int(min_prevalence_percent),
            transformation_args.positive_int(min_occurrences),
            prevalence_map_field,
            transformation_args.column_arg(prevalent_key_field),
            separator)


@aggregation.make_transformation("proc-chains-canon-tree", _parse_proc_chains_canon_tree_args)
def canonize_proc_chains(aggregator, proc_chain_column, min_percent, min_occurrences, prevalence_map_field, prevalent_key_field, separator):
    '''
    take the output from process_creation_tree.py and build the process creation canon tree.
    '''
    prevalence_map = getattr(aggregator, prevalence_map_field)
    canonizer_factory = lambda key: tree_canonization.get_relative_prevalence_count_canonizer(0 if prevalence_map is None else prevalence_map.get(key[prevalent_key_field], None), min_percent, min_occurrences)

    tree_canonization.canonizing_transformation(
        aggregator,
        canonizer_factory,
        TreeCsvChainPathBuilder(separator, proc_chain_column),
        trap_branch_name='[PARENT]',
        trap_leaf_name='[PARENT]',
        debug_print=False
    )

