#!/usr/bin/env python3

import ipaddress
import re
import logging
import sys
import pprint
import datetime
import pathlib
import csv
import recordclass
from collections import Iterable
from collections import defaultdict

from .. import aggregation
from . import transformation_args
from . import fs_transformations
from . import net_transformations
from . import registry_transformations
from . import proc_transformations

@aggregation.make_transformation("stop-parallelization", parallelizable=False)
def stop_parallization(aggregator):
    '''
    internal. a marker used to force stop parallelization of transformations and collect the results. don't use this.
    '''
    pass

@aggregation.make_transformation("intermediate-print", parallelizable=False)
def percentages_transformation(aggregator):
    '''
    print the aggregation.
    '''
    pprint.pprint(aggregator.aggregates, stream=sys.stderr)

@aggregation.make_transformation("fail")
def fail(aggregator):
    raise Exception("initiated a failing transformation")

@aggregation.make_transformation("percentages")
def percentages_transformation(aggregator):
    '''
    convert aggregate numbers to percentages.
    '''
    for key, values_record in aggregator.aggregates.items():
        _sum = sum(values_record.values())
        values = list(values_record.keys())
        for value in values:
            values_record[value] /= _sum # in python 3 the result is a float, which is what we want

def _to_set(obj):
    return set(obj) if isinstance(obj, Iterable) and not any((isinstance(obj, _iter) for _iter in (str, bytes, dict))) else {obj}

@aggregation.make_transformation("collect-least-key", transformation_args.column_arg)
def collect_least_value(aggregator, column=-1):
    '''
    transform values in the following form: (a,b,c)+(a,b,d) ->(a,b,c+d). the prevalence is summed.
    '''
    keys = list(aggregator.aggregates.keys())
    for key in keys:
        value_map = aggregator.aggregates[key]

        collected_values = {}
        col = column
        for value, prevalence in value_map.items():
            if col < 0:
                col = len(value) + col
            super_key = (value[:col], value[col+1:])
            if super_key not in collected_values:
                collected_values[super_key] = {
                    'values' : _to_set(value[col]), # a set of the last entries in the tuples
                    'prevalence' : prevalence,
                }
            else:
                collected = collected_values[super_key]
                collected['values'].update(_to_set(value[col]))
                collected['prevalence'] += prevalence

        aggregator.aggregates[key] = {
            super_key[0] + (tuple(collected['values']),) + super_key[1] : collected['prevalence']
            for super_key, collected in collected_values.items()
        }


@aggregation.make_transformation("prettify-collected-key", transformation_args.column_arg)
def prettify_collected_key(aggregator, column=-1):
    '''
    convert a tuple yielded by collect_least_value in the last element of a value to a better formatted string.
    '''
    for key, value_map in aggregator.aggregates.items():
        values = list(value_map.keys())
        col = column
        for value in values:
            if col < 0:
                col = len(value) + col
            _value = value[:col] + ("[" + ",".join(value[col]) + "]",) + value[col+1:]
            value_map[_value] = value_map.pop(value)

def _parse_drop_sub_key_args(column):
    column = int(column)
    return column - 1 if column > 0 else column

@aggregation.make_transformation("drop-sub-key", _parse_drop_sub_key_args)
def drop_sub_key(aggregator, column=-1):
    '''
    drop a column from the values tuple and coalesce the resultant keys.
    '''
    for key, value_map in aggregator.aggregates.items():
        values = list(value_map.keys())
        col = column
        for value in values:
            if col < 0:
                col += len(value)
            prevalence = value_map.pop(value)
            _value = value[:col] + value[col+1:]

            value_map.setdefault(_value, 0)
            value_map[_value] += prevalence

@aggregation.make_transformation("low-percentages", lambda percent: [float(percent)/100])
def low_percents(aggregator, percent = 0.1):
    '''
    drop values with prevelance above a certain percent (depends on 'percentages').
    '''
    # pre-requisites
    assert 'percentages' in aggregator.tags

    aggregates = aggregator.aggregates
    keys = list(aggregates.keys())
    for key in keys:
        values_record = aggregates[key]
        values = list(values_record.keys())
        for value in values:
            if values_record.get(value, 0) > percent:
                values_record.pop(value)

        if len(values_record) == 0:
            aggregates.pop(key)

@aggregation.make_transformation("boolean-entries")
def boolean_entries_transformation(aggregator):
    '''
    Sets the ref-count for every value in the hash-map to 1. This allows you to count prevalence.
    '''

    assert 'percentages' not in aggregator.tags

    aggregates = aggregator.aggregates
    for key, value_map in aggregates.items():
        for value, value_prevalence in value_map.items():
            value_map[value] -= value_prevalence - 1

def _parse_sum_by_secondary_key_args(column=1, min_sum=0):
    column = int(column)
    assert column != 0
    return column - 1 if column > 0 else column, int(min_sum)

@aggregation.make_transformation("sum-by-secondary-key", _parse_sum_by_secondary_key_args)
def sum_by_secondary_key(aggregator, column, min_sum):
    '''
    If the values in the mapping are tuples (x, y) then this will sum all the values
    containing the same x as their first element.
    '''

    assert 'percentages' not in aggregator.tags

    aggregates = aggregator.aggregates
    keys = list(aggregates.keys())
    for key in keys:
        value_map = aggregates[key]
        new_map = {}
        for value, value_prevalence in value_map.items():
            value = (value[column],) # make sure the values are still tuples
            new_map.setdefault(value, 0)
            new_map[value] += value_prevalence

        if min_sum > 0:
            # create the iterator via list comprehension to create a list so when we pop items the list 
            # will remain intact (if we were to just iterate over new_map.items() then the list would dynamically
            # change and the iteration would stop with a "dict modified" exception)
            for value in [value for value, prevalence in new_map.items() if prevalence < min_sum]:
                new_map.pop(value)

        aggregates[key] = new_map

@aggregation.make_transformation("min-occurrences", transformation_args.number_arg)
def sum_by_secondary_key(aggregator, min_occurrences = 10):
    '''
    Drop every value with too low a ref count.
    '''

    aggregates = aggregator.aggregates
    keys = list(aggregates.keys())
    for key in keys:
        value_map = aggregates[key]
        values = list(value_map.keys())
        for value in values:
            if value_map[value] < min_occurrences:
                value_map.pop(value)

        if len(aggregates[key]) == 0:
            aggregates.pop(key)

def _parse_total_prevalence_args(dict_format="true"):
    return transformation_args.bool_arg(dict_format)

@aggregation.make_transformation("total-prevalence", _parse_total_prevalence_args)
def total_prevalence(aggregator, dict_format):
    aggregates = aggregator.aggregates
    keys = list(aggregates.keys())
    for key in keys:
        value_map = aggregates[key]
        total_prev = sum(value_map.values())
        aggregates[key] = {('prevalence',) : total_prev} if dict_format else total_prev

def _parse_drop_by_prevalence_args(prevalence_bar, *low_or_high):
    prevalence_bar = int(prevalence_bar)

    assert len(low_or_high) <= 1

    is_low = True
    if len(low_or_high) == 1:
        comparison = low_or_high[0].lower()
        assert comparison in ('low', 'high')
        is_low = ('low' == comparison)

    return [prevalence_bar, is_low]

@aggregation.make_transformation("drop-by-prevalence", _parse_drop_by_prevalence_args)
def drop_by_prevalence(aggregator, prevalence_bar=5, is_low=True):
    assert "total-prevalence" in aggregator.tags

    aggregates = aggregator.aggregates
    keys = list(aggregates.keys())
    for key in keys:
         if (is_low and aggregates[key] < prevalence_bar) or (not is_low and aggregates[key] >= prevalence_bar):
            aggregates.pop(key)

@aggregation.make_transformation("drop-by-total-prevalence", _parse_drop_by_prevalence_args)
def drop_by_prevalence(aggregator, prevalence_bar=5, is_low=True):
    assert "total-prevalence" not in aggregator.tags

    aggregates = aggregator.aggregates
    keys = list(aggregates.keys())
    for key in keys:
        prevalence = sum(aggregates[key].values(), 0)
        if (is_low and prevalence < prevalence_bar) or (not is_low and prevalence >= prevalence_bar):
            aggregates.pop(key)

def _parse_col_as_prevalence_args(column=-1):
    column = int(column)
    assert 0 != column
    return column - 1 if column > 0 else column

@aggregation.make_transformation("col-as-prevalence", _parse_col_as_prevalence_args)
def col_as_prevalence(aggregator, column):
    '''
    drop a column from the value tuples and assign it's value to value prevalence.
    '''

    for key, value_map in aggregator.aggregates.items():
        values = list(value_map.keys())

        col = column
        for value in values:
            if col < 0:
                col += len(value)

            _value = value[:col] + value[col+1:]
            value_map.setdefault(_value, 0)
            value_map[_value] += value_map.pop(value)*value[col]

def _parse_keys_with_dominance_args(min_percent):
    return int(min_percent)/100

@aggregation.make_transformation("keys-with-dominance", _parse_keys_with_dominance_args)
def keys_with_dominance(aggregator, min_percent=0.1):
    '''
    drop keys that don't have a value with a minimal percent prevalence (values spread too widely for grouping).
    '''

    assert 'percentages' in aggregator.tags

    aggregates = aggregator.aggregates
    keys = list(aggregates.keys())
    for key in keys:
        value_map = aggregates[key]
        if all((prevalence < min_percent for prevalence in value_map.values())):
            aggregates.pop(key)

def _parse_add_column_from_csv_args(new_col_values_csv, column=1, is_key="true"):
    with open(new_col_values_csv, "rt") as extra_col_csv:
        new_col_values = dict(csv.reader(extra_col_csv, delimiter=',', quotechar='"'))

    return new_col_values, transformation_args.column_arg(column), transformation_args.bool_arg(is_key)

@aggregation.make_transformation("csv-to-column", _parse_add_column_from_csv_args)
def add_column_from_csv(aggregator, new_col_values, column, is_key):
    '''
    DEPRECATED. add a column to either a key or a value in the aggregator
    '''

    aggregates = aggregator.aggregates
    if is_key:
        new_aggregates = {}
        for key, value_map in aggregates.items():
            new_aggregates[key + (new_col_values.get(key[column], ''),)] = value_map

        aggregator.aggregates = new_aggregates
    else:
        for value_map in aggregates.values():
            values = value_map.keys()
            for value in values:
                value_map[value + (new_col_values.get(value[column], ''),)] = value_map.pop(value)

def _parse_load_csv_map_args(csv_map_file_name, value_type, context_field):
    if csv_map_file_name is None or csv_map_file_name.strip() == '':
        return None, context_field

    type_func = {
        'int' : int,
        'str' : str,
    }[value_type]

    with open(csv_map_file_name, "rt") as csv_map_file:
        # infer whether the layout is tab separation or comman separation
        first_line = csv_map_file.readline()
        csv_map_file.seek(0)
        tab_separation = '\t' in first_line

        loaded_map = {
            key : type_func(value)
            for key, value in csv.reader(
                csv_map_file,
                delimiter='\t' if tab_separation else ',',
                quotechar='"',
                quoting=csv.QUOTE_NONE if tab_separation else csv.QUOTE_MINIMAL
            )
        }

    return loaded_map, context_field

@aggregation.make_transformation("load-csv-map", _parse_load_csv_map_args)
def load_csv_map(aggregator, loaded_map, context_field):
    '''
    load a map from a CSV file and store it in the aggregation context under a given name.
    '''
    setattr(aggregator, context_field, loaded_map)

def _parse_coalesce_value_columns_args(separator=':', *columns):
    assert len(columns) > 1, ValueError('no columns to coalesce')
    return (separator, ) + tuple((transformation_args.column_arg(col) for col in columns))

@aggregation.make_transformation("coalesce-value-columns", _parse_coalesce_value_columns_args)
def coalesce_columns(aggregator, separator, *columns):
    '''
    unite value columns into a single string column using a separator.
    e.g:
        (1, 2, 'hello', 3, 'world') -> coalesce(cols=1..4, separator=':') -> ('1:2:hello:3', 'world')
    '''
    aggregates = aggregator.aggregates
    keys = list(aggregates.keys())
    for key in keys:
        value_map = aggregates[key]
        new_value_map = {}
        for value, prevalence in value_map.items():
            new_value = (separator.join((str(value[col]) for col in columns)), )
            other_columns = tuple((value[col] for col in range(len(value)) if col not in columns))
            if len(other_columns) > 0:
                new_value += other_columns
            new_value_map[new_value] = prevalence

        aggregates[key] = new_value_map

def _parse_enumerate_col_arguments(column, enum_name):
    return transformation_args.column_arg(column), enum_name

_ColEnumeration = recordclass.recordclass('_ColEnumeration', ('column', 'enumeration_table'))
@aggregation.make_transformation("enumerate-col", _parse_enumerate_col_arguments)
def enumerate_column(aggregator, column, enum_name):
    if not hasattr(aggregator, 'enumerations'):
        aggregator.enumerations = {}

    assert enum_name not in aggregator.enumerations

    def _index_generator():
        x = 1
        while True:
            yield x
            x += 1

    index_generator = _index_generator()

    aggregates = aggregator.aggregates
    keys = list(aggregates.keys())
    col_hash_map = defaultdict(lambda : next(index_generator))

    for key in keys:
        value_map = aggregates[key]

        new_value_map = {}
        for value, prevalence in value_map.items():
            new_value = value[:column] + (col_hash_map[value[column]],) + value[column+1:]
            new_value_map[new_value] = prevalence

        aggregates[key] = new_value_map

    aggregator.enumerations[enum_name] = _ColEnumeration(column, dict(col_hash_map))

@aggregation.make_transformation("denumerate-col")
def denumerate_col(aggregator, enum_name=None):
    if enum_name is None:
        col_enumeration = aggregator.enumerations.popitem()
    else:
        col_enumeration = aggregator.enumerations.pop(enum_name)

    column = col_enumeration.column
    enumeration_table = {v : k for k,v in col_enumeration.enumeration_table.items()}

    aggregates = aggregator.aggregates
    keys = list(aggregates.keys())

    for key in keys:
        value_map = aggregates[key]

        restored_value_map = {}
        for value, prevalence in value_map.items():
            restored_value = value[:column] + (enumeration_table[value[column]],) + value[column+1:]
            restored_value_map[restored_value] = prevalence

        aggregates[key] = restored_value_map

@aggregation.make_transformation("filter-by-empty-col", transformation_args.column_arg)
def filter_by_empty_col(aggregator, column):
    aggregates = aggregator.aggregates
    keys = list(aggregates.keys())
    for key in keys:
        aggregates[key] = {
            value : prevalence for value, prevalence in aggregates[key].items() if value[column] not in ('', None)
        }

def _parse_filter_by_col_args(col, inclusive, *values):
    assert len(values) > 0
    return transformation_args.column_arg(col), transformation_args.bool_arg(inclusive), set(values)

@aggregation.make_transformation("filter-key-by-col", _parse_filter_by_col_args)
def filter_key_by_col(aggregator, column, inclusive, values):
    aggregates = aggregator.aggregates
    for key in [key for key in aggregates.keys() if (inclusive and str(key[column]) not in values) or (not inclusive and str(key[column]) in values)]:
        aggregates.pop(key)

def _parse_count_by_values_args(col_1=1, *columns):
    columns = [transformation_args.column_arg(col_1)] + [transformation_args.column_arg(col) for col in columns]
    return sorted(columns)

@aggregation.make_transformation("count-by-values", _parse_count_by_values_args)
def count_by_values(aggregator, *columns):
    '''
    an extension of count-by-secondary-key.
    extract a subset of given columns from the aggregation values to use for keys and sum their prevalences.
    '''

    aggregates = aggregator.aggregates
    keys = list(aggregates.keys())

    for key in keys:
        value_map = aggregates.pop(key)
        new_map = defaultdict(int)
        for value, prevalence in value_map.items():
            new_map[tuple((value[c] for c in columns))] += prevalence

        aggregates[key] = new_map

