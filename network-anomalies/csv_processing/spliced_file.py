#!/usr/bin/env python3

import io
import os
import csv
import sys
import argparse
import pprint

class FileSplice:
    def __init__(self, _file, offset, size):
        _file.seek(offset, io.SEEK_SET)
        self.file = _file
        self.size = size

    def read(self, n=None):
        if self.size <= 0:
            return ''

        if n is None or n < 0:
            buf = self.file.read(self.size) # this might end up as excessive reading but it's fine for our purposes, as this function is never called
            self.size = 0
            return buf

        before_pos = self.file.tell()

        n = min(self.size, n)
        buf = self.file.read(n)

        self.size -= self.file.tell() - before_pos

        return buf

    def readline(self):
        if self.size <= 0:
            return ''

        before_pos = self.file.tell()
        line = self.file.readline(self.size)

        self.size -= self.file.tell() - before_pos
        return line

    def __iter__(self):
        return self

    def __next__(self):
        line = self.readline()
        if line == '':
            raise StopIteration()
        return line

    def close(self):
        self.file.close()

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        self.close()

    @property
    def name(self):
        return self.file.name

def generate_splices(filename, splice_count):
    # generate file slices
    with open(filename, "rt") as big_file:
        big_file.seek(0, io.SEEK_END)
        fsize = big_file.tell()
        big_file.seek(0)

        splice_size = (fsize + splice_count - 1)//splice_count
        splices = []

        offset = 0
        while offset < fsize:
            start = offset
            base_offset = offset + splice_size
            big_file.seek(base_offset, io.SEEK_SET)

            read_line_again = True
            while read_line_again:
                try:
                    big_file.readline()
                    read_line_again = False

                except UnicodeDecodeError:
                    base_offset += 1
                    big_file.seek(base_offset, io.SEEK_SET)
                    read_line_again = True

            offset = big_file.tell()

            if start == offset:
                break

            splices.append((start, offset))

    return splices

def get_splice(filename, start, end):
    return FileSplice(open(filename, "rt"), start, end - start)

def splice_file(filename, splice_count):
    for start, end in generate_splices(filename, splice_count):
        yield get_splice(filename, start, end)

def _test_offsets(filename, splice_count):
    print(f"splicing {filename} to {splice_count}")
    offsets = generate_splices(filename, splice_count)
    slice_index = 1
    for start, end in offsets:
        print(f"slice #{slice_index}", start, "->", end)
        slice_index += 1

def _test_csv(filename, splice_count):
    print(f"splicing CSV {filename} to {splice_count}")
    slice_index = 1
    for _slice in splice_file(filename, splice_count):
        with _slice as _slice:
            print(f"\nslice #{slice_index}")
            for line in csv.reader(_slice):
                print(*line)
            print()

        slice_index += 1

def test():
    parser = argparse.ArgumentParser(description='test file splicing.')
    subparsers = parser.add_subparsers(dest='action')

    num_subparser = subparsers.add_parser('offsets')
    num_subparser.add_argument("-f", "--file", required=True, help="the file to splice.")
    num_subparser.add_argument("-n", "--num-splices", type=int, default=os.cpu_count(), help='number of splices.')

    csv_subparser = subparsers.add_parser('csv')
    csv_subparser.add_argument("-f", "--file", required=True, help="the CSV file to splice.")
    csv_subparser.add_argument("-n", "--num-splices", type=int, default=os.cpu_count(), help='number of splices.')

    args = parser.parse_args()

    if args.action == 'offsets':
        _test_offsets(args.file, args.num_splices)
    elif args.action == 'csv':
        _test_csv(args.file, args.num_splices)

if __name__ == "__main__":
    test()

