#!/usr/bin/env python3

import ipaddress
import re
import logging
import sys
import pprint
import datetime
import pathlib
from collections import Iterable

from . import aggregation

def _csv_formatter(aggregator, sep, quote):
    aggregates = aggregator.aggregates

    for key, value_map in aggregates.items():
        if isinstance(value_map, dict):
            for value, prevalence in value_map.items():
                csv_entry = tuple()
                if isinstance(key, Iterable) and not isinstance(key, bytes) and not isinstance(key, str):
                    csv_entry += tuple(key)
                else:
                    csv_entry += (key,)

                if isinstance(value, Iterable) and not isinstance(value, bytes) and not isinstance(value, str):
                    csv_entry += tuple(value)
                else:
                    csv_entry += (value,)

                csv_entry += (prevalence,)

                yield sep.join((f'{quote}{element}{quote}' for element in csv_entry))

        else:
            csv_entry = tuple()
            if isinstance(key, Iterable) and not isinstance(key, bytes) and not isinstance(key, str):
                csv_entry += tuple(key)
            else:
                csv_entry += (key,)

            csv_entry += (value_map,)
            yield sep.join((f'{quote}{element}{quote}' for element in csv_entry))

@aggregation.make_formatter("csv")
def csv_formatter(aggregator):
    yield from _csv_formatter(aggregator, ",", '"')

@aggregation.make_formatter("csv-tab")
def csv_tab_formatter(aggregator):
    yield from _csv_formatter(aggregator, "\t", '')

@aggregation.make_formatter("dict-print")
def dict_print_formatter(aggregator):
    '''
    format the output as a python dictionary printed via pprint.
    '''
    yield pprint.pformat(aggregator.aggregates)

