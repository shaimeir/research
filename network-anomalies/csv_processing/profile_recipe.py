#!/usr/bin/env python3
'''
define a profile format that we'll be able to use to automatically fill in the required command line options
for a specific task (file system profile generation, registry cross validation, etc...).
this module utilizes the utilities.argparse_recipes module to interact with other scripts.

the recipe can be extended with a subrecipe which is a recipe (with no subrecipes of it's own)
that specifies additional columns and transformations to be executed after the main recipe section is done.

the recipe also holds some weak data that will allow us to automate cross validation.
the data is a tag on each by-element/element that tells what type of content (in regard to cross-validation) it holds (key, tag and value).
the recipe supports mock columns for cross-validation. columns that don't have a cross-validation tag will be ignored on cross validation.
this allows us to work through the assumption that the transformation won't change the number of columns by allowing us to
omit columns that aren't tagged for cross-validation and add mock columns that are only of use for cross validation.
these columns are identified by an 'id' tag and lack of 'name' tag and must contain a 'cross_validation' tag.

NOTE:
currently there's no filter support because we don't acutally use that feature

NOTE:
the current recipe representation doesn't allow for multiple uses of the same transformation or aggregation element
(walked straight into that one with the ordered dictionary sadly...)
'''

import inspect
import enum
import sys
import pathlib
import pprint
import argparse
import json
import copy
import contextlib
from collections import OrderedDict, Iterable
from recordclass import recordclass

if __name__ == "__main__":
    sys.path.append(str(pathlib.Path(__file__).absolute().resolve().parent.parent)) # network anomalies directory
    from csv_processing import aggregation, scheme
    from csv_processing.elements import aggregation_elements
    from csv_processing.transformations import transformations
    from canonization import canon_paths_application
else:
    from . import aggregation, scheme
    from .elements import aggregation_elements
    from .transformations import transformations
    from .canonization import canon_paths_application

class ArgumentType(enum.Enum):
    const = 'const'
    optional = 'optional'
    required = 'required'
    # like optional, but of lesser priority
    code_default = 'code_default'

# an enumeration of tags attached to aggregation elements (actual or mock) that describe their role in the cross-validation process.
class CrossValidationTag(enum.Enum):
    # keys identify the profile to cross-validate against
    key = 'key'
    # tags are additional columns that hold a meaning for the cross-validation output
    # but should not be considered for picking the profile or the value to compare aginst it
    # (will usually be host id, secdo process id and instance id)
    tag = 'tag'
    # the value to compare against the profile
    value = 'value'
    # the column is irrelevant for cross validation
    none = None

# an enumeration of tags attached to aggregation elements (actual or mock) that describe their role in the output profile.
# we use these tags to infer the
class ProfileTag(enum.Enum):
    # keys that identify the profile
    key = 'key'
    # a value (canon path) of the profile
    value = 'value'
    # this column has no role in the profile
    none = None

# holds information obtained externally (probably via a command line) in the form of dictionaries:
#   RecipeItems.transformations = {
#       'transformation-name' : {
#           'arg1' : 'value1',
#           'arg2' : 'value2',
#           ...
#       }
#   }
RecipeItems = recordclass('RecipeItems', ('by_elements', 'elements', 'transformations'))

# describes an argument passed to a transformation/element/by-element
RecipeItemArgDesc = recordclass('RecipeItemArgDesc', ('name',     # the name of the argument - the actual argument name for the argument parsing function for the transformation/element
                                                      'type',     # the type of the argument in the eyes of the recipe - const (can't be changed), optional (has a default but can be overridden) or required (must be supplied externally)
                                                      'value',    # the default value for optional args and the constant value for const args (None for required)
                                                      'aliases')) # aliases given to this argument to enable the conformization of the recipe for automated processes that hand out well known arguments (such as generate-standard-profiles-v2.sh)

# describes an alias which can be used externally to refer to a specific argument in a specific element/by-element/transformation
RecipeArgAliasDesc = recordclass('RecipeArgAliasDesc', ('alias',     # the alias itself
                                                        'type',      # transformation/by-element/element
                                                        'element',   # the element the argument belongs tp
                                                        'argument')) # the name of the argument

# a descriptor that holds the information for an element/by-element/transformation instance
RecipeItemEntry = recordclass('RecipeItemEntry', ('name',             # actual transformation or element name
                                                  'args',             # an ordered dictionary of the transformation/element's arguments
                                                  'variadic',         # the name of the variadic argument for argument parsing function
                                                  'cross_validation', # a cross validation tag (if present). for transformations this field is None.
                                                  'profile_tag')) # a profile tag (if present). for transformations this field is None.

def _repr_helper(obj):
    '''
    a helper function because i'm tired of the way pprint formats it's output.
    '''
    if isinstance(obj, str) or obj is None:
        return obj

    elif isinstance(obj, enum.Enum):
        return obj.value

    elif isinstance(obj, (RecipeItems, RecipeItemArgDesc, RecipeArgAliasDesc, RecipeItemEntry)):
        return {
            k : _repr_helper(v)
            for k, v in obj.__dict__.items()
        }

    elif isinstance(obj, dict):
        return {
            k : _repr_helper(v)
            for k, v in obj.items()
        }

    elif isinstance(obj, (list, tuple)):
        return [
            _repr_helper(v)
            for v in obj
        ]

    elif hasattr(obj, 'represent'):
        return obj.represent()

    else:
        return str(obj)

class ProfileRecipe:
    @staticmethod
    def __validate_subrecipe_name__(subrecipe_name):
        assert '.' not in subrecipe_name
        return subrecipe_name

    def __init__(self, recipe):
        self._real_by_elements = None
        self._cross_validation_by_elements = None
        self._profile_by_elements = None
        self._real_elements = None
        self._cross_validation_elements = None
        self._profile_elements = None

        # a representation related utility variable - no functional relevance
        self.show_sub_recipes = False

        assert 'scheme' in recipe, ValueError('recipe is missing a scheme')
        self.scheme_name = recipe['scheme']
        self.scheme = scheme.load_default_scheme_file(recipe['scheme'])

        # get the profile type and validate it
        self.profile_type = recipe.get('profile_type', None)
        if self.profile_type is not None:
            assert self.profile_type in canon_paths_application.get_supported_profile_types(), ValueError('invalid profile type', self.profile_type, canon_paths_application.get_supported_profile_types())

        # get the profile tags
        self.profile_extra_tags = recipe.get('profile_extra_tags', [])
        if isinstance(self.profile_extra_tags, Iterable) and not isinstance(self.profile_extra_tags, (str, bytes)):
            self.profile_extra_tags = list(self.profile_extra_tags)
        else:
            self.profile_extra_tags = [self.profile_extra_tags]

        # currently, a profile is just a set of aggregation factors and a bunch of transformations
        # so that's what we'll load over here
        self._by_elements = self._load_aggregation_by_elements(recipe.get('by_elements', []),
                                                               recipe.get('by_elements_cross_validation', None),
                                                               recipe.get('by_elements_profile_tag', ProfileTag.none))

        self._elements = self._load_aggregation_elements(recipe.get('elements', []),
                                                         recipe.get('elements_cross_validation', None),
                                                         recipe.get('elements_profile_tag', ProfileTag.none))

        self._transformations = self._load_transformations(recipe.get('transformations', []))

        # create the aliases map that ties an alias to it's argument
        self._aliases = {}
        self._create_aliases_map()

        self.description = recipe.get('description', '<no description>')

        # now we're going to load sub-recipes which are actionable extensions of the data loaded so far.
        # basically it's a dict that it's values are similar to the recipe object passed to this constructor
        # but without a scheme.

        subrecipes = recipe.get('subrecipes', {})
        assert "main" not in subrecipes.keys(), ValueError("'main' is a reserved sub-recipe name")

        # first we're going to resolve sub-recipe references.
        # sub-recipe references are a way of having one sub-recipe extend the content of another sub-recipe (it
        # will appear as a sub-recipe of a sub-recipe).
        # this feature is added for one reason - allow the file-system recipe to have a specific modify-only
        # sub-recipe without duplicating the entire content.
        subrecipe_depenedency_chain = {}
        for subrecipe_name, subrecipe in subrecipes.items():
            if 'subrecipe_ref' in subrecipe:
                assert 'subrecipes' not in subrecipe, ValueError('recipe with sub-recipe ref can\'t have a subrecipes clause', subrecipe_name)
                assert isinstance(subrecipe['subrecipe_ref'], (list, str)), ValueError('invalid sub-recipe ref type in recipe', subrecipe_name)

                # make sure the subrecipe ref is in list format
                if isinstance(subrecipe['subrecipe_ref'], str):
                    subrecipe['subrecipe_ref'] = [subrecipe['subrecipe_ref']]
                elif len(subrecipe['subrecipe_ref']) == 0:
                    # just an empty clause
                    continue

                subrecipe_depenedency_chain[subrecipe_name] = subrecipe['subrecipe_ref']

        while len(subrecipe_depenedency_chain) > 0:
            # there are sub-recipe references that need to be resolved.
            # we need to resolve them in the correct order and make sure there are no cyclic dependencies

            # start with some reference
            referrer, reference = subrecipe_depenedency_chain.popitem()
            assert referrer not in reference, ValueError('self-reference in sub-recipe reference', referrer, reference)

            # build the dependency chain until an independent sub-recipe is met
            subchain = [referrer]
            while len(reference) > 0:
                for ref in reference:
                    assert ref not in subchain, ValueError('cyclic dependency in sub-recipe references', subchain, ref)
                    assert ref in subrecipes, ValueError('invalid sub-recipe reference to non-existing sub-recipe', ref)

                # subchain is a reference chain to which we aggregate the references of the current stage.
                # reference holds the list of recipes we need to resolve references to.
                # these will be added to subchain and then resolved for references themselves.
                # the output of this loop is a list (subchain) that when traversed from back to start will
                # yield recipes that aren't dependent of anything, then recipes that depend on those recipes
                # and then recipes that depend on those and so on and so forth.

                subchain += reference
                reference = sum((subrecipe_depenedency_chain.pop(ref, []) for ref in reference), [])

            # start resolving the references
            last_subrecipe_name = subchain.pop(-1)
            while len(subchain) > 0:
                # remove references that have no sub-recipe references themselves
                while len(subchain) > 0 and len(subrecipes[subchain[-1]].get('subrecipe_ref', [])) == 0:
                    subchain.pop(-1)

                if len(subchain) == 0:
                    break

                next_subrecipe_name = subchain.pop(-1)
                next_subrecipe = subrecipes[next_subrecipe_name]
                subrecipe_refs = next_subrecipe.pop('subrecipe_ref')

                next_subrecipe['subrecipes'] = {
                    ref : copy.deepcopy(subrecipes[ref])
                    for ref in subrecipe_refs
                }

        # now everything is nice and pretty and we can just represent the sub-recipes as recipes internally
        self.subrecipes = {
            self.__validate_subrecipe_name__(subrecipe_name) : ProfileRecipe(self.__fix_subrecipe_dict(subrecipe))
            for subrecipe_name, subrecipe in subrecipes.items()
        }

        # reset the aliases table
        self._create_aliases_map()

        for subrecipe in self.subrecipes.values():
            subrecipe.__augment(self)

    @property
    def by_elements(self):
        if self._real_by_elements is not None:
            return self._real_by_elements

        self._real_by_elements = OrderedDict()
        for by_element_id, by_element_desc in self._by_elements.items():
            if by_element_desc.name is not None:
                self._real_by_elements[by_element_id] = by_element_desc

        return self._real_by_elements

    @property
    def cross_validation_by_elements(self):
        if self._cross_validation_by_elements is not None:
            return self._cross_validation_by_elements

        self._cross_validation_by_elements = OrderedDict()
        for by_element_id, by_element_desc in self._by_elements.items():
            if by_element_desc.cross_validation != CrossValidationTag.none:
                self._cross_validation_by_elements[by_element_id] = by_element_desc.cross_validation

        return self._cross_validation_by_elements

    @property
    def profile_by_elements(self):
        if self._profile_by_elements is not None:
            return self._profile_by_elements

        self._profile_by_elements = OrderedDict()
        for by_element_id, by_element_desc in self._by_elements.items():
            if by_element_desc.profile_tag is not None and by_element_desc.profile_tag != ProfileTag.none:
                self._profile_by_elements[by_element_id] = by_element_desc.profile_tag

        return self._profile_by_elements

    @property
    def elements(self):
        if self._real_elements is not None:
            return self._real_elements

        self._real_elements = OrderedDict()
        for element_id, element_desc in self._elements.items():
            if element_desc.name is not None:
                self._real_elements[element_id] = element_desc

        return self._real_elements

    @property
    def cross_validation_elements(self):
        if self._cross_validation_elements is not None:
            return self._cross_validation_elements

        self._cross_validation_elements = OrderedDict()
        for element_id, element_desc in self._elements.items():
            if element_desc.cross_validation != CrossValidationTag.none:
                self._cross_validation_elements[element_id] = element_desc.cross_validation

        return self._cross_validation_elements

    @property
    def profile_elements(self):
        if self._profile_elements is not None:
            return self._profile_elements

        self._profile_elements = OrderedDict()
        for element_id, element_desc in self._elements.items():
            if element_desc.profile_tag is not None and element_desc.profile_tag != ProfileTag.none:
                self._profile_elements[element_id] = element_desc.profile_tag

        return self._profile_elements

    @property
    def transformations(self):
        return self._transformations

    def alias(self, alias_name):
        return self._aliases.get(alias_name, None)

    def __fix_subrecipe_dict(self, subrecipe):
        assert 'scheme' not in subrecipe
        subrecipe['scheme'] = self.scheme_name

        # this assignment makes sure that there's no conflict between a parent's profile type and a child's profile type.
        # if the parent has no profile type then the child may do as it pleases.
        if 'profile_type' in subrecipe and self.profile_type is not None:
            assert subrecipe['profile_type'] == self.profile_type, ValueError('sub-recipe profile type conflicts with parent recipe\'s', subrecipe['profile_type'], self.profile_type)
        else:
            subrecipe.setdefault('profile_type', self.profile_type)

        if 'description' in subrecipe:
            subrecipe['description'] = self.description + "\n" + subrecipe['description']
        else:
            subrecipe['description'] = self.description

        return subrecipe

    def get_subrecipes(self):
        return list(self.subrecipes.keys())

    def get_subrecipe(self, subrecipe_name):
        if subrecipe_name == 'main':
            return self

        subrecipe = self
        for _subrecipe_name in subrecipe_name.split("."):
            if 'main' == _subrecipe_name:
                continue
            subrecipe = subrecipe.subrecipes[_subrecipe_name]

        return subrecipe

    def validate(self):
        assert len(self._by_elements) > 0, ValueError('recipe has no aggregation-by elements')
        assert len(self._elements) > 0, ValueError('recipe has no aggregation elements')

    def augment_arguments(self, arguments):
        '''
        gets a partially filled RecipeItems with it's elements set to dictionaries of following the form:
            RecipeItems.elements = {
                'element-id-1' : {
                    'arg-1' : 'value-1',
                    'arg-2' : 'value-2'
                },
                'element-id-2' : {
                    'arg-3' : 'value-3',
                    'arg-4' : 'value-4'
                }
            }

            RecipeItems.by_elements = {
                'element-id-1' : {
                    'arg-1' : 'value-1',
                    'arg-2' : 'value-2'
                },
                'element-id-2' : {
                    'arg-3' : 'value-3',
                    'arg-4' : 'value-4'
                }
            }

            RecipeItems.transformations = {
                'transformation-id-1' : {
                    'arg-1' : 'value-1',
                    'arg-2' : 'value-2'
                },
                'transformation-id-2' : {
                    'arg-3' : 'value-3',
                    'arg-4' : 'value-4'
                }
            }

        the method asserts that all the arguments are valid (to a point) and augments missing entries
        with constants and defaults from the recipe.
        the method also places the content in an OrderedDict in the correct order the elements and transformations
        are to be accessed at.

        the output of the function will be an augmented RecipeItems of the following format:
            RecipeItems.elements = {
                'element-id-1' : RecipeItemEntry('element-name-1', OrderedDict({
                    'arg-1' : 'value-1',
                    'arg-2' : 'value-2'
                }), [], profile_cross_validaiton_tag, profile_tag),
                'element-id-2' : RecipeItemEntry('element-name-2', OrderedDict({
                    'arg-3' : 'value-3',
                    'arg-4' : 'value-4'
                }), ['vararg-1', 'vararg-2'], profile_cross_validaiton_tag, profile_tag)
            }

            RecipeItems.by_elements = {
                'element-id-1' : RecipeItemEntry('element-name-1', OrderedDict({
                    'arg-1' : 'value-1',
                    'arg-2' : 'value-2'
                }), ['vararg-3', 'vararg-4'], profile_cross_validaiton_tag),
                'element-name-2' : RecipeItemEntry('element-name-2', OrderedDict({
                    'arg-3' : 'value-3',
                    'arg-4' : 'value-4'
                }), ['vararg-5', 'vararg-6'], profile_cross_validaiton_tag, profile_tag)
            }

            RecipeItems.transformations = {
                'transformation-id-1' : RecipeItemEntry('element-name-1', OrderedDict({
                    'arg-1' : 'value-1',
                    'arg-2' : 'value-2'
                }), ['vararg-7', 'vararg-8'], profile_cross_validaiton_tag, profile_tag),
                'transformation-id-2' : RecipeItemEntry('element-name-2', OrderedDict({
                    'arg-3' : 'value-3',
                    'arg-4' : 'value-4'
                }), [], profile_cross_validaiton_tag, profile_tag)
            }

        note that there is an important distinction between element id and element name here.
        the keys are always the ids to allow for repetitions of the same element/transformation
        (which are identified in the code by their *name*).
        '''
        assert isinstance(arguments, RecipeItems)

        new_arguments = RecipeItems(OrderedDict(), OrderedDict(), OrderedDict())

        # make sure all the by-elements have proper values
        by_elements = list(self._by_elements.keys())
        for arg_by_element in arguments.by_elements:
            assert arg_by_element in by_elements, ValueError('by-element not specified in recipe', arg_by_element, by_elements)

        # make sure all the elements have proper values
        elements = list(self._elements.keys())
        for arg_element in arguments.elements:
            assert arg_element in elements, ValueError('element not specified in recipe', arg_element, elements)

        # make sure all the transformations have proper values
        transformations = list(self._transformations.keys())
        for arg_transformation in arguments.transformations:
            assert arg_transformation in transformations, ValueError('transformation not specified in recipe', arg_transformation, transformations, arguments)

        # fill new_arguments with the full information about by-element arguments.
        # note that we'd like to skip mock elements so we'll use self.by_elements which is a property and not a field.
        for recipe_by_element_id, recipe_by_element in self.by_elements.items():
            arg_by_element = arguments.by_elements.get(recipe_by_element_id, {})

            merged_by_element_args = RecipeItemEntry(recipe_by_element.name,
                                                     OrderedDict(),
                                                     [],
                                                     recipe_by_element.cross_validation,
                                                     recipe_by_element.profile_tag)

            # make sure there are no stray arguments
            for argument in arg_by_element:
                if recipe_by_element.variadic is None or recipe_by_element.variadic.name != argument:
                    assert argument in recipe_by_element.args, ValueError("invalid argument passed to by-element", recipe_by_element_id, argument)

            # fill up all the arguments according to the specification in the recipe object (self)
            for recipe_by_elem_arg, recipe_by_elem_arg_desc in recipe_by_element.args.items():
                if recipe_by_elem_arg_desc.type in (ArgumentType.optional, ArgumentType.code_default):
                    merged_by_element_args.args[recipe_by_elem_arg] = arg_by_element.get(recipe_by_elem_arg, recipe_by_elem_arg_desc.value)
                elif recipe_by_elem_arg_desc.type == ArgumentType.const:
                    assert recipe_by_elem_arg not in arg_by_element, ValueError('attempt to override const argument in by-element in recipe', recipe_by_element_id, recipe_by_elem_arg)
                    merged_by_element_args.args[recipe_by_elem_arg] = recipe_by_elem_arg_desc.value
                else: # ArgumentType.required
                    assert recipe_by_elem_arg in arg_by_element, ValueError('missing required argument in by-element in recipe', recipe_by_element_id, recipe_by_elem_arg)
                    merged_by_element_args.args[recipe_by_elem_arg] = arg_by_element[recipe_by_elem_arg]

            # account for variadic arguments
            if recipe_by_element.variadic is not None:
                merged_by_element_args.variadic = arg_by_element.get(recipe_by_element.variadic.name, recipe_by_element.value)

            # store the merged by-element in the new arguments container
            new_arguments.by_elements[recipe_by_element_id] = merged_by_element_args

        # fill new_arguments with the full information about element arguments
        # note that we'd like to skip mock elements so we'll use self.elements which is a property and not a field.
        for recipe_element_id, recipe_element in self.elements.items():
            arg_element = arguments.elements.get(recipe_element_id, {})

            merged_element_args = RecipeItemEntry(recipe_element.name,
                                                  OrderedDict(),
                                                  [],
                                                  recipe_element.cross_validation,
                                                  recipe_element.profile_tag)

            # make sure there are no stray arguments
            for argument in arg_element:
                if recipe_element.variadic is None or recipe_element.variadic.name != argument:
                    assert argument in recipe_element.args, ValueError("invalid argument passed to element", recipe_element_id, argument)

            # fill up all the arguments according to the specification in the recipe object (self)
            for recipe_elem_arg, recipe_elem_arg_desc in recipe_element.args.items():
                if recipe_elem_arg_desc.type in (ArgumentType.optional, ArgumentType.code_default):
                    merged_element_args.args[recipe_elem_arg] = arg_element.get(recipe_elem_arg, recipe_elem_arg_desc.value)
                elif recipe_elem_arg_desc.type == ArgumentType.const:
                    assert recipe_elem_arg not in arg_element, ValueError('attempt to override const argument in element in recipe', recipe_element_id, recipe_elem_arg)
                    merged_element_args.args[recipe_elem_arg] = recipe_elem_arg_desc.value
                else: # ArgumentType.required
                    assert recipe_elem_arg in arg_element, ValueError('missing required argument in element in recipe', recipe_element_id, recipe_elem_arg)
                    merged_element_args.args[recipe_elem_arg] = arg_element[recipe_elem_arg]

            # account for variadic arguments
            if recipe_element.variadic is not None:
                merged_element_args.variadic = arg_element.get(recipe_element.variadic.name, recipe_element.variadic.value)

                merged_by_element_args = recipe_by_element.cross_validation

            # store the merged element in the new arguments container
            new_arguments.elements[recipe_element_id] = merged_element_args

        # fill new_arguments with the full information about transformation arguments
        for recipe_transformation_id, recipe_transformation in self._transformations.items():
            arg_transformation = arguments.transformations.get(recipe_transformation_id, {})

            merged_transformation_args = RecipeItemEntry(recipe_transformation.name,
                                                         OrderedDict(),
                                                         [],
                                                         None,
                                                         None)

            # make sure there are no stray arguments
            for argument in arg_transformation:
                if recipe_transformation.variadic is None or recipe_transformation.variadic.name != argument:
                    assert argument in recipe_transformation.args, ValueError("invalid argument passed to transformation", recipe_transformation_id, argument)

            # fill up all the arguments according to the specification in the recipe object (self)
            for recipe_elem_arg, recipe_elem_arg_desc in recipe_transformation.args.items():
                if recipe_elem_arg_desc.type in (ArgumentType.optional, ArgumentType.code_default):
                    merged_transformation_args.args[recipe_elem_arg] = arg_transformation.get(recipe_elem_arg, recipe_elem_arg_desc.value)
                elif recipe_elem_arg_desc.type == ArgumentType.const:
                    assert recipe_elem_arg not in arg_transformation, ValueError('attempt to override const argument in transformation in recipe', recipe_transformation_id, recipe_elem_arg)
                    merged_transformation_args.args[recipe_elem_arg] = recipe_elem_arg_desc.value
                else: # ArgumentType.required
                    assert recipe_elem_arg in arg_transformation, ValueError('missing required argument in transformation in recipe', recipe_transformation_id, recipe_elem_arg)
                    merged_transformation_args.args[recipe_elem_arg] = arg_transformation[recipe_elem_arg]


            # account for variadic arguments
            if recipe_transformation.variadic is not None:
                merged_transformation_args.variadic = arg_transformation.get(recipe_transformation.variadic.name, recipe_transformation.variadic.value)

            # store the merged transformation in the new arguments container
            new_arguments.transformations[recipe_transformation_id] = merged_transformation_args

        return new_arguments

    def __augment(self, reference_recipe):
        '''
        takes a reference recipe and uses it's elements and transformations before the current object's elements and transformations.
        you can also modify arguments (all of them or any part) passed to the reference.
        the reference recipe should remain unchanged.

        e.g.
            ref elements: ref_elem1, ref_elem2, ref_elem3
            self elements: self_elem1, self_elem2

            output self elements: ref_elem1, ref_elem2, ref_elem3, self_elem1, self_elem2

        also:
            arguments are inherited from the reference. any argument set both in the reference and in the current
            recipe (i.e. type != required) are taken from the current recipe.
            this allows us to mimick "inheritance" by overriding ancestors arguments.

        IMPORTANT:
            there's an important nuance to *when* this method is called!
            it is meant to be called in the constructor after the creation of all sub-recipes.
            what will happen is that all the sub-recipes of the sub-recipes (of any depth) will be built
            from the lowest depth first, each of them in-turn augmented by it's parent.
            when a parent is augmented, it's children are re-augmented (as you can see in the finishing clause of this method)
            via this method. this causes a chain effect where sub-recipe of level n is augmented n times.
            this method is aware of that and the implementation takes care to first copy the ancestors content and then augment
            it with the current recipe's specific data and this allows us the consistency of the recipe parsing.
        '''
        assert self.scheme_name == reference_recipe.scheme_name, ValueError('augmentation error: mismatch in scheme name', self.scheme_name, reference_recipe.scheme_name)
        assert self.scheme == reference_recipe.scheme, ValueError('augmentation error: mismatch in scheme', self.scheme, reference_recipe.scheme)

        # merge profile extra tags
        self.profile_extra_tags = reference_recipe.profile_extra_tags + [tag for tag in self.profile_extra_tags if tag not in reference_recipe.profile_extra_tags]

        # merge reference recipe with current recipe.
        # we first need to make a deep copy of the reference recipe so when we override defaults the original won't change.

        # by-elements merge

        # copy the reference
        by_elements = OrderedDict()
        for by_elem_id, by_elem_desc in reference_recipe._by_elements.items():
            by_elements[by_elem_id] = copy.deepcopy(by_elem_desc)

        # merge with the current recipe
        for by_elem_id, by_elem_desc in self._by_elements.items():
            if by_elem_id not in by_elements:
                by_elements[by_elem_id] = by_elem_desc
                continue

            # make sure it's the same actual element
            ref_element = by_elements[by_elem_id]
            # note that this assertion includes the case where the element is a mock element (an element with no name field set used for cross validation)
            assert by_elem_desc.name == ref_elem.name, ValueError('subrecipe id has type collision', by_elem_id, by_elem_desc.name, ref_elem.name)

            # merge the arguments
            for ref_argument_name, ref_argument_desc in ref_element.args.items():
                arg_desc = by_elem_desc.args[ref_argument_name]

                # don't take the arguments from self if it's of type required (and hence the parent may supply a default/const)
                # or if it's a code-default (in which case the parent must be a code-default of the same value or a supplied optional/const).
                if arg_desc.type not in (ArgumentType.required, ArgumentType.code_default):
                    ref_argument_desc.type = arg_desc.type
                    ref_argument_desc.value = arg_desc.value

                if arg_desc.aliases is not None:
                    ref_argument_desc.aliases = arg_desc.aliases if ref_argument_desc.aliases is None else list(set(ref_argument_desc.aliases + arg_desc.aliases))

            # merge the variadic argument.
            # take the current variadic if it's anything other than a code-default (because then
            # the parent takes precednece and anyway, by default the parent will be code-default).
            if by_elem_desc.variadic is not None and by_elem_desc.variadic.type != ArgumentType.code_default:
                ref_element.variadic = by_elem_desc.variadic

            # merge the cross validation tag
            if ref_element.cross_validation is None: # if the reference has no cross-validation tag then take the new cross validation tag
                ref_element.cross_validation = by_elem_desc.cross_validation
            elif by_elem_desc.cross_validation is not None: # if both the reference and the new descriptors have cross validation tags make sure they're the same
                assert ref_element.cross_validation == by_elem_desc.cross_validation, ValueError('cross-validation tag mismatch in sub-recipe by-element',
                                                                                                 by_elem_id,
                                                                                                 ref_element.cross_validation,
                                                                                                 by_elem_desc.cross_validation)
            # merge the profile tag
            if ref_element.profile_tag is None: # if the reference has no profile tag then take the new profile tag
                ref_element.profile_tag = by_elem_desc.profile_tag
            elif by_elem_desc.profile_tag is not None:
                assert ref_element.profile_tag == by_elem_desc.profile_tag, ValueError('profile tag mismatch in sub-recipe by-element',
                                                                                        by_elem_id,
                                                                                        ref_element.profile_tag,
                                                                                        by_elem_desc.profile_tag)

        self._by_elements = by_elements

        # elements merge

        # copy the reference
        elements = OrderedDict()
        for elem_id, elem_desc in reference_recipe._elements.items():
            elements[elem_id] = copy.deepcopy(elem_desc)

        # merge with the current recipe
        for elem_id, elem_desc in self._elements.items():
            if elem_id not in elements:
                elements[elem_id] = elem_desc
                continue

            # make sure it's the same actual element
            ref_element = elements[elem_id]
            assert elem_desc.name == ref_element.name, ValueError('subrecipe id has type collision', elem_id, elem_desc.name, ref_element.name)

            # merge the arguments
            for ref_argument_name, ref_argument_desc in ref_element.args.items():
                arg_desc = elem_desc.args[ref_argument_name]

                # don't take the arguments from self if it's of type required (and hence the parent may supply a default/const)
                # or if it's a code-default (in which case the parent must be a code-default of the same value or a supplied optional/const).
                if arg_desc.type not in (ArgumentType.required, ArgumentType.code_default):
                    ref_argument_desc.type = arg_desc.type
                    ref_argument_desc.value = arg_desc.value

                if arg_desc.aliases is not None:
                    ref_argument_desc.aliases = arg_desc.aliases if ref_argument_desc.aliases is None else list(set(ref_argument_desc.aliases + arg_desc.aliases))

            # merge the variadic argument
            # take the current variadic if it's anything other than a code-default (because then
            # the parent takes precednece and anyway, by default the parent will be code-default).
            if elem_desc.variadic is not None and elem_desc.variadic.type != ArgumentType.code_default:
                ref_element.variadic = elem_desc.variadic

            # merge the cross validation tag
            if ref_element.cross_validation is None: # if the reference has no cross-validation tag then take the new cross validation tag
                ref_element.cross_validation = elem_desc.cross_validation
            elif elem_desc.cross_validation is not None: # if both the reference and the new descriptors have cross validation tags make sure they're the same
                assert ref_element.cross_validation == elem_desc.cross_validation, ValueError('cross-validation tag mismatch in sub-recipe element',
                                                                                              elem_id,
                                                                                              ref_element.cross_validation,
                                                                                              elem_desc.cross_validation)

            # merge the profile tag
            if ref_element.profile_tag is None: # if the reference has no profile tag then take the new profile tag
                ref_element.profile_tag = elem_desc.profile_tag
            elif elem_desc.profile_tag is not None:
                assert ref_element.profile_tag == elem_desc.profile_tag, ValueError('profile tag mismatch in sub-recipe by-element',
                                                                                     elem_id,
                                                                                     ref_element.profile_tag,
                                                                                     elem_desc.profile_tag)

        self._elements = elements

        # transformations merge

        # copy the reference
        transformations = OrderedDict()
        for transformation_id, transformation_desc in reference_recipe._transformations.items():
            transformations[transformation_id] = copy.deepcopy(transformation_desc)

        # merge with the current recipe
        for transformation_id, transformation_desc in self._transformations.items():
            if transformation_id not in transformations:
                transformations[transformation_id] = transformation_desc
                continue

            # make sure it's the same actual transformation
            ref_transformation = transformations[transformation_id]
            assert transformation_desc.name == ref_transformation.name, ValueError('subrecipe id has type collision', transformation_id, transformation_desc.name, ref_transformation.name)

            # merge the arguments
            for ref_argument_name, ref_argument_desc in ref_transformation.args.items():
                arg_desc = transformation_desc.args[ref_argument_name]

                # don't take the arguments from self if it's of type required (and hence the parent may supply a default/const)
                # or if it's a code-default (in which case the parent must be a code-default of the same value or a supplied optional/const).
                if arg_desc.type not in (ArgumentType.required, ArgumentType.code_default):
                    ref_argument_desc.type = arg_desc.type
                    ref_argument_desc.value = arg_desc.value

                if arg_desc.aliases is not None:
                    ref_argument_desc.aliases = arg_desc.aliases if ref_argument_desc.aliases is None else list(set(ref_argument_desc.aliases + arg_desc.aliases))

            # merge the variadic argument
            # take the current variadic if it's anything other than a code-default (because then
            # the parent takes precednece and anyway, by default the parent will be code-default).
            if transformation_desc.variadic is not None and transformation_desc.variadic.type != ArgumentType.code_default:
                ref_transformation.variadic = transformation_desc.variadic

        self._transformations = transformations

        # update the aliases table
        self._create_aliases_map()

        # finally, augment sub-recipes
        for sub_recipe in self.subrecipes.values():
            sub_recipe.__augment(self)

    def __str__(self):
        return repr(self)

    def set_full_representation(self, show_sub_recipes):
        self.show_sub_recipes = show_sub_recipes

    def represent(self):
        repr_dict = {
            'description' : self.description,
            'scheme' : self.scheme_name,
            'profile_type' : self.profile_type,
            'by_elements' : self.by_elements,
            'cross_validation_by_elements' : self.cross_validation_by_elements,
            'profile_by_elements' : self.profile_by_elements,
            'elements' : self.elements,
            'cross_validation_elements' : self.cross_validation_elements,
            'profile_elements' : self.profile_elements,
            'transformations' : self.transformations,
            'extra_tags' : self.profile_extra_tags,
            'aliases' : self._aliases,
        }

        repr_dict = {
            k : _repr_helper(v)
            for k, v in repr_dict.items()
        }

        if self.show_sub_recipes and len(self.subrecipes) > 0:
            repr_dict['subrecipes'] = _repr_helper(self.subrecipes)

        return repr_dict

    def __repr__(self):
        return json.dumps(self.represent(), indent=8)

    def __aggregation_elements_arguments_lookup(self, aggregation_element_name):
        # column elements are assumed to hold precedence in resolution order and are also assumed to have no arguments
        if aggregation_element_name in self.scheme.column_translation():
            return aggregation.ArgSpec(OrderedDict(), None)

        return aggregation.aggregation.option(aggregation_element_name).getargspec()

    def _load_aggregation_by_elements(self, _aggregation_elements, default_cross_validation_tag, default_profile_tag):
        return self.__load_elements(_aggregation_elements, self.__aggregation_elements_arguments_lookup, "by_elements", True, default_cross_validation_tag, True, default_profile_tag)

    def _load_aggregation_elements(self, _aggregation_elements, default_cross_validation_tag, default_profile_tag):
        return self.__load_elements(_aggregation_elements, self.__aggregation_elements_arguments_lookup, "elements", True, default_cross_validation_tag, True, default_profile_tag)

    def __transformations_arguments_lookup(self, transformation):
        return aggregation.data_transformations.option(transformation).getargspec()

    def _load_transformations(self, transformations):
        return self.__load_elements(transformations, self.__transformations_arguments_lookup, "transformations", False, None, False, None)

    def __load_elements(self, elements, element_arguments_lookup_func, tag, allow_cross_validation, default_cross_validation_tag, allow_profile_tag, default_profile_tag):
        '''
        generically load elemnts (be them transformations or aggregation(-by) elements.
        the input is an iterable (list) of descriptors of the form:
            {
                'id' : 'unique id of the aggregation element in the recipe',
                'name' : 'aggregation element name goes here',
                'cross_validation' : 'cross-validation tag', # the role of this element in the cross-validation process (CrossValidationTag)
                'profile_tag' : 'profile tag', # the role this element plays in the profile (ProfileTag)
                'arguments' : {
                    'arg1' : {
                        'type' : 'optional, const or required'
                        'value' : 'const value' # for const type
                        'default' : 'default value' # for optional
                    },
                    'arg2' : {
                        ...
                    }
                }
            }

        the 'id' field should be unique and it's use is to allow re-use of the same element name (same aggregation
        element or transformation) multiple times in the same recipe.
        if an 'id' isn't specified then the 'name' field is used by default.

        the 'arguments' clause is optional and the default 'type' for it's entries is 'optional'.
        you can replace the entire descriptor with just the element name string and then the following is inferred:
            {
                'name' : 'element name',
                'cross_validation' : default_cross_validation_tag,
                'profile_tag' : default_profile_tag
            }

        the 'allow_cross_validation' is a boolean to indicate whether there can be a cross validation label on elements loaded by this call.
        transformation elements, for example, can't have a cross-validation tag.

        the 'allow_profile_tag' is a boolean to indicate whether there can be a profile label on elements loaded by this call.
        transformation elements, for example, can't have a profile tag tag.
        '''
        default_cross_validation_tag = CrossValidationTag(default_cross_validation_tag)
        default_profile_tag = ProfileTag(default_profile_tag)

        recipe = OrderedDict()

        for element in elements:
            element_arguments_dict = OrderedDict()

            if isinstance(element, str):
                element = {
                    'name' : element,
                    # we can't add: 'cross_validation' : default_cross_validation_tag
                    # here because we would like to assert that there is no cross_validation field in some cases.
                    # same goes for 'profile_tag'.
                }

            element_name = element.get('name', None)

            element.setdefault('id', element_name)
            element_id = element['id']
            assert element_id not in recipe, ValueError('repetition of the same element id in the recipe', element_id)

            # if the name is None then we can treat this as a mock object for cross validation.
            # if this happens, we must assert that cross validation or profile tagging is allowed and that the id is not None (that's because there are no other purposes for mock elements).
            # we need not do further processing.
            if element_name is None:
                assert element_id is not None, ValueError('cross validation mock element must have an id', tag)
                assert (allow_cross_validation and 'cross_validation' in element) or (allow_profile_tag and 'profile_tag' in element), ValueError('mock elements (elements with no name) must have a cross validation tag or a profile tag', tag, element_id)

                recipe[element_id] = RecipeItemEntry(None,
                                                     OrderedDict(),
                                                     None,
                                                     CrossValidationTag(element.get('cross_validation', default_cross_validation_tag)) if allow_cross_validation else None,
                                                     ProfileTag(element.get('profile_tag', default_profile_tag)) if allow_profile_tag else None)
                continue

            # first build the list of arguments from the aggregation factor without regard of the recipe

            _element_arguments = element_arguments_lookup_func(element_name)

            # convert the argument to default value mapping to a more suitable form with _ElementArgument
            for arg_name, arg_default in _element_arguments.args.items():
                element_arguments_dict[arg_name] = RecipeItemArgDesc(
                    # argument name
                    arg_name,
                    # if the argument has a default value then it's optional, otherwise it's required.
                    # note that we use code_default for the argument type as it's an "optional" value with
                    # a default of lesser priority than optional fields specified in a recipe.
                    ArgumentType.required if arg_default is None else ArgumentType.code_default,
                    # save the default value
                    arg_default,
                    # by default there's no available alias
                    None
                )

            # now update the variadic entry
            variadic = None if _element_arguments.variadic is None else RecipeItemArgDesc(
                # argument name
                _element_arguments.variadic,
                # the type is variadic, obviously
                ArgumentType.code_default,
                # and it has an empty default value
                [],
                # again, no alias by default
                None
            )

            # now we can start taking into account the arguments specified in the recipe, and override the entries previously
            # generated from the aggregation factor factory.

            # the data in the recipe should denote constants or defaults
            for arg_name, arg_desc in element.get('arguments', {}).items():
                # allow short hand notation.
                # better check it's not a dict rather than checking it *is* something so we can support multiple JSON types
                if not isinstance(arg_desc, dict):
                    arg_desc = {
                        'type' : ArgumentType.const,
                        'value' : arg_desc,
                    }

                arg_aliases = arg_desc.get('alias', None)
                if arg_aliases is None:
                    pass
                elif isinstance(arg_aliases, (str, bytes)) or not isinstance(arg_aliases, Iterable):
                    arg_aliases = [arg_aliases]
                else:
                    arg_aliases = list(arg_aliases)

                if variadic is not None and arg_name == variadic.name:
                    # variadic argument
                    arg_type = ArgumentType(arg_desc.get('type', ArgumentType.optional))
                    assert arg_type in (ArgumentType.const, ArgumentType.optional), ValueError('recipe can only specify element variadic arguments with a const or optional type', arg_type, tag)

                    variadic.type = arg_type
                    variadic.value = arg_desc['value']
                    if isinstance(variadic.value, str):
                        variadic.value = [variadic.value]
                    variadic.aliases = arg_aliases

                else:
                    # none variadic argument
                    assert arg_name in element_arguments_dict, ValueError('invalid argument name in recipe',
                                                                          arg_name,
                                                                          list(element_arguments_dict.keys()),
                                                                          tag)

                    arg_type = ArgumentType(arg_desc.get('type', ArgumentType.optional))

                    # if the argument is optional but has no default value and the argument has a default value specified in the code
                    # then set it in the argument descriptor as the default value and set the argument type to code_default.
                    if arg_type == ArgumentType.optional and 'default' not in arg_desc and element_arguments_dict[arg_name].type == ArgumentType.code_default:
                        arg_desc['default'] = element_arguments_dict[arg_name].value
                        arg_desc['value'] = element_arguments_dict[arg_name].value # add this since the type is going to be code_default
                        arg_type = ArgumentType.code_default

                    if arg_type == ArgumentType.optional:
                        assert 'default' in arg_desc, ValueError('no optional value given in descriptor', arg_name, arg_desc)
                    elif arg_type != ArgumentType.required:
                        assert 'value' in arg_desc, ValueError('no value given for non-required descriptor in descriptor', arg_name, arg_desc)

                    # override the default inital layout generated from arguments inspection
                    element_arguments_dict[arg_name] = RecipeItemArgDesc(
                        arg_name,
                        arg_type,
                        # if the argument is optional then the descriptor should have a 'default' key and a 'value' key if it's constant
                        arg_desc['default'] if arg_type == ArgumentType.optional else None if arg_type == ArgumentType.required else arg_desc['value'],
                        arg_aliases
                    )

            # load cross validation tag
            cross_validation_tag = None
            if allow_cross_validation:
                cross_validation_tag = CrossValidationTag(element.get('cross_validation', default_cross_validation_tag))

            else: # make sure there's no cross validation tag!
                assert 'cross_validation' not in element, ValueError('cross validation tag not allowed in element', tag, element_id)

            # load profile tag
            profile_tag = None
            if allow_profile_tag:
                profile_tag = ProfileTag(element.get('profile_tag', default_profile_tag))

            else: # make sure there's no profile tag!
                assert 'profile_tag' not in element, ValueError('profile tag not allowed in element', tag, element_id)

            # now set the element descriptor in the recipe!
            recipe[element_id] = RecipeItemEntry(
                element_name,
                element_arguments_dict,
                variadic,
                cross_validation_tag,
                profile_tag
            )

        return recipe

    def _create_aliases_map(self):
        '''
        creates a mapping from an alias to the actual element it refers to, indicated by a tuple of
        element type (transformation/by-element/element), element id (the transformation/by-element/element id) and the argument name.
        '''

        self._aliases = {}

        def _yield_arguments(elements_dict):
            for element_id, element_desc in elements_dict.items():
                if element_desc.name is None:
                    # mock entry used for cross validation (describes output), skip
                    continue

                for arg_name, arg_desc in element_desc.args.items():
                    if arg_desc.aliases is not None:
                        yield from ((alias, element_id, arg_name) for alias in arg_desc.aliases)

        for aliases, element_id, arg_name in _yield_arguments(self._by_elements):
            assert alias not in self._aliases, ValueError('reuse of alias in by-element id', alias, element_id)
            self._aliases[alias] = RecipeArgAliasDesc(alias, 'by_element', element_id, arg_name)

        for alias, element_id, arg_name in _yield_arguments(self._transformations):
            assert alias not in self._aliases, ValueError('reuse of alias in transformation id', alias, element_id)
            self._aliases[alias] = RecipeArgAliasDesc(alias, 'transformation', element_id, arg_name)

        for alias, element_id, arg_name in _yield_arguments(self._elements):
            assert alias not in self._aliases, ValueError('reuse of alias in element id', alias, element_id)
            self._aliases[alias] = RecipeArgAliasDesc(alias, 'element', element_id, arg_name)

@contextlib.contextmanager
def auto_context(obj):
    yield obj

def load_recipe_from_json(recipe_file_name=None, resolve_with_defaults=True):
    default_recipes_dir = pathlib.Path(__file__).parent/'default-recipes'

    resolution_order = [None] if recipe_file_name is None else ([
        pathlib.Path(recipe_file_name),
        default_recipes_dir/recipe_file_name,
        default_recipes_dir/f"{recipe_file_name}.json"
    ] if resolve_with_defaults else [pathlib.Path(recipe_file_name)])

    for _recipe_file_name in resolution_order:
        if _recipe_file_name is None or _recipe_file_name.is_file():
            with (auto_context(sys.stdin) if _recipe_file_name is None else open(_recipe_file_name)) as recipe_file:
                return ProfileRecipe(json.load(recipe_file))

    raise FileNotFoundError("can't resolve recipe file name", recipe_file_name)

##### testing

def register_test_elements_and_transformations():
    @aggregation.register_aggregation_factor("no-args")
    def no_args(row):
        return 1

    @aggregation.register_aggregation_factor("args-untouched")
    def args_untouched(row, no_default, with_default="what have i done?"):
        return "when will all my hopes arise"

    @aggregation.register_aggregation_factor("args-overridden")
    def args_overridden(row, first_arg="how will i know it,", second_arg="when i look in my father's eyes"):
        return "then the jagged edge appears"

    def _untouched_parser(first_arg="throught the distant", second_arg="cloud of tears"):
        return "i'm like a bridge"

    def _modified_parser(first_arg="throught the distant", second_arg="cloud of tears"):
        return "i'm like a bridge"

    @aggregation.register_aggregation_factor("untouched-parser", _untouched_parser)
    def untouched_parser(row, *args):
        return "that has washed away"

    @aggregation.register_aggregation_factor("modified-parser", _modified_parser)
    def modified_parser(row, *args):
        return "that has washed away"

    @aggregation.register_aggregation_factor("untouched-variadic")
    def untouched_variadic(row, first_arg="my foundations", second_arg="were made of clay", *variadic_args):
        return "and as my soul"

    @aggregation.register_aggregation_factor("variadic")
    def variadic(row, first_arg="slides down to die", second_arg="how could i lose him?", *variadic_args):
        return "what did i try?"

    def _variadic_parser(first_arg="bit by bit", second_arg="i realise", *variadic_args):
        return 1

    @aggregation.register_aggregation_factor("variadic-parser", _variadic_parser)
    def variadic_parser(row, num):
        return "that he was here with me"

    def _all_in_one_transformation_args_parser(first_arg, second_arg, third_arg="if i could reach the stars", fourth_arg="i'd pull one down for you", *variadic_arg):
        return "shine it on my heart"

    @aggregation.make_transformation("all-in-one-transformation", _all_in_one_transformation_args_parser)
    def all_in_one_transformation(aggregator, *args):
        return "so you could see the truth"

    @aggregation.make_transformation("no-parser-transformation")
    def no_parser_transformation(aggregator, first_arg, *variadic_arg):
        return "that this love i have inside"

    @aggregation.register_aggregation_factor("subrecipe-1-element")
    def args_untouched(row, no_default, with_default="is everything it seems"):
        return "but for now i find"

    @aggregation.register_aggregation_factor("subrecipe-2-by-element")
    def args_untouched(row, no_default, with_default="it's only in my dreams"):
        return "and i could change the world"

    @aggregation.make_transformation("subrecipe-2-transformation")
    def all_in_one_transformation(aggregator):
        return "i could be the sunlight in your universe"

    @aggregation.register_aggregation_factor("args-parsing-by-elem-1")
    def args_parsing_by_elem_1(row, first_arg, second_arg="you would think my love was really something good"):
        return "baby, if i could"

    @aggregation.register_aggregation_factor("args-parsing-by-elem-2")
    def args_parsing_by_elem_2(row, first_arg, second_arg="change"):
        return "the world"

    @aggregation.register_aggregation_factor("args-parsing-elem-1")
    def args_parsing_elem_1(row, first_arg, second_arg="if i could be king"):
        return "even for a day"

    @aggregation.register_aggregation_factor("args-parsing-elem-2")
    def args_parsing_elem_2(row, first_arg, second_arg="i'd take you as my queen", *varargs):
        return "i'd have it no other way"

    @aggregation.make_transformation("args-augmenting-transformation-1")
    def args_aug_transformation_1(aggregator, first_arg="and our love will rule", second_arg="in this kingdom we have made", *varargs):
        return "till then i'll be a fool"

def test(args):
    register_test_elements_and_transformations()

    recipe = ProfileRecipe(
        {
            'description' : 'my father\'s eyes',

            'scheme' : 'file-system',

            'by_elements' : [
                # a scheme generated element
                'process', # the full notation of {'name':'process'} will be inferred for this

                # an element with no arguments
                'no-args', # full notation inferred...

                # an element with arguments but that we won't override them
                'args-untouched',

                # an element with arguments that we will override,
                # once with a const value and once with an optional value
                {
                    'id' : 'id-test-1',
                    'name' : 'args-overridden',
                    'arguments' : {
                        'first_arg' : {
                            'type' : 'const',
                            'value' : 'first arg const value'
                        },
                        'second_arg' : {
                            'type' : 'optional',
                            'default' : 'second arg default value',
                        }
                    },
                },
            ],

            'elements' : [
                # an element that has an argument parsing function and we won't override it's arguments
                {
                    'id' : 'id-test-2',
                    'name' : 'untouched-parser',
                }, # could have just put in 'untouched-parser' but why not give the full notation a go

                # an element that has an argument parsing function and we'll override one of them
                {
                    'name' : 'modified-parser',
                    'arguments' : {
                        'first_arg' : {
                            'type' : 'const',
                            'value' : 'another first arg const value',
                        },
                    },
                },

                # an element with a variadic argument that we won't override
                {
                    'name' : 'untouched-variadic',
                },

                # an element with a variadic argument that we will override
                {
                    'name' : 'variadic',
                    'arguments' : {
                        'variadic_args' : {
                            'type' : 'const',
                            'value' : "variadic arg with one value",
                        },
                    },
                },

                # an element with a variadic argument that we will override
                {
                    'name' : 'variadic-parser',
                    'arguments' : {
                        'variadic_args' : {
                            'type' : 'const',
                            'value' : ["variadic arg with", "multiple values"],
                        },
                        'first_arg' : {
                            'type' : 'optional',
                            'default' : 'yet another first arg value',
                        },
                    },
                },
            ],

            'transformations': [
                'no-parser-transformation', # full format inferred
                {
                    'id' : 'id-test-3',
                    'name' : 'all-in-one-transformation',
                    'arguments' : {
                        'second_arg' : {
                            'type' : 'const',
                            'value' : 'const second arg'
                        },
                    },
                },
            ],

            'subrecipes' : {
                'recipe-the-first' : {
                    'elements' : [
                        {
                            'id' : 'id-test-4',
                            'name' : 'subrecipe-1-element',
                            'arguments' : {
                                'no_default' : {
                                    'type' : 'const',
                                    'value' : 'no-default-const-value',
                                },
                            },
                        },
                    ],
                },

                'recipe-the-last' : {
                    'by_elements' : [
                        {
                            'name' : 'subrecipe-2-by-element',
                            'arguments' : {
                                'with_default' : {
                                    'type' : 'optional',
                                    'default' : 'default value for recipe 2',
                                },
                            }
                        },
                    ],

                    'transformations' : [
                        'subrecipe-2-transformation',
                    ],
                },
            },
        }
    )

    simple_recipe = ProfileRecipe({
        'scheme' : 'file-system',
        'by_elements' : [
            'args-parsing-by-elem-1',
            'args-parsing-by-elem-2'
        ],

        'elements' : [
            'args-parsing-elem-1',
            'args-parsing-elem-2'
        ],

        'transformations' : [
            'args-augmenting-transformation-1'
        ],
    })

    args = RecipeItems(
        {
            'args-parsing-by-elem-2' : {
                'first_arg' : 'augmented argument',
                'second_arg' : 'augmented 2nd argument'
            },
            'args-parsing-by-elem-1' : {
                'first_arg' : 'augmented argument'
            }
        },
        {
            'args-parsing-elem-1' : {
                'first_arg' : 'augmented argument'
            },
            'args-parsing-elem-2' : {
                'second_arg' : 'augmented 2nd argument',
                'first_arg' : 'augmented argument'
            }
        },
        {
            'args-augmenting-transformation-1' : {
                'varargs' : ['augmented argument', 'yet another augmented argument']
            }
        },
    )

    print("recipe\n======\n", repr(recipe), "\n\n\n\n")
    print("augmented args\n==============\n", pprint.pformat(simple_recipe.augment_arguments(args), indent=4), "\n\n\n\n")

def main():
    parser = argparse.ArgumentParser(description="test recipe parsing and representation.")
    parser.add_argument("-u", "--show-sub-recipes", action='store_true', help='describe sub recipes as well.')
    parser.add_argument("-s", "--sub-recipe", help="show a specific sub-recipe")
    parser.add_argument("-n", "--no-defaults-resolution", action="store_true", help="don't use default recipes directory to resolve the recipe file path.")
    parser.add_argument("recipe", nargs="+", help="recipe JSON files.")

    args = parser.parse_args()

    register_test_elements_and_transformations()

    for recipe_file_name in args.recipe:
        recipe = load_recipe_from_json(recipe_file_name, not args.no_defaults_resolution)
        recipe.set_full_representation(args.show_sub_recipes)
        if args.sub_recipe is not None:
            recipe = recipe.get_subrecipe(args.sub_recipe)
        print(repr(recipe))

if __name__ == "__main__":
    sys.exit(main())

