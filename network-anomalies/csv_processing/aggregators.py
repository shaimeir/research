#!/usr/bin/env python3

import redis
import pickle
import uuid
import contextlib
import sys
import multiprocessing
import io
import logging
import msgpack
import os
from collections import defaultdict

from . import aggregation

TheRedisUnixDomainSocket = "/var/run/redis/redis.sock"

def _default_aggregate():
    return defaultdict(lambda : 0)

@aggregation.aggregators.register('dict')
class DictAggregator(aggregation._Aggregator):
    '''
    a simple dictionary aggregator. keeps everything as a hash-map.
    This is the aggregator in use. The rest is experimental code
    '''

    # this global that tells us whether to try (up to the first failure) to optimize
    # this object's serialization via msgpacking.
    MsgpackOptimization = (os.getenv("DICT_AGG_NO_MSGPACK_OPTIMIZATION", "") == "")

    def __init__(self, key_extractor, value_extractor):
        super().__init__(key_extractor, value_extractor, True)
        self.aggregates = defaultdict(_default_aggregate)
        self.optimized_with_msgpack = False

    def handlekeyvalue(self, key, value):
        self.aggregates[key][value] += 1

    def jobdigest(self):
        '''
        Return the collected data self.aggregates after converting
        it to "dict" from "defaultdict"
        '''
        return self.stats()

    def consumejobdigest(self, job_result):
        for key, value_map in job_result.items():
            if isinstance(value_map, int):
                self.aggregates[key] += value_map
            else:
                _value_map = self.aggregates[key]
                for value, ref_count in value_map.items():
                    _value_map[value] += ref_count

    def stats(self):
        '''
        For example, self.aggregates.items() can look like
        {(filename, hostId): numberOfHosts}
        This method is basically a cloning operation of a "defaultdict" to
        a "dict" (no default values).
        account for the possibility where there is no value map and there's just a count
        (as is the case with the "total-prevalence" transformation)
        '''
        return {k: v if isinstance(v, int) else dict(v) for k, v in self.aggregates.items()}

    def _preserialization(self):
        if not DictAggregator.MsgpackOptimization:
            return

        # since pickle takes a shit-load of time we'll try to improve on that by using msgpack.
        # msgpack is a bit more efficient than pickling so this yields a nice improvements in some cases.
        # however, this may fail and we'll employ a first-strike-and-you're-out strategy so when
        # an error occurres no subsequent attempts will be made to use this trick again (to avoid
        # handling large amounts of errors)
        try:
            new_aggregates = msgpack.dumps([
                {
                    'key' : key,
                    'value_map' : [
                        (value, prevalence)
                        for value, prevalence in value_map.items()
                    ]
                }

                for key, value_map in self.aggregates.items()
            ])

        except Exception:
            logging.info("couldn't serialize aggregator content with msgpack so we'll stop trying")
            DictAggregator.MsgpackOptimization = False

        else:
            self.aggregates = new_aggregates
            self.optimized_with_msgpack = True

    def _retuplize(self, item):
        return tuple((
            e.decode() if isinstance(e, bytes) else e
            for e in item
        ))

    def postdeserialization(self):
        if not self.optimized_with_msgpack:
            return

        unpack_map = msgpack.unpack(io.BytesIO(self.aggregates))
        self.aggregates = defaultdict(_default_aggregate, {
            self._retuplize(desc[b'key']) : defaultdict(lambda : 0, {
                self._retuplize(value_prevalence[0]) : value_prevalence[1]
                for value_prevalence in desc[b'value_map']
            })
            for desc in unpack_map
        })

    def parallelize(self, chunks):
        # count the number of lines to calculate the mean chunk size
        line_count = sum((len(value_map) for value_map in self.aggregates.values()), 0)
        chunk_size = (line_count + chunks - 1)//chunks + 1 # make sure the number is greater than 0

        logging.info("dict parallelization: parallelizing %d lines to %d chunks (%d lines per chunk)", line_count, chunks, chunk_size)

        value_sizes = {
            key : len(value_map)
            for key, value_map in self.aggregates.items()
        }

        chunks_yielded = 0
        while len(self.aggregates) > 0:
            chunk = DictAggregator(self.key_extractor, self.value_extractor)
            size = 0

            keys = []
            for key, value_map_size in value_sizes.items():
                size += value_map_size
                keys.append(key)
                # chunk.aggregates[key] = value_map

                if size >= chunk_size:
                    break

            keys.sort(key=lambda k: value_sizes[k], reverse=True)
            removed_keys = set()
            for key in keys:
                value_map_size = value_sizes[key]
                if size - value_map_size >= chunk_size:
                    removed_keys.add(key)
                    size -= value_map_size

            for key in keys:
                if key not in removed_keys:
                    chunk.aggregates[key] = self.aggregates.pop(key)
                    value_sizes.pop(key)

            chunks_yielded += 1
            chunk._preserialization()
            logging.info("dict parallelization: generating a chunk with %d lines (chunk #%d)", size, chunks_yielded)
            yield chunk

        logging.info("dict parallelization: dict was parallelized across %d chunks", chunks_yielded)

    def collectparallel(self, parallel_digest):
        for key, value_map in parallel_digest.items():
            if key in self.aggregates:
                for value, prevalence in value_map:
                    self.aggregates.setdefault(value, 0)
                    self.aggregates[value] += prevalence
            else:
                self.aggregates[key] = value_map

    def paralleldigest(self):
        return self.stats()

class _RedisAggregatorBase(aggregation._Aggregator):
    PipelineOperationLimit = 100000

    def __init__(self, key_extractor, value_extractor, prefix=None):
        super().__init__(key_extractor, value_extractor)
        self.prefix = prefix if prefix is not None else f"{uuid.uuid4()}::".encode()
        self.aggregates = None

        self.procs = []

        self.newpipeline()

    @contextlib.contextmanager
    def cleanupcontext(self):
        '''
        The calling code will look like
        with  agg.cleanupcontext():
            # do something
        # Implicit call to "finally"
        '''
        try:
            yield

        finally:
            self.redis.delete(*self.redis.keys(self.prefix + b"*"))

    def newpipeline(self):
        self.redis = redis.Redis(unix_socket_path=TheRedisUnixDomainSocket)
        self.pipeline = self.redis.pipeline()
        self.opcount = 0

    def set(self, key, value):
        if self.opcount >= self.PipelineOperationLimit:
            self.executepipeline()
            self.clearpipeline()
            self.newpipeline()

        self.opcount += 1
        self.redis.set(self.prefix + pickle.dumps(key), value if isinstance(value, int) else pickle.dumps(value))

    def incr(self, key, amount=1):
        if self.opcount >= self.PipelineOperationLimit:
            self.executepipeline()
            self.clearpipeline()
            self.newpipeline()

        self.opcount += 1
        self.redis.incr(self.prefix + pickle.dumps(key), amount)

    def hincrby(self, key, value, amount=1):
        if self.opcount >= self.PipelineOperationLimit:
            self.executepipeline()
            self.clearpipeline()
            self.newpipeline()

        self.opcount += 1
        self.redis.hincrby(self.prefix + pickle.dumps(key), value if isinstance(value, int) else pickle.dumps(value), amount)

    def jobdigest(self):
        self.executepipeline()
        self.clearpipeline(False)
        return {}

    def consumejobdigest(self, job_result):
        pass

    def finalize(self):
        self.executepipeline()
        self.clearpipeline(False)
        self.newpipeline()

    @staticmethod
    def _executepipeline(redis_con, pipeline):
        pipeline.execute()

    def clearpipeline(self, fast=True):
        if fast:
            liveliness = [(proc, proc.is_alive()) for proc in self.procs]
            self.procs = [proc for proc, is_alive in liveliness if is_alive]
            for proc, is_alive in liveliness:
                if not is_alive:
                    proc.join()

        else:
            for proc in self.procs:
                proc.join()

        self.opcount = 0

    def executepipeline(self):
        execution_proc = multiprocessing.Process(target=self._executepipeline, args=(self.redis, self.pipeline,))
        execution_proc.start()
        self.procs.append(execution_proc)

@aggregation.aggregators.register('redis')
class RedisAggregator(_RedisAggregatorBase):
    '''
    aggregate via redis.
    '''
    def __init__(self, key_extractor, value_extractor):
        super().__init__(key_extractor, value_extractor)
        self.aggregates = None

    def handlekeyvalue(self, key, value):
        self.hincrby(key, value)

    def finalize(self):
        super().finalize()

        keys = self.redis.keys(self.prefix + b"*")

        skip = len(self.prefix)
        self.aggregates = {
            pickle.loads(key[skip:]) : {
                pickle.loads(value) : int(ref_count) for value, ref_count in self.redis.hgetall(key).items()
            }
            for key in keys
        }

    def stats(self):
        return self.aggregates

@aggregation.aggregators.register('redis-boolean')
class RedisBooleanAggregator(_RedisAggregatorBase):
    '''
    aggregate via redis. ignore repetitions of (key, value) and count them once
    '''
    def __init__(self, key_extractor, value_extractor):
        super().__init__(key_extractor, value_extractor)
        self.aggregates = None

    def handlekeyvalue(self, key, value):
        self.incr((key, value),1)

    def finalize(self):
        super().finalize()

        keys = self.redis.keys(self.prefix + b"*")

        skip = len(self.prefix)
        self.aggregates = defaultdict(lambda : defaultdict(lambda : 0))
        for composed_key in self.redis.keys(self.prefix + b"*"):
            key, value = pickle.loads(composed_key[skip:])
            self.aggregates[key][value] = 1

    def stats(self):
        return {k: dict(v) for k, v in self.aggregates.items()}

