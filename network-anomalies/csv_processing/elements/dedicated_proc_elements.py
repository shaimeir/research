#!/usr/bin/env python3

import pathlib
import logging
import re
import csv
import sys
import argparse
import contextlib
from collections import defaultdict

if __name__ == "__main__":
    sys.path.append(str(pathlib.Path(__file__).absolute().resolve().parent.parent.parent)) # network anomalies directory
    from csv_processing import aggregation
    from csv_processing.canonization import command_line_normalization
else:
    from .. import aggregation
    from ..canonization import command_line_normalization

from utilities import option_registry_util

dedicated_cmd_parsers = option_registry_util.OptionRegistry()

non_alpha_re = re.compile(r"[^\w]")
num_re = re.compile(r"^(\d+|0x[\da-f]+)$")
hash_re = re.compile(r'^((?:[0-9a-f]{16,}){1,16}|[0-9a-f]{40,})$', re.I)
def _normalize_arg(arg):
    global non_alpha_re

    if '://' in arg or num_re.match(arg) is not None or non_alpha_re.search(arg) is not None or hash_re.match(arg) is not None:
        return '[ARGUMENT]'

    return arg

space_re = re.compile(r'\s+')
substring_re = re.compile(r'((?<!\\)\".*?(?<!\\)\")')
win_arg_split = re.compile(r'[\=\,\:]')
terminating_quotation_removal = re.compile("((?<!\\\\)(\\\"|\\')$)")

def _generic_switches_order(process, process_name, cmd, is_posix):
    logging.debug("normalizing '%s', '%s', '%s'", process, process_name, cmd)

    global space_re
    global substring_re
    global win_arg_split
    global terminating_quotation_removal

    fs_sep = "/" if is_posix else "\\"

    parts_queue = substring_re.split(terminating_quotation_removal.sub('', cmd.strip().lstrip("\"'")).lower())

    parts = []
    last_element_was_str=False
    for part in parts_queue:
        if part.startswith('"') and part.endswith('"') and not part.endswith('\\"'):
            last_element_was_str = True
            part = space_re.sub(" ", part.strip('"'))

            if len(parts) == 0:
                parts.append(part)
            elif parts[-1] == '': # if the last decomposition had a space at the end then drop the space and attach this string
                parts.pop(-1)
                parts.append(part)
            else:
                parts[-1] += part

        else:
            # break spaces down
            space_split = space_re.split(part)

            if space_split[0] != '' and last_element_was_str:
                parts[-1] += space_split[0]
                parts += space_split[1:]
            else:
                parts += space_split

            last_element_was_str = False

    parts = [part for part in parts[1:] if part != ''] # skip process name - this poses some problematic assumptions that the command line isn't "c:\program files\chrome.exe bla bla" etc...

    switches = defaultdict(list)
    args = []
    while len(parts) > 0:
        elem = parts.pop(0)

        if elem.startswith("-") and '=' in elem:
            elem, arg = tuple(elem.split("=", 1))
            switches[elem].append(arg)
        elif elem.startswith("-"):
            arg = parts.pop(0) if len(parts) > 0 and not (parts[0].startswith("-") or (not is_posix and parts[0].startswith("/"))) else ''
            switches[elem].append(arg)
        elif not is_posix and elem.startswith("/") and (':' in elem or ',' in elem or '=' in elem): # windows
            elem, arg = tuple(win_arg_split.split(elem, 1))
            switches[elem].append(arg)
        elif not is_posix and elem.startswith("/"): # windows
            arg = parts.pop(0) if len(parts) > 0 and not (parts[0].startswith("-") or (not is_posix and parts[0].startswith("/"))) else ''
        else:
            args.append(elem)

    sorted_switches = sorted(list(switches.keys()))
    args = sorted(args)

    cmd = [process_name]
    for switch in sorted_switches:
        sw_args = switches[switch]
        for sw_arg in sw_args:
            cmd += [switch, _normalize_arg(sw_arg)]

    cmd += args

    return space_re.sub(" ", ' '.join(cmd))

@dedicated_cmd_parsers.register("svchost.exe")
@dedicated_cmd_parsers.register("chrome.exe")
def _generic_switches_order_windows(process, process_name, cmd):
    return _generic_switches_order(process, process_name, cmd, False)

def _parse_proc_name_ex_args(proc_col='process', cmd_col='command', platform='windows', *ex_procs):
    handlers = dedicated_cmd_parsers.choicesFunctions()

    path_parsers = {
        'windows' : pathlib.PureWindowsPath,
        'posix' : pathlib.PurePosixPath,
    }

    assert platform in path_parsers

    for p in ex_procs:
        assert p in handlers, ValueError(f"{p} has no dedicated parser", list(handlers.keys()))

    return proc_col, cmd_col, path_parsers[platform], set(ex_procs) if len(ex_procs) > 0 else set(dedicated_cmd_parsers.choices().keys()), handlers

@aggregation.register_aggregation_factor("proc-name-extended", _parse_proc_name_ex_args)
def proc_name_ex(row, proc_col, cmd_col, path_parser, ex_procs, handlers):
    '''
    process name for most processes and dedicated command line parsing for specifically tailored processes.
    '''

    process = row[proc_col].strip(" \t\"'").lower()
    proc_name = path_parser(process).name

    if proc_name in ex_procs:
        logging.debug("use dedicated handler for %s", proc_name)
        return handlers[proc_name](process, proc_name, row[cmd_col])

    return proc_name

def test():
    parser = argparse.ArgumentParser(description="normalize command lines from input.")
    parser.add_argument("-v", "--verbose", action="store_true", help="show debug prints.")
    parser.add_argument("file", nargs="*", default=["-"], help="input file. '-' for stdin.")

    args = parser.parse_args()
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s", level=logging.DEBUG if args.verbose else logging.WARN)

    proc_name_ex_args = _parse_proc_name_ex_args(0, 1)

    for input_file in args.file:
        with (auto_context(sys.stdin) if '-' == input_file else open(input_file, "rt")) as input_stream:
            for row in csv.reader(input_stream, delimiter='\t', quoting=csv.QUOTE_NONE):
                print(proc_name_ex(row, *proc_name_ex_args))

if __name__ == "__main__":
    with contextlib.suppress(BrokenPipeError):
        sys.exit(test())

