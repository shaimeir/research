#!/usr/bin/env python3

import ipaddress
import re
import logging
import sys
import pprint
import datetime
import dateutil.parser
import pathlib
from collections import Iterable

from .. import aggregation
from . import fs_elements
from . import proc_elements
from . import net_elements
from . import module_elements
from . import user_elements
from . import registry_elements

@aggregation.register_aggregation_factor("placeholder")
def placeholder(row, placeholder='placeholder'):
    '''
    yields the word 'placeholder'
    '''
    return placeholder

@aggregation.register_aggregation_factor("placeholder2")
def placeholder2(row, place_arg='place', holder_arg='holder'):
    '''
    used for testing.
    '''
    return place_arg + "_" + holder_arg

@aggregation.register_aggregation_factor("process-location")
def process_location(row):
    '''
    yields the path to the executable that ran.
    '''
    proc = row['process']
    if proc is None or proc == '':
        return ''

    return str(pathlib.PureWindowsPath(proc).parent)

_work_start = datetime.time(8) # 8 in the morning
_work_end = datetime.time(18) # 6 in the evening
@aggregation.register_aggregation_factor("work-hours")
def workhours(row):
    '''
    yields 'work' if the time is during workhours and 'home' otherwise.
    '''
    return 'work' if _work_start <= row['timestamp'].time() <= _work_end else 'home'

@aggregation.register_aggregation_factor("process-name")
def proc_name(row, column='process'):
    '''
    yields the process name (discard full path) of the executing process.
    '''
    return row[column].rsplit("\\",1)[-1]

_has_hostname=None
@aggregation.register_aggregation_factor("host-extended")
def proc_name(row, column='host', hostname='host_name'):
    '''
    yields the host id augmented with a host name
    '''
    global _has_hostname

    if _has_hostname is None:
        _has_hostname = hostname in row

    if _has_hostname:
        return row[column] + "-" + row[hostname]
    else:
        return row[column]

_is_datetime_on_arrival = None
@aggregation.register_aggregation_factor("date-and-hour")
def date_and_hour(row, ts_col='timestamp'):
    global _is_datetime_on_arrival
    ts = row[ts_col]

    if _is_datetime_on_arrival is None:
        _is_datetime_on_arrival = isinstance(ts, datetime.datetime)

    if not _is_datetime_on_arrival:
        ts = dateutil.parser.parse(ts)

    return ts.strftime("%Y/%m/%d %H")

