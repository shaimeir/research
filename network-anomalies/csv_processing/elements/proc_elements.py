#!/usr/bin/env python3

import pathlib
import logging
from .. import aggregation
from ..canonization import command_line_normalization

from . import dedicated_proc_elements

default_by_cmd_procs_file=pathlib.Path(__file__).parent/'by-cmd-procs'
def _parse_normalized_cmd_restricted_args(proc_col='process', cmd_col='command', strength='weak', cmd_type='windows', by_cmd_proc=default_by_cmd_procs_file, *by_cmd_procs):
    strength = strength.strip().lower()
    assert strength in ('strong', 'weak')

    _by_cmd_procs = set()

    by_cmd_procs = (by_cmd_proc,) + by_cmd_procs
    for _by_cmd_proc in by_cmd_procs:
        if isinstance(_by_cmd_proc, str):
            _by_cmd_proc = _by_cmd_proc.strip()

        _by_cmd_proc_file_path = pathlib.Path(_by_cmd_proc)
        if _by_cmd_proc_file_path.is_file():
            with open(_by_cmd_proc_file_path, "rt") as by_cmd_proc_list:
                _by_cmd_procs.update({proc.strip() for proc in by_cmd_proc_list})
        else:
            _by_cmd_procs.add(_by_cmd_proc)

    logging.info("extend proc names with cmd for: %r", _by_cmd_procs)
    return proc_col, cmd_col, command_line_normalization.CommandLineNormalizer(strength == 'strong', None, cmd_type), _by_cmd_procs

@aggregation.register_aggregation_factor("normalized-cmd-restricted", _parse_normalized_cmd_restricted_args)
def normalized_cmd_restricted(row, proc_column, cmd_column, normalizer, by_cmd_procs):
    '''
    a normalized representation of the command line of the process in the given CSV line, restricted to select processes.
    '''
    proc_name = normalizer.path_parser(row[proc_column]).name

    return normalizer.normalize(row[cmd_column]) if proc_name in by_cmd_procs else proc_name

def _parse_normalized_cmd_args(proc_col='process', cmd_col='command', strength='weak', cmd_type='windows', by_cmd_proc=default_by_cmd_procs_file, *by_cmd_procs):
    strength = strength.strip().lower()
    assert strength in ('strong', 'weak')

    return proc_col, cmd_col, command_line_normalization.CommandLineNormalizer(strength == 'strong', None, cmd_type)

@aggregation.register_aggregation_factor("normalized-cmd", _parse_normalized_cmd_args)
def normalized_cmd(row, proc_column, cmd_column, normalizer):
    '''
    a normalized representation of the command line of the process in the given CSV line.
    '''
    cmd = row.get(cmd_column, None)
    if cmd is None:
        return normalizer.path_parser(row[proc_column]).name

    return normalizer.normalize(cmd)

