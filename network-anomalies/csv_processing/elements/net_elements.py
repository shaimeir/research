#!/usr/bin/env python3

import ipaddress
import re
import logging
import sys
import pprint
import uuid
import datetime
import pathlib
from collections import Iterable

from .. import aggregation
from . import fs_elements
from . import net_elements

_internal_subnets = (
    ipaddress.ip_network('127.0.0.0/8'),
    ipaddress.ip_network('10.0.0.0/8'),
    ipaddress.ip_network('192.168.0.0/16'),
    ipaddress.ip_network('172.16.0.0/12')
)

@aggregation.register_aggregation_factor("is-internal-conn")
def is_connection_internal(row, remote='remote'):
    '''
    yields 'internal' if the connection is to an internal subnet and 'external' if to the internet.
    '''
    return 'internal' if any((row[remote] in subnet for subnet in _internal_subnets)) else 'external'

@aggregation.register_aggregation_factor("has-hostname")
def has_hostname(row, hostname_col='remote_host'):
    '''
    yields 'has-hostname' if the remote host is available in the row and 'no-hostname' otherwise
    '''
    hostname = row.get(hostname_col, None)
    hostname = ('' if hostname is None else hostname).strip()
    return "has-hostname" if hostname != '' else 'no-hostname'

_known_ports = {
    1029 : "MS-DCOM",
    1080 : "SOCKS proxy",
    1194 : "Open VPN",
    1293 : "IPSec",
    1443 : "MS-SQL",
    1688 : "MS-KMS",
    1900 : "SSDP",
    2483 : "Oracle DB",
    2484 : "Oracle DB (SSL)",
    3306 : "MySQL",
    3268 : "LDAP",
    3269 : "LDAP (SSL)",
    3389 : "RDP",
    5443 : "Vertica",
    5500 : "VNC",
    5722 : "MS-SYSVOL",
    5800 : "VNC stuff",
    5900 : "VNC stuff",
    5901 : "VNC stuff",
    8080 : "HTTP proxy",
    8172 : "MS-IIS remote administration",
    8443 : "HTTPS proxy",
    9389 : "MS-Bullshit (powershell and stuff, bitch)",
}
_known_ports.update({port : "Silverlight" for port in range(4502,4535)})

@aggregation.register_aggregation_factor("port-service")
def port_service(row, port_col="rport"):
    '''
    gives a port a name.
    '''
    port = row.get(port_col, None)
    if port is None:
        return "unknown"

    try:
        port = int(port)
        return "ephemeral" if port >= 32768  else _known_ports.get(port, str(port))
    except:
        return "unknown"

_net_access_categories = {
    'unknown'            : 'unknown',
    'tcp-connect'        : 'tcp-outbound',
    'tcp-failed-connect' : 'tcp-outbound',
    'tcp-accept'         : 'tcp-inbound',
    'udp-send'           : 'udp-outbound',
    'udp-connect'        : 'udp-outbound',
    'icmp-connect'       : 'icmp-outbound',
    'raw-socket'         : 'raw',
}

@aggregation.register_aggregation_factor("net-access-category")
def net_access_category(row, column="access_type"):
    '''
    a bucketed version of network access type
    '''
    return _net_access_categories.get(row[column], "other-access")

@aggregation.register_aggregation_factor("remote-host")
def remote_host(row, column="remote_host"):
    '''
    strip the remote host of padding.
    '''
    host = row[column]
    return None if host is None else host.strip()

auto_domain_uid = None
auto_domain_counter = 0
@aggregation.register_aggregation_factor("remote-host-enforced")
def remote_host_enforced(row, column="remote_host"):
    '''
    generate a unique domain if the remote host field is empty
    '''
    global auto_domain_uid
    global auto_domain_counter

    if auto_domain_uid is None: # this is here so different uuids will be generated in each process in parallelized processing
        auto_domain_uid = "no-domain-" + str(uuid.uuid4()) + "-generated-"

    remote_host = row[column]
    if remote_host is not None:
        remote_host = remote_host.strip()

    if remote_host is None or len(remote_host) == 0:
        auto_domain_counter += 1
        return auto_domain_uid + str(auto_domain_counter)

    return remote_host

@aggregation.register_aggregation_factor("remote-port")
def remote_port(row, rport='rport', real_rport='real_rport'):
    port = row.get(real_rport, None)
    return port if port is not None else row[rport]

