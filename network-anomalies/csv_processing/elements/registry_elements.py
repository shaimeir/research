#!/usr/bin/env python3

from .. import aggregation
from ..canonization import registry_canonization
import logging

_reg_canonizer = registry_canonization.RegistryCanonizer()

@aggregation.register_aggregation_factor("reg-canon")
def reg_canon(row, column='key'):
    key = row.get(column, None)
    if key is None:
        return ''

    return _reg_canonizer.canonize(key)

