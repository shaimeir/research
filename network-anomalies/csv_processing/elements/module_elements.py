#!/usr/bin/env python3

import ipaddress
import re
import logging
import sys
import pprint
import datetime
import pathlib
from collections import Iterable

from .. import aggregation
from . import fs_elements
from . import net_elements

@aggregation.register_aggregation_factor("module-name")
def module_name(row):
    '''
    yields the module name (discard full path) of the executing process.
    '''
    return row['module'].rsplit("\\",1)[-1]

sys32_re = re.compile(r"^c:\\windows\\sys(tem32|wow64)\\", re.I)
@aggregation.register_aggregation_factor("is-system32")
def is_system32(row, column='module'):
    '''
    yields 'sys' or 'aux' if the module is found in the system32 directory or not.
    '''
    return 'sys' if sys32_re.search(row[column]) is not None else 'aux'

