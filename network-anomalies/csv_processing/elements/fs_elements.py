#!/usr/bin/env python3

from .. import aggregation
from ..canonization import fs_digests
import logging
import pathlib

_fs_canonizer = fs_digests.get_canonizer()

@aggregation.register_aggregation_factor("fs-canonized-path")
def fs_canon_path(row, column='file'):
    '''
    yields a canonized path name taken from a 'file' column.
    '''
    path = row.get(column, None)
    if path is None:
        return ''

    return _fs_canonizer.canonizePath(path)[0]

@aggregation.register_aggregation_factor("fs-canonized-ext")
def fs_canon_ext(row, column='file'):
    '''
    yields a canonized extension name taken from the 'file' column.
    '''
    path = row.get(column, None)
    if path is None:
        return ''

    ext = _fs_canonizer.canonizeExtension(path)
    return ext

@aggregation.register_aggregation_factor("fs-canonize")
def canon_path(row, column='file'):
    '''
    yields a canonized full file path name taken from a 'file' column.
    '''
    path = row.get(column, None)
    if path is None:
        return ''

    canon, path_match, normalized_path = _fs_canonizer.canonize(path)
    return canon + normalized_path[len(path_match):]

def _parse_canon_path_args(col='file', parser='windows'):
    parser = {
        'windows' : pathlib.PureWindowsPath,
        'posix' : pathlib.PurePosixPath,
    }[parser.lower().strip()]
    return col, parser

@aggregation.register_aggregation_factor("fs-canonize-with-ext-dir", _parse_canon_path_args)
def canon_path(row, column, path_parser_class):
    '''
    yields a canonized full file path name taken from a 'file' column.
    '''
    path = row.get(column, None)
    if path is None:
        return ''

    canon, path_match, normalized_path = _fs_canonizer.canonize(path)
    path = canon + normalized_path[len(path_match):]

    extension = '[{}:EXTENSION:{}]'.format(_fs_canonizer.PlaceholderToken, _fs_canonizer.canonizeExtension(path))
    path = path_parser_class(path)

    return str(path.parent/extension/path.name)

def _parse_canon_proc_location_args(col='process', parser='windows'):
    return _parse_canon_path_args(col, parser)

@aggregation.register_aggregation_factor("fs-canonized-proc-location", _parse_canon_proc_location_args)
def canon_proc_location(row, column, path_parser_class):
    '''
    canonize the directory the process was run from
    '''
    process = row.get(column, None)
    if process is None:
        return ''

    path = path_parser_class(process).parent
    return _fs_canonizer.canonizePath(str(path))[0]

@aggregation.register_aggregation_factor("fs-canonized-process")
def canon_proc_location(row, column='process'):
    '''
    canonize the directory the process was run from
    '''
    process = row.get(column, None)
    if process is None:
        return ''

    canon = _fs_canonizer.canonize(process)
    return canon[0] + canon[2][len(canon[1]):]

_READ_OPS = {'open'}
_MODIFY_OPS = {'write','create','rename','delete','copy'}
_MODIFY_DIR_OPS = {'move-dir','new-dir','delete-dir'}

@aggregation.register_aggregation_factor("fs-access-category")
def fs_access_category(row, column='access_type'):
    '''
    canonize the access type (unknown, read, modify, read-dir, modify-dir)
    '''
    access = row.get(column, None)
    if access is None or access == 'unknown':
        return 'unknown'
    elif access in _READ_OPS:
        return 'read'
    elif access in _MODIFY_OPS:
        return 'modify'
    elif access in _MODIFY_DIR_OPS:
        return 'modify-dir'
    else:
        return 'unknown'

def _parse_partial_path_depth_args(depth=3, col='file', parser='windows'):
    parser = {
        'windows' : pathlib.PureWindowsPath,
        'posix' : pathlib.PurePosixPath,
    }[parser.lower().strip()]
    depth = int(depth)
    assert depth > 0
    return depth, col, parser

@aggregation.register_aggregation_factor("fs-partial-path-depth", _parse_partial_path_depth_args)
def partial_path_depth(row, depth, column, path_parser_class):
    '''
    canonize a path and extract the leading directories (given by a constant) from a path.
    '''
    path = row.get(column, None)
    if path is None:
        return ''

    canon, path_match, normalized_path = _fs_canonizer.canonize(path)
    path = canon + normalized_path[len(path_match):]

    extension = '[{}:EXTENSION:{}]'.format(_fs_canonizer.PlaceholderToken, _fs_canonizer.canonizeExtension(path))
    path = path_parser_class(path)

    return str(path_parser_class(*((path.parent/extension/path.name).parts[:depth])))

