#!/usr/bin/env python3

from ..canonization import user_digest
from .. import aggregation

_user_canonizer = user_digest.UsernameNormalizer()

@aggregation.register_aggregation_factor("canonized-user")
def canonized_user(row, column='username'):
    '''
    canonize a username into well-known buckets.
    '''
    username = row.get(column, None)
    return _user_canonizer.canonize(username) if username is not None else 'No User'

