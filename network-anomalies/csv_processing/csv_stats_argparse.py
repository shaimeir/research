#!/usr/bin/env python3
'''
csv-stats.py requires extra tasks of it's argument parser:
    * auto-register aggregation elements and transformations.
    * infer the scheme and register the column aggregation elements before fully parsing the command line
      (so we can write "-s file-system -a file -a access -a local_port" and have the parser succeed on
      'access' and fail on 'local_port')
    * be recipe aware. i.e. if there's a --recipe switch in the command line then auto-generate the
      proper command line parser and reformat the input in the same fashion as a non-recipe command
      line would format it.

the latter requirement is the "beefiest" and the one that justifies separation of the argument parser
to a module in and of it's own.
this module can later be used (maybe) with the cross validation scripts and etc.
'''

import argparse
import sys
from collections import defaultdict

from . import scheme

from . import aggregation
from . import aggregators
from . import formatters

from .elements import aggregation_elements
from .transformations import transformations
from .filters import filters

from . import profile_recipe

class CsvStatsArgparse(argparse.ArgumentParser):
    def __init__(self, *args, **kwargs):
        # argparse.ArgumentParser.__init__ uses add_argument to add the default help argument
        # so we have to disable our implementation until we're good to go
        self.apply_add_argument_mirroring = False

        super().__init__(*args, **kwargs)

        # an intermediate parser used to scheme and or recipe switches
        # to determine how the command line is supposed to be parsed (as
        # specified by the user or as a recipe execution)
        self.inferral_parser = argparse.ArgumentParser(description=self.description)
        self.inferral_parser.add_argument("--recipe", help="use a recipe specified in a JSON file.")
        self.inferral_parser.add_argument("--sub-recipe", help="use a sub-recipe of the specified recipe file.")
        self.recipe_mode = False # if False then it's regular cmd parsing mode
        self.mode_inferred = False # was the mode inferred already? (regular vs. recipe)

        # fields used to infer the scheme from the command line
        self.infer_scheme = False
        self.inferred_scheme_name = None
        self.scheme_switch_name = None
        self.scheme = None
        self.scheme_fixed = False

        # fields used to capture the by-element arguments
        self.by_element_argument_spec = None
        self.by_element_argument_dest = None
        self.by_element_args_argument_dest = None
        self.by_element_argument_set = False

        # fields used to capture the element arguments
        self.element_argument_spec = None
        self.element_argument_dest = None
        self.element_args_argument_dest = None
        self.element_argument_set = False

        # fields used to capture the transformations arguments
        self.transformation_argument_dest = None
        self.transformation_args_argument_dest = None

        # the recipe command line parser
        self.recipe_cmd_parser = argparse.ArgumentParser(description=self.description + " (executed with a recipe)")
        self.recipe_cmd_parser.add_argument("--recipe", help="use a recipe specified in a JSON file.")
        self.recipe_cmd_parser.add_argument("--sub-recipe", help="use a sub-recipe of the specified recipe file.")
        self.recipe_cmd_parser.add_argument("--by-elem-arg", action="append", nargs=3, default=[], help="set the arguments for by-elements using the format: by_element argument value.")
        self.recipe_cmd_parser.add_argument("--elem-arg", action="append", nargs=3, default=[], help="set the arguments for elements using the format: element argument value.")
        self.recipe_cmd_parser.add_argument("--transformation-arg", action="append", nargs=3, default=[], help="set the arguments for transformations using the format: transformation argument value.")
        self.recipe_cmd_parser.add_argument("--arg-by-alias", action="append", nargs=2, default=[], help="set a by-element/element/transformation argument by it's alias.")
        self.recipe_cmd_parser.add_argument("--ignore-unknown-aliases", action="store_true", help="if an alias that can't be resolved was handed to --arg-by-alias then skip it instead of failing the program.")
        self.recipe_mode_initialized = False

        self.apply_add_argument_mirroring = True

    def add_scheme_argument(self, *args, **kwargs):
        assert 'choices' not in kwargs, ValueError('add_scheme_argument() automatically sets the allowed scheme choices list')
        assert kwargs.get('required', True), ValueError('add_scheme_argument() - scheme switches must be set to required=True')
        kwargs.pop('required', None)
        super().add_argument(*args, choices=scheme.get_default_schemes_list(), required=True, **kwargs)

        self.scheme_switch_name = self.inferral_parser.add_argument(*args, choices=scheme.get_default_schemes_list(), required=False, **kwargs).dest
        self.infer_scheme = True

    def add_by_element_argument(self, *args, **kwargs):
        assert self.by_element_argument_spec is None, ValueError('calling add_by_element_argument() twice isn\'t supported')
        assert 'choices' not in kwargs, ValueError('by_add_element_argument() adds the choices keyword automatically, do not use it yourself')
        assert 'action' not in kwargs, ValueError('by_add_element_argument() adds the action keyword automatically, do not use it yourself')
        self.by_element_argument_spec = (args, kwargs)

    def add_element_argument(self, *args, **kwargs):
        assert self.element_argument_spec is None, ValueError('calling add_element_argument() twice isn\'t supported')
        assert 'choices' not in kwargs, ValueError('add_element_argument() adds the choices keyword automatically, do not use it yourself')
        assert 'action' not in kwargs, ValueError('add_element_argument() adds the action keyword automatically, do not use it yourself')
        self.element_argument_spec = (args, kwargs)

    def add_by_element_args_argument(self, *args, **kwargs):
        assert self.by_element_args_argument_dest is None, ValueError('calling add_by_element_args_argument() twice is not allowed')
        assert self.by_element_argument_spec is not None, ValueError('you must use add_by_element_argument() before you can use add_by_element_args_argument()')
        assert 'action' not in kwargs, ValueError('don\'t use the "action" keyword with add_by_element_args_argument() - it is filled automatically')
        assert 'default' not in kwargs, ValueError('don\'t use the "default" keyword with add_by_element_args_argument() - it is filled automatically')
        switch = super().add_argument(*args, action="append", default=[], **kwargs)
        self.by_element_args_argument_dest = switch.dest
        return switch

    def add_element_args_argument(self, *args, **kwargs):
        assert self.element_args_argument_dest is None, ValueError('calling add_element_args_argument() twice is not allowed')
        assert self.element_argument_spec is not None, ValueError('you must use add_element_argument() before you can use add_element_args_argument()')
        assert 'action' not in kwargs, ValueError('don\'t use the "action" keyword with add_element_args_argument() - it is filled automatically')
        assert 'default' not in kwargs, ValueError('don\'t use the "default" keyword with add_element_args_argument() - it is filled automatically')
        switch = super().add_argument(*args, action="append", default=[], **kwargs)
        self.element_args_argument_dest = switch.dest
        return switch

    def add_transformation_argument(self, *args, **kwargs):
        assert self.transformation_argument_dest is None, ValueError('calling add_transformation_argument() twice is not allowed')
        assert 'action' not in kwargs, ValueError('don\'t use the "action" keyword with add_transformation_argument() - it is filled automatically')
        assert 'default' not in kwargs, ValueError('don\'t use the "default" keyword with add_transformation_argument() - it is filled automatically')
        assert 'choices' not in kwargs, ValueError('add_transformation_argument() adds the choices keyword automatically, do not use it yourself')
        kwargs['help'] = kwargs.get('help', '<no help specified>').strip() + "\n" + aggregation.data_transformations.doc()
        switch = super().add_argument(*args, default=[], choices=aggregation.data_transformations.choicesList(), action="append", **kwargs)
        self.transformation_argument_dest = switch.dest
        return switch

    def add_transformation_args_argument(self, *args, **kwargs):
        assert self.transformation_args_argument_dest is None, ValueError('calling add_transformation_args_argument() twice is not allowed')
        assert 'action' not in kwargs, ValueError('don\'t use the "action" keyword with add_transformation_args_argument() - it is filled automatically')
        assert 'default' not in kwargs, ValueError('don\'t use the "default" keyword with add_transformation_args_argument() - it is filled automatically')
        switch = super().add_argument(*args, default=[], action="append", **kwargs)
        self.transformation_args_argument_dest = switch.dest
        return switch

    def add_argument(self, *args, **kwargs):
        '''
        allow support of regular switches even in recipe mode.
        '''
        if not self.apply_add_argument_mirroring:
            return super().add_argument(*args, **kwargs)

        self.recipe_cmd_parser.add_argument(*args, **kwargs)
        return super().add_argument(*args, **kwargs)

    def _infer_mode(self, args, namespace):
        if self.mode_inferred:
            return

        _args = sys.argv[1:] if args is None else args
        inferral_args = [arg for arg in _args if arg not in {"-h", "--help"}]
        inferred_namespace, other_args = self.inferral_parser.parse_known_args(inferral_args)

        if inferred_namespace.recipe is not None:
            self.recipe_mode = True
            assert not self.infer_scheme or getattr(inferred_namespace, self.scheme_switch_name) is None, ValueError("scheme and recipe switches are mutually exclusive, you can't use both")
        else:
            if not self.infer_scheme or getattr(inferred_namespace, self.scheme_switch_name) is None:
                self.inferral_parser.error("if you don't specify a recipe you must specify a scheme")
                sys.exit(1)

            self.recipe_mode = False
            self.inferred_scheme_name = getattr(inferred_namespace, self.scheme_switch_name)
            assert inferred_namespace.recipe is None and inferred_namespace.sub_recipe is None, ValueError("scheme and recipe switches are mutually exclusive, you can't use both")

        self.mode_inferred = True

    def parse_args(self, args=None, namespace=None):
        '''
        parse command line arguments, but infer the scheme first and register the appropriate column aggregation factors.
        '''
        self._infer_mode(args, namespace)

        if self.recipe_mode:
            return self._recipe_parse_args(args, namespace)
        else:
            return self._normal_parse_args(args, namespace)

    def _set_by_element_arguments(self):
        if self.by_element_argument_set:
            return

        assert self.by_element_argument_spec is not None
        switches = self.by_element_argument_spec[0]
        attributes = self.by_element_argument_spec[1]
        attributes['help'] = attributes.get('help', '<no help specified>').strip() + "\n" + aggregation.aggregation.doc()
        self.by_element_argument_dest = super().add_argument(*switches, action="append", choices=aggregation.aggregation.choicesList(), **attributes).dest

        self.by_element_argument_set = True

    def _set_element_arguments(self):
        if self.element_argument_set:
            return

        switches = self.element_argument_spec[0]
        attributes = self.element_argument_spec[1]
        attributes['help'] = attributes.get('help', '<no help specified>').strip() + "\n" + aggregation.aggregation.doc()
        self.element_argument_dest = super().add_argument(*switches, action="append", choices=aggregation.aggregation.choicesList(), **attributes).dest

        self.element_argument_set = True

    def _fix_scheme(self):
        if self.scheme_fixed:
            return

        self.scheme = scheme.load_default_scheme_file(self.inferred_scheme_name)

        for csv_column, col_parser in self.scheme.column_parsers().items():
            aggregation.register_column_factor(csv_column, col_parser)

        if self.by_element_argument_spec is not None:
            self._set_by_element_arguments()

        if self.element_argument_spec is not None:
            self._set_element_arguments()

        self.scheme_fixed = True

    def _init_recipe_mode(self):
        if self.recipe_mode_initialized:
            return

        assert self.by_element_argument_spec is not None, ValueError("recipe mode requires by-element argument specification")
        assert self.by_element_args_argument_dest is not None, ValueError("recipe mode requires by-element args argument specification")
        assert self.element_argument_spec is not None, ValueError("recipe mode requires element argument specification")
        assert self.element_args_argument_dest is not None, ValueError("recipe mode requires element args argument specification")
        assert self.transformation_argument_dest is not None, ValueError("recipe mode requires transformation argument specification")
        assert self.transformation_args_argument_dest is not None, ValueError("recipe mode requires transformation args argument specification")

        # set the by-elements/elements switches just to fetch their dest field
        self._set_by_element_arguments()
        self._set_element_arguments()

        self.recipe_mode_initialized = True

    def _recipe_parse_args(self, args, namespace):
        self._init_recipe_mode()
        parsed_args = self.recipe_cmd_parser.parse_args(args, namespace)

        # augment the parsed args with regular command line-like mock arguments

        # load the correct JSON recipe
        recipe = profile_recipe.load_recipe_from_json(parsed_args.recipe)
        if parsed_args.sub_recipe is not None:
            recipe = recipe.get_subrecipe(parsed_args.sub_recipe)

        recipe.validate()

        # this is the first point we can fix the scheme
        self.inferred_scheme_name = recipe.scheme_name
        self._fix_scheme()

        # load the arguments given in the command line
        arguments = profile_recipe.RecipeItems(defaultdict(dict), defaultdict(dict), defaultdict(dict))

        for by_elem_arg in parsed_args.by_elem_arg:
            by_elem, arg, value = tuple(by_elem_arg)
            arguments.by_elements[by_elem][arg.replace("-", "_")] = value

        for elem_arg in parsed_args.elem_arg:
            elem, arg, value = tuple(elem_arg)
            arguments.elements[elem][arg.replace("-", "_")] = value

        for transformation_arg in parsed_args.transformation_arg:
            transformation, arg, value = tuple(transformation_arg)
            arguments.transformations[transformation][arg.replace("-", "_")] = value

        # load values by aliases
        for by_alias_arg in parsed_args.arg_by_alias:
            alias, value = tuple(by_alias_arg)
            argument_desc = recipe.alias(alias)

            if argument_desc is None:
                if parsed_args.ignore_unknown_aliases:
                    continue
                else:
                    self.recipe_cmd_parser.error(f"invalid alias '{alias}'")

            if argument_desc.type == "by_element":
                arguments.by_elements[argument_desc.element][argument_desc.argument] = value
            elif argument_desc.type == "element":
                arguments.elements[argument_desc.element][argument_desc.argument] = value
            elif argument_desc.type == "transformation":
                arguments.transformations[argument_desc.element][argument_desc.argument] = value
            else:
                raise ValueError('invalid argument type in argument descriptor', argument_desc.type)

        arguments = recipe.augment_arguments(arguments)

        # augment parsed_args with the expected fields
        setattr(parsed_args, self.by_element_argument_dest, [by_element.name for by_element in recipe.by_elements.values()])
        setattr(parsed_args, self.element_argument_dest, [element.name for element in recipe.elements.values()])
        setattr(parsed_args, self.transformation_argument_dest, [transformation.name for transformation in recipe.transformations.values()])

        setattr(
            parsed_args,
            self.by_element_args_argument_dest,
            [
                args_desc.name + '=' + ",".join([str(v) for v in args_desc.args.values()] + [str(v) for v in args_desc.variadic])
                for by_element, args_desc in arguments.by_elements.items()
                if len(args_desc.args) + len(args_desc.variadic) > 0
            ]
        )

        setattr(
            parsed_args,
            self.element_args_argument_dest,
            [
                args_desc.name + '=' + ",".join([str(v) for v in args_desc.args.values()] + [str(v) for v in args_desc.variadic])
                for element, args_desc in arguments.elements.items()
                if len(args_desc.args) + len(args_desc.variadic) > 0
            ]
        )

        setattr(
            parsed_args,
            self.transformation_args_argument_dest,
            [
                args_desc.name + '=' + ",".join([str(v) for v in args_desc.args.values()] + [str(v) for v in args_desc.variadic])
                for transformation, args_desc in arguments.transformations.items()
                if len(args_desc.args) + len(args_desc.variadic) > 0
            ]
        )

        return parsed_args

    def _normal_parse_args(self, args, namespace):
        self._fix_scheme()
        return super().parse_args(args, namespace)

