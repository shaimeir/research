#!/usr/bin/env python3

import ipaddress
import re
import logging
import sys
import pprint
import datetime
import pathlib
from collections import Iterable

from .. import aggregation

@aggregation.make_filter("subnet", lambda scheme, col, *subnets: (col,) + tuple((ipaddress.ip_network(subnet) for subnet in subnets)))
def address_in_subnet(row, scheme, col, *subnets):
    '''
    check that a given IP address is in a given subnet
    '''
    return any((row[col] in subnet for subnet in subnets))

