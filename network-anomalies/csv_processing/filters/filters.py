#!/usr/bin/env python3

import ipaddress
import re
import logging
import sys
import pprint
import datetime
import pathlib
from collections import Iterable

from .. import aggregation

@aggregation.make_filter("contained", lambda scheme, col, *values: (col,) + tuple((scheme.scheme[col].type(val) for val in values)))
def containment_filter(row, scheme, column, *values):
    '''
    check that the value of a given column is in a given set
    '''
    return row[column] in values

@aggregation.make_filter("in-range", lambda scheme, col, *values: (col,) + tuple((scheme.scheme[col].type(val) for val in values)))
def range_filter(row, scheme, column, *values):
    '''
    check that a value is within a given range
    '''

    index = 0

    val = row[column]
    l = len(values)
    if (l&1) == 1:
        l -= 1

    while index < l:
        if values[index] <= val <= values[index+1]:
            return True
        index += 2

    return len(values)&1 == 1 and val >= values[-1]

_pat_escape=lambda pattern: pattern.replace("\\", "\\\\").replace(".","\\.").replace("*",".*")
@aggregation.make_filter("like", lambda scheme, col, *patterns: [col] + [re.compile(_pat_escape(pattern), re.I) for pattern in patterns])
def filter_process_name(row, scheme, col, *patterns):
    return any((pattern.match(row[col]) is not None for pattern in patterns))

