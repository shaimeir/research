#!/usr/bin/env python3

import contextlib
import re
import inspect
import recordclass
from collections import defaultdict, Iterable, OrderedDict
from utilities import option_registry_util

# OptionRegistry is a dictionary keeping a bunch of decorators
aggregation = option_registry_util.OptionRegistry()
data_transformations = option_registry_util.OptionRegistry()
filters = option_registry_util.OptionRegistry()
formatters = option_registry_util.OptionRegistry()
aggregators = option_registry_util.OptionRegistry()

whitespace_re = re.compile(r"\n\s+", re.I | re.M)
def extract_doc(obj):
    _doc = getattr(obj, '__doc__', None)
    _doc = _doc.strip() if isinstance(_doc, str) else 'no doc.'
    _doc = whitespace_re.sub("\n", _doc)
    return _doc

def register_option(name, func):
    aggregation.register(name)(func)

def register_aggregator(name):
    def _register_aggregator(aggregator_class):
        return aggregators.register(name)(aggregator_class)

    return _register_aggregator

class _Aggregator:
    def __init__(self, key_extractor, value_extractor, parallelizable=False):
        self.key_extractor = key_extractor
        self.value_extractor = value_extractor
        self.tags = []
        self.is_parallelizable = parallelizable

    def consumerow(self, row):
        key = tuple((key_func(row) for key_func in self.key_extractor))
        value = tuple((value_func(row) for value_func in self.value_extractor))
        self.handlekeyvalue(key, value)

    # optional, you may as well override consumerow
    def handlekeyvalue(self, key, value):
        raise NotImplementedError()

    def stats(self):
        raise NotImplementedError()

    def finalize(self):
        pass

    def jobdigest(self):
        raise NotImplementedError()

    def consumejobdigest(self, job_result):
        raise NotImplementedError()

    @contextlib.contextmanager
    def cleanupcontext(self):
        '''
        Arkady: The idea is to return control to the decorator?
        '''
        yield

    def tag(self, tag):
        self.tags.append(tag)

    def parallelize(self):
        raise NotImplementedError()

    def collectparallel(self, parallel_digest):
        raise NotImplementedError()

    def paralleldigest(self):
        raise NotImplementedError()

    def postdeserialization(self):
        pass

    @property
    def parallelizable(self):
        return self.is_parallelizable

class _Transformation:
    def __init__(self, parallelizable):
        self.is_parallelizable = parallelizable
        self.argspec = None

    @property
    def parallelizable(self):
        return self.is_parallelizable

    def transform(self, aggregator):
        '''
        do some operation on the aggregator.
        '''
        raise NotImplementedError()

    def consumeargs(self, *args, **kwargs):
        '''
        append arguments list to the transformation's inner queue so they can be
        evaluated when transforming data later.
        '''
        pass

    def argsparallelized(self):
        '''
        when a transformation runs in a separate process and there's a rendezvous with the
        main process (either for a non-parallelizable transformation or the end of the transformations queue),
        the transformation instance in the main process hasn't been updated about which arguments were consumed.
        use this method to "pop" arguments that were consumed in a parallel process.
        '''
        pass

    def do_getargspec(self):
        '''
        should return a tuple of args dictionary and the variadic arguments name.
        the first return value (args) should be set to an ordered dict of arguments and their defaults. use None if there's no default.
        the second return value (variadic) names any variadic argument if present. use None if not present.
        '''
        raise NotImplementedError()

    def getargspec(self):
        '''
        get a decomposition of the arguments th transformation accepts
        '''
        if self.argspec is not None:
            return self.argspec

        args, variadic = self.do_getargspec()
        args = OrderedDict() if args is None else args

        assert isinstance(args, OrderedDict)
        assert variadic is None or isinstance(variadic, str)

        self.argspec = ArgSpec(args, variadic)
        return self.argspec

class _TransformationImpl(_Transformation):
    def __init__(self, func, arg_parser, parallelizable):
        super().__init__(parallelizable)
        self.func = func
        self.arg_parser = arg_parser

        self.args_queue = []

        self.__doc__ = extract_doc(func)

    def __call__(self):
        return self

    def transform(self, aggregator):
        args = self._popargs()
        if isinstance(args, Iterable) and not isinstance(args, str) and not isinstance(args, bytes) and not isinstance(args, dict):
            return self.func(aggregator, *args)
        else:
            return self.func(aggregator, args)

    def consumeargs(self, *args):
        self.args_queue.append(args)

    def _popargs(self):
        args = self.args_queue.pop(0) if len(self.args_queue) > 0 else tuple()
        if self.arg_parser is not None:
            args = self.arg_parser(*args)

        return args

    def argsparallelized(self):
        if len(self.args_queue) > 0:
            self.args_queue.pop(0)

    def do_getargspec(self):
        '''
        transformations defined via this class should be inspected for their arguments and have their format validated.
        we only support regular arguments with defaults and varidaic arguments (no keyword-only arguments, keyword arguments and such...).
        '''
        if self.arg_parser is not None:
            # inspect the argument parser for it's arguments
            full_arg_spec = inspect.getfullargspec(self.arg_parser)
            assert len(full_arg_spec.kwonlyargs) == 0, ValueError(f'keyword only args are not supported for transformations ({self.func})')
            assert full_arg_spec.varkw is None, ValueError(f'variadic keyword arguments are not supported for transformations ({self.func})')

            args = full_arg_spec.args
            defaults = full_arg_spec.defaults
            variadic = full_arg_spec.varargs
        else:
            # there's no dedicated argument parser so have to examine the actual aggregation factor function for extra arguments.
            # skip the first arg as it's supposed to be the input row.
            full_arg_spec = inspect.getfullargspec(self.func)
            assert len(full_arg_spec.kwonlyargs) == 0, ValueError(f'keyword only args are not supported for transformations ({self.func})')
            assert full_arg_spec.varkw is None, ValueError(f'variadic keyword arguments are not supported for transformations ({self.func})')

            defaults = full_arg_spec.defaults
            if defaults is not None and len(defaults) == len(full_arg_spec.args):
                defaults = defaults[1:] # the aggregator argument has a default, skip it
            args = full_arg_spec.args[1:]
            variadic = full_arg_spec.varargs

        defaults = tuple() if defaults is None else defaults
        assert len(defaults) <= len(args), ValueError('more defaults than args, this shouldn\'t happen', defaults, args)
        defaults = (None,)*(len(args) - len(defaults)) + defaults

        return OrderedDict(zip(args, defaults)), variadic

def make_transformation(transformation_name, arg_parser=None, parallelizable=True):
    def _make_transformation(func):
        data_transformations.register(transformation_name)(
            _TransformationImpl(
                func,
                arg_parser,
                parallelizable
            )
        )
        return func

    return _make_transformation

ArgSpec = recordclass.recordclass('ArgSpec', ('args', 'variadic'))

class _AggregationFactorFactory:
    def __init__(self):
        self.argspec = None

    def newfactor(self):
        raise NotImplementedError()

    def do_getargspec(self):
        '''
        should return a tuple of args dictionary and the variadic arguments name.
        the first return value (args) should be set to an ordered dict of arguments and their defaults. use None if there's no default.
        the second return value (variadic) names any variadic argument if present. use None if not present.
        '''
        raise NotImplementedError()

    def getargspec(self):
        '''
        get a decomposition of the arguments the aggregation factor accepts
        '''
        if self.argspec is not None:
            return self.argspec

        args, variadic = self.do_getargspec()
        args = OrderedDict() if args is None else args

        assert isinstance(args, OrderedDict)
        assert variadic is None or isinstance(variadic, str)

        self.argspec = ArgSpec(args, variadic)
        return self.argspec

class _AggregationFactor:
    def __init__(self):
        pass

    def __call__(self, row):
        return self.extract(row)

    def extract(self, row):
        raise NotImplementedError()

    def consumeargs(self, *args):
        pass

class ColumnAggregationFactor(_AggregationFactor):
    def __init__(self, column, col_type):
        super().__init__()
        self.column = column
        self.type = col_type
        self.__doc__ = f"the '{column}' column in the CSV"

    def __call__(self, row):
        return self.type(row[self.column])

    def consumerow(self, *args):
        assert len(args) == 0, ValueError(f"'{self.column}' aggregator doesn't accept arguments")

class ColumnAggregationFactorFactory(_AggregationFactorFactory):
    def __init__(self, column, col_type):
        super().__init__()
        self.column = column
        self.type = col_type
        self.__doc__ = f"the '{column}' column in the CSV"

    def do_getargspec(self):
        '''
        column aggregation elements accept nothing so this function should just be a stub.
        '''
        return None, None

    def newfactor(self):
        return ColumnAggregationFactor(self.column, self.type)

class _AggregationFactorImpl(_AggregationFactor):
    def __init__(self, func, args_parser):
        super().__init__()
        self.__doc__ = extract_doc(func)
        self.func = func
        self.args_parser = args_parser

        self.args = tuple()

    def extract(self, row):
        return self.func(row, *self.args)

    def consumeargs(self, *args):
        if self.args_parser is None:
            self.args = args
        else:
            self.args = self.args_parser(*args)

class _AggregationFactorImplFactory(_AggregationFactorFactory):
    def __init__(self, func, args_parser):
        super().__init__()
        self.__doc__ = extract_doc(func)
        self.func = func
        self.args_parser = args_parser

    def do_getargspec(self):
        '''
        aggregation factors defined via this class should be inspected for their arguments and have their format validated.
        we only support regular arguments with defaults and varidaic arguments (no keyword-only arguments, keyword arguments and such...).
        '''
        if self.args_parser is not None:
            # inspect the argument parser for it's arguments
            full_arg_spec = inspect.getfullargspec(self.args_parser)
            assert len(full_arg_spec.kwonlyargs) == 0, ValueError(f'keyword only args are not supported for aggregation factors ({self.func})')
            assert full_arg_spec.varkw is None, ValueError(f'variadic keyword arguments are not supported for aggregation factors ({self.func})')

            args = full_arg_spec.args
            defaults = full_arg_spec.defaults
            variadic = full_arg_spec.varargs
        else:
            # there's no dedicated argument parser so have to examine the actual aggregation factor function for extra arguments.
            # skip the first arg as it's supposed to be the input row.
            full_arg_spec = inspect.getfullargspec(self.func)
            assert len(full_arg_spec.kwonlyargs) == 0, ValueError(f'keyword only args are not supported for aggregation factors ({self.func})')
            assert full_arg_spec.varkw is None, ValueError(f'variadic keyword arguments are not supported for aggregation factors ({self.func})')

            defaults = full_arg_spec.defaults
            if defaults is not None and len(defaults) == len(full_arg_spec.args):
                defaults = defaults[1:]
            args = full_arg_spec.args[1:]
            variadic = full_arg_spec.varargs

        defaults = tuple() if defaults is None else defaults
        assert len(defaults) <= len(args), ValueError('more defaults than args, this shouldn\'t happen', defaults, aggregates)
        defaults = (None,)*(len(args) - len(defaults)) + defaults

        return OrderedDict(zip(args, defaults)), variadic

    def newfactor(self):
        return _AggregationFactorImpl(self.func, self.args_parser)

def register_aggregation_factor(name, args_parser = None):
    def _register_agg(func):
        aggregation.register(name)(_AggregationFactorImplFactory(func, args_parser))
        return func
    return _register_agg

make_aggregation_factor = register_aggregation_factor

def register_column_factor(column, col_type):
    aggregation.register(column)(ColumnAggregationFactorFactory(column, col_type))

class _Filter:
    def __init__(self):
        self.scheme = None

    def filter(self, row):
        raise NotImplementedError()

    def consumeargs(self, scheme, *args, **kwargs):
        pass

class _local_filter(_Filter):
    def __init__(self, positive, func, arg_parser):
        super().__init__()
        self.args = []
        self.positive = positive
        self.func = func
        self.arg_parser = arg_parser

    def __call__(self):
        return self

    def filter(self, row):
        result = self.func(row, self.scheme, *self.args)
        return result if self.positive else not result

    def consumeargs(self, scheme, *args):
        self.scheme = scheme
        if self.arg_parser is not None:
            self.args = self.arg_parser(scheme, *args)
        else:
            self.args = args

class _pos_filter(_local_filter):
    def __init__(self, func, arg_parser):
        super().__init__(True, func, arg_parser)
        self.__doc__ = extract_doc(func)

class _neg_filter(_local_filter):
    def __init__(self, func, arg_parser, filter_name):
        super().__init__(False, func, arg_parser)
        self.__doc__ = f"negative of '{filter_name}'"

def make_filter(filter_name, arg_parser = None):
    def _make_filter(func):
        filters.register(filter_name)(_pos_filter(func, arg_parser))
        filters.register("not-" + filter_name)(_neg_filter(func, arg_parser, filter_name))
        return func

    return _make_filter

class _Formatter:
    # should create a generator
    def format(self, aggregator):
        raise NotImplementedError
        yield

def make_formatter(name):
    def _make_formatter(func):
        @formatters.register(name)
        class __formatter(_Formatter):
            __doc__ = extract_doc(func)
            def format(self, aggregator):
                yield from func(aggregator)

        return func

    return _make_formatter

