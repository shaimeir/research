#!/usr/bin/env python3

import sys
import argparse
import contextlib
import csv
from collections import defaultdict

@contextlib.contextmanager
def stdout_context():
    yield sys.stdout

def load_csv(csv_file, columns, drop_cols):
    with open(csv_file, "rt") as _csv_file:
        csv_content = list(csv.reader(_csv_file, delimiter=',', quotechar='"'))

    csv_dict = defaultdict(lambda : [])
    col_count = len(csv_content[0])
    for csv_line in csv_content:
        key = tuple((csv_line[col] for col in columns))
        if drop_cols:
            csv_line = [csv_line[col] for col in range(col_count) if col not in columns]
        csv_dict[key].append(csv_line)

    return csv_dict, ((col_count - len(columns)) if drop_cols else col_count)

def main():
    parser = argparse.ArgumentParser(description="joins two CSVs like a naive SQL join operation.")
    parser.add_argument("-c", "--column", type=lambda c: int(c)-1, nargs=2, action='append', help="the columns in the input files to compare (left vs right)")
    parser.add_argument("-o", "--output", default="-", help="output file. use '-' for stdout.")
    parser.add_argument("left", help="left CSV")
    parser.add_argument("right", help="right CSV")

    args = parser.parse_args()

    left, _ = load_csv(args.left, [col[0] for col in args.column], False)
    right, right_col_count = load_csv(args.right, [col[1] for col in args.column], True)

    keys = list(left.keys())
    for key in keys:
        right_list = right.pop(key, [[None]*right_col_count])
        left_list = left[key]
        left[key] = sum([[left_line + right_line for left_line in left_list] for right_line in right_list], [])

    with (stdout_context() if '-' == args.output else open(args.output,"wt")) as output_file:
        csv_output = csv.writer(output_file, delimiter=',', quotechar='"', dialect="unix")
        for lines_batch in left.values():
            csv_output.writerows(lines_batch)

if __name__ == "__main__":
    main()

