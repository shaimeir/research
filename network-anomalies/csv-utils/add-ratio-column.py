#!/usr/bin/env python3

import logging
import csv
import argparse
import contextlib
import sys

@contextlib.contextmanager
def auto_yield(_file):
    yield _file

def main():
    parser = argparse.ArgumentParser(description="add a column that's the ratio between two columns in a CSV.")
    parser.add_argument("-d", "--denominator", required=True, type=int, help="the denominator column")
    parser.add_argument("-n", "--nominator", required=True, type=int, help="the nominator column")
    parser.add_argument("-o", "--output", default="-", help="output file. use '-' for stdout.")
    parser.add_argument("-v", "--verbose", action="store_true", help="show debug prints")
    parser.add_argument("csv", nargs='?', default='-', help="the CSV to modify. use '-' for stdin.")

    args = parser.parse_args()

    logging.basicConfig(level=logging.INFO if args.verbose else logging.WARN, format="%(asctime)s %(message)s")

    nominator = args.nominator - 1
    denominator = args.denominator - 1
    range_verified = False

    with (auto_yield(sys.stdin) if '-' == args.csv else open(args.csv, "rt")) as input_csv_file:
        with (auto_yield(sys.stdout) if '-' == args.output else open(args.output, "wt")) as output_csv_file:
            reader = csv.reader(input_csv_file, delimiter=',', quotechar='"')
            writer = csv.writer(output_csv_file, delimiter=',', dialect='unix', quotechar='"')

            for line in reader:
                if not range_verified:
                    assert 0 <= nominator < len(line)
                    assert 0 <= denominator < len(line)
                    range_verified = True

                col1 = line[nominator]
                col2 = line[denominator]

                try:
                    line += (float(col1)/float(col2),)
                except:
                    logging.info("can't devide %r/%r", nominator, denominator)
                    line += ("-1",)

                writer.writerow(line)

if __name__ == "__main__":
    main()

