#!/usr/bin/env python3

import logging
import sys
import pprint
import csv
import time
import contextlib
import datetime
import pathlib
import uuid
import os
import io
import subprocess
import recordclass

# Package multiprocess allows calling methods of objects
import multiprocess as multiprocessing
from collections import defaultdict

from csv_processing import csv_stats_argparse
from csv_processing import aggregation
from csv_processing import aggregators
from csv_processing import formatters
from csv_processing import spliced_file
from csv_processing.elements import aggregation_elements
from csv_processing.transformations import transformations
from csv_processing.filters import filters

# sys.modules['multiprocessing'] = sys.modules['multiprocess'] # for logging...

@contextlib.contextmanager
def stdout_context():
    yield sys.stdout

@contextlib.contextmanager
def stdin_context():
    yield sys.stdin

_ErrorCounter = recordclass.recordclass('_ErrorCounter', ('value', 'get_lock', ))
def ErrorCounter(value=0):
    @contextlib.contextmanager
    def _lock():
        yield

    return _ErrorCounter(value, _lock)

def safe_csv_iterator(reader, error_counter=None):
    error_count = 0
    with contextlib.suppress(StopIteration):
        while True:
            try:
                while True:
                    yield next(reader)
            except csv.Error as csv_error:
                logging.info("CSV Error: %r", csv_error)
                error_count += 1

    if error_counter is not None:
        with error_counter.get_lock():
            error_counter.value += error_count

    raise StopIteration()

def do_aggregate(csv_file, agg, tab_seperation, field_names, scheme, first, filters_queue, error_counter=None):
    '''
    Read the file line by line, apply filters?
    '''
    start_time = time.monotonic()
    line_counter = 0

    if first:
        csv_file.readline() # skip the header - it was already inferred

    reader = csv.DictReader(csv_file, delimiter='\t', quotechar='"', fieldnames=field_names) if tab_seperation else csv.DictReader(csv_file, delimiter=',', quotechar='"', fieldnames=field_names)
    shown_exceptions = set()
    for row in safe_csv_iterator(reader, error_counter):
        line_counter += 1
        if line_counter % 1000 == 0:
            elapsed_time = time.strftime("%H:%M:%S", time.gmtime(time.monotonic() - start_time))
            # Printing \r and skiping trailing \n (end='') is a nice trick to return to beginning of the same line
            print(f"\r{elapsed_time} parsed {line_counter} lines", end='', file=sys.stderr, flush=True)

        try:
            parsed_row = scheme.parse_row(row)
            if all((_filter.filter(parsed_row) for _filter in filters_queue)):
                agg.consumerow(parsed_row)
        except Exception as exc:
            logging.error("error handling row (%d): %r", line_counter, row, exc_info=(type(exc).__name__ not in shown_exceptions))
            shown_exceptions.add(type(exc).__name__)

    print(file=sys.stderr)
    logging.info("%s was consumed entirely (%d lines)", csv_file.name, line_counter)

    return agg

def job_identification(job):
    def _identified_job(job_id, *args, **kwargs):
        import multiprocessing
        multiprocessing.current_process().name = '{} [{}]'.format(str(getattr(job, '__name__', 'no name')), str(job_id))
        logging.error("started job %s - %s", str(job_id), str(getattr(job, '__name__', 'no name')))
        result = job(*args, **kwargs)
        logging.error("finished job %s - %s", str(job_id), str(getattr(job, '__name__', 'no name')))
        return result

    return _identified_job

TheErrorCounter = None

def init_subprocess(error_counter):
    '''
    Arkady: this function is required only in Windows (?). Linux forks the processes and global variables are visible
    '''
    global TheErrorCounter
    TheErrorCounter = error_counter

@job_identification
def aggregation_job(csv_file_name, start, end, agg, tab_seperation, field_names, scheme, first, filters_queue):
    '''
    Process a part of the CSV file specified by start/end
    '''
    logging.info("consuming %s...", csv_file_name)
    global TheErrorCounter  # This is a global and shared among processes variable
    with spliced_file.get_splice(csv_file_name, start, end) as csv_file:
        return do_aggregate(csv_file, agg, tab_seperation, field_names, scheme, first, filters_queue, TheErrorCounter).jobdigest()

def do_transform(aggregator, transformations, transformations_order):
    for aggregation_element in aggregator.aggregates:
        logging.info("transformation task handling: %r", aggregation_element)

    for transformation_name in transformations_order:
        transformation = transformations[transformation_name]
        logging.info("\tapplying %s", transformation_name)
        aggregator.tag(transformation_name)
        transformation.transform(aggregator)

@job_identification
def transformation_job(aggregator, transformations, transformations_order):
    logging.info("running post-deserialization routine on the aggregator")
    aggregator.postdeserialization()
    do_transform(aggregator, transformations, transformations_order)
    return aggregator.paralleldigest()

def is_parallelizable_file(filename):
    if filename == "-" or not pathlib.Path(filename).is_file():
        return False

    with open(filename, "r") as _f:
        return _f.seekable()

def main():
    '''
    150 lines of code which parse arguments, spawn processes, apply specified filters and transformations
    '''

    parser = csv_stats_argparse.CsvStatsArgparse(description="Process dumps generated by dump-network-info.py.")
    parser.add_scheme_argument("-s", "--scheme", help="the scheme to use when parsing the CSV file")
    parser.add_argument("-v", "--verbose", action="store_true", help="show debug logs.")
    parser.add_argument("-V", "--extra-verbose", action="store_true", help="show extra debug logs.")
    parser.add_by_element_argument("-b", "--by", help="values to aggregate by.")
    parser.add_by_element_args_argument("-y", "--barg", help="pass an argument to a 'by' aggregation factor. use the syntax: factor=arg1[,arg2,...]")
    parser.add_argument("-f", "--filter",
                        action="append",
                        default=[],
                        help="basic filtering.\nonly rows that pass the filter will be consumed by the aggregator.\nuse the format <filter name>=<value1>,<value2>,...\n" + aggregation.filters.doc())
    parser.add_element_argument("-a", "--aggregate", help="the values to aggregate.")
    parser.add_element_args_argument("-w", "--aarg", help="pass an argument to a value aggregation factor. use the syntax: factor=arg1[,arg2,...]")
    parser.add_transformation_argument("-t", "--transformation", help="transformation to use on the data.")
    parser.add_transformation_args_argument("-x", "--transformation-argument", help="arguments to transformations given in the format <transformation name>=<arg1>,<arg2>,...")
    parser.add_argument("-r", "--formatter", default="csv", choices=aggregation.formatters.choicesList(), help="the output format writer.\n" + aggregation.formatters.doc())
    parser.add_argument("-g", "--aggregator", default='dict', choices=aggregation.aggregators.choicesList(), help="aggregation class to use.\n" + aggregation.aggregators.doc())
    parser.add_argument("-o", "--output", default="-", help="output file. use '-' for stdout.")
    parser.add_argument("-z", "--timezone", default=None, help="the timezone to apply for the timestamp")
    parser.add_argument("-m", "--multi-processing", action='store_true', help="parallelize the CSV parsing (doesn't work on stdin).")
    parser.add_argument("--serial-transformation", action='store_true', help="don't parallelize transformations.")
    parser.add_argument("--parallelization", type=int, default=multiprocessing.cpu_count(), help="the number of parallel processes to run.")
    parser.add_argument("-c", "--comment", help="a command line comment. unused and discarded.")
    parser.add_argument("csv", nargs="*", default=['-'], help="CSV files to parse and aggregate")

    args = parser.parse_args()

    scheme = parser.scheme

    with contextlib.suppress(Exception):
        with open(pathlib.Path("~/.research-history").expanduser(), "at") as history:
            print(' '.join(sys.argv), file=history)

    # a workaround as someone accesses the logger in one of the imports before it's initialization....
    if logging.getLogger() is not None:
        logging.getLogger().handlers = []
    logging.basicConfig(level=logging.DEBUG if args.extra_verbose else (logging.INFO if args.verbose else logging.WARN), format="%(asctime)s %(processName)s [%(process)d] %(message)s")

    logging.info("command line decomposition: %r", args)

    # The end result is a list of objects of type csv_processing.aggregation._AggregationFactorImpl
    # aggregation.aggregation is a global variable of type option_registry_util.OptionRegistry
    # OptionRegistry is a dictionary keeping a bunch of decorators. Tip: search for calls to function aggregation.make_transformation()
    aggregate_by = [aggregation.aggregation.option(value_extractor).newfactor() for value_extractor in args.by]
    aggregate_values = [aggregation.aggregation.option(value_extractor).newfactor() for value_extractor in args.aggregate]

    # Get all registered functions - functions with decorator like @aggregation ...
    # Here I am trying to figure out all -a (aggregate by index, for example fs-canonize-with-ext-dir)
    # and -b (events type like fs-access-category) parameters
    aggregation_choices_list = aggregation.aggregation.choicesList()
    for args_list, aggregates, names in ((args.barg, aggregate_by, args.by), (args.aarg, aggregate_values, args.aggregate)):
        aggregation_args = defaultdict(lambda : [])
        for factor_arg in args_list:
            factor, arg = tuple(factor_arg.split("=",1))
            arg = arg.split(",")
            assert factor in aggregation_choices_list
            aggregation_args[factor].append(arg)

        for index in range(len(aggregates)):
            aggregate = aggregates[index]
            name = names[index]

            agg_args = [] if len(aggregation_args[name]) == 0 else aggregation_args[name].pop(0)
            aggregate.consumeargs(*agg_args)

    # List of transformaitons - -t switch - csv-to-column, boolean-entries, etc
    transformations = {transformation : aggregation.data_transformations.option(transformation)() for transformation in args.transformation}
    formatter = aggregation.formatters.option(args.formatter)()

    if args.multi_processing:
        logging.info("using multiprocessing - may god help us")
        multiprocessing.set_start_method('fork')

    filters_queue = []
    # Parse parameters of -f switch. Filters are methods range_filter, filter_process_name, etc
    for filter_args in args.filter:
        filter_name, filt_args = tuple(filter_args.split("=", 1))
        _filter = aggregation.filters.option(filter_name)()
        # call parse_args() for the parameters - list of filters to apply to the data
        _filter.consumeargs(scheme, *(tuple() if filt_args.strip() == '' else filt_args.strip().split(",")))
        filters_queue.append(_filter)

    # Fetch all -t (transformations) parameters, parse them
    for arg_entry in args.transformation_argument:
        transformation, arguments = tuple(arg_entry.split("=", 1))
        transformations[transformation].consumeargs(*(arguments.split(",") if len(arguments) > 0 else tuple()))

    # Switch -g is not in use?
    agg = aggregation.aggregators.option(args.aggregator)(aggregate_by, aggregate_values)

    # will print something like: ./csv-stats.py -m -v --serial-transformation -s file-system ...
    logging.info("command line: %s", ' '.join(sys.argv))

    line_counter = 0
    # In Python 3 'end' allows to modify the trailing CR
    print("starting parsing", end=' ', file=sys.stderr, flush=True)

    # This variable is not used?
    start_time = time.monotonic()

    field_names = None

    # Arkady: cleanupcontext will sometimes do a useful job, like removing data from the Redis
    # class _RedisAggregatorBase()
    # The idea here is to call "finally" when "with" exits
    with agg.cleanupcontext():
        for csv_file_name in args.csv:
            if field_names is None:  # TODO Arkady: inferring of columns from the CSV heading can move to a separate function
                logging.info("inferring header")
                with (stdin_context() if csv_file_name == '-' else open(csv_file_name, "rt")) as csv_file:
                    line = csv_file.readline()
                    tab_separation = '\t' in line
                    csv_mock_file = io.StringIO(line)
                    field_names = (csv.DictReader(csv_mock_file, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL) if tab_separation else csv.DictReader(csv_mock_file, delimiter=',', quotechar='"')).fieldnames

                # Will print something like "fields are: 'process, command, timestmap, access_type, host, instance, process_id, file, old_file, remote_host, username'"
                logging.info("fields are: %r", ', '.join(field_names))

            logging.info("consuming %s...", csv_file_name)
            # Apply filters first - TODO divide into functions
            if args.multi_processing and is_parallelizable_file(csv_file_name):
                error_counter = multiprocessing.Value('i') # all forked processes will see this shared, global variable of integral type.
                with multiprocessing.Pool(args.parallelization, initializer=init_subprocess, initargs=(error_counter,)) as pool: # Arkady: 'with' here is not strictly required
                    # List of the results returned by jobs
                    async_results = []

                    logging.info("dealing out jobs")
                    first = True
                    for start, end in spliced_file.generate_splices(csv_file_name, args.parallelization):
                        job_id = uuid.uuid4()
                        logging.info("creating aggregation job %s", job_id)
                        async_results.append(pool.apply_async(aggregation_job, (job_id, csv_file_name, start, end, agg, tab_separation, field_names, scheme, first, filters_queue)))
                        first = False

                    pool.close()
                    pool.join()

                    logging.info("consuming results")
                    for res in async_results:
                        agg.consumejobdigest(res.get())
            else:
                    logging.info("consuming %s...", csv_file_name)
                    error_counter = ErrorCounter(0)
                    with (stdin_context() if csv_file_name == '-' else open(csv_file_name, "rt")) as csv_file:
                        do_aggregate(csv_file, agg, tab_separation, field_names, scheme, True, filters_queue, error_counter)

            logging.info("failed to parse %d lines", error_counter.value)
            logging.info("%s was consumed entirely (%d lines)", csv_file_name, line_counter)

        logging.info("finalizing aggregation")
        agg.finalize()

    logging.info("applying transformations")
    if args.multi_processing and agg.parallelizable and not args.serial_transformation and len(agg.aggregates) > 1:
        args.transformation.append("stop-parallelization")
        transformations["stop-parallelization"] = aggregation.data_transformations.option("stop-parallelization")()

        parallelization_queue = []
        for transformation_name in args.transformation:
            transformation = transformations[transformation_name]
            if transformation.parallelizable:
                parallelization_queue.append(transformation_name)
                continue

            _transformation_name = transformation_name # keep it for later

            # a serial transformation.
            # we need to execute and collect all the results
            if len(parallelization_queue) > 0:
                with multiprocessing.Pool(args.parallelization) as pool:
                    async_results = []

                    logging.info("parallelizing: %s", ', '.join(parallelization_queue))
                    for sub_aggregator in agg.parallelize(args.parallelization):
                        job_id = uuid.uuid4()
                        logging.info("creating transformation job %s", job_id)
                        async_results.append(pool.apply_async(transformation_job, (job_id, sub_aggregator, transformations, parallelization_queue)))

                    pool.close()
                    pool.join()

                    logging.info("consuming results for %s", ', '.join(parallelization_queue))
                    for res in async_results:
                        agg.collectparallel(res.get())

                for transformation_name in parallelization_queue:
                    transformations[transformation_name].argsparallelized()

                parallelization_queue = []

            do_transform(agg, transformations, [_transformation_name]) # run the synchronous transformation

    else:
        if args.multi_processing:
            logging.info("can't parallelize '%s' type aggregator - transforming data in a serial fashion", args.aggregator)
        do_transform(agg, transformations, args.transformation)

    logging.info("done!")
    with (stdout_context() if args.output == "-" else open(args.output, "wt")) as output_file:
        output_file.writelines((line + "\n" for line in formatter.format(agg)))

    if args.output != "-":
        with contextlib.suppress(Exception):
            os.chmod(args.output, stat.S_IRUSR | stat.S_IRGRP | stat.S_IROTH)

if __name__ == "__main__":
    sys.exit(main())

