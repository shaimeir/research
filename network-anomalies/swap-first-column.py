#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Swaps the first column of the specified CSV file with the specified column

This is roughly the same as
awk -F $'\t' ' { t = $1; $1 = $2; $2 = t; print; } ' OFS=$'\t'
but with error handling

O(lines in file) complexity
Single core 1M lines in ~6s

Usage:
  swap-first-column.py -h | --help
  swap-first-column.py  -i <FILE> -c <NUM> [-d <DELIMITER>] [-o <FILE>]

Options:
  -h --help                      Show this screen.
  -i --infile=<FILE>             File to process, can be a colon separated list of files
  -o --outfile=<FILE>            A single file to generate, default is to append "swap" to the input filename(s)
  -c --column=<NUM>              HostID column in the CSV file, can be a name of the column

Example:
  ./swap-first-column.py -i ~/Downloads/payoneer-process-creation-0-14.09.csv -c 4
"""

import logging
import csv
import sys
import os
from docopt import docopt
import pickle
import multiprocessing
import pandas
import time
import re
import math


def process_file(logger, filename, out_filename_config, column_idx):
    line_index = 0
    with open(filename, "rt") as infile:
        reader = csv.reader(infile, delimiter=delimiter)
        if out_filename_config["single_file"]:
            out_filename = out_filename_config["filename"]
        else:
            out_filename = filename + out_filename_config["suffix"]
        logger.info(f"Create {out_filename}")

        csv_writer = csv.writer(open(out_filename, "wt"), delimiter=delimiter)

        resume = True
        print_lines_timestamp = time.time()
        start_time = time.time()

        while resume:
            resume = False
            try:
                #reader = pandas.read_csv(filename)
                for row in reader:
                    row[0], row[column_idx] = row[column_idx], row[0]
                    csv_writer.writerow(row)
                    line_index += 1
                    if logger.level == logging.DEBUG and (time.time() - print_lines_timestamp) > 0.2:
                        print_lines_timestamp = time.time()
                        elapsed_time = math.ceil(print_lines_timestamp - start_time)
                        if elapsed_time:
                            rate = (line_index//elapsed_time)//1000
                            line_index_str = "{:,}".format(line_index)
                            print(f"\r{line_index_str} lines, {rate} Klines/s", end='', file=sys.stderr, flush=True)
            except UnicodeDecodeError:
                resume = True
                logger.debug("UnicodeDecodeError in the line {0} skipped".format(line_index))
            except Exception:
                logger.debug("Exception in the line {0} '{1}'".format(line_index, row), exc_info=True)
                resume = True

def main():
    logging.basicConfig()
    global logger
    logger = logging.getLogger('swap-first-column')
    logger.setLevel("DEBUG")
    arguments = docopt(__doc__, version='swap-first-column')
    global delimiter
    delimiter = "\t"

    in_filenames = arguments["--infile"].split(":")

    out_filename = arguments["--outfile"]
    if not out_filename:
        out_filename = {"single_file":False, "suffix":".swap"}
    else:
        out_filename = {"single_file":True, "filename":out_filename}
    column_idx = int(arguments["--column"])

    home_folder = os.path.expanduser("~")
    for filename in in_filenames:
        process_file(logger, filename, out_filename, column_idx)

if __name__ == '__main__':
    sys.exit(main())
