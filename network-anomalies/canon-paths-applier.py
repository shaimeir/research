#!/usr/bin/env python3
'''
this script deals with a few semi-related issues. it basically had one purpose and sort of grew a bit out of it's designated proportions.
it's functionality dependes on the command line action argument. it's options ad functionality are as follows:

    1. 'compile' - take a profile as it was output from csv-stats.py using a recipe or some command line and transform it into
                   a JSON file of standard representation. this feature was concieved as an optimization for the application of a file system
                   profile on given input list of files. the problem was that loading a large profile and building the appropriate regexes
                   from it and then merging them together took a long time and the compiled form was basically a JSON with the regexes and
                   a really inefficient tree representation, but it worked. currently this gives normalized representation facility.

    2. 'apply' - take a list of compiled profiles and apply them to an input stream. application meaning that the input stream
                 is a CSV with it's lines containing the information to decide which profile to pick for that particular line (e.g. explorer.exe/file modification),
                 a column to compare against the profile and some unrelated columns (host id, instance id, process id and the line prevalence usually),
                 and the output is the same line (minus the prevalence column) with an added column that tells whether or not the line was matched with the profile.
                 application can now take a recipe and using it's cross-validation scheme assume the input column order and use it to infer which profile to use
                 (by checking the elements marked as 'key' for cross validation) for comparison.

    3. 'diff' - takes many input CSV files that contain lines to compare against a given profile (usually separated in a file-per-process-instance fashion)
                and generate for each of them a JSON describing where the file matched/didn't match with the profile.
                these output files can be rendered using render-diff-tree.py.

    4. 'apply-contained' - a sort of an inverse version of the 'apply' switch. checks which paths in the profile weren't matched by the input stream and
                           writes them in the same format that 'apply' outputs (e.g. for file system profile the lines generated will be the lines
                           on the input stream with the file removed and a path from the profile put in it's stead).
'''

import csv
import argparse
import sys
import re
import logging
import contextlib
import pathlib
import os
import json
import ipaddress
import recordclass
from collections import defaultdict

from canonization import canon_paths_application
from canonization import matchable_nodes
from csv_processing import profile_recipe
from utilities import anytree_helper

@contextlib.contextmanager
def auto_context(obj):
    yield obj

def column_index(value):
    value = int(value)
    assert value > 0
    return value - 1

def load_paths_from_csv(csv_file, keys, canon_path_col):
    paths = defaultdict(lambda : set())
    reader = csv.reader(csv_file, delimiter=',', quotechar='"')

    for row in reader:
        paths[tuple((row[col] for col in keys))].add(row[canon_path_col])

    return paths

def _compile_main(args):
    type_suite = canon_paths_application.get_profile_type_suite(args.type)

    logging.info("loading canon paths")
    canon_paths = defaultdict(lambda : set())
    for csv_file_name in args.csv:
        logging.info("loading %s...", csv_file_name)

        with (auto_context(sys.stdin) if "-" == csv_file_name else open(csv_file_name, "rt")) as csv_file:
            _loaded_canon = load_paths_from_csv(csv_file, args.key, args.column)

        for key, canon in _loaded_canon.items():
            canon_paths[key].update(canon)

        logging.info("%s loaded", csv_file_name)

    logging.info("canon paths loaded")

    logging.info("generating profile")
    canon_profiles = []
    for key, paths in canon_paths.items():
        profile_tree = anytree_helper.paths_to_tree(paths, root_name='', node_class=type_suite.node_class, path_parser=type_suite.path_parser())

        profile_representation = {
            'key' : key,
            'type' : type_suite.name,
            'class' : args.profile_class,
            'profile' : type_suite.serializer().to_json_form(profile_tree),
        }
        canon_profiles.append(profile_representation)

        logging.info("%r: done", key)

    logging.info("profiles compiled")
    with (auto_context(sys.stdout) if "-" == args.output else open(args.output, "wt")) as output_stream:
        json.dump({'profiles' : canon_profiles}, output_stream, indent=4)

def _compile_by_recipe_main(args):
    # just fix up args a bit with the missing fields inferred from the recipe and pass it to _compile_main
    logging.info("loading recipe from %s", args.recipe)
    recipe = profile_recipe.load_recipe_from_json(args.recipe)
    if args.sub_recipe is not None:
        logging.info("using sub-recipe %s", args.sub_recipe)
        recipe = recipe.get_subrecipe(args.sub_recipe)

    keys = []
    values = []

    index = 0

    if args.type is None:
        args.type = recipe.profile_type

    if args.type is not None and recipe.profile_type is not None:
        assert args.type == recipe.profile_type, ValueError('conflict between type given on the command line and the one in the recipe', args.type, recipe.profile_type)

    assert args.type is not None, ValueError('missing the profile type')

    # first look at the by-elements as they should appear first in a correct input
    for by_element_id, by_element_profile_tag in recipe.profile_by_elements.items():
        logging.info("by-element: %s (column %d) is %r", by_element_id, index, by_element_profile_tag)

        if by_element_profile_tag == profile_recipe.ProfileTag.key:
            keys.append(index)
        elif by_element_profile_tag == profile_recipe.ProfileTag.value:
            values.append(index)

        # since these arrays should hold column 0-based indices advance the index after assigning it
        index += 1

    # now we can look at the elements
    for element_id, element_profile_tag in recipe.profile_elements.items():
        logging.info("element: %s (column %d) is %r", element_id, index, element_profile_tag)

        if element_profile_tag == profile_recipe.ProfileTag.key:
            keys.append(index)
        elif element_profile_tag == profile_recipe.ProfileTag.value:
            values.append(index)

        # since these arrays should hold column 0-based indices advance the index after assigning it
        index += 1

    # make sure we only got one value
    assert len(values) == 1, ValueError('the recipe has a bad number of fields marked as cross-validation values (only 1 is supported)', values)

    # augment args with the new information
    args.key = keys
    args.column = values[0]

    # and carry on my wayward son
    logging.debug("passing loaded arguments to _compile_main(): %r", args)
    return _compile_main(args)

_MatchCounter = recordclass.recordclass('_MatchCounter', ('matched', 'unmatched'))

def _apply_main(args):
    logging.debug("_apply_main() called with %r", args)

    type_suite = canon_paths_application.get_profile_type_suite(args.type)
    path_parser = type_suite.path_parser()
    serializer = type_suite.serializer()
    node_class = type_suite.node_class

    logging.info("loading compiled profiles from: %s", ", ".join(args.profile))
    profiles = defaultdict(lambda : [])
    for profile_file_name in args.profile:
        with (auto_context(sys.stdin) if '-' == profile_file_name else open(profile_file_name, 'rt')) as profile_file:
            _profiles = json.load(profile_file)

        for profile_record in _profiles['profiles']:
            profile_type = profile_record['type']
            if profile_type != args.type:
                logging.info("skipping %r/%r", profile_type)
                continue

            key = tuple(profile_record['key'])
            assert 'profile' in profile_record
            profile_record['tree'] = None

            profiles[key].append(profile_record)

    if args.verbose:
        logging.info("loaded keys:")
        for key in profiles:
            logging.info("loaded key: %r", key)

    match_counters = defaultdict(lambda : _MatchCounter(0,0))
    unavailable_keys = set()

    with (auto_context(sys.stdout) if "-" == args.output else open(args.output, "wt")) as output_stream:
        output_csv = csv.writer(output_stream, delimiter=',', quotechar='"', dialect='unix')

        logging.info("processing input CSVs")
        for csv_file_name in args.csv:
            file_prefix = (csv_file_name,) if args.file else tuple()
            logging.info("processing %s", csv_file_name)
            try:
                with (auto_context(sys.stdin) if "-" == csv_file_name else open(csv_file_name, "rt")) as csv_file:
                    reader = csv.reader(csv_file, delimiter=',', quotechar='"')
                    for row in reader:
                        key = tuple((str(row[key_index]) for key_index in args.key))
                        profile_records = profiles.get(key, None)
                        if profile_records is None:
                            if key not in unavailable_keys:
                                unavailable_keys.add(key)
                                logging.info("no profiles supplied for key %r", key)
                            continue

                        tag = tuple((str(row[tag_index]) for tag_index in args.tag))

                        path = row[args.column]
                        path_parts = tuple(path_parser.deconstruct_sample(path))
                        matched = False
                        for profile_record in profile_records:
                            if profile_record['tree'] is None:
                                profile_record['tree'] = serializer.from_json_form(profile_record['profile'], node_class)

                            matched = canon_paths_application.is_path_in_tree(profile_record['tree'], path_parts)[0]
                            if matched:
                                break

                        if not args.brief and not (matched and args.unmatched_only):
                            output_csv.writerow(file_prefix + key + tag + (path, 'matched' if matched else 'unmatched'))

                        if matched:
                            match_counters[key].matched += 1
                        else:
                            match_counters[key].unmatched += 1

            except:
                logging.warn("an error while handling %s", csv_file_name, exc_info=True)

            else:
                logging.info("finished %s", csv_file_name)

        if args.brief:
            for key, counter in match_counters.items():
                output_csv.writerow(key + (counter.matched, counter.unmatched, counter.matched/(counter.matched + counter.unmatched), counter.unmatched/(counter.matched + counter.unmatched)))

    logging.info("number of unavailable keys: %d", len(unavailable_keys))
    for key, counter in match_counters.items():
        logging.info("%r: %d matched (%f%%) and %d unmatched (%f%%)", key, counter.matched, counter.matched/(counter.matched + counter.unmatched), counter.unmatched, counter.unmatched/(counter.matched + counter.unmatched))

    logging.info("done!")

def _apply_by_recipe_main(args):
    # we'll just load the information from the recipe, augment the arguments with proper key, column and tag fields and just pass it on to _apply_main
    logging.info("loading recipe from %s", args.recipe)
    recipe = profile_recipe.load_recipe_from_json(args.recipe)
    if args.sub_recipe is not None:
        logging.info("using sub-recipe %s", args.sub_recipe)
        recipe = recipe.get_subrecipe(args.sub_recipe)

    keys = []
    tags = []
    values = []

    index = 0

    if args.type is None:
        args.type = recipe.profile_type

    if args.type is not None and recipe.profile_type is not None:
        assert args.type == recipe.profile_type, ValueError('conflict between type given on the command line and the one in the recipe', args.type, recipe.profile_type)

    assert args.type is not None, ValueError('missing the profile type')

    # first look at the by-elements as they should appear first in a correct input
    for by_element_id, by_element_cv_tag in recipe.cross_validation_by_elements.items():
        logging.info("by-element: %s (column %d) is %r", by_element_id, index, by_element_cv_tag)

        if by_element_cv_tag == profile_recipe.CrossValidationTag.key:
            keys.append(index)
        elif by_element_cv_tag == profile_recipe.CrossValidationTag.value:
            values.append(index)
        elif by_element_cv_tag == profile_recipe.CrossValidationTag.tag:
            tags.append(index)

        # since these arrays should hold column 0-based indices advance the index after assigning it
        index += 1

    # now we can look at the elements
    for element_id, element_cv_tag in recipe.cross_validation_elements.items():
        logging.info("element: %s (column %d) is %r", element_id, index, element_cv_tag)

        if element_cv_tag == profile_recipe.CrossValidationTag.key:
            keys.append(index)
        elif element_cv_tag == profile_recipe.CrossValidationTag.value:
            values.append(index)
        elif element_cv_tag == profile_recipe.CrossValidationTag.tag:
            tags.append(index)

        # since these arrays should hold column 0-based indices advance the index after assigning it
        index += 1

    # make sure we only got one value
    assert len(values) == 1, ValueError('the recipe has a bad number of fields marked as cross-validation values (only 1 is allowed)', values)

    # augment args with the new information
    args.key = keys
    args.tag = tags
    args.column = values[0]

    # and carry on my wayward son
    logging.debug("passing loaded arguments to _apply_main(): %r", args)
    return _apply_main(args)

def _apply_contained_main(args):
    type_suite = canon_paths_application.get_profile_type_suite(args.type)
    path_parser = type_suite.path_parser()
    serializer = type_suite.serializer()
    node_class = type_suite.node_class

    logging.info("loading compiled profiles from: %s", ", ".join(args.profile))
    profiles = {}
    for profile_file_name in args.profile:
        with (auto_context(sys.stdin) if '-' == profile_file_name else open(profile_file_name, 'rt')) as profile_file:
            _profiles = json.load(profile_file)

        for profile_record in _profiles['profiles']:
            profile_type = profile_record['type']
            if profile_type != args.type:
                logging.info("skipping %r/%r", profile_type)
                continue

            key = tuple(profile_record['key'])
            assert 'profile' in profile_record
            profile_record['tree'] = None

            if key in profiles:
                logging.warn("multiple profiles for key %r", key)
            profiles[key] = profile_record

    if args.verbose:
        logging.info("loaded keys:")
        for key in profiles:
            logging.info("loaded key: %r", key)

    match_counters = defaultdict(lambda : _MatchCounter(0,0))
    unavailable_keys = set()

    logging.info("processing input CSVs")
    available_tags = defaultdict(set)
    for csv_file_name in args.csv:
        file_prefix = (csv_file_name,) if args.file else tuple()
        logging.info("processing %s", csv_file_name)
        try:
            with (auto_context(sys.stdin) if "-" == csv_file_name else open(csv_file_name, "rt")) as csv_file:
                reader = csv.reader(csv_file, delimiter=',', quotechar='"')
                for row in reader:
                    key = tuple((str(row[key_index]) for key_index in args.key))
                    profile_record = profiles.get(key, None)
                    if profile_record is None:
                        if key not in unavailable_keys:
                            unavailable_keys.add(key)
                            logging.info("no profiles supplied for key %r", key)
                        continue

                    tag = tuple((str(row[tag_index]) for tag_index in args.tag))
                    available_tags[key].add(tag)

                    path = row[args.column]
                    path_parts = tuple(path_parser.deconstruct_sample(path))

                    if profile_record['tree'] is None:
                        # giving an empty set in the visited argument at this point will cause all the nodes in the tree
                        # to point to the same set so update to one node will update all of them.
                        # this should be done on demand.
                        profile_record['tree'] = serializer.from_json_form(profile_record['profile'], node_class, visited=None)

                    matched, nodes = canon_paths_application.is_path_in_tree(profile_record['tree'], path_parts)
                    if matched:
                        for node in nodes:
                            for n in node.path:
                                if n.visited is None:
                                    n.visited = {tag}
                                else:
                                    n.visited.add(tag)

                    if matched:
                        match_counters[key].matched += 1
                    else:
                        match_counters[key].unmatched += 1

        except:
            logging.warn("an error while handling %s", csv_file_name, exc_info=True)

        else:
            logging.info("finished %s", csv_file_name)

    logging.info("removing unused profiles")
    profiles = {
        key : profile_record
        for key, profile_record in profiles.items()
        if profile_record['tree'] is not None
    }

    with (auto_context(sys.stdout) if "-" == args.output else open(args.output, "wt")) as output_stream:
        output_csv = csv.writer(output_stream, delimiter=',', quotechar='"', dialect='unix')

        for key, profile_record in profiles.items():
            _available_tags = available_tags[key]

            for leaf in anytree_helper.get_tree_leaves(profile_record['tree']):
                leaf_path = path_parser.reconstruct(leaf)

                visited_set = leaf.visited
                if visited_set is None:
                    visited_set = set()
                unvisited_set = _available_tags.difference(visited_set)

                if not args.unmatched_only:
                    for tag in visited_set:
                        output_csv.writerow(file_prefix + key + tag + (leaf_path, 'matched'))

                for tag in unvisited_set:
                    output_csv.writerow(file_prefix + key + tag + (leaf_path, 'unmatched'))

    logging.info("number of unavailable keys: %d", len(unavailable_keys))
    for key, counter in match_counters.items():
        logging.info("%r: %d matched (%f%%) and %d unmatched (%f%%)", key, counter.matched, counter.matched/(counter.matched + counter.unmatched), counter.unmatched, counter.unmatched/(counter.matched + counter.unmatched))

    logging.info("done!")

def _merge_tree(profile_root, instance_root, tag_overlap, is_profile):
    if not profile_root.match(instance_root):
        logging.warn("root mismatch between %s and %s", profile_root, instance_root)
        instance_root.parent = profile_root
        return profile_root

    queue = [(profile_root, instance_root)]
    while len(queue) > 0:
        p_node, i_node = queue.pop(0)
        tag_overlap(p_node)

        p_children = list((p_child for p_child in p_node.children if is_profile(p_child)))
        i_children = list(i_node.children)
        for i_child in i_children:
            subdir_trap_node = None
            file_trap_node = None
            next_p_node = None

            for p_child in p_children:
                if p_child.match(i_child):
                    if p_child.is_subdir_trap:
                        subdir_trap_node = p_child
                    elif p_child.is_file_trap:
                        file_trap_node = p_child
                    else:
                        next_p_node = p_child
                        break

            if next_p_node is not None:
                queue.append((next_p_node, i_child))

            elif subdir_trap_node is not None and not i_child.is_leaf:
                tag_overlap(subdir_trap_node)

                i_child.parent = subdir_trap_node
                overlap_queue = [i_child]

                while len(overlap_queue) > 0:
                    overlapped_node = overlap_queue.pop(0)
                    tag_overlap(overlapped_node)
                    overlap_queue += list(overlapped_node.children)

            elif file_trap_node is not None and i_child.is_leaf:
                i_child.parent = file_trap_node
                tag_overlap(file_trap_node)
                tag_overlap(i_child)

            else:
                i_child.parent = p_node

    return profile_root

def _clear_non_profile(merged_tree, is_profile, fix_overlap):
    queue = [merged_tree]
    while len(queue) > 0:
        node = queue.pop(0)

        if is_profile(node):
            fix_overlap(node)
            queue += list(node.children)
        else:
            node.parent = None

def _fix_profile_overlap(node):
    node.extra_content = {'type' : 'comparison', 'tag' : 'profile'}

def _tag_overlap(node):
    node.extra_content['tag'] = 'overlap'

def _diff_main(args):
    assert args.output == '-' or (len(args.csv) > 1 and pathlib.Path(args.output).is_dir()) or args.single_file

    type_suite = canon_paths_application.get_profile_type_suite(args.type)
    path_parser = type_suite.path_parser()
    serializer = type_suite.serializer()
    node_class = type_suite.node_class

    logging.info("loading compiled profiles from: %s", ", ".join(args.profile))
    profiles = {}
    for profile_file_name in args.profile:
        with (auto_context(sys.stdin) if '-' == profile_file_name else open(profile_file_name, 'rt')) as profile_file:
            _profiles = json.load(profile_file)

        for profile_record in _profiles['profiles']:
            profile_type = profile_record['type']
            if profile_type != args.type:
                continue

            key = tuple(profile_record['key'])
            assert 'profile' in profile_record
            profile_record['tree'] = None

            if key in profiles:
                logging.warn("overriding key %r in profiles", key)
            profiles[key] = profile_record

    if args.verbose:
        for key in profiles:
            logging.info("loaded key: %r", key)

    match_counters = defaultdict(lambda : _MatchCounter(0,0))
    unavailable_keys = set()

    single_file = args.single_file or (args.output == '-') # or (len(args.csv) == 1)
    logging.info("single file mode = %r", single_file)
    output_dir = None if single_file else pathlib.Path(args.output)
    single_file_diffs = []

    for csv_file_name in args.csv:
        logging.info("processing %s", csv_file_name)
        try:
            with (auto_context(sys.stdin) if "-" == csv_file_name else open(csv_file_name, "rt")) as csv_file:
                csv_paths = load_paths_from_csv(csv_file, args.key, args.column)

            csv_trees = {}
            for key, paths in csv_paths.items():
                logging.info("handling key %r for %s", key, csv_file_name)
                tree_root = matchable_nodes.MatchableNode(str(key), extra_content={'type' : 'comparison', 'tag' : 'instance'})
                tree = anytree_helper.paths_to_tree(paths, root_name='', parent=tree_root, path_parser=path_parser.deconstruct_sample, node_class=matchable_nodes.MatchableNode, extra_content={'type' : 'comparison', 'tag' : 'instance'})

                profile = profiles.get(key, None)
                if profile is not None:
                    if profile['tree'] is None:
                        profile_tree = serializer.from_json_form(profile['profile'], node_class)
                        profile_root = matchable_nodes.MatchableNode(str(key), extra_content={'type' : 'comparison', 'tag' : 'profile'})
                        profile_tree.parent = profile_root
                        profile['tree'] = profile_root
                        _clear_non_profile(profile['tree'], lambda node: True, _fix_profile_overlap)

                    merged_tree = _merge_tree(profile['tree'], tree_root, _tag_overlap, lambda node: (node.extra_content['tag'] != 'instance'))

                elif not args.skip_no_profile:
                    merged_tree = tree_root

                else:
                    continue

                output_path = output_dir
                serialized_tree = serializer.to_json_form(merged_tree)
                if single_file:
                    single_file_diffs.append(serialized_tree)
                else:
                    output_path = output_dir/((pathlib.Path('stdin' if '-' == csv_file_name else csv_file_name).name) + '.json')
                    logging.info("writing diff to %s", output_path)
                    with open(output_path, "wt") as output_file:
                        json.dump({'diffs' : [serialized_tree]}, output_file, indent=4)

                _clear_non_profile(merged_tree, lambda node: (node.extra_content['tag'] != 'instance'), _fix_profile_overlap)

        except Exception:
            logging.warn("an error while handling %s", csv_file_name, exc_info=True)

        else:
            logging.info("finished %s", csv_file_name)

    if single_file:
        logging.info("writing all diffs to a single file - %s", args.output)
        with (auto_context(sys.stdout) if args.output == "-" else open(args.output, "wt")) as output_file:
            json.dump({'diffs' : single_file_diffs}, output_file, indent=4)

    logging.info("done!")

def main():
    parser = argparse.ArgumentParser(description="generate apply and test regexes from canonized paths list.")
    parser.add_argument("-V", "--extra-verbose", action="store_true", help='show extra debug prints.')
    parser.add_argument("-v", "--verbose", action="store_true", help='show debug prints.')
    parser.add_argument("-o", "--output", default="-", help="output file. '-' for stdout.")

    subparsers = parser.add_subparsers(dest='action')

    compile_parser = subparsers.add_parser('compile', help="generate profile matching patterns from canonized paths templates.")
    compile_parser.add_argument("-k", "--key", required=True, type=column_index, action='append', help='the key columns.')
    compile_parser.add_argument("-c", "--column", required=True, type=column_index, help='the canon paths columns.')
    compile_parser.add_argument("-t", "--type", required=True, choices=canon_paths_application.get_supported_profile_types(), help="the profile type.")
    compile_parser.add_argument("-l", "--class", dest='profile_class', default="relaxed", choices=["relaxed", "strict"], help="a tag to add to the profile. 'relaxed' means it contains legitimate possibilities and 'strict' means it contains paths that should mostly be present.")
    compile_parser.add_argument("csv", nargs="*", default=["-"], help="the CSVs containing the regex patterns. '-' for stdin.")

    compile_by_recipe_parser = subparsers.add_parser('compile-by-recipe', help="generate profile matching patterns from canonized paths templates. (get information from a recipe)")
    compile_by_recipe_parser.add_argument("-r", "--recipe", required=True, help="a recipe that describes the input stream (taken from profile-tags information).")
    compile_by_recipe_parser.add_argument("-s", "--sub-recipe", help="use a specific sub-recipe.")
    compile_by_recipe_parser.add_argument("-t", "--type", choices=canon_paths_application.get_supported_profile_types(), help="the profile type.")
    compile_by_recipe_parser.add_argument("-l", "--class", dest='profile_class', default="relaxed", choices=["relaxed", "strict"], help="a tag to add to the profile. 'relaxed' means it contains legitimate possibilities and 'strict' means it contains paths that should mostly be present.")
    compile_by_recipe_parser.add_argument("csv", nargs="*", default=["-"], help="the CSVs containing the regex patterns. '-' for stdin.")

    apply_parser = subparsers.add_parser('apply', help="apply the profiles generated by 'compile' to a CSV.")
    apply_parser.add_argument("-k", "--key", required=True, type=column_index, action='append', help='the key columns.')
    apply_parser.add_argument("-c", "--column", required=True, type=column_index, help='the canon paths columns.')
    apply_parser.add_argument("-p", "-r", "--profile", required=True, action='append', help=f"a file generated by '{pathlib.Path(__file__).name} compile'.")
    apply_parser.add_argument("-t", "--type", required=True, choices=canon_paths_application.get_supported_profile_types(), help="the profile type.")
    apply_parser.add_argument("-b", "--brief", action="store_true", help="just show match/unmatch percentages per key.")
    apply_parser.add_argument("-f", "--file", action="store_true", help="add a file column to the output csv.")
    apply_parser.add_argument("-g", "--tag", action="append", type=column_index, default=[], help="additional columns to append to the output.")
    apply_parser.add_argument("-u", "--unmatched-only", action="store_true", help="don't generate output lines for matches.")
    apply_parser.add_argument("csv", nargs="*", default=["-"], help="the CSVs containing canonized content (file access, registry access, network, etc...). '-' for stdin.")

    apply_by_recipe_parser = subparsers.add_parser('apply-by-recipe', help="apply the profiles generated by 'compile' to a CSV. take input scheme from a recipe.")
    apply_by_recipe_parser.add_argument("-r", "--recipe", required=True, help="a recipe that describes the input stream (taken from cross-validation information).")
    apply_by_recipe_parser.add_argument("-s", "--sub-recipe", help="use a specific sub-recipe.")
    apply_by_recipe_parser.add_argument("-p", "--profile", required=True, action='append', help=f"a file generated by '{pathlib.Path(__file__).name} compile'.")
    apply_by_recipe_parser.add_argument("-t", "--type", choices=canon_paths_application.get_supported_profile_types(), help="the profile type.")
    apply_by_recipe_parser.add_argument("-b", "--brief", action="store_true", help="just show match/unmatch percentages per key.")
    apply_by_recipe_parser.add_argument("-f", "--file", action="store_true", help="add a file column to the output csv.")
    apply_by_recipe_parser.add_argument("-u", "--unmatched-only", action="store_true", help="don't generate output lines for matches.")
    apply_by_recipe_parser.add_argument("csv", nargs="*", default=["-"], help="the CSVs containing canonized content (file access, registry access, network, etc...). '-' for stdin.")

    apply_contained_parser = subparsers.add_parser('apply-contained', help="apply the profiles generated by 'compile' to a CSV to instances to determine if the profile is contained in the instance.")
    apply_contained_parser.add_argument("-k", "--key", required=True, type=column_index, action='append', help='the key columns.')
    apply_contained_parser.add_argument("-c", "--column", required=True, type=column_index, help='the canon paths columns.')
    apply_contained_parser.add_argument("-p", "-r", "--profile", required=True, action='append', help=f"a file generated by '{pathlib.Path(__file__).name} compile'.")
    apply_contained_parser.add_argument("-t", "--type", choices=canon_paths_application.get_supported_profile_types(), help="the profile type. can be taken from the recipe if not set but there must not be a conflict between this switch and the recipe.")
    apply_contained_parser.add_argument("-f", "--file", action="store_true", help="add a file column to the output csv.")
    apply_contained_parser.add_argument("-g", "--tag", action="append", type=column_index, default=[], help="additional columns to append to the output.")
    apply_contained_parser.add_argument("-u", "--unmatched-only", action="store_true", help="don't generate output lines for matches.")
    apply_contained_parser.add_argument("csv", nargs="*", default=["-"], help="the CSVs containing canonized content (file access, registry access, network, etc...). '-' for stdin.")

    diff_parser = subparsers.add_parser('diff', help="apply the profiles generated by 'compile' to a CSV and generate json-formatted diff trees. -o is assumed to be a directory.")
    diff_parser.add_argument("-k", "--key", required=True, type=column_index, action='append', help='the key columns.')
    diff_parser.add_argument("-c", "--column", required=True, type=column_index, help='the canon paths columns.')
    diff_parser.add_argument("-p", "-r", "--profile", required=True, action='append', help=f"a file generated by '{pathlib.Path(__file__).name} compile'.")
    diff_parser.add_argument("-s", "--skip-no-profile", action="store_true", help="skip files that match no profile.")
    diff_parser.add_argument("-t", "--type", required=True, choices=canon_paths_application.get_supported_profile_types(), help="the profile type.")
    diff_parser.add_argument("-n", "--single-file", action="store_true", help="output all the diff trees to a single file")
    diff_parser.add_argument("csv", nargs="*", default=["-"], help="the CSVs containing canonized content (file access, registry access, network, etc...). '-' for stdin.")

    args = parser.parse_args()

    if args.action is None:
        parser.print_help(file=sys.stderr)
        return 1

    logging.basicConfig(level=logging.DEBUG if args.extra_verbose else (logging.INFO if args.verbose else logging.WARN), format="%(asctime)s [%(levelname)s] %(message)s")

    csv_list = []
    for csv_path in args.csv:
        _csv_path = pathlib.Path(csv_path)
        if _csv_path.is_dir():
            csv_list += [str(_path) for _path in (_csv_path/fname for fname in os.listdir(csv_path)) if _path.is_file()]
        else:
            csv_list.append(csv_path)

    args.csv = csv_list

    if args.action == 'compile':
        _compile_main(args)
    elif args.action == 'compile-by-recipe':
        _compile_by_recipe_main(args)
    elif args.action == 'apply':
        _apply_main(args)
    elif args.action == 'apply-by-recipe':
        _apply_by_recipe_main(args)
    elif args.action == 'apply-contained':
        _apply_contained_main(args)
    elif args.action == 'diff':
        _diff_main(args)
    else:
        parser.print_help(file=sys.stderr)

if __name__ == "__main__":
    with contextlib.suppress(BrokenPipeError):
        sys.exit(main())
    sys.exit(1)

