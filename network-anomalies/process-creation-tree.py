#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""
Read the CSV file, create dictionary of instances, generate process creation trees
The large data sets are going to be splited first by HostID and the result fed to the script. 
The script processes the input files in parallel.  

The script is single pass, processes the input file one line at time.
Seems like the space complexity is O(rows), performance O(rows)
Tips:
    Force cpus=1 to simplify profiling

On a single core i5 with 5GB free RAM:

1M lines 28s
2M lines 58s
4M lines 130s (probably swap kicks in)
6M lines 210s
1M lines requires ~1GB of RAM

  
Usage:
  process-creation-tree.py -h | --help
  process-creation-tree.py -i <CSV_FILE> [-o <FILE>] [-r <FILE>] [-p <FILE>] [--heading] [-c <STR>] [--instances <LIMIT>]  [--hosts <LIMIT>] [--csv] [--loglevel <LEVEL>] [--cpus <NUM>] [--leaves] [--temp-dir <STR>]

Options:
  -h --help                      Show this screen.
  -i --infile=<FILE>             CSV files to process, separated by colon. Can be a Unix style pattern
  -o --output=<FILE>             Output file [default: -]
  -r --replace=<FILE>            A file containing list of regex to normalize paths. If not specified ignore the paths
  -p --processes=<FILE>          Limit the generated data to the processes in the list
  -c --columns=<STR>             Index/header of columns [default: secdoProcessId:secdoParentId:instanceId:parentInstanceId:hostId:processPath:parentFullFilePath]
  --heading                      Heading presents
  --instances=<LIMIT>            Print list of instanceIds for every execution chain, limit by the specified value. This flag impacts performance [default: 0]
  --hosts=<LIMIT>                Print list of hosts for an execution chain, limit by the specified value [default: 0]
  --csv                          Output execution chains in CSV format <execution chain>\t<host>
  --loglevel=<LEVEL>             Logger level [default: INFO]
  --cpus=<NUM>                   Number of cores to use will be min[--cpus, len(--infile)] [default: -1]
  --leaves                       Output all tuples (host_id, secdo_process_id, instance_id) for the starting point of the execution chains
  --temp-dir=<STR>               Use this temporary directory

Example:
  ./process-creation-tree.py -i "~/Downloads/payoneer-process-creation-0-14.09.csv??" -p ../db-utils/standard-processes --columns 0:1:2:3:4:5:6
  
Environment variables:
*  PROC_SPLIT_MEMORY limit amount of RAM required by the processes (percents)

"""

import logging
import re
import csv
import sys
import os
import itertools
import multiprocessing
#import multiprocess as multiprocessing
import operator
import glob
import pickle
import tempfile
import time
import psutil
import contextlib

from docopt import docopt

'''
This code was hard to write, it is impossible to debug
Jokes aside - the logic is complex in places. I tried to comment the less obvious parts of the code

The idea:

* I read the data set - CSV file - line by line
* every line in the data set represents a process creation event and contains process name, instanceId, hostId, parentProcessId, parentProcessName
* collect the creation events in the dictionaries of parents and children, collect some other information. 
* Key is a tuple (host_id, secdo_process_id, instance_id). InstanceId alone IS not enough because there WAS a bug in the agent which caused instanceId to be 16bits (ask Ohad, GuyP, ShaiM for details)
* After I collected all the process execution events I am trying to build execution chains child::parent::grand parent usinf the dictionary "parents"
* Execution chain is represented as a list of tuples (host_id, secdo_process_id, instance_id)
* I aggregate the execution chains in a dictionary - key is an execution chain and data is a list of hosts where I encountered this chain
* This is where mutliprocessing ends - every process dumps the collected data to a file 
* The main process reads all data files into a single dictionary - this step is relatively fast because lot of data was aggregated and the data set is smaller by now
* Finally I print the collected data to the stdout

For large (1G lines) data sets the processing takes hours. Memory footprint can be significant as well
''' 

@contextlib.contextmanager
def auto_obj(obj):
    yield obj

class CsvColumns(object):
    def __init__(self, keys, columns_names=""):
        '''
        @param keys: list of keys (strings) in the dictionary of columns {"secdoProcessId":0, "secdoParentId":1, ...}
        @param s: a list of columns index or name as specified in the CSV header [0, 1, ...] or ["secdo_process", "secdo_parent", ... ]
        '''
        self.columns = {}
        self.columns_names = columns_names.split(":")
        columns_names = list(self.columns_names)
        self.keys = keys
        try:
            for key in self.keys:
                column_name = columns_names.pop(0)
                column_index = int(column_name)
                self.columns[key] = column_index
        except Exception:
            self.columns = {}

        self.initialized = (len(self.columns) == len(keys))

    def get_columns(self):
        return self.columns

    def is_ready(self):
        return self.initialized

    def infer_from_header(self, header):
        '''
        Try to infer the columns from the CSV file heading
        @param header is a list of column names in the same order as param keys in the __init__
        '''
        header_indices = {header[index] : index for index in range(len(header))}
        keys = list(self.keys)
        columns_names = list(self.columns_names)
        self.columns = {}
        for key in self.keys:
            column_name = columns_names.pop(0)
            self.columns[key] = header_indices[column_name]

        self.initialized = True

    def infer_from_file(self, filename, delimiter="\t"):
        with open(filename, "rt") as infile:
            reader = csv.reader(infile, delimiter=delimiter)
            headings = next(reader)
            self.infer_from_header(headings)

def process_arguments(logger, arguments):
    '''
    Parse command line arguments set defaults value where possible
    '''

    filenames = arguments["--infile"].split(":")
    # remove all empty strings
    filenames = [filename for filename in filenames if filename != ""]

    filename = filenames[0]
    if not os.path.isfile(filename):
        logger.debug("File {0} is not a path. Trying pattern".format(filename))
        # this is a quick workaround for bash commands like "~/myfile.csv"
        if filename.startswith("~"):
            filename = os.path.join(os.path.expanduser("~") + filename[1:])
        filenames = glob.glob(filename)

    replace_patterns = []
    if arguments["--replace"]:
        filename_replace = arguments["--replace"]
        with open(filename_replace, "r") as f:
            for line in f:
                line = line.strip()
                if line != "" and line[0] != '#':
                    replace_pair = line.split("-:-")
                    replace_patterns.append({"re":replace_pair[0], "replace":replace_pair[1], "compiled_re":re.compile(replace_pair[0]), "hits":0})
    # print replace_patterns
    processes = []
    processes_filename = arguments["--processes"]
    if processes_filename:
        with open(processes_filename, "r") as f:
            for line in f:
                line = line.strip()
                if line != "" and line[0] != '#':
                    processes.append(line)

    columns_keys = ["secdoProcessId", "secdoParentId", "instanceId", "parentInstanceId", "hostId", "processPath", "cmd", "processName", "parentFullFilePath"]
    csv_columns = CsvColumns(columns_keys, arguments["--columns"])
    if not csv_columns.is_ready() and len(filenames):
        filename = filenames[0]
        logger.debug("Attempt to infer the CSV columns from file {0}".format(filename))
        csv_columns.infer_from_file(filename)
    if not csv_columns.is_ready():
        logger.error("Failed to infer the CSV columns from file '{0}'".format(filenames))
        raise

    cpus = int(arguments["--cpus"])
    if cpus < 0:
        cpus = multiprocessing.cpu_count()

    # Reserve 4GB per process - this is an approximate number for a reasonable  
    # number of lines per file - 1-2M lines
    # See also check_memory()
    ram_per_process = 4*1000*1000*1000
    cpus = min(cpus, psutil.virtual_memory().total//ram_per_process)
    logger.info(f"Run on {cpus} cpus")

    if arguments["--temp-dir"]:
        temp_dir = arguments["--temp-dir"]
    else:
        temp_dir = tempfile.gettempdir()
    logger.info(f"temp_dir={temp_dir}")

    max_memory_str = os.environ.get('PROC_SPLIT_MEMORY', "0.5")
    max_memory = float(max_memory_str)

    return {
        "filenames":filenames,
        "replace_patterns":replace_patterns,
        "processes":processes,
        "columns":csv_columns.get_columns(),
        "print_instances":int(arguments["--instances"]),
        "print_leaves":arguments["--leaves"],
        "temp_dir":temp_dir,
        "print_hosts":int(arguments["--hosts"]),
        "headings_presents":arguments["--heading"],
        "print_csv":arguments["--csv"],
        "loglevel":arguments["--loglevel"],
        "max_memory":max_memory,
        "cpus":cpus,
        "output":arguments["--output"],
        }

class ProcessCreationTree(object):
    '''
    Usage: ProcessCreationTree(logger, filenames=configuration["filenames"]).do_job()
    @param replace_patterns: list of tuples (re, replace), for example (r"c:\\users\\.*", r<HOME>)
    @param processes: a list of processes to print upon completion, for example [explorer.exe, iexplore.exe]
    @param filenames: a list of files to process
    @param columns: dictionary of columns in the CSV file {"secdoProcessId":0, "secdoParentId":1, ...}
    '''
    def __init__(self, logger, shared_data, **kwargs):
        '''
        This is the module API
        @param logger: I need a logger to print things out
        @param kwargs["filenames"]: a list of files to process, can not be empty
        '''
        self.logger = logger
        self.filenames = kwargs.get("filenames", [])
        self.columns = kwargs["columns"]

        # Optional args
        self.replace_patterns = kwargs.get("replace_patterns", [])
        # replace_patterns is a list of structures. I want a faster thing - tuples
        self.replace_patterns_tuples = [(pattern["compiled_re"], pattern["replace"]) for pattern in self.replace_patterns]
        self.processes = kwargs.get("processes", [])
        # I want a dictionary of processes - I collect number of hits per process
        self.processes = {process: 0 for process in self.processes}
        self.headings_presents = kwargs.get("headings_presents", True)
        self.print_csv = kwargs.get("print_csv", False)
        self.delimiter = kwargs.get("delimiter", "\t")
        self.print_instances = kwargs.get("print_instances", 0)
        self.print_leaves = kwargs.get("print_leaves", 0)
        self.print_hosts = kwargs.get("print_hosts", 0)
        self.temp_dir = kwargs.get("temp_dir", tempfile.gettempdir())
        self.output = kwargs["output"]

        self.column_process_path = self.columns["processPath"]
        self.column_parent_process_path = self.columns["parentFullFilePath"]
        self.column_host_id = self.columns["hostId"]
        self.column_instance_id = self.columns["instanceId"]
        self.column_secdo_process_id = self.columns["secdoProcessId"]
        self.column_parent_instance_id = self.columns["parentInstanceId"]
        self.column_secdo_parent_id = self.columns["secdoParentId"]
        self.shared_data = shared_data

        # TODO Some instance ids are "-1" for not clear reason
        self.bad_instance_ids = ["-1", ""]
        self._log_status()
        self.filename_line = {}  # keep the corresponding CSV filename and line for every hostId,intsanceId tuple

        # since this script relies on the column order being properly supplied externally it's useful to print
        # a parsed row once just to make sure all the fields are in order
        self.sample_logged = False
        self.sample_logging_try_count = 0

    def do_csv_process(self):
        '''
        Call the method to process the input files
        I build a dictionary where I keep a parentInstanceId for every instanceId
        I build a dictionary of all instances with the normalized corresponding process path
        A normalized process path is a tuple of normalized folder name and basename
        I build a dictionary for normalized process paths which keeps 2 levels of parents (normalized paths)
        '''
        instances = {}  # keep normalized folder, basename tuple for every instance I meet
        parents = {}  # keep parentInstanceId for every instanceId I meet
        children = {}  # keep (child)instanceId for every parentInstanceId I meet - this is a reverse of parents{}
        lines_total = 0
        for filename in self.filenames:
            processing_stats = {"lines":0, "replace_match":0, "skipped_instance_id":0}
            self.logger.info("Processing {0}".format(filename))
            paths, lines = self._process_file(parents, children, instances, filename, processing_stats)
            lines_total += lines
            self.logger.info("Completed {0}, lengths: instances={1},parents={2},children={3},stats={4}".format(filename, len(instances), len(parents), len(children), processing_stats))

        for replace_pattern in self.replace_patterns:
            self.logger.debug("re {0}, hits {1}".format(replace_pattern["re"], replace_pattern["hits"]))
        for key in paths:
            # self.logger.info("paths {0} hits {1}".format(key, paths[key]["count"]))
            pass

        max_depth = 50
        self.logger.debug("Chains for {0} instances, {1} parents, {2} children".format(len(instances), len(parents), len(children)))
        family_trees, trees_height = self._get_family_trees(parents, children, instances, max_depth)
        self.logger.debug("Trees height {0}".format(trees_height))
        sorted_processes = sorted(self.processes.items(), key=operator.itemgetter(1))
        self.logger.debug("Standard processess {0}".format(sorted_processes))

        # This is faster to merge all data files in a single path
        # even if using a single core
        # This line will force merging the data files as you go
        #family_trees = self.merge_data_files(family_trees)

        # Dump the collected and merged data to a temporary file
        _, data_filename = tempfile.mkstemp(suffix=".tmp", prefix=".process-creation-tree_data", dir=self.temp_dir)
        pickle.dump(family_trees, open(data_filename, "wb"))

        # Warning! I do not know if the shared data mechanism in the multiprocessing package
        # production grade. The manual promises to do any mutex, barrier, etc required

        # Let other processes know that there is a new data file to merge
        # The shared data is managed by the multiprocessing package
        # which does the synchronization magic
        self.shared_data.data_files.append(data_filename)
        # update the global shared variable number of processed lines
        # += does not work here
        self.shared_data.lines.append(lines_total)

        self.logger.info(f"Completed data file {data_filename}, {lines_total} lines")
        return True


    def _union_family_trees(self, family_trees, new_data):
        '''
        Add data from new_data to family_trees
        '''
        trees_total = 0
        for process_name, new_tree_data in new_data.items():
            tree_data = family_trees.get(process_name, {"parents":set(), "depth":0, "instances":{}, "hosts":{}, "hits":{}, "leaves":{}})

            tree_data["depth"] = max(tree_data["depth"], new_tree_data["depth"])

            tree_data["parents"] = tree_data["parents"].union(new_tree_data["parents"])
            for parents_tree_tuple in new_tree_data["parents"]:
                tree_data["parents"].add(parents_tree_tuple)
                self._increment_counter(tree_data["hits"], parents_tree_tuple, new_tree_data["hits"][parents_tree_tuple])

                if self.print_instances:
                    tree_data_instances = tree_data["instances"].get(parents_tree_tuple, [])
                    tree_data_instances.extend(new_tree_data["instances"][parents_tree_tuple])
                    tree_data["instances"][parents_tree_tuple] = tree_data_instances

                if self.print_leaves:
                    tree_data_leaves = tree_data["leaves"].get(parents_tree_tuple, [])
                    tree_data_leaves.extend(new_tree_data["leaves"][parents_tree_tuple])
                    tree_data["leaves"][parents_tree_tuple] = tree_data_leaves

                new_tree_data_hosts = new_tree_data["hosts"].get(parents_tree_tuple, {})
                tree_data_hosts = tree_data["hosts"].get(parents_tree_tuple, {})
                for host_id, host_id_count in new_tree_data_hosts.items():
                    self._increment_counter(tree_data_hosts, host_id, host_id_count)
                tree_data["hosts"][parents_tree_tuple] = tree_data_hosts

            family_trees[process_name] = tree_data
            trees_total += len(tree_data["parents"])
        return trees_total

    def merge_data_files(self, family_trees):
        '''
        Pick a filename from the list shared_data.data_files, pickle.load it
        merge the data with the specified tree, remove the file
        Repeat until there are no data files in the list
        '''
        files_count = 0
        files_to_merge = len(self.shared_data.data_files)
        if files_to_merge:
            self.logger.info(f"Data files to merge {files_to_merge}")
        # Unfortunately multiprocessing ListProxy does not provide pop/push API
        while len(self.shared_data.data_files):
            try:
                # The shared data is managed by the multiprocessing package
                # doing necessary synchronization magic
                filename = self.shared_data.data_files[0]
                self.shared_data.data_files.remove(filename)
            except ValueError:
                logger.debug(f"Merging {filename} skipped, probably race")
                continue
            filesize = os.path.getsize(filename)
            self.logger.info(f"Merging {files_count} {filename}, {filesize} bytes")
            self._merge_data_file(family_trees, filename)
            os.remove(filename)
            files_count += 1
        return family_trees

    def _merge_data_file(self, family_trees, filename):
        '''
        Pickle.load the specified file, merge the data
        '''
        new_data = pickle.load(open(filename, "rb"))
        self._union_family_trees(family_trees, new_data)
        return family_trees

    def print_family_trees(self):
        # Merge the data files first
        family_trees = self.merge_data_files({})
        if self.print_csv:
            self._print_family_trees_csv(family_trees)
        else:
            self._print_family_trees(family_trees)

    def _log_status(self):
        self.logger.debug("Running for files '{0}'".format(
            self.filenames))
        for replace_pattern in self.replace_patterns:
            self.logger.debug("{0}".format(replace_pattern["re"]))

    def normalize_folder_slow(self, path):
        for replace_pattern in self.replace_patterns:
            replace, compiled_re = replace_pattern["replace"], replace_pattern["compiled_re"]
            normalized_path = compiled_re.sub(replace, path)
            # print replace == normalized_path, path, pattern, replace
            if replace == normalized_path:
                self._increment_counter(replace_pattern, "hits")
                return True, normalized_path

        # self.logger.debug("No match for '{0}'".format(full_path))

    def normalize_folder_fast(self, path):
        for compiled_re, replace in self.replace_patterns_tuples:
            normalized_path = compiled_re.sub(replace, path)
            if replace == normalized_path:
                return True, normalized_path

        return False, path

    def _process_file(self, parents, children, instances, filename, processing_stats):
        '''
        Read all CSV files, collect dictionaries parents, children, instances
        '''
        paths = {}
        line_index = 0
        with open(filename, "rt") as infile:
            reader = csv.reader(infile, delimiter=self.delimiter)
            self._process_headings(reader)  # skip the first row if needed
            for row in reader:
                line_index = line_index + 1
                try:
                    self._unique_paths(parents, children, instances, paths, filename, processing_stats, row, line_index)
                except Exception:
                    self.logger.error(f"Failed in {line_index}, {row}", exc_info=1)

        processing_stats["lines"] = line_index
        processing_stats["paths"] = len(paths)

        # Force one end of line out
        print(file=sys.stderr, flush=True)
        return paths, line_index

    def get_parents_tree(self, parents, instances, leaf, max_depth):
        '''
        Follow the family tree from a child specified by a tuple  (host_id, secdo_process_id, instance_id) to the the
        grand parent, but no more than max_depth
        @param leaf: (host_id, secdo_process_id, instance_id)
        '''
        processed_instances = []
        parents_instances = []
        process_name = instances.get(leaf, "<UNKNOWN>")
        parents_tree = [process_name]
        parents_instances.append(leaf)
        (host_id, secdo_process_id, parent_id) = leaf
        depth = 0
        while depth < max_depth:
            processed_instances.append((host_id, secdo_process_id, parent_id))
            key = parents.get((host_id, secdo_process_id, parent_id), None)
            if key:
                (host_id, secdo_process_id, parent_id) = key
                parent_process_name = instances.get(key, "<UNKNOWN>")
                parents_tree.append(parent_process_name)
                parents_instances.append(key)
                depth = depth + 1
            else:
                break

        if len(processed_instances) >= (max_depth - 1):
            #self.logger.debug("Longer than {0} nodes chain for {1}".format(max_depth, leaf))
            pass

        return parents_tree, parents_instances

    def _increment_counter(self, dict, key, value=1):
        count = dict.get(key, 0)
        count = count + value
        dict[key] = count

    def _get_family_trees(self, parents, children, instances, max_depth):
        '''
        Second stage of processing the data - build execution chains (family trees)
        '''
        trees_height = {}  # {i: 0 for i in range(max_depth+1)}
        instance_trees = {}
        family_trees = {}
        line_index = 0

        # I can not iterate DictProxy created by multiprocessing.Manager, I need keys() instead
        keys = instances.keys()
        for key in keys:
            # key is (host_id, secdo_process_id, instance_id)
            # Dictionary instances keeps tuples normalized path, basename
            process_folder_normalized, process_basename = process_name = instances.get(key, "<UNKNOWN>")
            # Shai Meir: process the instance if in the list of processes provided by the user
            # Arkady: or, if no such list is provided, the instance does not have children
            # I want to start a process with a leaf which does not have children
            # to_process = True
            to_process = len(self.processes) and process_basename in self.processes
            to_process = to_process or not (len(self.processes) or key in children)
            if not to_process:
                continue

            parents_tree, parents_instances = self.get_parents_tree(parents, instances, key, max_depth)
            depth = len(parents_tree) - 1  # includes the instanceId itself, not only parents
            self._increment_counter(trees_height, depth)

            # Dictionary family_trees keeps all execution threads, number of hits per execution chain, max chain length,
            # list of instances for debug
            family_tree_data = family_trees.get(process_name, {"parents":set(), "depth":0, "instances":{}, "hosts":{}, "hits":{}, "leaves":{}})
            parents_tree_tuple = tuple(parents_tree)  # I need a hashable object
            family_tree_data["parents"].add(parents_tree_tuple)
            self._increment_counter(family_tree_data["hits"], parents_tree_tuple)

            family_tree_data["depth"] = max(len(parents_tree) - 1, family_tree_data["depth"])

            family_tree_data_instances = family_tree_data["instances"].get(parents_tree_tuple, [])
            family_tree_data_instances.append((parents_instances))
            family_tree_data["instances"][parents_tree_tuple] = family_tree_data_instances

            family_tree_data_leaves = family_tree_data["leaves"].get(parents_tree_tuple, [])
            # add (host_id, secdo_process_id, instance_id) to the list of leaves
            family_tree_data_leaves.append(key)
            family_tree_data["leaves"][parents_tree_tuple] = family_tree_data_leaves

            family_tree_data_hosts = family_tree_data["hosts"].get(parents_tree_tuple, {})
            (host_id, secdo_process_id, instance_id) = key
            self._increment_counter(family_tree_data_hosts, host_id)
            family_tree_data["hosts"][parents_tree_tuple] = family_tree_data_hosts

            family_trees[process_name] = family_tree_data

            # Reduce memory foot print faster
            # instances.pop((host_id, secdo_process_id, instance_id), None)  # this is like 'del', but w/o exceptions
            # parents.pop((host_id, secdo_process_id, instance_id), None)

            line_index = line_index + 1
            if line_index % 10000 == 0:
                self.logger.debug("Processed {0} instances from {1}".format(line_index, len(keys)))

        return family_trees, trees_height

    def get_items_from_iterable(self, iterable, max_count):
        return list(itertools.islice(iterable.iteritems(), max_count))

    def _parent_tuple_csv(self, parents_tree_tuple):
        '''
        Convert execution chain to a string suitable for CSV
        '''
        s = ""
        nodes_separator = "|"
        for (process_folder_normalized, process_basename) in parents_tree_tuple:
            # s = s + process_folder_normalized + "\\" + process_basename + nodes_separator
            s = s + process_basename + nodes_separator
        # rm trailing separator
        s = s[:-len(nodes_separator)]
        return s

    def _print_family_trees_csv(self, family_trees):
        with (auto_obj(sys.stdout) if "-" == self.output else open(self.output, "wt")) as output_file:
            if self.print_leaves:
                print("process\texecution_chain\thost\tprocess_id\tinstance", file=output_file)
            else:
                print("process\texecution_chain\thost", file=output_file)
            for (process_folder_normalized, process_basename), family_tree_data in family_trees.items():
                if not process_basename in self.processes:
                    continue
                    pass  # allows to comment out the previous line

                for parents_tree_tuple in family_tree_data["parents"]:
                    parents_tree_tuple_csv = self._parent_tuple_csv(parents_tree_tuple)
                    if self.print_leaves:
                        # Print all (host_id, secdo_process_id, instance_id) tuples for the execution chain starting leaf
                        leaves = family_tree_data["leaves"][parents_tree_tuple]
                        for leaf in leaves:
                            print("{0}\t{1}\t{2}\t{3}\t{4}".format(parents_tree_tuple[0][1], parents_tree_tuple_csv, leaf[0], leaf[1], leaf[2]), file=output_file)
                    else:
                        # family_tree_data["hosts"] keeps list list of hosts where I encountered the specific execution chain
                        hosts = family_tree_data["hosts"][parents_tree_tuple]  # list of hosts where I see the execution chain
                        for host in hosts:
                            print("{0}\t{1}\t{2}".format(parents_tree_tuple[0][1], parents_tree_tuple_csv, host), file=output_file)

    def _is_subtree_tuple(self, tuples1, tuples2):
        '''
        Return True if tuples2 is a 'substring' of tuples2
        For example, (A, B, C) is a subtree of (A, B, C, D)
        '''
        if (len(tuples2) >= len(tuples1)):  # fast first test
            return False  # should be shorter

        if tuples1[0][1] != tuples2[0][1]:  # fast second test
            return False  # Compare basenames of the chains

        for index in range(1, len(tuples2)):  # Porbably similar chain. Compare all nodes in the chains
            if tuples1[index][1] != tuples2[index][1]:  # This shall not happen often
                return False

        return True

    def _is_subtree(self, tree_data_parents, sub_tree_tuple):
        '''
        Return True if parents_tree_tuple appears as a subtree in one of the tree_data_parents
        For example, (A, B, C) is a subtree of (A, B, C, D)
        '''
        for parents_tree_tuple in tree_data_parents:
            if self._is_subtree_tuple(parents_tree_tuple, sub_tree_tuple):
                return True
        return False

    def parents_tree_tuple_to_str(self, parents_tree_tuple):
        s = ""
        for leaf in  parents_tree_tuple:
            s += "::" + leaf[1]
        return s

    def _print_family_trees(self, family_trees):
        '''
        Generate human readable output for execution chains
        '''
        with (auto_obj(sys.stdout) if "-" == self.output else open(self.output, "wt")) as output_file:
            removed_sub_trees = 0
            for (process_folder_normalized, process_basename), family_tree_data in family_trees.items():
                if not process_basename in self.processes:
                    pass
                    # continue
                print("{0} {1}".format(process_folder_normalized, process_basename), file=output_file)
                for parents_tree_tuple in family_tree_data["parents"]:
                    # Collect distribution of the trees lengths on the way
                    #self._increment_counter(self.shared_data.trees_height, len(parents_tree_tuple))
                    # if self._is_subtree(family_tree_data["parents"], parents_tree_tuple):
                    #     removed_sub_trees = removed_sub_trees + 1
                    #     continue
                    # if len(parents_tree_tuple) < 10:
                    #    continue
                    hits = family_tree_data["hits"][parents_tree_tuple]  # number of occurrences of the execution chain
                    # family_tree_data["hosts"] keeps list list of hosts where I encountered the specific execution chain
                    hosts = family_tree_data["hosts"][parents_tree_tuple]  # list of hosts where I see the execution chain
                    print("\t{0}, hits={1}, hosts={2} ".format(self.parents_tree_tuple_to_str(parents_tree_tuple), hits, len(hosts)), file=output_file)
                    if self.print_hosts:
                        print("\t{0}".format(self.get_items_from_iterable(hosts, self.print_hosts)), file=output_file)
                    if self.print_leaves:
                        # Print all (host_id, secdo_process_id, instance_id) tuples for the execution chain starting leaf
                        leaves = family_tree_data["leaves"][parents_tree_tuple]
                        print("\t{0}".format(leaves), file=output_file)

                    self._print_family_trees_instances(family_tree_data, parents_tree_tuple, output_file)

            self.logger.info("Removed {0} subtrees".format(removed_sub_trees))
            #self.logger.info("Trees height {0}".format(self.shared_data.trees_height))
            #sorted_processes = sorted(self.shared_data.processes.items(), key=operator.itemgetter(1))
            #self.logger.info("Standard processess {0}".format(sorted_processes))



    def _print_family_trees_instances(self, family_tree_data, parents_tree_tuple, output_file):
        # family_tree_data["instances"] keeps list(instances of nodes) for every
        # discovered execution path
        instances = family_tree_data["instances"].get(parents_tree_tuple, [])
        instances = instances[:min(self.print_instances, len(instances))]
        for instance in instances:
            # instance is a list of tuples (hostId, secdoId, instanceId) where every tuple corresponds to a node
            # in the execution tree
            parent_instances_str = ""
            for instance_id in instance:
                # instance_id is a tuple hostId, secdoId, instanceId
                parent_instances_str = parent_instances_str + "{0}, file:{1}".format(instance_id, self.filename_line.get(instance_id, "UNKNOWN"))
            print("\t\t{0}".format(parent_instances_str), file=output_file)

    def _split_path(self, path):
        '''
        This code is slow
        folder = ntpath.dirname(path)
        folder = os.path.join(folder, '')
        basename = ntpath.basename(path)
        '''
        # This code is x10 (?) faster
        splitted_path = path.rsplit("\\", 1)
        if len(splitted_path) > 1:
            folder, basename = splitted_path[0], splitted_path[1]
        else:
            basename = splitted_path[0]
            folder = ""

        if len(folder) and folder[-1] != "\\":
            folder = folder + "\\"

        # print("path='{0}' folder='{1}' base='{2}'".format(path, folder, basename))
        return folder, basename

    def _unique_path(self, paths, filename, process_folder, process_basename):
        '''
        Wrapper method for normalize_folder_fast(). The goal is to collect statistics and debug data

        Normally I ignore the folders and replace the folder by an empty line
        The idea here is that if there are two word.exe "good" and "bad" we collect the information for 
        both
        '''
        if not len(self.replace_patterns):
            process_folder_normalized = ""
        else:
            _, process_folder_normalized = self.normalize_folder_fast(process_folder)
        # if normalize_result:
        #    self._increment_counter(processing_stats, "replace_match")

        if self.print_instances:
            process_path_stat = paths.get(process_folder_normalized, {"count":0, "base_names":set()})
            self._increment_counter(process_path_stat, "count")
            paths[process_folder_normalized] = process_path_stat
            base_names = process_path_stat["base_names"]  # number of unique basenames in this folder
            base_names.add(process_basename)
        return process_folder_normalized

    def _unique_paths(self, parents, children, instances, paths, filename, processing_stats, row, line_index):
        '''
        Collect child - parent dictionary (instances) and reverse dictionary (parents)
        '''
        if not self.sample_logged and self.sample_logging_try_count <= 10:
            self.sample_logging_try_count += 1
            try:
                self.logger.info("row parsing sample:")
                self.logger.info("\trow: %r", row)
                self.logger.info("\tprocess path: %r", row[self.column_process_path])
                self.logger.info("\tparent path: %r", row[self.column_parent_process_path])
                self.logger.info("\thost: %r", row[self.column_host_id])
                self.logger.info("\tinstance: %r", row[self.column_instance_id])
                self.logger.info("\tsecdo pid: %r", row[self.column_secdo_process_id])
                self.logger.info("\tparent instance: %r", row[self.column_parent_instance_id])
                self.logger.info("\tparent secdo pid: %r", row[self.column_secdo_parent_id])
            except Exception:
                self.logger.error("couldn't print row parsing sample!")
            else:
                self.sample_logged = True

        process_path = row[self.column_process_path]
        process_folder, process_basename = self._split_path(process_path)
        if process_basename in self.processes:
            process_basename_in_self_processes = True
            self._increment_counter(self.processes, process_basename)
        else:
            process_basename_in_self_processes = False
        parent_process_path = row[self.column_parent_process_path]
        parent_process_folder, parent_process_basename = self._split_path(parent_process_path)

        process_folder_normalized = self._unique_path(paths, filename, process_folder, process_basename)
        parent_process_folder_normalized = self._unique_path(paths, filename, parent_process_folder, parent_process_basename)
        host_id = row[self.column_host_id]
        instance_id = row[self.column_instance_id]
        secdo_process_id = row[self.column_secdo_process_id]
        parent_instance_id = row[self.column_parent_instance_id]
        secdo_parent_id = row[self.column_secdo_parent_id]
        # For not clear reason sometimes instanceId is -1 in the data, skip these lines
        if not (instance_id in self.bad_instance_ids or parent_instance_id in self.bad_instance_ids):
            if True:
            # if (len(self.processes) and not (parent_process_basename in self.processes and process_basename_in_self_processes)):  # Shai Meir: we need only execution chains wit processes from my list
            #    self._increment_counter(processing_stats, "skipped_instance_not_standard")
            # else:
                if self.print_instances:
                    # for debug purposes I collect reference for every (host_id, secdo_process_id, instance_id) tuple - filename and line number
                    # This alows to come back and check where the tuple comes from. Costs lot of RAM and peformance
                    self.filename_line[(host_id, secdo_process_id, instance_id)] = (filename, line_index)
                # TBD check if it is already there and different, print error
                instances[(host_id, secdo_process_id, instance_id)] = (process_folder_normalized, process_basename)
                # TBD check if it is already there and different, print error
                instances[(host_id, secdo_parent_id, parent_instance_id)] = (parent_process_folder_normalized, parent_process_basename)
                parents[(host_id, secdo_process_id, instance_id)] = (host_id, secdo_parent_id, parent_instance_id)
                children[(host_id, secdo_parent_id, parent_instance_id)] = (host_id, secdo_process_id, instance_id)
        else:
            self._increment_counter(processing_stats, "skipped_instance_id")


    def _process_headings(self, reader):
        '''
        Skip the first raw of the CSV if the command line option --heading presents
        I can parse the heading and check if complies the CsvColumns.infer_from_file
        '''
        if self.headings_presents:
            _ = next(reader)



class SharedData(object):
    def __init__(self, manager):
        self.data_files = manager.list()  # data files for union
        self.lines = []

def job(args):
    '''
    A new process will call this method
    '''
    shared_data, configuration, filename, log_level = args
    global logger
    configuration = configuration.copy()
    configuration["filenames"] = [filename]
    result = ProcessCreationTree(logger, shared_data, **configuration).do_csv_process()
    logger.info(f"Completed {filename}")
    return result

def print_completed_lines(shared_data):
    '''
    TODO Does not work for not clear reason. List of files appears to be working
    List of processed lines does not.  

    # Print something like 1,030,184
    lines_total = sum(shared_data.lines)
    lines_str = "{:,}".format(lines_total)
    # Skip trailing CR - print line index in the same line
    print(f"\r{lines_total} ", end='', file=sys.stderr, flush=True)
    '''

def check_jobs(started_jobs):
    '''
    Poll the list of the running jobs, return True if there 
    is a completed job
    '''
    job_completed = False
    for p in started_jobs:
        if  not p.is_alive():
            p.join()
            job_completed = True
            break

    return job_completed, p

def check_memory(threshold):
    '''
    check the system RAM and pause the thread spawning until
    there is free RAM
    @param threshold: for example 0.15 means block until 15% of RAM is available
    '''
    while True:
        memory_status = psutil.virtual_memory()
        # If there is threshold percents of available RAM I can continue
        if memory_status.available > memory_status.total * (1.0-threshold):
            break
        time.sleep(1.0)

def start_jobs(logger, configuration, args, cpus):
    started_jobs = []
    jobs_done = 0
    for arg in args:
        shared_memory, _, _, _ = arg
        p = multiprocessing.Process(target=job, args=(arg,))
        started_jobs.append(p)
        p.start()
        while len(started_jobs) > cpus:  # if there are too many jobs running block the caller
            job_completed, p = check_jobs(started_jobs)
            if job_completed:
                started_jobs.remove(p)
            print_completed_lines(shared_memory)
            time.sleep(0.5)
        check_memory(configuration["max_memory"])

    while len(started_jobs):
        job_completed, p = check_jobs(started_jobs)
        if job_completed:
            started_jobs.remove(p)
        print_completed_lines(shared_memory)
        time.sleep(0.5)
        
    check_memory(configuration["max_memory"])

    logger.info(f"{jobs_done} jobs done")

def main():
    '''
    A placeholder
    '''
    logging.basicConfig()
    global logger
    logger = logging.getLogger('process-creation')
    logger.setLevel("DEBUG")
    # logger.setLevel("INFO")
    arguments = docopt(__doc__, version='process-creation')

    configuration = process_arguments(logger, arguments)
    configuration["delimiter"] = "\t"
    log_level = configuration["loglevel"]
    logger.setLevel(log_level)

    logger.debug(configuration)

    filenames = configuration["filenames"]
    if not len(filenames):
        logger.error("No files to process in {0}".format(arguments["--infile"]))
        return
    cpus = configuration["cpus"]

    manager = multiprocessing.Manager()
    shared_data = SharedData(manager)

    args = [(shared_data, configuration, filenames[x], log_level) for x in range(len(filenames))]
    logger.debug(args)
    if cpus > 1:
        start_jobs(logger, configuration, args, cpus)
    else:  # force single CPU to simplify profiling
        ProcessCreationTree(logger, shared_data, **configuration).do_csv_process()

    # Finally I can print all execution chains
    logger.info("Output trees")
    ProcessCreationTree(logger, shared_data, **configuration).print_family_trees()


if __name__ == '__main__':
    sys.exit(main())

