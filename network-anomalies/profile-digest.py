#!/usr/bin/env python3

import logging
import csv
import argparse
import contextlib
import sys
import pathlib
import ipaddress
from collections import defaultdict

from utilities import anytree_helper

@contextlib.contextmanager
def auto_context(obj):
    '''
    Arkady: Turn an object (stdin) into an iterator ? I wonder why it is not a lambda
    '''
    yield obj

def column_arg(value):
    value = int(value)
    assert value > 0
    return value - 1

def digest_fs_like_paths(paths, traps):
    traps = set(traps)

    paths_tree = anytree_helper.paths_to_tree(paths)

    queue = [paths_tree]
    while len(queue) > 0:
        node = queue.pop(0)
        if node.name in traps: # if this is a trap, drop it for the digest
            node.parent = None
        elif traps.isdisjoint({child.name for child in node.children}):
            queue += list(node.children)
        else: # the node has a trap child so just drop everything AND RUN
            node.children = tuple()

    return anytree_helper.tree_to_paths(paths_tree)

def digest_net_profile(canon_subnets):
    subnets = set()
    for subnet in canon_subnets:
        components = subnet.split(":")
        if len(components) == 2:
            access = ''
            port = components[0]
            net = ipaddress.ip_network(components[1])
        elif len(components) == 3:
            access = components[0] + ':'
            port = components[1]
            net = ipaddress.ip_network(components[2])
        else:
            raise ValueError('invalid subnet', subnet)

        net = net.supernet(net.prefixlen % 8)
        subnets.add(f"{access}{port}:{net}")

    return subnets

digest_handlers = {
    'registry' : lambda paths: digest_fs_like_paths(paths, ['[SUBKEY]', '[VALUE]']),
    'file-system' : lambda paths: digest_fs_like_paths(paths, ['[SUBFOLDER]', '[FILE]']),
    'network' : digest_net_profile,
}

def main():
    parser = argparse.ArgumentParser(description="generate a 'human palatable' digest of a profile.")
    parser.add_argument("-o", "--output", default="-", help="output file. use '-' for stdout.")
    parser.add_argument("-c", "--column", required=True, type=column_arg, help="the column of the profile entry.")
    parser.add_argument("-k", "--key", default=[], action='append', type=column_arg, help="key columns to perform pre-digest grouping on.")
    parser.add_argument("-t", "--type", choices=list(digest_handlers.keys()), default='file-system', help="the type of the profile")
    parser.add_argument("profile", default="-", nargs="?", help="the profile file to generate a digest for. '-' for stdin.")

    args = parser.parse_args()

    groups = defaultdict(lambda : set())
    with (auto_context(sys.stdin) if '-' == args.profile else open(args.profile, "rt")) as csv_file:
        reader = csv.reader(csv_file, delimiter=',', quotechar='"')
        for line in reader:
            groups[tuple((line[k] for k in args.key))].add(line[args.column])

    digest = {}
    for group, paths in groups.items():
        digest[group] = digest_handlers[args.type](paths)

    with (auto_context(sys.stdout) if '-' == args.output else open(args.output, 'wt')) as output_stream:
        writer = csv.writer(output_stream, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
        for group, paths in digest.items():
            writer.writerows((group + (path,) for path in paths))

if __name__ == "__main__":
    main()

