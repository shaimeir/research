#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Similar to the Unix split utility. Output file will contain suffix aa, ab, ac, ..
O(lines in file) complexity

Single core 1M lines in ~3s

Usage:
  split-by-hostid.py -h | --help
  split-by-hostid.py -l <COUNT> -i <FILE> -c <NUM> [-d <DELIMITER>] [-s <FILE>] [--noheadings] [--loglevel <LEVEL>]

Options:
  -h --help                      Show this screen.
  -l --length=<COUNT>            Number of hosts in a file
  -i --infile=<FILE>             File to process, can be a colon separated list of files
  -c --column=<NUM>              HostID column in the CSV file, can be a name of the column
  -s --suffix=<FILE>             Suffix of the generated files, by default first specified file
  --noheadings                   Prevent copy of the headers to the output files.
  --loglevel=<LEVEL>             Logger level [default: INFO]

Example:
  ./split-by-hostid.py -l 200 -i ~/Downloads/payoneer-process-creation-0-14.09.csv -c 4
"""

import logging
import csv
import sys
import os
from docopt import docopt
import pickle
import multiprocessing
#import pandas
if sys.version_info >= (3, 0):
    import hashlib
else:
    import md5
import time
import re
import math

OUTPUT_FILES = {}         # for every N HostIDs I meet I allocate an output file file
OUTPUT_FILENAMES = {}     # List of output files
LATEST_CREATED_FILE = (None, 0, None, None)

# Number of letter in ABC
ABC_LEN = len("ABCDEFGHIKLMNOPQRSTVXYZ")

# Start with 'aaaa' - 1
CURRENT_PREFIX = "aaa" + chr(ord('a')-1)

def get_next_prefix(prefix):
    '''
    aa + 1 = ab
    '''
    index = len(prefix)-1
    carry = 1
    prefix = list(prefix)
    while True:
        next_ord = ord(prefix[index]) - ord('a') + carry
        prefix[index] = chr(next_ord % ABC_LEN + ord('a'))
        carry = next_ord // ABC_LEN
        if not carry:
            break
        index = index - 1

    return "".join(prefix)

def get_file(suffix, host_id, max_host_count, header):
    '''
    Get CSV writer for the specified host, allocate and open new files as required
    '''
    global LATEST_CREATED_FILE
    if host_id in OUTPUT_FILES:
        return OUTPUT_FILES[host_id]

    # I reach this part of the code once for every hostId in the raw data

    # Check if there is a file with less than max_host_count hosts
    # I want to collect this hostId in the file
    filename, hosts, file_descriptor, csv_writer = LATEST_CREATED_FILE
    found_file = filename and hosts < max_host_count
    if found_file:
        # book keeping
        OUTPUT_FILES[host_id] = (filename, file_descriptor, csv_writer)
        OUTPUT_FILENAMES[filename] = (hosts+1, file_descriptor, csv_writer)
        # update number of hosts in the last created file
        LATEST_CREATED_FILE = (filename, hosts+1, file_descriptor, csv_writer)
        return filename, file_descriptor, csv_writer

    # Last created file is full. I need to open a new file for the coming hostIds
    global CURRENT_PREFIX
    CURRENT_PREFIX = get_next_prefix(CURRENT_PREFIX)
    filename = suffix + CURRENT_PREFIX
    file_descriptor = open(filename, "wt")
    csv_writer = csv.writer(file_descriptor, delimiter=delimiter)
    if header:
        csv_writer.writerow(header)
    OUTPUT_FILES[host_id] = (filename, file_descriptor, csv_writer)
    OUTPUT_FILENAMES[filename] = (1, file_descriptor, csv_writer)
    logger.info("Allocated file {0}".format(filename))
    # I will add the coming hostIds to this file
    LATEST_CREATED_FILE = (filename, 1, file_descriptor, csv_writer)
    return filename, file_descriptor, csv_writer

def process_file(filename, suffix, host_id_column, max_host_count, no_headings):
    line_index = 0
    with open(filename, "rt") as infile:
        reader = csv.reader(infile, delimiter=delimiter)
        if not no_headings:
            header = next(reader)
        else:
            header = None

        resume = True
        print_lines_timestamp = time.time()
        start_time = time.time()
        skipped_lines = 0

        # A column is a non TAB character
        # HostId column can be a last column in the row or followed by a \t
        # Alternatively I can ensure that host_id_column is the first column - this is fastest
        # if hostId column is 4 (zero based) I want regex  (?:[^\t]+\t){4}(\S+)(?:\t|$)
        re_pattern = "(?:[^\\t]+\\t){{{0}}}([^\\t]+)(?:\\t|$)".format(host_id_column)
        logger.debug(f"Search pattern {re_pattern}, column {host_id_column}")

        re_pattern = re.compile(re_pattern)
        bytes_total = 0
        while resume:
            resume = False
            try:
                # csv_reader does a better job with end of lines 
                # reading the CSV line by line is 3x faster than CSV parser
                # but fails from time to time. In one sample 400 lines from 170M 
                # failed to match the regex
                # There are csv_reader based versions roughly 3-4 days back in the history 
                for line in infile:
                    bytes_total += len(line)
                    # Regex gives 50% improvement vs CSV parsing
                    host_id = re_pattern.search(line).group(1)
                    #print(f"{host_id}")
                    '''
                    host_id = row[host_id_column]
                    # This variant is slower by 30% than csv.reader
                    #columns = row.split(delimiter)
                    #host_id = columns[host_id_column]
                    '''
                    # Performance tuning - I skip call to the function get_file() in the most likely case
                    _, outfile, csv_writer = OUTPUT_FILES.get(host_id, (None, None, None))
                    if not csv_writer: # This is less likely For example in 2 weeks worth of data there are 170M lines for 1K hosts.
                        _, outfile, csv_writer = get_file(suffix, host_id, max_host_count, header)
                    outfile.write(line) #csv_writer.writerow(row)
                    line_index += 1
                    if logger.level >= logging.DEBUG and (time.time() - print_lines_timestamp) > 0.2:
                        print_lines_timestamp = time.time()
                        elapsed_time = math.ceil(print_lines_timestamp - start_time)
                        if elapsed_time:
                            rate = (line_index//elapsed_time)//1000
                            bytes_rate = (bytes_total//elapsed_time)//(1000*1000)
                            line_index_str = "{:,}".format(line_index)
                            print(f"\r{line_index_str} lines, {rate}Klines/s, {bytes_rate}MB/s ", end='', file=sys.stderr, flush=True)

            except UnicodeDecodeError:
                resume = True
                logger.debug("UnicodeDecodeError in the line {0} skipped".format(line_index))
            except TypeError:
                skipped_lines += 1
                resume = True
            except Exception:
                logger.debug("Exception in the line {0} '{1}'".format(line_index, line), exc_info=True)
                resume = True

    return line_index, skipped_lines

class CacheData(object):
    '''
    Splitting large data sets by hostId takes lot of time. I try to avoid doing the same work twise.
    When I complete the split I save the hashes of the resulting files and a "fingerprint" 
    of the original data set in the file ~/.split-by-hostid 
    '''
    def __init__(self, cache_filename):
        self.cache_filename = cache_filename
        if os.path.isfile(cache_filename):
            self.processed_files = pickle.load(open(cache_filename, "rb"))
        else:
            logger.info("Cache {0} is not found".format(cache_filename))
            self.processed_files = {}

    def add_file(self, in_filenames, result_files, max_host_count):
        '''
        Collect fingerprints of the files, store in the cache file
        '''
        self.processed_files[in_filenames] = {"in_files":self._get_actual_fingerprints(in_filenames),
                                              "result_files_fingerprints":self._get_actual_fingerprints(result_files),
                                              "result_files_list":result_files,
                                              "max_host_count":max_host_count}
        self._store()

    def get_file(self, in_filenames):
        return self.processed_files.get(tuple(in_filenames), None)

    def get_stored_fingerprints(self, in_filenames):
        processed_file = self.get_file(in_filenames)
        return processed_file["in_files"], processed_file["result_files"]

    def check_file(self, in_filenames, max_host_count):
        '''
        Do I have this file(s) in the data base (in the cache)?
        Is the fingerprint correct?
        Are all resulting file correct? 
        @param max_host_count: maximum number of hosts per file
        '''
        processed_file = self.processed_files.get(in_filenames, None)
        if not processed_file:
            logger.info("No '{0}' in the cache".format(in_filenames))
            return False
        if processed_file["max_host_count"] != max_host_count:
            logger.info("Max count in '{0}' is {1}, expected {2}".format(in_filenames, max_host_count, processed_file["max_host_count"]))
            return False
        fingerprints = self._get_actual_fingerprints(in_filenames)
        if fingerprints != processed_file["in_files"]:
            logger.info("Bad fingerprint in '{0}' ".format(in_filenames))
            return False
        result_files_list = processed_file["result_files_list"]
        result_files_fingerprints = self._get_actual_fingerprints(result_files_list)
        expected_files_fingerprints = processed_file["result_files_fingerprints"]
        same_fingerprints, bad_file = self._compare_fingerprints(expected_files_fingerprints, result_files_fingerprints)
        if not same_fingerprints:
            logger.info("Bad fingerprint in '{0}', actual {1}, expected {2}".format(bad_file, result_files_fingerprints[bad_file]["size"], expected_files_fingerprints[bad_file]["size"]))
            return False

        return True
    
    def _compare_fingerprints(self, fingerprints1, fingerprints2):
        for filename, fingerprint1 in fingerprints1.items():
            fingerprint2 = fingerprints2.get(filename, None)
            if fingerprint2 != fingerprint1:
                return False, filename
        return True, None

    def _get_actual_fingerprints(self, in_filenames):
        '''
        Return dictionary which contains size, second and last lines of the files
        '''
        fingerprints = {}
        for filename in in_filenames:
            if not os.path.isfile(filename):
                continue
            fingerprints[filename] = {}
            fingerprints[filename]["size"] = os.path.getsize(filename)
            fingerprints[filename]["hashes"] = self._get_check_sums(filename)

        return fingerprints

    def _get_check_sums(self, filename):
        '''
        Checksum is a part of the fingerprint. I avoid reading the whole file and choose 
        a couple of samples. I can do this work in parallel and use very fast hash function
        '''
        sums = []
        BLOCK_SIZE = 4096
        # At 0% - first lines, 100% - last lines, and a couple of points in the middle
        samples = [(0, 0), (10, 0), (30, 0), (70, 0), (80, 0), (85, 0), (90, 0), (100, -BLOCK_SIZE)]
        filesize = os.path.getsize(filename)
        with open(filename, "rb") as f:
            for sample_point, sample_offset in samples:
                offset = (filesize * sample_point) // 100 + sample_offset
                # the file can be smaller than BLOCK_SIZE
                try:
                    f.seek(offset, 0)
                    data = f.read(BLOCK_SIZE)
                except: 
                    f.seek(0, 0)
                    data = f.read()
                if sys.version_info >= (3, 0):
                    sums.append(hashlib.md5(data).digest())
                else:
                    sums.append(md5.new(data).digest())
        return sums

    def _store(self):
        pickle.dump(self.processed_files, open(self.cache_filename, "wb"))

def file_in_cache(cache_filename, in_filenames, max_host_count):
    '''
    Return True if the files already processed
    '''
    logger.info("Check cache {0}".format(cache_filename))
    cache_data = CacheData(cache_filename)
    try:
        result = cache_data.check_file(tuple(in_filenames), max_host_count)
    except Exception:
        result = False
        logger.error("Failed to check the data files", exc_info=False)
    return result, cache_data.get_file(in_filenames)

def add_to_cache(cache_filename, in_filenames, result_files, max_host_count):
    cache_data = CacheData(cache_filename)
    cache_data.add_file(tuple(in_filenames), tuple(result_files), max_host_count)

def main():
    logging.basicConfig()
    global logger
    logger = logging.getLogger('split-by-hostid')
    logger.setLevel("DEBUG")
    arguments = docopt(__doc__, version='split-by-hostid')
    global delimiter
    delimiter = "\t"

    max_host_count = int(arguments["--length"])
    in_filenames = arguments["--infile"].split(":")
    no_headings = arguments["--noheadings"]
    log_level = arguments["--loglevel"]
    logger.setLevel(log_level)

    suffix = arguments["--suffix"]
    if not suffix:
        filename = in_filenames[0]
        suffix = filename

    home_folder = os.path.expanduser("~")
    cache_file = os.path.join(home_folder, ".split_by_host_id")
    in_cache, processed_file = file_in_cache(cache_file, in_filenames, max_host_count)
    if in_cache:
        for filename in processed_file["in_files"]:
            logger.info("File {0}, {1} bytes has a match".format(filename, processed_file["in_files"][filename]["size"]))
        result_files_fingerprints = processed_file["result_files_fingerprints"]
        result_files_list = processed_file["result_files_list"]
        for filename in result_files_list:
            logger.info("{0}, {1} bytes".format(filename, result_files_fingerprints[filename]["size"]))
        return

    try:
        host_id_column = int(arguments["--column"])
    except Exception:
        # try heading
        with open(in_filenames[0], "rt") as infile:
            reader = csv.reader(infile, delimiter=delimiter)
            header = next(reader)
            header_indices = {header[index] : index for index in range(len(header))}
            host_id_column = header_indices[arguments["--column"]]

    total_lines = 0
    total_sipped_lines = 0
    for filename in in_filenames:
        lines, skipped_lines = process_file(filename, suffix, host_id_column, max_host_count, no_headings)
        total_lines += lines
        total_sipped_lines += skipped_lines


    hosts_total = 0
    files_total = len(OUTPUT_FILENAMES)
    for filename, (hosts, file_descriptor, _) in OUTPUT_FILENAMES.items():
        file_descriptor.close()
        hosts_total = hosts_total + hosts

    logger.info("Store the results in cache {0}".format(cache_file))
    add_to_cache(cache_file, in_filenames, OUTPUT_FILENAMES.keys(), max_host_count)
    total_sipped_lines_str = "{:,}".format(total_sipped_lines)
    total_lines_str = "{:,}".format(total_lines)
    logger.info(f"{hosts_total} hosts, {files_total} files, skipped {total_sipped_lines_str} from {total_lines_str} lines")

if __name__ == '__main__':
    sys.exit(main())
