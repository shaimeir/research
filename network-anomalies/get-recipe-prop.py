#!/usr/bin/env python3

import sys
import argparse

from csv_processing import profile_recipe
from utilities import option_registry_util

properties = option_registry_util.OptionRegistry()

@properties.register("scheme")
def scheme_property(recipe):
    return recipe.scheme_name

@properties.register("subrecipes")
def sub_recipes(recipe):
    return list(recipe.subrecipes.keys())

def main():
    parser = argparse.ArgumentParser(description="prints certain properties for given recipe files")
    parser.add_argument("-p", "--property", action="append", default=[], choices=properties.choices(), help="what to print.")
    parser.add_argument("recipe", help="the recipe file to print the property for.")

    args = parser.parse_args()

    recipe = profile_recipe.load_recipe_from_json(args.recipe)
    for prop in args.property:
        _prop = properties.option(prop)(recipe)
        if isinstance(_prop, (list, tuple)):
            print("\n".join(map(str, _prop)))
        else:
            print(_prop)

if __name__ == "__main__":
    sys.exit(main())

