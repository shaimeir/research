#!/usr/bin/env python3
'''
this script takes the output of a profile applied to some data (generated by "normalizing" a dump CSV file,
i.e. running it through the appropriate aggregation scheme with csv-stats.py. you may refere to the "normalization"
sub-recipe in recipes/*.json for clarification and examples) through canon-paths-applier.py and uses the added
"matched"/"unmatched" label to generate cross-validation statistics.

the "classic" use of this script is to count the number of process instances (identified in the input stream with
host id, secdo process id and instance id) that had at least one access outside of the profile used in the application.

the output is a JSON file that holds the statistics summary.
this script also supports formatting of the data in the JSON into a nicer format.

this script now supports obtaining some of it's relevant information from recipes to allow it to be fully automated.
'''

import csv
import sys
import pprint
import argparse
import contextlib
import logging
import tabulate
import csv
import json
import numpy
from collections import defaultdict
from collections import namedtuple

from utilities import option_registry_util
from csv_processing import profile_recipe

@contextlib.contextmanager
def auto_context(obj):
    yield obj

def generate_main(args):
    instances = defaultdict(set)
    mismatched_instances = defaultdict(set)
    mismatched_content = defaultdict(lambda : defaultdict(lambda : 0))

    instances_paths = defaultdict(lambda : defaultdict(set))
    instances_mismatched_paths = defaultdict(lambda : defaultdict(set))

    for matches_file in args.matches:
        with (auto_context(sys.stdin) if '-' == matches_file else open(matches_file, 'rt')) as match_file_stream:
            csv_reader = csv.reader(match_file_stream, delimiter=',', quotechar='"', dialect='unix')
            for entry in csv_reader:
                key = tuple((entry[index] for index in args.process_key))
                instance = tuple((entry[index] for index in args.instance_key))
                content = entry[args.column]
                match = (entry[args.match_column] == args.match_tag)

                instances[key].add(instance)
                instances_paths[key][instance].add(content)

                if match:
                    continue

                mismatched_content[key][content] += 1
                mismatched_instances[key].add(instance)
                instances_mismatched_paths[key][instance].add(content)

    instances = {
        k : len(v)
        for k, v in instances.items()
    }
    instances = defaultdict(lambda : 0, instances)
    mismatched_instances = {
        k : len(v)
        for k, v in mismatched_instances.items()
    }
    mismatched_instances = defaultdict(lambda : 0, mismatched_instances)

    events_series = {
        key : [len(events) for events in instances.values()]
        for key, instances in instances_paths.items()
    }

    events_average = {
        key : (numpy.average(series), numpy.std(series), max(series), min(series))
        for key, series in events_series.items()
    }

    exception_percents = {
        key : [len(instances_mismatched_paths.get(key,{}).get(instance,set()))/len(instances[instance]) for instance in instances]
        for key, instances in instances_paths.items()
    }

    exception_average = {
        key : (numpy.average(series), numpy.std(series), max(series), min(series))
        for key, series in exception_percents.items()
    }

    cross_validation_digest = {
        'cross_validation' : [
            {
                'key' : key,
                'tag' : args.tag,
                'instance_count' : instances[key],
                'mismatched' : mismatched_instances[key],
                'mismatched_content' : dict(mismatched_content[key]),
                'events_average' : events_average[key][0],
                'events_std' : events_average[key][1],
                'events_max' : events_average[key][2],
                'events_min' : events_average[key][3],
                'exception_average' : exception_average[key][0],
                'exception_std' : exception_average[key][1],
                'exception_max' : exception_average[key][2],
                'exception_min' : exception_average[key][3],
            }

            for key in instances.keys()
        ]
    }

    with (auto_context(sys.stdout) if '-' == args.output else open(args.output, "wt")) as output_stream:
        json.dump(cross_validation_digest, output_stream, indent=4)

def generate_by_recipe_main(args):
    # this function merely augments args with the arguments generate_main() expects from the recipe and passes on the work

    # NOTE: there's an important distinction here between 'tags' in the recipe and 'tags' in the arguments list.
    #
    # 'tags' in the recipe are what's referred to in this script as an 'instance_key' - extra columns added to input streams used
    # to identify to which instance to attribute the match/fail (ususally these fields will be host id, secdo process id and instance id
    # but it may as well be just the host id or whatever we'd like - just a matter of deciding how to count the matches/mismatches).
    #
    # 'tags' in the arguments is what's referred to in the profile as 'profile_extra_tags' - just some extra strings to add
    # to the output cross-validation summary ('file-system', 'occurrences', 'strict', 'clown', 'badaboom', etc...).
    #
    # 'keys' in the recipe are what's referred to here as 'process-key' - something that identifies the profile to pick
    # to match the line against ('services.exe', 'svchost.exe', ('explorer.exe', 'modify') and such).

    logging.info("loading recipe from %s", args.recipe)
    recipe = profile_recipe.load_recipe_from_json(args.recipe)
    if args.sub_recipe is not None:
        logging.info("using sub-recipe %s", args.sub_recipe)
        recipe = recipe.get_subrecipe(args.sub_recipe)

    process_keys = []
    instance_keys = []
    values = []

    index = 0

    # first look at the by-elements as they should appear first in a correct input
    for by_element_id, by_element_cv_tag in recipe.cross_validation_by_elements.items():
        logging.info("by-element: %s (column %d) is %r", by_element_id, index, by_element_cv_tag)

        if by_element_cv_tag == profile_recipe.CrossValidationTag.key:
            process_keys.append(index)
        elif by_element_cv_tag == profile_recipe.CrossValidationTag.value:
            values.append(index)
        elif by_element_cv_tag == profile_recipe.CrossValidationTag.tag:
            instance_keys.append(index)

        # since these arrays should hold column 0-based indices advance the index after assigning it
        index += 1

    # now we can look at the elements
    for element_id, element_cv_tag in recipe.cross_validation_elements.items():
        logging.info("element: %s (column %d) is %r", element_id, index, element_cv_tag)

        if element_cv_tag == profile_recipe.CrossValidationTag.key:
            process_keys.append(index)
        elif element_cv_tag == profile_recipe.CrossValidationTag.value:
            values.append(index)
        elif element_cv_tag == profile_recipe.CrossValidationTag.tag:
            instance_keys.append(index)

        # since these arrays should hold column 0-based indices advance the index after assigning it
        index += 1

    # make sure we only got one value
    assert len(values) == 1, ValueError('the recipe has a bad number of fields marked as cross-validation values (only 1 is allowed)', values)

    # augment args with the new information
    args.process_key = process_keys
    args.instance_key = instance_keys
    args.column = values[0]

    # augmenting the tags with extra tags from the recipe
    args.tag = recipe.profile_extra_tags + [tag for tag in args.tag if args.tag not in recipe.profile_extra_tags]
    assert len(args.tag) > 0, ValueError("tags must be specified (in the recipe, in the command line or in both)")

    # and carry on my wayward son
    logging.debug("passing loaded arguments to generate_main(): %r", args)
    generate_main(args)

formatting_options = option_registry_util.OptionRegistry()

@formatting_options.register("summary")
def _format_summarize(digests, tags, output_file):
    '''
    human readable summary containing false positive rate and aggregated profile misses.
    '''
    false_positive_ratio = []
    for digest_key, digest_records in digests.items():
        line = ['/'.join(digest_key)]
        for tag in tags:
            record = digest_records.get(tag, None)
            if record is None:
                line.append("--")
            else:
                instance_count = int(record['instance_count'])
                mismatched_count = int(record['mismatched'])
                line.append("{:.02f}% ({}/{})".format(100*mismatched_count/instance_count, mismatched_count, instance_count) if instance_count != 0 else '--')

        false_positive_ratio.append(line)

    exceptions = []
    for digest_key, digest_records in digests.items():
        line = ['/'.join(digest_key)]
        for tag in tags:
            record = digest_records.get(tag, None)
            if record is None:
                line.append("--")
            else:
                avg = record['exception_average']
                std = record['exception_std']
                _min = record['exception_min']
                _max = record['exception_max']
                line.append("{:.02f}% ({:.02f}%, {:.02f}%-{:.02f}%)".format(100*avg, 100*std, 100*_min, 100*_max))

        exceptions.append(line)

    events = []
    for digest_key, digest_records in digests.items():
        line = ['/'.join(digest_key)]
        for tag in tags:
            record = digest_records.get(tag, None)
            if record is None:
                line.append("--")
            else:
                avg = record['events_average']
                std = record['events_std']
                _min = record['events_min']
                _max = record['events_max']
                line.append("{:.02f} ({:.02f}, {}-{})".format(avg, std, _min, _max))

        events.append(line)

    print("percentages summary (mismatches/total instances):", file=output_file)
    print("\t" + tabulate.tabulate(false_positive_ratio, headers=('',) + tuple(('[' + '/'.join(tag) + ']' for tag in tags))).replace("\n", "\n\t"), file=output_file)

    print(file=output_file)
    print("exception summary: [average (std., min-max)]", file=output_file)
    print("\t" + tabulate.tabulate(exceptions, headers=('',) + tuple(('[' + '/'.join(tag) + ']' for tag in tags))).replace("\n", "\n\t"), file=output_file)

    print(file=output_file)
    print("events summary: [average (std., min-max)]", file=output_file)
    print("\t" + tabulate.tabulate(events, headers=('',) + tuple(('[' + '/'.join(tag) + ']' for tag in tags))).replace("\n", "\n\t"), file=output_file)

    print(file=output_file)
    for digest_key, digest_records in digests.items():
        for tag, record in digest_records.items():
            print("/".join(digest_key) + " [" + ",".join(tag) + "]:", file=output_file)
            for content, hit_count in sorted(record['mismatched_content'].items(), key=(lambda pair: pair[1]), reverse=True):
                print("\t", hit_count, content, file=output_file)

            print(file=output_file)

@formatting_options.register("csv")
def _format_summarize(digests, tags, output_file):
    '''
    csv output with the first column a process name and the rest of the columns are profile types and their hit rate.
    '''
    false_positive_ratio = [('process',) + tuple(('/'.join(tag) for tag in tags))]
    for digest_key, digest_records in digests.items():
        line = ['/'.join(digest_key)]
        for tag in tags:
            record = digest_records.get(tag, None)
            if record is None:
                line.append("--")
            else:
                instance_count = int(record['instance_count'])
                mismatched_count = int(record['mismatched'])
                line.append("{:.04f}".format(mismatched_count/instance_count) if instance_count != 0 else '-1')

        false_positive_ratio.append(line)

    output_writer = csv.writer(output_file, delimiter='\t', dialect='unix', quoting=csv.QUOTE_MINIMAL)
    output_writer.writerows(false_positive_ratio)

def format_main(args):
    if args.format is None:
        args.format = ["summary"]
    if args.output is None:
        args.output = ["-"]

    assert len(args.output) == len(args.format), (args.output, args.format)
    args.format = [formatting_options.option(fmt) for fmt in args.format]

    digests_list = []
    for digest_file_name in args.digest:
        with (auto_context(sys.stdin) if '-' == digest_file_name else open(digest_file_name)) as digest_json:
            digests_list += json.load(digest_json)['cross_validation']

    tags = set()
    digests = defaultdict(lambda : {})
    for digest in digests_list:
        digests[tuple(digest['key'])][tuple(digest['tag'])] = digest
        tags.add(tuple(digest['tag']))

    tags = tuple(tags)

    for fmt, output_file_name in zip(args.format, args.output):
        with (auto_context(sys.stdout) if '-' == output_file_name else open(output_file_name, "wt")) as output_file:
            fmt(digests, tags, output_file)

def content_main(args):
    digests = []
    for digest_file_name in args.digest:
        with (auto_context(sys.stdin) if '-' == digest_file_name else open(digest_file_name)) as digest_json:
            digests += json.load(digest_json)['cross_validation']

    with (auto_context(sys.stdout) if '-' == args.output else open(args.output, "wt")) as output_file:
        writer = csv.writer(output_file, delimiter=",", dialect='unix')
        for record in digests:
            key = record['key'] + record['tag']
            writer.writerows((key + [mismatched_content, str(hit_count)] for mismatched_content, hit_count in record['mismatched_content'].items()))

def _positive_int(v):
    v = int(v)
    assert v > 0
    return v - 1

def main():
    parser = argparse.ArgumentParser(description="summarise the output of canon-path-applier.py 'apply' (a CSV with key, content and match tag) into false-positive profile.")
    subparsers = parser.add_subparsers(dest='action')

    parser.add_argument("-v", "--verbose", action="store_true", help="show debug prints.")
    parser.add_argument("-V", "--extra-verbose", action="store_true", help='show extra debug prints.')

    gen_parser = subparsers.add_parser('generate', help='generate the false positive rate digest (JSON format)')
    gen_parser.add_argument("-o", "--output", default="-", help="output file. '-' for stdout.")
    gen_parser.add_argument("-p", "--process-key", type=_positive_int, action="append", required=True, help="process key columns in the input CSVs.")
    gen_parser.add_argument("-i", "--instance-key", type=_positive_int, action="append", required=True, help="instance key columns in the input CSVs.")
    gen_parser.add_argument("-c", "--column", type=_positive_int, required=True, help="content column.")
    gen_parser.add_argument("-m", "--match-column", type=_positive_int, default=-1, help="the column with the match tag.")
    gen_parser.add_argument("-t", "--tag", required=True, action="append", help="profile type (simply a tag, it will not be validated)")
    gen_parser.add_argument("--match-tag", default="matched", help="the value to identify a match in the match column.")
    gen_parser.add_argument("matches", default=["-"], nargs="*", help="input files. use '-' for stdin.")

    gen_by_recipe_parser = subparsers.add_parser('generate-by-recipe', help='generate the false positive rate digest (JSON format). take input structure from a recipe.')
    gen_by_recipe_parser.add_argument("-o", "--output", default="-", help="output file. '-' for stdout.")
    gen_by_recipe_parser.add_argument("-r", "--recipe", required=True, help="the recipe file to load.")
    gen_by_recipe_parser.add_argument("-s", "--sub-recipe", help="sub-recipe to use.")
    gen_by_recipe_parser.add_argument("-m", "--match-column", type=_positive_int, default=-1, help="the column with the match tag.")
    gen_by_recipe_parser.add_argument("-t", "--tag", default=[], action="append", help="profile type (simply a tag, it will not be validated)")
    gen_by_recipe_parser.add_argument("--match-tag", default="matched", help="the value to identify a match in the match column.")
    gen_by_recipe_parser.add_argument("matches", default=["-"], nargs="*", help="input files. use '-' for stdin.")

    format_parser = subparsers.add_parser('format', help="nicely format the output generated by the 'generate' command.")
    format_parser.add_argument("-o", "--output", action="append", help="output file. '-' for stdout.")
    format_parser.add_argument("-f", "--format", action="append", choices=formatting_options.choicesList(), help="output format.\n" + formatting_options.doc())
    format_parser.add_argument("digest", nargs="*", default=["-"], help="digest file generated by 'generate'")

    content_csv_parser = subparsers.add_parser('content', help="print mismatched content in digest in CSV format")
    content_csv_parser.add_argument("-o", "--output", default="-", help="output file. '-' for stdout.")
    content_csv_parser.add_argument("digest", nargs="+", help="digest file generated by 'generate'")

    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.extra_verbose else logging.INFO if args.verbose else logging.WARN, format="%(asctime)s [%(levelname)s] %(message)s")

    if args.action == 'generate':
        generate_main(args)
    elif args.action == 'generate-by-recipe':
        generate_by_recipe_main(args)
    elif args.action == 'format':
        format_main(args)
    elif args.action == 'content':
        content_main(args)
    else:
        logging.warn("invalid command %s", args.action)
        return 1

if __name__ == "__main__":
    with contextlib.suppress(BrokenPipeError):
        sys.exit(main())

    sys.exit(1) # broken pipe...

