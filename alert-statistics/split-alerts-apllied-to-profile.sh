#!/bin/dash
for f in *alert*applied-to-profile*csv ; do 
    #echo $f | awk -F'-' '{ evt_type=$2; system("mkdir -p output/" evt_type); }'
    evt=$(echo $f | cut -d'-' -f2)
    outdir="split/$evt"
    echo "Splitting $f into $outdir..." 
    mkdir -p $outdir
    if [ "fs" = $evt ] ; then
        cat $f | awk -F'","' -v myvar=$outdir '{ print > myvar "/" $3 "-" $4 "-" $5 ".csv"}' ;
    else
        cat $f | awk -F'","' -v myvar=$outdir '{ print > myvar "/" $2 "-" $3 "-" $4 ".csv"}' ;
    fi  
    done
