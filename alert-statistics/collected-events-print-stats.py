#!/usr/bin/python3 
import re
import os,sys
import argparse 

EVT_TYPES = ['fs', 'net', 'domain', 'reg'] 



class event(object):
    def __init__(self):
        self.fs_matched = 0
        self.fs_unmatched = 0
        self.net_matched = 0
        self.net_unmatched = 0
        self.reg_matched = 0
        self.reg_unmatched = 0
        self.domain_matched = 0
        self.domain_unmatched = 0
        self.overall_matched = 0
        self.overall_matched_score = 0.0
        self.overall_unmatched = 0
        self.siem_evt_id = 0
        self.alert_name = ''
        self.severity = None
        self.process_name = ''
        self.stats = ''
    def get_matched_stats(self, evt_type = None):
        assert (evt_type is None or isinstance(evt_type, str))
        stats = 'SIEM event id: {evtid}, {proc}, {name}, {severity}\n'.format(evtid=self.siem_event_id, proc=self.process_name, name=self.alert_name, severity=self.severity)
        if evt_type is None or 'net' == evt_type.lower():
            net_total = self.net_matched+self.net_unmatched
            stats +=  "Network - Total Events: {tot}, Matched: {m}%, UnMatched: {um}%\n".format(tot=net_total,
                                                                                                m=100*(self.net_matched)/(net_total if net_total > 0 else 1), 
                                                                                                um=100*(self.net_unmatched)/(net_total if net_total > 0 else 1))
        if evt_type is None or 'domain' == evt_type.lower():
            domain_total = self.domain_matched+self.domain_unmatched
            stats += "Domain - Total Events: {tot}, Matched: {m}%, UnMatched: {um}%\n".format(tot=domain_total,
                                                                                            m=100.0*(self.domain_matched)/(domain_total if domain_total > 0 else 1), 
                                                                                            um=100*(self.domain_unmatched)/(domain_total if domain_total > 0 else 1))
        if evt_type is None or 'reg' == evt_type.lower():
            reg_total = self.reg_matched+self.reg_unmatched
            stats +=  "Registry - Total Events:{tot},  Matched: {m}%, UnMatched: {um}%\n".format(tot=reg_total,
                                                                                                m=100*(self.reg_matched)/(reg_total if reg_total > 0 else 1), 
                                                                                                um=100*(self.reg_unmatched)/(reg_total if reg_total > 0 else 1))
        if evt_type is None or 'fs' == evt_type.lower():
            fs_total =  self.fs_matched+self.fs_unmatched
            stats += "File System -  Total Events: {tot},  Matched: {m}%, UnMatched: {um}%\n".format(tot=fs_total,
                                                                                                    m=100*(self.fs_matched)/(fs_total if fs_total > 0 else 1), 
                                                                                                    um=100*(self.fs_unmatched)/(fs_total if fs_total > 0 else 1))
        if evt_type is None or 'all' == evt_type.lower():
            overall_total =  self.overall_matched+self.overall_unmatched
            stats += "Overall -  Total Events: {tot}, Matched: {m}%, UnMatched: {um}%\n".format(tot=overall_total, 
                                                                                                m=100*(self.overall_matched)/(overall_total if overall_total > 0 else 1), 
                                                                                                um=100*(self.overall_unmatched)/(overall_total if overall_total > 0 else 1))
            self.overall_matched_score = self.overall_matched/(overall_total if overall_total > 0 else 1)

        self.stats = stats
        return stats

    def add_evt(self, evt_type, count, matched):
        if 'net' == evt_type:
            if matched:
                self.net_matched += count
                self.overall_matched += count
            else:
                self.overall_unmatched += count
                self.net_unmatched += count
        if 'domain' == evt_type:
            if matched:
                self.domain_matched += count
                self.overall_matched += count
            else:
                self.overall_unmatched += count
                self.domain_unmatched += count
        if 'reg' == evt_type:
            if matched:
                self.reg_matched += count
                self.overall_matched += count
            else:
                self.overall_unmatched += count
                self.reg_unmatched += count
        if 'fs' == evt_type:
            if matched:
                self.fs_matched += count
                self.overall_matched += count
            else:
                self.overall_unmatched += count
                self.fs_unmatched += count

def process_event_lines(lines, f_out):
    # siem_event_id: 92117
    # is_archive: f
    # alert_name: BIOC (taskhost process masquerading)
    # severity: HIGH
    # host_id: mlnx78679a7779d8eee4eea9b5e8873ea83e
    # secdo_process_id: 2efc42c0e9b8c344ba17898dac81f44c
    # instance_id: 2089449948
    e = event()
    # print (lines)
    try:
        e.siem_event_id = int(lines[0].split(':')[1].strip())
        e.alert_name = lines[2].split(':')[1].strip()
        e.severity = lines[3].split(':')[1].strip()
        e.process_name = ''
    except Exception as excpt:
        print (lines)
        print ("Failed processing event, {excpt}".format(excpt=excpt))
    
    if 7 == len(lines):
        return None

    matched = 0
    unmatched = 0         
    process_name = ''
    for l in lines[7:]:
        if l.strip().endswith('.csv'):
            if 0 < matched or 0 < unmatched:
                e.add_evt(evt_type, matched, True)
                e.add_evt(evt_type, unmatched, False)
                matched = 0
                unmatched = 0
            evt_type = l.split('-')[1].strip()
            continue
        l = l.split(',')
        process_name = l[0].replace('"', '').strip() 
        if 0 == len(e.process_name):
            e.process_name = process_name
        else:
            assert(e.process_name == process_name)
        res =  l[-1].replace('"', '').strip()
        if 'unmatched' == res:
            unmatched += 1
        elif 'matched' == res:
            matched += 1
        else:
            print (lines)
            raise Exception(res)
   
    if (0 < e.overall_matched or  0 < e.overall_unmatched) and (0 < (e.fs_matched + e.fs_unmatched)):
        return e
        # f_out.write(e.get_matched_stats()+'\n')
    return None

def process_lines(lines, f_out):
    events = []
    last_idx = 0
    i = 0
    while last_idx < len(lines):
        while i < len(lines):
            if lines[i].startswith('siem_event_id:'):
                break
            else:
                i += 1
        j = i + 1
        while j < len(lines):
            if lines[j].startswith('siem_event_id:'):
                break
            else:
                j += 1
        e = process_event_lines(lines[i:j], f_out)
        if e is not None:
            # Calculate
            e.get_matched_stats()
            # Add to list
            events.append(e)
        last_idx = j
        i = j

    return events


def sort_event(e):
    return e.overall_matched_score


def output_sorted_events(events, f_out):
    events.sort(key = sort_event)
    
    for e in events:
        f_out.write(e.stats+'\n')


def main():
    
    ap = argparse.ArgumentParser(description = 'Print matched/unmatched statistics between a siem event and the present data about it.' + \
    '\nAccepts the outout of collect-siem-event-info')
    ap.add_argument("-i", "--input-file", default='-', help="The input file name", required=False)
    ap.add_argument("-o", "--output-file", default='-', help="Output to file", required=False)
    args = ap.parse_args()

    f_in = None
    f_out = None
    if '-' == args.input_file:
        f_in = sys.stdin
    else:
        f_in = open(args.input_file, 'r').readlines()
    if '-' == args.output_file:
        f_out = sys.stdout
    else:
        f_out = open(args.output_file, 'w')

    print ("Going to process {0} lines...".format(len(f_in)))
    events = process_lines(f_in, f_out)
    output_sorted_events(events, f_out)
   
    
if __name__=="__main__":
    main()
