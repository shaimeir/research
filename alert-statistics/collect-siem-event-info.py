#!/usr/bin/python3

import argparse
import os, sys, re
import csv
import multiprocessing
import time

def main():
    global file_to_scan
    num_processors = multiprocessing.cpu_count()
    ap = argparse.ArgumentParser(description = 'Collect profile related events for a siem alert.' + \
    '\nAccepts the siem-events csv file and a list of csv files containing events to search through.')
    ap.add_argument("-s", "--siem-events", default="-", help="The siem alerts (events) to process", required=True)
    ap.add_argument("-e", "--event-file", nargs='+', help="A list of csv files containing the events to go through", required=True)
    ap.add_argument("-o", "--output-file", default='-', help="The output file name", required=False)
    ap.add_argument("-i", "--start-from-siem-event-id", help="SIEM event id to start from. Use it if the previous run stopped unexpectedly")
    args = ap.parse_args()
    
    
    if os.path.isfile(args.output_file):
        ans = input("Output file exists. Delete? (y/n)")
        if 'y' in ans.lower():
            try:
                os.remove(args.output_file)
            except Exception as e:
                print (e)
    with open(args.siem_events, 'r') as siem_in:
        csv_reader = csv.DictReader(siem_in, delimiter='\t')
        for i in range(len(args.event_file)):
            try:
                file_to_scan.append(open(args.event_file[i]).readlines())
            except Exception as e:
                print ("Error reading input file: {err}".format(err=e))
        
        event_maps = map_event_tuples(args.event_file, file_to_scan)

        completed_events = 0
        total_events = len(open(args.siem_events, 'r').readlines())
        resumed = False if args.start_from_siem_event_id else True
        for siem_evt in csv_reader:
            events = []
            workdone = float(completed_events)/total_events
            print("\rProgress: [{0:50s}] {1:.1f}%".format('#' * int(50 * workdone), workdone*100), end="", flush=True)
            if not all_elemnts_present([siem_evt['host_id']] + [siem_evt['secdo_process_id']] + [siem_evt['instance_id']]):
                # print ("Skipping line with missing elements")
                continue
            
            if False == resumed and args.start_from_siem_event_id:
                if not (siem_evt['siem_event_id'].strip() == args.start_from_siem_event_id):
                    completed_events += 1
                    continue
                else:
                    resumed = True


            hid=siem_evt['host_id']
            spid=siem_evt['secdo_process_id']
            iid=siem_evt['instance_id']
            
            search_t = re.compile(r'^.*"{hid}","{spid}","{iid}".*$'.format(hid=hid, spid=spid, iid=iid), re.I)
            with open(args.output_file, 'a') as outfile:
                evt_str = ''
                for item in siem_evt.items():
                    evt_str += item[0] + ': ' + item[1] + '\n'
                outfile.write(evt_str)
                # Check the maps before scanning the file
                for m, evt_type in event_maps.items():
                    try:
                        x = evt_type[hid][spid]
                        assert iid in x
                    except:
                        continue
                    outfile.write(m+'\n')
                    try:
                        evt = m.split('-')[1]
                        evt_file = r'split/{evt}/{hid}-{spid}-{iid}.csv'.format(evt=evt, hid=hid, spid=spid, iid=iid)
                        data = open(evt_file, 'r').read()
                    except Exception as no_data_e:
                        # print ("Found tuple in map but could not find the file {f}".format(f=evt_file))
                        continue
                    outfile.write(data)
            completed_events += 1
    print ("\n")
file_to_scan = []


def all_elemnts_present(elements):
    for e in elements:
        if e is None or "" == e.strip():
            return False
    return True


def map_event_tuples(event_files, file_to_scan):
    maps = {}
    get_hid_sid_iid_re = re.compile(r'^.*?,"(\w+?[0-9a-f]{32,})","([0-9a-f]{32})","(\d+)",.*$', re.I)
    print ("(*) Starting maps generation")
    start_t = time.time()
    for i in range(len(event_files)):
        file_name = event_files[i]
        print ("(*) Processing file {file}".format(file=file_name))
        maps.setdefault(file_name, {})
        f = file_to_scan[i]
        for line in f:
            m = re.match(get_hid_sid_iid_re, line)
            if m:
                maps[file_name].setdefault(m.group(1), {})
                maps[file_name][m.group(1)].setdefault(m.group(2), set())
                maps[file_name][m.group(1)][m.group(2)] |= set([m.group(3)])
    print ("(*) Maps generation completed in {elps} seconds".format(elps=time.time() - start_t))
    return maps

    pass


if "__main__"==__name__:
    main()
