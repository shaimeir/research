#!/usr/bin/python3
import argparse
import requests
import pathlib
import datetime
import logging


def main(url, company, dest_folder, file_name):
    params = {'company': company}
    res = requests.post(url, params=params)
    j = res.json()

    try:
        biocs = j['reply']
        path = pathlib.Path(dest_folder, file_name)
        with open(path, 'w') as fout:
            fout.write(biocs)
            logging.info(f'Saved biocs in path: {path}')
    except Exception as e:
        raise e


if __name__ == '__main__':
    logging.basicConfig(format="%(asctime)s %(message)s", level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--company", default='bioc',
                        help="The company to pull the bioc rules from. Company must exist")
    parser.add_argument("-s", "--server", default='10.0.0.17', help="The backend server to query")
    parser.add_argument("-p", "--port", default="5001", help="The backend port to query")
    parser.add_argument("-d", "--dest", help="The destination directory")
    parser.add_argument('-f', '--file_name', default=f'bioc_rules_{datetime.datetime.now():%Y-%m-%d}.bin',
                        help='file name for the biocs file')
    args = parser.parse_args()

    url = f'http://{args.server}:{args.port}/ioc/export_iocs/'

    if not args.dest:
        dest_folder = pathlib.Path(pathlib.Path.home(), 'biocs_bins', args.company)
    else:
        dest_folder = pathlib.Path(args.dest)
    dest_folder.mkdir(parents=True, exist_ok=True)

    main(url, args.company, dest_folder, args.file_name)
