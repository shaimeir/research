#!/usr/bin/env bash

set -eu

# this script handles a given uploaded file, unpacks it and dispatches the various handlers required to process it's content type.

readonly scripts_dir="$(dirname $(readlink -f ${BASH_SOURCE[0]}))"
readonly prog_name="$(basename ${BASH_SOURCE[0]})"
readonly ini_query="${scripts_dir}/script-utilities/ini-query.py"
readonly ini_query_field="${scripts_dir}/script-utilities/ini-query-field.py"
readonly summary_script="${scripts_dir}/summary-file/summary-ops.py"

# the expected summary file name
readonly summary_file_name="content.summary"
readonly configuration_handlers_subsection="handlers"
readonly configuration_handlers_config_subsection="handlers-config"

function trace()
{
    echo "$(date): ${@}" >&2
}

function print_help()
{
    (
        cat <<-EOFSEQ
            process a product file uploaded to the data science server.

            usage: ${BASH_SOURCE[0]} [-h] [-x] [-k] [-u] [-c CONFIGURATION-FILE] [-s CONFIGURATION-SUB-SECTION [...]] [-d TAG HANDLER [...]] [-g TAG CONFIG-FILE [...]] [-o EXTRACTION-DIRECTORY] PRODUCT-FILE
            | * -h - print this help.
            | * -x - turn bash's -x flag on.
            | * -k - keep the unpacked content and all intermediate files.
            | * -u - fail the program if a content summary file that's either malformulated or has an unsupported tag.
            | * -c CONFIGURATION-FILE - the configuration file to use. any missing command line option will be fetched from the configuration. (optional)
            | * -s CONFIGURATION-SUB-SECTION - use a sub-section of the configuration file. use multiple -s flags to nest the sub-sections. (aggregate)
            | * -d TAG HANDLER - run HANDLER for a summary file with a tag TAG. (aggregate)
            | * -g TAG CONFIG-FILE - use CONFIG-FILE when handling content summary files with a tag TAG. (aggregate)
            | * -o EXTRACTION-DIRECTORY - a directory to unpack tarballs into.
            | * PRODUCT-FILE - the product file to handle.
EOFSEQ
    ) | sed -r 's/^\s*\|?//'
}

function perror()
{
    echo "ERROR: ${@}" >&2
    print_help
    exit 1
}

function debug_trace()
{
    [ -n "${DEBUG_TRACES:-}" ] && trace "DEBUG: ${@}"
    return 0
}

# globals...
extraction_dir=""
keep_intermediate=""

function cleanup()
{
    [ -n "${extraction_dir}" -a -z "${keep_intermediate}" ] && rm -rf "${extraction_dir}"
    true # just make sure the cleanup always succeeds
}

trap cleanup EXIT

function main()
{
    local configuration_file=""
    local configuration_section=""
    local _extraction_dir=""
    local x_flag=""
    local error_handler="trace"
    local fail_on_error=""
    declare -A handlers
    declare -A handler_configs

    while getopts ":hxukc:s:o:d:g:" option
    do
        case "${option}" in
            "h") print_help
                 exit 0
                 ;;

            "x") set -x
                 x_flag="1"
                 ;;

            "u") fail_on_error="1"
                 ;;

            "k") keep_intermediate="1"
                 ;;

            "c") configuration_file="${OPTARG}"
                 ;;

            "s") configuration_section="${configuration_section} ${OPTARG}"
                 ;;

            "o") _extraction_dir="${OPTARG}"
                 ;;

            "d") local tag="${OPTARG}"
                 local _handler_index=$((OPTIND+2))

                 handlers["${tag}"]="${!OPTIND}"

                 let "OPTIND += 1" # this switch accepts two arguments
                 ;;

            "g") local tag="${OPTARG}"
                 local _handler_index=$((OPTIND+2))

                 handler_configs["${tag}"]="${!OPTIND}"

                 let "OPTIND += 1" # this switch accepts two arguments
                 ;;

            *) perror "unknown option ${OPTARG}"
        esac
    done
    shift $((OPTIND-1))

    if [ "${#}" -eq 0 ]
    then
        perror "you must specify a product file to handle."
    fi

    local product_file="${1}"

    # load missing arguments
    trace "loading arguments from configuration."
    _extraction_dir=$("${ini_query_field}" -f "extraction-dir" -r "${_extraction_dir}" "${configuration_file}" ${configuration_section})

    fail_on_error=$("${ini_query_field}" -eqf "fail-on-error" -r "${fail_on_error}" "${configuration_file}" ${configuration_section})
    [ -n "${fail_on_error}" ] && error_handler="perror"

    keep_intermediate=$("${ini_query_field}" -eqf "keep-intermediate" -r "${keep_intermediate}" "${configuration_file}" ${configuration_section})
    x_flag=$("${ini_query_field}" -eqf "bash-x" -r "${x_flag}" "${configuration_file}" ${configuration_section}) # bash -x flag
    if [ -n "${x_flag}" ]
    then
        set -x
    fi

    trace "creating work directory structure in ${_extraction_dir}"
    mkdir -p "${_extraction_dir}"
    extraction_dir=$(mktemp -d "${_extraction_dir}/product-extraction-XXXXXXXXXX")
    trace "work path is ${extraction_dir}"

    unpack_dir="${extraction_dir}/unpack-dir"
    mkdir -p "${unpack_dir}"

    # process the product
    trace "unpacking ${product_file} to ${unpack_dir}"
    tar -C "${unpack_dir}" -xzf "${product_file}"

    summary_list="${extraction_dir}/summary-list"
    trace "listing content summary files to ${summary_list}"
    find "${unpack_dir}" -type f -name "${summary_file_name}" >"${summary_list}"

    trace "handling summary files"
    while read summary_file
    do
        trace "handling summary file ${summary_file}"

        # make sure the summary file is indeed a well formulated one
        if ! "${summary_script}" validate-format "${summary_file}"
        then
            "${error_handler}" "${summary_file} is not a well formulated summary file."
            continue
        fi

        local summary_tag=$("${summary_script}" get-tag "${summary_file}")

        # get the handler
        local handler="${handlers[${summary_tag}]:-}"
        [ -z "${handler}" -a -f "${configuration_file}" ] && handler=$("${ini_query}" -q -k "${summary_tag}" "${configuration_file}" ${configuration_section} "${configuration_handlers_subsection}")

        # get the handler's configuration file
        local handler_config="${handler_configs[${summary_tag}]:-}"
        [ -z "${handler_config}" -a -f "${configuration_file}" ] && handler_config=$("${ini_query}" -q -k "${summary_tag}" "${configuration_file}" ${configuration_section} "${configuration_handlers_config_subsection}")

        if [ -n "${handler_config}" ]
        then
            handler_config="-c ${handler_config}"
        fi

        # if we have a handler, run it!
        if [ -n "${handler}" ]
        then
            trace "using ${handler} to handle ${summary_file}"
            if ! "${handler}" ${handler_config} "${summary_file}"
            then
                "${error_handler}" "ERROR: failed during the handling of ${summary_file} by ${handler}"
            fi
        else
            "${error_handler}" "can't handle summary tag '${summary_tag}' in ${summary_file}"
        fi
    done <"${summary_list}"

    trace "all summary files were handled."
}

main ${@}

