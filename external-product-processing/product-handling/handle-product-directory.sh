#!/usr/bin/env bash

set -eu

# handle a directory that contains products and process them individually via handle-product.sh.

readonly scripts_dir="$(dirname $(readlink -f ${BASH_SOURCE[0]}))"
readonly prog_name="$(basename ${BASH_SOURCE[0]})"
readonly product_handler="${scripts_dir}/handle-product.sh"
readonly ini_query="${scripts_dir}/script-utilities/ini-query.py"
readonly ini_query_field="${scripts_dir}/script-utilities/ini-query-field.py"
readonly pyenv_validation_script="${scripts_dir}/script-utilities/test-python-requirements.py"
readonly summary_script="${scripts_dir}/summary-file/summary-ops.py"

# by default only process products modified at least one minute ago to make sure we don't accidentally
# touch products that are still being written.
readonly default_file_modification_minutes_buffer=1
readonly default_config_file="/etc/secdo/research/upload-directories-handler.ini"
readonly locks_dir="/tmp/research-locks/upload-management"

function trace()
{
    echo "$(date): ${@}" >&2
}

function print_help()
{
    local product_handler_name=$(basename "${product_handler}")

    (
        cat <<-EOFSEQ
            process a directory of products uploaded to the data science server.
            most the options are passed directly to ${product_handler_name}.

            usage: ${BASH_SOURCE[0]} [-h] [-x] [-k] [-K] [-w MIN-MODIFICTION-INTERVAL] [-v PYTHON-VIRTUAL-ENVIRONMENT] [-u] [-c CONFIGURATION-FILE] [-s CONFIGURATION-SUB-SECTION [...]] [-d TAG HANDLER [...]] [-g TAG CONFIG-FILE [...]] [-o EXTRACTION-DIRECTORY] PRODUCT-DIRECTORY [...]
            | * -h - print this help.
            | * -x - turn bash's -x flag on.
            | * -k - keep the unpacked content and all intermediate files.
            | * -K - don't modify product files at all (don't rename or remove).
            | * -w MIN-MODIFICTION-INTERVAL - handle only product files that were modified at least MIN-MODIFICTION-INTERVAL minutes ago.
            | * -v PYTHON-VIRTUAL-ENVIRONMENT - use the specified python virtual environment.

            ${product_handler_name} options:
            | * -u - fail ${product_handler_name} if a content summary file that's either malformulated or has an unsupported tag.
            | * -c CONFIGURATION-FILE - the configuration file to use with ${product_handler_name}. any missing command line option will be fetched from the configuration. (optional)
            | * -s CONFIGURATION-SUB-SECTION - use a sub-section of the configuration file. use multiple -s flags to nest the sub-sections. (aggregate)
            | * -d TAG HANDLER - associate the TAG tag with the HANDLER handler in . (aggregate)
            | * -g TAG CONFIG-FILE - use CONFIG-FILE when handling content summary files with a tag TAG. (aggregate)
            | * -o EXTRACTION-DIRECTORY - a directory to unpack tarballs into.
            | * PRODUCT-DIRECTORY - list of product directories and files to handle.
EOFSEQ
    ) | sed -r 's/^\s*\|?//'
}

function perror()
{
    echo "$(date) ERROR: ${@}" >&2
    print_help >&2
    exit 1
}

function debug_trace()
{
    [ -n "${DEBUG_TRACES:-}" ] && trace "DEBUG: ${@}"
    return 0
}

function load_python_virtual_env()
{
    local cmd_env="${1}"
    local configuration_file="${2}"

    local virtual_env=""

    # if possible use the command line given env
    if [ -n "${cmd_env}" ]
    then
        virtual_env="${cmd_env}"

    # let's try the configuration then
    elif [ -f "${configuration_file}" ]
    then
        virtual_env=$(awk -F'=' '
            $0 ~ /^\s*virtualenv\s*=/ {
                virtualenv=$2
                gsub(/(^\s*)|(\s*)$/, "", virtualenv)
                print virtualenv
                exit 0
            }
        ' "${configuration_file}")
    fi

    local virtual_env_loader="${virtual_env}/bin/activate"

    if [ -z "${virtual_env}" -o ! -f "${virtual_env_loader}" ]
    then
        trace "virtual env not specified or no activation script found in specified path - running outside of virtual environment."
    else
        set +eu
        source "${virtual_env_loader}"
        set -eu

        trace "loaded python virtual environment from ${virtual_env}"
    fi

    if "${pyenv_validation_script}" -s "3.6" -P
    then
        return 0
    else
        trace "python is not setup properly!"
        return 1
    fi
}

# cleanup routine
active_lock="" # used by lock_path
intermediate_dir=""
keep_intermediate=""

function cleanup()
{
    [ -n "${active_lock}" ] && rm -f "${active_lock}"
    [ -n "${intermediate_dir}" -a -z "${keep_intermediate}" ] && rm -rf "${intermediate_dir}"
    true # just make sure the cleanup always succeeds
}

trap clenaup EXIT

# synchronization routine - makes sure we don't process the same file twice
function lock_path()
{
    local path="${1}"

    local lock="${locks_dir}/$(readlink -f ${path} | md5sum | cut -d' ' -f1)"

    mkdir -p "${locks_dir}"

    exec 100>${lock}

    if flock -n 100
    then
        trace "obtained lock for ${path}"
        active_lock="${active_lock}"
    else
        exec 100>/dev/null
        trace "can't obtain lock for ${path}"
    fi
}

function unlock_path()
{
    local path="${1}"
    local lock="${locks_dir}/$(readlink -f ${path} | md5sum | cut -d' ' -f1)"

    # release the lock
    rm -f "${lock}"
    active_lock=""
    exec 100>/dev/null
}

trap cleanup EXIT

function main()
{
    local x_flag=""
    local keep_product_files=""
    local min_modifiction_interval=""
    local virtual_env=""
    local configuration_file="${default_config_file}"
    local configuration_section=""

    local configuration_section_switches=""
    local _extraction_dir=""
    local error_handler="trace"
    local fail_on_error=""
    local handlers=""
    local handler_configs=""

    while getopts ":hxukKv:w:c:s:o:d:g:" option
    do
        case "${option}" in
            "h") print_help
                 exit 0
                 ;;

            "x") set -x
                 x_flag="1"
                 ;;

            "K") keep_product_files="1"
                 ;;

            "w") min_modifiction_interval="${OPTARG}"
                 ;;

            "v") virtual_env="${OPTARG}"
                 ;;

            "u") fail_on_error="-u"
                 ;;

            "k") keep_intermediate="1"
                 ;;

            "c") configuration_file="${OPTARG}"
                 ;;

            "s") configuration_section="${configuration_section} ${OPTARG}"
                 configuration_section_switches="${configuration_section_switches} -s ${OPTARG}"
                 ;;

            "o") _extraction_dir="${OPTARG}"
                 ;;

            "d") local tag="${OPTARG}"
                 local _handler_index=$((OPTIND+2))

                 handlers="${handlers} -d ${tag} ${!OPTIND}"

                 let "OPTIND += 1" # this switch accepts two arguments
                 ;;

            "g") local tag="${OPTARG}"
                 local _handler_index=$((OPTIND+2))

                 handler_configs="${handler_configs} -g ${tag} ${!OPTIND}"

                 let "OPTIND += 1" # this switch accepts two arguments
                 ;;

            *) perror "unknown option ${OPTARG}"
        esac
    done
    shift $((OPTIND-1))

    if [ "${#}" -eq 0 ]
    then
        perror "you must specify a products directory to handle."
    fi

    local product_dirs="${@}"

    # load a python virtual environment and validate the python setup
    trace "loading python virtual environment"
    load_python_virtual_env "${virtual_env}" "${configuration_file}"

    # load values from the configuration where needed
    trace "loading configuration values from ${configuration_file} [${configuration_section}]"

    _extraction_dir=$("${ini_query_field}" -f "extraction-dir" -r "${_extraction_dir}" "${configuration_file}" ${configuration_section})

    keep_intermediate=$("${ini_query_field}" -eqf "keep-intermediate" -r "${keep_intermediate}" "${configuration_file}" ${configuration_section})
    [ -n "${keep_intermediate}" ] && keep_intermediate="-k"
    keep_product_files=$("${ini_query_field}" -eqf "keep-products" --override="${keep_intermediate}" "${configuration_file}" ${configuration_section})

    x_flag=$("${ini_query_field}" -eqf "bash-x" -r "${x_flag}" "${configuration_file}" ${configuration_section}) # bash -x flag
    if [ -n "${x_flag}" ]
    then
        set -x
        x_flag="-x"
    fi

    min_modifiction_interval=$("${ini_query_field}" -f "min-modification-interval" \
                                                     -r "${min_modifiction_interval}" \
                                                     -d "${default_file_modification_minutes_buffer}" \
                                                     "${configuration_file}" ${configuration_section})

    # create the intermediate working directory so we can create temp files
    trace "creating directory structure at ${_extraction_dir}"
    mkdir -p "${_extraction_dir}"
    intermediate_dir=$(mktemp -d "${_extraction_dir}/upload-dir-handler-XXXXXXXXXX")
    trace "work directory is ${intermediate_dir}"

    # list all product files
    trace "listing product files"
    local products_list="${intermediate_dir}/products-list"
    local older_than=$(date -d "now - ${min_modifiction_interval} minutes")

    # make sure that if some of the product dirs don't exist we won't terminate because find will return
    # with failure so use '|| true' (all files will be properly found anyway).
    (find ${product_dirs} -type f -name "*.tar.gz" ! -newermt "${older_than}" || true) >"${products_list}"

    # handling the bastards
    trace "handling the found products ($(wc -l ${products_list} | cut -d' ' -f1) products found)"
    while read product_file
    do
        if ! lock_path "${product_file}"
        then
            trace "${product_file} is already locked! - skipping"
            continue
        fi

        if [ ! -f "${product_file}" ]
        then
            trace "${product_file} no longer exists - skipping"
            unlock_path "${product_file}"
            continue
        fi

        trace "handling ${product_file}"
        if "${product_handler}" ${x_flag} \
                                ${keep_intermediate} \
                                -c "${configuration_file}" \
                                ${configuration_section_switches} \
                                -o "${_extraction_dir}" \
                                ${fail_on_error} \
                                ${handlers} \
                                ${handler_configs} \
                                "${product_file}"
        then
            trace "successfuly handled ${product_file}"
        else
            trace "failed to handle ${product_file}"
        fi

        # if -K was specified then keep the product files
        if [ -n "${keep_product_files}" ]
        then
            trace "keeping ${product_file}"

        # -K wasn't specified so remove the product files if possible
        elif [ -z "${keep_intermediate}" ]
        then
            if ! rm "${product_file}"
            then
                trace "can't remove ${product_file} - flagging it with a '.cant.remove' suffix"
                mv "${product_file}" "${product_file}.cant.remove" || trace "couldn't rename ${product_file} to ${product_file}.cant.remove"
            fi

        else
            mv "${product_file}" "${product_file}.processed" || trace "couldn't rename ${product_file}.processed"
        fi

        unlock_path "${product_file}"
    done <"${products_list}"
}

main ${@}

