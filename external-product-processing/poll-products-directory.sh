#!/usr/bin/env bash

set -eu

# this script polls a list of directories and runs product handling on them.
# this is meant to be used as a cron task to empty out the ftps's upload directory from products.

readonly program_dir="$(dirname $(readlink -f ${BASH_SOURCE[0]}))"
readonly product_dir_handler_script="${program_dir}/product-handling/handle-product-directory.sh"

readonly default_tracked_dirs_file="/etc/secdo/research/polled-product-directories"

readonly default_polling_interval="5" # poll every 5 minutes

function print_help()
{
    (
        cat <<-EOFSEQ
        discover and handle products in a pre-listed directories.
        this script is best adjusted to run as a cron job.

        usage: $(basename ${BASH_SOURCE[0]}) [-h] [-x] [-i] [-u USER] [-m EXECUTION-INTERVAL-IN-MINUTES] [-d TRACKED-DIRECTORIES-FILE] [-c PRODUCT-HANDLING-CONFIGURATION] [-s PRODUCT-HANDLING-CONFIGURATION-SUBSECTION]
        | * -h - print this help.
        | * -x - turn on bash's -x flag.
        | * -d TRACKED-DIRECTORIES-FILE - a text files that contains paths to poll. (default: ${default_tracked_dirs_file})
        | * -c PRODUCT-HANDLING-CONFIGURATION - the configuration file to use for $(basename ${product_dir_handler_script}). (optional)
        | * -s PRODUCT-HANDLING-CONFIGURATION-SUBSECTION - use a specific sub-section of the configuration file passed with -c. (aggregate)

        installation options:
        | * -i - install a cron task for this script.
        | * -u USER - the cron task will execute as the user USER.
        | * -m EXECUTION-INTERVAL-IN-MINUTES - execute every EXECUTION-INTERVAL-IN-MINUTES minutes. (default: ${default_polling_interval} minutes)
EOFSEQ
    ) | sed -r 's/^\s*\|?//'
}

function trace()
{
    echo "$(date): ${@}" >&2
}

function perror()
{
    echo "$(date) ERROR: ${@}" >&2
    print_help >&2
    exit 1
}

tracked_directories_file="${default_tracked_dirs_file}"
configuration_switch=""
configuration_subsection_switches=""

install=""
minutes_interval="${default_polling_interval}"
cronjob_user=""

while getopts ":hxiu:m:d:c:s:" option
do
    case "${option}" in
        "h") print_help
             exit 0
             ;;

        "x") set -x
             ;;

        "d") tracked_directories_file="$(readlink -f ${OPTARG})"
             ;;

        "c") configuration_switch="-c $(readlink -f ${OPTARG})"
             ;;

        "s") configuration_subsection_switches="${configuration_subsection_switches} -s ${OPTARG}"
             ;;

        "i") install="1"
             ;;

        "m") minutes_interval="${OPTARG}"
             ;;

        "u") cronjob_user="-u ${OPTARG}"
             ;;

        *) perror "unknown option passed in the command line: ${OPTARG}"
    esac
done

# if this is an installation call
if [ -n "${install}" ]
then
    (
        crontab ${cronjob_user} -l 2>/dev/null || true
        echo "*/${minutes_interval} * * * * $(readlink -f ${BASH_SOURCE[0]}) -d ${tracked_directories_file} ${configuration_switch} ${configuration_subsection_switches} >/tmp/test-log 2>&1"
    ) | tee /dev/stderr | crontab ${cronjob_user} -

# this is an execution rather than an installation
elif [ "$(grep -v '^\s*$' ${tracked_directories_file} | wc -l)" -gt 0 ]
then
    "${product_dir_handler_script}" ${configuration_switch} ${configuration_subsection_switches} $(cat ${tracked_directories_file}) || trace "handling of the paths listed in ${tracked_directories_file}"

else
    trace "${tracked_directories_file} is empty"
fi

