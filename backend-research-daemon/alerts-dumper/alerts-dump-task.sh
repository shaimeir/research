#!/usr/bin/env bash

set -eu

# this script dumps the alerts from the local PostgreSQL and uploads them to a remote PostgreSQL service.
#
# the script can take it's command line arguments from a configuration file (see ${default_config_file} for the default configuration location).
# the configuration file is an INI file with one section and the following fields:
#
#   # the location of a python3 virtual environment to use. non is used if this isn't specified (-e).
#   # this can be anywhere in the file.
#   virtualenv=/path/to/python3/virtual/env # optional
#
#   # a required section
#   [dump-alerts]
#
#   # if this flag is present (regardless of value) bash's -x flag will be turned on. (-x)
#   bash-x=1 # optional
#
#   # if this flag is present (regardless of value) intermediate directories will not be removed. (-k)
#   keep-intermediate=1 # optional
#
#   # a directory for the intermediate and final products that's deleted on termination. (-o)
#   output=/path/to/storage/alert-dumps/
#
#   # a list of companies to dump the alerts for. (-c)
#   companies=company-A, company-B
#
#   # time range filters. (-a, -b)
#   after=2w # 2 weeks ago (optional)
#   before=1w # 1 week ago (optional)
#
#   # FTPS client's (local machine) PEM that holds it's private key + certificate. (-f)
#   ftp-credentials=/path/to/server/generated/certificate-and-key.pem
#   # the expected certificate of the FTPS server (used to identify and validate the the server). (-i)
#   ftp-server-cert=/path/to/ftps/server/certificate.pem
#   # the FTP server's IP address. (-r)
#   ftp-server=1.2.3.4
#   # the directory in the FTPS server to upload products to (usually the main directory will not have write permissions). (-t)
#   ftp-root-dir=root # optional
#
#   # the IP address of the postgres server to connect to. (-s)
#   postgresql=127.0.0.1 # optional
#   # the port postgres is listening on. (-p)
#   postgresql-port=5432 # optional
#   # the user to log into postgres with. (-u)
#   postgresql-user=postgres # optional
#

readonly scripts_dir="$(dirname $(readlink -f ${BASH_SOURCE[0]}))"
readonly prog_name="$(basename ${BASH_SOURCE[0]})"
readonly ini_query="${scripts_dir}/script-utilities/ini-query.py"
readonly ini_query_field="${scripts_dir}/script-utilities/ini-query-field.py"
readonly test_python_requirements_script="${scripts_dir}/script-utilities/test-python-requirements.py"
readonly query_generation_script="${scripts_dir}/scheme-dumper-v3/dump-scheme-v3.py"
readonly summary_script="${scripts_dir}/summary-file/summary-ops.py"

readonly default_config_file="/etc/secdo/research/alerts-dumper.ini"
readonly locks_directory="/tmp/.research-locks"

readonly default_source_postgresql="127.0.0.1"
readonly default_source_postgresql_port="5432"
readonly default_source_postgresql_user="postgres"

readonly configuration_section="dump-alerts"

readonly default_ftp_server="127.0.0.1"
readonly default_ftp_root_dir="root"

function trace()
{
    echo "$(date) ${@}" >&2
}

function debug_trace()
{
    [ -n "${DEBUG_TRACES:-}" ] && trace "DEBUG: ${@}"
    return 0
}

function print_help()
{
    (
        cat <<-EOFSEQ
            dump the alerts for a given list of companies stored on the local machine and upload them to a remote PostgreSQL server.

            usage: ${BASH_SOURCE[0]} [-h] [-x] [-o WORK-DIRECTORY] [-e PYTHON-VIRTUAL-ENV] [-r RESEARCH-FTPS-SERVER] [-t RESEARCH-FTPS-ROOT-DIRECTORY] [-i FTPS-SERVER-CERTIFICATE] [-f FTPS-PRIVATE-KEY-AND-CERT] [-s SOURCE-POSTGRESQL] [-p SOURCE-POSTGRESQL-PORT] [-u SOURCE-POSTGRESQL-USER] [-c COMPANY] CENTRAL-CONFIGURATION-FILE
            | * -h - print this help.
            | * -x - set bash's -x flag.
            | * -k - keep intermediate files.
            | * -o WORK-DIRECTORY - the work directory to use for intermediate products.
            | * -e PYTHON-VIRTUAL-ENV - the python virtual environment to use.
            | * -r FTPS-SERVER - the address of the research FTPS server. (default: ${default_ftp_server})
            | * -t FTPS-ROOT-DIRECTORY - the subdirectory in the FTPS server to upload the generated dumps to. (default: ${default_ftp_root_dir})
            | * -i FTPS-SERVER-CERTIFICATE - the certificate of the FTPS server to authenticate the server with.
            | * -f FTPS-PRIVATE-KEY-AND-CERT - the private key + certificate (in a single PEM file) to use when uploading products via FTPS.
            | * -s SOURCE-POSTGRESQL - the PostgreSQL to dump the alerts from.
            | * -p SOURCE-POSTGRESQL-PORT - the port the PostgreSQL listens on.
            | * -u SOURCE-POSTGRESQL-USER - the user to use to connect to the source PostgreSQL.
            | * -c COMPANY - the companies to dump the alerts for (aggregate).
            | * -a AFTER - a lower time constraint on the alerts.
            | * -b before - an upper time constraint on the alerts.
            | * CENTRAL-CONFIGURATION-FILE - the central configuration file to use to load missing switches. (default: ${default_config_file})
EOFSEQ
    ) | sed -r 's/^\s*\|?//'
}

function perror()
{
    trace "error: ${@}"
    print_help >&2
    exit 1
}

function cleanup()
{
    if [ -z "${keep_intermediate:-}" ]
    then
        [ -n "${output_directory:-}" ] && rm -rf "${output_directory}"
    fi

    return 0
}

function acquire_alerts_lock()
{
    local readonly lock_fd="200"
    local readonly lock_file="${locks_directory}/${prog_name}.lock"

    mkdir -p "${locks_directory}"

    exec 200>"${lock_file}"

    if flock -n "${lock_fd}"
    then
        trace "successfully acquired program lock - ${locks_directory}"
        return 0
    else
        trace "failed to acquired program lock - ${locks_directory}"
        return 1
    fi
}

function load_pythonenv()
{
    local configuration_file="${1}"
    local virtual_env="${2}"

    if [ -z "${virtual_env}" -a -f "${configuration_file}" ]
    then
        virtual_env=$(
            awk -F'=' '
                $0 ~ /^\s*virtualenv\s*=/ {
                    virtualenv=$2
                    gsub(/(^\s*)|(\s*)$/, "", virtualenv)
                    print virtualenv
                    exit 0
                }
            ' "${configuration_file}"
        )
    fi

    if [ -n "${virtual_env}" ]
    then
        trace "loading python virtual environement from ${virtual_env}"

        virtualenv_activate="${virtual_env}/bin/activate"

        set +eu # we cant control how well the activation script is written
        [ -f "${virtualenv_activate}" ] && source "${virtualenv_activate}"
        set -eu
    fi

    "${test_python_requirements_script}" -P -s "3.6" || perror "missing python packages or python version isn't 3.6+"
}

function dump_alerts()
{
    local company=""
    local dump_output=""
    local intermediate_dir=""
    local user=""
    local db_host=""
    local port=""

    local after=""
    local before=""

    OPTIND=1
    while getopts ":c:o:w:a:b:u:d:p:" option
    do
        case "${option}" in
            "c") company="${OPTARG}"
                 ;;

            "o") dump_output="${OPTARG}"
                 ;;

            "w") intermediate_dir="${OPTARG}"
                 ;;

            "a") [ -n "${OPTARG}" ] && after="alerts-after=${OPTARG}"
                 ;;

            "b") [ -n "${OPTARG}" ] && before="alerts-before=${OPTARG}"
                 ;;

            "u") user="${OPTARG}"
                 ;;

            "d") db_host="${OPTARG}"
                 ;;

            "p") port="${OPTARG}"
                 ;;

            *) perror "dump_alerts(): got an unknown switch ${OPTARG}"
        esac
    done

    [ -n "${company}" ] || perror "dump_alerts(): missing company."
    [ -n "${dump_output}" ] || perror "dump_alerts(): missing output file."
    [ -n "${intermediate_dir}" ] || perror "dump_alerts(): missing intermediate directory."
    [ -n "${user}" ] || perror "dump_alerts(): missing user."
    [ -n "${db_host}" ] || perror "dump_alerts(): missing db host."
    [ -n "${port}" ] || perror "dump_alerts(): missing port."

    # create a db configuration file (because there's no way to pass the company as an argument
    # to the query generation script - it's a bit of a design flaw but it's good enough.
    local query_config_file="${intermediate_dir}/${company// /_}.config"

    (
        cat <<-EOFSEQ
            [companies]
            [[dbcompany]]
            dbtype=postgresql
            company="${company}"

            [query-flags]
            scheme=siem-alerts
            no-augmentation=
            ${after}
            ${before}
EOFSEQ
    ) | sed -r 's/^\s*//' >"${query_config_file}"

    local query_file="${intermediate_dir}/${company// /_}.query.sql"
    "${query_generation_script}" --db-config "${query_config_file}" companies \
                                 --config-file "${query_config_file}" query-flags \
                                 -d dbcompany >"${query_file}"

    # and now execute the damn thing
    trace "dumping alerts for ${company} to ${dump_output}"
    psql -U "${user}" -h "${db_host}" -p "${port}" -d secdo -q -A -F "$(echo -ne '\t')" -t <"${query_file}" \
    | awk '
        # this awk script skips any output preceding the actual header and table
        # by looking for the SCHEME_DUMP_MARKER_XXXXX marker generated by the
        # query genreation script.

        BEGIN {
            marker_found = 0
        }

        marker_found == 1 {print;} # print everything after the marker
        marker_found == 0 && $0 ~ /^SCHEME_DUMP_MARKER/ {marker_found = 1;} # skip lines until the marker
    ' >"${dump_output}" || return 1
}

function main()
{
    trap cleanup EXIT

    local companies_counter=0
    local companies=()

    local source_postgresql=""
    local source_postgresql_user=""
    local source_postgresql_port=""

    local x_flag=""

    local after=""
    local before=""

    local ftp_credentials=""
    local ftp_server=""
    local ftp_root_dir=""
    local ftp_server_cert=""

    OPTIND=1
    while getopts ":hxko:e:s:r:i:t:f:p:u:c:a:b:" option
    do
        case "${option}" in
            "h") print_help >&2
                 exit 0
                 ;;

            "x") set -x
                 x_flag="1"
                 ;;

            "k") keep_intermediate="1"
                 ;;

            "o") output_directory="${OPTARG}"
                 ;;

            "e") virtual_env="${OPTARG}"
                 ;;

            "s") source_postgresql="${OPTARG}"
                 ;;

            "r") ftp_server="${OPTARG}"
                 ;;

            "i") ftp_server_cert="${OPTARG}"
                 ;;

            "t") ftp_root_dir="${OPTARG}"
                 ;;

            "f") ftp_credentials="${OPTARG}"
                 ;;

            "p") source_postgresql_port="${OPTARG}"
                 ;;

            "u") source_postgresql_user="${OPTARG}"
                 ;;

            "c") let "companies_counter += 1"
                 companies[${companies_counter}]="${OPTARG}"
                 ;;

            "a") after="${OPTARG}"
                 ;;

            "b") before="${OPTARG}"
                 ;;

            *) perror "invalid switch encountered."
        esac
    done

    shift $((${OPTIND}-1))

    [ "${#}" -le 1 ] || perror "too many configuration files specified!"

    configuration_file=""
    if [ "${#}" -eq 1 ]
    then
        configuration_file="${1}"
    elif [ -f "${default_config_file}" ]
    then
        configuration_file="${default_config_file}"
    fi

    trace "acquiring program lock..."
    acquire_alerts_lock || perror "failed to acquire alerts dump lock - perhaps another instance of this script is running."
    trace "program lock acquired"

    trace "setting up python environment"
    load_pythonenv "${configuration_file}" "${virtual_env:-}"

    trace "loading data from configuration"

    x_flag=$("${ini_query_field}" -eqf "bash-x" -r "${x_flag}" "${configuration_file}" "${configuration_section}") # bash -x flag
    if [ -n "${x_flag}" ]
    then
        set -x
    fi

    keep_intermediate=$("${ini_query_field}" -eqf "keep-intermediate" -r "${keep_intermediate:-}" "${configuration_file}" "${configuration_section}") # keep intermediate files flag

    output_directory=$("${ini_query_field}" -f "output" -r "${output_directory:-}" "${configuration_file}" "${configuration_section}")

    after=$("${ini_query_field}" -qf "after" -r "${after}" "${configuration_file}" "${configuration_section}")
    before=$("${ini_query_field}" -qf "before" -r "${before}" "${configuration_file}" "${configuration_section}")

    ftp_credentials=$("${ini_query_field}" -f "ftp-credentials" -r "${ftp_credentials}" "${configuration_file}" "${configuration_section}")
    ftp_server_cert=$("${ini_query_field}" -f "ftp-server-cert" -r "${ftp_server_cert}" "${configuration_file}" "${configuration_section}")
    ftp_server=$("${ini_query_field}" -f "ftp-server" -r "${ftp_server}" -d "${default_ftp_server}" "${configuration_file}" "${configuration_section}")
    ftp_root_dir=$("${ini_query_field}" -f "ftp-root-dir" -r "${ftp_root_dir}" -d "${default_ftp_root_dir}" "${configuration_file}" "${configuration_section}")

    source_postgresql=$("${ini_query_field}" -q -f "postgresql" -r "${source_postgresql}" "${configuration_file}" "${configuration_section}")
    source_postgresql_port=$("${ini_query_field}" -q -f "postgresql-port" -r "${source_postgresql_port}" "${configuration_file}" "${configuration_section}")
    source_postgresql_user=$("${ini_query_field}" -q -f "postgresql-user" -r "${source_postgresql_user}" "${configuration_file}" "${configuration_section}")

    # load defaults
    [ -z "${source_postgresql}" ] && source_postgresql="${default_source_postgresql}"
    [ -z "${source_postgresql_port}" ] && source_postgresql_port="${default_source_postgresql_port}"
    [ -z "${source_postgresql_user}" ] && source_postgresql_user="${default_source_postgresql_user}"

    trace "setting up output directory"
    output_directory="${output_directory}/alerts-dump-output"
    local dumps_dir="${output_directory}/dumps"
    local intermediate_dir="${output_directory}/intermediate"

    # clear any pre-existing directory and start a new
    rm -rf "${output_directory}"
    mkdir -p "${output_directory}" "${dumps_dir}" "${intermediate_dir}"

    trace "output directory set to ${output_directory}"

    local summary_file="${dumps_dir}/content.summary"
    "${summary_script}" init -r -t "alerts-dump" -f product-file value -f company value "${summary_file}"
    trace "summary file created at ${summary_file}"

    trace "dumping alerts from ${source_postgresql_user}@${source_postgresql}:${source_postgresql_port}"

    # use this variable to avoid naming two dumps with the same name
    local dump_index="0"

    # if the companies were specified via the command line then use them
    if [ "${#companies[@]}" -gt 0 ]
    then
        for i in ${!companies[@]}
        do
            echo "${companies[i]}"
        done

    # if no companies were specified using the command line then take them from the configuration
    else
        "${ini_query}" -k companies "${configuration_file}" "${configuration_section}"
    fi \
    | while read company
    do
        trace "dumping alerts for ${company}"

        let "dump_index += 1"

        local alerts_dump_file_name="${company// /_}-alerts.${dump_index}.csv"
        local alerts_dump_csv="${dumps_dir}/${alerts_dump_file_name}"

        local success="0"
        for _ in 1
        do
            dump_alerts -c "${company}" \
                        -o "${alerts_dump_csv}" \
                        -w "${intermediate_dir}" \
                        -d "${source_postgresql}" \
                        -u "${source_postgresql_user}" \
                        -p "${source_postgresql_port}" \
                        -a "${after}" \
                        -b "${before}" || break
            success="1"
        done

        if [ "1" == "${success}" ]
        then
            trace "successfully dumped alerts for ${company}"
            "${summary_script}" add-line -f product-file "${alerts_dump_file_name}" -f company "${company}" "${summary_file}"
        else
            trace "failed to dump alerts for ${company}"
        fi
    done

    # package everything in a neat .tar.gz
    local alerts_bundle="${output_directory}/alerts-$(head -c 8 /dev/urandom | xxd -ps).tar.gz"
    tar -czf "${alerts_bundle}" -C "${output_directory}" "$(basename ${dumps_dir})"

    # upload everything to the research FTPS server
    curl --ssl -k \
         -T "${alerts_bundle}" \
         -E "${ftp_credentials}" \
         --cacert "${ftp_server_cert}" \
         "ftp://${ftp_server}/${ftp_root_dir}/"
}

main ${@}

