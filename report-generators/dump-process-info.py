#!/usr/bin/env python3

import argparse
import logging
import hashlib
import uuid
import getpass
import sys
import contextlib
import json

from utilities import dbcursors, default_dbs

GroupedTables = [
    'process_instance_start',
    'network_events',
    'code_injection_events',
    'file_system_events',
    'executed_by_events',
    'module_events',
    'performance_monitor',
    'registry_events',
    'stop_process',
    'user_presence',
    'usb_devices',
    'tty_events',
    'window_activity_events',
]

@contextlib.contextmanager
def stdout_context():
    yield sys.stdout

def get_proc_basic_info(cursor, host, pid, instance):
    logging.info("resolving process name")
    instances = cursor.execute(f"""
        SELECT DISTINCT
            processPath,
            sessionUser,
            processSigned,
            processSignedValid,
            processSignedCompanyName,
            processSignedProductName

        FROM
            process_instance_start

        WHERE
            hostId='{host}' AND
            secdoProcessId='{pid}' AND
            instanceId={instance}
    """).fetchall()

    assert len(instances) == 1
    proc_inst = instances[0]

    logging.info("resolving parentage")
    parents = []
    queue = [(pid, instance)]

    while len(queue) > 0:
        proc_id, inst_id = queue.pop(0)

        parent = cursor.execute(f"""
            SELECT DISTINCT
                parentInstanceId,
                secdoParentId,
                parentFullFilePath,
                parentInstanceId

            FROM
                process_instance_start

            WHERE
                hostId='{host}' AND
                secdoProcessId='{proc_id}' AND
                instanceId={inst_id}
        """).fetchall()

        if len(parent) != 1:
            break

        parent = parent[0]

        if parent['parentFullFilePath'] == "":
            break

        parent_proc_id = parent['secdoParentId']
        parent_inst = parent['parentInstanceId']
        signature = cursor.execute(f"""
            SELECT DISTINCT
                processSigned,
                processSignedValid,
                processSignedCompanyName,
                processSignedProductName

            FROM
                process_instance_start

            WHERE
                hostId='{host}' AND
                secdoProcessId='{parent_proc_id}' AND
                instanceId={parent_inst}
        """).fetchall()

        if len(signature) > 1:
            break

        if len(signature) == 0:
            parent_entry = {
                'signed' : False,
                'sig_valid' : False,
                'sig_company' : False,
                'sig_product' : False,
            }
        else:
            sig = dict(signature[0])
            parent_entry = {
                'signed' : sig['processSigned'],
                'sig_valid' : sig['processSignedValid'],
                'sig_company' : sig['processSignedCompanyName'],
                'sig_product' : sig['processSignedProductName'],
            }

        parent_entry['path'] = parent['parentFullFilePath']
        parents.append(parent_entry)

        queue.append((parent['secdoParentId'], parent['parentInstanceId']))

    logging.info("resolving children")
    children = [
        {
            'path' : child['processPath'],
            'signed' : child['processSigned'],
            'sig_valid' : child['processSignedValid'],
            'sig_company' : child['processSignedCompanyName'],
            'sig_product' : child['processSignedProductName'],
        }

        for child in cursor.execute(f"""
        SELECT DISTINCT
            processPath,
            processSigned,
            processSignedValid,
            processSignedCompanyName,
            processSignedProductName

        FROM
            process_instance_start

        WHERE
            hostId='{host}' AND
            secdoParentId='{pid}' AND
            parentInstanceId={instance}
        """).fetchall()
    ]

    return {
        'path' : proc_inst['processPath'],
        'user' : proc_inst['sessionUser'],
        'signed' : proc_inst['processSigned'],
        'sig_valid' : proc_inst['processSignedValid'],
        'sig_company' : proc_inst['processSignedCompanyName'],
        'sig_product' : proc_inst['processSignedProductName'],
        'parents' : parents,
        'children' : children,
    }

def get_network_info(cursor, host, pid, instance, proc_start_time):
    out_connections = cursor.execute(f"""
        SELECT DISTINCT
            timestamp,
            networkAccessType,
            remoteIpv4,
            remotePort,
            country,
            remoteRealHostname

        FROM
            network_events

        WHERE
            hostId='{host}' AND
            secdoProcessId='{pid}' AND
            instanceId={instance} AND

            networkAccessType IN (
                1  /*TCP Connect*/,
                11 /*UDP Connect*/
            )

        ORDER BY
            timestamp
    """).fetchall()

    in_connections = cursor.execute(f"""
        SELECT DISTINCT
            timestamp,
            networkAccessType,
            remoteIpv4,
            localPort,
            country,
            remoteRealHostname

        FROM
            network_events

        WHERE
            hostId='{host}' AND
            secdoProcessId='{pid}' AND
            instanceId={instance} AND

            networkAccessType IN (
                2  /*TCP Accept */,
                13 /*UDP Listen */
            )

        ORDER BY
            timestamp
    """).fetchall()

    return {
        'outgoing' : {
            'tcp' : [
                {'remote' : conn['remoteIpv4'], 'remote_port' : conn['remotePort'], 'country' : conn['country'], 'remote_host' : conn['remoteRealHostname'], 'timing' : (conn['timestamp']-proc_start_time).total_seconds()}
                for conn in out_connections if conn['networkAccessType'] == 1 # TCP Connect
            ],
            'udp' : [
                {'remote' : conn['remoteIpv4'], 'remote_port' : conn['remotePort'], 'country' : conn['country'], 'remote_host' : conn['remoteRealHostname'], 'timing' : (conn['timestamp']-proc_start_time).total_seconds()}
                for conn in out_connections if conn['networkAccessType'] == 11 # UDP Connect
            ],
        },

        'incoming' : {
            'tcp' : [
                {'remote' : conn['remoteIpv4'], 'local_port' : conn['localPort'], 'country' : conn['country'], 'remote_host' : conn['remoteRealHostname'], 'timing' : (conn['timestamp']-proc_start_time).total_seconds()}
                for conn in in_connections if conn['networkAccessType'] == 2 # TCP Accept
            ],
            'udp' : [
                {'remote' : conn['remoteIpv4'], 'local_port' : conn['localPort'], 'country' : conn['country'], 'remote_host' : conn['remoteRealHostname'], 'timing' : (conn['timestamp']-proc_start_time).total_seconds()}
                for conn in in_connections if conn['networkAccessType'] == 13 # UDP Listen
            ],
        },
    }

def get_injections_info(cursor, host, pid, instance):
    injected_by = cursor.execute(f"""
        SELECT DISTINCT
            processPath

        FROM
            process_instance_start

        WHERE
            secdoProcessId IN (
                SELECT DISTINCT
                    secdoProcessId

                FROM
                    code_injection_events

                WHERE
                    hostId='{host}' AND
                    injectedFullFilePathId='{pid}' AND
                    injectedInstanceId={instance}
            )
    """).fetchall()

    injected_to = cursor.execute(f"""
        SELECT DISTINCT
            injectedFullFilePath

        FROM
            code_injection_events

        WHERE
            hostId='{host}' AND
            secdoProcessId='{pid}' AND
            instanceId={instance}
    """).fetchall()

    return {
        'injected_by' : [inj['processPath'] for inj in injected_by],
        'injected_to' : [inj['injectedFullFilePath'] for inj in injected_to],
    }

def get_fs_info(cursor, host, pid, instance):
    files = cursor.execute(f"""
        SELECT DISTINCT
            fileAccessType,
            filePath,
            oldFilePath

        FROM
            file_system_events

        WHERE
            hostId='{host}' AND
            secdoProcessId='{pid}' AND
            instanceId={instance}
    """).fetchall()

    opened = [_file['filePath'] for _file in files if _file['fileAccessType'] == 1]
    written = [_file['filePath'] for _file in files if _file['fileAccessType'] == 2]
    created = [_file['filePath'] for _file in files if _file['fileAccessType'] == 3]
    renamed = [
        {'src' : _file['oldFilePath'], 'dst' : _file['filePath']}
        for _file in files if _file['fileAccessType'] == 4
    ]
    deleted = [_file['filePath'] for _file in files if _file['fileAccessType'] == 5]

    dirs_renamed = [
        {'src' : _dir['oldFilePath'], 'dst' : _dir['filePath']}
        for _dir in files if _dir['fileAccessType'] == 6
    ]
    dirs_created = [_dir['filePath'] for _dir in files if _dir['fileAccessType'] == 7]
    dirs_deleted = [_dir['filePath'] for _dir in files if _dir['fileAccessType'] == 8]

    copied = [
        {'dst' : _file['filePath'], 'src' : _file['oldFilePath']}
        for _file in files if _file['fileAccessType'] == 9
    ]

    uniq = lambda l: list(set(l))
    return {
        'opened' : uniq(opened),
        'modified' : uniq(written),
        'created' : uniq(created),
        'renamed' : renamed,
        'deleted' : uniq(deleted),
        'dirs_renamed' : dirs_renamed,
        'dirs_created' : uniq(dirs_created),
        'dirs_deleted' : uniq(dirs_deleted),
        'copied' : copied,
    }

def get_modules_info(cursor, host, pid, instance, proc_start_time):
    modules = cursor.execute(f"""
        SELECT DISTINCT
            timestamp,
            filePath

        FROM
            module_events

        WHERE
            hostId='{host}' AND
            secdoProcessId='{pid}' AND
            instanceId={instance}

        ORDER BY
            timestamp
    """).fetchall()

    return [
        {'module' : module['filePath'], 'load_time' : (module['timestamp']-proc_start_time).total_seconds()}
        for module in modules
    ]

def get_registry_info(cursor, host, pid, instance):
    registry = cursor.execute(f"""
        SELECT DISTINCT
            registryAccessType,
            keyName,
            oldKeyName

        FROM
            registry_events

        WHERE
            hostId='{host}' AND
            secdoProcessId='{pid}' AND
            instanceId={instance}
    """).fetchall()

    keys_created = [reg['keyName'] for reg in registry if reg['registryAccessType'] == 1]
    keys_deleted = [reg['keyName'] for reg in registry if reg['registryAccessType'] == 2]
    values_set = [reg['keyName'] for reg in registry if reg['registryAccessType'] == 3]
    values_deleted = [reg['keyName'] for reg in registry if reg['registryAccessType'] == 4]
    keys_renamed = [
        {'dst' : reg['keyName'], 'src' : reg['oldKeyName']}
        for reg in registry if reg['registryAccessType'] == 5
    ]
    keys_loaded = [reg['keyName'] for reg in registry if reg['registryAccessType'] == 6]
    keys_unloaded = [reg['keyName'] for reg in registry if reg['registryAccessType'] == 7]
    keys_saved = [reg['keyName'] for reg in registry if reg['registryAccessType'] == 8]
    keys_restored = [reg['keyName'] for reg in registry if reg['registryAccessType'] == 9]

    return {
        'keys_created' : keys_created,
        'keys_deleted' : keys_deleted,
        'values_set' : values_set,
        'values_deleted' : values_deleted,
        'keys_renamed' : keys_renamed,
        'keys_loaded' : keys_loaded,
        'keys_unloaded' : keys_unloaded,
        'keys_saved' : keys_saved,
        'keys_restored' : keys_restored,
    }

def dump_proc_info(cursor, host, instance, process):
    process_filter = "1=1" if process is None else f"processPath ILIKE '{process}'"
    pid = cursor.execute(f"""
        SELECT DISTINCT
            processPath,
            secdoProcessId

        FROM
            process_instance_start

        WHERE
            hostId='{host}' AND
            instanceId={instance} AND
            {process_filter}
    """).fetchall()

    assert len(pid) == 1, Exception(pid)
    pid = pid[0]['secdoProcessId']

    proc_start_time = cursor.execute(f"""
        SELECT DISTINCT
            timestamp

        FROM
            process_instance_start

        WHERE
            hostId='{host}' AND
            secdoProcessId='{pid}' AND
            instanceId={instance}
    """).fetchall()[0]['timestamp']

    proc_info = {}

    proc_info['unique_id'] = str(uuid.uuid4())
    proc_info['process'] = get_proc_basic_info(cursor, host, pid, instance)
    proc_info['network'] = get_network_info(cursor, host, pid, instance, proc_start_time)
    proc_info['injections'] = get_injections_info(cursor, host, pid, instance)
    proc_info['file_system'] = get_fs_info(cursor, host, pid, instance)
    proc_info['modules'] = get_modules_info(cursor, host, pid, instance, proc_start_time)
    proc_info['registry'] = get_registry_info(cursor, host, pid, instance)

    return proc_info

def main():
    db_defaults = default_dbs.load_defaults()

    parser = argparse.ArgumentParser(description="dump the complete information about a process instance.")

    parser.add_argument("-d", "--db", choices=list(db_defaults.keys()), help="the database to dump the information from.")
    parser.add_argument("-o", "--output", default='-', help="the output summary file. use '-' for stdout.")
    parser.add_argument("-v", "--verbose", action="store_true", help="show debug prints.")
    parser.add_argument("host", help="the host id the process ran on.")
    parser.add_argument("instance", help="the instance id of the process in question.")
    parser.add_argument("process", nargs='?', help="the process path.")

    args = parser.parse_args()

    logging.basicConfig(format="%(asctime)s %(message)s", level=logging.INFO if args.verbose else logging.WARN)

    db = db_defaults[args.db]
    schema = hashlib.md5(db['company'].encode()).hexdigest()

    cursor = dbcursors.cursors.option(db['dbtype'])(
        host=db['host'],
        database=db['database'],
        user=db['user'],
        password=db['password'] if 'password' in db else getpass.getpass('Database password: ')
    )

    group = int(args.host[-2:], 16) & 0x7F
    with dbcursors.multigroupcursor(dbcursors.tracecursor(cursor), [group], GroupedTables).context() as dcursor:
        dcursor.setschema(schema)
        proc_info = dump_proc_info(dcursor, args.host, args.instance, args.process)

    proc_info.setdefault('process', {})
    proc_info['process']['company'] = db['company']
    with (stdout_context() if '-' == args.output else open(args.output, "wt")) as output:
        json.dump(proc_info, output, indent=4)

if __name__ == "__main__":
    main()

