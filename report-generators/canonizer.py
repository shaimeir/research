#!/usr/bin/env python3

import re
import pathlib
from collections import namedtuple

import path_canonizer

WellKnownProcesses = None
WellKnownSigners = None
WellKnownRegistryKeys = None
WellKnownProducts = None

class _ProcessCanonizer:
    CurrentDir = pathlib.PureWindowsPath("")
    WindowsDir = pathlib.PureWindowsPath("C:\\Windows")
    SpaceRegex = re.compile(r"\s+")
    BackslashRegex = re.compile("\\\\+")

    def __init__(self, process_path, signed=False, sig_valid=None, sig_company=None, sig_product=None):
        self.process_path = pathlib.PureWindowsPath(process_path)
        self.signed = signed
        self.sig_valid = sig_valid
        self.sig_company = sig_company.lower().strip() if sig_company is not None else None
        self.sig_product = sig_product.lower().strip() if sig_product is not None else None

    def _canonizeProcessName(self, process_path):
        process_path = self.SpaceRegex.sub(' ', process_path.lower().strip())
        if process_path in WellKnownProcesses:
            return process_path

        process = pathlib.PureWindowsPath(process_path)
        if process == self.process_path:
            return 'self'
        
        # leniently account for agent (or is it OS?) bugs that don't yield the full path but just the file name
        if (process.parent == self.CurrentDir or self.process_path.parent == self.CurrentDir) and process.stem == self.process_path.stem:
            return 'self'

        # find the closest common root
        common_ancestors = {path for path in set(process.parents).intersection(set(self.process_path.parents)) if path != self.CurrentDir}
        if len(common_ancestors) > 0:
            closest_ancestor = max(common_ancestors, key=len)
            # parts example : 'C:' 'Program Files' 'Something'
            if len(closest_ancestor.parts) > 2:
                return 'same-package'

        return self.BackslashRegex.sub('\\\\', path_canonizer.canonicalize_path(process_path))

    def _canonizeCompany(self, company):
        company = self.SpaceRegex.sub(' ', company.lower().strip())
        if self.signed and company == self.sig_company:
            return ['self']

        company = set(company.split())
        signers = company.intersection(WellKnownSigners)

        if signers != company:
            signers.add('signed')

        return list(signers)

    def _canonizeProduct(self, product):
        product = self.SpaceRegex.sub(' ', product.lower().strip())
        if self.signed and product == self.sig_product:
            return 'self'
        elif product in WellKnownProducts:
            return product
        else:
            return 'product'

    @staticmethod
    def _buildProcDict(process, signed, sig_valid, sig_company, sig_product):
        return {
            'path' : process,
            'signed' : signed,
            'sig_valid' : sig_valid,
            'sig_company' : sig_company,
            'sig_product' : sig_product,
        }

    def canonize(self, process, signed=False, sig_valid=None, sig_company=None, sig_product=None):
        process = self._canonizeProcessName(process)
        if signed:
            sig_company = self._canonizeCompany(sig_company)
            sig_product = self._canonizeProduct(sig_product)
            return self._buildProcDict(process, signed, sig_valid, sig_company, sig_product)
        else:
            return self._buildProcDict(process, False, None, None, None)

def _canonize_processes(report):
    process = report['process']
    canonizer = _ProcessCanonizer(process['path'],
                                  process['signed'],
                                  process['sig_valid'],
                                  process['sig_company'],
                                  process['sig_product'])

    process['children'] = [
        canonizer.canonize(child['path'], child['signed'], child['sig_valid'], child['sig_company'], child['sig_product'])
        for child in process['children']
    ]

    process['parents'] = [
        canonizer.canonize(parent['path'], parent['signed'], parent['sig_valid'], parent['sig_company'], parent['sig_product'])
        for parent in process['parents']
    ]

    return report

def canonize(report):
    report = _canonize_processes(report)
    return report

def _load_list_file(list_file_name):
    _space = re.compile(r"\s+")

    list_file = pathlib.Path(__file__).parent/'well-known'/list_file_name
    if list_file.is_file():
        with open(list_file, 'rt') as _list_file:
            return {line.lower() for line in (_space.sub(' ', _line.strip()) for _line in _list_file) if line != ''}

    return set()

def module_init():
    global WellKnownProcesses
    global WellKnownSigners
    global WellKnownRegistryKeys
    global WellKnownProducts

    WellKnownProcesses = _load_list_file('well-known-procs')
    WellKnownSigners = _load_list_file('well-known-signers')
    WellKnownRegistryKeys = _load_list_file('well-known-reg-keys')
    WellKnownProducts = _load_list_file('well-known-products')

module_init()

